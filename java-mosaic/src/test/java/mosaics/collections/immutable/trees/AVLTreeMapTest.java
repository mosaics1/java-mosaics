package mosaics.collections.immutable.trees;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import lombok.Value;
import mosaics.fp.FP;
import mosaics.fp.collections.FPIterable;
import mosaics.junit.JMRandomExtension;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.opentest4j.AssertionFailedError;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import static mosaics.lang.Backdoor.cast;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;


@ExtendWith(JMRandomExtension.class)
public class AVLTreeMapTest {
    @Nested
    public class EqualsTestCases {
        @Test
        public void testEquals() {
            assertTrue(AVLTreeMap.empty().equals(AVLTreeMap.empty()));
            assertTrue(AVLTreeMap.of("a",1).equals(AVLTreeMap.of("a",1)));
        }
    }

    @Nested
    public class BasicPutGetTestCases {
        @Test
        public void givenAnEmptyTree_callIsEmpty_expectTrue() {
            FPTreeMap<String,Integer> tree = AVLTreeMap.empty();

            assertTrue( tree.isEmpty() );
            assertFalse( tree.hasContents() );
            assertEquals( 17, tree.hashCode() );
        }

        @Test
        public void givenAnEmptyTree_getByAKey_expectNoValueBack() {
            FPTreeMap<String,Integer> tree = AVLTreeMap.empty();

            assertEquals( FP.emptyOption(), tree.get("a") );
        }

        @Test
        public void givenAnEmptyTree_addAValue_callIsEmpty_expectFalse() {
            FPTreeMap<String,Integer> tree = AVLTreeMap.of( "a", 1 );

            assertFalse( tree.isEmpty() );
            assertTrue( tree.hasContents() );
            assertEquals( 17+"a".hashCode(), tree.hashCode() );
        }

        @Test
        public void givenAnEmptyTree_addAValue_getByKey_expectValueBack() {
            FPTreeMap<String,Integer> tree = AVLTreeMap.of( "a", 1 );

            assertEquals( FP.option(1), tree.get("a") );
        }

        @Test
        public void givenAnEmptyTree_addAValue_getWrongKey_expectNoMatch() {
            FPTreeMap<String,Integer> tree = AVLTreeMap.of( "a", 1 );

            assertEquals( FP.emptyOption(), tree.get("b") );
        }


        @Test
        public void givenOneValue_addSameValue_expectSameInstanceOfTreeBack() {
            FPTreeMap<String,Integer> tree = AVLTreeMap.of( "a", 1 );

            assertSame( tree, tree.put("a", 1) );
        }

        @Test
        public void givenOneValue_addUpdateExistingKey_expectOldValueToBeReplaced() {
            FPTreeMap<String,Integer> tree = AVLTreeMap.<String,Integer>empty()
                                                      .put( "a", 1 )
                                                      .put( "a", 2 );

            assertEquals( FP.option(2), tree.get("a") );
            assertEquals( 17+"a".hashCode(), tree.hashCode() );
        }

        @Test
        public void givenOneValue_addToLHS_callGetByKey_expectMatch() {
            FPTreeMap<String,Integer> tree = AVLTreeMap.<String,Integer>empty()
                                                      .put( "b", 2 )
                                                      .put( "a", 1 );

            assertEquals( FP.option(1), tree.get("a") );
            assertEquals( FP.option(2), tree.get("b") );

            assertEquals( new TreeNode<>("b", 2, FP.option("a"), FP.emptyOption()), tree.getNodeFor("b").get() );
            assertEquals( new TreeNode<>("a", 1, FP.emptyOption(), FP.emptyOption()), tree.getNodeFor("a").get() );
            assertEquals( 17+"a".hashCode()+"b".hashCode(), tree.hashCode() );
        }

        @Test
        public void givenOneValue_addToLHS_callGetByMissingKey_expectNoMatch() {
            FPTreeMap<String,Integer> tree = AVLTreeMap.<String,Integer>empty()
                                                      .put( "b", 2 )
                                                      .put( "a", 1 );

            assertEquals( FP.emptyOption(), tree.get("c") );
        }

        @Test
        public void givenOneValue_addToRHS_callGetByKey_expectMatch() {
            FPTreeMap<String,Integer> tree = AVLTreeMap.<String,Integer>empty()
                .put( "b", 2 )
                .put( "c", 3 );

            assertEquals( FP.option(3), tree.get("c") );
            assertEquals( FP.option(2), tree.get("b") );

            assertEquals( new TreeNode<>("b", 2, FP.emptyOption(), FP.option("c")), tree.getNodeFor("b").get() );
            assertEquals( new TreeNode<>("c", 3, FP.emptyOption(), FP.emptyOption()), tree.getNodeFor("c").get() );
            assertEquals( 17+"b".hashCode()+"c".hashCode(), tree.hashCode() );
        }

        @Test
        public void givenOneValue_addToRHS_callGetByMissingKey_expectNoMatch() {
            FPTreeMap<String,Integer> map = AVLTreeMap.<String,Integer>empty()
                .put( "b", 2 )
                .put( "c", 3 );

            assertEquals( FP.emptyOption(), map.get("a") );
        }

        @Test
        public void createTreeWithOfDepthTwo_ensureAllNodesCanBeRetrieved() {
            FPTreeMap<String,Integer> tree = AVLTreeMap.<String,Integer>empty()
                .put( "b", 2 )
                .put( "a", 1 )
                .put( "d", 4 )
                .put( "c", 3 )
                .put( "e", 5 );

            assertEquals( new TreeNode<>("a", 1, FP.emptyOption(), FP.emptyOption()), tree.getNodeFor("a").get() );
            assertEquals( new TreeNode<>("b", 2, FP.option("a"), FP.option("d")), tree.getNodeFor("b").get() );
            assertEquals( new TreeNode<>("d", 4, FP.option("c"), FP.option("e")), tree.getNodeFor("d").get() );
            assertEquals( new TreeNode<>("c", 3, FP.emptyOption(), FP.emptyOption()), tree.getNodeFor("c").get() );
            assertEquals( new TreeNode<>("e", 5, FP.emptyOption(), FP.emptyOption()), tree.getNodeFor("e").get() );

            assertEquals( FP.option(1), tree.get("a") );
            assertEquals( FP.option(2), tree.get("b") );
            assertEquals( FP.option(3), tree.get("c") );
            assertEquals( FP.option(4), tree.get("d") );
            assertEquals( FP.option(5), tree.get("e") );

            assertEquals( 17+"a".hashCode()+"b".hashCode()+"c".hashCode()+"d".hashCode()+"e".hashCode(), tree.hashCode() );
        }

        @Test
        public void createTreeWithOfDepthTwo_duplicateALHSNode_ensureRootDoesNotChange() {
            FPTreeMap<String,Integer> tree = AVLTreeMap.<String,Integer>empty()
                .put( "b", 2 )
                .put( "a", 1 )
                .put( "d", 4 )
                .put( "c", 3 )
                .put( "e", 5 );

            assertSame( tree, tree.put("d", 4) );
        }

        @Test
        public void createRandomTree_ensureAllValuesCanBeRetrieved(
            @JMRandomExtension.Random List<KVPair> rndEntries
        ) {
            FPTreeMap<String,String> map = AVLTreeMap.empty();
            for ( KVPair kv : rndEntries ) {
                map = map.put( kv.getKey(), kv.getValue() );
            }

            // when duplicate keys are generated, expect the latest one to win
            Map<String,String> expectations = rndEntries.stream().collect(
                Collectors.toMap(KVPair::getKey,KVPair::getValue, (a,b) -> cast(b))
            );

            for ( Map.Entry<String,String> kv : expectations.entrySet() ) {
                assertEquals( FP.option(kv.getValue()), map.get(kv.getKey()) );
            }
        }
    }

    @Nested
    public class SizeAndHeightTestCases {
        @Test
        public void givenEmptyTree_expectValueCountOfZero() {
            FPTreeMap<String,Integer> map = AVLTreeMap.empty();

            assertEquals( 0, map.getValueCount() );
        }

        @Test
        public void givenEmptyTree_expectHeightOfZero() {
            FPTreeMap<String,Integer> map = AVLTreeMap.empty();

            assertEquals( 0, map.getHeightCount() );
        }

        @Test
        public void givenSingleValueInTree_expectValueCountOfOne() {
            FPTreeMap<String,Integer> map = AVLTreeMap.of("5", 5);

            assertEquals( 1, map.getValueCount() );
        }

        @Test
        public void givenSingleValueInTree_expectHeightOfOne() {
            FPTreeMap<String,Integer> map = AVLTreeMap.of("5", 5);

            assertEquals( 1, map.getHeightCount() );
        }

        @Test
        public void givenTwoValuesInTree_expectValueCountOfTwo() {
            FPTreeMap<String,Integer> map = AVLTreeMap.of("5", 5, "4", 4);

            assertEquals( 2, map.getValueCount() );
        }

        @Test
        public void givenTwoValuesInTree_expectHeightOfTwo() {
            FPTreeMap<String,Integer> map = AVLTreeMap.of("5", 5, "4", 4);

            assertEquals( 2, map.getHeightCount() );
        }

        @Test
        public void givenBalancedTreeWithThreeValues_expectValueCountOfThree() {
            FPTreeMap<String,Integer> map = AVLTreeMap.of("5", 5, "4", 4, "6", 6);

            assertEquals( 3, map.getValueCount() );
        }

        @Test
        public void givenBalancedTreeWithThreeValues_expectHeightOfTwo() {
            FPTreeMap<String,Integer> map = AVLTreeMap.of("5", 5, "4", 4, "6", 6);

            assertEquals( 2, map.getHeightCount() );
        }

        @Test
        public void givenTreeWithFourValues_expectValueCountOfFour() {
            FPTreeMap<String,Integer> map = AVLTreeMap.of("5", 5, "4", 4, "6", 6, "7", 7);

            assertEquals( 4, map.getValueCount() );
        }

        @Test
        public void givenTreeWithFourValues_expectHeightOfThree() {
            FPTreeMap<String,Integer> map = AVLTreeMap.of("5", 5, "4", 4, "6", 6, "7", 7);

            assertEquals( 3, map.getHeightCount() );
        }
    }

    @Nested
    public class AutoBalancingTestCases {
        @Test
        public void givenThreeValuesInAscendingOrder_expectTreeToRotateLeft() {
            FPTreeMap<String,Integer> tree = AVLTreeMap.of("a", 1, "b", 2, "c", 3 );

            assertEquals( 3, tree.getValueCount() );
            assertEquals( 2, tree.getHeightCount() );

            assertEquals( new TreeNode<>("a",1), tree.getNodeFor("a").get() );
            assertEquals( new TreeNode<>("b",2, "a","c"), tree.getNodeFor("b").get() );
            assertEquals( new TreeNode<>("c",3), tree.getNodeFor("c").get() );

            assertEquals( 17+"a".hashCode()+"b".hashCode()+"c".hashCode(), tree.hashCode() );
        }

        @Test
        public void givenThreeValuesInDescendingOrder_expectTreeToRotateRight() {
            FPTreeMap<String,Integer> map = AVLTreeMap.of("c", 3, "b", 2, "a", 1 );

            assertEquals( 3, map.getValueCount() );
            assertEquals( 2, map.getHeightCount() );

            assertEquals( new TreeNode<>("a",1), map.getNodeFor("a").get() );
            assertEquals( new TreeNode<>("b",2, "a","c"), map.getNodeFor("b").get() );
            assertEquals( new TreeNode<>("c",3), map.getNodeFor("c").get() );
        }

        @Test
        public void givenTenValuesInAscendingOrder() {
            FPTreeMap<Integer,String> tree1 = createMapRange( 1, 11 );

            assertEquals( List.of(1,2,3,4,5,6,7,8,9,10), tree1.entriesAsc().map(TreeNode::getKey).toList() );
        }

        @Test
        public void givenRandomSetOfValues_ensureTreeRemainsBalancedAndWellFormedAsEachValueIsEntered( @JMRandomExtension.Random List<String> keys ) {
            FPTreeMap<String,String> tree = AVLTreeMap.empty();
            for ( String key : keys ) {
                System.out.println(key);
                tree = tree.put( key, key );

                assertTreeIsWellFormed( tree );
            }
        }

        @Test
        public void givenRLTree_expectToRebalance() {
            FPTreeMap<Integer,String> tree = AVLTreeMap.of(
                5,"5",
                20,"20",
                10,"10"
            );

            assertTreeIsWellFormed( tree );
        }

        @Test
        public void givenLRTree_expectToRebalance() {
            FPTreeMap<Integer,String> tree = AVLTreeMap.of(
                20,"20",
                5,"5",
                10,"10"
            );

            assertTreeIsWellFormed( tree );
        }

        @Test
        public void insertIntoABalancedTreeSuchThatOnlyRotatingAtTheRootWillImbalanceTheTree_ensureThatTheImplDoesNotFallForThisCase() {
            FPTreeMap<Integer,Integer> tree = AVLTreeMap.of(
                21,21,
                5,5,
                20,20,
                4,4,
                10, 10
            );

            assertTreeIsWellFormed( tree );

            FPTreeMap<Integer,Integer> tree2 = tree.put( 15,15 );
            assertTreeIsWellFormed( tree2 );
        }

        private FPTreeMap<Integer,String> createMapRange( int fromInc, int toExc ) {
            FPTreeMap<Integer,String> tree  = AVLTreeMap.empty();
            FPIterable<Integer>       range = FP.range( fromInc, toExc );

            for ( int i : range ) {
                tree = tree.put( i, Integer.toString(i) );
            }

            return tree;
        }
    }

    @Nested
    public class TreeWalkTestCases {
        @Test
        public void inorderDepthFirst() {
            assertInorderDepthFirst( 11 );
        }

        @Test
        public void inorderDepthFirst( @JMRandomExtension.Random int i ) {
            assertInorderDepthFirst( Math.min(Math.abs(i),100) );
        }

        private void assertInorderDepthFirst( int numEntries ) {
            FPTreeMap<Integer,String> map   = AVLTreeMap.empty();
            FPIterable<Integer>       range = FP.range( 0, numEntries );

            for ( int i : range ) {
                map = map.put( i, Integer.toString(i) );
            }

            assertTreeAsc( map, range );
        }

        private void assertTreeAsc( FPTreeMap<Integer,String> tree, FPIterable<Integer> expectations ) {
            List<Integer> expectedKeys   = expectations.toList();
            List<String>  expectedValues = expectations.map(i -> Integer.toString(i)).toList();

            assertEquals( expectedKeys, tree.entriesAsc().map( Map.Entry::getKey ).toList() );
            assertEquals( expectedValues, tree.entriesAsc().map( Map.Entry::getValue ).toList() );
        }
    }

    @Nested
    public class RemoveTestCases {
        @Test
        public void givenOneNode_removeIt_expectAnEmptyTree() {
            FPTreeMap<String,Integer> tree   = AVLTreeMap.of("a",1);
            FPTreeMap<String,Integer> actual = tree.remove("a");

            assertTrue( actual.isEmpty() );
            assertEquals( 17, actual.hashCode() );
        }

        @Test
        public void givenOneNode_tryRemovingNodeThatDoesNotExist_expectUnchangedTreeBack() {
            FPTreeMap<String,Integer> tree   = AVLTreeMap.of("a",1);
            FPTreeMap<String,Integer> actual = tree.remove("b");

            assertSame( tree, actual );
            assertEquals( 17+"a".hashCode(), tree.hashCode() );
        }

        @Test
        public void givenLHSChildNode_removeChild_expectParentOnlyBack() {
            FPTreeMap<String,Integer> tree   = AVLTreeMap.of("b",2, "a", 1);
            FPTreeMap<String,Integer> actual = tree.remove("a");

            assertEquals( new TreeNode<>("b", 2, FP.emptyOption(), FP.emptyOption()), actual.getRoot().get() );
            assertEquals( 17+"b".hashCode(), actual.hashCode() );
        }

        @Test
        public void givenLHSChildNode_removeParent_expectChildOnlyBack() {
            FPTreeMap<String,Integer> tree   = AVLTreeMap.of("b",2, "a", 1);
            FPTreeMap<String,Integer> actual = tree.remove("b");

            assertEquals( new TreeNode<>("a", 1, FP.emptyOption(), FP.emptyOption()), actual.getRoot().get() );
            assertEquals( 17+"a".hashCode(), actual.hashCode() );
        }

        @Test
        public void givenRHSChildNode_removeChild_expectParentOnlyBack() {
            FPTreeMap<String,Integer> tree   = AVLTreeMap.of("b",2, "c", 3);
            FPTreeMap<String,Integer> actual = tree.remove("c");

            assertEquals( new TreeNode<>("b", 2, FP.emptyOption(), FP.emptyOption()), actual.getRoot().get() );
            assertEquals( 17+"b".hashCode(), actual.hashCode() );
        }

        @Test
        public void givenRHSChildNode_removeParent_expectChildOnlyBack() {
            FPTreeMap<String,Integer> tree   = AVLTreeMap.of("b",2, "c", 3);
            FPTreeMap<String,Integer> actual = tree.remove("b");

            assertEquals( new TreeNode<>("c", 3, FP.emptyOption(), FP.emptyOption()), actual.getRoot().get() );
        }

        @Test
        public void givenTreeTwoDeep_removeRoot() {
            FPTreeMap<String,Integer> map    = AVLTreeMap.of(
                "20",20,
                "15", 15, "25", 25,
                "14", 14, "16", 16, "24", 24, "26", 26
            );
            FPTreeMap<String,Integer> actual = map.remove("20");

            assertEquals(List.of(14,15,16,24,25,26), actual.entriesAsc().map(TreeNode::getValue).toList());
            assertTreeIsWellFormed( actual );
            assertEquals( 17+"14".hashCode()+"15".hashCode()+"16".hashCode()+"24".hashCode()+"25".hashCode()+"26".hashCode(), actual.hashCode() );
        }

        //
        //
        //
        // givenTreeTwoDeep_removeRootsLHS
        // givenTreeTwoDeep_removeRootsRHS
        // givenTreeTwoDeep_removeLHSLHS
        // givenTreeTwoDeep_removeLHSRHS
        // givenTreeTwoDeep_removeRHSLHS
        // givenTreeTwoDeep_removeRHSRHS

        // givenRandomTree_removeRandomNodes
    }

    @Nested
    public class MinMaxTestCases {
        @Test
        public void givenEmptyTree_callMin_expectNoMatch() {
            FPTreeMap<Integer,String> tree = AVLTreeMap.empty();

            assertEquals( FP.emptyOption(), tree.minEntry() );
        }

        @Test
        public void givenSingleValueTree_callMin_expectValue() {
            FPTreeMap<Integer,String> tree = AVLTreeMap.of(1,"a");

            assertEquals( FP.option(new E<>(1,"a")), tree.minEntry() );
        }

        @Test
        public void givenTree_callMin_expectMin() {
            FPTreeMap<Integer,String> tree = AVLTreeMap.of(
                4, "d",
                3, "c",
                6, "f",
                1, "a",
                2, "b",
                5, "e",
                7, "g"
            );

            assertEquals( FP.option(new E<>(1,"a")), tree.minEntry() );
        }
        
        @Test
        public void givenEmptyTree_callMax_expectNoMatch() {
            FPTreeMap<Integer,String> tree = AVLTreeMap.empty();

            assertEquals( FP.emptyOption(), tree.maxEntry() );
        }

        @Test
        public void givenSingleValueTree_callMax_expectValue() {
            FPTreeMap<Integer,String> tree = AVLTreeMap.of(1,"a");

            assertEquals( FP.option(new E<>(1,"a")), tree.maxEntry() );
        }

        @Test
        public void givenTree_callMax_expectMax() {
            FPTreeMap<Integer,String> tree = AVLTreeMap.of(
                4, "d",
                3, "c",
                6, "f",
                1, "a",
                2, "b",
                5, "e",
                7, "g"
            );

            assertEquals( FP.option(new E<>(7,"g")), tree.maxEntry() );
        }
    }

    @Nested
    public class GetNextPreviousTestCases {
        @Test
        public void givenEmptyTree_getNext_expectNone() {
            assertEquals( FP.emptyOption(), AVLTreeMap.empty().getNextEntry(0) );
        }

        @Test
        public void givenSingleValueTree_callGetNextWithExactMatch_expectNone() {
            FPTreeMap<Integer,String> tree = createTree(7);

            assertEquals( FP.emptyOption(), tree.getNextEntry(7) );
        }

        @Test
        public void givenSingleValueTree_callGetNextWithKeyGreaterThanValue_expectNone() {
            FPTreeMap<Integer,String> tree = createTree(7);

            assertEquals( FP.emptyOption(), tree.getNextEntry(8) );
        }

        @Test
        public void givenSingleValueTree_callGetNextWithKeyLessThanValue_expectMatch() {
            FPTreeMap<Integer,String> tree = createTree(7);

            assertEquals( FP.option(new E<>(7,"h")), tree.getNextEntry(6) );
        }

        @Test
        public void givenThreeValueTree_callGetNextForEachKeyPossibility() {
            FPTreeMap<Integer,String> tree = createTree(4,1,6);

            assertEquals( FP.option(new E<>(1,"b")), tree.getNextEntry(-1) );
            assertEquals( FP.option(new E<>(1,"b")), tree.getNextEntry(0) );
            assertEquals( FP.option(new E<>(4,"e")), tree.getNextEntry(1) );
            assertEquals( FP.option(new E<>(4,"e")), tree.getNextEntry(2) );
            assertEquals( FP.option(new E<>(4,"e")), tree.getNextEntry(3) );
            assertEquals( FP.option(new E<>(6,"g")), tree.getNextEntry(4) );
            assertEquals( FP.option(new E<>(6,"g")), tree.getNextEntry(5) );
            assertEquals( FP.emptyOption(), tree.getNextEntry(6) );
            assertEquals( FP.emptyOption(), tree.getNextEntry(7) );
        }

        @Test
        public void givenSevenValueTree_callGetNextForEachKeyPossibility() {
            FPTreeMap<Integer,String> tree = createTree(2, 4, 6, 8, 10, 12, 14);

            assertEquals( FP.option(new E<>(2,"c")), tree.getNextEntry(-1) );
            assertEquals( FP.option(new E<>(2,"c")), tree.getNextEntry(0) );
            assertEquals( FP.option(new E<>(2,"c")), tree.getNextEntry(1) );
            assertEquals( FP.option(new E<>(4,"e")), tree.getNextEntry(2) );
            assertEquals( FP.option(new E<>(4,"e")), tree.getNextEntry(3) );
            assertEquals( FP.option(new E<>(6,"g")), tree.getNextEntry(4) );
            assertEquals( FP.option(new E<>(6,"g")), tree.getNextEntry(5) );
            assertEquals( FP.option(new E<>(8,"i")), tree.getNextEntry(6) );
            assertEquals( FP.option(new E<>(8,"i")), tree.getNextEntry(7) );
            assertEquals( FP.option(new E<>(10,"k")), tree.getNextEntry(8) );
            assertEquals( FP.option(new E<>(10,"k")), tree.getNextEntry(9) );
            assertEquals( FP.option(new E<>(12,"m")), tree.getNextEntry(10) );
            assertEquals( FP.option(new E<>(12,"m")), tree.getNextEntry(11) );
            assertEquals( FP.option(new E<>(14,"o")), tree.getNextEntry(12) );
            assertEquals( FP.option(new E<>(14,"o")), tree.getNextEntry(13) );
            assertEquals( FP.emptyOption(), tree.getNextEntry(14) );
            assertEquals( FP.emptyOption(), tree.getNextEntry(15) );
            assertEquals( FP.emptyOption(), tree.getNextEntry(16) );
        }

        @Test
        public void givenEmptyTree_getPrevious_expectNone() {
            assertEquals( FP.emptyOption(), AVLTreeMap.empty().getPreviousEntry(0) );
        }

        @Test
        public void givenSingleValueTree_callGetPreviousWithExactMatch_expectNone() {
            FPTreeMap<Integer,String> tree = createTree(7);

            assertEquals( FP.emptyOption(), tree.getPreviousEntry(7) );
        }

        @Test
        public void givenSingleValueTree_callGetPreviousWithKeyLessThanValue_expectNone() {
            FPTreeMap<Integer,String> tree = createTree(7);

            assertEquals( FP.emptyOption(), tree.getPreviousEntry(6) );
        }

        @Test
        public void givenSingleValueTree_callGetPreviousWithKeyGreaterThanValue_expectMatch() {
            FPTreeMap<Integer,String> tree = createTree(7);

            assertEquals( FP.option(new E<>(7,"h")), tree.getPreviousEntry(8) );
        }

        @Test
        public void givenThreeValueTree_callGetPreviousForEachKeyPossibility() {
            FPTreeMap<Integer,String> tree = createTree(4,1,6);

            assertEquals( FP.emptyOption(), tree.getPreviousEntry(-1) );
            assertEquals( FP.emptyOption(), tree.getPreviousEntry(0) );
            assertEquals( FP.emptyOption(), tree.getPreviousEntry(1) );
            assertEquals( FP.option(new E<>(1,"b")), tree.getPreviousEntry(2) );
            assertEquals( FP.option(new E<>(1,"b")), tree.getPreviousEntry(3) );
            assertEquals( FP.option(new E<>(1,"b")), tree.getPreviousEntry(4) );
            assertEquals( FP.option(new E<>(4,"e")), tree.getPreviousEntry(5) );
            assertEquals( FP.option(new E<>(4,"e")), tree.getPreviousEntry(6) );
            assertEquals( FP.option(new E<>(6,"g")), tree.getPreviousEntry(7) );
            assertEquals( FP.option(new E<>(6,"g")), tree.getPreviousEntry(8) );
        }

        @Test
        public void givenSevenValueTree_callGetPreviousForEachKeyPossibility() {
            FPTreeMap<Integer,String> tree = createTree(2, 4, 6, 8, 10, 12, 14);

            assertEquals( FP.emptyOption(), tree.getPreviousEntry(-1) );
            assertEquals( FP.emptyOption(), tree.getPreviousEntry(0) );
            assertEquals( FP.emptyOption(), tree.getPreviousEntry(1) );
            assertEquals( FP.emptyOption(), tree.getPreviousEntry(2) );
            assertEquals( FP.option(new E<>(2,"c")), tree.getPreviousEntry(3) );
            assertEquals( FP.option(new E<>(2,"c")), tree.getPreviousEntry(4) );
            assertEquals( FP.option(new E<>(4,"e")), tree.getPreviousEntry(5) );
            assertEquals( FP.option(new E<>(4,"e")), tree.getPreviousEntry(6) );
            assertEquals( FP.option(new E<>(6,"g")), tree.getPreviousEntry(7) );
            assertEquals( FP.option(new E<>(6,"g")), tree.getPreviousEntry(8) );
            assertEquals( FP.option(new E<>(8,"i")), tree.getPreviousEntry(9) );
            assertEquals( FP.option(new E<>(8,"i")), tree.getPreviousEntry(10) );
            assertEquals( FP.option(new E<>(10,"k")), tree.getPreviousEntry(11) );
            assertEquals( FP.option(new E<>(10,"k")), tree.getPreviousEntry(12) );
            assertEquals( FP.option(new E<>(12,"m")), tree.getPreviousEntry(13) );
            assertEquals( FP.option(new E<>(12,"m")), tree.getPreviousEntry(14) );
            assertEquals( FP.option(new E<>(14,"o")), tree.getPreviousEntry(15) );
            assertEquals( FP.option(new E<>(14,"o")), tree.getPreviousEntry(16) );
            assertEquals( FP.option(new E<>(14,"o")), tree.getPreviousEntry(17) );
            assertEquals( FP.option(new E<>(14,"o")), tree.getPreviousEntry(18) );
            assertEquals( FP.option(new E<>(14,"o")), tree.getPreviousEntry(100) );
        }

        @Test
        public void givenEmptyTree_getClosest_expectNone() {
            assertEquals( FP.emptyOption(), AVLTreeMap.empty().getClosestEntry(0) );
        }

        @Test
        public void givenSingleValueTree_callGetClosestWithExactMatch_expectMatch() {
            FPTreeMap<Integer,String> tree = createTree(7);

            assertEquals( FP.option(new E<>(7,"h")), tree.getClosestEntry(7) );
        }

        @Test
        public void givenSingleValueTree_callGetClosestWithKeyLessThanValue_expectMatch() {
            FPTreeMap<Integer,String> tree = createTree(7);

            assertEquals( FP.option(new E<>(7,"h")), tree.getClosestEntry(6) );
        }

        @Test
        public void givenSingleValueTree_callGetClosestWithKeyGreaterThanValue_expectMatch() {
            FPTreeMap<Integer,String> tree = createTree(7);

            assertEquals( FP.option(new E<>(7,"h")), tree.getClosestEntry(8) );
        }

        @Test
        public void givenThreeValueTree_callGetClosestForEachKeyPossibility() {
            FPTreeMap<Integer,String> tree = createTree(4,1,7);

            assertEquals( FP.option(new E<>(1,"b")), tree.getClosestEntry(-1) );
            assertEquals( FP.option(new E<>(1,"b")), tree.getClosestEntry(0) );
            assertEquals( FP.option(new E<>(1,"b")), tree.getClosestEntry(1) );
            assertEquals( FP.option(new E<>(1,"b")), tree.getClosestEntry(2) );
            assertEquals( FP.option(new E<>(4,"e")), tree.getClosestEntry(3) );
            assertEquals( FP.option(new E<>(4,"e")), tree.getClosestEntry(4) );
            assertEquals( FP.option(new E<>(4,"e")), tree.getClosestEntry(5) );
            assertEquals( FP.option(new E<>(7,"h")), tree.getClosestEntry(6) );
            assertEquals( FP.option(new E<>(7,"h")), tree.getClosestEntry(7) );
            assertEquals( FP.option(new E<>(7,"h")), tree.getClosestEntry(8) );
        }

        @Test
        public void givenSevenValueTree_callGetClosestForEachKeyPossibility() {
            FPTreeMap<Integer,String> tree = createTree(2, 5, 8, 11, 14, 17, 20);

            assertEquals( FP.option(new E<>(2,"c")), tree.getClosestEntry(-1) );
            assertEquals( FP.option(new E<>(2,"c")), tree.getClosestEntry(0) );
            assertEquals( FP.option(new E<>(2,"c")), tree.getClosestEntry(1) );
            assertEquals( FP.option(new E<>(2,"c")), tree.getClosestEntry(2) );
            assertEquals( FP.option(new E<>(2,"c")), tree.getClosestEntry(3) );
            assertEquals( FP.option(new E<>(5,"f")), tree.getClosestEntry(4) );
            assertEquals( FP.option(new E<>(5,"f")), tree.getClosestEntry(5) );
            assertEquals( FP.option(new E<>(5,"f")), tree.getClosestEntry(6) );
            assertEquals( FP.option(new E<>(8,"i")), tree.getClosestEntry(7) );
            assertEquals( FP.option(new E<>(8,"i")), tree.getClosestEntry(8) );
            assertEquals( FP.option(new E<>(8,"i")), tree.getClosestEntry(9) );
            assertEquals( FP.option(new E<>(11,"l")), tree.getClosestEntry(10) );
            assertEquals( FP.option(new E<>(11,"l")), tree.getClosestEntry(11) );
            assertEquals( FP.option(new E<>(11,"l")), tree.getClosestEntry(12) );
            assertEquals( FP.option(new E<>(14,"o")), tree.getClosestEntry(13) );
            assertEquals( FP.option(new E<>(14,"o")), tree.getClosestEntry(14) );
            assertEquals( FP.option(new E<>(14,"o")), tree.getClosestEntry(15) );
            assertEquals( FP.option(new E<>(17,"r")), tree.getClosestEntry(16) );
            assertEquals( FP.option(new E<>(17,"r")), tree.getClosestEntry(17) );
            assertEquals( FP.option(new E<>(17,"r")), tree.getClosestEntry(18) );
            assertEquals( FP.option(new E<>(20,"u")), tree.getClosestEntry(19) );
            assertEquals( FP.option(new E<>(20,"u")), tree.getClosestEntry(20) );
            assertEquals( FP.option(new E<>(20,"u")), tree.getClosestEntry(21) );
            assertEquals( FP.option(new E<>(20,"u")), tree.getClosestEntry(22) );
            assertEquals( FP.option(new E<>(20,"u")), tree.getClosestEntry(23) );
            assertEquals( FP.option(new E<>(20,"u")), tree.getClosestEntry(100) );
        }
    }

    @Nested
    public class FilterValuesTestCases {
        @Test
        public void givenEmptyTree_callFilterValues_expectNoChange() {
            FPTreeMap<String,String> originalTree = AVLTreeMap.empty();

            assertSame(originalTree, originalTree.filterValues(v -> false));
        }

        @Test
        public void givenFullTree_callFilterValuesWithNoMatches_expectNoChange() {
            FPTreeMap<String,String> originalTree = AVLTreeMap.of("B","2","A","1","C","3");

            assertSame(originalTree, originalTree.filterValues(v -> true));
        }

        @Test
        public void givenFullTree_callFilterValuesWithMatches_expectMatchesToBeRemovedFromNewTree() {
            FPTreeMap<String,String> originalTree = AVLTreeMap.of("B","2","A","1","C","3");

            assertEquals(AVLTreeMap.empty(), originalTree.filterValues(v -> false));
            assertEquals(AVLTreeMap.of("B","2","C","3"), originalTree.filterValues(v -> !v.equals("1")));
            assertEquals(AVLTreeMap.of("A","1","C","3"), originalTree.filterValues(v -> !v.equals("2")));
            assertEquals(AVLTreeMap.of("A","1","B","2"), originalTree.filterValues(v -> !v.equals("3")));
        }
    }

    private static FPTreeMap<Integer,String> createTree(int...keys) {
        return FP.wrapArray(keys).fold(
            AVLTreeMap.empty((a,b) -> a - b),
            (tree,nextKey) -> tree.put(nextKey, Character.toString('a'+nextKey))
        );
    }

    @Getter
    @ToString
    @AllArgsConstructor
    public static class E<K,V> implements Map.Entry<K,V> {
        private K key;
        private V value;

        public V setValue( V value ) {
            throw new UnsupportedOperationException();
        }

        public int hashCode() {
            return Objects.hashCode( key );
        }

        public boolean equals( Object o ) {
            if ( o instanceof Map.Entry other) {
                return Objects.equals(this.getKey(), other.getKey()) && Objects.equals(this.getValue(), other.getValue());
            } else {
                return false;
            }
        }
    }

    @Value
    public static class KVPair {
        private String key;
        private String value;
    }

    private static <K extends Comparable<K>,V> void assertTreeIsWellFormed( FPTreeMap<K,V> tree ) {
        try {
            tree.iterator().forEachRemaining( node -> assertTreeNodeIsWellFormed( tree, node ) );
        } catch ( AssertionFailedError ex ) {
            tree.dumpTree();
            throw ex;
        }
    }

    private static <K extends Comparable<K>,V> void assertTreeNodeIsWellFormed( FPTreeMap<K,V> tree, TreeNode<K,V> node ) {
        assertTreeNodeChildrenAreOrdered( node );

        int lhsHeight = node.getLhsKey().map( tree::getHeightCount ).orElse( 0 );
        int rhsHeight = node.getRhsKey().map( tree::getHeightCount ).orElse( 0 );

        assertTrue( Math.abs(lhsHeight-rhsHeight) <= 1 );
    }

    private static <K extends Comparable<K>,V> void assertTreeNodeChildrenAreOrdered( TreeNode<K,V> node ) {
        K key = node.getKey();

        node.getLhsKey().ifPresent( lhs -> assertTrue(lhs.compareTo(key) < 0) );
        node.getRhsKey().ifPresent( rhs -> assertTrue(rhs.compareTo(key) > 0) );
    }
}
