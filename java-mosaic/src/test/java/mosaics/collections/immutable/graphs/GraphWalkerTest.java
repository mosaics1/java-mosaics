package mosaics.collections.immutable.graphs;

import mosaics.fp.FP;
import mosaics.fp.collections.ConsList;
import mosaics.fp.collections.FPOption;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class GraphWalkerTest {


// WALK THE FOLLOWING PRE-EXISTING GRAPH

    // A -> B -> C -> D
    //   e1   e2   e3
    //   ------> C
    //        e4
    //   <-
    //   e5
    //        <------
    //        e6

    private Graph<String,String> testGraph = new GraphBuilder<String,String>()
        .withEdge( "A", "B", "e1" )
        .withEdge( "B", "C", "e2" )
        .withEdge( "C", "D", "e3" )
        .withEdge( "A", "C", "e4" )
        .withEdge( "B", "A", "e5" )
        .withEdge( "D", "B", "e6" )
        .build();


    @Test
    public void tryWalkingEdgeLabelThatDoesNotExistAtNode_expectNone() {
        FPOption<Node<String,String>> actual = testGraph.createGraphWalker(0)
            .walk( FP.wrapAll("e2") )
            .getMatchedNode();

        assertEquals( FP.emptyOption(), actual );
    }

    @Test
    public void tryWalkingEdgeLabelThatDoesNotExistAtNode_expectNodePathWillContainTheStartNodeOnly() {
        ConsList<Node<String,String>> actual = testGraph.createGraphWalker(0)
            .walk( FP.wrapAll("e2") )
            .getNodesTraversed();

        assertEquals( nodeList(0), actual );
    }

    @Test
    public void tryWalkingEdgeLabelThatDoesNotExistAtNode_expectEdgePathWillBeEmpty() {
        ConsList<Edge<String>> actual = testGraph.createGraphWalker(0)
            .walk( FP.wrapAll("e2") )
            .getEdgesTraversed();

        assertEquals( FP.nil(), actual );
    }

    @Test
    public void walkOneEdgeThatExists_expectEdgePathToBeUpdated() {
        ConsList<Edge<String>> actual = testGraph.createGraphWalker(0)
            .walk( FP.wrapAll("e1") )
            .getEdgesTraversed();

        assertEquals( edgeList(0), actual );
    }

    @Test
    public void walkOneEdgeThatExists_expectNodePathToBeUpdated() {
        ConsList<Node<String,String>> actual = testGraph.createGraphWalker(0)
            .walk( FP.wrapAll("e1") )
            .getNodesTraversed();

        assertEquals( nodeList(0,1), actual );
    }

    @Test
    public void walkOneEdgeThatExists_expectCurrentNodeToBeUpdated() {
        FPOption<Node<String,String>> actual = testGraph.createGraphWalker(0)
            .walk( FP.wrapAll("e1") )
            .getCurrentNode();

        assertEquals( testGraph.getNode(1), actual );
    }

    @Test
    public void walkTwoEdgesThatExists_expectEdgePathToBeUpdated() {
        ConsList<Edge<String>> actual = testGraph.createGraphWalker(0)
            .walk( FP.wrapAll("e1", "e2") )
            .getEdgesTraversed();

        assertEquals( edgeList(0,1), actual );
    }

    @Test
    public void walkTwoEdgesThatExists_expectNodePathToBeUpdated() {
        ConsList<Node<String,String>> actual = testGraph.createGraphWalker(0)
            .walk( FP.wrapAll("e1", "e2") )
            .getNodesTraversed();

        assertEquals( nodeList(0,1,2), actual );
    }

    @Test
    public void walkTwoEdgesThatExists_expectCurrentNodeToBeUpdated() {
        FPOption<Node<String,String>> actual = testGraph.createGraphWalker(0)
            .walk( FP.wrapAll("e1", "e2") )
            .getCurrentNode();

        assertEquals( testGraph.getNode(2), actual );
    }

    @Test
    public void tryWalkingEdgeLabelExistsAndThenOneThatDoesNotExistAtNode_expectNoCurrentNode() {
        FPOption<Node<String,String>> actual = testGraph.createGraphWalker(0)
            .walk( FP.wrapAll("e1", "e3") )
            .getCurrentNode();

        assertEquals( FP.emptyOption(), actual );
    }

    @Test
    public void tryWalkingEdgeLabelExistsAndThenOneThatDoesNotExistAtNode_expectNodePathToContainTwoNodes() {
        ConsList<Node<String,String>> actual = testGraph.createGraphWalker(0)
            .walk( FP.wrapAll("e1", "e3") )
            .getNodesTraversed();

        assertEquals( nodeList(0,1), actual );
    }

    @Test
    public void tryWalkingEdgeLabelExistsAndThenOneThatDoesNotExistAtNode_expectEdgePathToContainOneEdge() {
        ConsList<Edge<String>> actual = testGraph.createGraphWalker(0)
            .walk( FP.wrapAll("e1", "e3") )
            .getEdgesTraversed();

        assertEquals( edgeList(0), actual );
    }


// AUTO CREATE ENABLED

    @Test
    public void tryWalkingEdgeLabelThatDoesNotExistAndThenAnotherAtNodeAndAutoCreateIsEnabled_expectNewNodesToBeAdded() {
        GraphWalker<String, String, String> updatedWalker = testGraph.<String>createGraphWalker( 0 )
            .withAutoCreateFlag( true )
            .walk( FP.wrapAll( "e2", "e1" ) );

        FPOption<Node<String,String>> actual = updatedWalker.getCurrentNode();

        assertEquals( updatedWalker.getGraph().getNode(5), actual );
    }

    @Test
    public void tryWalkingEdgeLabelThatDoesNotExistAndThenAnotherAtNodeAndAutoCreateIsEnabled_expectCurrentNodeMovesOn() {
        GraphWalker<String, String, String> updatedWalker = testGraph.<String>createGraphWalker( 0 )
            .withAutoCreateFlag( true )
            .walk( FP.wrapAll( "e2", "e1" ) );

        ConsList<Node<String,String>> actual = updatedWalker.getNodesTraversed();

        assertEquals( nodeList(updatedWalker.getGraph(),0,4,5), actual );
    }

    @Test
    public void tryWalkingEdgeLabelThatDoesNotExistAndThenAnotherAtNodeAndAutoCreateIsEnabled_expectEdgePathIsUpdated() {
        GraphWalker<String, String, String> updatedWalker = testGraph.<String>createGraphWalker( 0 )
            .withAutoCreateFlag( true )
            .walk( FP.wrapAll( "e2", "e1" ) );

        ConsList<Edge<String>> actual = updatedWalker.getEdgesTraversed();

        assertEquals( edgeList(updatedWalker.getGraph(),6,7), actual );
    }

    @Test
    public void tryWalkingEdgeLabelThatExistsAndThenOneThatDoesNotExistAtNodeAndAutoCreateIsEnabled_expectCurrentNodeMovesOn() {
        GraphWalker<String, String, String> updatedWalker = testGraph.<String>createGraphWalker( 0 )
            .withAutoCreateFlag( true )
            .walk( FP.wrapAll( "e1", "abc" ) );

        FPOption<Node<String,String>> actual = updatedWalker.getCurrentNode();

        assertEquals( updatedWalker.getGraph().getNode(4), actual );
    }

    @Test
    public void tryWalkingEdgeLabelThatExistsAndThenOneThatDoesNotExistAtNodeAndAutoCreateIsEnabled_expectNodePathIsUpdated() {
        GraphWalker<String, String, String> updatedWalker = testGraph.<String>createGraphWalker( 0 )
            .withAutoCreateFlag( true )
            .walk( FP.wrapAll( "e1", "abc" ) );

        ConsList<Node<String,String>> actual = updatedWalker.getNodesTraversed();

        assertEquals( nodeList(updatedWalker.getGraph(),0,1,4), actual );
    }

    @Test
    public void tryWalkingEdgeLabelThatExistsAndThenOneThatDoesNotExistAtNodeAndAutoCreateIsEnabled_expectEdgePathIsUpdated() {
        GraphWalker<String, String, String> updatedWalker = testGraph.<String>createGraphWalker( 0 )
            .withAutoCreateFlag( true )
            .walk( FP.wrapAll( "e1", "abc" ) );

        ConsList<Edge<String>> actual = updatedWalker.getEdgesTraversed();

        assertEquals( edgeList(updatedWalker.getGraph(),0,6), actual );
    }


    private ConsList<Edge<String>> edgeList(long...keys) {
        return edgeList( testGraph, keys );
    }

    private ConsList<Edge<String>> edgeList(Graph<String,String> graph, long...keys) {
        return ConsList.consList(keys).map(graph::getEdgeMandatory).toConsList().reverse();
    }

    private ConsList<Node<String,String>> nodeList(long...keys) {
        return nodeList( testGraph, keys );
    }

    private ConsList<Node<String,String>> nodeList( Graph<String,String> graph, long...keys) {
        return ConsList.consList(keys).map(graph::getNodeMandatory).toConsList().reverse();
    }

}
