package mosaics.collections.mutable.ring;

import mosaics.collections.SeqDouble;
import mosaics.fp.collections.DoubleIterable;
import mosaics.fp.collections.DoubleIterator;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.Iterator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;


public class ArrayRingBufferDoubleTest {
    @Nested
    public class GetByIndexTestCases {
        @Test
        public void givenAnEmptyBuffer_getIndexZero_expectException() {
            SeqDouble buf = new ArrayRingBufferDouble( 4 );

            try {
                buf.get( 0 );
                fail( "expected IndexOutOfBoundsException" );
            } catch ( IndexOutOfBoundsException ex ) {
                assertEquals( "index 0 is out of bounds, as the buffer is currently empty", ex.getMessage() );
            }
        }

        @Test
        public void givenAnEmptyBuffer_getIndexOne_expectException() {
            SeqDouble buf = new ArrayRingBufferDouble( 4 );

            try {
                buf.get( 1 );
                fail( "expected IndexOutOfBoundsException" );
            } catch ( IndexOutOfBoundsException ex ) {
                assertEquals( "index 1 is out of bounds, as the buffer is currently empty", ex.getMessage() );
            }
        }

        @Test
        public void givenAnEmptyBuffer_pushAndCallGetZero_expectToReadTheValueBack() {
            SeqDouble buf = new ArrayRingBufferDouble( 4 );

            buf.append( -42 );

            assertEquals( -42, buf.get( 0 ) );
        }

        @Test
        public void givenAnEmptyBuffer_pushLHSAndCallGetOne_expectException() {
            SeqDouble buf = new ArrayRingBufferDouble( 4 );

            buf.append( -42 );

            try {
                buf.get( 1 );
                fail( "expected IndexOutOfBoundsException" );
            } catch ( IndexOutOfBoundsException ex ) {
                assertEquals( "index 1 is out of bounds, currently supported indexes are 0 to 0 (inc)", ex.getMessage() );
            }
        }

        @Test
        public void givenAFullBuffer_showGetByIndexWillRetrieveEveryValueCorrectly() {
            SeqDouble buf = new ArrayRingBufferDouble( 4 );

            buf.appendAll( 10, 11, 12, 13 );

            assertEquals( 10, buf.get( 0 ) );
            assertEquals( 11, buf.get( 1 ) );
            assertEquals( 12, buf.get( 2 ) );
            assertEquals( 13, buf.get( 3 ) );
        }

        @Test
        public void givenAFullBuffer_showThatTryingToRetrieveBeyondTheRHSErrors() {
            SeqDouble buf = new ArrayRingBufferDouble( 4 );

            buf.appendAll( 10, 11, 12, 13 );

            try {
                buf.get( 4 );
                fail( "expected IndexOutOfBoundsException" );
            } catch ( IndexOutOfBoundsException ex ) {
                assertEquals( "index 4 is out of bounds, currently supported indexes are 0 to 3 (inc)", ex.getMessage() );
            }
        }

        @Test
        public void givenAFullBuffer_pushAnExtraValue_showThatTheLHSValueIsLostButTheNewValueIsKept() {
            SeqDouble buf = new ArrayRingBufferDouble( 4 );

            buf.appendAll( 10, 11, 12, 13 );
            buf.append( 14 );

            assertEquals( 11, buf.get( 1 ) );
            assertEquals( 12, buf.get( 2 ) );
            assertEquals( 13, buf.get( 3 ) );
            assertEquals( 14, buf.get( 4 ) );
        }

        @Test
        public void givenAFullBuffer_pushAnExtraValue_showThatTryingToRetrieveTheDroppedLHSErrors() {
            SeqDouble buf = new ArrayRingBufferDouble( 4 );

            buf.appendAll( 10, 11, 12, 13 );
            buf.append( 14 );

            try {
                buf.get( 0 );
                fail( "expected IndexOutOfBoundsException" );
            } catch ( IndexOutOfBoundsException ex ) {
                assertEquals( "index 0 is out of bounds, currently supported indexes are 1 to 4 (inc)", ex.getMessage() );
            }
        }
    }


    @Nested
    public class SizeTestCases {
        @Test
        public void givenAnEmptyBuffer_showThatNoMatterHowManyValuesArePushed_theSizeDoesNotChange() {
            SeqDouble buf = new ArrayRingBufferDouble( 4 );

            assertEquals( 4, buf.size() );
            for ( double v = 0; v < buf.size() * 10; v++ ) {
                buf.append( v );

                assertEquals( 4, buf.size() );
            }
        }
    }


    @Nested
    public class HasOverflowedTestCases {
        @Test
        public void givenEmptySeq_expectHasOverflowedToReturnFalse() {
            SeqDouble buf = new ArrayRingBufferDouble( 3 );

            assertFalse( buf.hasOverflowed() );
        }

        @Test
        public void givenOneSeq_expectHasOverflowedToReturnFalse() {
            SeqDouble buf = new ArrayRingBufferDouble( 3 );

            buf.append(7);

            assertFalse( buf.hasOverflowed() );
        }

        @Test
        public void givenFullSeq_expectHasOverflowedToReturnFalse() {
            SeqDouble buf = new ArrayRingBufferDouble( 3 );

            buf.append(7);
            buf.append(7);
            buf.append(7);

            assertFalse( buf.hasOverflowed() );
        }

        @Test
        public void givenFullSeq_appendOneMoreValue_expectHasOverflowedToReturnTrue() {
            SeqDouble buf = new ArrayRingBufferDouble( 3 );

            buf.append(7);
            buf.append(7);
            buf.append(7);
            buf.append(7);

            assertTrue( buf.hasOverflowed() );
        }
    }

    @Nested
    public class RemainingTestCases {
        @Test
        public void showThatRemainingDropsByOneEverytimeAValueIsPushedUpToSize_andThenStaysAtZeroFromThenOn() {
            SeqDouble buf = new ArrayRingBufferDouble( 4 );

            assertEquals( 4, buf.remaining() );

            buf.append( 42 );
            assertEquals( 3, buf.remaining() );

            buf.append( 42 );
            assertEquals( 2, buf.remaining() );

            buf.append( 42 );
            assertEquals( 1, buf.remaining() );

            buf.append( 42 );
            assertEquals( 0, buf.remaining() );

            for ( double v = 0; v < buf.size() * 2; v++ ) {
                buf.append( v );

                assertEquals( 0, buf.remaining() );
            }
        }
    }


    @Nested
    public class FilledTestCases {
        @Test
        public void showThatFilledStartsAtZeroAndThenClimbsUpToSizeAsEachValueIsPushedAndThenNeverIncreasesPastSize() {
            SeqDouble buf = new ArrayRingBufferDouble( 4 );

            assertEquals( 0, buf.contentsCount() );

            buf.append( 42 );
            assertEquals( 1, buf.contentsCount() );

            buf.append( 42 );
            assertEquals( 2, buf.contentsCount() );

            buf.append( 42 );
            assertEquals( 1, buf.remaining() );

            buf.append( 42 );
            assertEquals( 0, buf.remaining() );

            for ( double v = 0; v < buf.size() * 2; v++ ) {
                buf.append( v );

                assertEquals( 0, buf.remaining() );
            }
        }
    }


    @Nested
    public class SetTestCases {
        @Test
        public void givenAFullBuffer_showThatEveryValueCanBeSet() {
            SeqDouble buf = new ArrayRingBufferDouble( 4 );

            buf.appendAll( 1, 2, 3, 4 );

            buf.set( 2, 33 );
            assertEquals( "1.0, 2.0, 33.0, 4.0", buf.mkString() );

            buf.set( 0, 11 );
            assertEquals( "11.0, 2.0, 33.0, 4.0", buf.mkString() );

            buf.set( 3, 44 );
            assertEquals( "11.0, 2.0, 33.0, 44.0", buf.mkString() );

            buf.set( 1, 22 );
            assertEquals( "11.0, 22.0, 33.0, 44.0", buf.mkString() );
        }

        @Test
        public void givenBufferThatHasWrapped_showThatEveryValueCanBeSet() {
            SeqDouble buf = new ArrayRingBufferDouble( 4 );

            buf.appendAll( 1, 2, 3, 4, 5, 6, 7, 8 );

            buf.set( 6, 33 );
            assertEquals( "5.0, 6.0, 33.0, 8.0", buf.mkString() );

            buf.set( 4, 11 );
            assertEquals( "11.0, 6.0, 33.0, 8.0", buf.mkString() );

            buf.set( 7, 44 );
            assertEquals( "11.0, 6.0, 33.0, 44.0", buf.mkString() );

            buf.set( 5, 22 );
            assertEquals( "11.0, 22.0, 33.0, 44.0", buf.mkString() );
        }

        @Test
        public void givenBufferThatHasWrapped_tryAccessingOneToTheLeftOfTheLHSIndex_expectException() {
            SeqDouble buf = new ArrayRingBufferDouble( 4 );

            buf.appendAll( 1, 2, 3, 4, 5, 6, 7, 8 );

            try {
                buf.set( 3, 33 );
                fail( "Expected IndexOutOfBoundsException" );
            } catch ( IndexOutOfBoundsException ex ) {
                assertEquals( "index 3 is out of bounds, currently supported indexes are 4 to 7 (inc)", ex.getMessage() );
            }
        }

        @Test
        public void givenBufferThatHasWrapped_tryAccessingOneToTheRightOfTheRHSIndexInc_expectException() {
            SeqDouble buf = new ArrayRingBufferDouble( 4 );

            buf.appendAll( 1, 2, 3, 4, 5, 6, 7, 8 );

            try {
                buf.set( 8, 33 );
                fail( "Expected IndexOutOfBoundsException" );
            } catch ( IndexOutOfBoundsException ex ) {
                assertEquals( "index 8 is out of bounds, currently supported indexes are 4 to 7 (inc)", ex.getMessage() );
            }
        }
    }


    @Nested
    public class IsEmptyTestCases {
        @Test
        public void showThaIsEmptyStartsOfTrueAndThenBecomesFalseAfterTheFirstValueIsPushedAndThenRemainsThatWay() {
            SeqDouble buf = new ArrayRingBufferDouble( 4 );

            assertTrue( buf.isEmpty() );

            buf.append( 42 );
            assertFalse( buf.isEmpty() );

            for ( double v = 0; v < buf.size() * 2; v++ ) {
                buf.append( v );

                assertFalse( buf.isEmpty() );
            }
        }
    }


    @Nested
    public class IsFullTestCases {
        @Test
        public void showThaIsFullStartsOfFalseAndThenRemainsFalseUntilSizeNumberOfValuesHaveBeenPushedAndThenFromThenOnRemainsTrue() {
            SeqDouble buf = new ArrayRingBufferDouble( 4 );

            assertFalse( buf.isFull() );

            buf.append( 42 );
            assertFalse( buf.isFull() );

            buf.append( 42 );
            assertFalse( buf.isFull() );

            buf.append( 42 );
            assertFalse( buf.isFull() );

            buf.append( 42 );
            assertTrue( buf.isFull() );

            for ( double v = 0; v < buf.size() * 2; v++ ) {
                buf.append( v );

                assertTrue( buf.isFull() );
            }
        }
    }


    @Nested
    public class LHSIndexTestCases {
        @Test
        public void showThatTheLHSIndexRemainsAtZeroUntilTheBufferIsFull() {
            SeqDouble buf = new ArrayRingBufferDouble( 4 );

            assertEquals( 0, buf.lhsIndex() );

            buf.append( 42 );
            assertEquals( 0, buf.lhsIndex() );

            buf.append( 42 );
            assertEquals( 0, buf.lhsIndex() );

            buf.append( 42 );
            assertEquals( 0, buf.lhsIndex() );

            buf.append( 42 );
            assertEquals( 0, buf.lhsIndex() );

            int expectedLHS = 0;
            for ( double v = 0; v < buf.size() * 2; v++ ) {
                buf.append( v );

                expectedLHS += 1;
                assertEquals( expectedLHS, buf.lhsIndex() );
            }
        }
    }


    @Nested
    public class RHSIndexIncTestCases {
        @Test
        public void showThatRHSIndexIncStartsAtZeroAndIncrementsByOneEverytimeAValueIsPushed() {
            SeqDouble buf = new ArrayRingBufferDouble( 4 );

            assertEquals( 0, buf.rhsIndexInc() );

            buf.append( 42 );
            assertEquals( 0, buf.rhsIndexInc() );


            int expectedRHSInc = 0;
            for ( double v = 0; v < buf.size() * 2; v++ ) {
                buf.append( v );

                expectedRHSInc += 1;
                assertEquals( expectedRHSInc, buf.rhsIndexInc() );
            }
        }
    }


    @Nested
    public class RHSIndexExcTestCases {
        @Test
        public void showThatRHSIndexExcStartsAtZeroAndIncrementsByOneEverytimeAValueIsPushed() {
            SeqDouble buf = new ArrayRingBufferDouble( 4 );

            assertEquals( 0, buf.rhsIndexExc() );

            int expectedRHSInc = 0;
            for ( double v = 0; v < buf.size() * 2; v++ ) {
                buf.append( v );

                expectedRHSInc += 1;
                assertEquals( expectedRHSInc, buf.rhsIndexExc() );
            }
        }
    }


    @Nested
    public class IteratorTestCases {
        @Test
        public void testTheIteratorByInvokingMkStringOnIterator() {
            SeqDouble buf = new ArrayRingBufferDouble( 4 );

            assertEquals( "", buf.iterator().mkString() );

            buf.append( 1 );
            assertEquals( "1.0", buf.iterator().mkString() );

            buf.append( 2 );
            assertEquals( "1.0, 2.0", buf.iterator().mkString() );

            buf.append( 3 );
            assertEquals( "1.0, 2.0, 3.0", buf.iterator().mkString() );

            buf.append( 4 );
            assertEquals( "1.0, 2.0, 3.0, 4.0", buf.iterator().mkString() );

            buf.append( 5 );
            assertEquals( "2.0, 3.0, 4.0, 5.0", buf.iterator().mkString() );

            buf.append( 6 );
            assertEquals( "3.0, 4.0, 5.0, 6.0", buf.iterator().mkString() );

            buf.append( 7 );
            assertEquals( "4.0, 5.0, 6.0, 7.0", buf.iterator().mkString() );
        }
    }


    @Nested
    public class GetRHSByOffsetTestCases {
        @Test
        public void givenAnEmptyBuffer_insertARandomSampleOfValues() {
            // todo inject random samples
            double[] samples = new double[] {10L, Double.MAX_VALUE, Double.MIN_VALUE, 0, -1, 100, 2001, 42,-100};
            SeqDouble buf = new ArrayRingBufferDouble( 4 );

            Iterator<double[]> it = DoubleIterable.wrap( samples ).iterator().slice( 4 );
            while ( it.hasNext() ) {
                double[] values = it.next();
                if ( values.length != 4 ) {
                    break;
                }

                buf.appendAll( values );

                assertEquals( values[3], buf.getRHS() );
                assertEquals( values[3], buf.getRHSByOffset( 0 ) );
                assertEquals( values[2], buf.getRHSByOffset( 1 ) );
                assertEquals( values[1], buf.getRHSByOffset( 2 ) );
                assertEquals( values[0], buf.getRHSByOffset( 3 ) );
            }
        }
    }


    @Nested
    public class UpdateRHSTestCases {
        @Test
        public void updateAnEmptyBuffer_expectException() {
            SeqDouble buf = new ArrayRingBufferDouble( 4 );

            try {
                buf.updateRHS( 3 );

                fail( "expected IndexOutOfBoundsException" );
            } catch ( IndexOutOfBoundsException ex ) {
                assertEquals( "index 0 is out of bounds, currently supported indexes are 0 to 0 (inc)", ex.getMessage() );
            }
        }

        @Test
        public void givenASingleValueBuffer_expectUpdates() {
            SeqDouble buf = new ArrayRingBufferDouble( 4 );

            buf.append( 10 );

            buf.updateRHS( 3 );
            buf.updateRHS( 2 );
            buf.updateRHS( 1 );

            assertEquals( 4, buf.size() );
            assertEquals( 3, buf.remaining() );
            assertEquals( 1, buf.get(0) );
        }

        @Test
        public void givenATwoValueBuffer_expectUpdates() {
            SeqDouble buf = new ArrayRingBufferDouble( 4 );

            buf.append( 10 );
            buf.append( 9 );

            buf.updateRHS( 3 );
            buf.updateRHS( 2 );
            buf.updateRHS( 1 );

            assertEquals( 4, buf.size() );
            assertEquals( 2, buf.remaining() );
            assertEquals( 10, buf.get(0) );
            assertEquals( 1, buf.get(1) );
        }

        @Test
        public void givenFullValueBuffer_expectUpdates() {
            SeqDouble buf = new ArrayRingBufferDouble( 4 );

            buf.append( 10 );
            buf.append( 9 );
            buf.append( 8 );
            buf.append( 7 );

            buf.updateRHS( 3 );
            buf.updateRHS( 2 );
            buf.updateRHS( 1 );

            assertEquals( 4, buf.size() );
            assertEquals( 0, buf.remaining() );
            assertEquals( 10, buf.get(0) );
            assertEquals( 9, buf.get(1) );
            assertEquals( 8, buf.get(2) );
            assertEquals( 1, buf.get(3) );
        }

        @Test
        public void givenOverflowedBuffer_expectUpdates() {
            SeqDouble buf = new ArrayRingBufferDouble( 4 );

            buf.append( 10 );
            buf.append( 9 );
            buf.append( 8 );
            buf.append( 7 );
            buf.append( 6 );
            buf.append( 5 );

            buf.updateRHS( 3 );
            buf.updateRHS( 2 );
            buf.updateRHS( 1 );

            assertEquals( 4, buf.size() );
            assertEquals( 0, buf.remaining() );
            assertEquals( 8, buf.get(2) );
            assertEquals( 7, buf.get(3) );
            assertEquals( 6, buf.get(4) );
            assertEquals( 1, buf.get(5) );
        }
    }


    @Nested
    public class MapTestCases {
        @Test
        public void testMap() {
            SeqDouble origBuf = new ArrayRingBufferDouble( 4 );
            SeqDouble mappedBuf = origBuf.map( i -> i * 10 );


            assertEquals( "", origBuf.iterator().mkString() );
            assertEquals( "", mappedBuf.iterator().mkString() );

            origBuf.append( 1 );
            assertEquals( "1.0", origBuf.iterator().mkString() );
            assertEquals( "10.0", mappedBuf.iterator().mkString() );

            origBuf.append( 2 );
            assertEquals( "1.0, 2.0", origBuf.iterator().mkString() );
            assertEquals( "10.0, 20.0", mappedBuf.iterator().mkString() );

            origBuf.append( 3 );
            assertEquals( "1.0, 2.0, 3.0", origBuf.iterator().mkString() );
            assertEquals( "10.0, 20.0, 30.0", mappedBuf.iterator().mkString() );


            origBuf.append( 4 );
            assertEquals( "1.0, 2.0, 3.0, 4.0", origBuf.iterator().mkString() );
            assertEquals( "10.0, 20.0, 30.0, 40.0", mappedBuf.iterator().mkString() );

            origBuf.append( 5 );
            assertEquals( "2.0, 3.0, 4.0, 5.0", origBuf.iterator().mkString() );
            assertEquals( "20.0, 30.0, 40.0, 50.0", mappedBuf.iterator().mkString() );

            origBuf.append( 6 );
            assertEquals( "3.0, 4.0, 5.0, 6.0", origBuf.iterator().mkString() );
            assertEquals( "30.0, 40.0, 50.0, 60.0", mappedBuf.iterator().mkString() );

            origBuf.append( 7 );
            assertEquals( "4.0, 5.0, 6.0, 7.0", origBuf.iterator().mkString() );
            assertEquals( "40.0, 50.0, 60.0, 70.0", mappedBuf.iterator().mkString() );
        }

        @Test
        public void testClonedMap() {
            SeqDouble origBuf   = new ArrayRingBufferDouble( 4 );
            SeqDouble mappedBuf = origBuf.map( i -> i * 10 );

            SeqDouble copiedBuf0 = mappedBuf.copy();
            assertEquals( "", origBuf.iterator().mkString() );
            assertEquals( "", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );

            origBuf.append( 1 );
            SeqDouble copiedBuf1 = mappedBuf.copy();

            assertEquals( "1.0", origBuf.iterator().mkString() );
            assertEquals( "10.0", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "10.0", copiedBuf1.iterator().mkString() );

            origBuf.append( 2 );
            SeqDouble copiedBuf2 = mappedBuf.copy();

            assertEquals( "1.0, 2.0", origBuf.iterator().mkString() );
            assertEquals( "10.0, 20.0", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "10.0", copiedBuf1.iterator().mkString() );
            assertEquals( "10.0, 20.0", copiedBuf2.iterator().mkString() );

            origBuf.append( 3 );
            SeqDouble copiedBuf3 = mappedBuf.copy();

            assertEquals( "1.0, 2.0, 3.0", origBuf.iterator().mkString() );
            assertEquals( "10.0, 20.0, 30.0", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "10.0", copiedBuf1.iterator().mkString() );
            assertEquals( "10.0, 20.0", copiedBuf2.iterator().mkString() );
            assertEquals( "10.0, 20.0, 30.0", copiedBuf3.iterator().mkString() );


            origBuf.append( 4 );
            SeqDouble copiedBuf4 = mappedBuf.copy();

            assertEquals( "1.0, 2.0, 3.0, 4.0", origBuf.iterator().mkString() );
            assertEquals( "10.0, 20.0, 30.0, 40.0", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "10.0", copiedBuf1.iterator().mkString() );
            assertEquals( "10.0, 20.0", copiedBuf2.iterator().mkString() );
            assertEquals( "10.0, 20.0, 30.0", copiedBuf3.iterator().mkString() );
            assertEquals( "10.0, 20.0, 30.0, 40.0", copiedBuf4.iterator().mkString() );

            origBuf.append( 5 );
            SeqDouble copiedBuf5 = mappedBuf.copy();

            assertEquals( "2.0, 3.0, 4.0, 5.0", origBuf.iterator().mkString() );
            assertEquals( "20.0, 30.0, 40.0, 50.0", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "10.0", copiedBuf1.iterator().mkString() );
            assertEquals( "10.0, 20.0", copiedBuf2.iterator().mkString() );
            assertEquals( "10.0, 20.0, 30.0", copiedBuf3.iterator().mkString() );
            assertEquals( "10.0, 20.0, 30.0, 40.0", copiedBuf4.iterator().mkString() );
            assertEquals( "20.0, 30.0, 40.0, 50.0", copiedBuf5.iterator().mkString() );

            origBuf.append( 6 );
            SeqDouble copiedBuf6 = mappedBuf.copy();
            assertEquals( "3.0, 4.0, 5.0, 6.0", origBuf.iterator().mkString() );
            assertEquals( "30.0, 40.0, 50.0, 60.0", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "10.0", copiedBuf1.iterator().mkString() );
            assertEquals( "10.0, 20.0", copiedBuf2.iterator().mkString() );
            assertEquals( "10.0, 20.0, 30.0", copiedBuf3.iterator().mkString() );
            assertEquals( "10.0, 20.0, 30.0, 40.0", copiedBuf4.iterator().mkString() );
            assertEquals( "20.0, 30.0, 40.0, 50.0", copiedBuf5.iterator().mkString() );
            assertEquals( "30.0, 40.0, 50.0, 60.0", copiedBuf6.iterator().mkString() );

            origBuf.append( 7 );
            SeqDouble copiedBuf7 = mappedBuf.copy();

            assertEquals( "4.0, 5.0, 6.0, 7.0", origBuf.iterator().mkString() );
            assertEquals( "40.0, 50.0, 60.0, 70.0", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "10.0", copiedBuf1.iterator().mkString() );
            assertEquals( "10.0, 20.0", copiedBuf2.iterator().mkString() );
            assertEquals( "10.0, 20.0, 30.0", copiedBuf3.iterator().mkString() );
            assertEquals( "10.0, 20.0, 30.0, 40.0", copiedBuf4.iterator().mkString() );
            assertEquals( "20.0, 30.0, 40.0, 50.0", copiedBuf5.iterator().mkString() );
            assertEquals( "30.0, 40.0, 50.0, 60.0", copiedBuf6.iterator().mkString() );
            assertEquals( "40.0, 50.0, 60.0, 70.0", copiedBuf7.iterator().mkString() );
        }

        @Test
        public void showThatAMappedBufferCanNotBeModified() {
            SeqDouble origBuf = new ArrayRingBufferDouble( 4 );
            SeqDouble mappedBuf = origBuf.map( i -> i * 10 );

            try {
                mappedBuf.append( 0 );
                fail( "expected UnsupportedOperationException" );
            } catch ( UnsupportedOperationException ex ) {
                assertEquals( "This ring buffer has been set to read only", ex.getMessage() );
            }
        }

        @Test
        public void showThatACopyOfAMappedBufferCanBeModified() {
            SeqDouble origBuf = new ArrayRingBufferDouble( 4 );
            SeqDouble mappedBuf = origBuf.map( i -> i * 10 );

            origBuf.appendAll( 1, 2, 3, 4 );

            SeqDouble copiedBuf = mappedBuf.copy();

            copiedBuf.append( 5 );
            assertEquals( "20.0, 30.0, 40.0, 5.0", copiedBuf.mkString() );
        }

        @Test
        public void testGetByIndex() {
            SeqDouble origBuf = new ArrayRingBufferDouble( 4 );
            SeqDouble mappedBuf = origBuf.map( i -> i * 10 );


            assertEquals( 0, mappedBuf.rhsIndexExc() );
            assertEquals( 0, mappedBuf.rhsIndexInc() );

            origBuf.append( 1 );

            assertEquals( 0, mappedBuf.rhsIndexInc() );
            assertEquals( 1, mappedBuf.rhsIndexExc() );
            assertEquals( 10, mappedBuf.get(0) );

            origBuf.append( 2 );

            assertEquals( 1, mappedBuf.rhsIndexInc() );
            assertEquals( 2, mappedBuf.rhsIndexExc() );
            assertEquals( 10, mappedBuf.get(0) );
            assertEquals( 20, mappedBuf.get(1) );

            origBuf.append( 3 );

            assertEquals( 2, mappedBuf.rhsIndexInc() );
            assertEquals( 3, mappedBuf.rhsIndexExc() );
            assertEquals( 10, mappedBuf.get(0) );
            assertEquals( 20, mappedBuf.get(1) );
            assertEquals( 30, mappedBuf.get(2) );

            origBuf.append( 4 );

            assertEquals( 3, mappedBuf.rhsIndexInc() );
            assertEquals( 4, mappedBuf.rhsIndexExc() );
            assertEquals( 10, mappedBuf.get(0) );
            assertEquals( 20, mappedBuf.get(1) );
            assertEquals( 30, mappedBuf.get(2) );
            assertEquals( 40, mappedBuf.get(3) );

            origBuf.append( 5 );

            assertEquals( 4, mappedBuf.rhsIndexInc() );
            assertEquals( 5, mappedBuf.rhsIndexExc() );
            assertEquals( 20, mappedBuf.get(1) );
            assertEquals( 30, mappedBuf.get(2) );
            assertEquals( 40, mappedBuf.get(3) );
            assertEquals( 50, mappedBuf.get(4) );
        }

        @Test
        public void callSet_expectException() {
            SeqDouble origBuf = new ArrayRingBufferDouble( 4 );
            SeqDouble mappedBuf = origBuf.map( i -> i * 10 );

            origBuf.append( 5 );

            try {
                mappedBuf.set( 0, 10 );
                fail( "expected UnsupportedOperationException" );
            } catch ( UnsupportedOperationException ex ) {
                assertEquals( "This ring buffer has been set to read only", ex.getMessage() );
            }
        }
    }


    @Nested
    public class MapWithIndexTestCases {
        @Test
        public void testMap() {
            SeqDouble origBuf = new ArrayRingBufferDouble( 4 );
            SeqDouble mappedBuf = origBuf.mapWithIndex( (i,v) -> v * 10 + i/10.0 );


            assertEquals( "", origBuf.iterator().mkString() );
            assertEquals( "", mappedBuf.iterator().mkString() );

            origBuf.append( 1 );
            assertEquals( "1.0", origBuf.iterator().mkString() );
            assertEquals( "10.0", mappedBuf.iterator().mkString() );

            origBuf.append( 2 );
            assertEquals( "1.0, 2.0", origBuf.iterator().mkString() );
            assertEquals( "10.0, 20.1", mappedBuf.iterator().mkString() );

            origBuf.append( 3 );
            assertEquals( "1.0, 2.0, 3.0", origBuf.iterator().mkString() );
            assertEquals( "10.0, 20.1, 30.2", mappedBuf.iterator().mkString() );


            origBuf.append( 4 );
            assertEquals( "1.0, 2.0, 3.0, 4.0", origBuf.iterator().mkString() );
            assertEquals( "10.0, 20.1, 30.2, 40.3", mappedBuf.iterator().mkString() );

            origBuf.append( 5 );
            assertEquals( "2.0, 3.0, 4.0, 5.0", origBuf.iterator().mkString() );
            assertEquals( "20.1, 30.2, 40.3, 50.4", mappedBuf.iterator().mkString() );

            origBuf.append( 6 );
            assertEquals( "3.0, 4.0, 5.0, 6.0", origBuf.iterator().mkString() );
            assertEquals( "30.2, 40.3, 50.4, 60.5", mappedBuf.iterator().mkString() );

            origBuf.append( 7 );
            assertEquals( "4.0, 5.0, 6.0, 7.0", origBuf.iterator().mkString() );
            assertEquals( "40.3, 50.4, 60.5, 70.6", mappedBuf.iterator().mkString() );
        }

        @Test
        public void testClonedMap() {
            SeqDouble origBuf   = new ArrayRingBufferDouble( 4 );
            SeqDouble mappedBuf = origBuf.mapWithIndex( (i,v) -> v * 10 + i/10.0);

            SeqDouble copiedBuf0 = mappedBuf.copy();
            assertEquals( "", origBuf.iterator().mkString() );
            assertEquals( "", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );

            origBuf.append( 1 );
            SeqDouble copiedBuf1 = mappedBuf.copy();

            assertEquals( "1.0", origBuf.iterator().mkString() );
            assertEquals( "10.0", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "10.0", copiedBuf1.iterator().mkString() );

            origBuf.append( 2 );
            SeqDouble copiedBuf2 = mappedBuf.copy();

            assertEquals( "1.0, 2.0", origBuf.iterator().mkString() );
            assertEquals( "10.0, 20.1", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "10.0", copiedBuf1.iterator().mkString() );
            assertEquals( "10.0, 20.1", copiedBuf2.iterator().mkString() );

            origBuf.append( 3 );
            SeqDouble copiedBuf3 = mappedBuf.copy();

            assertEquals( "1.0, 2.0, 3.0", origBuf.iterator().mkString() );
            assertEquals( "10.0, 20.1, 30.2", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "10.0", copiedBuf1.iterator().mkString() );
            assertEquals( "10.0, 20.1", copiedBuf2.iterator().mkString() );
            assertEquals( "10.0, 20.1, 30.2", copiedBuf3.iterator().mkString() );


            origBuf.append( 4 );
            SeqDouble copiedBuf4 = mappedBuf.copy();

            assertEquals( "1.0, 2.0, 3.0, 4.0", origBuf.iterator().mkString() );
            assertEquals( "10.0, 20.1, 30.2, 40.3", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "10.0", copiedBuf1.iterator().mkString() );
            assertEquals( "10.0, 20.1", copiedBuf2.iterator().mkString() );
            assertEquals( "10.0, 20.1, 30.2", copiedBuf3.iterator().mkString() );
            assertEquals( "10.0, 20.1, 30.2, 40.3", copiedBuf4.iterator().mkString() );

            origBuf.append( 5 );
            SeqDouble copiedBuf5 = mappedBuf.copy();

            assertEquals( "2.0, 3.0, 4.0, 5.0", origBuf.iterator().mkString() );
            assertEquals( "20.1, 30.2, 40.3, 50.4", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "10.0", copiedBuf1.iterator().mkString() );
            assertEquals( "10.0, 20.1", copiedBuf2.iterator().mkString() );
            assertEquals( "10.0, 20.1, 30.2", copiedBuf3.iterator().mkString() );
            assertEquals( "10.0, 20.1, 30.2, 40.3", copiedBuf4.iterator().mkString() );
            assertEquals( "20.1, 30.2, 40.3, 50.4", copiedBuf5.iterator().mkString() );

            origBuf.append( 6 );
            SeqDouble copiedBuf6 = mappedBuf.copy();
            assertEquals( "3.0, 4.0, 5.0, 6.0", origBuf.iterator().mkString() );
            assertEquals( "30.2, 40.3, 50.4, 60.5", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "10.0", copiedBuf1.iterator().mkString() );
            assertEquals( "10.0, 20.1", copiedBuf2.iterator().mkString() );
            assertEquals( "10.0, 20.1, 30.2", copiedBuf3.iterator().mkString() );
            assertEquals( "10.0, 20.1, 30.2, 40.3", copiedBuf4.iterator().mkString() );
            assertEquals( "20.1, 30.2, 40.3, 50.4", copiedBuf5.iterator().mkString() );
            assertEquals( "30.2, 40.3, 50.4, 60.5", copiedBuf6.iterator().mkString() );

            origBuf.append( 7 );
            SeqDouble copiedBuf7 = mappedBuf.copy();

            assertEquals( "4.0, 5.0, 6.0, 7.0", origBuf.iterator().mkString() );
            assertEquals( "40.3, 50.4, 60.5, 70.6", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "10.0", copiedBuf1.iterator().mkString() );
            assertEquals( "10.0, 20.1", copiedBuf2.iterator().mkString() );
            assertEquals( "10.0, 20.1, 30.2", copiedBuf3.iterator().mkString() );
            assertEquals( "10.0, 20.1, 30.2, 40.3", copiedBuf4.iterator().mkString() );
            assertEquals( "20.1, 30.2, 40.3, 50.4", copiedBuf5.iterator().mkString() );
            assertEquals( "30.2, 40.3, 50.4, 60.5", copiedBuf6.iterator().mkString() );
            assertEquals( "40.3, 50.4, 60.5, 70.6", copiedBuf7.iterator().mkString() );
        }

        @Test
        public void showThatAMappedBufferCanNotBeModified() {
            SeqDouble origBuf   = new ArrayRingBufferDouble( 4 );
            SeqDouble mappedBuf = origBuf.mapWithIndex( (i,v) -> v*10 + i/10.0);

            try {
                mappedBuf.append( 0 );
                fail( "expected UnsupportedOperationException" );
            } catch ( UnsupportedOperationException ex ) {
                assertEquals( "This ring buffer has been set to read only", ex.getMessage() );
            }
        }

        @Test
        public void showThatACopyOfAMappedBufferCanBeModified() {
            SeqDouble origBuf   = new ArrayRingBufferDouble( 4 );
            SeqDouble mappedBuf = origBuf.mapWithIndex( (i,v) -> v*10 + i/10.0);

            origBuf.appendAll( 1, 2, 3, 4 );

            SeqDouble copiedBuf = mappedBuf.copy();

            copiedBuf.append( 5 );
            assertEquals( "20.1, 30.2, 40.3, 5.0", copiedBuf.mkString() );
        }

        @Test
        public void testGetByIndex() {
            SeqDouble origBuf   = new ArrayRingBufferDouble( 4 );
            SeqDouble mappedBuf = origBuf.mapWithIndex( (i,v) -> v*10 + i/10.0);


            assertEquals( 0, mappedBuf.rhsIndexExc() );
            assertEquals( 0, mappedBuf.rhsIndexInc() );

            origBuf.append( 1 );

            assertEquals( 0, mappedBuf.rhsIndexInc() );
            assertEquals( 1, mappedBuf.rhsIndexExc() );
            assertEquals( 10.0, mappedBuf.get(0) );

            origBuf.append( 2 );

            assertEquals( 1, mappedBuf.rhsIndexInc() );
            assertEquals( 2, mappedBuf.rhsIndexExc() );
            assertEquals( 10, mappedBuf.get(0) );
            assertEquals( 20.1, mappedBuf.get(1) );

            origBuf.append( 3 );

            assertEquals( 2, mappedBuf.rhsIndexInc() );
            assertEquals( 3, mappedBuf.rhsIndexExc() );
            assertEquals( 10, mappedBuf.get(0) );
            assertEquals( 20.1, mappedBuf.get(1) );
            assertEquals( 30.2, mappedBuf.get(2) );

            origBuf.append( 4 );

            assertEquals( 3, mappedBuf.rhsIndexInc() );
            assertEquals( 4, mappedBuf.rhsIndexExc() );
            assertEquals( 10, mappedBuf.get(0) );
            assertEquals( 20.1, mappedBuf.get(1) );
            assertEquals( 30.2, mappedBuf.get(2) );
            assertEquals( 40.3, mappedBuf.get(3) );

            origBuf.append( 5 );

            assertEquals( 4, mappedBuf.rhsIndexInc() );
            assertEquals( 5, mappedBuf.rhsIndexExc() );
            assertEquals( 20.1, mappedBuf.get(1) );
            assertEquals( 30.2, mappedBuf.get(2) );
            assertEquals( 40.3, mappedBuf.get(3) );
            assertEquals( 50.4, mappedBuf.get(4) );
        }

        @Test
        public void callSet_expectException() {
            SeqDouble origBuf   = new ArrayRingBufferDouble( 4 );
            SeqDouble mappedBuf = origBuf.mapWithIndex( (i,v) -> v*10 + i/10.0);

            origBuf.append( 5 );

            try {
                mappedBuf.set( 0, 10 );
                fail( "expected UnsupportedOperationException" );
            } catch ( UnsupportedOperationException ex ) {
                assertEquals( "This ring buffer has been set to read only", ex.getMessage() );
            }
        }
    }


    @Nested
    public class AddTestCases {
        @Test
        public void testAdd() {
            SeqDouble origBuf1 = new ArrayRingBufferDouble( 4 );
            SeqDouble origBuf2 = new ArrayRingBufferDouble( 4 );
            SeqDouble mappedBuf = origBuf1.add( origBuf2 );


            assertEquals( "", origBuf1.iterator().mkString() );
            assertEquals( "", mappedBuf.iterator().mkString() );

            origBuf1.append( 1 );
            origBuf2.append( 10 );
            assertEquals( "1.0", origBuf1.iterator().mkString() );
            assertEquals( "10.0", origBuf2.iterator().mkString() );
            assertEquals( "11.0", mappedBuf.iterator().mkString() );

            origBuf1.append( 2 );
            origBuf2.append( 20 );
            assertEquals( "1.0, 2.0", origBuf1.iterator().mkString() );
            assertEquals( "10.0, 20.0", origBuf2.iterator().mkString() );
            assertEquals( "11.0, 22.0", mappedBuf.iterator().mkString() );

            origBuf1.append( 3 );
            origBuf2.append( 30 );
            assertEquals( "1.0, 2.0, 3.0", origBuf1.iterator().mkString() );
            assertEquals( "10.0, 20.0, 30.0", origBuf2.iterator().mkString() );
            assertEquals( "11.0, 22.0, 33.0", mappedBuf.iterator().mkString() );


            origBuf1.append( 4 );
            origBuf2.append( 40 );
            assertEquals( "1.0, 2.0, 3.0, 4.0", origBuf1.iterator().mkString() );
            assertEquals( "10.0, 20.0, 30.0, 40.0", origBuf2.iterator().mkString() );
            assertEquals( "11.0, 22.0, 33.0, 44.0", mappedBuf.iterator().mkString() );

            origBuf1.append( 5 );
            origBuf2.append( 50 );
            assertEquals( "2.0, 3.0, 4.0, 5.0", origBuf1.iterator().mkString() );
            assertEquals( "20.0, 30.0, 40.0, 50.0", origBuf2.iterator().mkString() );
            assertEquals( "22.0, 33.0, 44.0, 55.0", mappedBuf.iterator().mkString() );

            origBuf1.append( 6 );
            origBuf2.append( 60 );
            assertEquals( "3.0, 4.0, 5.0, 6.0", origBuf1.iterator().mkString() );
            assertEquals( "30.0, 40.0, 50.0, 60.0", origBuf2.iterator().mkString() );
            assertEquals( "33.0, 44.0, 55.0, 66.0", mappedBuf.iterator().mkString() );

            origBuf1.append( 7 );
            origBuf2.append( 70 );
            assertEquals( "4.0, 5.0, 6.0, 7.0", origBuf1.iterator().mkString() );
            assertEquals( "40.0, 50.0, 60.0, 70.0", origBuf2.iterator().mkString() );
            assertEquals( "44.0, 55.0, 66.0, 77.0", mappedBuf.iterator().mkString() );
        }

        @Test
        public void testClonedAdd() {
            SeqDouble origBuf1 = new ArrayRingBufferDouble( 4 );
            SeqDouble origBuf2 = new ArrayRingBufferDouble( 4 );
            SeqDouble mappedBuf = origBuf1.add( origBuf2 );

            SeqDouble copiedBuf0 = mappedBuf.copy();
            assertEquals( "", origBuf1.iterator().mkString() );
            assertEquals( "", origBuf2.iterator().mkString() );
            assertEquals( "", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );

            origBuf1.append( 1 );
            origBuf2.append( 10 );
            SeqDouble copiedBuf1 = mappedBuf.copy();

            assertEquals( "1.0", origBuf1.iterator().mkString() );
            assertEquals( "10.0", origBuf2.iterator().mkString() );
            assertEquals( "11.0", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "11.0", copiedBuf1.iterator().mkString() );

            origBuf1.append( 2 );
            origBuf2.append( 20 );
            SeqDouble copiedBuf2 = mappedBuf.copy();

            assertEquals( "1.0, 2.0", origBuf1.iterator().mkString() );
            assertEquals( "10.0, 20.0", origBuf2.iterator().mkString() );
            assertEquals( "11.0, 22.0", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "11.0", copiedBuf1.iterator().mkString() );
            assertEquals( "11.0, 22.0", copiedBuf2.iterator().mkString() );

            origBuf1.append( 3 );
            origBuf2.append( 30 );
            SeqDouble copiedBuf3 = mappedBuf.copy();

            assertEquals( "1.0, 2.0, 3.0", origBuf1.iterator().mkString() );
            assertEquals( "10.0, 20.0, 30.0", origBuf2.iterator().mkString() );
            assertEquals( "11.0, 22.0, 33.0", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "11.0", copiedBuf1.iterator().mkString() );
            assertEquals( "11.0, 22.0", copiedBuf2.iterator().mkString() );
            assertEquals( "11.0, 22.0, 33.0", copiedBuf3.iterator().mkString() );


            origBuf1.append( 4 );
            origBuf2.append( 40 );
            SeqDouble copiedBuf4 = mappedBuf.copy();

            assertEquals( "1.0, 2.0, 3.0, 4.0", origBuf1.iterator().mkString() );
            assertEquals( "10.0, 20.0, 30.0, 40.0", origBuf2.iterator().mkString() );
            assertEquals( "11.0, 22.0, 33.0, 44.0", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "11.0", copiedBuf1.iterator().mkString() );
            assertEquals( "11.0, 22.0", copiedBuf2.iterator().mkString() );
            assertEquals( "11.0, 22.0, 33.0", copiedBuf3.iterator().mkString() );
            assertEquals( "11.0, 22.0, 33.0, 44.0", copiedBuf4.iterator().mkString() );

            origBuf1.append( 5 );
            origBuf2.append( 50 );
            SeqDouble copiedBuf5 = mappedBuf.copy();

            assertEquals( "2.0, 3.0, 4.0, 5.0", origBuf1.iterator().mkString() );
            assertEquals( "20.0, 30.0, 40.0, 50.0", origBuf2.iterator().mkString() );
            assertEquals( "22.0, 33.0, 44.0, 55.0", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "11.0", copiedBuf1.iterator().mkString() );
            assertEquals( "11.0, 22.0", copiedBuf2.iterator().mkString() );
            assertEquals( "11.0, 22.0, 33.0", copiedBuf3.iterator().mkString() );
            assertEquals( "11.0, 22.0, 33.0, 44.0", copiedBuf4.iterator().mkString() );
            assertEquals( "22.0, 33.0, 44.0, 55.0", copiedBuf5.iterator().mkString() );

            origBuf1.append( 6 );
            origBuf2.append( 60 );
            SeqDouble copiedBuf6 = mappedBuf.copy();
            assertEquals( "3.0, 4.0, 5.0, 6.0", origBuf1.iterator().mkString() );
            assertEquals( "30.0, 40.0, 50.0, 60.0", origBuf2.iterator().mkString() );
            assertEquals( "33.0, 44.0, 55.0, 66.0", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "11.0", copiedBuf1.iterator().mkString() );
            assertEquals( "11.0, 22.0", copiedBuf2.iterator().mkString() );
            assertEquals( "11.0, 22.0, 33.0", copiedBuf3.iterator().mkString() );
            assertEquals( "11.0, 22.0, 33.0, 44.0", copiedBuf4.iterator().mkString() );
            assertEquals( "22.0, 33.0, 44.0, 55.0", copiedBuf5.iterator().mkString() );
            assertEquals( "33.0, 44.0, 55.0, 66.0", copiedBuf6.iterator().mkString() );

            origBuf1.append( 7 );
            origBuf2.append( 70 );
            SeqDouble copiedBuf7 = mappedBuf.copy();

            assertEquals( "4.0, 5.0, 6.0, 7.0", origBuf1.iterator().mkString() );
            assertEquals( "40.0, 50.0, 60.0, 70.0", origBuf2.iterator().mkString() );
            assertEquals( "44.0, 55.0, 66.0, 77.0", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "11.0", copiedBuf1.iterator().mkString() );
            assertEquals( "11.0, 22.0", copiedBuf2.iterator().mkString() );
            assertEquals( "11.0, 22.0, 33.0", copiedBuf3.iterator().mkString() );
            assertEquals( "11.0, 22.0, 33.0, 44.0", copiedBuf4.iterator().mkString() );
            assertEquals( "22.0, 33.0, 44.0, 55.0", copiedBuf5.iterator().mkString() );
            assertEquals( "33.0, 44.0, 55.0, 66.0", copiedBuf6.iterator().mkString() );
            assertEquals( "44.0, 55.0, 66.0, 77.0", copiedBuf7.iterator().mkString() );
        }

        @Test
        public void showThatAAddpedBufferCanNotBeModified() {
            SeqDouble origBuf1 = new ArrayRingBufferDouble( 4 );
            SeqDouble origBuf2 = new ArrayRingBufferDouble( 4 );
            SeqDouble mappedBuf = origBuf1.add( origBuf2 );

            try {
                mappedBuf.append( 0 );
                fail( "expected UnsupportedOperationException" );
            } catch ( UnsupportedOperationException ex ) {
                assertEquals( "This ring buffer has been set to read only", ex.getMessage() );
            }
        }

        @Test
        public void showThatACopyOfAAddpedBufferCanBeModified() {
            SeqDouble origBuf1 = new ArrayRingBufferDouble( 4 );
            SeqDouble origBuf2 = new ArrayRingBufferDouble( 4 );
            SeqDouble mappedBuf = origBuf1.add( origBuf2 );

            origBuf1.appendAll( 1, 2, 3, 4 );
            origBuf2.appendAll( 10, 20, 30, 40 );

            SeqDouble copiedBuf = mappedBuf.copy();

            copiedBuf.append( 5 );
            assertEquals( "22.0, 33.0, 44.0, 5.0", copiedBuf.mkString() );
        }

        @Test
        public void callGetByIndex() {
            SeqDouble origBuf1 = new ArrayRingBufferDouble( 4 );
            SeqDouble origBuf2 = new ArrayRingBufferDouble( 4 );
            SeqDouble mappedBuf = origBuf1.add( origBuf2 );

            assertEquals( 0, mappedBuf.rhsIndexInc() );
            assertEquals( 0, mappedBuf.rhsIndexExc() );
            assertEquals( 4, mappedBuf.size() );

            origBuf1.append( 1 );
            origBuf2.append( 10 );

            assertEquals( 11, mappedBuf.get(0) );
            assertEquals( 0, mappedBuf.rhsIndexInc() );
            assertEquals( 1, mappedBuf.rhsIndexExc() );
            assertEquals( 4, mappedBuf.size() );


            origBuf1.append( 2 );
            origBuf2.append( 11 );

            assertEquals( 11, mappedBuf.get(0) );
            assertEquals( 13, mappedBuf.get(1) );

            assertEquals( 1, mappedBuf.rhsIndexInc() );
            assertEquals( 2, mappedBuf.rhsIndexExc() );
            assertEquals( 4, mappedBuf.size() );
        }
    }


    @Nested
    public class SubTestCases {
        @Test
        public void testSub() {
            SeqDouble origBuf1 = new ArrayRingBufferDouble( 4 );
            SeqDouble origBuf2 = new ArrayRingBufferDouble( 4 );
            SeqDouble mappedBuf = origBuf1.sub( origBuf2 );


            assertEquals( "", origBuf1.iterator().mkString() );
            assertEquals( "", mappedBuf.iterator().mkString() );

            origBuf1.append( 1 );
            origBuf2.append( 10 );
            assertEquals( "1.0", origBuf1.iterator().mkString() );
            assertEquals( "10.0", origBuf2.iterator().mkString() );
            assertEquals( "-9.0", mappedBuf.iterator().mkString() );

            origBuf1.append( 2 );
            origBuf2.append( 20 );
            assertEquals( "1.0, 2.0", origBuf1.iterator().mkString() );
            assertEquals( "10.0, 20.0", origBuf2.iterator().mkString() );
            assertEquals( "-9.0, -18.0", mappedBuf.iterator().mkString() );

            origBuf1.append( 3 );
            origBuf2.append( 30 );
            assertEquals( "1.0, 2.0, 3.0", origBuf1.iterator().mkString() );
            assertEquals( "10.0, 20.0, 30.0", origBuf2.iterator().mkString() );
            assertEquals( "-9.0, -18.0, -27.0", mappedBuf.iterator().mkString() );


            origBuf1.append( 4 );
            origBuf2.append( 40 );
            assertEquals( "1.0, 2.0, 3.0, 4.0", origBuf1.iterator().mkString() );
            assertEquals( "10.0, 20.0, 30.0, 40.0", origBuf2.iterator().mkString() );
            assertEquals( "-9.0, -18.0, -27.0, -36.0", mappedBuf.iterator().mkString() );

            origBuf1.append( 5 );
            origBuf2.append( 50 );
            assertEquals( "2.0, 3.0, 4.0, 5.0", origBuf1.iterator().mkString() );
            assertEquals( "20.0, 30.0, 40.0, 50.0", origBuf2.iterator().mkString() );
            assertEquals( "-18.0, -27.0, -36.0, -45.0", mappedBuf.iterator().mkString() );

            origBuf1.append( 6 );
            origBuf2.append( 60 );
            assertEquals( "3.0, 4.0, 5.0, 6.0", origBuf1.iterator().mkString() );
            assertEquals( "30.0, 40.0, 50.0, 60.0", origBuf2.iterator().mkString() );
            assertEquals( "-27.0, -36.0, -45.0, -54.0", mappedBuf.iterator().mkString() );

            origBuf1.append( 7 );
            origBuf2.append( 70 );
            assertEquals( "4.0, 5.0, 6.0, 7.0", origBuf1.iterator().mkString() );
            assertEquals( "40.0, 50.0, 60.0, 70.0", origBuf2.iterator().mkString() );
            assertEquals( "-36.0, -45.0, -54.0, -63.0", mappedBuf.iterator().mkString() );
        }

        @Test
        public void testClonedSub() {
            SeqDouble origBuf1 = new ArrayRingBufferDouble( 4 );
            SeqDouble origBuf2 = new ArrayRingBufferDouble( 4 );
            SeqDouble mappedBuf = origBuf1.sub( origBuf2 );

            SeqDouble copiedBuf0 = mappedBuf.copy();
            assertEquals( "", origBuf1.iterator().mkString() );
            assertEquals( "", origBuf2.iterator().mkString() );
            assertEquals( "", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );

            origBuf1.append( 1 );
            origBuf2.append( 10 );
            SeqDouble copiedBuf1 = mappedBuf.copy();

            assertEquals( "1.0", origBuf1.iterator().mkString() );
            assertEquals( "10.0", origBuf2.iterator().mkString() );
            assertEquals( "-9.0", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "-9.0", copiedBuf1.iterator().mkString() );

            origBuf1.append( 2 );
            origBuf2.append( 20 );
            SeqDouble copiedBuf2 = mappedBuf.copy();

            assertEquals( "1.0, 2.0", origBuf1.iterator().mkString() );
            assertEquals( "10.0, 20.0", origBuf2.iterator().mkString() );
            assertEquals( "-9.0, -18.0", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "-9.0", copiedBuf1.iterator().mkString() );
            assertEquals( "-9.0, -18.0", copiedBuf2.iterator().mkString() );

            origBuf1.append( 3 );
            origBuf2.append( 30 );
            SeqDouble copiedBuf3 = mappedBuf.copy();

            assertEquals( "1.0, 2.0, 3.0", origBuf1.iterator().mkString() );
            assertEquals( "10.0, 20.0, 30.0", origBuf2.iterator().mkString() );
            assertEquals( "-9.0, -18.0, -27.0", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "-9.0", copiedBuf1.iterator().mkString() );
            assertEquals( "-9.0, -18.0", copiedBuf2.iterator().mkString() );
            assertEquals( "-9.0, -18.0, -27.0", copiedBuf3.iterator().mkString() );


            origBuf1.append( 4 );
            origBuf2.append( 40 );
            SeqDouble copiedBuf4 = mappedBuf.copy();

            assertEquals( "1.0, 2.0, 3.0, 4.0", origBuf1.iterator().mkString() );
            assertEquals( "10.0, 20.0, 30.0, 40.0", origBuf2.iterator().mkString() );
            assertEquals( "-9.0, -18.0, -27.0, -36.0", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "-9.0", copiedBuf1.iterator().mkString() );
            assertEquals( "-9.0, -18.0", copiedBuf2.iterator().mkString() );
            assertEquals( "-9.0, -18.0, -27.0", copiedBuf3.iterator().mkString() );
            assertEquals( "-9.0, -18.0, -27.0, -36.0", copiedBuf4.iterator().mkString() );

            origBuf1.append( 5 );
            origBuf2.append( 50 );
            SeqDouble copiedBuf5 = mappedBuf.copy();

            assertEquals( "2.0, 3.0, 4.0, 5.0", origBuf1.iterator().mkString() );
            assertEquals( "20.0, 30.0, 40.0, 50.0", origBuf2.iterator().mkString() );
            assertEquals( "-18.0, -27.0, -36.0, -45.0", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "-9.0", copiedBuf1.iterator().mkString() );
            assertEquals( "-9.0, -18.0", copiedBuf2.iterator().mkString() );
            assertEquals( "-9.0, -18.0, -27.0", copiedBuf3.iterator().mkString() );
            assertEquals( "-9.0, -18.0, -27.0, -36.0", copiedBuf4.iterator().mkString() );
            assertEquals( "-18.0, -27.0, -36.0, -45.0", copiedBuf5.iterator().mkString() );

            origBuf1.append( 6 );
            origBuf2.append( 60 );
            SeqDouble copiedBuf6 = mappedBuf.copy();
            assertEquals( "3.0, 4.0, 5.0, 6.0", origBuf1.iterator().mkString() );
            assertEquals( "30.0, 40.0, 50.0, 60.0", origBuf2.iterator().mkString() );
            assertEquals( "-27.0, -36.0, -45.0, -54.0", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "-9.0", copiedBuf1.iterator().mkString() );
            assertEquals( "-9.0, -18.0", copiedBuf2.iterator().mkString() );
            assertEquals( "-9.0, -18.0, -27.0", copiedBuf3.iterator().mkString() );
            assertEquals( "-9.0, -18.0, -27.0, -36.0", copiedBuf4.iterator().mkString() );
            assertEquals( "-18.0, -27.0, -36.0, -45.0", copiedBuf5.iterator().mkString() );
            assertEquals( "-27.0, -36.0, -45.0, -54.0", copiedBuf6.iterator().mkString() );

            origBuf1.append( 7 );
            origBuf2.append( 70 );
            SeqDouble copiedBuf7 = mappedBuf.copy();

            assertEquals( "4.0, 5.0, 6.0, 7.0", origBuf1.iterator().mkString() );
            assertEquals( "40.0, 50.0, 60.0, 70.0", origBuf2.iterator().mkString() );
            assertEquals( "-36.0, -45.0, -54.0, -63.0", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "-9.0", copiedBuf1.iterator().mkString() );
            assertEquals( "-9.0, -18.0", copiedBuf2.iterator().mkString() );
            assertEquals( "-9.0, -18.0, -27.0", copiedBuf3.iterator().mkString() );
            assertEquals( "-9.0, -18.0, -27.0, -36.0", copiedBuf4.iterator().mkString() );
            assertEquals( "-18.0, -27.0, -36.0, -45.0", copiedBuf5.iterator().mkString() );
            assertEquals( "-27.0, -36.0, -45.0, -54.0", copiedBuf6.iterator().mkString() );
            assertEquals( "-36.0, -45.0, -54.0, -63.0", copiedBuf7.iterator().mkString() );
        }

        @Test
        public void showThatASubpedBufferCanNotBeModified() {
            SeqDouble origBuf1 = new ArrayRingBufferDouble( 4 );
            SeqDouble origBuf2 = new ArrayRingBufferDouble( 4 );
            SeqDouble mappedBuf = origBuf1.sub( origBuf2 );

            try {
                mappedBuf.append( 0 );
                fail( "expected UnsupportedOperationException" );
            } catch ( UnsupportedOperationException ex ) {
                assertEquals( "This ring buffer has been set to read only", ex.getMessage() );
            }
        }

        @Test
        public void showThatACopyOfASubedBufferCanBeModified() {
            SeqDouble origBuf1 = new ArrayRingBufferDouble( 4 );
            SeqDouble origBuf2 = new ArrayRingBufferDouble( 4 );
            SeqDouble mappedBuf = origBuf1.sub( origBuf2 );

            origBuf1.appendAll( 1, 2, 3, 4 );
            origBuf2.appendAll( 10, 20, 30, 40 );

            SeqDouble copiedBuf = mappedBuf.copy();

            copiedBuf.append( 5 );
            assertEquals( "-18.0, -27.0, -36.0, 5.0", copiedBuf.mkString() );
        }
    }


    @Nested
    public class ShiftLeftTestCases {
        @Test
        public void testShiftleft() {
            SeqDouble origBuf1 = new ArrayRingBufferDouble( 4 );
            SeqDouble mappedBuf = origBuf1.shiftLeft( 1 );


            assertEquals( "", origBuf1.iterator().mkString() );
            assertEquals( "", mappedBuf.iterator().mkString() );

            origBuf1.append( 1 );

            assertEquals( 0, mappedBuf.contentsCount() );
            assertEquals( 3, mappedBuf.remaining() );

            assertEquals( "1.0", origBuf1.iterator().mkString() );
            assertEquals( "", mappedBuf.iterator().mkString() );

            origBuf1.append( 2 );

            assertEquals( 1, mappedBuf.contentsCount() );
            assertEquals( 2, mappedBuf.remaining() );
            assertEquals( 2, mappedBuf.get( 0 ) );

            assertEquals( "1.0, 2.0", origBuf1.iterator().mkString() );
            assertEquals( "2.0", mappedBuf.iterator().mkString() );

            origBuf1.append( 3 );
            assertEquals( "1.0, 2.0, 3.0", origBuf1.iterator().mkString() );
            assertEquals( "2.0, 3.0", mappedBuf.iterator().mkString() );


            origBuf1.append( 4 );
            assertEquals( "1.0, 2.0, 3.0, 4.0", origBuf1.iterator().mkString() );
            assertEquals( "2.0, 3.0, 4.0", mappedBuf.iterator().mkString() );

            origBuf1.append( 5 );
            assertEquals( "2.0, 3.0, 4.0, 5.0", origBuf1.iterator().mkString() );
            assertEquals( "3.0, 4.0, 5.0", mappedBuf.iterator().mkString() );

            origBuf1.append( 6 );
            assertEquals( "3.0, 4.0, 5.0, 6.0", origBuf1.iterator().mkString() );
            assertEquals( "4.0, 5.0, 6.0", mappedBuf.iterator().mkString() );

            origBuf1.append( 7 );
            assertEquals( "4.0, 5.0, 6.0, 7.0", origBuf1.iterator().mkString() );
            assertEquals( "5.0, 6.0, 7.0", mappedBuf.iterator().mkString() );
        }

        @Test
        public void testClonedShiftleft() {
            SeqDouble origBuf1 = new ArrayRingBufferDouble( 4 );
            SeqDouble mappedBuf = origBuf1.shiftLeft( 1 );

            SeqDouble copiedBuf0 = mappedBuf.copy();
            assertEquals( "", origBuf1.iterator().mkString() );
            assertEquals( "", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );

            origBuf1.append( 1 );
            SeqDouble copiedBuf1 = mappedBuf.copy();

            assertEquals( 3, mappedBuf.size() );
            assertEquals( 3, copiedBuf0.size() );

            assertEquals( "1.0", origBuf1.iterator().mkString() );
            assertEquals( "", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "", copiedBuf1.iterator().mkString() );

            origBuf1.append( 2 );
            SeqDouble copiedBuf2 = mappedBuf.copy();

            assertEquals( "1.0, 2.0", origBuf1.iterator().mkString() );
            assertEquals( "2.0", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "", copiedBuf1.iterator().mkString() );
            assertEquals( "2.0", copiedBuf2.iterator().mkString() );

            origBuf1.append( 3 );
            SeqDouble copiedBuf3 = mappedBuf.copy();

            assertEquals( "1.0, 2.0, 3.0", origBuf1.iterator().mkString() );
            assertEquals( "2.0, 3.0", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "", copiedBuf1.iterator().mkString() );
            assertEquals( "2.0", copiedBuf2.iterator().mkString() );
            assertEquals( "2.0, 3.0", copiedBuf3.iterator().mkString() );


            origBuf1.append( 4 );
            SeqDouble copiedBuf4 = mappedBuf.copy();

            assertEquals( "1.0, 2.0, 3.0, 4.0", origBuf1.iterator().mkString() );
            assertEquals( "2.0, 3.0, 4.0", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "", copiedBuf1.iterator().mkString() );
            assertEquals( "2.0", copiedBuf2.iterator().mkString() );
            assertEquals( "2.0, 3.0", copiedBuf3.iterator().mkString() );
            assertEquals( "2.0, 3.0, 4.0", copiedBuf4.iterator().mkString() );

            origBuf1.append( 5 );
            SeqDouble copiedBuf5 = mappedBuf.copy();

            assertEquals( "2.0, 3.0, 4.0, 5.0", origBuf1.iterator().mkString() );
            assertEquals( "3.0, 4.0, 5.0", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "", copiedBuf1.iterator().mkString() );
            assertEquals( "2.0", copiedBuf2.iterator().mkString() );
            assertEquals( "2.0, 3.0", copiedBuf3.iterator().mkString() );
            assertEquals( "2.0, 3.0, 4.0", copiedBuf4.iterator().mkString() );
            assertEquals( "3.0, 4.0, 5.0", copiedBuf5.iterator().mkString() );

            origBuf1.append( 6 );
            SeqDouble copiedBuf6 = mappedBuf.copy();
            assertEquals( "3.0, 4.0, 5.0, 6.0", origBuf1.iterator().mkString() );
            assertEquals( "4.0, 5.0, 6.0", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "", copiedBuf1.iterator().mkString() );
            assertEquals( "2.0", copiedBuf2.iterator().mkString() );
            assertEquals( "2.0, 3.0", copiedBuf3.iterator().mkString() );
            assertEquals( "2.0, 3.0, 4.0", copiedBuf4.iterator().mkString() );
            assertEquals( "3.0, 4.0, 5.0", copiedBuf5.iterator().mkString() );
            assertEquals( "4.0, 5.0, 6.0", copiedBuf6.iterator().mkString() );

            origBuf1.append( 7 );
            SeqDouble copiedBuf7 = mappedBuf.copy();

            assertEquals( "4.0, 5.0, 6.0, 7.0", origBuf1.iterator().mkString() );
            assertEquals( "5.0, 6.0, 7.0", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "", copiedBuf1.iterator().mkString() );
            assertEquals( "2.0", copiedBuf2.iterator().mkString() );
            assertEquals( "2.0, 3.0", copiedBuf3.iterator().mkString() );
            assertEquals( "2.0, 3.0, 4.0", copiedBuf4.iterator().mkString() );
            assertEquals( "3.0, 4.0, 5.0", copiedBuf5.iterator().mkString() );
            assertEquals( "4.0, 5.0, 6.0", copiedBuf6.iterator().mkString() );
            assertEquals( "5.0, 6.0, 7.0", copiedBuf7.iterator().mkString() );
        }

        @Test
        public void showThatAShiftleftedBufferCanNotBeModified() {
            SeqDouble origBuf1 = new ArrayRingBufferDouble( 4 );
            SeqDouble mappedBuf = origBuf1.shiftLeft( 1 );

            try {
                mappedBuf.append( 0 );
                fail( "expected UnsupportedOperationException" );
            } catch ( UnsupportedOperationException ex ) {
                assertEquals( "This ring buffer has been set to read only", ex.getMessage() );
            }
        }

        @Test
        public void showThatACopyOfAShiftleftedBufferCanBeModified() {
            SeqDouble origBuf1 = new ArrayRingBufferDouble( 4 );
            SeqDouble mappedBuf = origBuf1.shiftLeft( 1 );

            origBuf1.appendAll( 1, 2, 3, 4 );

            SeqDouble copiedBuf = mappedBuf.copy();

            copiedBuf.append( 5 );
            assertEquals( "3.0, 4.0, 5.0", copiedBuf.mkString() );
        }
    }


    @Nested
    public class ShiftRightTestCases {
        @Test
        public void testShiftRight() {
            SeqDouble origBuf = new ArrayRingBufferDouble( 4 );
            SeqDouble mappedBuf = origBuf.shiftRight( 1 );

            assertEquals( 4, mappedBuf.size() );
            assertEquals( 4, mappedBuf.remaining() );
            assertEquals( 0, mappedBuf.contentsCount() );

            assertEquals( "", origBuf.iterator().mkString() );
            assertEquals( "", mappedBuf.iterator().mkString() );

            origBuf.append( 1 );

            assertEquals( 0, mappedBuf.get( 0 ) );
            assertEquals( 1, mappedBuf.get( 1 ) );
            assertEquals( 4, mappedBuf.size() );
            assertEquals( 2, mappedBuf.remaining() );
            assertEquals( 2, mappedBuf.contentsCount() );

            assertEquals( "1.0", origBuf.iterator().mkString() );
            assertEquals( "0.0, 1.0", mappedBuf.iterator().mkString() );

            origBuf.append( 2 );
            assertEquals( "1.0, 2.0", origBuf.iterator().mkString() );
            assertEquals( "0.0, 1.0, 2.0", mappedBuf.iterator().mkString() );

            origBuf.append( 3 );
            assertEquals( "1.0, 2.0, 3.0", origBuf.iterator().mkString() );
            assertEquals( "0.0, 1.0, 2.0, 3.0", mappedBuf.iterator().mkString() );


            origBuf.append( 4 );
            assertEquals( "1.0, 2.0, 3.0, 4.0", origBuf.iterator().mkString() );
            assertEquals( "1.0, 2.0, 3.0, 4.0", mappedBuf.iterator().mkString() );

            origBuf.append( 5 );
            assertEquals( "2.0, 3.0, 4.0, 5.0", origBuf.iterator().mkString() );
            assertEquals( "2.0, 3.0, 4.0, 5.0", mappedBuf.iterator().mkString() );

            origBuf.append( 6 );
            assertEquals( "3.0, 4.0, 5.0, 6.0", origBuf.iterator().mkString() );
            assertEquals( "3.0, 4.0, 5.0, 6.0", mappedBuf.iterator().mkString() );

            origBuf.append( 7 );
            assertEquals( "4.0, 5.0, 6.0, 7.0", origBuf.iterator().mkString() );
            assertEquals( "4.0, 5.0, 6.0, 7.0", mappedBuf.iterator().mkString() );

            assertEquals( 4, origBuf.get( 3 ) );
            assertEquals( 5, origBuf.get( 4 ) );
            assertEquals( 6, origBuf.get( 5 ) );
            assertEquals( 7, origBuf.get( 6 ) );

            assertEquals( 4, mappedBuf.get( 4 ) );
            assertEquals( 5, mappedBuf.get( 5 ) );
            assertEquals( 6, mappedBuf.get( 6 ) );
            assertEquals( 7, mappedBuf.get( 7 ) );

            assertEquals( 4, mappedBuf.size() );
            assertEquals( 0, mappedBuf.remaining() );
            assertEquals( 4, mappedBuf.contentsCount() );
        }

        @Test
        public void testClonedShiftRight() {
            SeqDouble origBuf1 = new ArrayRingBufferDouble( 4 );
            SeqDouble mappedBuf = origBuf1.shiftRight( 1 );

            SeqDouble copiedBuf0 = mappedBuf.copy();
            assertEquals( "", origBuf1.iterator().mkString() );
            assertEquals( "", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );

            origBuf1.append( 1 );
            SeqDouble copiedBuf1 = mappedBuf.copy();

            assertEquals( 4, mappedBuf.size() );
            assertEquals( 4, copiedBuf0.size() );

            assertEquals( "1.0", origBuf1.iterator().mkString() );
            assertEquals( "0.0, 1.0", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "0.0, 1.0", copiedBuf1.iterator().mkString() );

            origBuf1.append( 2 );
            SeqDouble copiedBuf2 = mappedBuf.copy();

            assertEquals( "1.0, 2.0", origBuf1.iterator().mkString() );
            assertEquals( "0.0, 1.0, 2.0", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "0.0, 1.0", copiedBuf1.iterator().mkString() );
            assertEquals( "0.0, 1.0, 2.0", copiedBuf2.iterator().mkString() );

            origBuf1.append( 3 );
            SeqDouble copiedBuf3 = mappedBuf.copy();

            assertEquals( "1.0, 2.0, 3.0", origBuf1.iterator().mkString() );
            assertEquals( "0.0, 1.0, 2.0, 3.0", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "0.0, 1.0", copiedBuf1.iterator().mkString() );
            assertEquals( "0.0, 1.0, 2.0", copiedBuf2.iterator().mkString() );
            assertEquals( "0.0, 1.0, 2.0, 3.0", copiedBuf3.iterator().mkString() );


            origBuf1.append( 4 );
            SeqDouble copiedBuf4 = mappedBuf.copy();

            assertEquals( "1.0, 2.0, 3.0, 4.0", origBuf1.iterator().mkString() );
            assertEquals( "1.0, 2.0, 3.0, 4.0", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "0.0, 1.0", copiedBuf1.iterator().mkString() );
            assertEquals( "0.0, 1.0, 2.0", copiedBuf2.iterator().mkString() );
            assertEquals( "0.0, 1.0, 2.0, 3.0", copiedBuf3.iterator().mkString() );
            assertEquals( "1.0, 2.0, 3.0, 4.0", copiedBuf4.iterator().mkString() );

            origBuf1.append( 5 );
            SeqDouble copiedBuf5 = mappedBuf.copy();

            assertEquals( "2.0, 3.0, 4.0, 5.0", origBuf1.iterator().mkString() );
            assertEquals( "2.0, 3.0, 4.0, 5.0", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "0.0, 1.0", copiedBuf1.iterator().mkString() );
            assertEquals( "0.0, 1.0, 2.0", copiedBuf2.iterator().mkString() );
            assertEquals( "0.0, 1.0, 2.0, 3.0", copiedBuf3.iterator().mkString() );
            assertEquals( "1.0, 2.0, 3.0, 4.0", copiedBuf4.iterator().mkString() );
            assertEquals( "2.0, 3.0, 4.0, 5.0", copiedBuf5.iterator().mkString() );

            origBuf1.append( 6 );
            SeqDouble copiedBuf6 = mappedBuf.copy();
            assertEquals( "3.0, 4.0, 5.0, 6.0", origBuf1.iterator().mkString() );
            assertEquals( "3.0, 4.0, 5.0, 6.0", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "0.0, 1.0", copiedBuf1.iterator().mkString() );
            assertEquals( "0.0, 1.0, 2.0", copiedBuf2.iterator().mkString() );
            assertEquals( "0.0, 1.0, 2.0, 3.0", copiedBuf3.iterator().mkString() );
            assertEquals( "1.0, 2.0, 3.0, 4.0", copiedBuf4.iterator().mkString() );
            assertEquals( "2.0, 3.0, 4.0, 5.0", copiedBuf5.iterator().mkString() );
            assertEquals( "3.0, 4.0, 5.0, 6.0", copiedBuf6.iterator().mkString() );

            origBuf1.append( 7 );
            SeqDouble copiedBuf7 = mappedBuf.copy();

            assertEquals( "4.0, 5.0, 6.0, 7.0", origBuf1.iterator().mkString() );
            assertEquals( "4.0, 5.0, 6.0, 7.0", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "0.0, 1.0", copiedBuf1.iterator().mkString() );
            assertEquals( "0.0, 1.0, 2.0", copiedBuf2.iterator().mkString() );
            assertEquals( "0.0, 1.0, 2.0, 3.0", copiedBuf3.iterator().mkString() );
            assertEquals( "1.0, 2.0, 3.0, 4.0", copiedBuf4.iterator().mkString() );
            assertEquals( "2.0, 3.0, 4.0, 5.0", copiedBuf5.iterator().mkString() );
            assertEquals( "3.0, 4.0, 5.0, 6.0", copiedBuf6.iterator().mkString() );
            assertEquals( "4.0, 5.0, 6.0, 7.0", copiedBuf7.iterator().mkString() );
        }

        @Test
        public void showThatAShiftrightedBufferCanNotBeModified() {
            SeqDouble origBuf1 = new ArrayRingBufferDouble( 4 );
            SeqDouble mappedBuf = origBuf1.shiftRight( 1 );

            try {
                mappedBuf.append( 0 );
                fail( "expected UnsupportedOperationException" );
            } catch ( UnsupportedOperationException ex ) {
                assertEquals( "This ring buffer has been set to read only", ex.getMessage() );
            }
        }

        @Test
        public void showThatACopyOfAShiftrightedBufferCanBeModified() {
            SeqDouble origBuf1 = new ArrayRingBufferDouble( 4 );
            SeqDouble mappedBuf = origBuf1.shiftRight( 1 );

            origBuf1.appendAll( 1, 2, 3, 4 );

            SeqDouble copiedBuf = mappedBuf.copy();

            copiedBuf.append( 5 );
            assertEquals( "2.0, 3.0, 4.0, 5.0", copiedBuf.mkString() );
        }
    }


    @Nested
    public class MultIntTestCases {
        @Test
        public void testMultint() {
            SeqDouble origBuf = new ArrayRingBufferDouble( 4 );
            SeqDouble mappedBuf = origBuf.mult( 2 );


            assertEquals( "", origBuf.iterator().mkString() );
            assertEquals( "", mappedBuf.iterator().mkString() );

            origBuf.append( 1 );
            assertEquals( "1.0", origBuf.iterator().mkString() );
            assertEquals( "2.0", mappedBuf.iterator().mkString() );

            origBuf.append( 2 );
            assertEquals( "1.0, 2.0", origBuf.iterator().mkString() );
            assertEquals( "2.0, 4.0", mappedBuf.iterator().mkString() );

            origBuf.append( 3 );
            assertEquals( "1.0, 2.0, 3.0", origBuf.iterator().mkString() );
            assertEquals( "2.0, 4.0, 6.0", mappedBuf.iterator().mkString() );


            origBuf.append( 4 );
            assertEquals( "1.0, 2.0, 3.0, 4.0", origBuf.iterator().mkString() );
            assertEquals( "2.0, 4.0, 6.0, 8.0", mappedBuf.iterator().mkString() );

            origBuf.append( 5 );
            assertEquals( "2.0, 3.0, 4.0, 5.0", origBuf.iterator().mkString() );
            assertEquals( "4.0, 6.0, 8.0, 10.0", mappedBuf.iterator().mkString() );

            origBuf.append( 6 );
            assertEquals( "3.0, 4.0, 5.0, 6.0", origBuf.iterator().mkString() );
            assertEquals( "6.0, 8.0, 10.0, 12.0", mappedBuf.iterator().mkString() );

            origBuf.append( 7 );
            assertEquals( "4.0, 5.0, 6.0, 7.0", origBuf.iterator().mkString() );
            assertEquals( "8.0, 10.0, 12.0, 14.0", mappedBuf.iterator().mkString() );
        }

        @Test
        public void testClonedMultint() {
            SeqDouble origBuf = new ArrayRingBufferDouble( 4 );
            SeqDouble mappedBuf = origBuf.mult( 2 );

            SeqDouble copiedBuf0 = mappedBuf.copy();
            assertEquals( "", origBuf.iterator().mkString() );
            assertEquals( "", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );

            origBuf.append( 1 );
            SeqDouble copiedBuf1 = mappedBuf.copy();

            assertEquals( "1.0", origBuf.iterator().mkString() );
            assertEquals( "2.0", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "2.0", copiedBuf1.iterator().mkString() );

            origBuf.append( 2 );
            SeqDouble copiedBuf2 = mappedBuf.copy();

            assertEquals( "1.0, 2.0", origBuf.iterator().mkString() );
            assertEquals( "2.0, 4.0", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "2.0", copiedBuf1.iterator().mkString() );
            assertEquals( "2.0, 4.0", copiedBuf2.iterator().mkString() );

            origBuf.append( 3 );
            SeqDouble copiedBuf3 = mappedBuf.copy();

            assertEquals( "1.0, 2.0, 3.0", origBuf.iterator().mkString() );
            assertEquals( "2.0, 4.0, 6.0", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "2.0", copiedBuf1.iterator().mkString() );
            assertEquals( "2.0, 4.0", copiedBuf2.iterator().mkString() );
            assertEquals( "2.0, 4.0, 6.0", copiedBuf3.iterator().mkString() );


            origBuf.append( 4 );
            SeqDouble copiedBuf4 = mappedBuf.copy();

            assertEquals( "1.0, 2.0, 3.0, 4.0", origBuf.iterator().mkString() );
            assertEquals( "2.0, 4.0, 6.0, 8.0", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "2.0", copiedBuf1.iterator().mkString() );
            assertEquals( "2.0, 4.0", copiedBuf2.iterator().mkString() );
            assertEquals( "2.0, 4.0, 6.0", copiedBuf3.iterator().mkString() );
            assertEquals( "2.0, 4.0, 6.0, 8.0", copiedBuf4.iterator().mkString() );

            origBuf.append( 5 );
            SeqDouble copiedBuf5 = mappedBuf.copy();

            assertEquals( "2.0, 3.0, 4.0, 5.0", origBuf.iterator().mkString() );
            assertEquals( "4.0, 6.0, 8.0, 10.0", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "2.0", copiedBuf1.iterator().mkString() );
            assertEquals( "2.0, 4.0", copiedBuf2.iterator().mkString() );
            assertEquals( "2.0, 4.0, 6.0", copiedBuf3.iterator().mkString() );
            assertEquals( "2.0, 4.0, 6.0, 8.0", copiedBuf4.iterator().mkString() );
            assertEquals( "4.0, 6.0, 8.0, 10.0", copiedBuf5.iterator().mkString() );

            origBuf.append( 6 );
            SeqDouble copiedBuf6 = mappedBuf.copy();
            assertEquals( "3.0, 4.0, 5.0, 6.0", origBuf.iterator().mkString() );
            assertEquals( "6.0, 8.0, 10.0, 12.0", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "2.0", copiedBuf1.iterator().mkString() );
            assertEquals( "2.0, 4.0", copiedBuf2.iterator().mkString() );
            assertEquals( "2.0, 4.0, 6.0", copiedBuf3.iterator().mkString() );
            assertEquals( "2.0, 4.0, 6.0, 8.0", copiedBuf4.iterator().mkString() );
            assertEquals( "4.0, 6.0, 8.0, 10.0", copiedBuf5.iterator().mkString() );
            assertEquals( "6.0, 8.0, 10.0, 12.0", copiedBuf6.iterator().mkString() );

            origBuf.append( 7 );
            SeqDouble copiedBuf7 = mappedBuf.copy();

            assertEquals( "4.0, 5.0, 6.0, 7.0", origBuf.iterator().mkString() );
            assertEquals( "8.0, 10.0, 12.0, 14.0", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "2.0", copiedBuf1.iterator().mkString() );
            assertEquals( "2.0, 4.0", copiedBuf2.iterator().mkString() );
            assertEquals( "2.0, 4.0, 6.0", copiedBuf3.iterator().mkString() );
            assertEquals( "2.0, 4.0, 6.0, 8.0", copiedBuf4.iterator().mkString() );
            assertEquals( "4.0, 6.0, 8.0, 10.0", copiedBuf5.iterator().mkString() );
            assertEquals( "6.0, 8.0, 10.0, 12.0", copiedBuf6.iterator().mkString() );
            assertEquals( "8.0, 10.0, 12.0, 14.0", copiedBuf7.iterator().mkString() );
        }

        @Test
        public void showThatAMultintpedBufferCanNotBeModified() {
            SeqDouble origBuf = new ArrayRingBufferDouble( 4 );
            SeqDouble mappedBuf = origBuf.mult( 2 );

            try {
                mappedBuf.append( 0 );
                fail( "expected UnsupportedOperationException" );
            } catch ( UnsupportedOperationException ex ) {
                assertEquals( "This ring buffer has been set to read only", ex.getMessage() );
            }
        }

        @Test
        public void showThatACopyOfAMultintpedBufferCanBeModified() {
            SeqDouble origBuf = new ArrayRingBufferDouble( 4 );
            SeqDouble mappedBuf = origBuf.mult( 2 );

            origBuf.appendAll( 1, 2, 3, 4 );

            SeqDouble copiedBuf = mappedBuf.copy();

            copiedBuf.append( 5 );
            assertEquals( "4.0, 6.0, 8.0, 5.0", copiedBuf.mkString() );
        }
    }


    @Nested
    public class MultDoubleTestCases {
        @Test
        public void testMultDouble() {
            SeqDouble origBuf = new ArrayRingBufferDouble( 4 );
            SeqDouble mappedBuf = origBuf.mult( 2.5 );


            assertEquals( "", origBuf.iterator().mkString() );
            assertEquals( "", mappedBuf.iterator().mkString() );

            origBuf.append( 1 );
            assertEquals( "1.0", origBuf.iterator().mkString() );
            assertEquals( "2.5", mappedBuf.iterator().mkString() );

            origBuf.append( 2 );
            assertEquals( "1.0, 2.0", origBuf.iterator().mkString() );
            assertEquals( "2.5, 5.0", mappedBuf.iterator().mkString() );

            origBuf.append( 3 );
            assertEquals( "1.0, 2.0, 3.0", origBuf.iterator().mkString() );
            assertEquals( "2.5, 5.0, 7.5", mappedBuf.iterator().mkString() );


            origBuf.append( 4 );
            assertEquals( "1.0, 2.0, 3.0, 4.0", origBuf.iterator().mkString() );
            assertEquals( "2.5, 5.0, 7.5, 10.0", mappedBuf.iterator().mkString() );

            origBuf.append( 5 );
            assertEquals( "2.0, 3.0, 4.0, 5.0", origBuf.iterator().mkString() );
            assertEquals( "5.0, 7.5, 10.0, 12.5", mappedBuf.iterator().mkString() );

            origBuf.append( 6 );
            assertEquals( "3.0, 4.0, 5.0, 6.0", origBuf.iterator().mkString() );
            assertEquals( "7.5, 10.0, 12.5, 15.0", mappedBuf.iterator().mkString() );

            origBuf.append( 7 );
            assertEquals( "4.0, 5.0, 6.0, 7.0", origBuf.iterator().mkString() );
            assertEquals( "10.0, 12.5, 15.0, 17.5", mappedBuf.iterator().mkString() );
        }

        @Test
        public void testClonedMultdouble() {
            SeqDouble origBuf = new ArrayRingBufferDouble( 4 );
            SeqDouble mappedBuf = origBuf.mult( 2.5 );

            SeqDouble copiedBuf0 = mappedBuf.copy();
            assertEquals( "", origBuf.iterator().mkString() );
            assertEquals( "", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );

            origBuf.append( 1 );
            SeqDouble copiedBuf1 = mappedBuf.copy();

            assertEquals( "1.0", origBuf.iterator().mkString() );
            assertEquals( "2.5", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "2.5", copiedBuf1.iterator().mkString() );

            origBuf.append( 2 );
            SeqDouble copiedBuf2 = mappedBuf.copy();

            assertEquals( "1.0, 2.0", origBuf.iterator().mkString() );
            assertEquals( "2.5, 5.0", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "2.5", copiedBuf1.iterator().mkString() );
            assertEquals( "2.5, 5.0", copiedBuf2.iterator().mkString() );

            origBuf.append( 3 );
            SeqDouble copiedBuf3 = mappedBuf.copy();

            assertEquals( "1.0, 2.0, 3.0", origBuf.iterator().mkString() );
            assertEquals( "2.5, 5.0, 7.5", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "2.5", copiedBuf1.iterator().mkString() );
            assertEquals( "2.5, 5.0", copiedBuf2.iterator().mkString() );
            assertEquals( "2.5, 5.0, 7.5", copiedBuf3.iterator().mkString() );


            origBuf.append( 4 );
            SeqDouble copiedBuf4 = mappedBuf.copy();

            assertEquals( "1.0, 2.0, 3.0, 4.0", origBuf.iterator().mkString() );
            assertEquals( "2.5, 5.0, 7.5, 10.0", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "2.5", copiedBuf1.iterator().mkString() );
            assertEquals( "2.5, 5.0", copiedBuf2.iterator().mkString() );
            assertEquals( "2.5, 5.0, 7.5", copiedBuf3.iterator().mkString() );
            assertEquals( "2.5, 5.0, 7.5, 10.0", copiedBuf4.iterator().mkString() );

            origBuf.append( 5 );
            SeqDouble copiedBuf5 = mappedBuf.copy();

            assertEquals( "2.0, 3.0, 4.0, 5.0", origBuf.iterator().mkString() );
            assertEquals( "5.0, 7.5, 10.0, 12.5", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "2.5", copiedBuf1.iterator().mkString() );
            assertEquals( "2.5, 5.0", copiedBuf2.iterator().mkString() );
            assertEquals( "2.5, 5.0, 7.5", copiedBuf3.iterator().mkString() );
            assertEquals( "2.5, 5.0, 7.5, 10.0", copiedBuf4.iterator().mkString() );
            assertEquals( "5.0, 7.5, 10.0, 12.5", copiedBuf5.iterator().mkString() );

            origBuf.append( 6 );
            SeqDouble copiedBuf6 = mappedBuf.copy();
            assertEquals( "3.0, 4.0, 5.0, 6.0", origBuf.iterator().mkString() );
            assertEquals( "7.5, 10.0, 12.5, 15.0", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "2.5", copiedBuf1.iterator().mkString() );
            assertEquals( "2.5, 5.0", copiedBuf2.iterator().mkString() );
            assertEquals( "2.5, 5.0, 7.5", copiedBuf3.iterator().mkString() );
            assertEquals( "2.5, 5.0, 7.5, 10.0", copiedBuf4.iterator().mkString() );
            assertEquals( "5.0, 7.5, 10.0, 12.5", copiedBuf5.iterator().mkString() );
            assertEquals( "7.5, 10.0, 12.5, 15.0", copiedBuf6.iterator().mkString() );

            origBuf.append( 7 );
            SeqDouble copiedBuf7 = mappedBuf.copy();

            assertEquals( "4.0, 5.0, 6.0, 7.0", origBuf.iterator().mkString() );
            assertEquals( "10.0, 12.5, 15.0, 17.5", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "2.5", copiedBuf1.iterator().mkString() );
            assertEquals( "2.5, 5.0", copiedBuf2.iterator().mkString() );
            assertEquals( "2.5, 5.0, 7.5", copiedBuf3.iterator().mkString() );
            assertEquals( "2.5, 5.0, 7.5, 10.0", copiedBuf4.iterator().mkString() );
            assertEquals( "5.0, 7.5, 10.0, 12.5", copiedBuf5.iterator().mkString() );
            assertEquals( "7.5, 10.0, 12.5, 15.0", copiedBuf6.iterator().mkString() );
            assertEquals( "10.0, 12.5, 15.0, 17.5", copiedBuf7.iterator().mkString() );
        }

        @Test
        public void showThatAMultdoublepedBufferCanNotBeModified() {
            SeqDouble origBuf = new ArrayRingBufferDouble( 4 );
            SeqDouble mappedBuf = origBuf.mult( 2 );

            try {
                mappedBuf.append( 0 );
                fail( "expected UnsupportedOperationException" );
            } catch ( UnsupportedOperationException ex ) {
                assertEquals( "This ring buffer has been set to read only", ex.getMessage() );
            }
        }

        @Test
        public void showThatACopyOfAMultdoublepedBufferCanBeModified() {
            SeqDouble origBuf = new ArrayRingBufferDouble( 4 );
            SeqDouble mappedBuf = origBuf.mult( 2 );

            origBuf.appendAll( 1, 2, 3, 4 );

            SeqDouble copiedBuf = mappedBuf.copy();

            copiedBuf.append( 5 );
            assertEquals( "4.0, 6.0, 8.0, 5.0", copiedBuf.mkString() );
        }
    }


    @Nested
    public class DivDoubleTestCases {
        @Test
        public void testDivDouble() {
            SeqDouble origBuf = new ArrayRingBufferDouble( 4 );
            SeqDouble mappedBuf = origBuf.div( 0.1 );


            assertEquals( "", origBuf.iterator().mkString() );
            assertEquals( "", mappedBuf.iterator().mkString() );

            origBuf.append( 1 );
            assertEquals( "1.0", origBuf.iterator().mkString() );
            assertEquals( "10.0", mappedBuf.iterator().mkString() );

            origBuf.append( 2 );
            assertEquals( "1.0, 2.0", origBuf.iterator().mkString() );
            assertEquals( "10.0, 20.0", mappedBuf.iterator().mkString() );

            origBuf.append( 3 );
            assertEquals( "1.0, 2.0, 3.0", origBuf.iterator().mkString() );
            assertEquals( "10.0, 20.0, 30.0", mappedBuf.iterator().mkString() );


            origBuf.append( 4 );
            assertEquals( "1.0, 2.0, 3.0, 4.0", origBuf.iterator().mkString() );
            assertEquals( "10.0, 20.0, 30.0, 40.0", mappedBuf.iterator().mkString() );

            origBuf.append( 5 );
            assertEquals( "2.0, 3.0, 4.0, 5.0", origBuf.iterator().mkString() );
            assertEquals( "20.0, 30.0, 40.0, 50.0", mappedBuf.iterator().mkString() );

            origBuf.append( 6 );
            assertEquals( "3.0, 4.0, 5.0, 6.0", origBuf.iterator().mkString() );
            assertEquals( "30.0, 40.0, 50.0, 60.0", mappedBuf.iterator().mkString() );

            origBuf.append( 7 );
            assertEquals( "4.0, 5.0, 6.0, 7.0", origBuf.iterator().mkString() );
            assertEquals( "40.0, 50.0, 60.0, 70.0", mappedBuf.iterator().mkString() );
        }

        @Test
        public void testClonedDivDouble() {
            SeqDouble origBuf = new ArrayRingBufferDouble( 4 );
            SeqDouble mappedBuf = origBuf.div( 0.1 );

            SeqDouble copiedBuf0 = mappedBuf.copy();
            assertEquals( "", origBuf.iterator().mkString() );
            assertEquals( "", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );

            origBuf.append( 1 );
            SeqDouble copiedBuf1 = mappedBuf.copy();

            assertEquals( "1.0", origBuf.iterator().mkString() );
            assertEquals( "10.0", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "10.0", copiedBuf1.iterator().mkString() );

            origBuf.append( 2 );
            SeqDouble copiedBuf2 = mappedBuf.copy();

            assertEquals( "1.0, 2.0", origBuf.iterator().mkString() );
            assertEquals( "10.0, 20.0", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "10.0", copiedBuf1.iterator().mkString() );
            assertEquals( "10.0, 20.0", copiedBuf2.iterator().mkString() );

            origBuf.append( 3 );
            SeqDouble copiedBuf3 = mappedBuf.copy();

            assertEquals( "1.0, 2.0, 3.0", origBuf.iterator().mkString() );
            assertEquals( "10.0, 20.0, 30.0", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "10.0", copiedBuf1.iterator().mkString() );
            assertEquals( "10.0, 20.0", copiedBuf2.iterator().mkString() );
            assertEquals( "10.0, 20.0, 30.0", copiedBuf3.iterator().mkString() );


            origBuf.append( 4 );
            SeqDouble copiedBuf4 = mappedBuf.copy();

            assertEquals( "1.0, 2.0, 3.0, 4.0", origBuf.iterator().mkString() );
            assertEquals( "10.0, 20.0, 30.0, 40.0", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "10.0", copiedBuf1.iterator().mkString() );
            assertEquals( "10.0, 20.0", copiedBuf2.iterator().mkString() );
            assertEquals( "10.0, 20.0, 30.0", copiedBuf3.iterator().mkString() );
            assertEquals( "10.0, 20.0, 30.0, 40.0", copiedBuf4.iterator().mkString() );

            origBuf.append( 5 );
            SeqDouble copiedBuf5 = mappedBuf.copy();

            assertEquals( "2.0, 3.0, 4.0, 5.0", origBuf.iterator().mkString() );
            assertEquals( "20.0, 30.0, 40.0, 50.0", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "10.0", copiedBuf1.iterator().mkString() );
            assertEquals( "10.0, 20.0", copiedBuf2.iterator().mkString() );
            assertEquals( "10.0, 20.0, 30.0", copiedBuf3.iterator().mkString() );
            assertEquals( "10.0, 20.0, 30.0, 40.0", copiedBuf4.iterator().mkString() );
            assertEquals( "20.0, 30.0, 40.0, 50.0", copiedBuf5.iterator().mkString() );

            origBuf.append( 6 );
            SeqDouble copiedBuf6 = mappedBuf.copy();
            assertEquals( "3.0, 4.0, 5.0, 6.0", origBuf.iterator().mkString() );
            assertEquals( "30.0, 40.0, 50.0, 60.0", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "10.0", copiedBuf1.iterator().mkString() );
            assertEquals( "10.0, 20.0", copiedBuf2.iterator().mkString() );
            assertEquals( "10.0, 20.0, 30.0", copiedBuf3.iterator().mkString() );
            assertEquals( "10.0, 20.0, 30.0, 40.0", copiedBuf4.iterator().mkString() );
            assertEquals( "20.0, 30.0, 40.0, 50.0", copiedBuf5.iterator().mkString() );
            assertEquals( "30.0, 40.0, 50.0, 60.0", copiedBuf6.iterator().mkString() );

            origBuf.append( 7 );
            SeqDouble copiedBuf7 = mappedBuf.copy();

            assertEquals( "4.0, 5.0, 6.0, 7.0", origBuf.iterator().mkString() );
            assertEquals( "40.0, 50.0, 60.0, 70.0", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "10.0", copiedBuf1.iterator().mkString() );
            assertEquals( "10.0, 20.0", copiedBuf2.iterator().mkString() );
            assertEquals( "10.0, 20.0, 30.0", copiedBuf3.iterator().mkString() );
            assertEquals( "10.0, 20.0, 30.0, 40.0", copiedBuf4.iterator().mkString() );
            assertEquals( "20.0, 30.0, 40.0, 50.0", copiedBuf5.iterator().mkString() );
            assertEquals( "30.0, 40.0, 50.0, 60.0", copiedBuf6.iterator().mkString() );
            assertEquals( "40.0, 50.0, 60.0, 70.0", copiedBuf7.iterator().mkString() );
        }

        @Test
        public void showThatADivDoubleBufferCanNotBeModified() {
            SeqDouble origBuf = new ArrayRingBufferDouble( 4 );
            SeqDouble mappedBuf = origBuf.div( 0.1 );

            try {
                mappedBuf.append( 0 );
                fail( "expected UnsupportedOperationException" );
            } catch ( UnsupportedOperationException ex ) {
                assertEquals( "This ring buffer has been set to read only", ex.getMessage() );
            }
        }

        @Test
        public void showThatACopyOfADivDoubleBufferCanBeModified() {
            SeqDouble origBuf = new ArrayRingBufferDouble( 4 );
            SeqDouble mappedBuf = origBuf.div( 0.1 );

            origBuf.appendAll( 1, 2, 3, 4 );

            SeqDouble copiedBuf = mappedBuf.copy();

            copiedBuf.append( 5 );
            assertEquals( "20.0, 30.0, 40.0, 5.0", copiedBuf.mkString() );
        }
    }


    @Nested
    public class DivIntTestCases {
        @Test
        public void testDiv() {
            SeqDouble origBuf = new ArrayRingBufferDouble( 4 );
            SeqDouble mappedBuf = origBuf.div( 2 );


            assertEquals( "", origBuf.iterator().mkString() );
            assertEquals( "", mappedBuf.iterator().mkString() );

            origBuf.append( 1 );
            assertEquals( "1.0", origBuf.iterator().mkString() );
            assertEquals( "0.5", mappedBuf.iterator().mkString() );

            origBuf.append( 2 );
            assertEquals( "1.0, 2.0", origBuf.iterator().mkString() );
            assertEquals( "0.5, 1.0", mappedBuf.iterator().mkString() );

            origBuf.append( 3 );
            assertEquals( "1.0, 2.0, 3.0", origBuf.iterator().mkString() );
            assertEquals( "0.5, 1.0, 1.5", mappedBuf.iterator().mkString() );


            origBuf.append( 4 );
            assertEquals( "1.0, 2.0, 3.0, 4.0", origBuf.iterator().mkString() );
            assertEquals( "0.5, 1.0, 1.5, 2.0", mappedBuf.iterator().mkString() );

            origBuf.append( 5 );
            assertEquals( "2.0, 3.0, 4.0, 5.0", origBuf.iterator().mkString() );
            assertEquals( "1.0, 1.5, 2.0, 2.5", mappedBuf.iterator().mkString() );

            origBuf.append( 6 );
            assertEquals( "3.0, 4.0, 5.0, 6.0", origBuf.iterator().mkString() );
            assertEquals( "1.5, 2.0, 2.5, 3.0", mappedBuf.iterator().mkString() );

            origBuf.append( 22.2 );
            assertEquals( "4.0, 5.0, 6.0, 22.2", origBuf.iterator().mkString() );
            assertEquals( "2.0, 2.5, 3.0, 11.1", mappedBuf.iterator().mkString() );
        }

        @Test
        public void testClonedDiv() {
            SeqDouble origBuf = new ArrayRingBufferDouble( 4 );
            SeqDouble mappedBuf = origBuf.div( 2 );

            SeqDouble copiedBuf0 = mappedBuf.copy();
            assertEquals( "", origBuf.iterator().mkString() );
            assertEquals( "", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );

            origBuf.append( 1 );
            SeqDouble copiedBuf1 = mappedBuf.copy();

            assertEquals( "1.0", origBuf.iterator().mkString() );
            assertEquals( "0.5", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "0.5", copiedBuf1.iterator().mkString() );

            origBuf.append( 2 );
            SeqDouble copiedBuf2 = mappedBuf.copy();

            assertEquals( "1.0, 2.0", origBuf.iterator().mkString() );
            assertEquals( "0.5, 1.0", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "0.5", copiedBuf1.iterator().mkString() );
            assertEquals( "0.5, 1.0", copiedBuf2.iterator().mkString() );

            origBuf.append( 3 );
            SeqDouble copiedBuf3 = mappedBuf.copy();

            assertEquals( "1.0, 2.0, 3.0", origBuf.iterator().mkString() );
            assertEquals( "0.5, 1.0, 1.5", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "0.5", copiedBuf1.iterator().mkString() );
            assertEquals( "0.5, 1.0", copiedBuf2.iterator().mkString() );
            assertEquals( "0.5, 1.0, 1.5", copiedBuf3.iterator().mkString() );


            origBuf.append( 4 );
            SeqDouble copiedBuf4 = mappedBuf.copy();

            assertEquals( "1.0, 2.0, 3.0, 4.0", origBuf.iterator().mkString() );
            assertEquals( "0.5, 1.0, 1.5, 2.0", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "0.5", copiedBuf1.iterator().mkString() );
            assertEquals( "0.5, 1.0", copiedBuf2.iterator().mkString() );
            assertEquals( "0.5, 1.0, 1.5", copiedBuf3.iterator().mkString() );
            assertEquals( "0.5, 1.0, 1.5, 2.0", copiedBuf4.iterator().mkString() );

            origBuf.append( 5 );
            SeqDouble copiedBuf5 = mappedBuf.copy();

            assertEquals( "2.0, 3.0, 4.0, 5.0", origBuf.iterator().mkString() );
            assertEquals( "1.0, 1.5, 2.0, 2.5", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "0.5", copiedBuf1.iterator().mkString() );
            assertEquals( "0.5, 1.0", copiedBuf2.iterator().mkString() );
            assertEquals( "0.5, 1.0, 1.5", copiedBuf3.iterator().mkString() );
            assertEquals( "0.5, 1.0, 1.5, 2.0", copiedBuf4.iterator().mkString() );
            assertEquals( "1.0, 1.5, 2.0, 2.5", copiedBuf5.iterator().mkString() );

            origBuf.append( 6 );
            SeqDouble copiedBuf6 = mappedBuf.copy();
            assertEquals( "3.0, 4.0, 5.0, 6.0", origBuf.iterator().mkString() );
            assertEquals( "1.5, 2.0, 2.5, 3.0", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "0.5", copiedBuf1.iterator().mkString() );
            assertEquals( "0.5, 1.0", copiedBuf2.iterator().mkString() );
            assertEquals( "0.5, 1.0, 1.5", copiedBuf3.iterator().mkString() );
            assertEquals( "0.5, 1.0, 1.5, 2.0", copiedBuf4.iterator().mkString() );
            assertEquals( "1.0, 1.5, 2.0, 2.5", copiedBuf5.iterator().mkString() );
            assertEquals( "1.5, 2.0, 2.5, 3.0", copiedBuf6.iterator().mkString() );

            origBuf.append( 7 );
            SeqDouble copiedBuf7 = mappedBuf.copy();

            assertEquals( "4.0, 5.0, 6.0, 7.0", origBuf.iterator().mkString() );
            assertEquals( "2.0, 2.5, 3.0, 3.5", mappedBuf.iterator().mkString() );
            assertEquals( "", copiedBuf0.iterator().mkString() );
            assertEquals( "0.5", copiedBuf1.iterator().mkString() );
            assertEquals( "0.5, 1.0", copiedBuf2.iterator().mkString() );
            assertEquals( "0.5, 1.0, 1.5", copiedBuf3.iterator().mkString() );
            assertEquals( "0.5, 1.0, 1.5, 2.0", copiedBuf4.iterator().mkString() );
            assertEquals( "1.0, 1.5, 2.0, 2.5", copiedBuf5.iterator().mkString() );
            assertEquals( "1.5, 2.0, 2.5, 3.0", copiedBuf6.iterator().mkString() );
            assertEquals( "2.0, 2.5, 3.0, 3.5", copiedBuf7.iterator().mkString() );
        }

        @Test
        public void showThatADivBufferCanNotBeModified() {
            SeqDouble origBuf = new ArrayRingBufferDouble( 4 );
            SeqDouble mappedBuf = origBuf.div( 2 );

            try {
                mappedBuf.append( 0 );
                fail( "expected UnsupportedOperationException" );
            } catch ( UnsupportedOperationException ex ) {
                assertEquals( "This ring buffer has been set to read only", ex.getMessage() );
            }
        }

        @Test
        public void showThatACopyOfADivBufferCanBeModified() {
            SeqDouble origBuf = new ArrayRingBufferDouble( 4 );
            SeqDouble mappedBuf = origBuf.div( 2 );

            origBuf.appendAll( 1, 2, 3, 4 );

            SeqDouble copiedBuf = mappedBuf.copy();

            copiedBuf.append( 5 );
            assertEquals( "1.0, 1.5, 2.0, 5.0", copiedBuf.mkString() );
        }
    }


    @Nested
    public class SumPreviousTestCases {
        @Nested
        public class UsingSumPrevious2 {
            @Test
            public void givenEmptySeq_expectEmptySumPrevious2() {
                SeqDouble origBuf        = new ArrayRingBufferDouble( 10 );
                SeqDouble sumPreviousBuf = origBuf.sumPrevious( 2 );

                assertEquals( "", sumPreviousBuf.mkString() );
                assertEquals( 0, sumPreviousBuf.contentsCount() );
                assertEquals( 10, sumPreviousBuf.size() );
            }

            @Test
            public void givenOneValueSeq_expectSumPrevious2() {
                SeqDouble origBuf = new ArrayRingBufferDouble( 10 );
                SeqDouble sumPreviousBuf = origBuf.sumPrevious( 2 );

                origBuf.appendAll( 1.0 );

                assertEquals( "1.0", sumPreviousBuf.mkString() );
                assertEquals( 1, sumPreviousBuf.contentsCount() );
                assertEquals( 10, sumPreviousBuf.size() );
            }

            @Test
            public void givenTwoValueSeq_expectSumPrevious2() {
                SeqDouble origBuf = new ArrayRingBufferDouble( 10 );
                SeqDouble sumPreviousBuf = origBuf.sumPrevious( 2 );

                origBuf.appendAll( 1.0, 2.0 );

                assertEquals( "1.0, 3.0", sumPreviousBuf.mkString() );
                assertEquals( 2, sumPreviousBuf.contentsCount() );
                assertEquals( 10, sumPreviousBuf.size() );
            }

            @Test
            public void givenThreeValueSeq_expectSumPrevious2() {
                SeqDouble origBuf = new ArrayRingBufferDouble( 10 );
                SeqDouble sumPreviousBuf = origBuf.sumPrevious( 2 );

                origBuf.appendAll( 1.0, 2.0, 3.0 );

                assertEquals( "1.0, 3.0, 5.0", sumPreviousBuf.mkString() );
                assertEquals( 3, sumPreviousBuf.contentsCount() );
                assertEquals( 10, sumPreviousBuf.size() );
            }

            @Test
            public void givenFourValueSeq_expectSumPrevious2() {
                SeqDouble origBuf = new ArrayRingBufferDouble( 10 );
                SeqDouble sumPreviousBuf = origBuf.sumPrevious( 2 );

                origBuf.appendAll( 1.0, 2.0, 3.0, 4.0 );

                assertEquals( "1.0, 3.0, 5.0, 7.0", sumPreviousBuf.mkString() );
                assertEquals( 4, sumPreviousBuf.contentsCount() );
                assertEquals( 10, sumPreviousBuf.size() );
            }

            @Test
            public void givenFiveValueSeq_expectSumPrevious2() {
                SeqDouble origBuf = new ArrayRingBufferDouble( 10 );
                SeqDouble sumPreviousBuf = origBuf.sumPrevious( 2 );

                origBuf.appendAll( 1.0, 2.0, 3.0, 4.0, 5.0 );

                assertEquals( "1.0, 3.0, 5.0, 7.0, 9.0", sumPreviousBuf.mkString() );
                assertEquals( 5, sumPreviousBuf.contentsCount() );
                assertEquals( 10, sumPreviousBuf.size() );
            }
        }

        @Nested
        public class UsingSumPrevious3 {
            @Test
            public void givenEmptySeq_expectEmptySumPrevious3() {
                SeqDouble origBuf = new ArrayRingBufferDouble( 10 );
                SeqDouble sumPreviousBuf = origBuf.sumPrevious( 3 );

                assertEquals( "", sumPreviousBuf.mkString() );
                assertEquals( 0, sumPreviousBuf.contentsCount() );
                assertEquals( 10, sumPreviousBuf.size() );
            }

            @Test
            public void givenOneValueSeq_expectSumPrevious3() {
                SeqDouble origBuf = new ArrayRingBufferDouble( 10 );
                SeqDouble sumPreviousBuf = origBuf.sumPrevious( 3 );

                origBuf.appendAll( 1.0 );

                assertEquals( "1.0", sumPreviousBuf.mkString() );
                assertEquals( 1, sumPreviousBuf.contentsCount() );
                assertEquals( 10, sumPreviousBuf.size() );
            }

            @Test
            public void givenTwoValueSeq_expectPrevious3() {
                SeqDouble origBuf = new ArrayRingBufferDouble( 10 );
                SeqDouble sumPreviousBuf = origBuf.sumPrevious( 3 );

                origBuf.appendAll( 1.0, 2.0 );

                assertEquals( "1.0, 3.0", sumPreviousBuf.mkString() );
                assertEquals( 2, sumPreviousBuf.contentsCount() );
                assertEquals( 10, sumPreviousBuf.size() );
            }

            @Test
            public void givenThreeValueSeq_expectSumPrevious3() {
                SeqDouble origBuf = new ArrayRingBufferDouble( 10 );
                SeqDouble sumPreviousBuf = origBuf.sumPrevious( 3 );

                origBuf.appendAll( 1.0, 2.0, 3.0 );

                assertEquals( "1.0, 3.0, 6.0", sumPreviousBuf.mkString() );
                assertEquals( 3, sumPreviousBuf.contentsCount() );
                assertEquals( 10, sumPreviousBuf.size() );
            }

            @Test
            public void givenFourValueSeq_expectSumPrevious3() {
                SeqDouble origBuf = new ArrayRingBufferDouble( 10 );
                SeqDouble sumPreviousBuf = origBuf.sumPrevious( 3 );

                origBuf.appendAll( 1.0, 2.0, 3.0, 4.0 );

                assertEquals( "1.0, 3.0, 6.0, 9.0", sumPreviousBuf.mkString() );
                assertEquals( 4, sumPreviousBuf.contentsCount() );
                assertEquals( 10, sumPreviousBuf.size() );
            }

            @Test
            public void givenFiveValueSeq_expectSumPrevious3() {
                SeqDouble origBuf = new ArrayRingBufferDouble( 10 );
                SeqDouble sumPreviousBuf = origBuf.sumPrevious( 3 );

                origBuf.appendAll( 1.0, 2.0, 3.0, 4.0, 5.0 );

                assertEquals( "1.0, 3.0, 6.0, 9.0, 12.0", sumPreviousBuf.mkString() );
                assertEquals( 5, sumPreviousBuf.contentsCount() );
                assertEquals( 10, sumPreviousBuf.size() );
            }

            @Test
            public void givenSixValueSeq_expectSumPrevious3() {
                SeqDouble origBuf = new ArrayRingBufferDouble( 10 );
                SeqDouble sumPreviousBuf = origBuf.sumPrevious( 3 );

                origBuf.appendAll( 1.0, 2.0, 3.0, 4.0, 5.0, 6.0 );

                assertEquals( "1.0, 3.0, 6.0, 9.0, 12.0, 15.0", sumPreviousBuf.mkString() );
                assertEquals( 6, sumPreviousBuf.contentsCount() );
                assertEquals( 10, sumPreviousBuf.size() );
            }

            @Test
            public void givenSixValueSeqAndRingBufferSizeOf3_expectThreeSumPrevious3() {
                SeqDouble origBuf = new ArrayRingBufferDouble( 3 );
                SeqDouble sumPreviousBuf = origBuf.sumPrevious( 3 );

                origBuf.appendAll( 1.0, 2.0, 3.0, 4.0, 5.0, 6.0 );

                assertEquals( "4.0, 9.0, 15.0", sumPreviousBuf.mkString() );
                assertEquals( 3, sumPreviousBuf.contentsCount() );
                assertEquals( 3, sumPreviousBuf.size() );
            }
        }
    }


    @Nested
    public class ReverseIteratorTestCases {
        @Test
        public void givenEmptySeq_expectEmptyIterator() {
            SeqDouble seq = new ArrayRingBufferDouble( 3 );

            assertEquals( "", seq.reverseIterator().mkString() );
        }

        @Test
        public void givenOneValue_expectSingleValueInIterator() {
            SeqDouble seq = new ArrayRingBufferDouble( 3 );

            seq.append( 3.14 );

            assertEquals( "3.14", seq.reverseIterator().mkString() );
        }

        @Test
        public void givenTwoValues_expectTwoValuesInIterator() {
            SeqDouble seq = new ArrayRingBufferDouble( 3 );

            seq.appendAll( 1.1, 1.2 );

            assertEquals( "1.2, 1.1", seq.reverseIterator().mkString() );
        }

        @Test
        public void givenThreeValues_expectThreeValuesInIterator() {
            SeqDouble seq = new ArrayRingBufferDouble( 3 );

            seq.appendAll( 1.1, 1.2, 1.3 );

            assertEquals( "1.3, 1.2, 1.1", seq.reverseIterator().mkString() );
        }

        @Test
        public void givenMoreValuesThanSeqCanStore_expectIteratorToContainTheMaxNumberOfValues() {
            SeqDouble seq = new ArrayRingBufferDouble( 3 );

            seq.appendAll( 1.1, 1.2, 1.3, 1.4 );

            assertEquals( "1.4, 1.3, 1.2", seq.reverseIterator().mkString() );
        }
    }


    @Test
    public void testEquals() {
        assertEquals( seq4(), seq4() );
        assertNotEquals( seq4(), seq4(1.1) );
        assertEquals( seq4(1.1), seq4(1.1) );
        assertNotEquals( seq4(1.1), seq4(1.1,1.2) );
        assertEquals( seq4(1.1,1.2), seq4(1.1,1.2) );
    }

    private static SeqDouble seq4(double...values) {
        SeqDouble seq = new ArrayRingBufferDouble( 4 );

        seq.appendAll( values );

        return seq;
    }

    @Nested
    public class GeneralTestCases {
        @Test
        public void testDoubleArray() {
            SeqDouble buf = new ArrayRingBufferDouble( 4 );

            // VALIDATE NEW CREATED/EMPTY RING BUFFER
            assertTrue( buf.isEmpty() );
            assertFalse( buf.isFull() );
            assertEquals( 0, buf.lhsIndex() );
            assertEquals( 0, buf.rhsIndexExc() );
            assertEquals( 4, buf.remaining() );
            assertEquals( 0, buf.contentsCount() );

            assertThrows( IndexOutOfBoundsException.class, () -> buf.get( 0 ), "index 0 is out of bounds, as the buffer is currently empty" );
            assertFalse( buf.iterator().hasNext() );


            // ADD ONE VALUE TO THE BUFFER
            buf.append( 3 );

            assertFalse( buf.isEmpty() );
            assertFalse( buf.isFull() );
            assertEquals( 0, buf.lhsIndex() );
            assertEquals( 0, buf.rhsIndexInc() );
            assertEquals( 3, buf.remaining() );
            assertEquals( 1, buf.contentsCount() );

            assertEquals( 3, buf.get( 0 ) );
            assertThrows( IndexOutOfBoundsException.class, () -> buf.get( 1 ), "index 1 is out of bounds, currently supported indexes are 0 to 0 (inc)" );

            assertIterator( buf, 3 ); // ensure that iterator() can be called twice, giving the same results
            assertIterator( buf, 3 );


            // ADD ANOTHER VALUE TO THE BUFFER  (BUFFER WILL NOT WRAP YET)
            buf.append( 2 );

            assertFalse( buf.isEmpty() );
            assertFalse( buf.isFull() );
            assertEquals( 0, buf.lhsIndex() );
            assertEquals( 1, buf.rhsIndexInc() );
            assertEquals( 2, buf.remaining() );
            assertEquals( 2, buf.contentsCount() );

            assertEquals( 3, buf.get( 0 ) );
            assertEquals( 2, buf.get( 1 ) );
            assertThrows( IndexOutOfBoundsException.class, () -> buf.get( 2 ), "index 2 is out of bounds, currently supported indexes are 0 to 1 (inc)" );

            assertIterator( buf, 3, 2 ); // ensure that iterator() can be called twice, giving the same results
            assertIterator( buf, 3, 2 );


            // ADD ANOTHER VALUE TO THE BUFFER  (BUFFER WILL NOT WRAP YET)
            buf.append( 1 );

            assertFalse( buf.isEmpty() );
            assertFalse( buf.isFull() );
            assertEquals( 0, buf.lhsIndex() );
            assertEquals( 2, buf.rhsIndexInc() );
            assertEquals( 1, buf.remaining() );
            assertEquals( 3, buf.contentsCount() );

            assertEquals( 3, buf.get( 0 ) );
            assertEquals( 2, buf.get( 1 ) );
            assertEquals( 1, buf.get( 2 ) );
            assertThrows( IndexOutOfBoundsException.class, () -> buf.get( 3 ), "index 3 is out of bounds, currently supported indexes are 0 to 2 (inc)" );

            assertIterator( buf, 3, 2, 1 ); // ensure that iterator() can be called twice, giving the same results
            assertIterator( buf, 3, 2, 1 );


            // ADD ANOTHER VALUE TO THE BUFFER  (BUFFER WILL NOT WRAP YET)
            buf.append( 2 );

            assertFalse( buf.isEmpty() );
            assertTrue( buf.isFull() );
            assertEquals( 0, buf.lhsIndex() );
            assertEquals( 3, buf.rhsIndexInc() );
            assertEquals( 0, buf.remaining() );
            assertEquals( 4, buf.contentsCount() );

            assertEquals( 3, buf.get( 0 ) );
            assertEquals( 2, buf.get( 1 ) );
            assertEquals( 1, buf.get( 2 ) );
            assertEquals( 2, buf.get( 3 ) );
            assertThrows( IndexOutOfBoundsException.class, () -> buf.get( 4 ), "index 4 is out of bounds, currently supported indexes are 0 to 3 (inc)" );

            assertIterator( buf, 3, 2, 1, 2 ); // ensure that iterator() can be called twice, giving the same results
            assertIterator( buf, 3, 2, 1, 2 );


            // ADD ANOTHER VALUE TO THE BUFFER  (BUFFER WILL WRAP, DROPPING THE FIRST VALUE)
            buf.append( 3 );

            assertFalse( buf.isEmpty() );
            assertTrue( buf.isFull() );
            assertEquals( 1, buf.lhsIndex() );
            assertEquals( 4, buf.rhsIndexInc() );
            assertEquals( 0, buf.remaining() );
            assertEquals( 4, buf.contentsCount() );

            assertThrows( IndexOutOfBoundsException.class, () -> buf.get( 0 ), "index 0 is out of bounds, currently supported indexes are 1 to 4 (inc)" );
            assertEquals( 2, buf.get( 1 ) );
            assertEquals( 1, buf.get( 2 ) );
            assertEquals( 2, buf.get( 3 ) );
            assertEquals( 3, buf.get( 4 ) );
            assertThrows( IndexOutOfBoundsException.class, () -> buf.get( 5 ), "index 5 is out of bounds, currently supported indexes are 1 to 4 (inc)" );

            assertIterator( buf, 2, 1, 2, 3 ); // ensure that iterator() can be called twice, giving the same results
            assertIterator( buf, 2, 1, 2, 3 );
        }

        private void assertIterator( SeqDouble buf, double... expectedValues ) {
            DoubleIterator it = buf.iterator();

            for ( int i = 0; i < expectedValues.length; i++ ) {
                assertTrue( it.hasNext() );
                assertEquals( expectedValues[i], it.next() );
            }

            assertFalse( it.hasNext() );
        }
    }
}
