package mosaics.collections.mutable.graphs.oo;

import mosaics.fp.FP;
import mosaics.fp.collections.FPOption;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class OOGraphNodeTest {


// NEW NODE

    @Test
    public void createNewNode_expectNoContents() {
        GraphNode<String,Integer> node = createNode();

        assertTrue( node.getNodeContents().isEmpty() );
    }

    @Test
    public void createNewNode_expectNoOutLinks() {
        GraphNode<String,Integer> node = createNode();

        assertTrue( node.getOutEdges().isEmpty() );
    }


//  NODES CONTENTS

    @Test
    public void createNewNode_setContents_expectToRetrieveIt() {
        GraphNode<String,Integer> node = createNode();

        node.setNodeContents( 42 );
        assertEquals( 42, node.getNodeContents().get().intValue() );
    }

    @Test
    public void givenContents_clear_expectNoContents() {
        GraphNode<String,Integer> node = createNode();

        node.setNodeContents( 42 );
        node.clearNodeContents();

        assertTrue( node.getNodeContents().isEmpty() );
    }


// SINGLE EDGE

    @Test
    public void givenUnlinkedNode_getMissingLink_expectNoMatchingNode() {
        GraphNode<String,Integer> node = createNode();

        FPOption<GraphNode<String, Integer>> e1 = node.get( "e1" );

        assertTrue( e1.isEmpty() );
    }

    @Test
    public void givenLinkedNode_getMissingLink_expectNoMatchingNode() {
        GraphNode<String,Integer> node = createNode();
        node.getOrCreateEdge( "e1" );

        FPOption<GraphNode<String, Integer>> e1 = node.get( "e2" );

        assertTrue( e1.isEmpty() );
    }

    @Test
    public void givenLinkedNode_getExistingLink_expectMatchingNode() {
        GraphNode<String,Integer> node = createNode();
        node.getOrCreateEdge( "e1" );

        FPOption<GraphNode<String, Integer>> e1 = node.get( "e1" );

        assertTrue( e1.hasValue() );
    }

    @Test
    public void givenLinkedNode_getOutEdges_expectOneEdge() {
        GraphNode<String,Integer> node = createNode();
        GraphEdge<String,Integer> e1 = node.getOrCreateEdge( "e1" );

        assertEquals( "e1", e1.getKey() );
        assertEquals( FP.wrapAll(e1), node.getOutEdges() );
    }

    @Test
    public void givenTwoLinkedNode_getOutEdges_expectOneEdge() {
        GraphNode<String,Integer> node = createNode();
        GraphEdge<String,Integer> e1 = node.getOrCreateEdge( "e1" );
        GraphEdge<String,Integer> e2 = node.getOrCreateEdge( "e2" );


        assertEquals( "e1", e1.getKey() );
        assertEquals( "e2", e2.getKey() );
        assertEquals( FP.wrapAll(e1,e2), node.getOutEdges() );
    }

// MULTIPLE EDGES

    @Test
    public void givenTwoNodesThatPartialShareRoute() {
        GraphNode<String,Integer> node = createNode();
        GraphNode<String,Integer> b1 = node.getOrCreate( "a1", "b1" );
        GraphNode<String,Integer> b2 = node.getOrCreate( "a1", "b2" );

        assertNotSame( b1, b2 );

        GraphNode<String,Integer> a1 = node.get( "a1" ).get();
        assertEquals( FP.wrapAll(new OOGraphEdge<>("b1",b1), new OOGraphEdge<>("b2",b2)), a1.getOutEdges() );
    }


    private GraphNode<String,Integer> createNode() {
        return new OOGraphNode<>();
    }

}
