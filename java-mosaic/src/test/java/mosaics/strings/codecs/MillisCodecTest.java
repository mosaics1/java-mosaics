package mosaics.strings.codecs;

import mosaics.lang.reflection.JavaClass;
import mosaics.strings.StringCodec;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;


public class MillisCodecTest {

    public static final StringCodec<Long> codec = MillisCodec.INSTANCE;


    @Test
    public void testGetType() {
        assertEquals( JavaClass.LONG_PRIMITIVE, codec.getType() );
    }


    @Test
    public void testSupportsNull() {
        assertFalse( codec.supportsNull() );
    }

    @Test
    public void testToString() {
        assertEquals( "MillisCodec", codec.toString() );
    }


    @Test
    public void encodeMillis() {
        assertEquals("",   codec.encode(null));
        assertEquals("0 millis",   codec.encode(0L));
        assertEquals("1 milli",   codec.encode(1L));
        assertEquals("2 millis",   codec.encode(2L));
        assertEquals("3 millis",   codec.encode(3L));
        assertEquals("999 millis",   codec.encode(999L));
        assertEquals("1000 millis",   codec.encode(1000L));
        assertEquals("1001 millis",   codec.encode(1001L));

        assertEquals("60000 millis",   codec.encode(60000L));
        assertEquals("63000 millis",   codec.encode(63000L));
        assertEquals("63050 millis",   codec.encode(63050L));

        assertEquals("3600000 millis",   codec.encode(60*60*1000L));

        assertEquals("-1 milli",   codec.encode(-1L));
        assertEquals("-2 millis",   codec.encode(-2L));
        assertEquals("-3 millis",   codec.encode(-3L));
        assertEquals("-999 millis",   codec.encode(-999L));
        assertEquals("-1000 millis",   codec.encode(-1000L));
        assertEquals("-1001 millis",   codec.encode(-1001L));

        assertEquals("-60000 millis",   codec.encode(-60000L));
        assertEquals("-63000 millis",   codec.encode(-63000L));
        assertEquals("-63050 millis",   codec.encode(-63050L));
    }

    @Test
    public void decodeMillis() {
        assertEquals(0L,   codec.decode("0 millis").getResult().longValue());
        assertEquals(1L,   codec.decode("1 milli").getResult().longValue());
        assertEquals(1L,   codec.decode("1 millis").getResult().longValue());
        assertEquals(2L,   codec.decode("2 millis").getResult().longValue());
        assertEquals(3L,   codec.decode("3 millis").getResult().longValue());
        assertEquals(999L,   codec.decode("999 millis").getResult().longValue());
        assertEquals(1000L,   codec.decode("1000 millis").getResult().longValue());
        assertEquals(1001L,   codec.decode("1001 millis").getResult().longValue());

        assertEquals(60000L,   codec.decode("60000 millis").getResult().longValue());
        assertEquals(63000L,   codec.decode("63000 millis").getResult().longValue());
        assertEquals(63050L,   codec.decode("63050 millis").getResult().longValue());

        assertEquals(-1L,   codec.decode("-1 millis").getResult().longValue());
        assertEquals(-1L,   codec.decode("-1 milli").getResult().longValue());
        assertEquals(-2L,   codec.decode("-2 millis").getResult().longValue());
        assertEquals(-63000L,   codec.decode("-63000 millis").getResult().longValue());
        assertEquals(-63052L,   codec.decode("-63052 millis").getResult().longValue());


        assertEquals( "java.lang.NumberFormatException: For input string: \"-1234a5\"", codec.decode("-1234a5").getFailure().toString() );
    }

}
