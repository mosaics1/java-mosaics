package mosaics.strings.codecs;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class CashCodecTest {

    @Test
    public void encodeCash() {
        assertEquals("£0.00",   CashCodec.INSTANCE.encode(0L));
        assertEquals("£0.00",   CashCodec.INSTANCE.encode(7L));
        assertEquals("£0.13",  CashCodec.INSTANCE.encode(132L));
        assertEquals("-£0.13", CashCodec.INSTANCE.encode(-132L));
    }

    @Test
    public void decodeCash() {
        assertEquals(0L,    CashCodec.INSTANCE.decode("0").getResult().longValue());
        assertEquals(0L,    CashCodec.INSTANCE.decode("0.0").getResult().longValue());
        assertEquals(13200L,  CashCodec.INSTANCE.decode("13.2").getResult().longValue());
        assertEquals(13219L,  CashCodec.INSTANCE.decode("13.219").getResult().longValue());
        assertEquals(13210L,  CashCodec.INSTANCE.decode("13.210").getResult().longValue());
        assertEquals(13201L,  CashCodec.INSTANCE.decode("13.201").getResult().longValue());
        assertEquals(13211L, CashCodec.INSTANCE.decode("13.2111").getResult().longValue());
        assertEquals(13211L, CashCodec.INSTANCE.decode("13.2114").getResult().longValue());
        assertEquals(13212L, CashCodec.INSTANCE.decode("13.2115").getResult().longValue());
        assertEquals(13212L, CashCodec.INSTANCE.decode("13.2119").getResult().longValue());
        assertEquals(-13211L, CashCodec.INSTANCE.decode("-13.2114").getResult().longValue());
        assertEquals(-13212L, CashCodec.INSTANCE.decode("-13.2115").getResult().longValue());
        assertEquals(-13212L, CashCodec.INSTANCE.decode("-13.2119").getResult().longValue());

        assertEquals(0L,    CashCodec.INSTANCE.decode("£0").getResult().longValue());
        assertEquals(0L,    CashCodec.INSTANCE.decode("£0.0").getResult().longValue());
        assertEquals(13200L,  CashCodec.INSTANCE.decode("£13.2").getResult().longValue());
        assertEquals(13219L,  CashCodec.INSTANCE.decode("£13.219").getResult().longValue());
        assertEquals(13210L,  CashCodec.INSTANCE.decode("£13.210").getResult().longValue());
        assertEquals(13201L,  CashCodec.INSTANCE.decode("£13.201").getResult().longValue());
        assertEquals(13211L, CashCodec.INSTANCE.decode("£13.2111").getResult().longValue());
        assertEquals(13211L, CashCodec.INSTANCE.decode("£13.2114").getResult().longValue());
        assertEquals(13212L, CashCodec.INSTANCE.decode("£13.2115").getResult().longValue());
        assertEquals(13212L, CashCodec.INSTANCE.decode("£13.2119").getResult().longValue());
        assertEquals(-13211L, CashCodec.INSTANCE.decode("-£13.2114").getResult().longValue());
        assertEquals(-13212L, CashCodec.INSTANCE.decode("-£13.2115").getResult().longValue());
        assertEquals(-13212L, CashCodec.INSTANCE.decode("-£13.2119").getResult().longValue());

        assertEquals( "java.lang.NumberFormatException: For input string: \"-1234a5\"", CashCodec.INSTANCE.decode("-1234a5").getFailure().toString() );
    }

}
