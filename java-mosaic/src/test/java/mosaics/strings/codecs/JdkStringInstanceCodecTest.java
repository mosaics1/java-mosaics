package mosaics.strings.codecs;

import mosaics.strings.StringCodec;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;


public class JdkStringInstanceCodecTest {

    public static final StringCodec<String> codec = JdkStringInstanceCodec.INSTANCE;


    @Test
    public void encodeNoOp() {
        assertEquals( "abc", codec.encode( "abc" ) );
        assertEquals( "123", codec.encode( "123" ) );
    }

    @Test
    public void decodeNoOp() {
        assertEquals( "abc", codec.decode("abc").getResult() );
        assertEquals( "123", codec.decode("123").getResult() );
    }

    @Test
    public void testSupportsNull() {
        assertFalse( codec.supportsNull() );
    }

    @Test
    public void testToString() {
        assertEquals( "StringCodec", codec.toString() );
    }

}
