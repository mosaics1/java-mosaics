package mosaics.strings.codecs;

import mosaics.lang.reflection.JavaClass;
import mosaics.lang.time.DTM;
import mosaics.strings.StringCodec;
import org.junit.jupiter.api.Test;

import static mosaics.lang.time.DTMUtils.DATE_ISO8601_FORMATTER;
import static mosaics.lang.time.DTMUtils.DTM_ISO8601_FORMATTER;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;


public class DTMCodecTest {

    private StringCodec<DTM> codec = DTMCodec.DEFAULT_CODEC;


    @Test
    public void testHashCode() {
        assertNotEquals( new DTMCodec(DTM_ISO8601_FORMATTER.toPattern()), new DTMCodec(DATE_ISO8601_FORMATTER.toPattern()) );
    }

    @Test
    public void testType() {
        assertEquals( JavaClass.LONG_PRIMITIVE, codec.getType() );
    }

    @Test
    public void testEncode() {
        assertEquals( "null", codec.encode(null) );
        assertEquals( "2016-10-01 20:32:11", codec.encode( new DTM(2016,10,1, 20,32,11)) );
    }

    @Test
    public void testDecode() {
        assertEquals( new DTM(2016,10,1, 20,32,11), codec.decode("2016-10-01 20:32:11").getResult());
    }

    @Test
    public void testToString() {
        assertEquals( "DTMCodec(yyyy-MM-dd HH:mm:ss)", codec.toString() );
    }

    @Test
    public void testSupportsNull() {
        assertFalse( codec.supportsNull() );
    }
}
