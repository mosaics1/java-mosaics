package mosaics.strings.codecs;

import mosaics.fp.Try;
import mosaics.lang.reflection.JavaClass;
import mosaics.strings.MalformedValueFailure;
import mosaics.strings.StringCodec;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.fail;


public class EnumCodecTest {
    private StringCodec<RGB> codec = new EnumCodec<>( RGB.class );


    @Test
    @SuppressWarnings("unchecked")
    public void tryCreatingCodecForNonEnumType_expectException() {
        try {
            new EnumCodec( String.class );
            fail( "expected exception" );
        } catch ( IllegalArgumentException ex ) {
            assertEquals( "java.lang.String must be an Enum", ex.getMessage() );
        }
    }

    @Test
    public void encodeEnumValue() {
        assertEquals( "BLUE", codec.encode(RGB.BLUE) );
    }

    @Test
    public void decodeEnumValue() {
        assertEquals( Try.succeeded(RGB.RED), codec.decode("RED") );
    }

    @Test
    public void decodeLowerCaseEnumValue() {
        assertEquals( Try.succeeded(RGB.BLUE), codec.decode("blue") );
    }

    @Test
    public void decodeMixCaseEnumValue() {
        assertEquals( Try.succeeded(RGB.GREEN), codec.decode("gReEn") );
    }

    @Test
    public void getType() {
        assertEquals( JavaClass.of(RGB.class), codec.getType() );
    }


    @Test
    public void testSupportsNull() {
        assertFalse( codec.supportsNull() );
    }

    @Test
    public void testHashCode() {
        assertEquals( codec.hashCode(), new EnumCodec<>(RGB.class).hashCode());
        assertNotEquals( codec.hashCode(), new EnumCodec<>(Planets.class).hashCode());
    }

    @Test
    public void tryDecodingNonEnumValue_expectDescriptiveErrorMessage() {
        assertEquals(
            Try.failed(new MalformedValueFailure("rouge", "RED,GREEN,BLUE")),
            codec.decode("rouge")
        );
    }

    @Test
    public void equalsContract() {
        EqualsVerifier.forClass( EnumCodec.class )
            .suppress( Warning.STRICT_INHERITANCE )
            .withPrefabValues( RGB.class, RGB.RED, RGB.BLUE )
            .withPrefabValues( JavaClass.class, JavaClass.of(RGB.class), JavaClass.STRING )
            .withIgnoredFields( "validValues", "validValuesStr" )
            .verify();
    }


    public enum RGB {
        RED,GREEN,BLUE
    }

    public enum Planets {
        MERCURY, VENUS, EARTH
    }
}
