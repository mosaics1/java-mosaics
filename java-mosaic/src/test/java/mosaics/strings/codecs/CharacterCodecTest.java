package mosaics.strings.codecs;

import mosaics.strings.StringCodec;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;


public class CharacterCodecTest {

    @Test
    public void testHashCode() {
        assertNotEquals( CharacterCodec.CHARACTER_PRIMITIVE_CODEC, CharacterCodec.CHARACTER_OBJECT_CODEC );
    }

    @Nested
    public class PrimitiveCHARACTERTestCases {
        private StringCodec<Character> codec = CharacterCodec.CHARACTER_PRIMITIVE_CODEC;


        @Test
        public void encodeCHARACTER() {
            assertEquals( "", codec.encode(null) );
            assertEquals( "c", codec.encode('c') );
            assertEquals("£", codec.encode('£') );
            assertEquals("グ", codec.encode('グ') );
        }

        @Test
        public void decodeCHARACTER() {
            assertEquals("java.lang.IllegalArgumentException: Invalid character: null", codec.decode(null).getFailureMessage());

            assertEquals('a', codec.decode("a").getResult().charValue());
            assertEquals('£', codec.decode("£").getResult().charValue());
            assertEquals('グ', codec.decode("グ").getResult().charValue());
            assertEquals("java.lang.IllegalArgumentException: Invalid character: ab", codec.decode("ab").getFailureMessage());
        }

        @Test
        public void testToString() {
            assertEquals( "CharacterCodec", codec.toString() );
        }

        @Test
        public void testSupportsNull() {
            assertFalse( codec.supportsNull() );
        }
    }


    @Nested
    public class ObjectTestCases {
        private StringCodec<Character> codec = CharacterCodec.CHARACTER_OBJECT_CODEC;

        @Test
        public void encodeCHARACTER() {
            assertEquals( "", codec.encode(null) );
            assertEquals( "c", codec.encode('c') );
            assertEquals("£", codec.encode('£') );
            assertEquals("グ", codec.encode('グ') );
        }

        @Test
        public void decodeCHARACTER() {
            assertEquals("java.lang.IllegalArgumentException: Invalid character: null", codec.decode(null).getFailureMessage());

            assertEquals('a', codec.decode("a").getResult().charValue());
            assertEquals('£', codec.decode("£").getResult().charValue());
            assertEquals('グ', codec.decode("グ").getResult().charValue());
            assertEquals("java.lang.IllegalArgumentException: Invalid character: ab", codec.decode("ab").getFailureMessage());
        }

        @Test
        public void testToString() {
            assertEquals( "CharacterCodec", codec.toString() );
        }

        @Test
        public void testSupportsNull() {
            assertFalse( codec.supportsNull() );
        }
    }

}
