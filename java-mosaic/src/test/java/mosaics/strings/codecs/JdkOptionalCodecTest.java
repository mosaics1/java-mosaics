package mosaics.strings.codecs;

import mosaics.lang.reflection.JavaClass;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static mosaics.strings.codecs.StringCodecAssertions.assertCodecRoundTrip;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class JdkOptionalCodecTest {

    private final JdkOptionalCodec<Long> codec = new JdkOptionalCodec<>( DurationLongCodec.INSTANCE );


    @Test
    public void fpOptionCases() {
        assertCodecRoundTrip( codec, Optional.empty(), null );
        assertCodecRoundTrip( codec, Optional.of(0L), "0s" );
        assertCodecRoundTrip( codec, Optional.of(42L), "0.042s" );

        assertEquals( Optional.empty(), codec.decode("").getResult() );
    }

    @Test
    public void testToString() {
        assertEquals( "JdkOptionalCodec(DurationPrimitiveLongCodec)", codec.toString() );
    }

    @Test
    public void testSupportsNull() {
        assertTrue( codec.supportsNull() );
    }

    @Test
    public void testGetType() {
        assertEquals( JavaClass.LONG_PRIMITIVE, codec.getType() );
    }

    @Test
    public void testHashCode() {
        assertEquals( codec.hashCode(), codec.hashCode() );
        assertEquals( codec.hashCode(), new JdkOptionalCodec<>( DurationLongCodec.INSTANCE ).hashCode() );
        assertNotEquals( codec.hashCode(), new JdkOptionalCodec<>( DefaultStringCodecFactory.createCodecFor(Float.class) ).hashCode() );
    }
}
