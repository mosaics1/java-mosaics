package mosaics.strings.codecs;

import mosaics.fp.FP;
import org.junit.jupiter.api.Test;

import static mosaics.strings.codecs.StringCodecAssertions.assertCodecRoundTrip;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class FPOptionCodecTest {

    private static final FPOptionCodec<Integer> intOptCodec = new FPOptionCodec<>( DefaultStringCodecFactory.createCodecFor(Integer.class) );


    @Test
    public void fpOptionCases() {
        assertCodecRoundTrip( intOptCodec, FP.emptyOption(), null );
        assertCodecRoundTrip( intOptCodec, FP.option(0), "0" );
        assertCodecRoundTrip( intOptCodec, FP.option(42), "42" );

        assertEquals( FP.emptyOption(), intOptCodec.decode("").getResult() );
    }

    @Test
    public void testToString() {
        assertEquals( "FPOptionCodec(innerCodec=IntegerCodec, type=java.lang.Integer)", intOptCodec.toString() );
    }

    @Test
    public void testSupportsNull() {
        assertTrue( intOptCodec.supportsNull() );
    }

}
