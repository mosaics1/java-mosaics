package mosaics.strings.codecs;

import mosaics.lang.reflection.JavaClass;
import mosaics.strings.StringCodec;
import org.junit.jupiter.api.Test;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;


public class JdkDurationCodecTest {

    private StringCodec<Duration> codec = JdkDurationCodec.INSTANCE;


    @Test
    public void testHashCode() {
        assertEquals( new JdkDurationCodec(), new JdkDurationCodec() );
    }

    @Test
    public void testType() {
        assertEquals( JavaClass.DURATION, codec.getType() );
    }

    @Test
    public void testToString() {
        assertEquals( "JdkDurationCodec", codec.toString() );
    }

    @Test
    public void testSupportsNull() {
        assertFalse( codec.supportsNull() );
    }


    @Test
    public void encodeDuration() {
        assertEquals("",           codec.encode(null));
        assertEquals("0s",         codec.encode(Duration.ofMillis(0L)));
        assertEquals("0.001s",     codec.encode(Duration.ofMillis(1L)));
        assertEquals("0.002s",     codec.encode(Duration.ofMillis(2L)));
        assertEquals("0.003s",     codec.encode(Duration.ofMillis(3L)));
        assertEquals("0.999s",     codec.encode(Duration.ofMillis(999L)));
        assertEquals("1s",         codec.encode(Duration.ofMillis(1000L)));
        assertEquals("1.001s",     codec.encode(Duration.ofMillis(1001L)));

        assertEquals("1m",         codec.encode(Duration.ofMillis(60000L)));
        assertEquals("1m 3s",      codec.encode(Duration.ofMillis(63000L)));
        assertEquals("1m 3.050s",  codec.encode(Duration.ofMillis(63050L)));
        assertEquals("2h 10m",     codec.encode(Duration.ofMillis( 2*60*60*1000L + 10*60*1000L )));

        assertEquals("1h",         codec.encode(Duration.ofMillis(60*60*1000L)));

        assertEquals("-0.001s",    codec.encode(Duration.ofMillis(-1L)));
        assertEquals("-0.002s",    codec.encode(Duration.ofMillis(-2L)));
        assertEquals("-0.003s",    codec.encode(Duration.ofMillis(-3L)));
        assertEquals("-0.999s",    codec.encode(Duration.ofMillis(-999L)));
        assertEquals("-1s",        codec.encode(Duration.ofMillis(-1000L)));
        assertEquals("-1.001s",    codec.encode(Duration.ofMillis(-1001L)));

        assertEquals("-1m",        codec.encode(Duration.ofMillis(-60000L)));
        assertEquals("-1m 3s",     codec.encode(Duration.ofMillis(-63000L)));
        assertEquals("-1m 3.050s", codec.encode(Duration.ofMillis(-63050L)));

        assertEquals("-1h",        codec.encode(Duration.ofMillis(-60*60*1000L)));
    }

    @Test
    public void decodeDuration() {
        assertEquals(1L,   JdkDurationCodec.INSTANCE.decode("0.001s").getResult().toMillis());
        assertEquals(2L,   JdkDurationCodec.INSTANCE.decode("0.002s").getResult().toMillis());
        assertEquals(3L,   JdkDurationCodec.INSTANCE.decode("0.003s").getResult().toMillis());
        assertEquals(999L,   JdkDurationCodec.INSTANCE.decode("0.999s").getResult().toMillis());
        assertEquals(1000L,   JdkDurationCodec.INSTANCE.decode("1s").getResult().toMillis());
        assertEquals(1001L,   JdkDurationCodec.INSTANCE.decode("1.001s").getResult().toMillis());

        assertEquals(60000L,   JdkDurationCodec.INSTANCE.decode("1m 0s").getResult().toMillis());
        assertEquals(63000L,   JdkDurationCodec.INSTANCE.decode("1m 3s").getResult().toMillis());
        assertEquals(63050L,   JdkDurationCodec.INSTANCE.decode("1m 3.050s").getResult().toMillis());

        assertEquals(60 * 60 * 1000L,   JdkDurationCodec.INSTANCE.decode("1h 0m 0s").getResult().toMillis());

        assertEquals(-1L,   JdkDurationCodec.INSTANCE.decode("-0.001s").getResult().toMillis());
        assertEquals(-2L,   JdkDurationCodec.INSTANCE.decode("-0.002s").getResult().toMillis());
        assertEquals(-3L,   JdkDurationCodec.INSTANCE.decode("-0.003s").getResult().toMillis());
        assertEquals(-999L,   JdkDurationCodec.INSTANCE.decode("-0.999s").getResult().toMillis());
        assertEquals(-1000L,   JdkDurationCodec.INSTANCE.decode("-1s").getResult().toMillis());
        assertEquals(-1001L,   JdkDurationCodec.INSTANCE.decode("-1.001s").getResult().toMillis());

        assertEquals(-60000L,   JdkDurationCodec.INSTANCE.decode("-1m 0s").getResult().toMillis());
        assertEquals(-63000L,   JdkDurationCodec.INSTANCE.decode("-1m 3s").getResult().toMillis());
        assertEquals(-63050L,   JdkDurationCodec.INSTANCE.decode("-1m 3.050s").getResult().toMillis());

        assertEquals(-60 * 60 * 1000L,   JdkDurationCodec.INSTANCE.decode("-1h 0m 0s").getResult().toMillis());


        assertEquals( "java.lang.NumberFormatException: For input string: null", JdkDurationCodec.INSTANCE.decode(null).getFailure().toString() );
        assertEquals( "java.lang.NumberFormatException: For input string: \"-1234a5\"", JdkDurationCodec.INSTANCE.decode("-1234a5").getFailure().toString() );
        assertEquals( "java.lang.NumberFormatException: For input string: \"1p\"", JdkDurationCodec.INSTANCE.decode("1p").getFailure().toString() );
        assertEquals( "java.lang.NumberFormatException: For input string: \"pppp\"", JdkDurationCodec.INSTANCE.decode("pppp").getFailure().toString() );
    }

}
