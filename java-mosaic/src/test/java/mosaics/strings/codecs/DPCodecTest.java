package mosaics.strings.codecs;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class DPCodecTest {

    @Test
    public void encode3dpLongs() {
        assertEquals("0.000",   DPCodec.DP3_CODEC.encode(0L));
        assertEquals("0.007",   DPCodec.DP3_CODEC.encode(7L));
        assertEquals("0.132",  DPCodec.DP3_CODEC.encode(132L));
        assertEquals("-0.132", DPCodec.DP3_CODEC.encode(-132L));
    }

    @Test
    public void decode3dpLongs() {
        assertEquals(0L,    DPCodec.DP3_CODEC.decode("0").getResult().longValue());
        assertEquals(0L,    DPCodec.DP3_CODEC.decode("0.0").getResult().longValue());
        assertEquals(13200L,  DPCodec.DP3_CODEC.decode("13.2").getResult().longValue());
        assertEquals(13219L,  DPCodec.DP3_CODEC.decode("13.219").getResult().longValue());
        assertEquals(13210L,  DPCodec.DP3_CODEC.decode("13.210").getResult().longValue());
        assertEquals(13201L,  DPCodec.DP3_CODEC.decode("13.201").getResult().longValue());
        assertEquals(13211L, DPCodec.DP3_CODEC.decode("13.2111").getResult().longValue());
        assertEquals(13211L, DPCodec.DP3_CODEC.decode("13.2114").getResult().longValue());
        assertEquals(13212L, DPCodec.DP3_CODEC.decode("13.2115").getResult().longValue());
        assertEquals(13212L, DPCodec.DP3_CODEC.decode("13.2119").getResult().longValue());
        assertEquals(-13211L, DPCodec.DP3_CODEC.decode("-13.2114").getResult().longValue());
        assertEquals(-13212L, DPCodec.DP3_CODEC.decode("-13.2115").getResult().longValue());
        assertEquals(-13212L, DPCodec.DP3_CODEC.decode("-13.2119").getResult().longValue());

        assertEquals( "java.lang.NumberFormatException: For input string: \"-1234a5\"", DPCodec.DP3_CODEC.decode("-1234a5").getFailure().toString() );
    }

}
