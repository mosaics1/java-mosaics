package mosaics.strings.templates.ast;

import mosaics.strings.parser.CharacterPosition;
import mosaics.strings.templates.ast.nodes.ParameterAST;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class ASTPredicateExtractorTest {

    private TemplateASTParser parser = new TemplateASTParser();


    @Test
    public void walkAnASTWithNoPredicates_expectNoPredicatesToBeFound() {
        TemplateAST ast = parseTemplate(
            "Hello World"
        );

        List<TemplateAST> predicates = ASTPredicateExtractor.extractPredicatesFrom( ast );

        assertTrue( predicates.isEmpty() );
    }

    @Test
    public void walkAnASTWithOneIfStatement_expectOnePredicateToBeFound() {
        TemplateAST ast = parseTemplate(
            "Hello ",
            "#if a",
            "James",
            "#else",
            "#endif"
        );

        List<TemplateAST> predicates = ASTPredicateExtractor.extractPredicatesFrom( ast );
        List<TemplateAST> expectedPredicates = Collections.singletonList(
            new ParameterAST(
                new CharacterPosition("junit", 2, 5, 11),
                "a"
            ).withParameterType( "boolean" )
        );


        assertEquals( expectedPredicates, predicates );
    }

    @Test
    public void walkAnASTWithNestedIfStatements_expectEachPredicateToBeFound() {
        TemplateAST ast = parseTemplate(
            "Hello ",
            "#if a",
            "  #if b",
            "James",
            "  #else",
            "Jenni",
            "  #endif",
            "#else",
            "  #if c",
            "Mags",
            "  #endif",
            "#endif"
        );

        List<TemplateAST> predicates = ASTPredicateExtractor.extractPredicatesFrom( ast );
        List<TemplateAST> expectedPredicates = Arrays.asList(
            new ParameterAST(
                new CharacterPosition( "junit", 2, 5, 11 ),
                "a"
            ).withParameterType( "boolean" ),
            new ParameterAST(
                new CharacterPosition( "junit", 3, 7, 19 ),
                "b"
            ).withParameterType( "boolean" ),
            new ParameterAST(
                new CharacterPosition( "junit", 9, 7, 62 ),
                "c"
            ).withParameterType( "boolean" )
        );


        assertEquals( expectedPredicates, predicates );
    }


    private TemplateAST parseTemplate( String...templateLines ) {
        return parser.parse( "junit", templateLines ).get();
    }

}
