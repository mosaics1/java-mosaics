package mosaics.strings.templates.ast;

import mosaics.fp.FP;
import mosaics.fp.Try;
import mosaics.fp.Tryable;
import mosaics.fp.collections.FPOption;
import mosaics.lang.Backdoor;
import mosaics.strings.parser.CharacterPosition;
import mosaics.strings.parser.ParseException;
import mosaics.strings.templates.ast.nodes.AndAST;
import mosaics.strings.templates.ast.nodes.BlockAST;
import mosaics.strings.templates.ast.nodes.ForEachAST;
import mosaics.strings.templates.ast.nodes.IfBlockAST;
import mosaics.strings.templates.ast.nodes.ParameterAST;
import mosaics.strings.templates.ast.nodes.TextAST;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;


public class TemplateASTParserTest {

    private TemplateASTParser parser = new TemplateASTParser();


    @Nested
    public class PlainTextNodesTestCases {
        @Test
        public void blankTemplate() {
            FPOption<TemplateAST> actual = parseTemplate( "" );
            FPOption<TemplateAST> expected = FP.emptyOption();

            assertASTs( expected, actual );
        }

        @Test
        public void plainTextTemplate() {
            FPOption<TemplateAST> actual = parseTemplate( "today is the day" );

            FPOption<TemplateAST> expected = FP.option(
                new TextAST( pos( 1, 1, 0 ), "today is the day" )
            );

            assertASTs( expected, actual );
        }
    }


    @Nested
    public class ShortFormParameterTestCases { // eg $foo
        @Test
        public void shortFormParameter() {
            FPOption<TemplateAST> actual = parseTemplate( "$name" );

            FPOption<TemplateAST> expected = FP.option(
                new ParameterAST( pos( 1, 1, 0 ), "name" )
            );

            assertASTs( expected, actual );
        }

        @Test
        public void escapedShortFormParameter() {
            FPOption<TemplateAST> actual = parseTemplate( "\\$name" );

            FPOption<TemplateAST> expected = FP.option(
                new TextAST( pos( 1, 1, 0 ), "$name" )
            );

            assertASTs( expected, actual );
        }
    }

    @Nested
    public class longFormParameterTestCases { // eg ${foo} ${foo.a.b.c} ${foo:type} ${foo.b:type+mod1+mod2}
        @Test
        public void longFormParameter() {
            FPOption<TemplateAST> actual = parseTemplate( "${name}" );

            FPOption<TemplateAST> expected = FP.option(
                new ParameterAST( pos( 1, 1, 0 ), "name" )
            );

            assertASTs( expected, actual );
        }

        @Test
        public void longFormParameterWithOnePropertyCall() {
            FPOption<TemplateAST> actual = parseTemplate( "${name.length}" );

            FPOption<TemplateAST> expected = FP.option(
                new ParameterAST( pos( 1, 1, 0 ), "name" )
                    .withChainedProperties( "length" )
            );

            assertASTs( expected, actual );
        }

        @Test
        public void longFormParameterWithTwoPropertyCalls() {
            FPOption<TemplateAST> actual = parseTemplate( "${name.toUpperCase().length}" );

            FPOption<TemplateAST> expected = FP.option(
                new ParameterAST( pos( 1, 1, 0 ), "name" )
                    .withChainedProperties( "toUpperCase", "length" )
            );

            assertASTs( expected, actual );
        }

        @Test
        public void longFormParameterWithType() {
            FPOption<TemplateAST> actual = parseTemplate( "${name:int}" );

            FPOption<TemplateAST> expected = FP.option(
                new ParameterAST( pos( 1, 1, 0 ), "name" )
                    .withParameterType( "int" )
            );

            assertASTs( expected, actual );
        }

        @Test
        public void longFormParameterWithModifier() {
            FPOption<TemplateAST> actual = parseTemplate( "${name:+uc}" );

            FPOption<TemplateAST> expected = FP.option(
                new ParameterAST( pos( 1, 1, 0 ), "name" )
                    .withModifiers( "uc" )
            );

            assertASTs( expected, actual );
        }

        @Test
        public void longFormParameterWithTypeAndTwoModifiers() {
            FPOption<TemplateAST> actual = parseTemplate( "${car.value:millis+rd+gbp}" );

            FPOption<TemplateAST> expected = FP.option(
                new ParameterAST( pos( 1, 1, 0 ), "car" )
                    .withChainedProperties( "value" )
                    .withParameterType( "millis" )
                    .withModifiers( "rd", "gbp" )
            );

            assertASTs( expected, actual );
        }
    }


    @Nested
    public class LongFormTestCasesWithDefaultValues { // eg ${name=DEFAULT}
        @Test
        public void longFormParameterWithDefaultValue() {
            FPOption<TemplateAST> actual = parseTemplate( "${car=red}" );

            FPOption<TemplateAST> expected = FP.option(
                new ParameterAST( pos( 1, 1, 0 ), "car" )
                    .withDefaultValue( "red" )
            );

            assertASTs( expected, actual );
        }

        @Test
        public void longFormParameterWithDefaultValueTypeAndModifiers() {
            FPOption<TemplateAST> actual = parseTemplate( "${car.value=42:millis+rd+gbp}" );

            FPOption<TemplateAST> expected = FP.option(
                new ParameterAST( pos( 1, 1, 0 ), "car" )
                    .withChainedProperties( "value" )
                    .withParameterType( "millis" )
                    .withModifiers( "rd", "gbp" )
                    .withDefaultValue( "42" )
            );

            assertASTs( expected, actual );
        }
    }

    @Nested
    public class LongFormTestCasesWithTrinarySupport { // eg ${a ? hello : goodbye}
        @Test
        public void _trivalue_noDefaultValuePathTypeOrMods() {
            FPOption<TemplateAST> actual = parseTemplate( "${isNew ? Hello : Goodbye}" );

            FPOption<TemplateAST> expected = FP.option(
                new IfBlockAST(
                    pos( 1, 1, 0 ),
                    new ParameterAST( pos( 1, 1, 0 ), "isNew" )
                        .withParameterType( "boolean" ),
                    FP.option(
                        new TextAST( pos( 1, 11, 10 ), "Hello" )
                    ),
                    FP.option(
                        new TextAST( pos( 1, 19, 18 ), "Goodbye" )
                    )
                )
            );

            assertASTs( expected, actual );
        }

        @Test
        public void trivalue_withPropertyPath() {
            FPOption<TemplateAST> actual = parseTemplate( "${car.isNew ? Hello : Goodbye}" );

            FPOption<TemplateAST> expected = FP.option(
                new IfBlockAST(
                    pos( 1, 1, 0 ),
                    new ParameterAST( pos( 1, 1, 0 ), "car" )
                        .withChainedProperties( "isNew" )
                        .withParameterType( "boolean" ),
                    FP.option(
                        new TextAST( pos( 1, 15, 14 ), "Hello" )
                    ),
                    FP.option(
                        new TextAST( pos( 1, 23, 22 ), "Goodbye" )
                    )
                )
            );

            assertASTs( expected, actual );
        }

        @Test
        public void trivalue_givenDefault_expectException() {
            try {
                parseTemplate( "${car.isNew=true ? Hello : Goodbye}" );

                fail( "expected ParseException" );
            } catch ( ParseException ex ) {
                assertEquals( "junit [1,13,12]: default values are not supported with the `? :` operators", ex.getMessage() );
            }
        }

        @Test
        public void trivalue_givenType_expectException() {
            try {
                parseTemplate( "${car.isNew:gbp ? Hello : Goodbye}" );
                fail( "expected ParseException" );
            } catch ( ParseException ex ) {
                assertEquals( "junit [1,13,12]: explicit types are not supported with the `? :` operators", ex.getMessage() );
            }
        }
    }


    @Nested
    public class OneLineIfStatementTestCases {
        @Test
        public void oneLineIfBlock_noPropertyPath() {
            FPOption<TemplateAST> actual = parseTemplate( "[ Hello $name]" );

            FPOption<TemplateAST> expected = FP.option(
                new IfBlockAST(
                    pos( 1, 1, 0 ),
                    new ParameterAST( pos( 1, 9, 8 ), "name" )
                        .withParameterType( "boolean" ),
                    FP.option(
                        new BlockAST(
                            pos( 1, 1, 0 ),
                            new TextAST( pos( 1, 2, 1 ), " Hello " ),
                            new ParameterAST( pos( 1, 9, 8 ), "name" )
                        )
                    ),
                    FP.emptyOption()
                )
            );

            assertASTs( expected, actual );
        }

        @Test
        public void oneLineIfBlock_hasPropertyPath() {
            FPOption<TemplateAST> actual = parseTemplate( "[ Hello ${a.b.name}]" );

            FPOption<TemplateAST> expected = FP.option(
                new IfBlockAST(
                    pos( 1, 1, 0 ),
                    new ParameterAST( pos( 1, 9, 8 ), "a" )
                        .withParameterType( "boolean" )
                        .withChainedProperties( "b", "name" ),
                    FP.option(
                        new BlockAST(
                            pos( 1, 1, 0 ),
                            new TextAST( pos( 1, 2, 1 ), " Hello " ),
                            new ParameterAST( pos( 1, 9, 8 ), "a" )
                                .withChainedProperties( "b", "name" )
                        )
                    ),
                    FP.emptyOption()
                )
            );

            assertASTs( expected, actual );
        }

        @Test
        public void oneLineIfBlock_hasTwoParameters() {
            FPOption<TemplateAST> actual = parseTemplate( "[Hello ${firstName} ${surname},]" );

            FPOption<TemplateAST> expected = FP.option(
                new IfBlockAST(
                    pos( 1, 1, 0 ),
                    new AndAST(
                        pos( 1, 1, 0 ),
                        new ParameterAST( pos( 1, 8, 7 ), "firstName" )
                            .withParameterType( "boolean" ),
                        new ParameterAST( pos( 1, 21, 20 ), "surname" )
                            .withParameterType( "boolean" )
                    ),
                    FP.option(
                        new BlockAST(
                            pos( 1, 1, 0 ),
                            new TextAST( pos( 1, 2, 1 ), "Hello " ),
                            new ParameterAST( pos( 1, 8, 7 ), "firstName" ),
                            new TextAST( pos( 1, 20, 19 ), " " ),
                            new ParameterAST( pos( 1, 21, 20 ), "surname" ),
                            new TextAST( pos( 1, 31, 30 ), "," )
                        )
                    ),
                    FP.emptyOption()
                )
            );

            assertASTs( expected, actual );
        }

        @Test
        public void oneLineIfBlock_hasTwoParameters_oneParameterHasADefaultValue() {
            FPOption<TemplateAST> actual = parseTemplate( "[Hello ${firstName} ${surname=},]" );

            FPOption<TemplateAST> expected = FP.option(
                new IfBlockAST(
                    pos( 1, 1, 0 ),
                    new AndAST(
                        pos( 1, 1, 0 ),
                        new ParameterAST( pos( 1, 8, 7 ), "firstName" )
                            .withParameterType( "boolean" ),
                        new ParameterAST( pos( 1, 21, 20 ), "surname" )
                            .withParameterType( "boolean" )
                            .withDefaultValue( "" )
                    ),
                    FP.option(
                        new BlockAST(
                            pos( 1, 1, 0 ),
                            new TextAST( pos( 1, 2, 1 ), "Hello " ),
                            new ParameterAST( pos( 1, 8, 7 ), "firstName" ),
                            new TextAST( pos( 1, 20, 19 ), " " ),
                            new ParameterAST( pos( 1, 21, 20 ), "surname" )
                                .withDefaultValue( "" ),
                            new TextAST( pos( 1, 32, 31 ), "," )
                        )
                    ),
                    FP.emptyOption()
                )
            );

            assertASTs( expected, actual );
        }

        @Test
        public void oneLineIfBlock_escapeTheEntireBlock_expectConstantStringInstead() {
            FPOption<TemplateAST> actual = parseTemplate( "\\[Hello ${firstName},]" );

            FPOption<TemplateAST> expected = FP.option(
                new BlockAST(
                    pos( 1, 1, 0 ),
                    new TextAST( pos( 1, 1, 0 ), "[Hello " ),
                    new ParameterAST( pos( 1, 8, 7 ), "firstName" ),
                    new TextAST( pos( 1, 20, 19 ), ",]" )
                )
            );

            assertASTs( expected, actual );
        }

        @Test
        public void oneLineIfBlock_hasPropertyPathAndTrinary_expectTheTopLevelConditionalToBeSkippedAsTheTrinaryWillAlwaysGiveAValue() {
            FPOption<TemplateAST> actual = parseTemplate( "[ Hello ${a.b.name ? A : B}]" );

            FPOption<TemplateAST> expected = FP.option(
                new IfBlockAST(
                    pos( 1, 1, 0 ),
                    new ParameterAST( pos( 1, 9, 8 ), "a" )
                        .withChainedProperties( "b", "name" )
                        .withParameterType( "boolean" ),
                    FP.option(
                        new BlockAST(
                            pos( 1, 1, 0 ),
                            new TextAST( pos( 1, 2, 1 ), " Hello " ),
                            new IfBlockAST(
                                pos( 1, 9, 8 ),
                                new ParameterAST( pos( 1, 9, 8 ), "a" )
                                    .withChainedProperties( "b", "name" )
                                    .withParameterType( "boolean" ),
                                FP.option( new TextAST( pos( 1, 22, 21 ), "A" ) ),
                                FP.option( new TextAST( pos( 1, 26, 25 ), "B" ) )
                            )
                        )
                    ),
                    FP.emptyOption()
                )
            );

            assertASTs( expected, actual );
        }
    }


    @Nested
    public class IfStatementTestCases {
        @Test
        public void ifBlock_noPropertyPath() {
            FPOption<TemplateAST> actual = parseTemplate(
                "#if x",
                "  Welcome $x",
                "#endif"
            );

            FPOption<TemplateAST> expected = FP.option(
                new IfBlockAST(
                    pos( 1, 1, 0 ),
                    new ParameterAST( pos( 1, 5, 4 ), "x" )
                        .withParameterType( "boolean" ),
                    FP.option(
                        new BlockAST(
                            pos( 2, 1, 6 ),
                            new TextAST( pos( 2, 1, 6 ), "  Welcome " ),
                            new ParameterAST( pos( 2, 11, 16 ), "x" ),
                            new TextAST( pos( 2, 13, 18 ), Backdoor.NEWLINE )
                        )
                    ),
                    FP.emptyOption()
                )
            );

            assertASTs( expected, actual );
        }

        @Test
        public void ifBlock_hasPropertyPath() {
            FPOption<TemplateAST> actual = parseTemplate(
                "#if x.hasDamage",
                "Ship Damaged",
                "Return to base",
                "#endif"
            );

            FPOption<TemplateAST> expected = FP.option(
                new IfBlockAST(
                    pos( 1, 1, 0 ),
                    new ParameterAST( pos( 1, 5, 4 ), "x" )
                        .withChainedProperties( "hasDamage" )
                        .withParameterType( "boolean" ),
                    FP.option(
                        new TextAST( pos( 2, 1, 16 ), "Ship Damaged" + Backdoor.NEWLINE + "Return to base" + Backdoor.NEWLINE )
                    ),
                    FP.emptyOption()
                )
            );

            assertASTs( expected, actual );
        }

        @Test
        public void ifElseIf_noPropertyPath() {
            FPOption<TemplateAST> actual = parseTemplate(
                "#if x",
                "  Welcome $x",
                "#else if y",
                "  Hello $y",
                "#endif"
            );

            FPOption<TemplateAST> expected = FP.option(
                new IfBlockAST(
                    pos( 1, 1, 0 ),
                    new ParameterAST( pos( 1, 5, 4 ), "x" )
                        .withParameterType( "boolean" ),
                    FP.option(
                        new BlockAST(
                            pos( 2, 1, 6 ),
                            new TextAST( pos( 2, 1, 6 ), "  Welcome " ),
                            new ParameterAST( pos( 2, 11, 16 ), "x" ),
                            new TextAST( pos( 2, 13, 18 ), Backdoor.NEWLINE )
                        )
                    ),
                    FP.option(
                        new IfBlockAST(
                            pos( 3, 1, 19 ),
                            new ParameterAST( pos( 3, 10, 28 ), "y" )
                                .withParameterType( "boolean" ),
                            FP.option(
                                new BlockAST(
                                    pos( 4, 1, 30 ),
                                    new TextAST( pos( 4, 1, 30 ), "  Hello " ),
                                    new ParameterAST( pos( 4, 9, 38 ), "y" ),
                                    new TextAST( pos( 4, 11, 40 ), Backdoor.NEWLINE )
                                )
                            ),
                            FP.emptyOption()
                        )
                    )
                )
            );

            assertASTs( expected, actual );
        }

        @Test
        public void ifElseIfElseIf_noPropertyPath() {
            FPOption<TemplateAST> actual = parseTemplate(
                "#if x",
                "  Welcome $x",
                "#else if y",
                "  Hello $y",
                "#else if z",
                "  Hola $z",
                "#endif"
            );

            FPOption<TemplateAST> expected = FP.option(
                new IfBlockAST(
                    pos( 1, 1, 0 ),
                    new ParameterAST( pos( 1, 5, 4 ), "x" )
                        .withParameterType( "boolean" ),
                    FP.option(
                        new BlockAST(
                            pos( 2, 1, 6 ),
                            new TextAST( pos( 2, 1, 6 ), "  Welcome " ),
                            new ParameterAST( pos( 2, 11, 16 ), "x" ),
                            new TextAST( pos( 2, 13, 18 ), Backdoor.NEWLINE )
                        )
                    ),
                    FP.option(
                        new IfBlockAST(
                            pos( 3, 1, 19 ),
                            new ParameterAST( pos( 3, 10, 28 ), "y" )
                                .withParameterType( "boolean" ),
                            FP.option(
                                new BlockAST(
                                    pos( 4, 1, 30 ),
                                    new TextAST( pos( 4, 1, 30 ), "  Hello " ),
                                    new ParameterAST( pos( 4, 9, 38 ), "y" ),
                                    new TextAST( pos( 4, 11, 40 ), Backdoor.NEWLINE )
                                )
                            ),
                            FP.option(
                                new IfBlockAST(
                                    pos( 5, 1, 41 ),
                                    new ParameterAST( pos( 5, 10, 50 ), "z" )
                                        .withParameterType( "boolean" ),
                                    FP.option(
                                        new BlockAST(
                                            pos( 6, 1, 52 ),
                                            new TextAST( pos( 6, 1, 52 ), "  Hola " ),
                                            new ParameterAST( pos( 6, 8, 59 ), "z" ),
                                            new TextAST( pos( 6, 10, 61 ), Backdoor.NEWLINE )
                                        )
                                    ),
                                    FP.emptyOption()
                                )
                            )
                        )
                    )
                )
            );

            assertASTs( expected, actual );
        }

        @Test
        public void ifElseBlock() {
            FPOption<TemplateAST> actual = parseTemplate(
                "#if x",
                "  Welcome $x",
                "#else",
                "  Goodbye $x",
                "#endif"
            );

            FPOption<TemplateAST> expected = FP.option(
                new IfBlockAST(
                    pos( 1, 1, 0 ),
                    new ParameterAST( pos( 1, 5, 4 ), "x" )
                        .withParameterType( "boolean" ),
                    FP.option(
                        new BlockAST(
                            pos( 2, 1, 6 ),
                            new TextAST( pos( 2, 1, 6 ), "  Welcome " ),
                            new ParameterAST( pos( 2, 11, 16 ), "x" ),
                            new TextAST( pos( 2, 13, 18 ), Backdoor.NEWLINE )
                        )
                    ),
                    FP.option(
                        new BlockAST(
                            pos( 4, 1, 25 ),
                            new TextAST( pos( 4, 1, 25 ), "  Goodbye " ),
                            new ParameterAST( pos( 4, 11, 35 ), "x" ),
                            new TextAST( pos( 4, 13, 37 ), Backdoor.NEWLINE )
                        )
                    )
                )
            );

            assertASTs( expected, actual );
        }

        @Test
        public void nestedIfBlocks() {
            FPOption<TemplateAST> actual = parseTemplate(
                "#if x",
                "  #if y",
                "    Welcome $x",
                "  #else",
                "    Goodbye",
                "  #endif",
                "#endif"
            );

            FPOption<TemplateAST> expected = FP.option(
                new IfBlockAST(
                    pos( 1, 1, 0 ),
                    new ParameterAST( pos( 1, 5, 4 ), "x" )
                        .withParameterType( "boolean" ),
                    FP.option(
                        new IfBlockAST(
                            pos( 2, 3, 8 ),
                            new ParameterAST( pos( 2, 7, 12 ), "y" )
                                .withParameterType( "boolean" ),
                            FP.option(
                                new BlockAST(
                                    pos( 3, 1, 14 ),
                                    new TextAST( pos( 3, 1, 14 ), "    Welcome " ),
                                    new ParameterAST( pos( 3, 13, 26 ), "x" ),
                                    new TextAST( pos( 3, 15, 28 ), Backdoor.NEWLINE )
                                )
                            ),
                            FP.option(
                                new TextAST( pos( 5, 1, 37 ), "    Goodbye" + Backdoor.NEWLINE )
                            )
                        )
                    ),
                    FP.emptyOption()
                )
            );

            assertASTs( expected, actual );
        }
    }


    @Nested
    public class ForStatementTestCases {
        @Test
        public void givenDisabledForBlockSupport_forBlock_expectParseFailure() {
            parser = parser.withDisableFeatures( TemplateParserFeature.FOREACH_SUPPORT );

            try {
                parseTemplate(
                    "#for x in guestList",
                    "Hello $x",
                    "#endfor"
                );
            } catch ( ParseException ex ) {
                assertEquals( "junit [1,1,0]: Unknown directive: #for", ex.getMessage() );
            }
        }

        @Test
        public void forBlock_noPropertyPath() {
            FPOption<TemplateAST> actual = parseTemplate(
                "#for x in guestList",
                "Hello $x",
                "#endfor"
            );

            FPOption<TemplateAST> expected = FP.option(
                new ForEachAST(
                    pos( 1, 1, 0 ),
                    "x",
                    new ParameterAST( pos( 1, 11, 10 ), "guestList" ),
                    new BlockAST(
                        pos( 2, 1, 20 ),
                        new TextAST( pos( 2, 1, 20 ), "Hello " ),
                        new ParameterAST( pos( 2, 7, 26 ), "x" ),
                        new TextAST( pos( 2, 9, 28 ), Backdoor.NEWLINE )
                    )
                )
            );

            assertASTs( expected, actual );
        }
    }

    @Nested
    public class StaticTextBlockTestCases {
        @Test
        public void ifBlock_noPropertyPath() {
            FPOption<TemplateAST> actual = parseTemplate(
                "#static",
                "[{Welcome $x}]",
                "#endstatic"
            );

            FPOption<TemplateAST> expected = FP.option(
                new TextAST( pos( 2, 1, 8 ), "[{Welcome $x}]"+Backdoor.NEWLINE )
            );

            assertASTs( expected, actual );
        }
    }

    @Nested
    public class IncludeStaticTestCases {
        @Test
        public void includeStatic_givenMissingSpec_expectError() {
            Tryable<FPOption<TemplateAST>> result = Try.invoke( () ->
                parseTemplate(
                    "#include-static"
                )
            );

            assertEquals( ParseException.class, result.getFailure().toException().getClass() );
            assertEquals( "junit [1,1,0]: #include-static requires a classpath resource path", result.getFailure().toException().getMessage() );
        }

        @Test
        public void includeStatic_givenMissingResource_expectError() {
            Tryable<FPOption<TemplateAST>> result = Try.invoke( () ->
                parseTemplate(
                    "#include-static /mosaics/strings/templates/ast/TemplateASTParserTest.missing.txt"
                )
            );

            assertEquals( ParseException.class, result.getFailure().toException().getClass() );
            assertEquals( "junit [1,1,0]: Unable to find resource '/mosaics/strings/templates/ast/TemplateASTParserTest.missing.txt' on the classpath.", result.getFailure().toException().getMessage() );
        }

        @Test
        public void includeStatic_givenValidResourceSpec_expectFileContentsToBeLoadedAndNotInterpreted() {
            FPOption<TemplateAST> actual = parseTemplate(
                "#include-static /mosaics/strings/templates/ast/TemplateASTParserTest.includeStatic_givenValidResourceSpec_expectFileContentsToBeLoadedAndNotInterpreted.txt"
            );

            FPOption<TemplateAST> expected = FP.option(
                new TextAST( pos( 1, 1, 0 ), "Hello ${greeting}"+Backdoor.NEWLINE )
            );

            assertASTs( expected, actual );
        }
    }

    private CharacterPosition pos( int line, int col, int offset ) {
        return new CharacterPosition( "junit", line, col, offset );
    }

    private FPOption<TemplateAST> parseTemplate( String...templateLines ) {
        return parser.parse( "junit", templateLines );
    }

    private void assertASTs( FPOption<TemplateAST> expected, FPOption<TemplateAST> actual ) {
        if ( expected.equals(actual) || expected.isEmpty() || actual.isEmpty() ) {
            assertEquals( expected, actual );

            return;
        }

        assertEquals( TemplateASTPrinter.toStringWithPositions(expected.get()), TemplateASTPrinter.toStringWithPositions(actual.get()) );
        assertEquals( expected, actual );
    }

}

