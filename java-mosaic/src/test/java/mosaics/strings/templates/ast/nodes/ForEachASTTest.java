package mosaics.strings.templates.ast.nodes;

import mosaics.strings.parser.CharacterPosition;
import mosaics.strings.templates.ast.TemplateAST;
import mosaics.strings.templates.ast.TemplateASTWalker;
import mosaics.strings.templates.ast.TemplateContext;
import mosaics.fp.FP;
import mosaics.fp.collections.FPOption;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;


public class ForEachASTTest {

    private CharacterPosition pos = new CharacterPosition( "junit" );
    private TemplateContext   ctx = new TemplateContext();


    @Test
    public void walkAST_expectWalkForEachASTCallback() {
        TemplateAST expression = new AlreadyOptimalASTMock(pos);
        TemplateAST block      = new AlreadyOptimalASTMock(pos);
        TemplateASTWalker walkerMock = Mockito.mock(TemplateASTWalker.class);
        ForEachAST        ast        = new ForEachAST( pos, "x", expression, block );

        ast.walkAST( walkerMock );

        Mockito.verify(walkerMock).walkForEachAST(pos, "x", expression, block);
    }

    @Test
    public void givenExpressionOptimisesItSelfOut_optimise_expectRemovalOfForEach() {
        TemplateAST expression = new RemovesSelfASTMock(pos);
        TemplateAST block      = new AlreadyOptimalASTMock(pos);
        ForEachAST   ast        = new ForEachAST( pos, "x", expression, block );

        assertEquals( FP.emptyOption(), ast.optimiseSelf(ctx) );
    }

    @Test
    public void givenBlockOptimisesItSelfOut_optimise_expectRemovalOfForEach() {
        TemplateAST expression = new AlreadyOptimalASTMock(pos);
        TemplateAST block      = new RemovesSelfASTMock(pos);
        ForEachAST   ast        = new ForEachAST( pos, "x", expression, block );

        assertEquals( FP.emptyOption(), ast.optimiseSelf(ctx) );
    }

    @Test
    public void givenExpressionThatOptimisesToDifferentInstance_optimise_expectOptimisedForEach() {
        TemplateAST expression = new OptimisesSelfASTMock(pos);
        TemplateAST block      = new AlreadyOptimalASTMock(pos);
        ForEachAST   ast        = new ForEachAST( pos, "x", expression, block );

        FPOption<TemplateAST> expected = FP.option(
            new ForEachAST( pos, "x", new OptimisesSelfASTMock(pos,2), block )
        );

        assertEquals( expected, ast.optimiseSelf(ctx) );
    }

    @Test
    public void givenBlockThatOptimisesToDifferentInstance_optimise_expectOptimisedForEach() {
        TemplateAST expression = new AlreadyOptimalASTMock(pos);
        TemplateAST block      = new OptimisesSelfASTMock(pos);
        ForEachAST   ast        = new ForEachAST( pos, "x", expression, block );

        FPOption<TemplateAST> expected = FP.option(
            new ForEachAST( pos, "x", expression, new OptimisesSelfASTMock(pos,2) )
        );

        assertEquals( expected, ast.optimiseSelf(ctx) );
    }

    @Test
    public void givenExpressionAndBlockOptimisesToSelf_optimise_expectOriginalASTObjectToBeReused() {
        TemplateAST expression = new AlreadyOptimalASTMock(pos);
        TemplateAST block      = new AlreadyOptimalASTMock(pos);
        ForEachAST   ast        = new ForEachAST( pos, "x", expression, block );

        assertSame(ast, ast.optimiseSelf(ctx).get() );
    }

}
