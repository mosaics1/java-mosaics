package mosaics.strings.templates.ast.nodes;

import mosaics.strings.parser.CharacterPosition;
import mosaics.strings.templates.ast.TemplateAST;
import mosaics.strings.templates.ast.TemplateASTWalker;
import mosaics.strings.templates.ast.TemplateContext;
import mosaics.fp.FP;
import mosaics.fp.collections.FPOption;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;


public class AndASTTest {

    private CharacterPosition pos = new CharacterPosition( "junit" );
    private TemplateContext   ctx = new TemplateContext();


    @Test
    public void walkAST_expectWalkAndASTCallback() {
        List<TemplateAST> predicates = Collections.emptyList();
        TemplateASTWalker  walkerMock = Mockito.mock(TemplateASTWalker.class);
        AndAST             ast        = new AndAST( pos, predicates );

        ast.walkAST( walkerMock );

        Mockito.verify(walkerMock).walkAndAST(pos, predicates);
    }

    @Test
    public void givenEmptyPredicateList_optimise_expectRemovalOfAndBlock() {
        List<TemplateAST> predicates = Collections.emptyList();
        AndAST             ast        = new AndAST( pos, predicates );

        assertEquals( FP.emptyOption(), ast.optimiseSelf(ctx) );
    }

    @Test
    public void givenOnePredicate_optimise_expectRemovalOfAndAST() {
        AlreadyOptimalASTMock predicate  = new AlreadyOptimalASTMock( pos );
        List<TemplateAST>    predicates = Collections.singletonList( predicate );
        AndAST                ast        = new AndAST( pos, predicates );

        assertSame( predicate, ast.optimiseSelf(ctx).get() );
    }

    @Test
    public void givenPredicateListThatOptimisedToEmptyList_optimise_expectRemovalOfAndBlock() {
        List<TemplateAST> predicates = Arrays.asList( new RemovesSelfASTMock(pos), new RemovesSelfASTMock(pos) );
        AndAST             ast        = new AndAST( pos, predicates );

        assertEquals( FP.emptyOption(), ast.optimiseSelf(ctx) );
    }

    @Test
    public void givenPredicateListThatOptimisesToDifferentList_optimise_expectOptimisedAndBlock() {
        List<TemplateAST> predicates = Arrays.asList( new OptimisesSelfASTMock(pos), new RemovesSelfASTMock(pos) );
        AndAST             ast        = new AndAST( pos, predicates );

        FPOption<TemplateAST> expected = FP.option( new OptimisesSelfASTMock(pos,2) );

        assertEquals( expected, ast.optimiseSelf(ctx) );
    }

    @Test
    public void givenPredicateListThatOptimisesToSelf_optimise_expectOriginalASTObjectToBeReused() {
        List<TemplateAST> predicates = Arrays.asList( new AlreadyOptimalASTMock(pos), new AlreadyOptimalASTMock(pos) );
        AndAST             ast        = new AndAST( pos, predicates );

        assertSame(ast, ast.optimiseSelf(ctx).get() );
    }

}
