package mosaics.strings.templates.ast.nodes;

import mosaics.fp.FP;
import mosaics.fp.collections.FPOption;
import mosaics.strings.parser.CharacterPosition;
import mosaics.strings.templates.ast.TemplateAST;
import mosaics.strings.templates.ast.TemplateASTWalker;
import mosaics.strings.templates.ast.TemplateContext;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Value;


@Value
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
class OptimisesSelfASTMock extends TemplateAST {
    private CharacterPosition pos;
    private int               ver;

    public OptimisesSelfASTMock( CharacterPosition pos ) {
        this(pos,1);
    }

    public void walkAST( TemplateASTWalker walker ) {
        throw new UnsupportedOperationException();
    }

    public FPOption<TemplateAST> optimiseSelf( TemplateContext ctx ) {
        return FP.option(new OptimisesSelfASTMock(pos,ver+1));
    }
}
