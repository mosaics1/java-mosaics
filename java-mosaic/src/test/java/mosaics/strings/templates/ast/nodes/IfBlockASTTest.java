package mosaics.strings.templates.ast.nodes;

import mosaics.strings.parser.CharacterPosition;
import mosaics.strings.templates.ast.TemplateAST;
import mosaics.strings.templates.ast.TemplateASTWalker;
import mosaics.strings.templates.ast.TemplateContext;
import mosaics.fp.FP;
import mosaics.fp.collections.FPOption;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;


public class IfBlockASTTest {

    private CharacterPosition pos = new CharacterPosition( "junit" );
    private TemplateContext   ctx = new TemplateContext();


    @Test
    public void walkAST_expectWalkIfBlockASTCallback() {
        TemplateAST predicate   = new AlreadyOptimalASTMock(pos);
        FPOption<TemplateAST> ifTrueBlock = FP.option( new AlreadyOptimalASTMock(pos) );
        FPOption<TemplateAST> elseBlock   = FP.option( new AlreadyOptimalASTMock(pos) );
        TemplateASTWalker      walkerMock  = Mockito.mock(TemplateASTWalker.class);
        IfBlockAST             ast         = new IfBlockAST( pos, predicate, ifTrueBlock, elseBlock );

        ast.walkAST( walkerMock );

        Mockito.verify(walkerMock).walkIfBlockAST(pos, predicate, ifTrueBlock, elseBlock);
    }

    @Test
    public void givenExpressionOptimisesItSelfOut_optimise_expectRemovalOfIfTrueBlockAndTheExpression() {
        TemplateAST expression   = new RemovesSelfASTMock(pos);
        FPOption<TemplateAST> ifTrueBlock = FP.option( new AlreadyOptimalASTMock(pos) );
        FPOption<TemplateAST> elseBlock   = FP.option( new AlreadyOptimalASTMock(pos) );
        IfBlockAST             ast         = new IfBlockAST( pos, expression, ifTrueBlock, elseBlock );

        assertEquals( elseBlock, ast.optimiseSelf(ctx) );
    }

    @Test
    public void givenIfTrueBlockOptimisesItSelfOut_optimise_expectRemovalOfIfTrueBlock() {
        TemplateAST expression   = new AlreadyOptimalASTMock(pos);
        FPOption<TemplateAST> ifTrueBlock = FP.option( new RemovesSelfASTMock(pos) );
        FPOption<TemplateAST> elseBlock   = FP.option( new AlreadyOptimalASTMock(pos) );
        IfBlockAST             ast         = new IfBlockAST( pos, expression, ifTrueBlock, elseBlock );

        FPOption<TemplateAST> expected = FP.option(
            new IfBlockAST( pos, expression, FP.emptyOption(), elseBlock )
        );

        assertEquals( expected, ast.optimiseSelf(ctx) );
    }

    @Test
    public void givenElseBlockOptimisesItSelfOut_optimise_expectRemovalOfElseBlock() {
        TemplateAST expression   = new AlreadyOptimalASTMock(pos);
        FPOption<TemplateAST> ifTrueBlock = FP.option( new AlreadyOptimalASTMock(pos) );
        FPOption<TemplateAST> elseBlock   = FP.option( new RemovesSelfASTMock(pos) );
        IfBlockAST             ast         = new IfBlockAST( pos, expression, ifTrueBlock, elseBlock );

        FPOption<TemplateAST> expected = FP.option(
            new IfBlockAST( pos, expression, ifTrueBlock, FP.emptyOption() )
        );

        assertEquals( expected, ast.optimiseSelf(ctx) );
    }

    @Test
    public void givenPredicateThatOptimisesToDifferentInstance_optimise_expectOptimisedIfBlock() {
        TemplateAST predicate   = new OptimisesSelfASTMock(pos);
        FPOption<TemplateAST> ifTrueBlock = FP.option( new AlreadyOptimalASTMock(pos) );
        FPOption<TemplateAST> elseBlock   = FP.option( new AlreadyOptimalASTMock(pos) );
        IfBlockAST             ast         = new IfBlockAST( pos, predicate, ifTrueBlock, elseBlock );

        FPOption<TemplateAST> expected = FP.option(
            new IfBlockAST( pos, new OptimisesSelfASTMock(pos,2), ifTrueBlock, elseBlock )
        );

        assertEquals( expected, ast.optimiseSelf(ctx) );
    }

    @Test
    public void givenIfTrueBlockThatOptimisesToDifferentInstance_optimise_expectOptimisedIfBlock() {
        TemplateAST predicate   = new AlreadyOptimalASTMock(pos);
        FPOption<TemplateAST> ifTrueBlock = FP.option( new OptimisesSelfASTMock(pos) );
        FPOption<TemplateAST> elseBlock   = FP.option( new AlreadyOptimalASTMock(pos) );
        IfBlockAST             ast         = new IfBlockAST( pos, predicate, ifTrueBlock, elseBlock );

        FPOption<TemplateAST> expected = FP.option(
            new IfBlockAST( pos, predicate, FP.option(new OptimisesSelfASTMock(pos,2)), elseBlock )
        );

        assertEquals( expected, ast.optimiseSelf(ctx) );
    }

    @Test
    public void givenElseTrueBlockThatOptimisesToDifferentInstance_optimise_expectOptimisedIfBlock() {
        TemplateAST predicate   = new AlreadyOptimalASTMock(pos);
        FPOption<TemplateAST> ifTrueBlock = FP.option( new AlreadyOptimalASTMock(pos) );
        FPOption<TemplateAST> elseBlock   = FP.option( new OptimisesSelfASTMock(pos) );
        IfBlockAST             ast         = new IfBlockAST( pos, predicate, ifTrueBlock, elseBlock );

        FPOption<TemplateAST> expected = FP.option(
            new IfBlockAST( pos, predicate, ifTrueBlock, FP.option(new OptimisesSelfASTMock(pos,2)) )
        );

        assertEquals( expected, ast.optimiseSelf(ctx) );
    }

    @Test
    public void givenAllPartsOptimisesToSelf_optimise_expectOriginalASTObjectToBeReused() {
        TemplateAST predicate   = new AlreadyOptimalASTMock(pos);
        FPOption<TemplateAST> ifTrueBlock = FP.option( new AlreadyOptimalASTMock(pos) );
        FPOption<TemplateAST> elseBlock   = FP.option( new AlreadyOptimalASTMock(pos) );
        IfBlockAST             ast         = new IfBlockAST( pos, predicate, ifTrueBlock, elseBlock );

        assertSame(ast, ast.optimiseSelf(ctx).get() );
    }

    @Test
    public void givenIfTrueAndElseBlockOptimiseThemSelvesOut_optimise_expectNoIfBlock() {
        TemplateAST predicate   = new AlreadyOptimalASTMock(pos);
        FPOption<TemplateAST> ifTrueBlock = FP.option( new RemovesSelfASTMock(pos) );
        FPOption<TemplateAST> elseBlock   = FP.option( new RemovesSelfASTMock(pos) );
        IfBlockAST             ast         = new IfBlockAST( pos, predicate, ifTrueBlock, elseBlock );

        assertSame(FP.emptyOption(), ast.optimiseSelf(ctx) );
    }

    @Test
    public void givenIfTrueAndElseBlockAreMissing_optimise_expectNoIfBlock() {
        TemplateAST predicate   = new AlreadyOptimalASTMock(pos);
        FPOption<TemplateAST> ifTrueBlock = FP.emptyOption();
        FPOption<TemplateAST> elseBlock   = FP.emptyOption();
        IfBlockAST             ast         = new IfBlockAST( pos, predicate, ifTrueBlock, elseBlock );

        assertSame(FP.emptyOption(), ast.optimiseSelf(ctx) );
    }

}
