package mosaics.strings.templates.ast.nodes;

import mosaics.strings.parser.CharacterPosition;
import mosaics.strings.templates.ast.TemplateAST;
import mosaics.strings.templates.ast.TemplateASTWalker;
import mosaics.strings.templates.ast.TemplateContext;
import mosaics.fp.FP;
import mosaics.fp.collections.FPOption;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;


public class BlockASTTest {

    private CharacterPosition pos = new CharacterPosition( "junit" );
    private TemplateContext   ctx = new TemplateContext();


    @Test
    public void walkAST_expectWalkBlockASTCallback() {
        List<TemplateAST> predicates = Collections.emptyList();
        TemplateASTWalker  walkerMock = Mockito.mock(TemplateASTWalker.class);
        BlockAST           ast        = new BlockAST( pos, predicates );

        ast.walkAST( walkerMock );

        Mockito.verify(walkerMock).walkBlockAST(pos, predicates);
    }

    @Test
    public void givenEmptyPredicateList_optimise_expectRemovalOfBlock() {
        List<TemplateAST> predicates = Collections.emptyList();
        BlockAST           ast        = new BlockAST( pos, predicates );

        assertEquals( FP.emptyOption(), ast.optimiseSelf(ctx) );
    }

    @Test
    public void givenPredicateListThatOptimisedToEmptyList_optimise_expectRemovalOfBlock() {
        List<TemplateAST> predicates = Arrays.asList( new RemovesSelfASTMock(pos), new RemovesSelfASTMock(pos) );
        BlockAST           ast        = new BlockAST( pos, predicates );

        assertEquals( FP.emptyOption(), ast.optimiseSelf(ctx) );
    }

    @Test
    public void givenPredicateListThatOptimisesToDifferentList_optimise_expectOptimisedBlock() {
        List<TemplateAST> predicates = Arrays.asList( new OptimisesSelfASTMock(pos), new RemovesSelfASTMock(pos) );
        BlockAST           ast        = new BlockAST( pos, predicates );

        FPOption<TemplateAST> expected = FP.option(new OptimisesSelfASTMock(pos,2));

        assertEquals( expected, ast.optimiseSelf(ctx) );
    }

    @Test
    public void givenPredicateListThatOptimisesToSelf_optimise_expectOriginalASTObjectToBeReused() {
        List<TemplateAST> predicates = Arrays.asList( new AlreadyOptimalASTMock(pos), new AlreadyOptimalASTMock(pos) );
        BlockAST           ast        = new BlockAST( pos, predicates );

        assertSame(ast, ast.optimiseSelf(ctx).get() );
    }

    @Test
    public void givenPredicateThatContainsOneElementAfterOptimisation_optimise_expectBlockToBeReplacedByTheSingleChildElement() {
        List<TemplateAST> predicates = Collections.singletonList( new OptimisesSelfASTMock( pos ) );
        BlockAST           ast        = new BlockAST( pos, predicates );

        assertEquals(new OptimisesSelfASTMock(pos,2), ast.optimiseSelf(ctx).get() );
    }


// MERGE MERGEABLE CHILDREN

    @Test
    public void givenTwoTextNodesNextToEachOther_optimise_expectThemToBeCombinedTogether() {
        List<TemplateAST> predicates = Arrays.asList( new TextAST(pos,"ab"), new TextAST(pos,"cd") );
        BlockAST           ast        = new BlockAST( pos, predicates );

        assertEquals(new TextAST(pos,"abcd"), ast.optimiseSelf(ctx).get() );
    }

}
