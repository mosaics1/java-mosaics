package mosaics.strings.templates.ast.nodes;

import mosaics.strings.parser.CharacterPosition;
import mosaics.strings.templates.ast.TemplateASTWalker;
import mosaics.strings.templates.ast.TemplateContext;
import mosaics.fp.FP;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;


public class TextASTTest {

    private CharacterPosition pos = new CharacterPosition( "junit" );
    private TemplateContext   ctx = new TemplateContext();


    @Test
    public void walkAST_expectWalkTextASTCallback() {
        TemplateASTWalker walkerMock = Mockito.mock(TemplateASTWalker.class);
        TextAST           ast        = new TextAST( pos, "text" );

        ast.walkAST( walkerMock );

        Mockito.verify(walkerMock).walkTextAST(pos, "text");
    }

    @Test
    public void givenNonBlankText_optimise_expectNoChange() {
        TextAST ast = new TextAST( pos, "text" );

        assertSame( ast, ast.optimiseSelf(ctx).get() );
    }

    @Test
    public void givenEmptyText_optimise_expectNoChangeOptimisationToRemoveNode() {
        TextAST ast = new TextAST( pos, "" );

        assertEquals( FP.emptyOption(), ast.optimiseSelf(ctx) );
    }

    @Test
    public void givenWhiteText_optimise_expectNoChange() {
        TextAST ast = new TextAST( pos, "    " );

        assertSame( ast, ast.optimiseSelf(ctx).get() );
    }
    
}
