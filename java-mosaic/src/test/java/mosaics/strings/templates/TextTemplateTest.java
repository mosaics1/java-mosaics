package mosaics.strings.templates;

import mosaics.fp.collections.FPOption;
import mosaics.lang.Backdoor;
import mosaics.lang.time.DTMUtils;
import mosaics.strings.StringFormatter;
import mosaics.strings.StringFormatters;
import mosaics.strings.StringUtils;
import mosaics.strings.parser.CharacterIterators;
import mosaics.strings.parser.ParseException;
import mosaics.utils.MapUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

import static mosaics.junit.JMAssertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;


public class TextTemplateTest {

    private TextTemplateParser parser = new TextTemplateParser();


    @Nested
    public class PlainTextTestCases {
        @Test
        public void givenNullTemplate_expectNPE() {
            try {
                parser.parse( "templateName", (String) null );
                fail( "expect IAE" );
            } catch ( IllegalArgumentException ex ) {
                assertEquals( "'text' must not be null", ex.getMessage() );
            }
        }

        @Test
        public void givenEmptyTemplate_expectEmptyResult() {
            String output = processTemplate( "" );

            assertEquals( "", output );
        }

        @Test
        public void givenNonEmptyPlainTextTemplate_expectStraightPassThroughOfText() {
            String output = processTemplate( "hello world" );

            assertEquals( "hello world", output );
        }

        @Test
        public void callGetTemplatePropertyNames_expectEmptySet() {
            TextTemplate template = parser.parse( "templateName", "hello world" );

            assertEquals( Set.of(), template.getTemplatePropertyNames() );
        }
    }


    @Nested
    public class StaticTextTestCases {
        @Test
        public void givenNonEmptyPlainTextTemplate_expectStraightPassThroughOfText() {
            String output = parseTemplate(
                "#static",
                "#if [abc 123]$abc",
                "#endstatic"
            ).invokeWith().result;

            assertEquals( "#if [abc 123]$abc"+Backdoor.NEWLINE, output );
        }
    }


    @Nested
    public class IncludeStaticTextTestCases {
        @Test
        public void givenNonEmptyPlainTextTemplate_expectStraightPassThroughOfText() {
            String output = parseTemplate(
                "#include-static /mosaics/strings/templates/TextTemplateTest.givenNonEmptyPlainTextTemplate_expectStraightPassThroughOfText.txt"
            ).invokeWith().result;

            assertEquals( "#[abc 123]$abc"+Backdoor.NEWLINE, output );
        }
    }

    @Nested
    public class ShortFormParameterTestCases { // $foo
// todo
        // shortFormParameter_givenJust$_expectException
        // shortFormParameter_given$AtEndOfTemplate_expectException
        // shortFormParameter_given$WithNoName_expectException

        @Test
        public void shortFormParameter_givenSingleParameterWithMissingValue_expectTheTemplateToError() {
            TextTemplate template = parser.parse( "templateName", "$missingParameter" );

            try {
                template.invoke();
                fail( "expect MissingParameterException" );
            } catch ( MissingParameterException ex ) {
                assertEquals( "templateName [1,1,0]: Missing value for 'missingParameter'", ex.getMessage() );
            }
        }

        @Test
        public void shortFormParameter_givenValue_expectValueToBeReturned() {
            String output = processTemplate( "$name", "name", "Jim" );

            assertEquals( "Jim", output );
        }

        @Test
        public void shortFormParameter_givenConstantBeforeStringValue() {
            String output = processTemplate( "Hello $name", "name", "Jim" );

            assertEquals( "Hello Jim", output );
        }

        @Test
        public void shortFormParameter_givenConstantAfterStringValue() {
            String output = processTemplate( "$name Doe", "name", "John" );

            assertEquals( "John Doe", output );
        }

        @Test
        public void shortFormParameter_givenDoubleValue_expectOneOutParameterToBeDeclared() {
            String output = processTemplate( "$name", "name", 0.13 );

            assertEquals( "0.13", output );
        }

        @Test
        public void shortFormParameter_commentedOutParameter_expectNoSubstitution() {
            String output = processTemplate( "\\$name", "name", 0.13 );

            assertEquals( "$name", output );
        }

        @Test
        public void withOneVariable_callGetTemplatePropertyNames_expectOneParameterName() {
            TextTemplate template = parser.parse( "templateName", "hello $name" );

            assertEquals( Set.of("name"), template.getTemplatePropertyNames() );
        }

        @Test
        public void givenDollarWithNoParameterName_tryToParse_expectExceptionWithMeaningfulDescription() {
            assertThrows(
                ParseException.class,
                "templateName [1,7,6]: missing parameter name after $ at end of template",
                () -> parser.parse( "templateName", "hello $" )
            );
        }

        @Test
        public void givenSpaceBetweenDollarParameterName_tryToParse_expectExceptionWithMeaningfulDescription() {
            assertThrows(
                ParseException.class,
                "templateName [1,8,7]: Expected 'JavaVariableNameMatcher' but found ' name'",
                () -> parser.parse( "templateName", "hello $ name" )
            );
        }
    }


    @Nested
    public class LongFormParamterTestCases { // ${foo}  ${a.b.c}  ${foo:type}  ${foo:type+mod+mod}

        @Test
        public void longFormParameter_givenNameOnly_expectStringFormatterToBeCalledWithTypeOnly() {
            StringFormatters formattersMock = new StringFormatters() {
                @Override
                public <T> StringFormatter<T> getFormatterFor( Class<?> jc, Optional<String> alias ) {
                    if ( !jc.equals( String.class ) || alias.isPresent() ) {
                        fail( "unexpected args to StringFormatters call" );
                    }

                    return v -> "aaa";
                }
            };

            parser = parser.withStringFormatters( formattersMock );

            String output = processTemplate( "${name}", "name", "Jane" );

            assertEquals( "aaa", output );
        }

        @Test
        public void longFormParameter_givenNameAndType_verifyTheCallToStringFormatters() {
            StringFormatters formattersMock = new StringFormatters() {
                @Override
                public <T> StringFormatter<T> getFormatterByName( String name ) {
                    if ( !name.equals( "String" ) ) {
                        fail( "unexpected name '" + name + "' to StringFormatters call" );
                    }

                    return v -> "aaa";
                }
            };

            parser = parser.withStringFormatters( formattersMock );

            String output = processTemplate( "${name:String}", "name", "Jane" );

            assertEquals( "aaa", output );
        }

        @Test
        public void longFormParameter_givenNameTypeAndUCMod() {
            String output = processTemplate( "${name:String+uc}", "name", "Jane" );

            assertEquals( "JANE", output );
        }

        @Test
        public void longFormParameter_givenNameTypeAndLCMod() {
            String output = processTemplate( "${name:String+lc}", "name", "Jane" );

            assertEquals( "jane", output );
        }

        @Test
        public void longFormParameter_givenNameTypeUCThenLCMod_expectModsToBeInvokedInDeclarationOrder() {
            String output = processTemplate( "${name:String+uc+lc}", "name", "Jane" );

            assertEquals( "jane", output );
        }

        @Test
        public void longFormParameter_givenNameTypeLCThenUCMod_expectModsToBeInvokedInDeclarationOrder() {
            String output = processTemplate( "${name:String+lc+uc}", "name", "Jane" );

            assertEquals( "JANE", output );
        }

        @Test
        public void withOneVariable_callGetTemplatePropertyNames_expectOneParameterName() {
            TextTemplate template = parser.parse( "templateName", "${name:String+lc+uc}" );

            assertEquals( Set.of("name"), template.getTemplatePropertyNames() );
        }

        @Test
        public void givenCapitaliseFirstWordModifier_expectOnlyTheFirstLetterToBeUC() {
            String output = processTemplate( "${desc:+CapitaliseFirstWord}", "desc", "what an awesome day" );

            assertEquals( "What an awesome day", output );
        }

        @Test
        public void longFormParameter_givenUnknownType_expectException() {
            try {
                processTemplate( "${name:FooBar}", "name", "Jane" );
                fail( "Expected UnknownTypeException" );
            } catch ( UnknownTypeException ex ) {
                assertEquals( "templateName [1,1,0]: Unknown parameter type 'FooBar'", ex.getMessage() );
            }
        }

        @Test
        public void longFormParameter_givenUnknownModifier_expectException() {
            try {
                processTemplate( "${name:+foo}", "name", "Jane" );
                fail( "Expected MissingModifierException" );
            } catch ( MissingModifierException ex ) {
                assertEquals( "templateName [1,1,0]: Missing modifier for 'foo'", ex.getMessage() );
            }
        }

        @Test
        public void longFormParameter_givenPropertyPathThatLeadsToValue_expectValue() {
            String output = processTemplate( "${name.toString.length}", "name", "Jane" );

            assertEquals( "4", output );
        }

        @Test
        public void longFormParameter_givenMissingProperty_expectException() {
            try {
                processTemplate( "${name.foo}", "name", "Jane" );
                fail( "expected exception" );
            } catch ( MissingPropertyException ex ) {
                assertEquals( "templateName [1,1,0]: java.lang.String is missing property 'foo'", ex.getMessage() );
            }
        }

        @Test
        public void longFormParameter_givenPropertyWithExplicitMethodThatLeadsToValue_expectValue() {
            String output = processTemplate( "${name.toString()}", "name", "Jane" );

            assertEquals( "Jane", output );
        }

        @Test
        public void longFormParameter_givenSomeValue_expectValue() {
            String output = processTemplate( "${name}", "name", FPOption.of( "Jane" ) );

            assertEquals( "Jane", output );
        }

        @Test
        public void longFormParameter_givenPresentOptionalValue_expectValue() {
            String output = processTemplate( "${name}", "name", Optional.of( "Jane" ) );

            assertEquals( "Jane", output );
        }

        @Test
        public void longFormParameter_givenPresentSomeValueWithFunctionCalledOnValue_expectValue() {
            String output = processTemplate( "${name.length()}", "name", FPOption.of( "Jane" ) );

            assertEquals( "4", output );
        }

        @Test
        public void longFormParameter_givenPresentOptionalValueWithFunctionCalledOnValue_expectValue() {
            String output = processTemplate( "${name.length()}", "name", Optional.of( "Jane" ) );

            assertEquals( "4", output );
        }

        @Test
        public void longFormParameter_givenPropertyPathThatPassesThroughSome_expectValue() {
            String output = processTemplate( "${list.next.value}", "list", new OptionLinkedValue( "a", new OptionLinkedValue( "b" ) ) );

            assertEquals( "b", output );
        }

        @Test
        public void longFormParameter_givenPropertyPathWithLeadsToANullValue_expectException() {
            TextTemplate template = parser.parse( "templateName", "${list.next.value}" );

            try {
                template.invoke( "list", new LinkedValue( "" ) );
                fail( "expect MissingParameterException" );
            } catch ( MissingParameterException ex ) {
                assertEquals( "templateName [1,1,0]: Missing value for 'next'", ex.getMessage() );
            }
        }

        @Test
        public void longFormParameter_givenPropertyPathWithLeadsToANullEndValue_expectException() {
            TextTemplate template = parser.parse( "templateName", "${list.next.value}" );

            try {
                template.invoke( "list", new LinkedValue( "", new LinkedValue( null ) ) );
                fail( "expect MissingParameterException" );
            } catch ( MissingParameterException ex ) {
                assertEquals( "templateName [1,1,0]: Missing value for 'value'", ex.getMessage() );
            }
        }
    }


    @Nested
    public class ParametersWithDefaultValueTestCases { // ${name=Bob}

        @Test
        public void longFormParameter_missingValueWithDefaultBlankValue_expectDefaultValue() {
            String output = processTemplate( "${name=}" );

            assertEquals( "", output );
        }

        @Test
        public void longFormParameter_missingValueWithDefaultValue_expectDefaultValue() {
            String output = processTemplate( "${name=Bob}" );

            assertEquals( "Bob", output );
        }

        @Test
        public void longFormParameter_withValueAndDefaultValue_expectSuppliedValue() {
            String output = processTemplate( "${name=Bob}", "name", "Jane" );

            assertEquals( "Jane", output );
        }

        @Test
        public void longFormParameter_withDefaultValueAndMods_expectSuppliedValueToBeModified() {
            String output = processTemplate( "${name=Bob:String+uc}" );

            assertEquals( "BOB", output );
        }
    }


    @Nested
    public class SingleLineIfBlockTestCases {
        @Test
        public void singleLineIfBlockWithOneMissingParameter_expectBlankOutput() {
            String output = processTemplate( "[Hello ${name}]" );

            assertEquals( "", output );
        }

        @Test
        public void singleLineIfBlockWithOneExistingParameter_expectWholeBlockIncluded() {
            String output = processTemplate( "[Hello ${name}]", "name", "JJ" );

            assertEquals( "Hello JJ", output );
        }

        @Test
        public void singleLineIfBlockWithOneDefaultedParameter_expectWholeBlockIncluded() {
            String output = processTemplate( "[Hello ${name=Bob}]" );

            assertEquals( "Hello Bob", output );
        }
    }


    @Nested
    public class TrivalueParameterTestCases { // ${a ? hello : goodbye} ${a.b.c:String+up=bob ? john : sarah}
        @Test
        public void trivalue_givenNonBlankValue_expectTrueValue() {
            String output = processTemplate( "${name ? a : b}", "name", "Jane" );

            assertEquals( "a", output );
        }

        @Test
        public void trivalue_givenTrueValue_expectTrueValue() {
            String output = processTemplate( "${name ? a : b}", "name", true );

            assertEquals( "a", output );
        }

        @Test
        public void trivalue_givenMissingValue_expectFalseValue() {
            String output = processTemplate( "${name ? a : b}" );

            assertEquals( "b", output );
        }

        @Test
        public void trivalue_givenBlankStringValue_expectFalseValue() {
            String output = processTemplate( "${name ? a : b}", "name", "" );

            assertEquals( "b", output );
        }

        @Test
        public void trivalue_givenFalseValue_expectFalseValue() {
            String output = processTemplate( "${name ? a : b}", "name", "false" );

            assertEquals( "b", output );
        }

        @Test
        public void withOneVariable_callGetTemplatePropertyNames_expectOneParameterName() {
            TextTemplate template = parser.parse( "templateName", "${name ? a : b}" );

            assertEquals( Set.of("name"), template.getTemplatePropertyNames() );
        }
    }


    @Nested
    public class IfStatementTestCases {
        @Test
        public void ifBlock_givenTrueValue_expectIfBody() {
            parseTemplate(
                "#if a",
                "Hello $a",
                "#endif"
            ).invokeWith(
                "a", "Jim"
            ).assertEquals(
                "Hello Jim",
                ""
            );
        }

        @Test
        public void ifBlock_givenMissingValue_expectNothing() {
            parseTemplate(
                "#if a",
                "Hello $a",
                "#endif"
            ).invokeWith().assertEquals();
        }

        @Test
        public void ifElseIfBlock_givenA_expectA() {
            parseTemplate(
                "#if a",
                "AAA",
                "#else if b",
                "BBB",
                "#endif"
            ).invokeWith( "a", "A" ).assertEquals(
                "AAA",
                ""
            );
        }

        @Test
        public void withOneVariable_callGetTemplatePropertyNames_expectOneParameterName() {
            String templateText = toSingleLine(
                "#if a",
                "$aaa",
                "#else if b",
                "$bbb",
                "#endif"
            );

            TextTemplate template = parser.parse( "templateName", templateText );

            assertEquals( Set.of("a", "b", "aaa", "bbb"), template.getTemplatePropertyNames() );
        }

        @Test
        public void ifElseIfBlock_givenB_expectB() {
            parseTemplate(
                "#if a",
                "AAA",
                "#else if b",
                "BBB",
                "#endif"
            ).invokeWith( "b", "B" ).assertEquals(
                "BBB",
                ""
            );
        }

        @Test
        public void ifElseIfBlock_givenNothing_expectNoMatch() {
            parseTemplate(
                "#if a",
                "AAA",
                "#else if b",
                "BBB",
                "#endif"
            ).invokeWith().assertEquals();
        }

        @Test
        public void ifElseIfElseBlock_givenA_expectA() {
            parseTemplate(
                "#if a",
                "AAA",
                "#else if b",
                "BBB",
                "#else",
                "CCC",
                "#endif"
            ).invokeWith( "a", "A" ).assertEquals(
                "AAA",
                ""
            );
        }

        @Test
        public void ifElseIfElseBlock_givenB_expectB() {
            parseTemplate(
                "#if a",
                "AAA",
                "#else if b",
                "BBB",
                "#else",
                "CCC",
                "#endif"
            ).invokeWith( "b", "B" ).assertEquals(
                "BBB",
                ""
            );
        }

        @Test
        public void ifElseIfElseBlock_givenNothing_expectElse() {
            parseTemplate(
                "#if a",
                "AAA",
                "#else if b",
                "BBB",
                "#else",
                "CCC",
                "#endif"
            ).invokeWith().assertEquals(
                "CCC",
                ""
            );
        }

        @Test
        public void ifBlock_propertyPathGoesThroughSome_expectAAA() {
            parseTemplate(
                "#if list.next",
                "AAA",
                "#else",
                "BBB",
                "#endif"
            ).invokeWith( "list", new OptionLinkedValue( "a", new OptionLinkedValue( "b" ) ) ).assertEquals(
                "AAA",
                ""
            );
        }

        @Test
        public void ifBlock_propertyPathGoesThroughNone_expectBBB() {
            parseTemplate(
                "#if list.next",
                "AAA",
                "#else",
                "BBB",
                "#endif"
            ).invokeWith( "list", new OptionLinkedValue( "a" ) ).assertEquals(
                "BBB",
                ""
            );
        }
    }


    @Nested
    public class ForeachStatementTestCases {
        @Test
        public void forEach_givenMissingList_expectNoIterations() {
            parseTemplate(
                "#for name in names",
                "Hello $name",
                "#endfor"
            ).invokeWith().assertEquals();
        }

        @Nested
        public class ListSupportTestCases {
            @Test
            public void forEach_givenZeroElementList_expectNoIterations() {
                parseTemplate(
                    "#for name in names",
                    "Hello $name",
                    "#endfor"
                ).invokeWith(
                    "names", Collections.emptyList()
                ).assertEquals();
            }

            @Test
            public void forEach_givenOneElementList_expectOneIteration() {
                parseTemplate(
                    "#for name in names",
                    "Hello $name",
                    "#endfor"
                ).invokeWith(
                    "names", Collections.singletonList( "Joe" )
                ).assertEquals(
                    "Hello Joe",
                    ""
                );
            }

            @Test
            public void withForVariableNameBeingNewWithinForBlock_callGetTemplatePropertyNames_expectVariableNameToNotBeThere() {
                String templateText = toSingleLine(
                    "#for name in names",
                    "Hello $name $foo",
                    "#endfor"
                );

                TextTemplate template = parser.parse( "templateName", templateText );

                assertEquals( Set.of("names", "foo"), template.getTemplatePropertyNames() );
            }

            @Test
            public void withForVariableNameBeingPreLovedBeforeForBlock_callGetTemplatePropertyNames_expectVariableNameToBeThere() {
                String templateText = toSingleLine(
                    "$name",
                    "#for name in names",
                    "Hello $name $foo",
                    "#endfor"
                );

                TextTemplate template = parser.parse( "templateName", templateText );

                assertEquals( Set.of("names", "name", "foo"), template.getTemplatePropertyNames() );
            }

            @Test
            public void forEach_givenTwoElementList_expectTwoIterations() {
                parseTemplate(
                    "#for name in names",
                    "Hello $name",
                    "#endfor"
                ).invokeWith(
                    "names", Arrays.asList( "Joe", "Sarah" )
                ).assertEquals(
                    "Hello Joe",
                    "Hello Sarah",
                    ""
                );
            }

            @Test
            public void forEach_givenThreeElementList_expectThreeIterations() {
                parseTemplate(
                    "#for name in names",
                    "Hello $name",
                    "#endfor"
                ).invokeWith(
                    "names", Arrays.asList( "Joe", "Sarah", "Bob" )
                ).assertEquals(
                    "Hello Joe",
                    "Hello Sarah",
                    "Hello Bob",
                    ""
                );
            }
        }

        @Nested
        public class MapTestCases {
            @Test
            public void forEach_givenMap_expectEntryIteration() {
                parseTemplate(
                    "#for kv in names",
                    "${kv.key}->${kv.value}",
                    "#endfor"
                ).invokeWith(
                    "names", MapUtils.asMap( "a", "1", "b", "2" )
                ).assertEquals(
                    "a->1",
                    "b->2",
                    ""
                );
            }
        }

        @Nested
        public class StreamTestCases {
            @Test
            public void forEach_givenMap_expectEntryIteration() {
                parseTemplate(
                    "#for v in names",
                    "$v",
                    "#endfor"
                ).invokeWith(
                    "names", Stream.of( "a", "b" )
                ).assertEquals(
                    "a",
                    "b",
                    ""
                );
            }
        }
    }


    @Nested
    public class FixesForDefectsDiscovered {
        @Test
        public void ensureThatAnInfiniteLoopIsNoLongerEnteredWhenHashesAppearInTheTemplate() {
            parseTemplate( "@import url('https://themes.googleusercontent.com/fonts/css?kit=MSSLfUayeNh9PW3ng9UWrqPqkO_clMDF6VDWdjQbdvOB1rEp0mYKuioi9p9DCaL5');ol{margin:0;padding:0}table td,table th{padding:0}.c3{border-right-style:solid;padding:0pt 5.4pt 0pt 5.4pt;border-bottom-color:#000000;border-top-width:1pt;border-right-width:1pt;border-left-color:#000000;vertical-align:top;border-right-color:#000000;border-left-width:1pt;border-top-style:solid;border-left-style:solid;border-bottom-width:1pt;width:94.3pt;border-top-color:#000000;border-bottom-style:solid}.c18{border-right-style:solid;padding:0pt 5.4pt 0pt 5.4pt;border-bottom-color:#000000;border-top-width:1pt;border-right-width:0pt;border-left-color:#000000;vertical-align:top;border-right-color:#000000;border-left-width:1pt;border-top-style:solid;border-left-style:solid;border-bottom-width:1pt;width:69.2pt;border-top-color:#000000;border-bottom-style:solid}.c19{border-right-style:solid;padding:0pt 5.4pt 0pt 5.4pt;border-bottom-color:#000000;border-top-width:1pt;border-right-width:1pt;border-left-color:#000000;vertical-align:top;border-right-color:#000000;border-left-width:0pt;border-top-style:solid;border-left-style:solid;border-bottom-width:1pt;width:262.2pt;border-top-color:#000000;border-bottom-style:solid}.c14{border-right-style:solid;padding:0pt 5.4pt 0pt 5.4pt;border-bottom-color:#000000;border-top-width:1pt;border-right-width:1pt;border-left-color:#000000;vertical-align:top;border-right-color:#000000;border-left-width:1pt;border-top-style:solid;border-left-style:solid;border-bottom-width:1pt;width:69.2pt;border-top-color:#000000;border-bottom-style:solid}.c8{border-right-style:solid;padding:0pt 5.4pt 0pt 5.4pt;border-bottom-color:#000000;border-top-width:1pt;border-right-width:1pt;border-left-color:#000000;vertical-align:top;border-right-color:#000000;border-left-width:1pt;border-top-style:solid;border-left-style:solid;border-bottom-width:1pt;width:262.2pt;border-top-color:#000000;border-bottom-style:solid}.c16{color:#3366ff;font-weight:400;text-decoration:none;vertical-align:baseline;font-size:14pt;font-family:\"Calibri\";font-style:normal}.c7{color:#000000;font-weight:400;text-decoration:none;vertical-align:baseline;font-size:14pt;font-family:\"Calibri\";font-style:normal}.c4{padding-top:0pt;padding-bottom:0pt;line-height:1.0;text-align:justify;height:12pt}.c17{color:#000000;font-weight:400;vertical-align:baseline;font-family:\"Cambria\"}.c13{margin-left:-5.4pt;border-spacing:0;border-collapse:collapse;margin-right:auto}.c12{color:#000000;font-weight:400;vertical-align:baseline;font-family:\"Calibri\"}.c10{padding-top:0pt;padding-bottom:0pt;line-height:1.0;text-align:justify}.c0{vertical-align:baseline;font-family:\"Calibri\";color:#4f13c8;font-weight:400}.c2{padding-top:0pt;padding-bottom:0pt;line-height:1.0;text-align:left}.c15{color:#000000;font-weight:400;vertical-align:baseline;font-family:\"Century Gothic\"}.c11{padding-top:0pt;padding-bottom:0pt;line-height:1.0;text-align:right}.c5{background-color:#ffffff;max-width:415pt;padding:72pt 90pt 35.5pt 90pt}.c1{text-decoration:none;font-size:12pt;font-style:normal}.c9{height:0pt}.c6{height:12pt}.title{padding-top:24pt;color:#000000;font-weight:700;font-size:36pt;padding-bottom:6pt;font-family:\"Cambria\";line-height:1.0;page-break-after:avoid;text-align:left}.subtitle{padding-top:18pt;color:#666666;font-size:24pt;padding-bottom:4pt;font-family:\"Georgia\";line-height:1.0;page-break-after:avoid;font-style:italic;text-align:left}li{color:#000000;font-size:12pt;font-family:\"Cambria\"}p{margin:0;color:#000000;font-size:12pt;font-family:\"Cambria\"}h1{padding-top:24pt;color:#000000;font-weight:700;font-size:24pt;padding-bottom:6pt;font-family:\"Cambria\";line-height:1.0;page-break-after:avoid;text-align:left}h2{padding-top:18pt;color:#000000;font-weight:700;font-size:18pt;padding-bottom:4pt;font-family:\"Cambria\";line-height:1.0;page-break-after:avoid;text-align:left}h3{padding-top:14pt;color:#000000;font-weight:700;font-size:14pt;padding-bottom:4pt;font-family:\"Cambria\";line-height:1.0;page-break-after:avoid;text-align:left}h4{padding-top:12pt;color:#000000;font-weight:700;font-size:12pt;padding-bottom:2pt;font-family:\"Cambria\";line-height:1.0;page-break-after:avoid;text-align:left}h5{padding-top:11pt;color:#000000;font-weight:700;font-size:11pt;padding-bottom:2pt;font-family:\"Cambria\";line-height:1.0;page-break-after:avoid;text-align:left}h6{padding-top:10pt;color:#000000;font-weight:700;font-size:10pt;padding-bottom:2pt;font-family:\"Cambria\";line-height:1.0;page-break-after:avoid;text-align:left}" )
                .invokeWith( "name", "James" )
                .assertEquals( "@import url('https://themes.googleusercontent.com/fonts/css?kit=MSSLfUayeNh9PW3ng9UWrqPqkO_clMDF6VDWdjQbdvOB1rEp0mYKuioi9p9DCaL5');ol{margin:0;padding:0}table td,table th{padding:0}.c3{border-right-style:solid;padding:0pt 5.4pt 0pt 5.4pt;border-bottom-color:#000000;border-top-width:1pt;border-right-width:1pt;border-left-color:#000000;vertical-align:top;border-right-color:#000000;border-left-width:1pt;border-top-style:solid;border-left-style:solid;border-bottom-width:1pt;width:94.3pt;border-top-color:#000000;border-bottom-style:solid}.c18{border-right-style:solid;padding:0pt 5.4pt 0pt 5.4pt;border-bottom-color:#000000;border-top-width:1pt;border-right-width:0pt;border-left-color:#000000;vertical-align:top;border-right-color:#000000;border-left-width:1pt;border-top-style:solid;border-left-style:solid;border-bottom-width:1pt;width:69.2pt;border-top-color:#000000;border-bottom-style:solid}.c19{border-right-style:solid;padding:0pt 5.4pt 0pt 5.4pt;border-bottom-color:#000000;border-top-width:1pt;border-right-width:1pt;border-left-color:#000000;vertical-align:top;border-right-color:#000000;border-left-width:0pt;border-top-style:solid;border-left-style:solid;border-bottom-width:1pt;width:262.2pt;border-top-color:#000000;border-bottom-style:solid}.c14{border-right-style:solid;padding:0pt 5.4pt 0pt 5.4pt;border-bottom-color:#000000;border-top-width:1pt;border-right-width:1pt;border-left-color:#000000;vertical-align:top;border-right-color:#000000;border-left-width:1pt;border-top-style:solid;border-left-style:solid;border-bottom-width:1pt;width:69.2pt;border-top-color:#000000;border-bottom-style:solid}.c8{border-right-style:solid;padding:0pt 5.4pt 0pt 5.4pt;border-bottom-color:#000000;border-top-width:1pt;border-right-width:1pt;border-left-color:#000000;vertical-align:top;border-right-color:#000000;border-left-width:1pt;border-top-style:solid;border-left-style:solid;border-bottom-width:1pt;width:262.2pt;border-top-color:#000000;border-bottom-style:solid}.c16{color:#3366ff;font-weight:400;text-decoration:none;vertical-align:baseline;font-size:14pt;font-family:\"Calibri\";font-style:normal}.c7{color:#000000;font-weight:400;text-decoration:none;vertical-align:baseline;font-size:14pt;font-family:\"Calibri\";font-style:normal}.c4{padding-top:0pt;padding-bottom:0pt;line-height:1.0;text-align:justify;height:12pt}.c17{color:#000000;font-weight:400;vertical-align:baseline;font-family:\"Cambria\"}.c13{margin-left:-5.4pt;border-spacing:0;border-collapse:collapse;margin-right:auto}.c12{color:#000000;font-weight:400;vertical-align:baseline;font-family:\"Calibri\"}.c10{padding-top:0pt;padding-bottom:0pt;line-height:1.0;text-align:justify}.c0{vertical-align:baseline;font-family:\"Calibri\";color:#4f13c8;font-weight:400}.c2{padding-top:0pt;padding-bottom:0pt;line-height:1.0;text-align:left}.c15{color:#000000;font-weight:400;vertical-align:baseline;font-family:\"Century Gothic\"}.c11{padding-top:0pt;padding-bottom:0pt;line-height:1.0;text-align:right}.c5{background-color:#ffffff;max-width:415pt;padding:72pt 90pt 35.5pt 90pt}.c1{text-decoration:none;font-size:12pt;font-style:normal}.c9{height:0pt}.c6{height:12pt}.title{padding-top:24pt;color:#000000;font-weight:700;font-size:36pt;padding-bottom:6pt;font-family:\"Cambria\";line-height:1.0;page-break-after:avoid;text-align:left}.subtitle{padding-top:18pt;color:#666666;font-size:24pt;padding-bottom:4pt;font-family:\"Georgia\";line-height:1.0;page-break-after:avoid;font-style:italic;text-align:left}li{color:#000000;font-size:12pt;font-family:\"Cambria\"}p{margin:0;color:#000000;font-size:12pt;font-family:\"Cambria\"}h1{padding-top:24pt;color:#000000;font-weight:700;font-size:24pt;padding-bottom:6pt;font-family:\"Cambria\";line-height:1.0;page-break-after:avoid;text-align:left}h2{padding-top:18pt;color:#000000;font-weight:700;font-size:18pt;padding-bottom:4pt;font-family:\"Cambria\";line-height:1.0;page-break-after:avoid;text-align:left}h3{padding-top:14pt;color:#000000;font-weight:700;font-size:14pt;padding-bottom:4pt;font-family:\"Cambria\";line-height:1.0;page-break-after:avoid;text-align:left}h4{padding-top:12pt;color:#000000;font-weight:700;font-size:12pt;padding-bottom:2pt;font-family:\"Cambria\";line-height:1.0;page-break-after:avoid;text-align:left}h5{padding-top:11pt;color:#000000;font-weight:700;font-size:11pt;padding-bottom:2pt;font-family:\"Cambria\";line-height:1.0;page-break-after:avoid;text-align:left}h6{padding-top:10pt;color:#000000;font-weight:700;font-size:10pt;padding-bottom:2pt;font-family:\"Cambria\";line-height:1.0;page-break-after:avoid;text-align:left}" );
        }

        @Test
        public void ensureThatADTMFormatMayBeSpecified() {
            long whenMillis = DTMUtils.toEpoch( 2016, 3, 23, 8, 1 );

            parseTemplate(
                "${whenMillis:DTM(dd.MM.YY)}"
            ).invokeWith(
                "whenMillis", whenMillis
            ).assertEquals(
                "23.03.16"
            );
        }
    }


    private String processTemplate( String templateDefinition, Object...kvPairs ) {
        TextTemplate template = parser.parse( "templateName", templateDefinition );

        return template.invoke(kvPairs);
    }

    private static class LinkedValue {
        public String value;
        public LinkedValue next;


        public LinkedValue( String value ) {
            this( value, null );
        }

        public LinkedValue( String value, LinkedValue next ) {
            this.value = value;
            this.next  = next;
        }
    }

    private static class OptionLinkedValue {
        public String value;
        public FPOption<OptionLinkedValue> next;


        public OptionLinkedValue( String value ) {
            this( value, null );
        }

        public OptionLinkedValue( String value, OptionLinkedValue next ) {
            this.value = value;
            this.next  = FPOption.of(next);
        }
    }


    private String toSingleLine( String...templateLines ) {
        return StringUtils.concat( templateLines, "\n" );
    }

    private MyTemplate parseTemplate( String...templateLines ) {
        String template = toSingleLine( templateLines );
        TextTemplate textTemplate = parser.parse( "junit", CharacterIterators.stringIterator(template) );

        return new MyTemplate(textTemplate);
    }

    private static class MyTemplate {
        private TextTemplate textTemplate;

        public MyTemplate( TextTemplate textTemplate ) {
            this.textTemplate = textTemplate;
        }

        public MyTemplateResults invokeWith( Object...argKVPairs ) {
            String result = textTemplate.invoke( argKVPairs );

            return new MyTemplateResults(result);
        }

    }

    private static class MyTemplateResults {
        private String result;

        public MyTemplateResults( String result ) {
            this.result = result;
        }

        public void assertEquals( String...expectedText ) {
            String txt = StringUtils.concat(expectedText, "\n");

            Assertions.assertEquals( txt, result );
        }
    }
}




// todo   padr3
// todo   padl3
// todo   padl(' ',10)
// todo   padr(' ',10)
// todo   uc
// todo   lc
// todo   plural('Day','Days')
// todo   +1dp
// todo   +ru
// todo   +rd
// todo   +rc
