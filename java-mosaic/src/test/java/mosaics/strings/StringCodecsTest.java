package mosaics.strings;

import mosaics.fp.FP;
import mosaics.fp.collections.FPOption;
import mosaics.lang.reflection.JavaClass;
import mosaics.lang.reflection.JavaMethod;
import mosaics.lang.reflection.JavaParameter;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static mosaics.lang.Backdoor.cast;
import static mosaics.strings.codecs.StringCodecAssertions.assertCodecRoundTrip;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.fail;

@SuppressWarnings({"unchecked", "rawtypes"})
public class StringCodecsTest {

    public static final JavaClass TESTSERVICE_JC = JavaClass.of( TestService.class );
    private final StringCodecs codecs = StringCodecs.DEFAULT_CODECS;


    @Nested
    public class FileTestCases {
        @Test
        public void encodeFile() {
            assertEquals( "a.html", codecs.getCodecFor( File.class ).get().encode( new File( "a.html" ) ) );
        }

        @Test
        public void decodeFile() {
            StringCodec<File> codec = codecs.getCodecFor( File.class ).get();

            assertEquals( new File( "a.html" ), codec.decode( "a.html" ).getResult() );
        }

        @Test
        public void decodeFPOptionalFile() {
            StringCodec<FPOption<File>> codec = cast( codecs.getCodecFor( JavaClass.of(FPOption.class, File.class) ).get() );

            assertEquals( new File( "a.html" ), codec.decode( "a.html" ).getResult().get() );
        }
    }

    @Test
    public void encodeIntegers() {
        assertEquals("0",                                 codecs.getCodecFor(Integer.class).get().encode(0));
        assertEquals("1",                                 codecs.getCodecFor(Integer.class).get().encode(1));
        assertEquals("-1",                                codecs.getCodecFor(Integer.class).get().encode(-1));
        assertEquals("-12345",                            codecs.getCodecFor(Integer.class).get().encode(-12345));
        assertEquals(Integer.toString(Integer.MAX_VALUE), codecs.getCodecFor(Integer.class).get().encode(Integer.MAX_VALUE));
        assertEquals(Integer.toString(Integer.MIN_VALUE), codecs.getCodecFor(Integer.class).get().encode(Integer.MIN_VALUE));
    }

    @Test
    public void decodeIntegerPrimitives() {
        FPOption<StringCodec<Integer>> codecFor = codecs.getCodecFor( Integer.TYPE );
        assertEquals(0,                 codecFor.get().decode(null).getResult().intValue());
        assertEquals(0,                 codecs.getCodecFor(Integer.TYPE).get().decode("0").getResult().intValue());
        assertEquals(12345,             codecs.getCodecFor(Integer.TYPE).get().decode("12345").getResult().intValue());
        assertEquals(-12345,            codecs.getCodecFor(Integer.TYPE).get().decode("-12345").getResult().intValue());
        assertEquals(Integer.MAX_VALUE, codecs.getCodecFor(Integer.TYPE).get().decode(Integer.toString(Integer.MAX_VALUE)).getResult().intValue());
        assertEquals(Integer.MIN_VALUE, codecs.getCodecFor(Integer.TYPE).get().decode(Integer.toString(Integer.MIN_VALUE)).getResult().intValue());

        assertEquals( "java.lang.NumberFormatException: For input string: \"-1234a5\"", codecs.getCodecFor(Integer.TYPE).get().decode("-1234a5").getFailure().toString() );
    }

    @Test
    public void decodeIntegerObjects() {
        assertEquals(0,                 codecs.getCodecFor(Integer.class).get().decode("0").getResult().intValue());
        assertEquals(12345,             codecs.getCodecFor(Integer.class).get().decode("12345").getResult().intValue());
        assertEquals(-12345,            codecs.getCodecFor(Integer.class).get().decode("-12345").getResult().intValue());
        assertEquals(Integer.MAX_VALUE, codecs.getCodecFor(Integer.class).get().decode(Integer.toString(Integer.MAX_VALUE)).getResult().intValue());
        assertEquals(Integer.MIN_VALUE, codecs.getCodecFor(Integer.class).get().decode(Integer.toString(Integer.MIN_VALUE)).getResult().intValue());


        assertEquals( "java.lang.NumberFormatException: For input string: \"-1234a5\"", codecs.getCodecFor(Integer.class).get().decode("-1234a5").getFailure().toString() );
    }



    @Test
    public void encodeLongs() {
        assertEquals("0", codecs.getCodecFor(Long.class).get().encode(0L));
        assertEquals("1", codecs.getCodecFor(Long.class).get().encode(1L));
        assertEquals("-1", codecs.getCodecFor(Long.class).get().encode(-1L));
        assertEquals("-12345", codecs.getCodecFor(Long.class).get().encode(-12345L));
        assertEquals(Long.toString(Long.MAX_VALUE), codecs.getCodecFor(Long.class).get().encode(Long.MAX_VALUE));
        assertEquals(Long.toString(Long.MIN_VALUE), codecs.getCodecFor(Long.class).get().encode(Long.MIN_VALUE));
    }

    @Test
    public void decodeLongs() {
        assertEquals(0L, codecs.getCodecFor(Long.class).get().decode("0").getResult().longValue());
        assertEquals(12345L, codecs.getCodecFor(Long.class).get().decode("12345").getResult().longValue());
        assertEquals(-12345L, codecs.getCodecFor(Long.class).get().decode("-12345").getResult().longValue());
        assertEquals(Long.MAX_VALUE, codecs.getCodecFor(Long.class).get().decode(Long.toString(Long.MAX_VALUE)).getResult().longValue());
        assertEquals(Long.MIN_VALUE, codecs.getCodecFor(Long.class).get().decode(Long.toString(Long.MIN_VALUE)).getResult().longValue());


        assertEquals( "java.lang.NumberFormatException: For input string: \"-1234a5\"", codecs.getCodecFor(Long.class).get().decode("-1234a5").getFailure().toString() );
    }



    @Test
    public void encodeFloats() {
        assertEquals("0.0", codecs.getCodecFor(Float.class).get().encode(0.0f));
        assertEquals("1.0", codecs.getCodecFor(Float.class).get().encode(1.0f));
        assertEquals("1.5", codecs.getCodecFor(Float.class).get().encode(1.5f));
        assertEquals("-1.5", codecs.getCodecFor(Float.class).get().encode(-1.5f));
        assertEquals(Float.toString(Float.MAX_VALUE), codecs.getCodecFor(Float.class).get().encode(Float.MAX_VALUE));
        assertEquals(Float.toString(Float.MIN_VALUE), codecs.getCodecFor(Float.class).get().encode(Float.MIN_VALUE));
    }

    @Test
    public void decodeFloats() {
        assertEquals(-0.0f, codecs.getCodecFor(Float.class).get().decode("-0").getResult(), 0.0001);
        assertEquals(0.0f, codecs.getCodecFor(Float.class).get().decode("0").getResult(), 0.0001);
        assertEquals(0.0f, codecs.getCodecFor(Float.class).get().decode("0.0").getResult(), 0.0001);
        assertEquals(12.34f, codecs.getCodecFor(Float.class).get().decode("12.34").getResult(), 0.0001);
        assertEquals(-12.34f, codecs.getCodecFor(Float.class).get().decode("-12.34").getResult(), 0.0001);
        assertEquals(Float.MAX_VALUE, codecs.getCodecFor(Float.class).get().decode(Float.toString(Float.MAX_VALUE)).getResult(), 0.0001);
        assertEquals(Float.MIN_VALUE, codecs.getCodecFor(Float.class).get().decode(Float.toString(Float.MIN_VALUE)).getResult(), 0.0001);

        assertEquals( "java.lang.NumberFormatException: For input string: \"0.12A\"", codecs.getCodecFor(Float.class).get().decode("0.12A").getFailure().toString() );
    }



    @Test
    public void encodeDoubles() {
        assertEquals( "0.0", codecs.getCodecFor(Double.class).get().encode(0.0) );
        assertEquals( "1.0", codecs.getCodecFor(Double.class).get().encode(1.0) );
        assertEquals( "1.5", codecs.getCodecFor(Double.class).get().encode(1.5) );
        assertEquals( "-1.5", codecs.getCodecFor(Double.class).get().encode(-1.5) );
        assertEquals( Double.toString( Double.MAX_VALUE ), codecs.getCodecFor(Double.class).get().encode(Double.MAX_VALUE) );
        assertEquals( Double.toString( Double.MIN_VALUE ), codecs.getCodecFor(Double.class).get().encode(Double.MIN_VALUE) );
    }

    @Test
    public void decodeDoubles() {
        assertEquals( -0.0, codecs.getCodecFor(Double.class).get().decode( "-0" ).getResult(), 0.0001 );
        assertEquals( 0.0, codecs.getCodecFor(Double.class).get().decode( "0" ).getResult(), 0.0001 );
        assertEquals( 0.0, codecs.getCodecFor(Double.class).get().decode("0.0").getResult(), 0.0001 );
        assertEquals( 12.34, codecs.getCodecFor(Double.class).get().decode("12.34").getResult(), 0.0001 );
        assertEquals( -12.34, codecs.getCodecFor(Double.class).get().decode( "-12.34" ).getResult(), 0.0001 );
        assertEquals( Double.MAX_VALUE, codecs.getCodecFor(Double.class).get().decode( Double.toString( Double.MAX_VALUE ) ).getResult(), 0.0001 );
        assertEquals( Double.MIN_VALUE, codecs.getCodecFor(Double.class).get().decode( Double.toString( Double.MIN_VALUE ) ).getResult(), 0.0001 );

        assertEquals( "java.lang.NumberFormatException: For input string: \"0.12A\"", codecs.getCodecFor(Double.class).get().decode("0.12A").getFailure().toString() );
    }


//    @Test
//    public void encode1dpLongs() {
//        assertEquals("0.0",   codecs.getCodecByAlias("1dp").encode(0L));
//        assertEquals("0.7",   codecs.getCodecByAlias("1dp").encode(7L));
//        assertEquals("13.2",  codecs.getCodecByAlias("1dp").encode(132L));
//        assertEquals("-13.2", codecs.getCodecByAlias("1dp").encode(-132L));
//    }

//    @Test
//    public void decode1dpLongs() {
//        assertEquals(0L,    codecs.getCodecByAlias("1dp").decode("0").getResult());
//        assertEquals(0L,    codecs.getCodecByAlias("1dp").decode("0.0").getResult());
//        assertEquals(132L,  codecs.getCodecByAlias("1dp").decode("13.2").getResult());
//        assertEquals(-132L, codecs.getCodecByAlias("1dp").decode("-13.2").getResult());
//        assertEquals(132L, codecs.getCodecByAlias("1dp").decode("13.21").getResult());
//        assertEquals(132L, codecs.getCodecByAlias("1dp").decode("13.24").getResult());
//        assertEquals(133L, codecs.getCodecByAlias("1dp").decode("13.25").getResult());
//        assertEquals(133L, codecs.getCodecByAlias("1dp").decode("13.29").getResult());
//        assertEquals(-132L, codecs.getCodecByAlias("1dp").decode("-13.24").getResult());
//        assertEquals(-133L, codecs.getCodecByAlias("1dp").decode("-13.29").getResult());
//
//        assertEquals(140L, codecs.getCodecByAlias("1dp").decode("13.99").getResult());
//        assertEquals(-140L, codecs.getCodecByAlias("1dp").decode("-13.99").getResult());
//
//
//        assertEquals( "java.lang.NumberFormatException: For input string: \"-1234a5\"", codecs.getCodecByAlias("1dp").decode("-1234a5").getFailure().toString() );
//    }

//    @Test
//    public void encodeGBP() {
//        assertEquals("£0.00",   codecs.getCodecByAlias("gbp").encode(0L));
//        assertEquals("£0.00",   codecs.getCodecByAlias("gbp").encode(7L));
//        assertEquals("£0.13",  codecs.getCodecByAlias("gbp").encode(132L));
//        assertEquals("-£0.13", codecs.getCodecByAlias("gbp").encode(-132L));
//    }

//    @Test
//    public void decodeGBP() {
//        assertEquals(0L,    codecs.getCodecByAlias("gbp").decode("0").getResult());
//        assertEquals(0L,    codecs.getCodecByAlias("gbp").decode("0.0").getResult());
//        assertEquals(13200L,  codecs.getCodecByAlias("gbp").decode("13.2").getResult());
//        assertEquals(13219L,  codecs.getCodecByAlias("gbp").decode("13.219").getResult());
//        assertEquals(13210L,  codecs.getCodecByAlias("gbp").decode("13.210").getResult());
//        assertEquals(13201L,  codecs.getCodecByAlias("gbp").decode("13.201").getResult());
//        assertEquals(13211L, codecs.getCodecByAlias("gbp").decode("13.2111").getResult());
//        assertEquals(13211L, codecs.getCodecByAlias("gbp").decode("13.2114").getResult());
//        assertEquals(13212L, codecs.getCodecByAlias("gbp").decode("13.2115").getResult());
//        assertEquals(13212L, codecs.getCodecByAlias("gbp").decode("13.2119").getResult());
//        assertEquals(-13211L, codecs.getCodecByAlias("gbp").decode("-13.2114").getResult());
//        assertEquals(-13212L, codecs.getCodecByAlias("gbp").decode("-13.2115").getResult());
//        assertEquals(-13212L, codecs.getCodecByAlias("gbp").decode("-13.2119").getResult());
//
//        assertEquals(0L,    codecs.getCodecByAlias("gbp").decode("£0").getResult());
//        assertEquals(0L,    codecs.getCodecByAlias("gbp").decode("£0.0").getResult());
//        assertEquals(13200L,  codecs.getCodecByAlias("gbp").decode("£13.2").getResult());
//        assertEquals(13219L,  codecs.getCodecByAlias("gbp").decode("£13.219").getResult());
//        assertEquals(13210L,  codecs.getCodecByAlias("gbp").decode("£13.210").getResult());
//        assertEquals(13201L,  codecs.getCodecByAlias("gbp").decode("£13.201").getResult());
//        assertEquals(13211L, codecs.getCodecByAlias("gbp").decode("£13.2111").getResult());
//        assertEquals(13211L, codecs.getCodecByAlias("gbp").decode("£13.2114").getResult());
//        assertEquals(13212L, codecs.getCodecByAlias("gbp").decode("£13.2115").getResult());
//        assertEquals(13212L, codecs.getCodecByAlias("gbp").decode("£13.2119").getResult());
//        assertEquals(-13211L, codecs.getCodecByAlias("gbp").decode("-£13.2114").getResult());
//        assertEquals(-13212L, codecs.getCodecByAlias("gbp").decode("-£13.2115").getResult());
//        assertEquals(-13212L, codecs.getCodecByAlias("gbp").decode("-£13.2119").getResult());
//
//        assertEquals( "java.lang.NumberFormatException: For input string: \"-1234a5\"", codecs.getCodecByAlias("gbp").decode("-1234a5").getFailure().toString() );
//    }

// GETCODECFOR TYPEMETA

    @Test
    public void getCodecForTypeMeta_primitiveBoolean() {
        StringCodec codec = codecs.getCodecFor( JavaClass.BOOLEAN_PRIMITIVE ).get();

        assertCodecRoundTrip( codec, true, "true" );
        assertCodecRoundTrip( codec, false, "false" );
    }

    @Test
    public void getCodecForTypeMeta_primitiveObject() {
        StringCodec codec = codecs.getCodecFor( JavaClass.BOOLEAN_OBJECT ).get();

        assertCodecRoundTrip( codec, null, null );
        assertCodecRoundTrip( codec, true, "true" );
        assertCodecRoundTrip( codec, false, "false" );
    }

    @Test
    public void getCodecForTypeMeta_fpOptionLong() {
        StringCodec codec = codecs.getCodecFor( JavaClass.of(FPOption.class, Long.class) ).get();

        assertCodecRoundTrip( codec, FP.emptyOption(), null );
        assertCodecRoundTrip( codec, FP.option(42L), "42" );
    }

    @Test
    public void getCodecForTypeMeta_fpOptionNoGeneric_expectNoCodec() {
        assertEquals( FP.emptyOption(), codecs.getCodecFor(JavaClass.of(FPOption.class)) );
    }

    @Test
    public void getCodecForTypeMeta_jdkOptionalLong() {
        StringCodec codec = codecs.getCodecFor( JavaClass.of(Optional.class, Long.class) ).get();

        assertCodecRoundTrip( codec, Optional.empty(), null );
        assertCodecRoundTrip( codec, Optional.of(42L), "42" );
    }

    @Test
    public void getCodecForTypeMeta_jdkOptionalNoGeneric_expectNoCodec() {
        assertEquals( FP.emptyOption(), codecs.getCodecFor(JavaClass.of(Optional.class)) );
    }


    @Nested
    public class WithAliasTestCases {
        @Test
        public void f() {
            StringCodecs codecs = new StringCodecs();

            assertNull( codecs.getCodecByAlias("wstr") );

            codecs = codecs.withAliasedCodec("wstr", JavaClass.STRING );

            assertNotNull( codecs.getCodecByAlias("wstr") );
        }
    }

    /**
     * NB we provide default map formatters so that we can order the keys, which makes unit
     * testing easier.
     */
    @Nested
    public class MapTestCases {
        @Test
        public void testMap() {
            StringCodec<Map<String,Integer>> codec = codecs.getCodecFor( JavaClass.of(Map.class, String.class, Integer.class) ).get();

            assertEquals("{a=1, b=2}", codec.encode(Map.of("b",2,"a",1)) );
            assertEquals("{a=1, b=2}", codec.encode(Map.of("a",1, "b",2)) );
        }

        @Test
        public void testHashMap() {
            StringCodec<Map<String,Integer>> codec = codecs.getCodecFor( JavaClass.of(Map.class, String.class, Integer.class) ).get();

            assertEquals("{a=1, b=2}", codec.encode(new HashMap<>(Map.of("b",2,"a",1))) );
            assertEquals("{a=1, b=2}", codec.encode(new HashMap<>(Map.of("a",1, "b",2))) );
        }
    }


    /**
     * NB we provide default set formatters so that we can order the values, which makes unit
     * testing easier.
     */
    @Nested
    public class SetTestCases {
        @Test
        public void testSet() {
            StringCodec<Set<String>> codec = codecs.getCodecFor( JavaClass.of(Set.class, String.class) ).get();

            assertEquals("[a,b]", codec.encode(Set.of("a", "b")) );
            assertEquals("[a,b]", codec.encode(Set.of("b", "a")) );
        }

        @Test
        public void testHashSet() {
            StringCodec<Set<String>> codec = codecs.getCodecFor( JavaClass.of(Set.class, String.class) ).get();

            assertEquals("[a,b]", codec.encode(new HashSet<>(Set.of("a", "b"))) );
            assertEquals("[a,b]", codec.encode(new HashSet<>(Set.of("b", "a"))) );
        }
    }




    @Nested
    public class StringifyTestCases {
        @Test
        public void givenNoArgMethod_stringify_expectSameMethodBack() {
            JavaMethod origMethod = TESTSERVICE_JC.getStaticMethod( Void.TYPE, "noArgMethod" ).get();

            assertSame( origMethod, codecs.stringify( origMethod ) );
        }

        @Test
        public void givenStringArgsMethod_stringify_expectSameMethodBack() {
            JavaMethod origMethod = TESTSERVICE_JC.getStaticMethod( Void.TYPE, "stringArgsMethod", String.class, String.class ).get();

            assertSame( origMethod, codecs.stringify( origMethod ) );
        }

        @Test
        public void givenFPOptionStringArgsMethod_stringify_invokeMethodAndEnsureArgIsAsExpected() {
            JavaMethod origMethod = TESTSERVICE_JC.getStaticMethod( Void.TYPE, "fpOptionStringArgsMethod", FPOption.class ).get();
            JavaMethod stringifiedMethod = codecs.stringify( origMethod );

            TestService service = new TestService();
            assertEquals( FP.option( "hello" ), stringifiedMethod.invokeAgainst( service, "hello" ) );
            assertEquals( FP.emptyOption(), stringifiedMethod.invokeAgainst( service, new Object[]{null} ) );
        }

        @Test
        public void givenJdkOptionalStringArgsMethod_stringify_invokeMethodAndEnsureArgIsAsExpected() {
            JavaMethod origMethod = TESTSERVICE_JC.getStaticMethod( Void.TYPE, "jdkOptionalStringArgsMethod", Optional.class ).get();
            JavaMethod stringifiedMethod = codecs.stringify( origMethod );

            TestService service = new TestService();
            assertEquals( Optional.of( "hello" ), stringifiedMethod.invokeAgainst( service, "hello" ) );
            assertEquals( Optional.empty(), stringifiedMethod.invokeAgainst( service, new Object[]{null} ) );
        }

        @Test
        public void givenIntFloatMethod_stringify_expectMethodThatDecodesStringArgs() {
            JavaMethod origMethod        = TESTSERVICE_JC.getStaticMethod( String.class, "intFloatMethod", int.class, float.class ).get();
            JavaMethod stringifiedMethod = codecs.stringify( origMethod );

            assertNotSame( origMethod, stringifiedMethod );

            assertEquals( "42:1.23", stringifiedMethod.invokeStaticAgainst( "42", "1.23" ) );
        }

        @Test
        public void givenIntFloatMethod_stringify_expectDocsToBeUpdated() {
            JavaMethod origMethod        = TESTSERVICE_JC.getStaticMethod( String.class, "intFloatMethod", int.class, float.class ).get();
            JavaMethod stringifiedMethod = codecs.stringify( origMethod );

            Object[] expected = new Object[]{
                new JavaParameter( stringifiedMethod, 0, JavaClass.STRING, "a", Collections.emptyList() ),
                new JavaParameter( stringifiedMethod, 1, JavaClass.STRING, "b", Collections.emptyList() )
            };

            Object[] actual = stringifiedMethod.getParameters().toArray();

            assertArrayEquals( expected, actual );
        }

        @Test
        public void givenMethodWithNonEncodableArg_stringify_expectException() {
            JavaMethod origMethod = TESTSERVICE_JC.getStaticMethod( String.class, "hasUnencoadableArg", UnencodableDTO.class ).get();

            try {
                codecs.stringify( origMethod );
                fail( "expected exception" );
            } catch ( CodecNotFoundException ex ) {
                assertEquals( "Unable to find StringCodec: 'mosaics.strings.StringCodecsTest$UnencodableDTO'", ex.getMessage() );
            }
        }
    }

    @SuppressWarnings("unused")
    public static class TestService {
        public static void noArgMethod() {
        }

        public static void stringArgsMethod( String a, String b ) {
        }

        public static FPOption<String> fpOptionStringArgsMethod( FPOption<String> a ) {
            return a;
        }

        public static Optional<String> jdkOptionalStringArgsMethod( Optional<String> a ) {
            return a;
        }

        public static String intFloatMethod( int a, float b ) {
            return a + ":" + b;
        }

        public static void hasUnencoadableArg( UnencodableDTO a ) {
        }
    }

    @SuppressWarnings("unused")
    public static class UnencodableDTO {
        public UnencodableDTO( String name, int age ) {}
    }
}
