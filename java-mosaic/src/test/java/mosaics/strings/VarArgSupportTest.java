package mosaics.strings;

import mosaics.fp.FP;
import mosaics.fp.collections.FPList;
import mosaics.fp.collections.FPSet;
import mosaics.fp.collections.RRBVector;
import mosaics.lang.reflection.JavaClass;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class VarArgSupportTest {

    @Nested
    public class SelectVarArgHandlerForTestCases {
        @Nested
        public class NoneCollectionTestCases {
            @Test
            public void givenString_expectNone() {
                assertEquals( FP.emptyOption(), VarArgSupport.selectVarArgHandlerFor( StringCodecs.DEFAULT_CODECS, JavaClass.STRING ) );
            }

            @Test
            public void givenInt_expectNone() {
                assertEquals( FP.emptyOption(), VarArgSupport.selectVarArgHandlerFor( StringCodecs.DEFAULT_CODECS, JavaClass.INTEGER_PRIMITIVE ) );
                assertEquals( FP.emptyOption(), VarArgSupport.selectVarArgHandlerFor( StringCodecs.DEFAULT_CODECS, JavaClass.INTEGER_OBJECT ) );
            }

            @Test
            public void givenSomethingWithNoCodec_expectNone() {
                assertEquals( FP.emptyOption(), VarArgSupport.selectVarArgHandlerFor( StringCodecs.DEFAULT_CODECS, JavaClass.of(VarArgSupportTest.class) ) );
            }
        }


        @Nested
        public class ListTestCases {
            @Test
            public void givenListOfStrings_expectListVarArgSupportConfiguredForStrings() {
                JavaClass jc = JavaClass.of( List.class, String.class );
                VarArgHandler actual = VarArgSupport.selectVarArgHandlerFor( StringCodecs.DEFAULT_CODECS, jc ).get();

                assertEquals( ListVarArgHandler.class, actual.getClass() );
                assertEquals( "ListVarArgHandler(String)", actual.toString() );
            }

            @Test
            public void givenListOfInts_expectListVarArgSupportConfiguredForInts() {
                JavaClass jc = JavaClass.of( List.class, Integer.class );
                VarArgHandler actual = VarArgSupport.selectVarArgHandlerFor( StringCodecs.DEFAULT_CODECS, jc ).get();

                assertEquals( ListVarArgHandler.class, actual.getClass() );
                assertEquals( "ListVarArgHandler(Integer)", actual.toString() );
            }

            @Test
            public void givenListOfUnknownTypes_expectNone() {
                JavaClass jc = JavaClass.of( List.class );

                assertTrue( VarArgSupport.selectVarArgHandlerFor( StringCodecs.DEFAULT_CODECS, jc ).isEmpty() );
            }

            @Test
            public void givenListOfSomethingThatHasNoCodec_expectNone() {
                JavaClass jc = JavaClass.of( List.class, VarArgSupportTest.class );

                assertTrue( VarArgSupport.selectVarArgHandlerFor( StringCodecs.DEFAULT_CODECS, jc ).isEmpty() );
            }
        }


        @Nested
        public class FPListTestCases {
            @Test
            public void givenFPListOfStrings_expectFPListVarArgSupportConfiguredForStrings() {
                JavaClass     jc     = JavaClass.of( FPList.class, String.class );
                VarArgHandler actual = VarArgSupport.selectVarArgHandlerFor( StringCodecs.DEFAULT_CODECS, jc ).get();

                assertEquals( FPListVarArgHandler.class, actual.getClass() );
                assertEquals( "FPListVarArgHandler(String)", actual.toString() );
            }

            @Test
            public void givenFPListOfInts_expectFPListVarArgSupportConfiguredForInts() {
                JavaClass     jc     = JavaClass.of( FPList.class, Integer.class );
                VarArgHandler actual = VarArgSupport.selectVarArgHandlerFor( StringCodecs.DEFAULT_CODECS, jc ).get();

                assertEquals( FPListVarArgHandler.class, actual.getClass() );
                assertEquals( "FPListVarArgHandler(Integer)", actual.toString() );
            }

            @Test
            public void givenFPListOfUnknownTypes_expectNone() {
                JavaClass jc = JavaClass.of( FPList.class );

                assertTrue( VarArgSupport.selectVarArgHandlerFor( StringCodecs.DEFAULT_CODECS, jc ).isEmpty() );
            }

            @Test
            public void givenFPListOfSomethingThatHasNoCodec_expectNone() {
                JavaClass jc = JavaClass.of( FPList.class, VarArgSupportTest.class );

                assertTrue( VarArgSupport.selectVarArgHandlerFor( StringCodecs.DEFAULT_CODECS, jc ).isEmpty() );
            }
        }


        @Nested
        public class SetTestCases {
            @Test
            public void givenSetOfStrings_expectSetVarArgSupportConfiguredForStrings() {
                JavaClass      jc = JavaClass.of( Set.class, String.class );
                VarArgHandler actual   = VarArgSupport.selectVarArgHandlerFor( StringCodecs.DEFAULT_CODECS, jc ).get();

                assertEquals( SetVarArgHandler.class, actual.getClass() );
                assertEquals( "SetVarArgHandler(String)", actual.toString() );
            }

            @Test
            public void givenSetOfInts_expectSetVarArgSupportConfiguredForInts() {
                JavaClass      jc = JavaClass.of( Set.class, Integer.class );
                VarArgHandler actual   = VarArgSupport.selectVarArgHandlerFor( StringCodecs.DEFAULT_CODECS, jc ).get();

                assertEquals( SetVarArgHandler.class, actual.getClass() );
                assertEquals( "SetVarArgHandler(Integer)", actual.toString() );
            }

            @Test
            public void givenSetOfUnknownTypes_expectNone() {
                JavaClass jc = JavaClass.of( Set.class );

                assertTrue( VarArgSupport.selectVarArgHandlerFor( StringCodecs.DEFAULT_CODECS, jc ).isEmpty() );
            }

            @Test
            public void givenSetOfSomethingThatHasNoCodec_expectNone() {
                JavaClass jc = JavaClass.of( Set.class, VarArgSupportTest.class );

                assertTrue( VarArgSupport.selectVarArgHandlerFor( StringCodecs.DEFAULT_CODECS, jc ).isEmpty() );
            }
        }


        @Nested
        public class FPSetTestCases {
            @Test
            public void givenFPSetOfStrings_expectFPSetVarArgSupportConfiguredForStrings() {
                JavaClass      jc = JavaClass.of( FPSet.class, String.class );
                VarArgHandler actual   = VarArgSupport.selectVarArgHandlerFor( StringCodecs.DEFAULT_CODECS, jc ).get();

                assertEquals( FPSetVarArgHandler.class, actual.getClass() );
                assertEquals( "FPSetVarArgHandler(String)", actual.toString() );
            }

            @Test
            public void givenFPSetOfInts_expectFPSetVarArgSupportConfiguredForInts() {
                JavaClass      jc = JavaClass.of( FPSet.class, Integer.class );
                VarArgHandler actual   = VarArgSupport.selectVarArgHandlerFor( StringCodecs.DEFAULT_CODECS, jc ).get();

                assertEquals( FPSetVarArgHandler.class, actual.getClass() );
                assertEquals( "FPSetVarArgHandler(Integer)", actual.toString() );
            }

            @Test
            public void givenFPSetOfUnknownTypes_expectNone() {
                JavaClass jc = JavaClass.of( FPSet.class );

                assertTrue( VarArgSupport.selectVarArgHandlerFor( StringCodecs.DEFAULT_CODECS, jc ).isEmpty() );
            }

            @Test
            public void givenFPSetOfSomethingThatHasNoCodec_expectNone() {
                JavaClass jc = JavaClass.of( FPSet.class, VarArgSupportTest.class );

                assertTrue( VarArgSupport.selectVarArgHandlerFor( StringCodecs.DEFAULT_CODECS, jc ).isEmpty() );
            }
        }


        @Nested
        public class RRBVectorTestCases {
            @Test
            public void givenRRBVectorOfStrings_expectRRBVectorVarArgSupportConfiguredForStrings() {
                JavaClass      jc = JavaClass.of( RRBVector.class, String.class );
                VarArgHandler actual   = VarArgSupport.selectVarArgHandlerFor( StringCodecs.DEFAULT_CODECS, jc ).get();

                assertEquals( RRBVectorVarArgHandler.class, actual.getClass() );
                assertEquals( "RRBVectorVarArgHandler(String)", actual.toString() );
            }

            @Test
            public void givenRRBVectorOfInts_expectRRBVectorVarArgSupportConfiguredForInts() {
                JavaClass      jc = JavaClass.of( RRBVector.class, Integer.class );
                VarArgHandler actual   = VarArgSupport.selectVarArgHandlerFor( StringCodecs.DEFAULT_CODECS, jc ).get();

                assertEquals( RRBVectorVarArgHandler.class, actual.getClass() );
                assertEquals( "RRBVectorVarArgHandler(Integer)", actual.toString() );
            }

            @Test
            public void givenRRBVectorOfUnknownTypes_expectNone() {
                JavaClass jc = JavaClass.of( RRBVector.class );

                assertTrue( VarArgSupport.selectVarArgHandlerFor( StringCodecs.DEFAULT_CODECS, jc ).isEmpty() );
            }

            @Test
            public void givenRRBVectorOfSomethingThatHasNoCodec_expectNone() {
                JavaClass jc = JavaClass.of( RRBVector.class, VarArgSupportTest.class );

                assertTrue( VarArgSupport.selectVarArgHandlerFor( StringCodecs.DEFAULT_CODECS, jc ).isEmpty() );
            }
        }


        @Nested
        public class ArrayTestCases {
            @Test
            public void givenArrayOfStrings_expectArrayVarArgSupportConfiguredForStrings() {
                JavaClass jc = JavaClass.of( String[].class );
                VarArgHandler actual = VarArgSupport.selectVarArgHandlerFor( StringCodecs.DEFAULT_CODECS, jc ).get();

                assertEquals( ArrayVarArgHandler.class, actual.getClass() );
                assertEquals( "ArrayVarArgHandler(String)", actual.toString() );
            }

            @Test
            public void givenArrayOfPrimitiveInts_expectArrayVarArgSupportConfiguredForPrimitiveInts() {
                JavaClass jc = JavaClass.of( int[].class );
                VarArgHandler actual = VarArgSupport.selectVarArgHandlerFor( StringCodecs.DEFAULT_CODECS, jc ).get();

                assertEquals( ArrayVarArgHandler.class, actual.getClass() );
                assertEquals( "ArrayVarArgHandler(int)", actual.toString() );
            }

            @Test
            public void givenArrayOfSomethingThatHasNoCodecs_expectNone() {
                JavaClass jc = JavaClass.of( VarArgSupportTest[].class );

                assertTrue( VarArgSupport.selectVarArgHandlerFor( StringCodecs.DEFAULT_CODECS, jc ).isEmpty() );
            }
        }
    }

}
