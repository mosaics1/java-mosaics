package mosaics.strings.parser;

import mosaics.lang.functions.BooleanFunctionC;
import org.junit.jupiter.api.Test;

import java.util.concurrent.atomic.AtomicBoolean;

import static org.junit.jupiter.api.Assertions.*;


/**
 *
 */
public abstract class CharacterIteratorTestsBase {

// HAS NEXT/NEXT

    @Test
    public void givenEmptyInput_callHasNext_expectFalse() {
        CharacterIterator in = createInputSource("");

        assertFalse( in.hasNext() );
        assertFalse( in.hasNext(1) );
        assertFalse( in.hasNext(2) );
        assertFalse( in.hasNext(3) );
        assertFalse( in.hasNext(4) );
    }

    @Test
    public void givenEmptyInput_callNext_expectException() {
        CharacterIterator in = createInputSource("");

        try {
            in.next();
            fail( "expected IllegalStateException" );
        } catch ( IllegalStateException ex ) {
            assertEquals( "next() called at EOF", ex.getMessage() );
        }
    }

    @Test
    public void givenNonEmptyInput_iterateOverFullLengthOfInput() {
        CharacterIterator in = createInputSource("hello world");

        assertTrue( in.hasNext() );
        assertTrue( in.hasNext(1) );
        assertTrue( in.hasNext(2) );
        assertTrue( in.hasNext(3) );
        assertTrue( in.hasNext(4) );
        assertNextChars( in, "hello world" );
        assertFalse( in.hasNext() );
    }

    @Test
    public void givenNonEmptyInputThatSpansOverTwoFrames_iterateOverFullLengthOfInput() {
        CharacterIterator in = createInputSource("hello world", 6);

        assertTrue( in.hasNext() );
        assertTrue( in.hasNext(1) );
        assertTrue( in.hasNext(2) );
        assertTrue( in.hasNext(3) );
        assertTrue( in.hasNext(4) );
        assertTrue( in.hasNext(5) );
        assertTrue( in.hasNext(6) );
        assertTrue( in.hasNext(7) );
        assertTrue( in.hasNext(8) );
        assertNextChars( in, "hello world" );
        assertFalse( in.hasNext() );
    }

    @Test
    public void givenNonEmptyInputThatSpansOverThreeFrames_iterateOverFullLengthOfInput() {
        CharacterIterator in = createInputSource("hello world", 4);

        assertTrue( in.hasNext() );
        assertTrue( in.hasNext(1) );
        assertTrue( in.hasNext(2) );
        assertTrue( in.hasNext(3) );
        assertTrue( in.hasNext(4) );
        assertTrue( in.hasNext(5) );
        assertTrue( in.hasNext(6) );
        assertTrue( in.hasNext(7) );
        assertTrue( in.hasNext(8) );
        assertNextChars( in, "hello world" );
        assertFalse( in.hasNext() );
    }


// TOSTRING

    @Test
    public void givenEmptyString_callToString_expectEmptyStringBack() {
        CharacterIterator in = createInputSource("");

        assertEquals( "", in.toString() );
    }

    @Test
    public void givenEmptyString_callToString3_expectEmptyStringBack() {
        CharacterIterator in = createInputSource("");

        assertEquals( "", in.pullUpTo(3) );
        assertEquals( "", in.pullRemaining() );
    }

    @Test
    public void givenNonEmptyString_callToString_expectFullStringBack() {
        CharacterIterator in = createInputSource("hello");

        assertEquals( "hello", in.toString() );
    }

    @Test
    public void given5CharString_callToString3_expectFirstThreeCharactersBackOnly() {
        CharacterIterator in = createInputSource("hello");

        assertEquals( "hel", in.pullUpTo(3) );
        assertEquals( "lo", in.pullRemaining() );
    }

    @Test
    public void given2CharString_callToString3_expectFirstTwoCharactersBackOnly() {
        CharacterIterator in = createInputSource("Hi");

        assertEquals( "Hi", in.pullUpTo(3) );
        assertEquals( "", in.pullRemaining() );
    }


// COUNT WHILE

    @Test
    public void givenEmptyString_callCountWhile_expectCountWhileToNotBeCalledAndTheReturnToBeZero() {
        CharacterIterator in = createInputSource("");

        AtomicBoolean flag = new AtomicBoolean();
        assertEquals( 0, in.countWhile(c -> {flag.set(true); return false;}) );
        assertFalse( flag.get() );
    }

    @Test
    public void given5CharString_callCountWhileThatAlwaysReturnsTrue_expect5() {
        CharacterIterator in = createInputSource("hello");

        assertEquals( 5, in.countWhile(c -> true) );
    }

    @Test
    public void given5CharString_callCountWhileThatAlwaysReturnsFalse_expect0() {
        CharacterIterator in = createInputSource("hello");

        assertEquals( 0, in.countWhile(c -> false) );
    }

    @Test
    public void given5CharString_callCountWhileThatTrueOnce_expect1() {
        CharacterIterator in = createInputSource("hello");

        assertEquals( 1, in.countWhile( new BooleanFunctionC() {
            private int count = 0;

            public boolean invoke( char c ) {
                count++;

                return count <= 1;
            }
        } ) );
    }

    @Test
    public void given5CharString_callCountWhileThatTrueTwice_expect2() {
        CharacterIterator in = createInputSource("hello");

        assertEquals( 2, in.countWhile( new BooleanFunctionC() {
            private int count = 0;

            public boolean invoke( char c ) {
                count++;

                return count <= 2;
            }
        } ) );
    }

    @Test
    public void given5CharString_callCountWhileThatTrueThrice_expect3() {
        CharacterIterator in = createInputSource("hello");

        assertEquals( 3, in.countWhile( new BooleanFunctionC() {
            private int count = 0;

            public boolean invoke( char c ) {
                count++;

                return count <= 3;
            }
        } ) );
    }


// PEEK

    @Test
    public void givenNonEmptyString_peek_expectNextCharAndThePositionToNotBeModified() {
        CharacterIterator in = createInputSource("");

        try {
            in.peek();
            fail( "expected IllegalStateException" );
        } catch ( IllegalStateException ex ) {
            assertEquals( "peek() called at EOF", ex.getMessage() );
        }
    }

    @Test
    public void givenEmptyString_peek_expectException() {
        CharacterIterator in = createInputSource("hello");

        assertEquals( 'h', in.peek() );
        assertEquals( "hello", in.toString() );
    }

// PULL

    @Test
    public void givenEmptyString_pullNegativeCharacter_expectException() {
        CharacterIterator in = createInputSource("");

        try {
            in.pull(-1);
            fail( "expected IllegalArgumentException" );
        } catch ( IllegalArgumentException ex ) {
            assertEquals( "'numChars' (-1) must be >= 0", ex.getMessage() );
        }
    }

    @Test
    public void givenNonEmptyString_pullNegativeCharacter_expectException() {
        CharacterIterator in = createInputSource("hello");

        try {
            in.pull(-1);
            fail( "expected IllegalArgumentException" );
        } catch ( IllegalArgumentException ex ) {
            assertEquals( "'numChars' (-1) must be >= 0", ex.getMessage() );
        }
    }

    @Test
    public void givenEmptyString_pullZeroCharacters_expectEmptyString() {
        CharacterIterator in = createInputSource("" );

        assertEquals( "", in.pull(0) );
        assertFalse( in.hasNext() );
    }

    @Test
    public void givenEmptyString_pullOneCharacter_expectException() {
        CharacterIterator in = createInputSource("");

        try {
            in.pull(1);
            fail( "expected IllegalStateException" );
        } catch ( IllegalStateException ex ) {
            assertEquals( "pull(1) crosses EOF", ex.getMessage() );
        }
    }

    @Test
    public void givenNonEmptyString_pullZeroCharacters_exceptEmptyString() {
        CharacterIterator in = createInputSource("hello world" );

        assertEquals( "", in.pull(0) );
        assertTrue( in.hasNext() );
    }

    @Test
    public void givenNonEmptyString_pullOneCharacter_exceptString() {
        CharacterIterator in = createInputSource("hello world" );

        assertEquals( "h", in.pull(1) );
        assertTrue( in.hasNext() );
    }

    @Test
    public void givenNonEmptyString_pullTwoCharacters_exceptString() {
        CharacterIterator in = createInputSource("hello world" );

        assertEquals( "he", in.pull(2) );
        assertTrue( in.hasNext() );
    }

    @Test
    public void givenNonEmptyString_pullThreeCharacters_exceptString() {
        CharacterIterator in = createInputSource("hello world" );

        assertEquals( "hel", in.pull(3) );
        assertTrue( in.hasNext() );
    }

    @Test
    public void givenNonEmptyString_pullEntireStringInOnesCrossingMultipleFrames() {
        CharacterIterator in = createInputSource("hello world",2 );

        assertEquals( "h", in.pull(1) );
        assertEquals( "e", in.pull(1) );
        assertEquals( "l", in.pull(1) );
        assertEquals( "l", in.pull(1) );
        assertEquals( "o", in.pull(1) );
        assertEquals( " ", in.pull(1) );
        assertEquals( "w", in.pull(1) );
        assertEquals( "o", in.pull(1) );
        assertEquals( "r", in.pull(1) );
        assertEquals( "l", in.pull(1) );
        assertEquals( "d", in.pull(1) );
        assertFalse( in.hasNext() );
    }

    @Test
    public void givenNonEmptyString_pullEntireStringInTwosCrossingMultipleFrames() {
        CharacterIterator in = createInputSource("hello world",2 );

        assertEquals( "he", in.pull(2) );
        assertEquals( "ll", in.pull(2) );
        assertEquals( "o ", in.pull(2) );
        assertEquals( "wo", in.pull(2) );
        assertEquals( "rl", in.pull(2) );
        assertEquals( "d", in.pull(1) );
        assertFalse( in.hasNext() );
    }

    @Test
    public void givenNonEmptyString_pullEntireStringInThreesCrossingMultipleFrames() {
        CharacterIterator in = createInputSource("hello world",2 );

        assertEquals( "hel", in.pull(3) );
        assertEquals( "lo ", in.pull(3) );
        assertEquals( "wor", in.pull(3) );
        assertEquals( "ld", in.pull(2) );
        assertFalse( in.hasNext() );
    }


    @Test
    public void givenNonEmptyString_pullPastEndOfString_expectException() {
        String txt = "hello world";
        CharacterIterator in = createInputSource(txt );

        try {
            in.pull(txt.length()+1 );
            fail( "expected IllegalStateException" );
        } catch ( IllegalStateException ex ) {
            assertEquals( "pull(12) crosses EOF", ex.getMessage() );
        }
    }


    @Test
    public void givenNonEmptyString_pullSomeCharactersAndThenpullPastEndOfString_expectException() {
        String txt = "hello world";
        CharacterIterator in = createInputSource(txt );

        assertEquals( "hel", in.pull(3) );

        try {
            in.pull(txt.length()+1 );
            fail( "expected IllegalStateException" );
        } catch ( IllegalStateException ex ) {
            assertEquals( "pull(12) crosses EOF", ex.getMessage() );
        }
    }


    @Test
    public void givenNonEmptyStringInFragments_pullPastEndOfString_expectException() {
        String txt = "hello world";
        CharacterIterator in = createInputSource(txt, 2);

        try {
            in.pull(txt.length()+1 );
            fail( "expected IllegalStateException" );
        } catch ( IllegalStateException ex ) {
            assertEquals( "pull(12) crosses EOF", ex.getMessage() );
        }
    }


    @Test
    public void givenNonEmptyStringInFragments_pullSomeCharactersAndThenpullPastEndOfString_expectException() {
        String txt = "hello world";
        CharacterIterator in = createInputSource(txt, 2);

        assertEquals( "hel", in.pull(3) );

        try {
            in.pull(txt.length()+1 );
            fail( "expected IllegalStateException" );
        } catch ( IllegalStateException ex ) {
            assertEquals( "pull(12) crosses EOF", ex.getMessage() );
        }
    }


// SKIP


    @Test
    public void givenEmptyString_skipNegativeCharacter_expectException() {
        CharacterIterator in = createInputSource("");

        try {
            in.skip(-1);
            fail( "expected IllegalArgumentException" );
        } catch ( IllegalArgumentException ex ) {
            assertEquals( "'numChars' (-1) must be >= 0", ex.getMessage() );
        }
    }

    @Test
    public void givenNonEmptyString_skipNegativeCharacter_expectException() {
        CharacterIterator in = createInputSource("hello");

        try {
            in.skip(-1);
            fail( "expected IllegalArgumentException" );
        } catch ( IllegalArgumentException ex ) {
            assertEquals( "'numChars' (-1) must be >= 0", ex.getMessage() );
        }
    }

    @Test
    public void givenEmptyString_skipZeroCharacters_expectEmptyString() {
        CharacterIterator in = createInputSource("" );

        in.skip(0);
        assertFalse( in.hasNext() );
    }

    @Test
    public void givenEmptyString_skipOneCharacter_expectException() {
        CharacterIterator in = createInputSource("");

        try {
            in.skip(1);
            fail( "expected IllegalStateException" );
        } catch ( IllegalStateException ex ) {
            assertEquals( "skip(1) crosses EOF", ex.getMessage() );
        }
    }

    @Test
    public void givenNonEmptyString_skipZeroCharacters_exceptEmptyString() {
        CharacterIterator in = createInputSource("hello world" );

        in.skip(0);
        assertTrue( in.hasNext() );
    }

    @Test
    public void givenNonEmptyString_skipOneCharacter_exceptFirstCharacterToBeDropped() {
        CharacterIterator in = createInputSource("hello world" );

        in.skip(1);
        assertEquals( "ello world", in.toString() );
    }

    @Test
    public void givenNonEmptyString_skipTwoCharacters_exceptFirstTwoCharactersToBeDropped() {
        CharacterIterator in = createInputSource("hello world" );

        in.skip(2);
        assertEquals( "llo world", in.toString() );
    }

    @Test
    public void givenNonEmptyString_skipThreeCharacters_exceptFirstThreeCharactersToBeDropped() {
        CharacterIterator in = createInputSource("hello world" );

        in.skip(3);
        assertEquals( "lo world", in.toString() );
    }

    @Test
    public void givenNonEmptyString_skipEntireStringInOnesCrossingMultipleFrames() {
        CharacterIterator in = createInputSource("hello world",2 );

        in.skip(1); // h
        in.skip(1); // e
        in.skip(1); // l
        in.skip(1); // l
        in.skip(1); // o
        in.skip(1); //
        in.skip(1); // w
        in.skip(1); // o
        in.skip(1); // r
        in.skip(1); // l
        in.skip(1); // d
        assertFalse( in.hasNext() );
    }

    @Test
    public void givenNonEmptyString_skipEntireStringInTwosCrossingMultipleFrames() {
        CharacterIterator in = createInputSource("hello world",2 );

        in.skip(2);
        in.skip(2);
        in.skip(2);
        in.skip(2);
        in.skip(2);
        in.skip(1);
        assertFalse( in.hasNext() );
    }

    @Test
    public void givenNonEmptyString_skipEntireStringInThreesCrossingMultipleFrames() {
        CharacterIterator in = createInputSource("hello world",2 );

        in.skip(3);
        in.skip(3);
        in.skip(3);
        in.skip(2);
        assertFalse( in.hasNext() );
    }


    @Test
    public void givenNonEmptyString_skipPastEndOfString_expectException() {
        String txt = "hello world";
        CharacterIterator in = createInputSource(txt );

        try {
            in.skip(txt.length()+1 );
            fail( "expected IllegalStateException" );
        } catch ( IllegalStateException ex ) {
            assertEquals( "skip(12) crosses EOF", ex.getMessage() );
        }
    }


    @Test
    public void givenNonEmptyString_skipSomeCharactersAndThenskipPastEndOfString_expectException() {
        String txt = "hello world";
        CharacterIterator in = createInputSource(txt );

        in.skip(3);

        try {
            in.skip(txt.length()-3+1 );
            fail( "expected IllegalStateException" );
        } catch ( IllegalStateException ex ) {
            assertEquals( "skip(9) crosses EOF", ex.getMessage() );
        }
    }


    @Test
    public void givenNonEmptyStringInFragments_skipPastEndOfString_expectException() {
        String txt = "hello world";
        CharacterIterator in = createInputSource(txt, 2);

        try {
            in.skip(txt.length()+1 );
            fail( "expected IllegalStateException" );
        } catch ( IllegalStateException ex ) {
            assertEquals( "skip(12) crosses EOF", ex.getMessage() );
        }
    }

    @Test
    public void givenNonEmptyStringInFragments_skipSomeCharactersAndThenskipPastEndOfString_expectException() {
        String txt = "hello world";
        CharacterIterator in = createInputSource(txt, 2);

        in.skip(3);

        try {
            in.skip(txt.length()-3+1 );
            fail( "expected IllegalStateException" );
        } catch ( IllegalStateException ex ) {
            assertEquals( "skip(9) crosses EOF", ex.getMessage() );
        }
    }


// MARK, RETURN TO AND DISCARD POSITIONS

    @Test
    public void givenNonEmptyString_markPositionReadTextThenReturnToStart_expectPositionToBeReset() {
        CharacterIterator in = createInputSource("hello world");

        assertEquals( 0, in.markedPositionCount() );
        in.markPosition();
        assertEquals( 1, in.markedPositionCount() );

        assertEquals( "hello world", in.pullRemaining() );
        assertFalse( in.hasNext() );

        assertEquals( 1, in.markedPositionCount() );
        in.returnToLastMark();
        assertEquals( 0, in.markedPositionCount() );
        assertTrue( in.hasNext() );

        assertEquals( "hello world", in.pullRemaining() );
        assertFalse( in.hasNext() );
    }

    @Test
    public void givenNonEmptyString_readAFewCharactersMarkPositionReadTextThenReturnToStart_expectPositionToBeResetToMarkPoint() {
        CharacterIterator in = createInputSource("hello world");

        assertEquals( "he", in.pull( 2 ) );

        assertEquals( 0, in.markedPositionCount() );
        in.markPosition();
        assertEquals( 1, in.markedPositionCount() );

        assertEquals( "llo world", in.pullRemaining() );
        assertFalse( in.hasNext() );

        assertEquals( 1, in.markedPositionCount() );
        in.returnToLastMark();
        assertEquals( 0, in.markedPositionCount() );
        assertTrue( in.hasNext() );

        assertEquals( "llo world", in.pullRemaining() );
        assertFalse( in.hasNext() );
    }

    @Test
    public void givenNonEmptyString_readAFewCharactersMarkPositionReadTextThenDiscardMark_expectPositionNotToBeReset() {
        CharacterIterator in = createInputSource("hello world");

        assertEquals( "he", in.pull( 2 ) );

        assertEquals( 0, in.markedPositionCount() );
        in.markPosition();
        assertEquals( 1, in.markedPositionCount() );

        assertEquals( "llo world", in.pullRemaining() );
        assertFalse( in.hasNext() );

        assertEquals( 1, in.markedPositionCount() );
        in.discardLastMark();
        assertEquals( 0, in.markedPositionCount() );
        assertFalse( in.hasNext() );
    }

    @Test
    public void givenNonEmptyStringWithMultipleFrames_markPositionReadTextThenReturnToStart_expectPositionToBeReset() {
        CharacterIterator in = createInputSource("hello world", 2);

        assertEquals( 0, in.markedPositionCount() );
        in.markPosition();
        assertEquals( 1, in.markedPositionCount() );

        assertEquals( "hello world", in.pullRemaining() );
        assertFalse( in.hasNext() );

        assertEquals( 1, in.markedPositionCount() );
        in.returnToLastMark();
        assertEquals( 0, in.markedPositionCount() );
        assertTrue( in.hasNext() );

        assertEquals( "hello world", in.pullRemaining() );
        assertFalse( in.hasNext() );
    }

    @Test
    public void givenNonEmptyStringWithMultipleFrames_readAFewCharactersMarkPositionReadTextThenReturnToStart_expectPositionToBeResetToMarkPoint() {
        CharacterIterator in = createInputSource("hello world", 2);

        assertEquals( "he", in.pull( 2 ) );

        assertEquals( 0, in.markedPositionCount() );
        in.markPosition();
        assertEquals( 1, in.markedPositionCount() );

        assertEquals( "llo world", in.pullRemaining() );
        assertFalse( in.hasNext() );

        assertEquals( 1, in.markedPositionCount() );
        in.returnToLastMark();
        assertEquals( 0, in.markedPositionCount() );
        assertTrue( in.hasNext() );

        assertEquals( "llo world", in.pullRemaining() );
        assertFalse( in.hasNext() );
    }

    @Test
    public void givenNonEmptyStringWithMultipleFrames_readAFewCharactersMarkPositionReadTextThenDiscardMark_expectPositionNotToBeReset() {
        CharacterIterator in = createInputSource("hello world", 2);

        assertEquals( "he", in.pull( 2 ) );

        assertEquals( 0, in.markedPositionCount() );
        in.markPosition();
        assertEquals( 1, in.markedPositionCount() );

        assertEquals( "llo world", in.pullRemaining() );
        assertFalse( in.hasNext() );

        assertEquals( 1, in.markedPositionCount() );
        in.discardLastMark();
        assertEquals( 0, in.markedPositionCount() );
        assertFalse( in.hasNext() );
    }




    protected abstract CharacterIterator createInputSource( String txt );

    protected abstract CharacterIterator createInputSource( String txt, int bufSize );

    protected void assertNextChars( CharacterIterator in, String expected ) {
        char[] expectedChars = expected.toCharArray();

        for ( char expectedChar : expectedChars ) {
            assertTrue( in.hasNext() );
            assertEquals( expectedChar, in.next() );
        }
    }

}
