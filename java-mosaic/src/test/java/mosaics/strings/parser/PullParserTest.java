package mosaics.strings.parser;

import mosaics.fp.FP;
import mosaics.fp.collections.FPIterator;
import mosaics.fp.collections.FPOption;
import org.junit.jupiter.api.Test;

import static mosaics.lang.QA.fail;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class PullParserTest {

// Standard uses of pull()

    @Test
    public void parseSingleLineInParts() {
        PullParser p = newPullParser("hello world");

        assertPosition( 1,1,0, p.getPosition() );

        assertEquals( "hello", p.pullOptional( CharacterMatchers.constant("hello")).get() );
        assertPosition( 1,6,5, p.getPosition() );

        assertEquals( " ", p.pullOptional( CharacterMatchers.constant(" ")).get() );
        assertPosition( 1,7,6, p.getPosition() );

        assertEquals( "world", p.pullOptional( CharacterMatchers.constant("world")).get() );
        assertPosition( 1,12,11, p.getPosition() );
    }

    @Test
    public void failToParsePartOfAString_expectThePullParsersPositionToNotChange() {
        PullParser p = newPullParser("hello world");

        assertPosition( 1,1,0, p.getPosition() );

        assertEquals( "hello", p.pullOptional( CharacterMatchers.constant("hello")).get() );
        assertPosition( 1,6,5, p.getPosition() );

        assertEquals( FPOption.none(), p.pullOptional( CharacterMatchers.constant( "hello" ) ) );
        assertPosition( 1,6,5, p.getPosition() );

        assertEquals( " world", p.pullOptional( CharacterMatchers.constant(" world")).get() );
        assertPosition( 1,12,11, p.getPosition() );
    }

    @Test
    public void parseAcrossALineBoundary_expectLineNumberToIncrement() {
        PullParser p = newPullParser("hello jim\nHow are we today?");

        assertPosition( 1,1,0, p.getPosition() );

        assertEquals( "hello jim\n", p.pullOptional( CharacterMatchers.constant("hello jim\n")).get() );
        assertPosition( 2,1,10, p.getPosition() );

        assertEquals( "How are we today?", p.pullOptional( CharacterMatchers.constant("How are we today?")).get() );
        assertPosition( 2,18,27, p.getPosition() );
    }

    @Test
    public void startParsingAfterLineBoundary_expectPositionToStartOnSecondLine() {
        PullParser p = newPullParser("hello jim\nHow are we today?");

        assertPosition( 1,1,0, p.getPosition() );

        assertEquals( "hello jim\n", p.pullOptional( CharacterMatchers.constant("hello jim\n")).get() );
        assertPosition( 2,1,10, p.getPosition() );

        assertEquals( "How are we today?", p.pullOptional( CharacterMatchers.constant("How are we today?")).get() );
        assertPosition( 2,18,27, p.getPosition() );
    }

    @Test
    public void parseAcrossADoubleCharLineBoundary_expectLineNumberToIncrement() {
        PullParser p = newPullParser("hello jim\r\nHow are we today?");

        assertPosition( 1,1,0, p.getPosition() );

        assertEquals( "hello jim\r\n", p.pullOptional( CharacterMatchers.constant("hello jim\r\n")).get() );
        assertPosition( 2,1,11, p.getPosition() );

        assertEquals( "How are we today?", p.pullOptional( CharacterMatchers.constant("How are we today?")).get() );
        assertPosition( 2,18,28, p.getPosition() );
    }

    @Test
    public void tryToParseAfterEOL_expectNoMatch() {
        PullParser p = newPullParser("abc");

        assertPosition( 1,1,0, p.getPosition() );

        assertEquals( FPOption.none(), p.pullOptional( CharacterMatchers.constant("abcd")) );
        assertPosition( 1,1,0, p.getPosition() );
    }

// pull() can auto skip characters (but not by default)

    @Test
    public void autoSkipWhitespace() {
        PullParser p = newPullParser("hello world");
        p.setSkipMatcher( CharacterMatchers.whitespace() );

        assertPosition( 1,1,0, p.getPosition() );

        assertEquals( "hello", p.pullOptional( CharacterMatchers.constant("hello")).get() );
        assertPosition( 1,6,5, p.getPosition() );

        assertEquals( "world", p.pullOptional( CharacterMatchers.constant("world")).get() );
        assertPosition( 1,12,11, p.getPosition() );
    }

// skip using Character2BooleanFunction

    @Test
    public void skipUsingPredicate() {
        PullParser p = newPullParser("setMethod");

        assertPosition( 1,1,0, p.getPosition() );

        assertEquals( 3, p.skipWhile(Character::isLowerCase) );
        assertPosition( 1,4,3, p.getPosition() );
    }

// explicitly skip characters (cheaper than calling pull() when we do not care about the matched text)

    @Test
    public void skipText() {
        PullParser p = newPullParser("hello world");
        p.setSkipMatcher( CharacterMatchers.whitespace() );

        assertPosition( 1,1,0, p.getPosition() );

        assertEquals( 5, p.skip( CharacterMatchers.constant("hello")) );
        assertPosition( 1,6,5, p.getPosition() );

        assertEquals( "world", p.pullOptional( CharacterMatchers.constant("world")).get() );
        assertPosition( 1,12,11, p.getPosition() );
    }

    @Test
    public void mandatorySkipSuccess() {
        PullParser p = newPullParser("hello world");

        p.mandatorySkip( CharacterMatchers.constant( "hello" ) );
        assertPosition( 1,6,5, p.getPosition() );
    }

    @Test
    public void mandatorySkipFail() {
        PullParser p = newPullParser("hello world");

        try {
            p.mandatorySkip( CharacterMatchers.constant( "world" ) );
            fail( "expected parse exception" );
        } catch ( ParseException ex ) {
            assertPosition( 1,1,0, p.getPosition() );
            assertPosition( 1, 1, 0, ex.getPosition() );
            assertEquals( "junit [1,1,0]: Expected 'world' but found 'hello world'", ex.getMessage() );
        }
    }

    @Test
    public void mandatorySkipFailEOF() {
        PullParser p = newPullParser("hello world");

        p.pullToEndOfLine();

        try {
            p.mandatorySkip( CharacterMatchers.constant( "world" ) );
            fail( "expected parse exception" );
        } catch ( ParseException ex ) {
            assertEquals( "junit [1,12,11]: Expected 'world' but found EOF", ex.getMessage() );
        }
    }

// peek

    @Test
    public void peek_expectMatchToNotChangePos() {
        PullParser p = newPullParser("hello world");
        p.setSkipMatcher( CharacterMatchers.whitespace() );

        assertPosition( 1,1,0, p.getPosition() );

        assertTrue( p.peek( CharacterMatchers.constant( "hello" ) ) );
        assertPosition( 1,1,0, p.getPosition() );
    }



    private void assertPosition( int expectedLine, int expectedColumn, int expectedOffset, CharacterPosition actualPosition ) {
        assertEquals( expectedLine, actualPosition.getLineNumber() );
        assertEquals( expectedColumn, actualPosition.getColumnNumber() );
        assertEquals( expectedOffset, actualPosition.getCharacterOffset() );
    }

// peekNextChar

    @Test
    public void peekNextChar() {
        PullParser p = newPullParser("hello world");
        p.setSkipMatcher( CharacterMatchers.whitespace() );

        assertPosition( 1,1,0, p.getPosition() );

        assertEquals( 'h', p.peek() );
        assertEquals( 'h', p.peek() );
        assertTrue( p.hasMore() );

        assertPosition( 1,1,0, p.getPosition() );
    }


// split

    @Test
    public void splitEmptyString_expectEmptyIterable() {
        CharacterMatcher delimiter = CharacterMatchers.constant( '/');
        PullParser p = newPullParser("");

        FPIterator<String> result = p.split(delimiter);

        assertEquals( FP.emptyIterator().mkString(), result.mkString() );
    }

    @Test
    public void splitStringThatOnlyHoldsTheSeparator_expectEmptyIterable() {
        CharacterMatcher delimiter = CharacterMatchers.constant( '/');
        PullParser p = newPullParser("/");

        FPIterator<String> result = p.split(delimiter);

        assertEquals( FP.emptyIterator().mkString(), result.mkString() );
    }

    @Test
    public void splitStringThatHoldsAnyTextOtherThanTheSeparator_expectWholeTextReturnedInFirstElementOfIterator() {
        CharacterMatcher delimiter = CharacterMatchers.constant( '/');
        PullParser p = newPullParser("foo");

        FPIterator<String> result = p.split(delimiter);

        assertEquals( FPIterator.wrap("foo").mkString(), result.mkString() );
    }

    @Test
    public void splitStringThatHoldsMixOfTextSeparatorText_expectEachOfTheTextBlocksToBeReturned() {
        CharacterMatcher delimiter = CharacterMatchers.constant( '/');
        PullParser p = newPullParser("foo/bar/tar/");

        FPIterator<String> result = p.split(delimiter);

        assertEquals( FPIterator.wrap("foo","bar","tar").mkString(), result.mkString() );
    }

    @Test
    public void splitStringThatHoldsMixOfTextSeparatorAndEscapedSeparatorText_expectEachOfTheTextBlocksToBeReturned() {
        CharacterMatcher delimiter = CharacterMatchers.constant( '/');
        PullParser p = newPullParser("foo\\/bar/tar/");

        FPIterator<String> result = p.split(delimiter, '\\');

        assertEquals( FPIterator.wrap("foo/bar","tar").mkString(), result.mkString() );
    }

    @Test
    public void splitStringThatContainsTheEscapeCharInMidlabel_expectNoEscaping() {
        CharacterMatcher delimiter = CharacterMatchers.constant( '/');
        PullParser p = newPullParser("fo\\o/bar/tar/");

        FPIterator<String> result = p.split(delimiter, '\\');

        assertEquals( FPIterator.wrap("fo\\o","bar","tar").mkString(), result.mkString() );
    }


    private PullParser newPullParser( String t ) {
        return new PullParser( "junit", t );
    }
}
