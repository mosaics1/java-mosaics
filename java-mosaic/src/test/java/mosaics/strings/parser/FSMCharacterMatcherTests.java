package mosaics.strings.parser;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class FSMCharacterMatcherTests {

    @Test
    public void f() {
        assertIntMatch( 0, "" );
        assertIntMatch( 0, "abc" );
        assertIntMatch( 0, "+" );
        assertIntMatch( 0, "-" );
        assertIntMatch( 3, "123" );
        assertIntMatch( 2, "+1" );
        assertIntMatch( 2, "-1" );
        assertIntMatch( 2, "-1a" );
        assertIntMatch( 2, "-1a33" );
    }


    private void assertIntMatch( int expectedLength, String txt ) {
        CharacterMatcher m = new MyIntMatcher();
        CharacterIterator it = CharacterIterators.stringIterator(txt);

        assertEquals( expectedLength, m.matchCount(it) );
    }



    private static class MyIntMatcher extends FSMCharacterMatcher {
        public static final NotMatchedYet  MATCHED_FIRST_LETTER = new NotMatchedYet( MyIntMatcher::matchNextDigit );
        public static final CandidateMatch MATCH_DIGITS         = new CandidateMatch( MyIntMatcher::matchNextDigit );

        // A: matchInitialCharacter
        // +|-    ->  MATCHED_FIRST_LETTER(notValidYet, B)
        // digit  ->  MATCH_DIGITS(candidate, B)

        // B: matchNextDigit
        // digit  -> MATCH_DIGITS(candidate, B)


        public MyIntMatcher( ) {
            super( MyIntMatcher::matchInitialCharacter );
        }



        private static FSMStep matchInitialCharacter( char c ) {
            if ( c == '+' || c == '-' ) {
                return MATCHED_FIRST_LETTER;
            } else if ( c >= '0' && c <= '9' ) {
                return MATCH_DIGITS;
            } else {
                return NO_MATCH;
            }
        }

        private static FSMStep matchNextDigit( char c ) {
            if ( c >= '0' && c <= '9' ) {
                return MATCH_DIGITS;
            } else {
                return NO_MATCH;
            }
        }
    }
}
