package mosaics.strings.parser;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class CharacterMatchersTest {

    @Test
    public void testJdkRegexp() {
        CharacterMatcher matcher = CharacterMatchers.regexp( "[0-9]+" );

        assertIntMatch( 3, matcher, "123" );
        assertIntMatch( 2, matcher, "12" );
        assertIntMatch( 0, matcher, "a12" );
        assertIntMatch( 1, matcher, "2" );
        assertIntMatch( 0, matcher, "" );
    }

    @Test
    public void testConstantCharMatcher() {
        CharacterMatcher matcher = CharacterMatchers.constant( 'r' );

        assertIntMatch( 0, matcher, "1" );
        assertIntMatch( 0, matcher, "123" );
        assertIntMatch( 1, matcher, "rar" );
        assertIntMatch( 0, matcher, "ar" );
        assertIntMatch( 1, matcher, "r" );
        assertIntMatch( 0, matcher, "" );
    }

    @Test
    public void testConstantMatcher() {
        CharacterMatcher matcher = CharacterMatchers.constant( "rar" );

        assertIntMatch( 0, matcher, "1" );
        assertIntMatch( 0, matcher, "123" );
        assertIntMatch( 3, matcher, "rar" );
        assertIntMatch( 0, matcher, "ra" );
        assertIntMatch( 0, matcher, "1rar" );
        assertIntMatch( 3, matcher, "rar1" );
    }

    @Test
    public void testConstantCharacterIgnoreCaseMatcher() {
        CharacterMatcher matcher = CharacterMatchers.constantIgnoreCase( 'r' );

        assertIntMatch( 0, matcher, "a" );
        assertIntMatch( 0, matcher, "b" );
        assertIntMatch( 1, matcher, "rar" );
        assertIntMatch( 1, matcher, "Ra" );
        assertIntMatch( 0, matcher, "1rar" );
        assertIntMatch( 1, matcher, "rar1" );
    }

    @Test
    public void testConstantStringIgnoreCaseMatcher() {
        CharacterMatcher matcher = CharacterMatchers.constantIgnoreCase( "hello" );

        assertIntMatch( 0, matcher, "a" );
        assertIntMatch( 0, matcher, "b" );
        assertIntMatch( 0, matcher, "hel" );
        assertIntMatch( 5, matcher, "hello" );
        assertIntMatch( 5, matcher, "HELLO" );
        assertIntMatch( 5, matcher, "HeLlO" );
        assertIntMatch( 5, matcher, "HeLlO world" );
        assertIntMatch( 0, matcher, "HeL lO" );
    }

    @Test
    public void testInts() {
        CharacterMatcher matcher = CharacterMatchers.integer( false );

        assertIntMatch( 0, matcher, "" );
        assertIntMatch( 1, matcher, "1" );
        assertIntMatch( 1, matcher, "2" );
        assertIntMatch( 2, matcher, "12" );
        assertIntMatch( 3, matcher, "123" );
        assertIntMatch( 5, matcher, "12345" );
        assertIntMatch( 2, matcher, "+1" );
        assertIntMatch( 2, matcher, "-1" );
        assertIntMatch( 4, matcher, "+123" );
        assertIntMatch( 1, matcher, "1+23" );
        assertIntMatch( 2, matcher, "+1+23" );
        assertIntMatch( 0, matcher, "abc" );
        assertIntMatch( 1, matcher, "1,2" );
        assertIntMatch( 1, matcher, "1,221" );
        assertIntMatch( 1, matcher, "1.221" );
    }

    @Test
    public void testFloatingPoints() {
        CharacterMatcher matcher = CharacterMatchers.floatingPointNumber();

        assertIntMatch( 0, matcher, "" );
        assertIntMatch( 1, matcher, "1" );
        assertIntMatch( 1, matcher, "2" );
        assertIntMatch( 2, matcher, "12" );
        assertIntMatch( 3, matcher, "123" );
        assertIntMatch( 5, matcher, "12345" );
        assertIntMatch( 2, matcher, "+1" );
        assertIntMatch( 2, matcher, "-1" );
        assertIntMatch( 4, matcher, "+123" );
        assertIntMatch( 1, matcher, "1+23" );
        assertIntMatch( 2, matcher, "+1+23" );
        assertIntMatch( 0, matcher, "abc" );
        assertIntMatch( 1, matcher, "1,2" );
        assertIntMatch( 1, matcher, "1,221" );
        assertIntMatch( 5, matcher, "1.221" );
        assertIntMatch( 6, matcher, "-1.221" );
        assertIntMatch( 0, matcher, "-1.-221" );
        assertIntMatch( 0, matcher, "-1." );
        assertIntMatch( 4, matcher, "-1.2e1" );
        assertIntMatch( 4, matcher, "-1.2e-1" );
        assertIntMatch( 4, matcher, "-1.2e15" );
        assertIntMatch( 2, matcher, "12e15" );
        assertIntMatch( 2, matcher, "12e-15" );
        assertIntMatch( 2, matcher, "12e--15" );
        assertIntMatch( 2, matcher, "12e-1.5" );
    }

    @Test
    public void testScientificFormat() {
        CharacterMatcher matcher = CharacterMatchers.floatingPointNumber(true);

        assertIntMatch( 0, matcher, "" );
        assertIntMatch( 1, matcher, "1" );
        assertIntMatch( 1, matcher, "2" );
        assertIntMatch( 2, matcher, "12" );
        assertIntMatch( 3, matcher, "123" );
        assertIntMatch( 5, matcher, "12345" );
        assertIntMatch( 2, matcher, "+1" );
        assertIntMatch( 2, matcher, "-1" );
        assertIntMatch( 4, matcher, "+123" );
        assertIntMatch( 1, matcher, "1+23" );
        assertIntMatch( 2, matcher, "+1+23" );
        assertIntMatch( 0, matcher, "abc" );
        assertIntMatch( 1, matcher, "1,2" );
        assertIntMatch( 1, matcher, "1,221" );
        assertIntMatch( 5, matcher, "1.221" );
        assertIntMatch( 6, matcher, "-1.221" );
        assertIntMatch( 0, matcher, "-1.-221" );
        assertIntMatch( 0, matcher, "-1." );
        assertIntMatch( 6, matcher, "-1.2e1" );
        assertIntMatch( 7, matcher, "-1.2e-1" );
        assertIntMatch( 7, matcher, "-1.2e15" );
        assertIntMatch( 5, matcher, "12e15" );
        assertIntMatch( 6, matcher, "12e-15" );
        assertIntMatch( 2, matcher, "12e--15" );
        assertIntMatch( 5, matcher, "12e-1.5" );
    }

    @Test
    public void testCommaSensitiveInts() {
        CharacterMatcher matcher = CharacterMatchers.integer( true );

        assertIntMatch( 0, matcher, "" );
        assertIntMatch( 1, matcher, "1" );
        assertIntMatch( 1, matcher, "2" );
        assertIntMatch( 2, matcher, "12" );
        assertIntMatch( 3, matcher, "123" );
        assertIntMatch( 5, matcher, "12345" );
        assertIntMatch( 2, matcher, "+1" );
        assertIntMatch( 2, matcher, "-1" );
        assertIntMatch( 4, matcher, "+123" );
        assertIntMatch( 1, matcher, "1+23" );
        assertIntMatch( 2, matcher, "+1+23" );
        assertIntMatch( 0, matcher, "abc" );
        assertIntMatch( 0, matcher, "1,2" );
        assertIntMatch( 5, matcher, "1,221" );
        assertIntMatch( 1, matcher, "1.221" );
        assertIntMatch( 0, matcher, ",1,221" );
        assertIntMatch( 6, matcher, "12,345" );
        assertIntMatch( 0, matcher, "12,34,5" );
        assertIntMatch( 0, matcher, "12,345," );
        assertIntMatch( 0, matcher, "12,3456" );
        assertIntMatch( 6, matcher, "12,345a" );
        assertIntMatch( 6, matcher, "123456" );
        assertIntMatch( 7, matcher, "123,456" );
    }

    @Test
    public void testWhitespaceMatcher() {
        CharacterMatcher matcher = CharacterMatchers.whitespace();

        assertIntMatch( 0, matcher, "foo bar" );
        assertIntMatch( 2, matcher, "  foo bar" );
        assertIntMatch( 1, matcher, "\tfoo bar" );
        assertIntMatch( 2, matcher, "\r\nfoo bar" );
    }

    @Test
    public void testEverythingExceptMatcher() {
        CharacterMatcher matcher = CharacterMatchers.everythingExcept( 'o' );

        assertIntMatch( 1, matcher, "foo bar" );
        assertIntMatch( 0, matcher, "" );
        assertIntMatch( 0, matcher, "oo bar" );
        assertIntMatch( 3, matcher, "ab\roo bar" );
    }

    @Test
    public void testJavaVariableNameMatcher() {
        CharacterMatcher matcher = CharacterMatchers.javaVariableName();

        assertIntMatch( 0, matcher, " a " );
        assertIntMatch( 0, matcher, " a" );
        assertIntMatch( 1, matcher, "a" );
        assertIntMatch( 1, matcher, "F" );
        assertIntMatch( 1, matcher, "a.b.c" );
        assertIntMatch( 0, matcher, "$abc" );
        assertIntMatch( 3, matcher, "abc" );
        assertIntMatch( 4, matcher, "_abc" );
        assertIntMatch( 0, matcher, "12abc" );
        assertIntMatch( 5, matcher, "ab12c" );
        assertIntMatch( 5, matcher, "abc12" );
    }

    @Test
    public void testJavaVariableTypeMatcher() {
        CharacterMatcher matcher = CharacterMatchers.javaVariableType();

        assertIntMatch( 0, matcher, " a " );
        assertIntMatch( 0, matcher, " a" );
        assertIntMatch( 1, matcher, "a" );
        assertIntMatch( 5, matcher, "a.b.c" );
        assertIntMatch( 0, matcher, "$abc" );
        assertIntMatch( 3, matcher, "abc" );
        assertIntMatch( 4, matcher, "_abc" );
        assertIntMatch( 0, matcher, "12abc" );
        assertIntMatch( 5, matcher, "ab12c" );
        assertIntMatch( 5, matcher, "abc12" );
    }

    @Test
    public void testConsumeUptoNCharactersMatcher() {
        CharacterMatcher matcher = CharacterMatchers.consumeUptoNCharacters(2);

        assertIntMatch( 2, matcher, " a " );
        assertIntMatch( 2, matcher, " a" );
        assertIntMatch( 1, matcher, " " );
        assertIntMatch( 0, matcher, "" );
    }

    @Test
    public void testDelimitedText() {
        CharacterMatcher matcher = CharacterMatchers.delimitedText('\"');

        assertIntMatch( 0, matcher, " a " );
        assertIntMatch( 0, matcher, " \"abc\"" );
        assertIntMatch( 5, matcher, "\"abc\"" );
        assertIntMatch( 7, matcher, "\" abc \"" );
        assertIntMatch( 0, matcher, "\" abc " );
        assertIntMatch( 9, matcher, "\" a\\\"bc \"" );
    }


    @Test
    public void testEverythingExcept() {
        CharacterMatcher matcher = CharacterMatchers.everythingExcept("$" );

        assertIntMatch( 1, matcher, " $x" );
        assertIntMatch( 0, matcher, "$x" );
        assertIntMatch( 2, matcher, "  $" );
    }


    private void assertIntMatch( int expectedLength, CharacterMatcher m, String txt ) {
        CharacterIterator it = CharacterIterators.stringIterator(txt);

        assertEquals( expectedLength, m.matchCount(it) );
        assertEquals( txt.length() - expectedLength, it.pullRemaining().length()  );
    }

}
