package mosaics.strings.parser;


import mosaics.fp.FP;
import mosaics.fp.collections.FPOption;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class CharacterMatchers_sequenceTest {

    private CharacterMatcher PREFIX_MATCHER    = CharacterMatchers.constant( '[' );
    private CharacterMatcher ELEMENT_MATCHER   = CharacterMatchers.integer();
    private CharacterMatcher SEPARATOR_MATCHER = CharacterMatchers.constant( ',' );
    private CharacterMatcher POSTFIX_MATCHER   = CharacterMatchers.constant( ']' );

    private CharacterMatcher ARRAY_MATCHER = CharacterMatchers.sequence( PREFIX_MATCHER, ELEMENT_MATCHER, SEPARATOR_MATCHER, POSTFIX_MATCHER );


    @Test
    public void givenBlankString_callSequenceParser_expectNoMatch() {
        PullParser pp = new PullParser( "junit", "" );

        FPOption<String> actual   = pp.pullOptional( ARRAY_MATCHER );
        FPOption<String> expected = FP.emptyOption();

        assertEquals( expected, actual );
    }

    @Test
    public void givenStringThatDoesNotMatchPrefix_callSequenceParser_expectNoMatch() {
        PullParser pp = new PullParser( "junit", "{}" );

        FPOption<String> actual   = pp.pullOptional( ARRAY_MATCHER );
        FPOption<String> expected = FP.emptyOption();

        assertEquals( expected, actual );
    }

    @Test
    public void givenStringThatMatchesPrefixOnly_callSequenceParser_expectNoMatch() {
        PullParser pp = new PullParser( "junit", "[" );

        FPOption<String> actual   = pp.pullOptional( ARRAY_MATCHER );
        FPOption<String> expected = FP.emptyOption();

        assertEquals( expected, actual );
    }

    @Test
    public void givenPrefixPostfix_callSequenceParser_expectMatch() {
        PullParser pp = new PullParser( "junit", "[]" );

        FPOption<String> actual   = pp.pullOptional( ARRAY_MATCHER );
        FPOption<String> expected = FP.option("[]");

        assertEquals( expected, actual );
    }

    @Test
    public void givenWSPrefixWSPostfixWS_callSequenceParser_expectMatch() {
        PullParser pp = new PullParser( "junit", " [ ] " );

        FPOption<String> actual   = pp.pullOptional( ARRAY_MATCHER );
        FPOption<String> expected = FP.option(" [ ] ");

        assertEquals( expected, actual );
    }

    @Test
    public void givenPrefixElementPostfix_callSequenceParser_expectMatch() {
        PullParser pp = new PullParser( "junit", " [ 21 ] " );

        FPOption<String> actual   = pp.pullOptional( ARRAY_MATCHER );
        FPOption<String> expected = FP.option(" [ 21 ] ");

        assertEquals( expected, actual );
    }

    @Test
    public void givenPrefixElementElementPostfix_callSequenceParser_expectNoMatch() {
        PullParser pp = new PullParser( "junit", " [ 21 41 ] " );

        FPOption<String> actual   = pp.pullOptional( ARRAY_MATCHER );
        FPOption<String> expected = FP.emptyOption();

        assertEquals( expected, actual );
    }

    @Test
    public void givenPrefixElementSeparatorElementPostfix_callSequenceParser_expectMatch() {
        PullParser pp = new PullParser( "junit", "[21,41]" );

        FPOption<String> actual   = pp.pullOptional( ARRAY_MATCHER );
        FPOption<String> expected = FP.option("[21,41]");

        assertEquals( expected, actual );
    }

    @Test
    public void givenPrefixElementSeparatorElementPostfixWithWS_callSequenceParser_expectMatch() {
        PullParser pp = new PullParser( "junit", " [ 21 , 41 ] " );

        FPOption<String> actual   = pp.pullOptional( ARRAY_MATCHER );
        FPOption<String> expected = FP.option(" [ 21 , 41 ] ");

        assertEquals( expected, actual );
    }

    @Test
    public void givenPrefixElementSeparatorElementElementPostfix_callSequenceParser_expectNoMatch() {
        PullParser pp = new PullParser( "junit", "[21,41 2]" );

        FPOption<String> actual   = pp.pullOptional( ARRAY_MATCHER );
        FPOption<String> expected = FP.emptyOption();

        assertEquals( expected, actual );
    }

    @Test
    public void givenPrefixElementSeparatorElementSeparatorElementPostfix_callSequenceParser_expectMatch() {
        PullParser pp = new PullParser( "junit", "[21,41,-1]" );

        FPOption<String> actual   = pp.pullOptional( ARRAY_MATCHER );
        FPOption<String> expected = FP.option("[21,41,-1]");

        assertEquals( expected, actual );
    }

    @Test
    public void givenPrefixElementSeparatorElementSeparatorElementSeparatorPostfix_callSequenceParser_expectNoMatch() {
        PullParser pp = new PullParser( "junit", "[21,41,-1,]" );

        FPOption<String> actual   = pp.pullOptional( ARRAY_MATCHER );
        FPOption<String> expected = FP.emptyOption();

        assertEquals( expected, actual );
    }

    @Test
    public void givenPrefixElementSeparatorElementSeparatorElement_callSequenceParser_expectNoMatch() {
        PullParser pp = new PullParser( "junit", "[21,41,-1" );

        FPOption<String> actual   = pp.pullOptional( ARRAY_MATCHER );
        FPOption<String> expected = FP.emptyOption();

        assertEquals( expected, actual );
    }

}
