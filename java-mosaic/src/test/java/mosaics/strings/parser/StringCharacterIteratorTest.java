package mosaics.strings.parser;


/**
 *
 */
public class StringCharacterIteratorTest extends CharacterIteratorTestsBase {

    protected CharacterIterator createInputSource( String txt ) {
        return createInputSource( txt, -1 );
    }

    protected CharacterIterator createInputSource( String txt, int bufSize ) {
        return new StringCharacterIterator( txt );
    }

}
