package mosaics.strings.parser;

import java.io.StringReader;


/**
 *
 */
public class ReaderCharacterIteratorTest extends CharacterIteratorTestsBase {

    protected ReaderCharacterIterator createInputSource( String txt ) {
        return new ReaderCharacterIterator( new StringReader(txt) );
    }

    protected ReaderCharacterIterator createInputSource( String txt, int bufSize ) {
        return new ReaderCharacterIterator( new StringReader(txt), bufSize );
    }

}
