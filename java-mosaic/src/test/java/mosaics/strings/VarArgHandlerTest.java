package mosaics.strings;

import mosaics.fp.collections.FPList;
import mosaics.fp.collections.FPSet;
import mosaics.fp.collections.RRBVector;
import mosaics.lang.reflection.ReflectionException;
import mosaics.utils.SetUtils;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;


public class VarArgHandlerTest {

    @Nested
    public class ObjectArrayVarArgTestCases {
        private StringCodec   codec   = StringCodecs.DEFAULT_CODECS.getCodecFor( Integer.class ).get();
        private VarArgHandler handler = new ArrayVarArgHandler( Object.class, codec );

        @Test
        public void givenNoArgs_expectEmptyArray() {
            Object[] input    = new Object[] {};
            Object[] actual   = (Object[]) handler.aggregate( input, 0 );

            Object[] expected = new Object[] {};

            assertArrayEquals( expected, actual );
        }

        @Test
        public void givenAFewArgsButNoVarArgs_expectEmptyArray() {
            Object[] input    = new Object[] {"a", "b", "c"};
            Object[] actual   = (Object[]) handler.aggregate( input, 3 );

            Object[] expected = new Object[] {};

            assertArrayEquals( expected, actual );
        }

        @Test
        public void givenOneVarArg_expectSingleElementArray() {
            Object[] input    = new Object[] {"12"};
            Object[] actual   = (Object[]) handler.aggregate( input, 0 );

            Object[] expected = new Object[] {12};

            assertArrayEquals( expected, actual );
        }

        @Test
        public void givenOneNonVarArgAndOneVarArg_expectSingleElementArray() {
            Object[] input    = new Object[] {"a", "12"};
            Object[] actual   = (Object[]) handler.aggregate( input, 1 );

            Object[] expected = new Object[] {12};

            assertArrayEquals( expected, actual );
        }

        @Test
        public void givenTwoVarArgs_expectTwoElementArray() {
            Object[] input    = new Object[] {"11", "12"};
            Object[] actual   = (Object[]) handler.aggregate( input, 0 );

            Object[] expected = new Object[] {11,12};

            assertArrayEquals( expected, actual );
        }

        @Test
        public void givenOneNonVarArgAndTwoVarArgs_expectTwoElementArray() {
            Object[] input    = new Object[] {"a", "11", "12"};
            Object[] actual   = (Object[]) handler.aggregate( input, 1 );

            Object[] expected = new Object[] {11,12};

            assertArrayEquals( expected, actual );
        }

        @Test
        public void givenTwoNonVarArgAndTwoVarArgs_expectTwoElementArray() {
            Object[] input    = new Object[] {"a", "b", "11", "12"};
            Object[] actual   = (Object[]) handler.aggregate( input, 2 );

            Object[] expected = new Object[] {11,12};

            assertArrayEquals( expected, actual );
        }

        @Test
        public void givenMalformedVarArg_expectException() {
            Object[] input  = new Object[] {"a", "b", "11", "12"};

            try {
                handler.aggregate( input, 1 );
                fail( "expected exception" );
            } catch ( ReflectionException ex ) {
                assertEquals( "Malformed value 'b' within vararg", ex.getMessage() );
            }
        }
    }

    @Nested
    public class StringArrayVarArgTestCases {
        private StringCodec   codec   = StringCodecs.DEFAULT_CODECS.getCodecFor( String.class ).get();
        private VarArgHandler handler = new ArrayVarArgHandler( String.class, codec );

        @Test
        public void givenNoArgs_expectEmptyArray() {
            Object[] input    = new Object[] {};
            String[] actual   = (String[]) handler.aggregate( input, 0 );

            String[] expected = new String[] {};

            assertArrayEquals( expected, actual );
        }

        @Test
        public void givenAFewArgsButNoVarArgs_expectEmptyArray() {
            Object[] input    = new Object[] {"a", "b", "c"};
            String[] actual   = (String[]) handler.aggregate( input, 3 );

            String[] expected = new String[] {};

            assertArrayEquals( expected, actual );
        }

        @Test
        public void givenOneVarArg_expectSingleElementArray() {
            Object[] input    = new Object[] {"12"};
            String[] actual   = (String[]) handler.aggregate( input, 0 );

            String[] expected = new String[] {"12"};

            assertArrayEquals( expected, actual );
        }

        @Test
        public void givenOneNonVarArgAndOneVarArg_expectSingleElementArray() {
            Object[] input    = new Object[] {"a", "12"};
            String[] actual   = (String[]) handler.aggregate( input, 1 );

            String[] expected = new String[] {"12"};

            assertArrayEquals( expected, actual );
        }

        @Test
        public void givenTwoVarArgs_expectTwoElementArray() {
            Object[] input    = new Object[] {"11", "12"};
            String[] actual   = (String[]) handler.aggregate( input, 0 );

            String[] expected = new String[] {"11","12"};

            assertArrayEquals( expected, actual );
        }

        @Test
        public void givenOneNonVarArgAndTwoVarArgs_expectTwoElementArray() {
            Object[] input    = new Object[] {"a", "11", "12"};
            String[] actual   = (String[]) handler.aggregate( input, 1 );

            String[] expected = new String[] {"11","12"};

            assertArrayEquals( expected, actual );
        }

        @Test
        public void givenTwoNonVarArgAndTwoVarArgs_expectTwoElementArray() {
            Object[] input    = new Object[] {"a", "b", "11", "12"};
            String[] actual   = (String[]) handler.aggregate( input, 2 );

            String[] expected = new String[] {"11","12"};

            assertArrayEquals( expected, actual );
        }
    }

    @Nested
    public class PrimitiveIntArrayVarArgTestCases {
        private StringCodec   codec   = StringCodecs.DEFAULT_CODECS.getCodecFor( Integer.TYPE ).get();
        private VarArgHandler handler = new ArrayVarArgHandler( Integer.TYPE, codec );

        @Test
        public void givenNoArgs_expectEmptyArray() {
            Object[] input    = new Object[] {};
            int[]    actual   = (int[]) handler.aggregate( input, 0 );

            int[]    expected = new int[] {};

            assertArrayEquals( expected, actual );
        }

        @Test
        public void givenAFewArgsButNoVarArgs_expectEmptyArray() {
            Object[] input    = new Object[] {"a", "b", "c"};
            int[]    actual   = (int[]) handler.aggregate( input, 3 );

            int[]    expected = new int[] {};

            assertArrayEquals( expected, actual );
        }

        @Test
        public void givenOneVarArg_expectSingleElementArray() {
            Object[] input    = new Object[] {"12"};
            int[]    actual   = (int[]) handler.aggregate( input, 0 );

            int[]    expected = new int[] {12};

            assertArrayEquals( expected, actual );
        }

        @Test
        public void givenOneNonVarArgAndOneVarArg_expectSingleElementArray() {
            Object[] input    = new Object[] {"a", "12"};
            int[]    actual   = (int[]) handler.aggregate( input, 1 );

            int[]    expected = new int[] {12};

            assertArrayEquals( expected, actual );
        }

        @Test
        public void givenTwoVarArgs_expectTwoElementArray() {
            Object[] input    = new Object[] {"11", "12"};
            int[]    actual   = (int[]) handler.aggregate( input, 0 );

            int[]    expected = new int[] {11,12};

            assertArrayEquals( expected, actual );
        }

        @Test
        public void givenOneNonVarArgAndTwoVarArgs_expectTwoElementArray() {
            Object[] input    = new Object[] {"a", "11", "12"};
            int[]    actual   = (int[]) handler.aggregate( input, 1 );

            int[]    expected = new int[] {11,12};

            assertArrayEquals( expected, actual );
        }

        @Test
        public void givenTwoNonVarArgAndTwoVarArgs_expectTwoElementArray() {
            Object[] input    = new Object[] {"a", "b", "11", "12"};
            int[]    actual   = (int[]) handler.aggregate( input, 2 );

            int[]    expected = new int[] {11,12};

            assertArrayEquals( expected, actual );
        }

        @Test
        public void givenMalformedVarArg_expectException() {
            Object[] input  = new Object[] {"a", "b", "11", "12"};

            try {
                handler.aggregate( input, 1 );
                fail( "expected exception" );
            } catch ( ReflectionException ex ) {
                assertEquals( "Malformed value 'b' within vararg", ex.getMessage() );
            }
        }
    }


    @Nested
    public class ListVarArgHandlerTestCases {
        private StringCodec   codec   = StringCodecs.DEFAULT_CODECS.getCodecFor( Integer.class ).get();
        private VarArgHandler handler = new ListVarArgHandler( codec );

        @Test
        public void givenNoArgs_expectEmptyArray() {
            Object[] input    = new Object[] {};
            Object   actual   = handler.aggregate( input, 0 );

            Object   expected = Collections.emptyList();

            assertEquals( expected, actual );
        }

        @Test
        public void givenAFewArgsButNoVarArgs_expectEmptyArray() {
            Object[] input    = new Object[] {"a", "b", "c"};
            Object   actual   = handler.aggregate( input, 3 );

            Object   expected = Collections.emptyList();

            assertEquals( expected, actual );
        }

        @Test
        public void givenOneVarArg_expectSingleElementArray() {
            Object[] input    = new Object[] {"12"};
            Object   actual   = handler.aggregate( input, 0 );

            Object   expected = Collections.singletonList( 12 );

            assertEquals( expected, actual );
        }

        @Test
        public void givenOneNonVarArgAndOneVarArg_expectSingleElementArray() {
            Object[] input    = new Object[] {"a", "12"};
            Object   actual   = handler.aggregate( input, 1 );

            Object   expected = Collections.singletonList( 12 );

            assertEquals( expected, actual );
        }

        @Test
        public void givenTwoVarArgs_expectTwoElementArray() {
            Object[] input    = new Object[] {"11", "12"};
            Object   actual   = handler.aggregate( input, 0 );

            Object   expected = Arrays.asList(11,12);

            assertEquals( expected, actual );
        }

        @Test
        public void givenOneNonVarArgAndTwoVarArgs_expectTwoElementArray() {
            Object[] input    = new Object[] {"a", "11", "12"};
            Object   actual   = handler.aggregate( input, 1 );

            Object   expected = Arrays.asList(11,12);

            assertEquals( expected, actual );
        }

        @Test
        public void givenTwoNonVarArgAndTwoVarArgs_expectTwoElementArray() {
            Object[] input    = new Object[] {"a", "b", "11", "12"};
            Object   actual   = handler.aggregate( input, 2 );

            Object   expected = Arrays.asList(11,12);

            assertEquals( expected, actual );
        }

        @Test
        public void givenMalformedVarArg_expectException() {
            Object[] input  = new Object[] {"a", "b", "11", "12"};

            try {
                handler.aggregate( input, 1 );
                fail( "expected exception" );
            } catch ( ReflectionException ex ) {
                assertEquals( "Malformed value 'b' within vararg", ex.getMessage() );
            }
        }
    }


    @Nested
    public class FPListVarArgHandlerTestCases {
        private StringCodec   codec   = StringCodecs.DEFAULT_CODECS.getCodecFor( Integer.class ).get();
        private VarArgHandler handler = new FPListVarArgHandler( codec );

        @Test
        public void givenNoArgs_expectEmptyArray() {
            Object[] input    = new Object[] {};
            Object   actual   = handler.aggregate( input, 0 );

            Object   expected = FPList.newArrayList();

            assertEquals( expected, actual );
        }

        @Test
        public void givenAFewArgsButNoVarArgs_expectEmptyArray() {
            Object[] input    = new Object[] {"a", "b", "c"};
            Object   actual   = handler.aggregate( input, 3 );

            Object   expected = FPList.newArrayList();

            assertEquals( expected, actual );
        }

        @Test
        public void givenOneVarArg_expectSingleElementArray() {
            Object[] input    = new Object[] {"12"};
            Object   actual   = handler.aggregate( input, 0 );

            Object   expected = FPList.wrapArray(12);

            assertEquals( expected, actual );
        }

        @Test
        public void givenOneNonVarArgAndOneVarArg_expectSingleElementArray() {
            Object[] input    = new Object[] {"a", "12"};
            Object   actual   = handler.aggregate( input, 1 );

            Object   expected = FPList.wrapArray(12);

            assertEquals( expected, actual );
        }

        @Test
        public void givenTwoVarArgs_expectTwoElementArray() {
            Object[] input    = new Object[] {"11", "12"};
            Object   actual   = handler.aggregate( input, 0 );

            Object   expected = FPList.wrapArray(11, 12);

            assertEquals( expected, actual );
        }

        @Test
        public void givenOneNonVarArgAndTwoVarArgs_expectTwoElementArray() {
            Object[] input    = new Object[] {"a", "11", "12"};
            Object   actual   = handler.aggregate( input, 1 );

            Object   expected = FPList.wrapArray(11, 12);

            assertEquals( expected, actual );
        }

        @Test
        public void givenTwoNonVarArgAndTwoVarArgs_expectTwoElementArray() {
            Object[] input    = new Object[] {"a", "b", "11", "12"};
            Object   actual   = handler.aggregate( input, 2 );

            Object   expected = FPList.wrapArray(11, 12);

            assertEquals( expected, actual );
        }

        @Test
        public void givenMalformedVarArg_expectException() {
            Object[] input  = new Object[] {"a", "b", "11", "12"};

            try {
                handler.aggregate( input, 1 );
                fail( "expected exception" );
            } catch ( ReflectionException ex ) {
                assertEquals( "Malformed value 'b' within vararg", ex.getMessage() );
            }
        }
    }


    @Nested
    public class RRBVectorVarArgHandlerTestCases {
        private StringCodec   codec   = StringCodecs.DEFAULT_CODECS.getCodecFor( Integer.class ).get();
        private VarArgHandler handler = new RRBVectorVarArgHandler( codec );

        @Test
        public void givenNoArgs_expectEmptyArray() {
            Object[] input    = new Object[] {};
            Object   actual   = handler.aggregate( input, 0 );

            Object   expected = RRBVector.toVector();

            assertEquals( expected, actual );
        }

        @Test
        public void givenAFewArgsButNoVarArgs_expectEmptyArray() {
            Object[] input    = new Object[] {"a", "b", "c"};
            Object   actual   = handler.aggregate( input, 3 );

            Object   expected = RRBVector.toVector();

            assertEquals( expected, actual );
        }

        @Test
        public void givenOneVarArg_expectSingleElementArray() {
            Object[] input    = new Object[] {"12"};
            Object   actual   = handler.aggregate( input, 0 );

            Object   expected = RRBVector.toVector(12);

            assertEquals( expected, actual );
        }

        @Test
        public void givenOneNonVarArgAndOneVarArg_expectSingleElementArray() {
            Object[] input    = new Object[] {"a", "12"};
            Object   actual   = handler.aggregate( input, 1 );

            Object   expected = RRBVector.toVector(12);

            assertEquals( expected, actual );
        }

        @Test
        public void givenTwoVarArgs_expectTwoElementArray() {
            Object[] input    = new Object[] {"11", "12"};
            Object   actual   = handler.aggregate( input, 0 );

            Object   expected = RRBVector.toVector(11,12);

            assertEquals( expected, actual );
        }

        @Test
        public void givenOneNonVarArgAndTwoVarArgs_expectTwoElementArray() {
            Object[] input    = new Object[] {"a", "11", "12"};
            Object   actual   = handler.aggregate( input, 1 );

            Object   expected = RRBVector.toVector(11,12);

            assertEquals( expected, actual );
        }

        @Test
        public void givenTwoNonVarArgAndTwoVarArgs_expectTwoElementArray() {
            Object[] input    = new Object[] {"a", "b", "11", "12"};
            Object   actual   = handler.aggregate( input, 2 );

            Object   expected = RRBVector.toVector(11,12);

            assertEquals( expected, actual );
        }

        @Test
        public void givenMalformedVarArg_expectException() {
            Object[] input  = new Object[] {"a", "b", "11", "12"};

            try {
                handler.aggregate( input, 1 );
                fail( "expected exception" );
            } catch ( ReflectionException ex ) {
                assertEquals( "Malformed value 'b' within vararg", ex.getMessage() );
            }
        }
    }


    @Nested
    public class SetVarArgHandlerTestCases {
        private StringCodec   codec   = StringCodecs.DEFAULT_CODECS.getCodecFor( Integer.class ).get();
        private VarArgHandler handler = new SetVarArgHandler( codec );

        @Test
        public void givenNoArgs_expectEmptyArray() {
            Object[] input    = new Object[] {};
            Object   actual   = handler.aggregate( input, 0 );

            Object   expected = Collections.emptySet();

            assertEquals( expected, actual );
        }

        @Test
        public void givenAFewArgsButNoVarArgs_expectEmptyArray() {
            Object[] input    = new Object[] {"a", "b", "c"};
            Object   actual   = handler.aggregate( input, 3 );

            Object   expected = Collections.emptySet();

            assertEquals( expected, actual );
        }

        @Test
        public void givenOneVarArg_expectSingleElementArray() {
            Object[] input    = new Object[] {"12"};
            Object   actual   = handler.aggregate( input, 0 );

            Object   expected = SetUtils.asSet(12);

            assertEquals( expected, actual );
        }

        @Test
        public void givenOneNonVarArgAndOneVarArg_expectSingleElementArray() {
            Object[] input    = new Object[] {"a", "12"};
            Object   actual   = handler.aggregate( input, 1 );

            Object   expected = SetUtils.asSet(12);

            assertEquals( expected, actual );
        }

        @Test
        public void givenTwoVarArgs_expectTwoElementArray() {
            Object[] input    = new Object[] {"11", "12"};
            Object   actual   = handler.aggregate( input, 0 );

            Object   expected = SetUtils.asSet(11, 12);

            assertEquals( expected, actual );
        }

        @Test
        public void givenOneNonVarArgAndTwoVarArgs_expectTwoElementArray() {
            Object[] input    = new Object[] {"a", "11", "12"};
            Object   actual   = handler.aggregate( input, 1 );

            Object   expected = SetUtils.asSet(11, 12);

            assertEquals( expected, actual );
        }

        @Test
        public void givenTwoNonVarArgAndTwoVarArgs_expectTwoElementArray() {
            Object[] input    = new Object[] {"a", "b", "11", "12"};
            Object   actual   = handler.aggregate( input, 2 );

            Object   expected = SetUtils.asSet(11, 12);

            assertEquals( expected, actual );
        }

        @Test
        public void givenMalformedVarArg_expectException() {
            Object[] input  = new Object[] {"a", "b", "11", "12"};

            try {
                handler.aggregate( input, 1 );
                fail( "expected exception" );
            } catch ( ReflectionException ex ) {
                assertEquals( "Malformed value 'b' within vararg", ex.getMessage() );
            }
        }
    }


    @Nested
    public class FPSetVarArgHandlerTestCases {
        private StringCodec   codec   = StringCodecs.DEFAULT_CODECS.getCodecFor( Integer.class ).get();
        private VarArgHandler handler = new FPSetVarArgHandler( codec );

        @Test
        public void givenNoArgs_expectEmptyArray() {
            Object[] input    = new Object[] {};
            Object   actual   = handler.aggregate( input, 0 );

            Object   expected = FPSet.wrapArray();

            assertEquals( expected, actual );
        }

        @Test
        public void givenAFewArgsButNoVarArgs_expectEmptyArray() {
            Object[] input    = new Object[] {"a", "b", "c"};
            Object   actual   = handler.aggregate( input, 3 );

            Object   expected = FPSet.wrapArray();

            assertEquals( expected, actual );
        }

        @Test
        public void givenOneVarArg_expectSingleElementArray() {
            Object[] input    = new Object[] {"12"};
            Object   actual   = handler.aggregate( input, 0 );

            Object   expected = FPSet.wrapArray(12);

            assertEquals( expected, actual );
        }

        @Test
        public void givenOneNonVarArgAndOneVarArg_expectSingleElementArray() {
            Object[] input    = new Object[] {"a", "12"};
            Object   actual   = handler.aggregate( input, 1 );

            Object   expected = FPSet.wrapArray(12);

            assertEquals( expected, actual );
        }

        @Test
        public void givenTwoVarArgs_expectTwoElementArray() {
            Object[] input    = new Object[] {"11", "12"};
            Object   actual   = handler.aggregate( input, 0 );

            Object   expected = FPSet.wrapArray(11, 12);

            assertEquals( expected, actual );
        }

        @Test
        public void givenOneNonVarArgAndTwoVarArgs_expectTwoElementArray() {
            Object[] input    = new Object[] {"a", "11", "12"};
            Object   actual   = handler.aggregate( input, 1 );

            Object   expected = FPSet.wrapArray(11, 12);

            assertEquals( expected, actual );
        }

        @Test
        public void givenTwoNonVarArgAndTwoVarArgs_expectTwoElementArray() {
            Object[] input    = new Object[] {"a", "b", "11", "12"};
            Object   actual   = handler.aggregate( input, 2 );

            Object   expected = FPSet.wrapArray(11, 12);

            assertEquals( expected, actual );
        }

        @Test
        public void givenMalformedVarArg_expectException() {
            Object[] input  = new Object[] {"a", "b", "11", "12"};

            try {
                handler.aggregate( input, 1 );
                fail( "expected exception" );
            } catch ( ReflectionException ex ) {
                assertEquals( "Malformed value 'b' within vararg", ex.getMessage() );
            }
        }
    }
}
