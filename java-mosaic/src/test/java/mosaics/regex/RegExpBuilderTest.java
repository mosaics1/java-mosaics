package mosaics.regex;

import org.junit.jupiter.api.Test;

import static mosaics.regex.RegExpBuilder.escape;
import static mosaics.regex.RegExpBuilder.re;
import static org.junit.jupiter.api.Assertions.*;


public class RegExpBuilderTest {

    @Test
    public void testMixStaticAndRegExpBlocks() {
        RegExp regExp = new RegExpBuilder()
            .appendStatic( "[" )
            .appendRegExp( "a.z" )
            .appendStatic( "]*" )
            .build();

        assertTrue( regExp.matches("[a-z]*") );
        assertTrue( regExp.matches("[abz]*") );
        assertFalse( regExp.matches("[a]*") );
        assertFalse( regExp.matches("a") );
        assertFalse( regExp.matches("abcd") );
    }

    @Test
    public void testMixStaticAndRegExpBlocksUsingFactoryMethod() {
        RegExp regExp = RegExpBuilder.createRegExp(escape("["), re("a.z"), escape("]*"));

        assertTrue( regExp.matches("[a-z]*") );
        assertTrue( regExp.matches("[abz]*") );
        assertFalse( regExp.matches("[a]*") );
        assertFalse( regExp.matches("a") );
        assertFalse( regExp.matches("abcd") );
    }

}
