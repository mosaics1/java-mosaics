package mosaics.regex;

import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;


@SuppressWarnings("ConstantConditions")
public class RegExpTest {
    @Test
    public void extractFrom() {
        assertEquals( "bc", new RegExp("bc").extractMatchFrom("abcd").get() );
        assertEquals( "ll", new RegExp("ll+").extractMatchFrom("hello").get() );
        assertEquals( "bc", new RegExp("bc").extractMatchFrom( "abcd" ).get() );
        assertEquals( "Foo", new RegExp("class[ \t\n\r]+([a-zA-Z$0-9_-]+)").extractMatchFrom("public class \t Foo {",1).get() );
    }

    @Test
    public void caseInsensitive() {
        assertEquals( Optional.empty(), new RegExp("bc").extractMatchFrom("bC") );
        assertEquals( "bC", new RegExp("bc").setCaseInsenitive(true).extractMatchFrom("bC").get() );
    }

    @Test
    public void toStringTest() {
        assertEquals( "/\\Qbc\\E/", new RegExp("bc").toString() );
        assertEquals( "/\\Qbc\\E/i", new RegExp("bc").setCaseInsenitive(true).toString() );
    }

    @Test
    public void caseSensitiveMatches() {
        assertTrue( new RegExp("bc").matches("bc") );
        assertTrue( new RegExp("bc").setCaseInsenitive(false).matches("bc") );

        assertFalse( new RegExp("bc").matches("bC") );
        assertFalse( new RegExp("bc").setCaseInsenitive(false).matches("bC") );
    }

    @Test
    public void caseInsensitiveMatches() {
        assertTrue( new RegExp("bc").setCaseInsenitive(true).matches("bc") );
        assertTrue( new RegExp("bc").setCaseInsenitive(true).matches("bC") );
        assertTrue( new RegExp("bc").setCaseInsenitive(true).matches("BC") );
    }

    @Test
    public void count() {
        assertEquals( 0, new RegExp("bc").count("aaaa") );
        assertEquals( 1, new RegExp("bc").count("abca") );
        assertEquals( 1, new RegExp("bc").setCaseInsenitive(true).count("BC") );
        assertEquals( 2, new RegExp("bc").count("abcabc") );
        assertEquals( 3, new RegExp("bc").count("abcabcbc") );
    }
}
