package mosaics.cache;

import mosaics.fp.FP;
import mosaics.junit.JMAssertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.parallel.Isolated;
import org.opentest4j.AssertionFailedError;

import java.lang.ref.Reference;
import java.lang.ref.WeakReference;


@Isolated // due to use of createMemoryPressure
public class SoftCacheTest extends JMAssertions {
    private static record Book(String name) {}

    @Test
    public void storeValueInSoftCacheAndLetItGetGCdOut() {
        SoftCache<String,Book> cache = new SoftCache<>();

        Book            book = new Book("LOTR");
        Reference<Book> ref  = new WeakReference<>( book );

        cache.put( "lotr", book );

        assertEquals( book, cache.getMandatory("lotr") );

        book = null;

        createMemoryPressure();

        spinUntilReleased( ref );

        assertEquals( FP.emptyOption(), cache.get("lotr") );
    }

    @Test
    public void showThatValueDoesNotGetGCdWhenThereIsNoMemoryPressure() {
        SoftCache<String,Book> cache = new SoftCache<>();

        Book            book = new Book("LOTR");
        Reference<Book> ref  = new WeakReference<>( book );

        cache.put( "lotr", book );

        assertEquals( book, cache.getMandatory("lotr") );

        book = null;

        try {
            spinUntilReleased( ref );
            fail( "expected failure" );
        } catch ( AssertionFailedError ex ) {

        }

        assertTrue( cache.get("lotr").hasValue() );
    }
}
