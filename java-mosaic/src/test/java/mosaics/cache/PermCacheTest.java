package mosaics.cache;

import mosaics.fp.FP;
import mosaics.junit.JMAssertions;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.lang.ref.Reference;
import java.lang.ref.WeakReference;


public class PermCacheTest extends JMAssertions  {
    private static record Book(String title) {}

    private static final Book BOOK1 = new Book("title 1");
    private static final Book BOOK2 = new Book("title 2");

    private Cache<String, Book> cache = new PermCache<>();


    @Nested
    public class GetTestCases {
        @Test
        public void givenAnEmptyCache_getAnUnknownKey_expectEmpty() {
            assertEquals( FP.emptyOption(), cache.get( "k1" ) );
        }

        @Test
        public void givenANonEmptyCache_getAnUnknownKey_expectEmpty() {
            cache.put("k1", BOOK1);

            assertEquals( FP.emptyOption(), cache.get( "k2" ) );
        }

        @Test
        public void givenANonEmptyCache_getAKnownKey_expectValue() {
            cache.put("k1", BOOK1);
            cache.put("k2", BOOK2);

            assertEquals( FP.option(BOOK2), cache.get( "k2" ) );
        }
    }

    @Nested
    public class RemoveTestCases {
        @Test
        public void givenAnEmptyCache_removeUnknownKey_expectNoError() {
            cache.remove("k1");
        }

        @Test
        public void givenTwoValuesInCache_removeKnownKeyThenCallGet_expectValueToBeUnretrievable() {
            cache.put("k1", BOOK1);
            cache.put("k2", BOOK2);


            cache.remove("k1");

            assertEquals( FP.emptyOption(), cache.get( "k1" ) );
            assertEquals( FP.option(BOOK2), cache.get( "k2" ) );
        }

        @Test
        public void givenOneValueInCache_removeTheValue_expectRemovedValueToBecomeGCable() {
            Book book = new Book("garbage");
            WeakReference<Book> ref = new WeakReference<>( book );

            cache.put("k1", book);
            book = null;

            cache.remove("k1");

            spinUntilReleased( ref );
        }
    }

    @Nested
    public class SizeTestCases {
        @Test
        public void givenAnEmptyCache_callSize_expectZero() {
            assertEquals( 0, cache.size() );
        }

        @Test
        public void givenTwoValuesInCache_callSize_expectTwo() {
            cache.put("k1", BOOK1);
            cache.put("k2", BOOK2);

            assertEquals( 2, cache.size() );
        }

        @Test
        public void givenAnEmptyCache_removeUnknownKeyThenCallSize_expectZero() {
            cache.remove( "k1" );

            assertEquals( 0, cache.size() );
        }

        @Test
        public void givenTwoValuesInCache_removeUnknownKeyThenCallSize_expectTwo() {
            cache.put("k1", BOOK1);
            cache.put("k2", BOOK2);

            cache.remove( "k3" );

            assertEquals( 2, cache.size() );
        }

        @Test
        public void givenTwoValuesInCache_removeKnownKeyThenCallSize_expectOne() {
            cache.put("k1", BOOK1);
            cache.put("k2", BOOK2);

            cache.remove( "k2" );

            assertEquals( 1, cache.size() );
        }

        @Test
        public void givenTwoValuesInCache_removeKnownKeyTwiceThenCallSize_expectOne() {
            cache.put("k1", BOOK1);
            cache.put("k2", BOOK2);

            cache.remove( "k2" );
            cache.remove( "k2" );

            assertEquals( 1, cache.size() );
        }
    }

    @Nested
    public class ClearTestCases {
        @Test
        public void givenTwoValuesInCache_callClear_expectSizeToDropToZero() {
            cache.put("k1", BOOK1);
            cache.put("k2", BOOK2);

            cache.clear();

            assertEquals( 0, cache.size() );
        }

        @Test
        public void givenTwoValuesInCache_callClear_expectNeitherKeyToBeRetrievable() {
            cache.put("k1", BOOK1);
            cache.put("k2", BOOK2);

            cache.clear();

            assertEquals( FP.emptyOption(), cache.get("k1") );
            assertEquals( FP.emptyOption(), cache.get("k2") );
        }

        @Test
        public void givenTwoValuesInCache_callClear_expectBothValuesToBecomeGCable() {
            Book b1 = new Book("g1");
            Book b2 = new Book("g2");
            Reference<Book> ref1 = new WeakReference<>( b1 );
            Reference<Book> ref2 = new WeakReference<>( b2 );

            cache.put("k1", b1);
            cache.put("k2", b2);

            b1 = null;
            b2 = null;

            cache.clear();

            spinUntilReleased( ref1 );
            spinUntilReleased( ref2 );
        }
    }
}
