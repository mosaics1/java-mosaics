package mosaics.xml.ast;

import mosaics.fp.FP;
import mosaics.fp.collections.RRBVector;
import mosaics.io.xml.TagWriter;
import mosaics.lang.functions.VoidFunction2;
import lombok.Value;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;


@SuppressWarnings("unchecked")
public class XmlForASTTest {

    private TagWriter                       tagWriterMock        = Mockito.mock(TagWriter.class);
    private XmlAST<Book>                    bodyASTMock          = Mockito.mock(XmlAST.class);
    private RRBVector<XmlAST<Book>>         bodyASTMocks         = FP.toVector(bodyASTMock);
    private VoidFunction2<Object,TagWriter> nestedSerializerMock = Mockito.mock(VoidFunction2.class);

// hasContents

    @Test
    public void givenEmptyList_callHasContents_expectFalse() {
        XmlForAST<Books,Book> ast = new XmlForAST<>( Books::getBooks, bodyASTMocks );

        Books books = new Books( Collections.emptyList() );

        assertFalse( ast.hasContents(books) );

        Mockito.verifyNoInteractions( bodyASTMock, tagWriterMock );
    }

    @Test
    public void givenNonEmptyList_callHasContents_expectTrue() {
        XmlForAST<Books,Book> ast = new XmlForAST<>( Books::getBooks, bodyASTMocks );

        Books books = new Books( Collections.singletonList(new Book("a")) );

        assertTrue( ast.hasContents(books) );

        Mockito.verifyNoInteractions( bodyASTMock, nestedSerializerMock, tagWriterMock );
    }


// writeTo

    @Test
    public void givenEmptyList_callWriteTo_expectNoOutput() {
        XmlForAST<Books,Book> ast = new XmlForAST<>( Books::getBooks, bodyASTMocks );

        Books books = new Books( Collections.emptyList() );

        ast.writeTo( books, nestedSerializerMock, tagWriterMock );

        Mockito.verifyNoInteractions( bodyASTMock, nestedSerializerMock, tagWriterMock );
    }

    @Test
    public void givenSingleElementList_callWriteTo_expectOneElementsOutput() {
        XmlForAST<Books,Book> ast = new XmlForAST<>( Books::getBooks, bodyASTMocks );

        Books books = new Books( Collections.singletonList(new Book("a")) );

        ast.writeTo( books, nestedSerializerMock, tagWriterMock );

        Mockito.verify(bodyASTMock).writeTo( new Book("a"), nestedSerializerMock, tagWriterMock );
    }

    @Test
    public void givenTwoElementList_callWriteTo_expectTwoElementsOutput() {
        XmlForAST<Books,Book> ast = new XmlForAST<>( Books::getBooks, bodyASTMocks );

        Books books = new Books(Arrays.asList(new Book("a"), new Book("b")));

        ast.writeTo( books, nestedSerializerMock, tagWriterMock );

        Mockito.verify(bodyASTMock).writeTo( new Book("a"), nestedSerializerMock, tagWriterMock );
        Mockito.verify(bodyASTMock).writeTo( new Book("b"), nestedSerializerMock, tagWriterMock );
    }


    @Value
    public static class Books {
        private List<Book> books;
    }

    @Value
    public static class Book {
        private String name;
    }

}
