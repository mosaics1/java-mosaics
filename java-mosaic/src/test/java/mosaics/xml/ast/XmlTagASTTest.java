package mosaics.xml.ast;

import lombok.Value;
import mosaics.io.xml.TagWriter;
import mosaics.lang.functions.VoidFunction2;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;


@SuppressWarnings("unchecked")
public class XmlTagASTTest {

    private POJO                            pojo                 = new POJO( "abc" );
    private TagWriter                       tagWriterMock        = Mockito.mock(TagWriter.class);
    private VoidFunction2<Object,TagWriter> nestedSerializerMock = Mockito.mock(VoidFunction2.class);


    @Test
    public void givenNoAttributesNoBody_expectNoTagOutput() {
        XmlTagAST tagAST = new XmlTagAST( "tag" );

        tagAST.writeTo(pojo, nestedSerializerMock, tagWriterMock);

        Mockito.verifyNoInteractions( tagWriterMock );
    }

    @Test
    public void givenOnlyEmptyAttributesAndEmptyBody_expectNoTagOutput() {
        XmlTagAST tagAST = new XmlTagAST( "tag" )
            .withBodyASTs( emptyXmlStringBodyAST() )
            .withAttributeASTs( emptyXmlAttributeAST(1), emptyXmlAttributeAST(2) );

        tagAST.writeTo(pojo, nestedSerializerMock, tagWriterMock);

        Mockito.verifyNoInteractions( tagWriterMock );
    }

    @Test
    public void givenOneAttributeWithValue_expectTagAndAttributeOutput() {
        XmlTagAST tagAST = new XmlTagAST( "tag" )
            .withBodyASTs( emptyXmlStringBodyAST() )
            .withAttributeASTs( nonEmptyXmlAttributeAST(1) );

        tagAST.writeTo(pojo, nestedSerializerMock, tagWriterMock);

        Mockito.verify( tagWriterMock ).tag("tag");
        Mockito.verify( tagWriterMock ).attr("attr1", "v1");
        Mockito.verify( tagWriterMock ).closeTag( "tag" );

        Mockito.verifyNoMoreInteractions( tagWriterMock );
    }

    @Test
    public void givenBodyValue_expectTagAndBodyOutput() {
        XmlTagAST tagAST = new XmlTagAST( "tag" )
            .withBodyASTs( nonEmptyXmlStringBodyAST() );

        tagAST.writeTo(pojo, nestedSerializerMock, tagWriterMock);

        Mockito.verify( tagWriterMock ).tag("tag");
        Mockito.verify( tagWriterMock ).bodyText("body text");
        Mockito.verify( tagWriterMock ).closeTag("tag");

        Mockito.verifyNoMoreInteractions( tagWriterMock );
    }

    @Test
    public void givenOneAttributeAndBodyValue_expectTagAndAttributeAndBodyOutput() {
        XmlTagAST tagAST = new XmlTagAST( "tag" )
            .withBodyASTs( nonEmptyXmlStringBodyAST() )
            .withAttributeASTs( nonEmptyXmlAttributeAST(1) );

        tagAST.writeTo(pojo, nestedSerializerMock, tagWriterMock);

        Mockito.verify( tagWriterMock ).tag("tag");
        Mockito.verify( tagWriterMock ).attr("attr1", "v1");
        Mockito.verify( tagWriterMock ).bodyText("body text");
        Mockito.verify( tagWriterMock ).closeTag("tag");

        Mockito.verifyNoMoreInteractions( tagWriterMock );
    }

    @Test
    public void givenTwoAttributesAndBodyValue_expectTagAndTwoAttributesAndBodyOutput() {
        XmlTagAST tagAST = new XmlTagAST( "tag" )
            .withBodyASTs( nonEmptyXmlStringBodyAST() )
            .withAttributeASTs( nonEmptyXmlAttributeAST(1), nonEmptyXmlAttributeAST(2) );

        tagAST.writeTo(pojo, nestedSerializerMock, tagWriterMock);

        Mockito.verify( tagWriterMock ).tag("tag");
        Mockito.verify( tagWriterMock ).attr("attr1", "v1");
        Mockito.verify( tagWriterMock ).attr("attr2", "v2");
        Mockito.verify( tagWriterMock ).bodyText("body text");
        Mockito.verify( tagWriterMock ).closeTag("tag");

        Mockito.verifyNoMoreInteractions( tagWriterMock );
    }

    @Test
    public void givenTwoAttributesOneEmpty_expectTagAndOneAttribute() {
        XmlTagAST tagAST = new XmlTagAST( "tag" )
            .withAttributeASTs( emptyXmlAttributeAST(1), nonEmptyXmlAttributeAST(2) );

        tagAST.writeTo(pojo, nestedSerializerMock, tagWriterMock);

        Mockito.verify( tagWriterMock ).tag("tag");
        Mockito.verify( tagWriterMock ).attr("attr2", "v2");
        Mockito.verify( tagWriterMock ).closeTag("tag");

        Mockito.verifyNoMoreInteractions( tagWriterMock );
    }

    @Test
    public void givenNestedTag_expectChildTagCall() {
        XmlTagAST tagAST = new XmlTagAST( "tag" )
            .withBodyASTs( new XmlTagAST("childTag").withAttributeASTs(nonEmptyXmlAttributeAST(1)) );

        tagAST.writeTo(pojo, nestedSerializerMock, tagWriterMock);

        Mockito.verify( tagWriterMock ).tag("tag");
        Mockito.verify( tagWriterMock ).tag("childTag");
        Mockito.verify( tagWriterMock ).attr("attr1", "v1");
        Mockito.verify( tagWriterMock ).closeTag("childTag");
        Mockito.verify( tagWriterMock ).closeTag("tag");

        Mockito.verifyNoMoreInteractions( tagWriterMock );
    }



    private XmlAttributeAST emptyXmlAttributeAST( int i ) {
        return new XmlAttributeAST( "attr"+i, x -> "" );
    }

    private XmlStringBodyAST emptyXmlStringBodyAST() {
        return new XmlStringBodyAST( x -> "" );
    }

    private XmlAttributeAST nonEmptyXmlAttributeAST( int i ) {
        return new XmlAttributeAST( "attr"+i, x -> "v"+i );
    }

    private XmlStringBodyAST nonEmptyXmlStringBodyAST() {
        return new XmlStringBodyAST( x -> "body text" );
    }


    @Value
    public static class POJO {
        private String v;
    }
}
