package mosaics.xml.ast;

import lombok.Value;
import mosaics.io.xml.TagWriter;
import mosaics.junit.JMRandomExtension;
import mosaics.junit.JMRandomExtension.Random;
import mosaics.lang.functions.Function1;
import mosaics.lang.functions.VoidFunction2;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;


@ExtendWith(JMRandomExtension.class)
@SuppressWarnings("unchecked")
public class XmlAttributeASTTest {

    private TagWriter                       tagWriterMock        = Mockito.mock(TagWriter.class);
    private Function1<POJO,String>          fetcherMock          = Mockito.mock(Function1.class);
    private VoidFunction2<Object,TagWriter> nestedSerializerMock = Mockito.mock(VoidFunction2.class);

// hasContents

    @Test
    public void givenEmptyString_callHasContents_expectFalse( @Random POJO rndPojo ) {
        XmlAttributeAST<POJO> ast = new XmlAttributeAST<>( "attr1", fetcherMock );

        Mockito.when( fetcherMock.invoke(rndPojo) ).thenReturn( "" );

        assertFalse( ast.hasContents(rndPojo) );
    }

    @Test
    public void givenNonEmptyString_callHasContents_expectTrue( @Random POJO rndPojo ) {
        XmlAttributeAST<POJO> ast = new XmlAttributeAST<>( "attr1", fetcherMock );

        Mockito.when( fetcherMock.invoke(rndPojo) ).thenReturn( "abc" );

        assertTrue( ast.hasContents(rndPojo) );
    }


// writeTo

    @Test
    public void givenEmptyString_expectNoAttribute( @Random POJO rndPojo ) {
        XmlAttributeAST<POJO> ast = new XmlAttributeAST<>( "attr1", fetcherMock );

        Mockito.when( fetcherMock.invoke(rndPojo) ).thenReturn( "" );

        ast.writeTo( rndPojo, nestedSerializerMock, tagWriterMock );


        Mockito.verifyNoMoreInteractions( nestedSerializerMock, tagWriterMock );
    }

    @Test
    public void givenBlankString_expectNoAttribute( @Random POJO rndPojo ) {
        XmlAttributeAST<POJO> ast = new XmlAttributeAST<>( "attr1", fetcherMock );

        Mockito.when( fetcherMock.invoke(rndPojo) ).thenReturn( "   \t   " );

        ast.writeTo( rndPojo, nestedSerializerMock, tagWriterMock );


        Mockito.verifyNoMoreInteractions( nestedSerializerMock, tagWriterMock );
    }

    @Test
    public void givenNonBlankString_expectAttribute( @Random POJO rndPojo ) {
        XmlAttributeAST<POJO> ast = new XmlAttributeAST<>( "attr1", fetcherMock );

        Mockito.when( fetcherMock.invoke(rndPojo) ).thenReturn( "value1" );

        ast.writeTo(rndPojo, nestedSerializerMock, tagWriterMock);

        Mockito.verify( tagWriterMock ).attr( "attr1", "value1" );

        Mockito.verifyNoMoreInteractions( nestedSerializerMock, tagWriterMock );
    }



    @Value
    public static class POJO {
        private String v;
    }

}
