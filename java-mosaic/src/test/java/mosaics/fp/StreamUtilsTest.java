package mosaics.fp;

import mosaics.fp.collections.FPOption;
import mosaics.fp.collections.Tuple2;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class StreamUtilsTest {

    @Nested
    public class GenerateStreamsTestCases {

        @Test
        public void simpleSeriesWhereNextValueIsGeneratedDirectlyFromThePreviousValue() {
            Stream<Long> stream = StreamUtils.generate( 1L, x -> FPOption.of(x*2) );

            assertEquals( Arrays.asList(1L,2L,4L,8L,16L), stream.limit(5).collect(Collectors.toList()) );
        }

        @Test
        public void generatorHasItsOwnMutableState() {
            AtomicLong state = new AtomicLong();
            Stream<Long> stream = StreamUtils.generate( () -> FPOption.of(state.incrementAndGet()) );

            assertEquals( Arrays.asList(1L,2L,3L,4L,5L), stream.limit(5).collect(Collectors.toList()) );
        }

        @Test
        public void genFibSequenceExample() {
            Stream<Long> stream = StreamUtils.generate(
                new Tuple2<>(0L,1L),
                t -> FPOption.of(new Tuple2<>(t.getSecond(), t.getFirst()+t.getSecond())),
                Tuple2::getSecond
            );

            assertEquals( Arrays.asList(1L,1L,2L,3L,5L), stream.limit(5).collect(Collectors.toList()) );
        }
    }


    @Nested
    public class BreadthFirstTestCases {
        @Test
        public void breadthFirst_0Elements() {
            Stream<Integer> stream = StreamUtils.breadthFirst( 1, v -> Stream.empty() );

            assertEquals( Collections.singletonList( 1 ), stream.collect( Collectors.toList() ) );
        }

        @Test
        public void breadthFirst_3Elements() {
            Stream<Integer> stream = StreamUtils.breadthFirst( 1, v -> {
                if ( v < 2 ) {
                    return Stream.of( v + 1, v + 2 );
                }

                return Stream.empty();
            } );

            assertEquals( Arrays.asList( 1, 2, 3 ), stream.collect( Collectors.toList() ) );
        }

        @Test
        public void breadthFirst_5Elements() {
            Stream<Integer> stream = StreamUtils.breadthFirst( 1, v -> {
                if ( v < 3 ) {
                    return Stream.of( v + 1, v + 2 );
                }

                return Stream.empty();
            } );

            assertEquals( Arrays.asList( 1, 2, 3, 3, 4 ), stream.collect( Collectors.toList() ) );
        }
    }

    @Nested
    public class ConcatTestCases {
        @Test
        @SuppressWarnings("unchecked")
        public void concatTests() {
            assertEquals( List.of(1,2), StreamUtils.concat( Stream.of(1,2) ).collect(Collectors.toList()) );
            assertEquals( List.of(1,2,3,4), StreamUtils.concat( Stream.of(1,2), Stream.of(3,4) ).collect(Collectors.toList()) );
            assertEquals( List.of(1,2,3,4,5,6), StreamUtils.concat( Stream.of(1,2), Stream.of(3,4), Stream.of(5,6) ).collect(Collectors.toList()) );
        }
    }
}
