package mosaics.fp;

import org.junit.jupiter.api.Test;

import java.util.concurrent.atomic.AtomicLong;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class LazyValTest {

    @Test
    public void fetchAValue() {
        LazyVal<String> v = new LazyVal<>( () -> "Hello" );

        assertEquals( "Hello", v.fetch() );
    }

    @Test
    public void countHowManyTimesTheFetcherIsInvoked_expect1() {
        AtomicLong counter = new AtomicLong(0);
        LazyVal<String> v = new LazyVal<>( () -> {counter.incrementAndGet(); return "Hello";} );

        assertEquals( "Hello", v.fetch() );
        assertEquals( "Hello", v.fetch() );
        assertEquals( "Hello", v.fetch() );
        assertEquals( 1, counter.get() );
    }

    @Test
    public void ensureThatTheFetcherIsNotInvokedBeforeFetchIsCalled() {
        AtomicLong counter = new AtomicLong(0);
        LazyVal<String> v = new LazyVal<>( () -> {counter.incrementAndGet(); return "Hello";} );

        assertEquals( 0, counter.get() );
    }

    @Test
    public void expectClearToClearTheVal() {
        AtomicLong counter = new AtomicLong(0);
        LazyVal<String> v = new LazyVal<>( () -> "Hello " + counter.incrementAndGet() );

        assertEquals( "Hello 1", v.fetch() );
        assertEquals( "Hello 1", v.fetch() );

        v.clear();

        assertEquals( "Hello 2", v.fetch() );

        assertEquals( 2, counter.get() );
    }

}
