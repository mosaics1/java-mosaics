package mosaics.fp.collections;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class Tuple2Test {
    @Test
    public void testDoAllValuesMatch() {
        assertTrue( new Tuple2<>("a","a").doAllValuesMatch() );
        assertFalse( new Tuple2<>("a","b").doAllValuesMatch() );
    }

    @Test
    public void mapFirst() {
        Tuple2<String, String> orig = new Tuple2<>( "a", "a" );

        assertEquals( new Tuple2<>("aa","a"), orig.mapFirst( v -> v + v ) );
        assertSame( orig, orig.mapFirst( v -> v ) );
    }

    @Test
    public void mapSecond() {
        Tuple2<String, String> orig = new Tuple2<>( "a", "a" );

        assertEquals( new Tuple2<>("a","aa"), orig.mapSecond( v -> v + v ) );
        assertSame( orig, orig.mapSecond( v -> v ) );
    }

    @Test
    public void flatMap() {
        Tuple2<String, String> orig = new Tuple2<>( "a", "a" );

        assertEquals( "aa", orig.flatMap( (a,b) -> a+b ) );
    }
}
