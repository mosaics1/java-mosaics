package mosaics.fp.collections;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class IntIterableTest {

// wrap( Int->Int )

    @Test
    public void wrap() {
        IntIterable iterable = IntIterable.wrap( i -> i );

        IntIterator it = iterable.iterator();

        for ( int i=0; i<10000; i++ ) {
            assertEquals( i, it.next() );
        }
    }

}
