package mosaics.fp.collections;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class DFSTest {

    @Test
    public void givenSingleNode_expectWalkToReturnSingleNode() {
        Node a = new Node("A");

        assertEquals( FPIterable.wrapAll(a), DFS.dfs(a, Node::getChildren) );
    }

    @Test
    public void givenOneChildNode_expectWalkToReturnChildFirst() {
        Node b = new Node("B");
        Node a = new Node("A", b);

        assertEquals( FPIterable.wrapAll(a, b), DFS.dfs(a, Node::getChildren) );
    }

    @Test
    public void givenTwoNodeCycle_expectWalkToReturnChildFirst_expectCycleToBeDetectedAndAlgToFinish() {
        Node b = new Node("B");
        Node a = new Node("A", b);

        b.children.add( a );

        assertEquals( FPIterable.wrapAll(a, b), DFS.dfs(a, Node::getChildren) );
    }

    @Test
    public void givenTwoChildNodes_expectRHSDFS() {
        Node a = new Node("A");
        Node b = new Node("B");
        Node c = new Node("C");

        a.children.addAll( b, c );

        assertEquals( FPIterable.wrapAll(a, c, b), DFS.dfs(a, Node::getChildren) );
    }

    @Test
    public void givenThreeDeepBinaryTree_expectRHSDFS() {
        Node a = new Node("A");
        Node b = new Node("B");
        Node c = new Node("C");
        Node d = new Node("D");
        Node e = new Node("E");
        Node f = new Node("F");
        Node g = new Node("G");

        a.children.addAll( b, c );
        b.children.addAll( d, e );
        c.children.addAll( f, g );

        assertEquals( FPIterable.wrapAll(a,c,g,f,b,e,d), DFS.dfs(a, Node::getChildren) );
    }

    @Test
    public void dfsMulti() {
        Node a = new Node("A");
        Node b = new Node("B");
        Node c = new Node("C");
        Node d = new Node("D");
        Node e = new Node("E");
        Node f = new Node("F");
        Node g = new Node("G");

        a.children.addAll( b, c );
        b.children.addAll( d, e );
        c.children.addAll( f, g );

        assertEquals( FPIterable.wrapAll(b,e,d,a,c,g,f), DFS.dfsMulti( FPList.wrapArray(a,b), Node::getChildren) );
    }


    public static class Node {
        public String       label;
        public FPList<Node> children = FPList.newArrayList();

        public Node( String label, Node...children ) {
            this.label = label;

            this.children.addAll( children );
        }

        public boolean isRoot() {
            return children.isEmpty();
        }

        public FPIterable<Node> getChildren() {
            return children;
        }

        public String toString() {
            return label;
        }
    }

}
