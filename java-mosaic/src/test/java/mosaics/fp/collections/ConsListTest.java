package mosaics.fp.collections;

import org.junit.jupiter.api.Test;

import static mosaics.fp.collections.ConsList.consList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class ConsListTest extends ConsListSharedTestsBase {

    protected ConsList<String> createList( String... args ) {
        return consList(args).reverse();
    }


    @Test
    public void givenEmptyList_hashCode_expect() {
        assertEquals( 17, ConsList.nil().hashCode() );
    }

    @Test
    public void givenEmptyList_toString_expect() {
        assertEquals( "Nil", ConsList.nil().toString() );
    }

    @Test
    public void givenEmptyList_equalsSelf_expectTrue() {
        assertEquals( ConsList.nil(), ConsList.nil() );
    }

    @Test
    public void givenEmptyList_equalsSingleElement_expectFalse() {
        assertFalse( consList("a").equals(ConsList.nil()) );
        assertFalse( ConsList.nil().equals( consList("a")) );
    }

    @Test
    public void givenSingleElement_hashCode_expect() {
        assertEquals( "a".hashCode(), consList("a").hashCode() );
    }

    @Test
    public void givenSingleElement_equalsClone_expectTrue() {
        assertTrue( consList("a").equals( consList("a")) );
    }

    @Test
    public void givenSingleElement_equalsDifferentSingleElementList_expectFalse() {
        assertFalse( consList("a").equals( consList("b")) );
    }

    @Test
    public void givenSingleElement_equalsTwoElementListContainingSameValues_expectFalse() {
        assertFalse( consList("a").equals( consList("a","a")) );
    }

    @Test
    public void givenSingleElement_toString_expect() {
        assertEquals( "a::Nil", consList("a").toString() );
    }



    @Test
    public void givenTwoElements_hashCode_expect() {
        assertEquals( "b".hashCode()*31 + "a".hashCode(), consList("a", "b").hashCode() );
    }

    @Test
    public void givenTwoElement_toString_expect() {
        assertEquals( "b::a::Nil", consList("a","b").toString() );
    }


    @Test
    public void givenListLargeEnoughThatRecursionWillBlowTheStack_hashCode_expectValue() {
        ConsList<Integer> bigList = ConsList.zeroToInc(100_000);

        assertTrue( bigList.hashCode() != 0 );
    }

    @Test
    public void givenListLargeEnoughThatRecursionWillBlowTheStack_toString_expectValue() {
        ConsList<Integer> bigList = ConsList.zeroToInc(100_000);

        assertTrue( bigList.toString() != null );
    }

    @Test
    public void givenListLargeEnoughThatRecursionWillBlowTheStack_equals_expectValue() {
        ConsList<Integer> bigList1 = ConsList.zeroToInc(100_000);
        ConsList<Integer> bigList2 = ConsList.zeroToInc(100_000);

        assertEquals( bigList1, bigList2 );
    }

// REMOVE

    @Test
    public void removeMiddleValues() {
        ConsList<String> origList = consList("a","b","b","c");

        assertEquals( consList("a","c"), origList.remove("b") );
    }

    @Test
    public void removeBothEndValues() {
        ConsList<String> origList = consList("a","b","b","a");

        assertEquals( consList("b","b"), origList.remove("a") );
    }

    @Test
    public void removeValueNotInList_expectListUnchanged() {
        ConsList<String> origList = consList("a","b","b","a");

        assertSame( origList, origList.remove("c") );
    }

}
