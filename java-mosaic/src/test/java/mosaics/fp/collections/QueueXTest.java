package mosaics.fp.collections;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class QueueXTest {

    @Test
    public void givenEmptyQueue_callIsEmpty_expectTrue() {
        FPQueue<String> q = new FPQueue<>();

        assertTrue( q.isEmpty() );
    }

    @Test
    public void givenEmptyQueue_callHasContents_expectFalse() {
        FPQueue<String> q = new FPQueue<>();

        assertFalse( q.hasContents() );
    }

    @Test
    public void givenQueueWithOneElement_callIsEmpty_expectFalse() {
        FPQueue<String> q = new FPQueue<>();

        q.push("1");

        assertFalse( q.isEmpty() );
    }

    @Test
    public void givenQueueWithOneElement_callHasContents_expectTrue() {
        FPQueue<String> q = new FPQueue<>();

        q.push("1");

        assertTrue( q.hasContents() );
    }

    @Test
    public void givenTwoElements_pushAndPop() {
        FPQueue<String> q = new FPQueue<>();

        q.push("1");
        q.push("2");

        assertEquals( "1", q.pop() );
        assertEquals( "2", q.pop() );

        assertFalse( q.hasContents() );
    }

}
