package mosaics.fp.collections;

import mosaics.fp.FP;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class FPIteratorTest {

    @Test
    public void partitionAsWeGo() {
        List<Integer> numbers = Arrays.asList(1, 1, 1, 2, 3, 3, 4);

        assertEquals("[[1, 1, 1], [2], [3, 3], [4]]", FPIterator.wrap(numbers).partitionAsWeGo(( a, b ) -> a.intValue() == b.intValue()).mkString() );
    }

    @Test
    public void createIteratorFor() {
        assertEquals( Collections.emptyList(), FPIterator.createIteratorFor(null).toList() );
        assertEquals( Collections.singletonList("a"), FPIterator.createIteratorFor("a").toList() );
        assertEquals( Arrays.asList("a","b"), FPIterator.createIteratorFor(new String[] {"a","b"}).toList() );
        assertEquals( Arrays.asList(), FPIterator.createIteratorFor(new Object[] {}).toList() );
        assertEquals( Arrays.asList(), FPIterator.createIteratorFor(new int[] {}).toList() );
        assertEquals( Arrays.asList(1,2), FPIterator.createIteratorFor(new int[] {1,2}).toList() );
        assertEquals( Arrays.asList("a","b"), FPIterator.createIteratorFor(Arrays.asList("a","b")).toList() );
        assertEquals( Arrays.asList("a", "b"), FPIterator.createIteratorFor(FP.toVector("a", "b")).toList() );
    }

    @Test
    public void min() {
        assertEquals( FP.emptyOption(), FPIterator.wrap().min(Object::hashCode) );
        assertEquals( FP.option("a"), FPIterator.wrap("a", "bc").min(String::length) );
        assertEquals( FP.option("a"), FPIterator.wrap("bc", "a").min(String::length) );
    }

    @Test
    public void map() {
        assertEquals(List.of(), FPIterator.emptyIterator().map(i -> i+"!").toList() );
        assertEquals(List.of("1!"), FPIterator.wrap(1).map(i -> i+"!").toList() );
        assertEquals(List.of("1!","2!"), FPIterator.wrap(1,2).map(i -> i+"!").toList() );
        assertEquals(List.of("1!","2!","3!"), FPIterator.wrap(1,2,3).map(i -> i+"!").toList() );
    }

    @Nested
    public class ZipOptTestCases {
        @Test
        public void bothEmpty() {
            FPIterable<String> a = FPIterable.wrapAll();
            FPIterable<Integer> b = FPIterable.wrapAll();
            List<Tuple2<FPOption<String>, FPOption<Integer>>> expected = List.of();

            assertEquals( expected, a.iterator().zipOpt( b.iterator() ).toList() );
        }

        @Test
        public void leftLongerThanRight() {
            FPIterable<String> a = FPIterable.wrapAll( "a", "b" );
            FPIterable<Integer> b = FPIterable.wrapAll( 1 );
            List<Tuple2<FPOption<String>, FPOption<Integer>>> expected = List.of(
                new Tuple2<>( FP.option( "a" ), FP.option( 1 ) ),
                new Tuple2<>( FP.option( "b" ), FP.emptyOption() )
            );

            assertEquals( expected, a.iterator().zipOpt( b.iterator() ).toList() );
        }

        @Test
        public void rightLongerThanLeft() {
            FPIterable<String> a = FPIterable.wrapAll( "a" );
            FPIterable<Integer> b = FPIterable.wrapAll( 1, 2 );
            List<Tuple2<FPOption<String>, FPOption<Integer>>> expected = List.of(
                new Tuple2<>( FP.option( "a" ), FP.option( 1 ) ),
                new Tuple2<>( FP.emptyOption(), FP.option(2) )
            );

            assertEquals( expected, a.iterator().zipOpt( b.iterator() ).toList() );
        }
    }
}
