package mosaics.fp.collections.maps;

import mosaics.fp.collections.RRBVector;
import mosaics.fp.collections.maps.FPMultiMap;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class FPMultiMapTest {


// GET/PUT

    @Test
    public void givenEmptyMap_getMissingKey_expectEmptyVectorBack() {
        FPMultiMap<String,Integer> map = FPMultiMap.emptyMap();

        assertEquals( 0, map.getElementCount() );
        assertEquals( 0, map.getKeyCount() );
        assertTrue( map.isEmpty() );
        assertFalse( map.hasContents() );

        Assertions.assertEquals( RRBVector.emptyVector(), map.get("k1") );
    }

    @Test
    public void givenEmptyMap_putOneKVGetMissingKey_expectEmptyVectorBack() {
        FPMultiMap<String,Integer> map = FPMultiMap.<String,Integer>emptyMap()
            .put( "k1", RRBVector.toVector(1) );

        assertEquals( 1, map.getElementCount() );
        assertEquals( 1, map.getKeyCount() );
        assertFalse( map.isEmpty() );
        assertTrue( map.hasContents() );

        assertEquals( RRBVector.emptyVector(), map.get("k2") );
    }

    @Test
    public void givenEmptyMap_putOneKVGetExistingKey_expectVectorBack() {
        FPMultiMap<String,Integer> map = FPMultiMap.<String,Integer>emptyMap()
            .put( "k1", RRBVector.toVector(1) );

        assertEquals( 1, map.getElementCount() );
        assertEquals( 1, map.getKeyCount() );
        assertFalse( map.isEmpty() );
        assertTrue( map.hasContents() );

        assertEquals( RRBVector.toVector(1), map.get("k1") );
    }

    @Test
    public void givenEmptyMap_putOneKVWithTwoElementsGetExistingKey_expectTwoElementVectorBack() {
        FPMultiMap<String,Integer> map = FPMultiMap.<String,Integer>emptyMap()
            .put( "k1", RRBVector.toVector(1,2) );

        assertEquals( 2, map.getElementCount() );
        assertEquals( 1, map.getKeyCount() );
        assertFalse( map.isEmpty() );
        assertTrue( map.hasContents() );

        assertEquals( RRBVector.toVector(1,2), map.get("k1") );
    }

    @Test
    public void givenMapWithValues_putOneKVWithTwoElementsOverExistingKey_expectExistingValueToBeOverwritten() {
        FPMultiMap<String,Integer> map = FPMultiMap.<String,Integer>emptyMap()
            .put( "k1", RRBVector.toVector(1,2) )
            .put( "k1", RRBVector.toVector(3,4) );

        assertEquals( 2, map.getElementCount() );
        assertEquals( 1, map.getKeyCount() );
        assertFalse( map.isEmpty() );
        assertTrue( map.hasContents() );

        assertEquals( RRBVector.toVector(3,4), map.get("k1") );
    }

    @Test
    public void givenMapWithValues_putOneKVWithZeroElementsOverExistingKey_expectExistingValueToBeEssentiallyRemoved() {
        FPMultiMap<String,Integer> map = FPMultiMap.<String,Integer>emptyMap()
            .put( "k1", RRBVector.toVector(1,2) )
            .put( "k1", RRBVector.emptyVector() );

        assertEquals( 0, map.getElementCount() );
        assertEquals( 0, map.getKeyCount() );
        assertTrue( map.isEmpty() );
        assertFalse( map.hasContents() );

        assertEquals( RRBVector.emptyVector(), map.get("k1") );
    }


// APPEND

    @Test
    public void givenMapWithOneValue_appendNewValue_expectGetToReturnTwoElementVector() {
        FPMultiMap<String,Integer> map = FPMultiMap.<String,Integer>emptyMap()
            .put( "k1", RRBVector.toVector(1) )
            .appendElement( "k1", 2 );

        assertEquals( 2, map.getElementCount() );
        assertEquals( 1, map.getKeyCount() );
        assertFalse( map.isEmpty() );
        assertTrue( map.hasContents() );

        assertEquals( RRBVector.toVector(1,2), map.get("k1") );
    }


// REMOVE KEY

    @Test
    public void givenEmptyMap_removeKey_expectNoChange() {
        FPMultiMap<String,Integer> originalMap = FPMultiMap.emptyMap();
        FPMultiMap<String,Integer> updatdMap   = originalMap.removeKey("k1");

        assertSame( originalMap, updatdMap );
    }

    @Test
    public void givenMapWithContents_removeMissingKey_expectNoChange() {
        FPMultiMap<String,Integer> originalMap = FPMultiMap.emptyMap();
        FPMultiMap<String,Integer> updatdMap   = originalMap.removeKey("k1");

        assertSame( originalMap, updatdMap );
    }

    @Test
    public void givenMapWithContents_removeKey_expectKeyAndAllOfItsElementsToBeRemoved() {
        FPMultiMap<String,Integer> originalMap = FPMultiMap.<String,Integer>emptyMap()
            .put( "k1", RRBVector.toVector(1,2) );
        FPMultiMap<String,Integer> updatdMap   = originalMap.removeKey("k1");

        assertFalse( updatdMap.hasContents() );
        assertEquals( 0, updatdMap.getElementCount() );
        assertEquals( 0, updatdMap.getKeyCount() );
    }


// REMOVE ELEMENT

    @Test
    public void givenEmptyMap_removeElement_expectNoChange() {
        FPMultiMap<String,Integer> originalMap = FPMultiMap.emptyMap();
        FPMultiMap<String,Integer> updatdMap   = originalMap.removeElement("k1", 2);

        assertSame( originalMap, updatdMap );
    }

    @Test
    public void givenMapWithContents_removeElementOfMissingKey_expectNoChange() {
        FPMultiMap<String,Integer> originalMap = FPMultiMap.<String,Integer>emptyMap()
            .put( "k1", RRBVector.toVector(1,2) );
        FPMultiMap<String,Integer> updatdMap   = originalMap.removeElement("k2", 2);

        assertSame( originalMap, updatdMap );
    }

    @Test
    public void givenMapWithContents_removeMissingElementOfAnExistingKey_expectNoChange() {
        FPMultiMap<String,Integer> originalMap = FPMultiMap.<String,Integer>emptyMap()
            .put( "k1", RRBVector.toVector(1,2) );
        FPMultiMap<String,Integer> updatdMap   = originalMap.removeElement("k1", 3);

        assertSame( originalMap, updatdMap );
    }

    @Test
    public void givenMapWithContents_removeElementOfAnExistingKey_expectElementRemoved() {
        FPMultiMap<String,Integer> originalMap = FPMultiMap.<String,Integer>emptyMap()
            .put( "k1", RRBVector.toVector(1,2) );
        FPMultiMap<String,Integer> updatdMap   = originalMap.removeElement("k1", 2);

        assertTrue( updatdMap.hasContents() );
        assertEquals( 1, updatdMap.getElementCount() );
        assertEquals( 1, updatdMap.getKeyCount() );
        assertEquals( RRBVector.toVector(1), updatdMap.get("k1") );
    }

    @Test
    public void givenMapWithContents_removeLastElementOfAnExistingKey_expectEntireKeyToBeRemoved() {
        FPMultiMap<String,Integer> originalMap = FPMultiMap.<String,Integer>emptyMap()
            .put( "k1", RRBVector.toVector(1) );
        FPMultiMap<String,Integer> updatdMap   = originalMap.removeElement("k1", 1);

        assertFalse( updatdMap.hasContents() );
        assertEquals( 0, updatdMap.getElementCount() );
        assertEquals( 0, updatdMap.getKeyCount() );
        assertEquals( RRBVector.emptyVector(), updatdMap.get("k1") );
    }

}
