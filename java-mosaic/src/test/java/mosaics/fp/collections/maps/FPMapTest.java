package mosaics.fp.collections.maps;

import mosaics.fp.FP;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertSame;


public class FPMapTest {

    @Test
    public void giveEmptyMap_size_expect0() {
        FPMap<String,String> map = FPMap.emptyMap();

        assertEquals( 0, map.size() );
    }

    @Test
    public void giveEmptyMap_getByKey_expectNone() {
        FPMap<String,String> map = FPMap.emptyMap();

        assertEquals( FP.emptyOption(), map.get("k1") );
    }

    @Test
    public void storeValuesInMap_showOriginalMapDoesNotChange() {
        FPMap<String,String> map1 = FPMap.emptyMap();
        FPMap<String,String> map2 = map1.put( "k1", "foo" );

        assertEquals( 0, map1.size() );
        assertEquals( 1, map2.size() );
    }

    @Test
    public void expectTwoEmptyMapsEqual() {
        assertEquals( FPMap.emptyMap(), FPMap.emptyMap() );
    }

    @Test
    public void expectTwoMapsWithSameContentsToEqual() {
        FPMap<String,String> map1 = FPMap.<String,String>emptyMap().put( "k1", "foo" );
        FPMap<String,String> map2 = FPMap.<String,String>emptyMap().put( "k1", "foo" );

        assertEquals( map1, map2 );
    }

    @Test
    public void expectTwoMapsContainingTheSameValuesEqualEachOther() {
        FPMap<String,String> map1 = FPMap.<String,String>emptyMap().put( "k1", "foo" );
        FPMap<String,String> map2 = FPMap.<String,String>emptyMap().put( "k1", "bar" );

        assertNotEquals( map1, map2 );
    }

    @Test
    public void expectRemovingAKeyThatDoesNotExistReturnsTheOriginalMap() {
        FPMap<String,String> map1 = FPMap.<String,String>emptyMap().put( "k1", "foo" );

        assertSame( map1, map1.remove("k2") );
    }

    @Test
    public void expectRemovingAKeyThatDoesExistReturnsAMapWithTheKVRemoved() {
        FPMap<String,String> map1 = FPMap.<String,String>emptyMap().put( "k1", "foo" );

        assertEquals( FPMap.emptyMap(), map1.remove("k1") );
    }

    @Test
    public void givenAnEmptyMap_updateKey_expectKeyToBeCreated() {
        FPMap<String,String> map1 = FPMap.emptyMap();
        FPMap<String,String> map2 = map1.updateValue( "k1", prevValue -> FP.option("GB: "+prevValue) );

        assertEquals( FPMap.wrapMap("k1", "GB: None"), map2 );
    }

    @Test
    public void givenANoneEmptyMap_updateExistingKey_expectKeyToBeUpdated() {
        FPMap<String,String> map1 = FPMap.wrapMap("k1", "foo");
        FPMap<String,String> map2 = map1.updateValue( "k1", prevValue -> FP.option("GB: "+prevValue) );

        assertEquals( FPMap.wrapMap("k1", "GB: Some(foo)"), map2 );
    }

    @Test
    public void givenANoneEmptyMap_updateExistingKeyRemovingIt_expectKeyToBeRemoved() {
        FPMap<String,String> map1 = FPMap.wrapMap("k1", "foo");
        FPMap<String,String> map2 = map1.updateValue( "k1", prevValue -> FP.emptyOption() );

        assertEquals( FPMap.emptyMap(), map2 );
        assertEquals( 1, map1.size() );
    }

    @Test
    public void bulkAddOneThousandEntries_expectToBeAbleToRetrieveAllThousand() {
        int numEntriesToAdd = 100;

        FPMap<String,Integer> map = FP.emptyMap();
        for ( int i=0; i<numEntriesToAdd; i++ ) {
            map = map.put(Integer.toString(i), i);

            assertEquals( FP.option(i), map.get(Integer.toString(i)) );
        }

        for ( int i=0; i<numEntriesToAdd; i++ ) {
            assertEquals( FP.option(i), map.get(Integer.toString(i)) );
        }
    }
}
