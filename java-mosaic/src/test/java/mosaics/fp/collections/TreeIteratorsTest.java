package mosaics.fp.collections;

import mosaics.fp.FP;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class TreeIteratorsTest {
    //            8
    //       4          12
    //     2   6     10    14
    //   1  3 5 7   9 11  13 15
    private static FPOption<Integer> getLHSFor( int v ) {
        return switch (v) {
            case 14 -> FPOption.of(13);
            case 12 -> FPOption.of(10);
            case 10 -> FPOption.of( 9);
            case  8 -> FPOption.of( 4);
            case  6 -> FPOption.of( 5);
            case  4 -> FPOption.of( 2);
            case  2 -> FPOption.of( 1);
            default -> FP.emptyOption();
        };
    }

    private static FPOption<Integer> getRHSFor( int v ) {
        return switch (v) {
            case 14 -> FPOption.of(15);
            case 12 -> FPOption.of(14);
            case 10 -> FPOption.of(11);
            case  8 -> FPOption.of(12);
            case  6 -> FPOption.of( 7);
            case  4 -> FPOption.of( 6);
            case  2 -> FPOption.of( 3);
            default -> FP.emptyOption();
        };
    }

    @Nested
    public class BreadthFirstIteratorTestCases {
        //            8
        //       4          12
        //     2   6     10    14
        //   1  3 5 7   9 11  13 15
        @Test
        public void testFullTree() {
            FPIterator<Integer> it = TreeIterators.breadthFirstIteratorOpt(
                FP.option(8),
                TreeIteratorsTest::getLHSFor,
                TreeIteratorsTest::getRHSFor
            );

            assertEquals( List.of(8,4,12,2,6,10,14,1,3,5,7,9,11,13,15), it.toList() );
        }
    }

    @Nested
    public class InorderDepthFirstIteratorTestCases {
        @Test
        public void givenNoInitial_expectEmptyIterator() {
            FPIterator<Integer> it = TreeIterators.inorderDepthFirstIteratorOpt( FP.emptyOption(), v -> FP.emptyOption(), v -> FP.emptyOption() );

            assertEquals( List.of(), it.toList() );
        }

        @Test
        public void givenInitialWithNoKids_expectSingleValueInIterator() {
            FPIterator<Integer> it = TreeIterators.inorderDepthFirstIteratorOpt( FP.option(8), v -> FP.emptyOption(), v -> FP.emptyOption() );

            assertEquals( List.of(8), it.toList() );
        }

        @Test
        public void givenInitialWithOneKidLHS_expectTwoValuesInIterator() {
            FPIterator<Integer> it = TreeIterators.inorderDepthFirstIteratorOpt(
                FP.option(8),
                v -> switch (v) {
                    case 8  -> FPOption.of(4);
                    default -> FP.emptyOption();
                },
                v -> FP.emptyOption()
            );

            assertEquals( List.of(4,8), it.toList() );
        }

        @Test
        public void givenInitialWithOneKidRHS_expectTwoValuesInIterator() {
            FPIterator<Integer> it = TreeIterators.inorderDepthFirstIteratorOpt(
                FP.option(8),
                v -> FP.emptyOption(),
                v -> switch (v) {
                    case 8  -> FPOption.of(10);
                    default -> FP.emptyOption();
                }
            );

            assertEquals( List.of(8,10), it.toList() );
        }

        @Test
        public void givenInitialWithTwoKids_expectThreeValuesInIterator() {
            FPIterator<Integer> it = TreeIterators.inorderDepthFirstIteratorOpt(
                FP.option(8),
                v -> switch (v) {
                    case 8  -> FPOption.of(4);
                    default -> FP.emptyOption();
                },
                v -> FP.emptyOption()
            );

            assertEquals( List.of(4,8), it.toList() );
        }

        //            8
        //       4          12
        //     2   6     10    14
        //   1  3 5 7   9 11  13 15
        @Test
        public void givenInitialWithFifteenKids_expectFifteenValuesInIterator() {
            FPIterator<Integer> it = TreeIterators.inorderDepthFirstIteratorOpt(
                FP.option(8),
                TreeIteratorsTest::getLHSFor,
                TreeIteratorsTest::getRHSFor
            );

            assertEquals( List.of(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15), it.toList() );
        }

        @Test
        public void givenInitialWithFifteenKids_mapAdd1_expectFifteenValuesInIterator() {
            FPIterator<Integer> it = TreeIterators.inorderDepthFirstIteratorOpt(
                FP.option(8),
                TreeIteratorsTest::getLHSFor,
                TreeIteratorsTest::getRHSFor
            );

            assertEquals( List.of(2,3,4,5,6,7,8,9,10,11,12,13,14,15,16), it.map(i -> i+1).toList() );
        }

        @Test
        public void givenInitialWithFifteenKids_flatMapAdd1_expectFifteenValuesInIterator() {
            FPIterator<Integer> it = TreeIterators.inorderDepthFirstIteratorOpt(
                FP.option(8),
                TreeIteratorsTest::getLHSFor,
                TreeIteratorsTest::getRHSFor
            );

            assertEquals( List.of(2,3,4,5,6,7,8,9,10,11,12,13,14,15,16), it.flatMap(i -> FP.option(i+1)).toList() );
        }
    }
}
