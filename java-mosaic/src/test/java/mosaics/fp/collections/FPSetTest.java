package mosaics.fp.collections;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class FPSetTest {
    @Test
    public void remove() {
        FPSet<String> a = FPSet.wrapArray( "a", "b", "c" );

        assertEquals( FPSet.wrapArray( "a", "b", "c" ), a );
        assertEquals( FPSet.wrapArray( "a", "c" ), a.remove("b") );
        assertEquals( FPSet.wrapArray( "a" ), a.remove("b").remove("c") );
        assertEquals( FPSet.wrapArray( "a", "b", "c" ), a );
    }

    @Test
    public void add() {
        FPSet<String> a = FPSet.wrapArray( "a", "b", "c" );

        assertEquals( FPSet.wrapArray( "a", "b", "c" ), a );
        assertEquals( FPSet.wrapArray( "a", "b", "c", "d" ), a.add("d") );
        assertEquals( FPSet.wrapArray( "a", "b", "c", "d", "e" ), a.add("d").add("e") );
        assertEquals( FPSet.wrapArray( "a", "b", "c" ), a );
    }

    @Test
    public void addAll() {
        FPSet<String> a = FPSet.wrapArray( "a" );

        assertEquals( FPSet.wrapArray( "a" ), a );
        assertEquals( FPSet.wrapArray( "a", "b", "c", "d" ), a.addAll("b","c","d") );
        assertEquals( FPSet.wrapArray( "a" ), a );
    }

    @Test
    public void removeAll() {
        FPSet<String> a = FPSet.wrapArray( "a", "b", "c", "d" );

        assertEquals( FPSet.wrapArray( "a", "b", "c", "d" ), a );
        assertEquals( FPSet.wrapArray( "a" ), a.removeAll("b","c","d") );
        assertEquals( FPSet.wrapArray( "a", "b", "c", "d" ), a );
    }
}
