package mosaics.fp.collections;

import mosaics.lang.ArrayUtils;
import mosaics.utils.ListUtils;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import static mosaics.fp.collections.LongIterable.wrap;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;


public class LongIteratorTest {

    @Test
    public void testMap() {
        assertListEquals(Arrays.asList(2, 3, 4, 5), wrap(new long[]{1, 2, 3, 4}).map(i -> i + 1).toList());
        assertListEquals(Arrays.asList(), wrap(new long[]{}).map(i -> i + 1).toList());
    }

    @Test
    public void testForEach() {
        AtomicLong sum = new AtomicLong(0);

        wrap(new long[]{1, 2, 3, 4}).iterator().forEach(sum::addAndGet);

        assertEquals(10, sum.longValue());
    }

    @Test
    public void testFold() {
        assertEquals(10, wrap(new long[]{1, 2, 3, 4}).iterator().fold((a,b) -> a + b) );
        assertEquals(1, wrap(new long[]{1}).iterator().fold(( a, b ) -> a + b));
        assertEquals(0, wrap(new long[]{}).iterator().fold(( a, b ) -> a + b) );
    }

    @Test
    public void testDrop() {
        assertListEquals(new long[]{1, 2, 3, 4}, wrap(new long[]{1, 2, 3, 4}).iterator().drop(0).toList());
        assertListEquals(new long[]{2, 3, 4}, wrap(new long[]{1, 2, 3, 4}).iterator().drop(1).toList());
        assertListEquals(new long[]{3, 4}, wrap(new long[]{1, 2, 3, 4}).iterator().drop(2).toList());
        assertListEquals(new long[]{4}, wrap(new long[]{1, 2, 3, 4}).iterator().drop(3).toList());
        assertListEquals(new long[]{}, wrap(new long[]{1, 2, 3, 4}).iterator().drop(4).toList());
        assertListEquals(new long[]{}, wrap(new long[]{1, 2, 3, 4}).iterator().drop(5).toList());
        assertListEquals(new long[]{}, wrap(new long[]{1, 2, 3, 4}).iterator().drop(6).toList());
    }

    @Test
    public void testTake() {
        assertListEquals(new long[]{1, 2, 3, 4}, wrap(new long[]{1, 2, 3, 4}).iterator().take(5).toList());
        assertListEquals(new long[]{1, 2, 3, 4}, wrap(new long[]{1, 2, 3, 4}).iterator().take(4).toList());
        assertListEquals(new long[]{1, 2, 3}, wrap(new long[]{1, 2, 3, 4}).iterator().take(3).toList());
        assertListEquals(new long[]{1, 2}, wrap(new long[]{1, 2, 3, 4}).iterator().take(2).toList());
        assertListEquals(new long[]{1}, wrap(new long[]{1, 2, 3, 4}).iterator().take(1).toList());
        assertListEquals(new long[]{}, wrap(new long[]{1, 2, 3, 4}).iterator().take(0).toList());


        try {
            wrap(new long[]{1, 2, 3, 4}).iterator().take(-1).toList();

            fail("expected IllegalArgumentException");
        } catch ( IllegalArgumentException e ) {
            assertEquals("'n' (-1) must be >= 0", e.getMessage());
        }
    }

    @Test
    public void testMin() {
        assertEquals( 1, wrap(new long[]{1, 2, 3, 4}).iterator().min() );
        assertEquals( 0, wrap(new long[]{}).iterator().min() );
        assertEquals( -10, wrap(new long[]{-10}).iterator().min() );
        assertEquals( -1, wrap(new long[]{7,3,1,10,-1,100}).iterator().min() );
    }

;
    private void assertListEquals(long[] a, List b) {
        assertListEquals(ArrayUtils.toList(a), b);
    }

    private void assertListEquals(List a, List b) {
        String s1 = ListUtils.toString(a,",");
        String s2 = ListUtils.toString(b,",");

        assertEquals( s1, s2);
    }
}
