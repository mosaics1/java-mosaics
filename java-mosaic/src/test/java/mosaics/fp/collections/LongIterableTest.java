package mosaics.fp.collections;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class LongIterableTest {

// wrap( Long->Long )

    @Test
    public void wrap() {
        LongIterable iterable = LongIterable.wrap( i -> i );

        LongIterator it = iterable.iterator();

        for ( long i=0; i<10000; i++ ) {
            assertEquals( i, it.next() );
        }
    }

}
