//package mosaics.fp;
//
//import mosaics.junit.JUnitMosaic;
//import org.junit.Test;
//
//import java.time.Duration;
//import java.util.concurrent.atomic.AtomicReference;
//
//import static org.junit.Assert.*;
//
//
//public class PromiseTest {
//    private static final Duration TIMEOUT = Duration.ofSeconds(10);
//
//    @Test
//    public void givenPromiseWithResult_setResult_expectException() {
//        Promise<String> p = new Promise<>();
//        p.setResult( "foo" );
//
//        try {
//            p.setResult( "bar" );
//        } catch ( IllegalStateException ex ) {
//            assertEquals( "The promise has already been completed", ex.getMessage() );
//        }
//    }
//
//    @Test
//    public void givenPromiseWithResult_setFailure_expectException() {
//        Promise<String> p = new Promise<>();
//        p.setResult( "bar" );
//
//        try {
//            p.setFailure( new ExceptionFailure( "foo" ) );
//        } catch ( IllegalStateException ex ) {
//            assertEquals( "The promise has already been completed", ex.getMessage() );
//        }
//    }
//
//    @Test
//    public void givenPromiseWithFailure_setResult_expectException() {
//        Promise<String> p = new Promise<>();
//        p.setFailure( new ExceptionFailure( "foo" ) );
//
//        try {
//            p.setResult( "bar" );
//        } catch ( IllegalStateException ex ) {
//            assertEquals( "The promise has already been completed", ex.getMessage() );
//        }
//    }
//
//    @Test
//    public void givenPromiseWithFailure_setFailure_expectException() {
//        Promise<String> p = new Promise<>();
//        p.setFailure( new ExceptionFailure( "foo" ) );
//
//        try {
//            p.setFailure( new ExceptionFailure( "bar" ) );
//        } catch ( IllegalStateException ex ) {
//            assertEquals( "The promise has already been completed", ex.getMessage() );
//        }
//    }
//
//
//// public boolean isComplete();
//
//    @Test
//    public void givenIncompletePromise_isComplete_expectFalse() {
//        Promise<String> p = new Promise<>();
//
//        assertFalse( p.isComplete() );
//    }
//
//    @Test
//    public void givenPromiseWithResult_isComplete_expectTrue() {
//        Promise<String> p = new Promise<>();
//        p.setResult( "foo" );
//
//        assertTrue( p.isComplete() );
//    }
//
//    @Test
//    public void givenPromiseWithFailure_isComplete_expectTrue() {
//        Promise<String> p = new Promise<>();
//        p.setFailure( new ExceptionFailure("foo") );
//
//        assertTrue( p.isComplete() );
//    }
//
//
////    public boolean hasResult();
//
//    @Test
//    public void givenIncompletePromise_hasResult_expectFalse() {
//        Promise<String> p = new Promise<>();
//
//        assertFalse( p.hasResult() );
//    }
//
//    @Test
//    public void givenPromiseWithResult_hasResult_expectTrue() {
//        Promise<String> p = new Promise<>();
//        p.setResult( "foo" );
//
//        assertTrue( p.hasResult() );
//    }
//
//    @Test
//    public void givenPromiseWithFailure_hasResult_expectFalse() {
//        Promise<String> p = new Promise<>();
//        p.setFailure( new ExceptionFailure("foo") );
//
//        assertFalse( p.hasResult() );
//    }
//
//
////    public boolean hasFailure();
//
//    @Test
//    public void givenIncompletePromise_hasFailure_expectFalse() {
//        Promise<String> p = new Promise<>();
//
//        assertFalse( p.hasFailure() );
//    }
//
//    @Test
//    public void givenPromiseWithResult_hasFailure_expectFalse() {
//        Promise<String> p = new Promise<>();
//        p.setResult( "foo" );
//
//        assertFalse( p.hasFailure() );
//    }
//
//    @Test
//    public void givenPromiseWithFailure_hasResult_expectTrue() {
//        Promise<String> p = new Promise<>();
//        p.setFailure( new ExceptionFailure("foo") );
//
//        assertTrue( p.hasFailure() );
//    }
//
//
////    public T getResultNoBlock();
//
//    @Test
//    public void givenIncompletePromise_getResultNoBlock_expectNull() {
//        Promise<String> p = new Promise<>();
//
//        JUnitMosaic.assertPotentiallyBlockingCallNull(p::getResultNoBlock);
//    }
//
//    @Test
//    public void givenPromiseWithResult_getResultNoBlock_expectResult() {
//        Promise<String> p = new Promise<>();
//        p.setResult( "foo" );
//
//
//        JUnitMosaic.assertPotentiallyBlockingCallEquals( "foo", p::getResultNoBlock );
//    }
//
//    @Test
//    public void givenPromiseWithFailure_getResultNoBlock_expectNull() {
//        Promise<String> p = new Promise<>();
//        p.setFailure( new ExceptionFailure( "foo" ) );
//
//
//        JUnitMosaic.assertPotentiallyBlockingCallNull( p::getResultNoBlock );
//    }
//
//
////public T getResultBlocking();
//
//    @Test
//    public void givenIncompletePromise_getResult_expectCallingThreadToBeBlocked() {
//        Promise<String> p = new Promise<>();
//
//        JUnitMosaic.assertFunctionBlocksTheCallingThread( () -> p.getResultBlocking(TIMEOUT) );
//    }
//
//    @Test
//    public void givenPromiseWithResult_getResult_expectResult() {
//        Promise<String> p = new Promise<>();
//        p.setResult( "foo" );
//
//        JUnitMosaic.assertPotentiallyBlockingCallEquals( "foo", () -> p.getResultBlocking(TIMEOUT) );
//    }
//
//    @Test
//    public void givenPromiseWithFailure_getResult_expectException() {
//        Promise<String> p = new Promise<>();
//        p.setFailure( new ExceptionFailure("foo") );
//
//        JUnitMosaic.assertPotentiallyBlockingCallThrows( new RuntimeException("foo"), () -> p.getResultBlocking(TIMEOUT) );
//    }
//
//
////    public Failure getFailureNoBlock();
//
//    @Test
//    public void givenIncompletePromise_getFailureNoBlock_expectNull() {
//        Promise<String> p = new Promise<>();
//
//        JUnitMosaic.assertPotentiallyBlockingCallNull( p::getFailureNoBlock );
//    }
//
//    @Test
//    public void givenPromiseWithResult_getFailureNoBlock_expectNull() {
//        Promise<String> p = new Promise<>();
//        p.setResult( "foo" );
//
//        JUnitMosaic.assertPotentiallyBlockingCallNull( p::getFailureNoBlock );
//    }
//
//    @Test
//    public void givenPromiseWithFailure_getFailureNoBlock_expectFailure() {
//        Promise<String> p = new Promise<>();
//        p.setFailure( new ExceptionFailure("foo") );
//
//        JUnitMosaic.assertPotentiallyBlockingCallEquals( new ExceptionFailure("foo"), p::getFailureNoBlock );
//    }
//
//
////    public <B> Tryable<B> mapResult( final Function1<T, B> mappingFunction );
//
//    @Test
//    public void givenIncompletePromise_mapResult_expectIncompleteTryable() {
//        Promise<String>  p             = new Promise<>();
//        Tryable<Integer> mappedPromise = p.<Integer>mapResult( String::length );
//
//        assertFalse( mappedPromise.isComplete() );
//    }
//
//    @Test
//    public void givenIncompletePromise_mapResultThenCompleteInitialTryableWithResult_expectTryableWithMappedResult() {
//        Promise<String>  p             = new Promise<>();
//        Tryable<Integer> mappedPromise = p.<Integer>mapResult( String::length );
//
//        p.setResult( "foo" );
//
//        assertTrue( mappedPromise.isComplete() );
//        assertEquals( 3, mappedPromise.getResultNoBlock().intValue() );
//    }
//
//    @Test
//    public void givenIncompletePromise_mapResultThenCompleteInitialTryableWithFailure_expectTryableWithFailure() {
//        Promise<String>  p             = new Promise<>();
//        Tryable<Integer> mappedPromise = p.mapResult( String::length );
//
//        p.setFailure( new ExceptionFailure( "foo" ) );
//
//        assertTrue( mappedPromise.isComplete() );
//        assertEquals( new ExceptionFailure("foo"), mappedPromise.getFailureNoBlock() );
//    }
//
//    @Test
//    public void givenPromiseWithResult_mapResult_expectMappedTryable() {
//        Promise<String>  p = new Promise<>();
//        p.setResult( "foo" );
//
//
//        Tryable<Integer> mappedPromise = p.mapResult( String::length );
//
//        assertTrue( mappedPromise.isComplete() );
//        assertEquals( 3, mappedPromise.getResultNoBlock().intValue() );
//    }
//
//    @Test
//    public void givenPromiseWithFailure_mapResult_expectNoChange() {
//        Promise<String>  p = new Promise<>();
//        p.setFailure( new ExceptionFailure( "foo" ) );
//
//
//        Tryable<Integer> mappedPromise = p.mapResult( String::length );
//
//        assertTrue( mappedPromise.isComplete() );
//        assertEquals( new ExceptionFailure("foo"), mappedPromise.getFailureNoBlock() );
//    }
//
//    @Test
//    public void givenIncompletePromise_mapResultThatErrorsThenCompleteWithResult_expectFailure() {
//        Promise<String>  p = new Promise<>();
//
//        Tryable<Integer> mappedPromise = p.mapResult( v -> {throw new RuntimeException("bar");} );
//
//        p.setResult( "foo" );
//
//        assertTrue( mappedPromise.isComplete() );
//        assertEquals( new ExceptionFailure(new RuntimeException("bar")), mappedPromise.getFailureNoBlock() );
//    }
//
//    @Test
//    public void givenPromiseWithResult_mapResultThatErrors_expectFailure() {
//        Promise<String>  p = new Promise<>();
//        p.setResult( "foo" );
//
//
//        Tryable<Integer> mappedPromise = p.mapResult( v -> {throw new RuntimeException("bar");} );
//
//        assertTrue( mappedPromise.isComplete() );
//        assertEquals( new ExceptionFailure(new RuntimeException("bar")), mappedPromise.getFailureNoBlock() );
//    }
//
//
////    public <B> Tryable<B> flatMapResult( final Function1<T, Tryable<B>> mappingFunction );
//
//    @Test
//    public void givenIncompletePromise_flatMapResult_expectIncompleteTryable() {
//        Promise<String>  p             = new Promise<>();
//        Tryable<Integer> mappedPromise = p.flatMapResult( v -> Try.succeeded(v.length()) );
//
//        assertFalse( mappedPromise.isComplete() );
//    }
//
//    @Test
//    public void givenIncompletePromise_flatMapResultThenCompleteInitialTryableWithResult_expectTryableWithMappedResult() {
//        Promise<String>  p             = new Promise<>();
//        Tryable<Integer> mappedPromise = p.flatMapResult( v -> Try.succeeded( v.length() ) );
//
//        p.setResult( "abcd" );
//
//        assertTrue( mappedPromise.isComplete() );
//        assertEquals( 4, mappedPromise.getResultNoBlock().intValue() );
//    }
//
//    @Test
//    public void givenIncompletePromise_flatMapResultThenCompleteInitialTryableWithFailure_expectTryableWithFailure() {
//        Promise<String>  p             = new Promise<>();
//        Tryable<Integer> mappedPromise = p.flatMapResult( v -> Try.succeeded( v.length() ) );
//
//        p.setFailure( new ExceptionFailure( "foo" ) );
//
//        assertTrue( mappedPromise.isComplete() );
//        assertEquals( new ExceptionFailure("foo"), mappedPromise.getFailureNoBlock() );
//    }
//
//    @Test
//    public void givenPromiseWithResult_flatMapResultReturningValue_expectMappedTryable() {
//        Promise<String>  p = new Promise<>();
//        p.setResult( "foo" );
//
//        Tryable<Integer> mappedPromise = p.flatMapResult( v -> Try.succeeded( v.length() ) );
//
//        assertTrue( mappedPromise.isComplete() );
//        assertEquals( 3, mappedPromise.getResultNoBlock().intValue() );
//    }
//
//    @Test
//    public void givenPromiseWithResult_flatMapResultReturningFailure_expectMappedTryable() {
//        Promise<String>  p = new Promise<>();
//        p.setResult( "foo" );
//
//        Tryable<Integer> mappedPromise = p.flatMapResult( v -> Try.failed( "bar" ) );
//
//        assertTrue( mappedPromise.isComplete() );
//        assertEquals( new ExceptionFailure("bar"), mappedPromise.getFailureNoBlock() );
//    }
//
//    @Test
//    public void givenPromiseWithFailure_flatMapResult_expectNoChangeAsMappingFunctionWillNotBeCalled() {
//        Promise<String>  p = new Promise<>();
//        p.setFailure( new ExceptionFailure( "foo" ) );
//
//        Tryable<Integer> mappedPromise = p.flatMapResult( v -> Try.succeeded( v.length() ) );
//
//        assertTrue( mappedPromise.isComplete() );
//        assertEquals( new ExceptionFailure("foo"), mappedPromise.getFailureNoBlock() );
//    }
//
//    @Test
//    public void givenIncompletePromise_flatMapResultThatErrorsThenCompleteWithResult_expectFailure() {
//        Promise<String>  p = new Promise<>();
//
//        Tryable<Integer> mappedPromise = p.flatMapResult( v -> {
//            throw new RuntimeException( "bar" );
//        } );
//
//        p.setResult( "foo" );
//
//        assertTrue( mappedPromise.isComplete() );
//        assertEquals( new ExceptionFailure(new RuntimeException("bar")), mappedPromise.getFailureNoBlock() );
//    }
//
//    @Test
//    public void givenPromiseWithResult_flatMapResultThatErrors_expectFailure() {
//        Promise<String>  p = new Promise<>();
//        p.setResult( "foo" );
//
//
//        Tryable<Integer> mappedPromise = p.flatMapResult( v -> {
//            throw new RuntimeException( "bar" );
//        } );
//
//        assertTrue( mappedPromise.isComplete() );
//        assertEquals( new ExceptionFailure(new RuntimeException("bar")), mappedPromise.getFailureNoBlock() );
//    }
//
//
////    public Tryable<T> recover( final Function1<Failure, T> recoveryFunction );
//
//    @Test
//    public void givenIncompletePromise_recover_expectIncompleteTryable() {
//        Promise<String> p                = new Promise<>();
//        Tryable<String> recoveredTryable = p.recover( f -> "hello" );
//
//        assertFalse( recoveredTryable.isComplete() );
//    }
//
//    @Test
//    public void givenIncompletePromise_recoverThenCompleteInitialTryableWithResult_expectTryableWithResult() {
//        Promise<String> p                = new Promise<>();
//        Tryable<String> recoveredTryable = p.recover( f -> "hello" );
//
//        p.setResult( "foo" );
//
//        assertTrue( recoveredTryable.isComplete() );
//        assertEquals( "foo", recoveredTryable.getResultNoBlock() );
//    }
//
//    @Test
//    public void givenIncompletePromise_recoverThenCompleteInitialTryableWithFailure_expectTryableWithRecoveredResult() {
//        Promise<String> p                = new Promise<>();
//        Tryable<String> recoveredTryable = p.recover( f -> "hello" );
//
//        p.setFailure( new ExceptionFailure( "foo" ) );
//
//        assertTrue( recoveredTryable.isComplete() );
//        assertEquals( "hello", recoveredTryable.getResultNoBlock() );
//    }
//
//    @Test
//    public void givenPromiseWithResult_recover_expectNoChange() {
//        Promise<String> p = new Promise<>();
//        p.setResult( "foo" );
//
//        Tryable<String> recoveredTryable = p.recover( f -> "hello" );
//
//        assertTrue( recoveredTryable.isComplete() );
//        assertEquals( "foo", recoveredTryable.getResultNoBlock() );
//    }
//
//    @Test
//    public void givenPromiseWithFailure_recover_expectRecoveredValue() {
//        Promise<String> p = new Promise<>();
//        p.setFailure( new ExceptionFailure( "foo" ) );
//
//        Tryable<String> recoveredTryable = p.recover( f -> "hello" );
//
//        assertTrue( recoveredTryable.isComplete() );
//        assertEquals( "hello", recoveredTryable.getResultNoBlock() );
//    }
//
//    @Test
//    public void givenIncompletePromise_recoverThatErrorsThenCompleteWithFailure_expectFailure() {
//        Promise<String>  p = new Promise<>();
//
//        Tryable<String> mappedPromise = p.recover( v -> {
//            throw new RuntimeException( "bar" );
//        } );
//
//        p.setFailure( new ExceptionFailure( "foo" ) );
//
//        assertTrue( mappedPromise.isComplete() );
//        assertEquals( new ExceptionFailure(new RuntimeException("bar")), mappedPromise.getFailureNoBlock() );
//    }
//
//    @Test
//    public void givenPromiseWithFailure_recoverThatErrors_expectFailure() {
//        Promise<String>  p = new Promise<>();
//        p.setFailure( new ExceptionFailure( "foo" ) );
//
//
//        Tryable<String> mappedPromise = p.recover( v -> {
//            throw new RuntimeException( "bar" );
//        } );
//
//        assertTrue( mappedPromise.isComplete() );
//        assertEquals( new ExceptionFailure(new RuntimeException("bar")), mappedPromise.getFailureNoBlock() );
//    }
//
//
////    public Tryable<T> flatRecover( final Function1<Failure, Tryable<T>> recoveryFunction );
//
//    @Test
//    public void givenIncompletePromise_flatRecover_expectIncompleteTryable() {
//        Promise<String> p                = new Promise<>();
//        Tryable<String> recoveredTryable = p.flatRecover( f -> Try.succeeded("hello") );
//
//        assertFalse( recoveredTryable.isComplete() );
//    }
//
//    @Test
//    public void givenIncompletePromise_flatRecoverThenCompleteInitialTryableWithResult_expectTryableWithResult() {
//        Promise<String> p                = new Promise<>();
//        Tryable<String> recoveredTryable = p.flatRecover( f -> Try.succeeded( "hello" ) );
//
//        p.setResult( "foo" );
//
//        assertTrue( recoveredTryable.isComplete() );
//        assertEquals( "foo", recoveredTryable.getResultNoBlock() );
//    }
//
//    @Test
//    public void givenIncompletePromise_flatRecoverThenCompleteInitialTryableWithFailure_expectTryableWithMappedFailure() {
//        Promise<String> p                = new Promise<>();
//        Tryable<String> recoveredTryable = p.flatRecover( f -> Try.succeeded( "hello" ) );
//
//        p.setFailure( new ExceptionFailure( "foo" ) );
//
//        assertTrue( recoveredTryable.isComplete() );
//        assertEquals( "hello", recoveredTryable.getResultNoBlock() );
//    }
//
//    @Test
//    public void givenPromiseWithResult_flatRecover_expectNoChange() {
//        Promise<String> p = new Promise<>();
//        p.setResult( "foo" );
//
//        Tryable<String> recoveredTryable = p.flatRecover( f -> Try.succeeded( "hello" ) );
//
//        assertTrue( recoveredTryable.isComplete() );
//        assertEquals( "foo", recoveredTryable.getResultNoBlock() );
//    }
//
//    @Test
//    public void givenPromiseWithFailure_flatRecoverReturningResult_expectRecoveredResult() {
//        Promise<String> p = new Promise<>();
//        p.setFailure( new ExceptionFailure( "foo" ) );
//
//        Tryable<String> recoveredTryable = p.flatRecover( f -> Try.succeeded( "hello" ) );
//
//        assertTrue( recoveredTryable.isComplete() );
//        assertEquals( "hello", recoveredTryable.getResultNoBlock() );
//    }
//
//    @Test
//    public void givenPromiseWithFailure_flatRecoverReturningAnotherFailure_expectFailure() {
//        Promise<String> p = new Promise<>();
//        p.setFailure( new ExceptionFailure( "foo" ) );
//
//        Tryable<String> recoveredTryable = p.flatRecover( f -> Try.failed( "hello" ) );
//
//        assertTrue( recoveredTryable.isComplete() );
//        assertEquals( new ExceptionFailure("hello"), recoveredTryable.getFailureNoBlock() );
//    }
//
//    @Test
//    public void givenIncompletePromise_flatRecoverThatErrorsThenCompleteWithFailure_expectFailure() {
//        Promise<String>  p = new Promise<>();
//
//        Tryable<String> mappedPromise = p.flatRecover( v -> {
//            throw new RuntimeException( "bar" );
//        } );
//
//        p.setFailure( new ExceptionFailure( "foo" ) );
//
//        assertTrue( mappedPromise.isComplete() );
//        assertEquals( new ExceptionFailure(new RuntimeException("bar")), mappedPromise.getFailureNoBlock() );
//    }
//
//    @Test
//    public void givenPromiseWithFailure_flatRecoverThatErrors_expectFailure() {
//        Promise<String>  p = new Promise<>();
//        p.setFailure( new ExceptionFailure( "foo" ) );
//
//
//        Tryable<String> mappedPromise = p.flatRecover( v -> {
//            throw new RuntimeException( "bar" );
//        } );
//
//        assertTrue( mappedPromise.isComplete() );
//        assertEquals( new ExceptionFailure(new RuntimeException("bar")), mappedPromise.getFailureNoBlock() );
//    }
//
//
////    public Tryable<T> mapFailure( final Function1<Failure, Failure> mappingFunction );
//
//    @Test
//    public void givenIncompletePromise_mapFailure_expectIncompleteTryable() {
//        Promise<String> p                = new Promise<>();
//        Tryable<String> recoveredTryable = p.mapFailure( f -> new ExceptionFailure("bar") );
//
//        assertFalse( recoveredTryable.isComplete() );
//    }
//
//    @Test
//    public void givenIncompletePromise_mapFailureThenCompleteInitialTryableWithResult_expectTryableWithResult() {
//        Promise<String> p                = new Promise<>();
//        Tryable<String> recoveredTryable = p.mapFailure( f -> new ExceptionFailure("bar") );
//
//        p.setResult( "foo" );
//
//        assertTrue( recoveredTryable.isComplete() );
//        assertEquals( "foo", recoveredTryable.getResultNoBlock() );
//    }
//
//    @Test
//    public void givenIncompletePromise_mapFailureThenCompleteInitialTryableWithFailure_expectTryableWithMappedFailure() {
//        Promise<String> p                = new Promise<>();
//        Tryable<String> recoveredTryable = p.mapFailure( f -> new ExceptionFailure("bar") );
//
//        p.setFailure( new ExceptionFailure( "foo" ) );
//
//        assertTrue( recoveredTryable.isComplete() );
//        assertEquals( new ExceptionFailure( "bar" ), recoveredTryable.getFailureNoBlock() );
//    }
//
//    @Test
//    public void givenPromiseWithResult_mapFailure_expectNoChange() {
//        Promise<String> p = new Promise<>();
//        p.setResult( "foo" );
//
//        Tryable<String> recoveredTryable = p.mapFailure( f -> new ExceptionFailure("bar") );
//
//        assertTrue( recoveredTryable.isComplete() );
//        assertEquals( "foo", recoveredTryable.getResultNoBlock() );
//    }
//
//    @Test
//    public void givenPromiseWithFailure_mapFailure_expectMappedFailure() {
//        Promise<String> p = new Promise<>();
//        p.setFailure( new ExceptionFailure( "foo" ) );
//
//        Tryable<String> recoveredTryable = p.mapFailure( f -> new ExceptionFailure("bar") );
//
//        assertTrue( recoveredTryable.isComplete() );
//        assertEquals( new ExceptionFailure("bar"), recoveredTryable.getFailureNoBlock() );
//    }
//
//    @Test
//    public void givenIncompletePromise_mapFailureThatErrorsThenCompleteWithFailure_expectFailure() {
//        Promise<String>  p = new Promise<>();
//
//        Tryable<String> mappedPromise = p.mapFailure( v -> {
//            throw new RuntimeException( "bar" );
//        } );
//
//        p.setFailure( new ExceptionFailure( "foo" ) );
//
//        assertTrue( mappedPromise.isComplete() );
//        assertEquals( new ExceptionFailure(new RuntimeException("bar")), mappedPromise.getFailureNoBlock() );
//    }
//
//    @Test
//    public void givenPromiseWithFailure_mapFailureThatErrors_expectFailure() {
//        Promise<String>  p = new Promise<>();
//        p.setFailure( new ExceptionFailure( "foo" ) );
//
//
//        Tryable<String> mappedPromise = p.mapFailure( v -> {
//            throw new RuntimeException( "bar" );
//        } );
//
//        assertTrue( mappedPromise.isComplete() );
//        assertEquals( new ExceptionFailure(new RuntimeException("bar")), mappedPromise.getFailureNoBlock() );
//    }
//
//
////    public Tryable<T> flatMapFailure( final Function1<Failure, Tryable<T>> mappingFunction );
//
//    @Test
//    public void givenIncompletePromise_flatMapFailure_expectIncompleteTryable() {
//        Promise<String> p                = new Promise<>();
//        Tryable<String> recoveredTryable = p.flatMapFailure( f -> Try.succeeded("bar") );
//
//        assertFalse( recoveredTryable.isComplete() );
//    }
//
//    @Test
//    public void givenIncompletePromise_flatMapFailureThenCompleteInitialTryableWithResult_expectTryableWithResult() {
//        Promise<String> p                = new Promise<>();
//        Tryable<String> recoveredTryable = p.flatMapFailure( f -> Try.succeeded("bar") );
//
//        p.setResult( "foo" );
//
//        assertTrue( recoveredTryable.isComplete() );
//        assertEquals( "foo", recoveredTryable.getResultNoBlock() );
//    }
//
//    @Test
//    public void givenIncompletePromise_flatMapFailureThenCompleteInitialTryableWithFailure_expectTryableWithMappedResult() {
//        Promise<String> p                = new Promise<>();
//        Tryable<String> recoveredTryable = p.flatMapFailure( f -> Try.succeeded("bar") );
//
//        p.setFailure( new ExceptionFailure( "foo" ) );
//
//        assertTrue( recoveredTryable.isComplete() );
//        assertEquals( "bar", recoveredTryable.getResultNoBlock() );
//    }
//
//    @Test
//    public void givenIncompletePromise_flatMapFailureThenCompleteInitialTryableWithFailure_expectTryableWithMappedFailure() {
//        Promise<String> p                = new Promise<>();
//        Tryable<String> recoveredTryable = p.flatMapFailure( f -> Try.failed( "bar" ) );
//
//        p.setFailure( new ExceptionFailure( "foo" ) );
//
//        assertTrue( recoveredTryable.isComplete() );
//        assertEquals( new ExceptionFailure("bar"), recoveredTryable.getFailureNoBlock() );
//    }
//
//    @Test
//    public void givenPromiseWithResult_flatMapFailure_expectNoChange() {
//        Promise<String> p = new Promise<>();
//        p.setResult( "foo" );
//
//        Tryable<String> recoveredTryable = p.flatMapFailure( f -> Try.failed( "bar" ) );
//
//        assertTrue( recoveredTryable.isComplete() );
//        assertEquals( "foo", recoveredTryable.getResultNoBlock() );
//    }
//
//    @Test
//    public void givenPromiseWithFailure_flatMapFailure_expectMappedFailure() {
//        Promise<String> p = new Promise<>();
//        p.setFailure( new ExceptionFailure( "foo" ) );
//
//        Tryable<String> recoveredTryable = p.flatMapFailure( f -> Try.failed( "bar" ) );
//
//        assertTrue( recoveredTryable.isComplete() );
//        assertEquals( new ExceptionFailure("bar"), recoveredTryable.getFailureNoBlock() );
//    }
//
//    @Test
//    public void givenIncompletePromise_flatMapFailureThatErrorsThenCompleteWithFailure_expectFailure() {
//        Promise<String>  p = new Promise<>();
//
//        Tryable<String> mappedPromise = p.flatMapFailure( v -> {
//            throw new RuntimeException( "bar" );
//        } );
//
//        p.setFailure( new ExceptionFailure( "foo" ) );
//
//        assertTrue( mappedPromise.isComplete() );
//        assertEquals( new ExceptionFailure(new RuntimeException("bar")), mappedPromise.getFailureNoBlock() );
//    }
//
//    @Test
//    public void givenPromiseWithFailure_flatMapFailureThatErrors_expectFailure() {
//        Promise<String>  p = new Promise<>();
//        p.setFailure( new ExceptionFailure( "foo" ) );
//
//
//        Tryable<String> mappedPromise = p.flatMapFailure( v -> {
//            throw new RuntimeException( "bar" );
//        } );
//
//        assertTrue( mappedPromise.isComplete() );
//        assertEquals( new ExceptionFailure(new RuntimeException("bar")), mappedPromise.getFailureNoBlock() );
//    }
//
//
////    public void onSuccess( VoidFunction1<T> callback );
//
//    @Test
//    public void givenIncompletePromise_onSuccess_expectCallbackToNotBeInvoked() {
//        Promise<String> p = new Promise<>();
//
//        AtomicReference<String> result = new AtomicReference<>();
//        p.onSuccess( result::set );
//
//        assertFalse( p.isComplete() );
//        assertNull( result.get() );
//    }
//
//    @Test
//    public void givenIncompletePromise_onSuccessThenCompleteInitialTryableWithResult_expectCallbackToBeInvoked() {
//        Promise<String> p = new Promise<>();
//
//        AtomicReference<String> result = new AtomicReference<>();
//        p.onSuccess( result::set );
//
//        p.setResult( "foo" );
//
//        assertEquals( "foo", result.get() );
//    }
//
//    @Test
//    public void givenIncompletePromise_onSuccessThenCompleteInitialTryableWithFailure_expectCallbackToNotBeInvoked() {
//        Promise<String> p = new Promise<>();
//
//        AtomicReference<String> result = new AtomicReference<>();
//        p.onSuccess( result::set );
//
//        p.setFailure( new ExceptionFailure( "foo" ) );
//
//        assertNull( result.get() );
//    }
//
//    @Test
//    public void givenPromiseWithResult_onSuccess_expectCallbackToBeInvoked() {
//        Promise<String> p = new Promise<>();
//        p.setResult( "foo" );
//
//        Tryable<String> recoveredTryable = p.flatMapFailure( f -> Try.failed("bar") );
//
//        AtomicReference<String> result = new AtomicReference<>();
//        recoveredTryable.onSuccess( result::set );
//
//        assertEquals( "foo", result.get() );
//    }
//
//    @Test
//    public void givenPromiseWithFailure_onSuccess_expectCallbackToNotBeInvoked() {
//        Promise<String> p = new Promise<>();
//        p.setFailure( new ExceptionFailure( "foo" ) );
//
//        Tryable<String> recoveredTryable = p.flatMapFailure( f -> Try.failed("bar") );
//
//        AtomicReference<String> result = new AtomicReference<>();
//        recoveredTryable.onSuccess( result::set );
//
//        assertNull( result.get() );
//    }
//
//    @Test
//    public void givenIncompletePromise_onSuccessThatErrorsThenComplete_expectErrorToBeWrittenToStdErr() {
//        Promise<String>  p = new Promise<>();
//
//        p.onSuccess( v -> {throw new RuntimeException( "bar" );} );
//        p.setResult( "foo" );
//
//        assertEquals( "foo", p.getResultNoBlock() );
//    }
//
//    @Test
//    public void givenPromiseThatErrors_onSuccess_expectError() {
//        Promise<String>  p = new Promise<>();
//        p.setResult( "foo" );
//
//        try {
//            p.onSuccess( v -> {throw new RuntimeException( "bar" );} );
//            fail( "expected exception" );
//        } catch ( RuntimeException ex ) {
//            assertEquals( "bar", ex.getMessage() );
//        }
//    }
//
//
////    public void onFailure( VoidFunction1<Failure> callback );
//
//    @Test
//    public void givenIncompletePromise_onFailure_expectCallbackToNotBeInvoked() {
//        Promise<String> p = new Promise<>();
//
//        AtomicReference<Failure> result = new AtomicReference<>();
//        p.onFailure( result::set );
//
//        assertNull( result.get() );
//    }
//
//    @Test
//    public void givenIncompletePromise_onFailureThenCompleteInitialTryableWithResult_expectCallbackToNotBeInvoked() {
//        Promise<String> p = new Promise<>();
//
//        AtomicReference<Failure> result = new AtomicReference<>();
//        p.onFailure( result::set );
//
//        p.setResult( "foo" );
//
//        assertNull( result.get() );
//    }
//
//    @Test
//    public void givenIncompletePromise_onFailureThenCompleteInitialTryableWithFailure_expectCallbackToBeInvoked() {
//        Promise<String> p = new Promise<>();
//
//        AtomicReference<Failure> result = new AtomicReference<>();
//        p.onFailure( result::set );
//
//        p.setFailure( new ExceptionFailure( "foo" ) );
//
//        assertEquals( new ExceptionFailure( "foo" ), result.get() );
//    }
//
//    @Test
//    public void givenPromiseWithResult_onFailure_expectCallbackToNotBeInvoked() {
//        Promise<String> p = new Promise<>();
//        p.setResult( "foo" );
//
//        AtomicReference<Failure> result = new AtomicReference<>();
//        p.onFailure( result::set );
//
//        assertNull( result.get() );
//    }
//
//    @Test
//    public void givenPromiseWithFailure_onFailure_expectCallbackToBeInvoked() {
//        Promise<String> p = new Promise<>();
//        p.setFailure( new ExceptionFailure( "foo" ) );
//
//        AtomicReference<Failure> result = new AtomicReference<>();
//        p.onFailure( result::set );
//
//        assertEquals( new ExceptionFailure("foo"), result.get() );
//    }
//
//    @Test
//    public void givenIncompletePromise_onFailureThatErrorsThenComplete_expectErrorToBeWrittenToStdErr() {
//        Promise<String>  p = new Promise<>();
//
//        p.onFailure( v -> {
//            throw new RuntimeException( "bar" );
//        } );
//        p.setFailure( new ExceptionFailure( "foo" ) );
//
//        assertEquals( new ExceptionFailure("foo"), p.getFailureNoBlock() );
//    }
//
//    @Test
//    public void givenPromiseThatErrors_onFailure_expectError() {
//        Promise<String>  p = new Promise<>();
//        p.setFailure( new ExceptionFailure( "foo" ) );
//
//        try {
//            p.onFailure( v -> {throw new RuntimeException( "bar" );} );
//            fail( "expected exception" );
//        } catch ( RuntimeException ex ) {
//            assertEquals( "bar", ex.getMessage() );
//        }
//    }
//}
