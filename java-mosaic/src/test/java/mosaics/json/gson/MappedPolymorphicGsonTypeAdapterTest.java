package mosaics.json.gson;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import mosaics.json.Animal;
import mosaics.json.Cow;
import mosaics.json.Dog;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class MappedPolymorphicGsonTypeAdapterTest {

    @Test
    public void cowAndDogRoundTrip() {
        MappedPolymorphicGsonTypeAdapter<Animal> codec = new MappedPolymorphicGsonTypeAdapter<Animal>()
            .withTypeJsonPropertyName( "animal" )
            .withChildMapping( Dog.class, "dog" )
            .withChildMapping( Cow.class, "cow" );

        Gson gson = new GsonBuilder()
            .registerTypeAdapter( Animal.class, codec )
            .create();

        Cow origCow = new Cow( "cow1" );
        Dog origDog = new Dog( "dog1" );

        String cowJson = gson.toJson( origCow, Animal.class );
        String dogJson = gson.toJson( origDog, Animal.class );

        assertEquals(
            "{\"name\":\"cow1\",\"animal\":\"cow\"}",
            cowJson
        );

        assertEquals(
            "{\"name\":\"dog1\",\"animal\":\"dog\"}",
            dogJson
        );

        Animal returnedCow = gson.fromJson( cowJson, Animal.class );
        Animal returnedDog = gson.fromJson( dogJson, Animal.class );

        assertEquals( origCow, returnedCow );
        assertEquals( origDog, returnedDog );
    }

}
