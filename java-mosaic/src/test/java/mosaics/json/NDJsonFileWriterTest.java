package mosaics.json;

import mosaics.io.IOUtils;
import mosaics.junit.JMFileExtension;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.io.File;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;


@ExtendWith(JMFileExtension.class)
public class NDJsonFileWriterTest {
    private static final Dog DOG1 = new Dog( "Cane Corso" );
    private static final Dog DOG2 = new Dog( "Boxer" );
    private static final Dog DOG3 = new Dog( "Staffordshire Bull Terrier" );


    @Test
    public void givenEmptyDirectory_writeOnePOJO( File dir ) {
        File f = new File(dir, "missing.ndjson");

        try ( NDJsonFileWriter<Dog> out = new NDJsonFileWriter<>(f) ) {
            out.append( DOG1 );
        }

        try ( NDJsonFileReader<Dog> in = new NDJsonFileReader<>(Dog.class, f) ) {
            assertTrue( in.hasNext() );
            assertEquals( DOG1, in.next() );
            assertFalse( in.hasNext() );
        }
    }

    @Test
    public void givenEmptyDirectory_writeTwoPOJOs( File dir ) {
        File f = new File(dir, "missing.ndjson");

        try ( NDJsonFileWriter<Dog> out = new NDJsonFileWriter<>(f) ) {
            out.append( DOG1 );
            out.append( DOG2 );
        }

        try ( NDJsonFileReader<Dog> in = new NDJsonFileReader<>(Dog.class, f) ) {
            assertTrue( in.hasNext() );
            assertEquals( DOG1, in.next() );
            assertTrue( in.hasNext() );
            assertEquals( DOG2, in.next() );
            assertFalse( in.hasNext() );
        }
    }

    @Test
    public void givenEmptyDirectory_writeThreePOJOs( File dir ) {
        File f = new File(dir, "missing.ndjson");

        try ( NDJsonFileWriter<Dog> out = new NDJsonFileWriter<>(f) ) {
            out.append( DOG1 );
            out.append( DOG2 );
            out.append( DOG3 );
        }

        try ( NDJsonFileReader<Dog> in = new NDJsonFileReader<>(Dog.class, f) ) {
            assertTrue( in.hasNext() );
            assertEquals( DOG1, in.next() );
            assertTrue( in.hasNext() );
            assertEquals( DOG2, in.next() );
            assertTrue( in.hasNext() );
            assertEquals( DOG3, in.next() );
            assertFalse( in.hasNext() );
        }
    }

    @Test
    public void tryToWriteToAfterClosing_expectException( File dir ) {
        File f = new File(dir, "missing.ndjson");

        NDJsonFileWriter<Dog> out = new NDJsonFileWriter<>(f);
        out.append( DOG1 );
        out.close();

        IllegalStateException ex = assertThrows( IllegalStateException.class, () -> out.append(DOG2) );
        assertEquals( "closed", ex.getMessage() );
    }

    @Test
    public void givenMissingDirectory_writePOJO( File dir ) {
        File f = new File(dir, "misingDir/missing.ndjson");

        try ( NDJsonFileWriter<Dog> out = new NDJsonFileWriter<>(f) ) {
            out.append( DOG1 );
        }

        try ( NDJsonFileReader<Dog> in = new NDJsonFileReader<>(Dog.class, f) ) {
            assertTrue( in.hasNext() );
            assertEquals( DOG1, in.next() );
            assertFalse( in.hasNext() );
        }
    }

    @Test
    public void givenNull_expectToReadNullBack( File dir ) {
        File f = new File(dir, "missing.ndjson");

        try ( NDJsonFileWriter<Dog> out = new NDJsonFileWriter<>(f) ) {
            out.append( null );
        }

        try ( NDJsonFileReader<Dog> in = new NDJsonFileReader<>(Dog.class, f) ) {
            assertTrue( in.hasNext() );
            assertEquals( null, in.next() );
            assertFalse( in.hasNext() );
        }
    }

    @Test
    public void overwriteExistingFile(
        @JMFileExtension.FileContents(lines= """
            {"name": "Cocker Spaniel"}
            """
        ) File file
    ) {
        try ( NDJsonFileWriter<Dog> out = new NDJsonFileWriter<>(file) ) {
            out.append( DOG1 );
        }

        try ( NDJsonFileReader<Dog> in = new NDJsonFileReader<>(Dog.class, file) ) {
            assertTrue( in.hasNext() );
            assertEquals( DOG1, in.next() );
            assertFalse( in.hasNext() );
        }
    }

    @Test
    public void appendToExistingFile(
        @JMFileExtension.FileContents(lines= """
            {"name": "Cane Corso"}
            """
        ) File file
    ) {
        try ( NDJsonFileWriter<Dog> out = new NDJsonFileWriter<>(file, true) ) {
            out.append( DOG2 );
        }

        try ( NDJsonFileReader<Dog> in = new NDJsonFileReader<>(Dog.class, file) ) {
            assertTrue( in.hasNext() );
            assertEquals( DOG1, in.next() );
            assertTrue( in.hasNext() );
            assertEquals( DOG2, in.next() );
            assertFalse( in.hasNext() );
        }
    }

    @Test
    public void writeRecordsToFile( File destDir ) {
        File destFile = new File(destDir, "f.ndjson");

        NDJsonFileWriter.writeAllTo( destFile, List.of(new Book("b1"), new Book("b2")) );

        String expected = """
                          {"name":"b1"}
                          {"name":"b2"}
                          """.stripIndent();
        String actual = IOUtils.toString( destFile );

        assertEquals( expected, actual );
    }

    private static record Book(String name) {}
}
