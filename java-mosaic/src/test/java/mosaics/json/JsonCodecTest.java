package mosaics.json;

import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import lombok.Value;
import mosaics.fp.FP;
import mosaics.fp.LazyVal;
import mosaics.fp.collections.ConsList;
import mosaics.fp.collections.FPList;
import mosaics.fp.collections.maps.FPMap;
import mosaics.fp.collections.FPOption;
import mosaics.fp.collections.FPSet;
import mosaics.fp.collections.RRBVector;
import mosaics.lang.time.DTM;
import mosaics.lang.time.Day;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;


public class JsonCodecTest {
    @Value
    public static class StdJavaPrimitiveTypes {
        private boolean booleanValue;
        private byte    byteValue;
        private char    charValue;
        private short   shortValue;
        private int     intValue;
        private long    longValue;
        private float   floatValue;
        private double  doubleValue;
    }

    @Value
    public static class StdJavaObjectTypes {
        private Boolean   booleanValue;
        private Byte      byteValue;
        private Character charValue;
        private Short     shortValue;
        private Integer   intValue;
        private Long      longValue;
        private Float     floatValue;
        private Double    doubleValue;
        private String    stringValue;
    }

    @Value
    public static class PojoWithOverriddenName {
        @SerializedName(value = "title", alternate = {"ref", "header"})
        private String name;
    }

    @Value
    public static class PojoWithJavaCollections {
        private Optional<String>    stringOptional;
        private List<String>        stringList;
        private Set<String>         stringSet;
        private Map<String, String> stringMap;
    }

    @Value
    public static class PojoWithGeneric<T> {
        private T value;
    }

    @Value
    public static class PojoWithNestedGeneric {
        private List<PojoWithGeneric<Double>> list;
    }

    @Value
    public static class PojoWithFPCollections {
        private FPOption<String>      stringFPOption;
        private FPList<String>        stringFPList;
        private FPSet<String>         stringFPSet;
        private FPMap<String, String> stringFPMap;
        private RRBVector<String>     stringRRBVector;
        private ConsList<String>      stringConsList;
    }

    @Value
    public static class PojoWithDtmAsMillisSinceEpoch {
        private DTM when;
    }

    @Value
    public static class PojoWithDay {
        private Day when;
    }

    @Value
    public static class PojoWithLazyVal {
        private LazyVal<String> name;

        public PojoWithLazyVal( String name ) {
            this.name = new LazyVal<>( () -> name );
        }

        public String getName() {
            return name.fetch();
        }
    }

    private JsonCodec jsonCodec = JsonCodec.ndJsonCodec;


    @Nested
    public class FormatJsonTestCases {
        @Test
        public void stdJavaPrimitiveTypes() {
            StdJavaPrimitiveTypes originalValue = new StdJavaPrimitiveTypes(
                true, Byte.MAX_VALUE, Character.MAX_VALUE, Short.MAX_VALUE,
                Integer.MAX_VALUE, Long.MAX_VALUE, Float.MAX_VALUE, Double.MAX_VALUE
            );

            String expected = """
              {"booleanValue":true,"byteValue":127,"charValue":"￿","shortValue":32767,"intValue":2147483647,"longValue":9223372036854775807,"floatValue":3.4028235E38,"doubleValue":1.7976931348623157E308}""";

            assertEquals( expected, jsonCodec.toJson(originalValue) );
            assertJsonRoundTripFor( originalValue );
        }

        @Test
        public void defaultJavaPrimitiveTypes_expectThemToNotBeSerialized() {
            StdJavaPrimitiveTypes originalValue = new StdJavaPrimitiveTypes(
                false, (byte) 0, (char) 0, (short) 0,
                0, 0L, 0.0f, 0.0
            );

            // NB I investigated dropping these default values out, but I did not find a
            //    simple way to tell GSON to do that.
            String expected = """
              {"booleanValue":false,"byteValue":0,"charValue":"\\u0000","shortValue":0,"intValue":0,"longValue":0,"floatValue":0.0,"doubleValue":0.0}""";

            assertEquals( expected, jsonCodec.toJson(originalValue) );
            assertJsonRoundTripFor( originalValue );
        }

        @Test
        public void stdJavaPrimitiveTypesWithPrettyPrinter() {
            StdJavaPrimitiveTypes originalValue = new StdJavaPrimitiveTypes(
                true, Byte.MAX_VALUE, Character.MAX_VALUE, Short.MAX_VALUE,
                Integer.MAX_VALUE, Long.MAX_VALUE, Float.MAX_VALUE, Double.MAX_VALUE
            );

            String expected = """
                {
                  "booleanValue": true,
                  "byteValue": 127,
                  "charValue": "￿",
                  "shortValue": 32767,
                  "intValue": 2147483647,
                  "longValue": 9223372036854775807,
                  "floatValue": 3.4028235E38,
                  "doubleValue": 1.7976931348623157E308
                }""";

            assertEquals( expected, JsonCodec.ppJsonCodec.toJson(originalValue) );
            assertJsonRoundTripFor( originalValue );
        }

        @Test
        public void StdJavaObjectTypesWithValues() {
            StdJavaObjectTypes originalValue = new StdJavaObjectTypes(
                true, Byte.MAX_VALUE, Character.MAX_VALUE, Short.MAX_VALUE,
                Integer.MAX_VALUE, Long.MAX_VALUE, Float.MAX_VALUE, Double.MAX_VALUE,
                "Hello World"
            );

            String expected = """
              {"booleanValue":true,"byteValue":127,"charValue":"￿","shortValue":32767,"intValue":2147483647,"longValue":9223372036854775807,"floatValue":3.4028235E38,"doubleValue":1.7976931348623157E308,"stringValue":"Hello World"}""";

            assertEquals( expected, jsonCodec.toJson(originalValue) );
            assertJsonRoundTripFor( originalValue );
        }

        @Test
        public void stdJavaObjectTypesWithNulls() {
            StdJavaObjectTypes originalValue = new StdJavaObjectTypes(
                null, null, null, null, null, null, null, null, null
            );

            String expected = "{}";

            assertEquals( expected, jsonCodec.toJson(originalValue) );
            assertJsonRoundTripFor( originalValue );
        }

        @Test
        public void pojoWithOverriddenName() {
            PojoWithOverriddenName originalValue = new PojoWithOverriddenName("Kirk");

            String expected = """
                {"title":"Kirk"}""";

            assertEquals( expected, jsonCodec.toJson(originalValue) );
            assertJsonRoundTripFor( originalValue );
        }

        @Test
        public void pojoWithEmptyJavaCollections() {
            PojoWithJavaCollections originalValue = new PojoWithJavaCollections(
                Optional.empty(),
                List.of(),
                Set.of(),
                Map.of()
            );

            String expected = "{\"stringList\":[],\"stringSet\":[],\"stringMap\":{}}";

            assertEquals( expected, jsonCodec.toJson(originalValue) );
            assertJsonRoundTripFor( originalValue );
        }

        @Test
        public void pojoWithNullJavaCollections() {
            PojoWithJavaCollections originalValue = new PojoWithJavaCollections(null,null,null,null);
            PojoWithJavaCollections expectedValue = new PojoWithJavaCollections( Optional.empty(),List.of(),Set.of(),Map.of());

            String expected = "{}";

            assertEquals( expected, jsonCodec.toJson(originalValue) );
            assertJsonRoundTripFor( originalValue, expectedValue );
        }

        @Test
        public void pojoWithJavaCollections() {
            PojoWithJavaCollections originalValue = new PojoWithJavaCollections(
                Optional.of("o"),List.of("a","b"),Set.of("1"),Map.of("a","1"));

            String expected = "{\"stringOptional\":\"o\",\"stringList\":[\"a\",\"b\"],\"stringSet\":[\"1\"],\"stringMap\":{\"a\":\"1\"}}";

            assertEquals( expected, jsonCodec.toJson(originalValue) );
            assertJsonRoundTripFor( originalValue );
        }

        @Test
        public void pojoWithEmptyFPCollections() {
            PojoWithFPCollections originalValue = new PojoWithFPCollections(
                FP.emptyOption(),
                FP.toList(),
                FP.toSet(),
                FP.toMap(),
                FP.toVector(),
                FP.toConsList()
            );

            String expected = "{\"stringFPList\":[],\"stringFPSet\":[],\"stringFPMap\":{}," +
                "\"stringRRBVector\":[],\"stringConsList\":[]}";

            assertEquals( expected, jsonCodec.toJson(originalValue) );
            assertJsonRoundTripFor( originalValue );
        }

        @Test
        public void pojoWithNullFPCollections() {
            PojoWithFPCollections originalValue = new PojoWithFPCollections(null,null,null,null, null,null);
            PojoWithFPCollections expectedValue = new PojoWithFPCollections( FP.emptyOption(),FP.toList(),FP.toSet(),FP.toMap(), FP.toVector(), FP.toConsList() );

            String expected = "{}";

            assertEquals( expected, jsonCodec.toJson(originalValue) );

            // NB: when adding a new type to PojoWithFPCollections and this assertion fails, remember
            //     to register the default value for the new type with DefaultValueRegistry.
            assertJsonRoundTripFor( originalValue, expectedValue );
        }

        @Test
        public void pojoWithFPCollections() {
            PojoWithFPCollections originalValue = new PojoWithFPCollections(
                FP.option("o"),FP.toList("a","b"),FP.toSet("1"),
                FP.toMap("a","1"),FP.toVector("v1","v2"),FP.toConsList("c1","c2")
            );

            // NB notice that stringConsList has been reversed
            String expected = "{\"stringFPOption\":\"o\",\"stringFPList\":[\"a\",\"b\"],\"stringFPSet\":[\"1\"]," +
                "\"stringFPMap\":{\"a\":\"1\"},\"stringRRBVector\":[\"v1\",\"v2\"]," +
                "\"stringConsList\":[\"c2\",\"c1\"]}";

            assertEquals( expected, jsonCodec.toJson(originalValue) );
            assertJsonRoundTripFor( originalValue );
        }

        @Test
        public void pojoWithNestedGeneric() {
            PojoWithNestedGeneric originalValue = new PojoWithNestedGeneric(
                List.of(new PojoWithGeneric<>(1.3))
            );

            String expected = "{\"list\":[{\"value\":1.3}]}";

            assertEquals( expected, jsonCodec.toJson(originalValue) );
            assertJsonRoundTripFor( originalValue );
        }

        @Test
        public void pojoWithDtmAsMillisSinceEpoch() {
            PojoWithDtmAsMillisSinceEpoch originalValue = new PojoWithDtmAsMillisSinceEpoch(
                new DTM( 2020,5,26, 10,30 )
            );

            String expected = "{\"when\":\"2020-05-26T10:30:00.0Z\"}";

            assertEquals( expected, jsonCodec.toJson(originalValue) );
            assertJsonRoundTripFor( originalValue );
        }

        @Test
        public void pojoWithDayAsMillisSinceEpoch() {
            PojoWithDay originalValue = new PojoWithDay(
                new Day( 2020,5,26 )
            );

            String expected = "{\"when\":\"2020-05-26\"}";

            assertEquals( expected, jsonCodec.toJson(originalValue) );
            assertJsonRoundTripFor( originalValue );
        }

        @Test
        public void pojoWithLazyVal() {
            PojoWithLazyVal originalValue = new PojoWithLazyVal("Jim");

            String expected = "{\"name\":\"Jim\"}";

            assertEquals( expected, jsonCodec.toJson(originalValue) );
            assertJsonRoundTripFor( originalValue );
        }
    }

    @Nested
    public class ParseJsonTestCases {
        @Test
        public void pojoWithOverriddenName() {
            PojoWithOverriddenName expectedValue = new PojoWithOverriddenName("Kirk");

            assertEquals( expectedValue, jsonCodec.fromJson("{\"title\":\"Kirk\"}", PojoWithOverriddenName.class) );
            assertEquals( expectedValue, jsonCodec.fromJson("{\"ref\":\"Kirk\"}", PojoWithOverriddenName.class) );
            assertEquals( expectedValue, jsonCodec.fromJson("{\"header\":\"Kirk\"}", PojoWithOverriddenName.class) );

            assertNotEquals( expectedValue, jsonCodec.fromJson("{\"name\":\"Kirk\"}", PojoWithOverriddenName.class) );
        }

        @Test
        public void pojoWithExplicitNullValue() {
            PojoWithOverriddenName expectedValue = new PojoWithOverriddenName(null);

            assertEquals( expectedValue, jsonCodec.fromJson("{\"title\":null}", PojoWithOverriddenName.class) );
        }
    }

    @Nested
    public class CustomCodecTestCases {
        @Test
        public void serializeClassWithCustomCodec() {
            Car car = new Car("Evie", "Tesla");

            assertJsonRoundTripFor( car );

            // ensures that the codec kicked in
            assertEquals("\"Tesla-Evie\"", jsonCodec.toJson(car) );
        }
    }

    @Nested
    public class ObjectHierarchyTestCases {
        @Test
        public void dogDTORoundTrip() {
            JsonCodec codec = new JsonCodecBuilder().create();

            Dog origDog = new Dog( "dog1" );

            String dogJson = codec.toJson( origDog, Dog.class );

            assertEquals(
                "{\"name\":\"dog1\"}",
                dogJson
            );

            Animal returnedDog = codec.fromJson( dogJson, Dog.class );

            assertEquals( origDog, returnedDog );
        }

        @Test
        public void cowAndDogDTOsRoundTrip() {
            JsonCodec codec = new JsonCodecBuilder()
                .identifyInheritanceTreeByClassName( Animal.class )
                .create();


            Cow origCow = new Cow( "cow1" );
            Dog origDog = new Dog( "dog1" );

            String cowJson = codec.toJson( origCow, Animal.class );
            String dogJson = codec.toJson( origDog, Animal.class );

            assertEquals(
                "{\"name\":\"cow1\",\"type\":\"mosaics.json.Cow\"}",
                cowJson
            );

            assertEquals(
                "{\"name\":\"dog1\",\"type\":\"mosaics.json.Dog\"}",
                dogJson
            );

            Animal returnedCow = codec.fromJson( cowJson, Animal.class );
            Animal returnedDog = codec.fromJson( dogJson, Animal.class );

            assertEquals( origCow, returnedCow );
            assertEquals( origDog, returnedDog );
        }

        @Test
        public void classRoundTrip() {
            JsonCodec codec = new JsonCodecBuilder().create();

            String json = codec.toJson( String.class, Class.class );

            assertEquals( "\"java.lang.String\"", json );

            Class roundTrip = codec.fromJson( json, Class.class );

            assertEquals( String.class, roundTrip );
        }

        @Test
        public void nullClassRoundTrip() {
            JsonCodec codec = new JsonCodecBuilder().create();

            String json = codec.toJson( null, Class.class );

            assertEquals( "null", json );

            Class roundTrip = codec.fromJson( "", Class.class );

            assertEquals( null, roundTrip );
        }

        @Test
        public void blankClassRoundTrip() {
            JsonCodec codec = new JsonCodecBuilder().create();

            Class roundTrip = codec.fromJson( "null", Class.class );

            assertEquals( null, roundTrip );
        }
        
        @Nested
        public class SealedClassTestCases {
            @Test
            public void givenInterfaceWithNoRegistration_expectItToBeHandled() {
                JsonCodec codec = new JsonCodecBuilder().create();


                Tesla origTesla = new Tesla( "tesla" );
                BMW   origBmw   = new BMW( "bmw" );

                String teslaJson = codec.toJson( origTesla, SealedInterface.class );
                String bmwJson   = codec.toJson( origBmw, SealedInterface.class );

                assertEquals(
                    "{\"name\":\"tesla\",\"type\":\"mosaics.json.Tesla\"}",
                    teslaJson
                );

                assertEquals(
                    "{\"name\":\"bmw\",\"type\":\"mosaics.json.BMW\"}",
                    bmwJson
                );

                SealedInterface returnedTesla = codec.fromJson( teslaJson, SealedInterface.class );
                SealedInterface returnedBmw   = codec.fromJson( bmwJson, SealedInterface.class );

                assertEquals( origTesla, returnedTesla );
                assertEquals( origBmw, returnedBmw );
            }

            @Test
            public void givenAbstractClassWithNoRegistration_expectItToBeHandled() {
                JsonCodec codec = new JsonCodecBuilder().create();


                Square origSquare = new Square( "square" );
                Circle origCircle = new Circle( "circle" );

                String squareJson = codec.toJson( origSquare, SealedAbstractClass.class );
                String circleJson = codec.toJson( origCircle, SealedAbstractClass.class );

                assertEquals(
                    "{\"name\":\"square\",\"type\":\"mosaics.json.Square\"}",
                    squareJson
                );

                assertEquals(
                    "{\"name\":\"circle\",\"type\":\"mosaics.json.Circle\"}",
                    circleJson
                );

                SealedAbstractClass returnedSquare = codec.fromJson( squareJson, SealedAbstractClass.class );
                SealedAbstractClass returnedCircle = codec.fromJson( circleJson, SealedAbstractClass.class );

                assertEquals( origSquare, returnedSquare );
                assertEquals( origCircle, returnedCircle );
            }
        }
    }

    private void assertJsonRoundTripFor( Object originalValue ) {
        assertJsonRoundTripFor( originalValue, originalValue );
    }

    private void assertJsonRoundTripFor( Object originalValue, Object expectedValue ) {
        String json        = jsonCodec.toJson( originalValue );
        Object parsedValue = jsonCodec.fromJson( json, originalValue.getClass() );

        assertEquals( expectedValue, parsedValue );
    }


    @Value
    @GsonCodec( CarJsonCodec.class )
    public static class Car {
        private String name;
        private String model;
    }

    public static class CarJsonCodec extends TypeAdapter<Car> {
        public void write( JsonWriter out, Car value ) throws IOException {
            out.value( value.getModel() + "-" + value.getName() );
        }

        @Override
        public Car read( JsonReader in ) throws IOException {
            String txt = in.nextString();
            int    i   = txt.indexOf('-');

            return new Car(txt.substring(i+1), txt.substring(0,i));
        }
    }
}
