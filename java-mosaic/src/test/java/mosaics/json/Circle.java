package mosaics.json;

import lombok.EqualsAndHashCode;
import lombok.Value;


@Value
@EqualsAndHashCode(callSuper=false)
public class Circle extends SealedAbstractClass {
    private String name;
}
