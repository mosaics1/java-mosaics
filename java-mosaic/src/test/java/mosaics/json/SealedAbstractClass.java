package mosaics.json;

public sealed abstract class SealedAbstractClass permits Circle, Square {
    public abstract String getName();
}
