package mosaics.json;

import lombok.Value;
import mosaics.io.IOUtils;
import mosaics.junit.JMFileExtension;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.parallel.Isolated;

import java.io.File;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;


@Isolated
@ExtendWith(JMFileExtension.class)
public class NDJsonFileReaderTest {
    @Test
    public void givenMissingFile_expectNoPojosToBeReadIn( File dir ) {
        File f = new File(dir, "missing.ndjson");

        try ( NDJsonFileReader<MyPojo> in = new NDJsonFileReader<>(MyPojo.class, f) ) {
            assertFalse( in.hasNext() );
        }
    }

    @Test
    public void givenSinglePojoInJsonFile_expectOnePojoToBeReadIn(
        @JMFileExtension.FileContents(lines = """
            {"name": "John", "age": 14}
            """)
        File jsonFile
    ) {
        try ( NDJsonFileReader<MyPojo> in = new NDJsonFileReader<>(MyPojo.class, jsonFile) ) {
            assertTrue( in.hasNext() );
            assertEquals( new MyPojo("John", 14), in.next() );
            assertFalse( in.hasNext() );
        }
    }

    @Test
    public void givenTwoPojosInJsonFile_expectTwoPojoToBeReadIn(
        @JMFileExtension.FileContents(lines = """
            {"name": "John", "age": 14}
            {"name": "Sarah", "age": 16}
            """)
        File jsonFile
    ) {
        try ( NDJsonFileReader<MyPojo> in = new NDJsonFileReader<>(MyPojo.class, jsonFile) ) {
            assertTrue( in.hasNext() );
            assertEquals( new MyPojo("John", 14), in.next() );
            assertTrue( in.hasNext() );
            assertEquals( new MyPojo("Sarah", 16), in.next() );
            assertFalse( in.hasNext() );
        }
    }

    @Test
    public void givenTwoPojosWithBlankLinesInJsonFile_expectTwoPojoToBeReadIn(
        @JMFileExtension.FileContents(lines = """
            
            
            {"name": "John", "age": 14}
            
            {"name": "Sarah", "age": 16}
            
            """)
        File jsonFile
    ) {
        try ( NDJsonFileReader<MyPojo> in = new NDJsonFileReader<>(MyPojo.class, jsonFile) ) {
            assertTrue( in.hasNext() );
            assertEquals( new MyPojo("John", 14), in.next() );
            assertTrue( in.hasNext() );
            assertEquals( new MyPojo("Sarah", 16), in.next() );
            assertFalse( in.hasNext() );
        }
    }

    @Test
    public void givenTwoPojosWithCommentsInJsonFile_expectTwoPojoToBeReadIn(
        @JMFileExtension.FileContents(lines = """
            # comment 1
            
            {"name": "John", "age": 14}
            
            # comment 2
                      # comment 3
            {"name": "Sarah", "age": 16}
            # comment 4
            """)
        File jsonFile
    ) {
        try ( NDJsonFileReader<MyPojo> in = new NDJsonFileReader<>(MyPojo.class, jsonFile) ) {
            assertTrue( in.hasNext() );
            assertEquals( new MyPojo("John", 14), in.next() );
            assertTrue( in.hasNext() );
            assertEquals( new MyPojo("Sarah", 16), in.next() );
            assertFalse( in.hasNext() );
        }
    }

    @Test
    public void givenTwoPojosWithCommentWithinJsonString_expectTwoPojoToBeReadIn(
        @JMFileExtension.FileContents(lines = """
            {"name": "John", "age": 14}
            {"name": "Sarah #tag", "age": 16}            
            """)
        File jsonFile
    ) {
        try ( NDJsonFileReader<MyPojo> in = new NDJsonFileReader<>(MyPojo.class, jsonFile) ) {
            assertTrue( in.hasNext() );
            assertEquals( new MyPojo("John", 14), in.next() );
            assertTrue( in.hasNext() );
            assertEquals( new MyPojo("Sarah #tag", 16), in.next() );
            assertFalse( in.hasNext() );
        }
    }

    /**
     * As of GSON 2.9, reading in records is not supported.
     * The first  solution tried was: https://github.com/Marcono1234/gson-record-type-adapter-factory
     * however it had two faults, 1) it did not know about our custom types (even though gson had been told)
     * and 2) it did not know how to set default values in the java records (which is a feature of the JavaClass)
     * and thus the second approach was to create our own JDK15 record integration (see Jdk15RecordTypeFactory).
     */
    @Test
    public void readRecords(
        @JMFileExtension.FileContents(
            name="books.ndjson",
            lines = """
                    |{"name":"b1"}
                    |{"name":"b2"}
                    """
        )
        File ndjsonFile
    ) {
        List<Book> actual = NDJsonFileReader.readAll( Book.class, ndjsonFile );

        List<Book> expected = List.of(
            new Book("b1"),
            new Book("b2")
        );

        assertEquals( expected, actual );
    }

    private static record Book(String name) {}

    @Value
    public static class MyPojo {
        private final String name;
        private final int    age;
    }
}
