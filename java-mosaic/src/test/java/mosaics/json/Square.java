package mosaics.json;

import lombok.EqualsAndHashCode;
import lombok.Value;


@Value
@EqualsAndHashCode(callSuper=false)
public class Square extends SealedAbstractClass {
    private String name;
}
