package mosaics.json;

import lombok.Value;


@Value
public class Dog implements Animal {
    public String name;

    public Dog( String name ) {
        this.name = name;
    }
}
