package mosaics.utils;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


/**
 *
 */
public class BitUtilsTest {

    @Test
    public void testBitSet() {
        assertEquals( 0, BitUtils.bitSet( 0, 0 ) );
        assertEquals( 2, BitUtils.bitSet( 0, 2 ) );
        assertEquals( 4, BitUtils.bitSet( 0, 4 ) );
        assertEquals( 5, BitUtils.bitSet( 1, 4 ) );
    }

    @Test
    public void testBitClear() {
        assertEquals( 0, BitUtils.bitClear(0, 0) );
        assertEquals( 1, BitUtils.bitClear(1, 0) );
        assertEquals( 2, BitUtils.bitClear(2, 0) );
        assertEquals( 4, BitUtils.bitClear(4, 0) );
        assertEquals( 5, BitUtils.bitClear(5, 0) );
        assertEquals( 0, BitUtils.bitClear(1, 1) );
        assertEquals( 4, BitUtils.bitClear(4, 1) );
        assertEquals( 4, BitUtils.bitClear(5, 1) );
        assertEquals( 1, BitUtils.bitClear(5, 4) );
        assertEquals( 0, BitUtils.bitClear(5, 5) );
        assertEquals( 8, BitUtils.bitClear(13, 5) );
    }

}
