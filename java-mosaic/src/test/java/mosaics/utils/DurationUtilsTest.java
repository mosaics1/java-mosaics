package mosaics.utils;

import mosaics.lang.Backdoor;
import mosaics.lang.time.DTMUtils;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class DurationUtilsTest {

    @Test
    public void testToStringMillis() {
        assertEquals( "0ms", DurationUtils.toStringMillis(0) );
        assertEquals( "1ms", DurationUtils.toStringMillis(1) );
        assertEquals( "2ms", DurationUtils.toStringMillis(2) );
        assertEquals( "999ms", DurationUtils.toStringMillis(999) );

        assertEquals( "1s", DurationUtils.toStringMillis(1000) );
        assertEquals( "1s 1ms", DurationUtils.toStringMillis(1001) );
        assertEquals( "1s 2ms", DurationUtils.toStringMillis(1002) );
        assertEquals( "1s 999ms", DurationUtils.toStringMillis(1999) );
        assertEquals( "2s", DurationUtils.toStringMillis(2000) );

        assertEquals( "1m", DurationUtils.toStringMillis( DTMUtils.MINUTE_MILLIS) );
        assertEquals( "1m 1ms", DurationUtils.toStringMillis( DTMUtils.MINUTE_MILLIS+1) );
        assertEquals( "1m 1s 1ms", DurationUtils.toStringMillis( DTMUtils.MINUTE_MILLIS+1001) );
        assertEquals( "2m", DurationUtils.toStringMillis(2* DTMUtils.MINUTE_MILLIS) );

        assertEquals( "1h", DurationUtils.toStringMillis( DTMUtils.HOUR_MILLIS) );
        assertEquals( "1h 1ms", DurationUtils.toStringMillis( DTMUtils.HOUR_MILLIS+1) );
        assertEquals( "1h 1s 1ms", DurationUtils.toStringMillis( DTMUtils.HOUR_MILLIS+1001) );
        assertEquals( "1h 1m 1s 1ms", DurationUtils.toStringMillis( DTMUtils.HOUR_MILLIS+ DTMUtils.MINUTE_MILLIS+1001) );
        assertEquals( "2h", DurationUtils.toStringMillis(2* DTMUtils.HOUR_MILLIS) );

        assertEquals( "1d", DurationUtils.toStringMillis( DTMUtils.DAY_MILLIS) );
        assertEquals( "1d 1ms", DurationUtils.toStringMillis( DTMUtils.DAY_MILLIS+1) );
        assertEquals( "1d 1s 1ms", DurationUtils.toStringMillis( DTMUtils.DAY_MILLIS+1001) );
        assertEquals( "1d 1h 1m 1s 1ms", DurationUtils.toStringMillis( DTMUtils.DAY_MILLIS+ DTMUtils.HOUR_MILLIS+ DTMUtils.MINUTE_MILLIS+1001) );
        assertEquals( "2d", DurationUtils.toStringMillis(2* DTMUtils.DAY_MILLIS) );
    }

    @Test
    public void testToStringNanos() {
        assertEquals( "0ns", DurationUtils.toStringNanos(0) );
        assertEquals( "1ns", DurationUtils.toStringNanos(1) );
        assertEquals( "2ns", DurationUtils.toStringNanos(2) );
        assertEquals( "999ns", DurationUtils.toStringNanos(999) );

        assertEquals( "1ms", DurationUtils.toStringNanos(Backdoor.NUMBER_OF_NANOSECONDS_PER_MILLISECOND ) );
        assertEquals( "1ms 1ns", DurationUtils.toStringNanos(Backdoor.NUMBER_OF_NANOSECONDS_PER_MILLISECOND +1) );

        assertEquals( "1s", DurationUtils.toStringNanos(1000*Backdoor.NUMBER_OF_NANOSECONDS_PER_MILLISECOND ) );
        assertEquals( "1s 1ms", DurationUtils.toStringNanos(1001*Backdoor.NUMBER_OF_NANOSECONDS_PER_MILLISECOND ) );
        assertEquals( "1s 2ms", DurationUtils.toStringNanos(1002*Backdoor.NUMBER_OF_NANOSECONDS_PER_MILLISECOND ) );
        assertEquals( "1s 999ms", DurationUtils.toStringNanos(1999*Backdoor.NUMBER_OF_NANOSECONDS_PER_MILLISECOND ) );
        assertEquals( "2s", DurationUtils.toStringNanos(2000*Backdoor.NUMBER_OF_NANOSECONDS_PER_MILLISECOND ) );

        assertEquals( "1m", DurationUtils.toStringNanos( DTMUtils.MINUTE_MILLIS*Backdoor.NUMBER_OF_NANOSECONDS_PER_MILLISECOND ) );
        assertEquals( "1m 1ms", DurationUtils.toStringNanos((DTMUtils.MINUTE_MILLIS+1)*Backdoor.NUMBER_OF_NANOSECONDS_PER_MILLISECOND ) );
        assertEquals( "1m 1s 1ms", DurationUtils.toStringNanos((DTMUtils.MINUTE_MILLIS+1001)*Backdoor.NUMBER_OF_NANOSECONDS_PER_MILLISECOND ) );
        assertEquals( "2m", DurationUtils.toStringNanos(2* DTMUtils.MINUTE_MILLIS*Backdoor.NUMBER_OF_NANOSECONDS_PER_MILLISECOND ) );

        assertEquals( "1h", DurationUtils.toStringNanos( DTMUtils.HOUR_MILLIS*Backdoor.NUMBER_OF_NANOSECONDS_PER_MILLISECOND ) );
        assertEquals( "1h 1ms", DurationUtils.toStringNanos((DTMUtils.HOUR_MILLIS+1)*Backdoor.NUMBER_OF_NANOSECONDS_PER_MILLISECOND ) );
        assertEquals( "1h 1s 1ms", DurationUtils.toStringNanos((DTMUtils.HOUR_MILLIS+1001)*Backdoor.NUMBER_OF_NANOSECONDS_PER_MILLISECOND ) );
        assertEquals( "1h 1m 1s 1ms", DurationUtils.toStringNanos((DTMUtils.HOUR_MILLIS+ DTMUtils.MINUTE_MILLIS+1001)*Backdoor.NUMBER_OF_NANOSECONDS_PER_MILLISECOND ) );
        assertEquals( "2h", DurationUtils.toStringNanos(2* DTMUtils.HOUR_MILLIS*Backdoor.NUMBER_OF_NANOSECONDS_PER_MILLISECOND ) );

        assertEquals( "1d", DurationUtils.toStringNanos( DTMUtils.DAY_MILLIS*Backdoor.NUMBER_OF_NANOSECONDS_PER_MILLISECOND ) );
        assertEquals( "1d 1ms", DurationUtils.toStringNanos((DTMUtils.DAY_MILLIS+1)*Backdoor.NUMBER_OF_NANOSECONDS_PER_MILLISECOND ) );
        assertEquals( "1d 1s 1ms", DurationUtils.toStringNanos((DTMUtils.DAY_MILLIS+1001)*Backdoor.NUMBER_OF_NANOSECONDS_PER_MILLISECOND ) );
        assertEquals( "1d 1h 1m 1s 1ms", DurationUtils.toStringNanos((DTMUtils.DAY_MILLIS+ DTMUtils.HOUR_MILLIS+ DTMUtils.MINUTE_MILLIS+1001)*Backdoor.NUMBER_OF_NANOSECONDS_PER_MILLISECOND ) );
        assertEquals( "2d", DurationUtils.toStringNanos(2* DTMUtils.DAY_MILLIS*Backdoor.NUMBER_OF_NANOSECONDS_PER_MILLISECOND ) );
    }

}
