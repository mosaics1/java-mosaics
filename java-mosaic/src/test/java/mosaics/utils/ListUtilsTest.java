package mosaics.utils;

import mosaics.fp.FP;
import mosaics.fp.collections.Tuple2;
import lombok.Value;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;


@SuppressWarnings( "unchecked" )
public class ListUtilsTest {


    @Nested
    public class CopyToListTestCases {
        @Test
        public void copyEmptyIterable_expectEmptyList() {
            List<String> list = new ArrayList();
            List<String> copy = ListUtils.copyToList( list );

            assertTrue( copy.isEmpty() );
        }

        @Test
        public void givenThreeItemsInIterable_copy_expectThreeItemsInList() {
            List<String> list = Arrays.asList( "a", "b", "c" );
            List<String> copy = ListUtils.copyToList( list );

            assertArrayEquals( new String[] {"a", "b", "c"}, copy.toArray() );
        }

        @Test
        public void givenThreeItemsInIterable_addNewValueToOriginalIterable_expectListToNotChange() {
            List<String> list = new ArrayList(Arrays.asList( "a", "b", "c" ));
            List<String> copy = ListUtils.copyToList( list );

            list.add( "D" );

            assertArrayEquals( new String[] {"a", "b", "c"}, copy.toArray() );
        }
    }


    @Nested
    public class RemoveFirstTestCases {
        @Test
        public void givenEmptyList_removePredicateIfCalledWouldReturnTrue_expectFalseAndNoChangeToList() {
            List<String> list = new ArrayList<>();

            assertFalse( ListUtils.removeFirst(list, v -> false) );
            assertArrayEquals( new Object[] {}, list.toArray() );
        }

        @Test
        public void givenThreeElementList_removePredicateReturnsMatchesNone_expectFalseAndNoChangeToList() {
            List<String> list = new ArrayList<>(Arrays.asList("a","b","c"));

            assertFalse( ListUtils.removeFirst(list, v -> false) );
            assertArrayEquals( new Object[] {"a", "b", "c"}, list.toArray() );
        }

        @Test
        public void givenThreeElementList_removePredicateReturnsMatchesFirstElement_expectTrueAndFirstElementToBeRemoved() {
            List<String> list = new ArrayList<>(Arrays.asList("a","b","c"));

            assertTrue( ListUtils.removeFirst(list, v -> v.equals("a")) );
            assertArrayEquals( new Object[] {"b", "c"}, list.toArray() );
        }

        @Test
        public void givenThreeElementList_removePredicateReturnsMatchesAllElements_expectTrueAndFirstElementToBeRemoved() {
            List<String> list = new ArrayList<>(Arrays.asList("a","b","c"));

            assertTrue( ListUtils.removeFirst(list, v -> true) );
            assertArrayEquals( new Object[] {"b", "c"}, list.toArray() );
        }

        @Test
        public void givenThreeElementList_removePredicateReturnsMatchesSecondElement_expectTrueAndSecondElementToBeRemoved() {
            List<String> list = new ArrayList<>(Arrays.asList("a","b","c"));

            assertTrue( ListUtils.removeFirst(list, v -> v.equals("b")) );
            assertArrayEquals( new Object[] {"a", "c"}, list.toArray() );
        }

        @Test
        public void givenThreeElementList_removePredicateReturnsMatchesThirdElement_expectTrueAndThirdElementToBeRemoved() {
            List<String> list = new ArrayList<>(Arrays.asList("a","b","c"));

            assertTrue( ListUtils.removeFirst(list, v -> v.equals("c")) );
            assertArrayEquals( new Object[] {"a", "b"}, list.toArray() );
        }
    }


    @Nested
    public class FirstMatchTestCases {
        @Test
        public void givenEmptyList_predicateIfCalledWouldReturnTrue_expectNoneNoChangeToList() {
            List<String> list = Collections.emptyList();

            assertEquals( FP.emptyOption(), ListUtils.firstMatch(list, v -> false) );
            assertArrayEquals( new Object[] {}, list.toArray() );
        }

        @Test
        public void givenThreeElementList_predicateNeverMatches_expectNoneAndNoChangeToList() {
            List<String> list = Arrays.asList("a","b","c");

            assertEquals( FP.emptyOption(), ListUtils.firstMatch(list, v -> false) );
            assertArrayEquals( new Object[] {"a", "b", "c"}, list.toArray() );
        }

        @Test
        public void givenThreeElementList_predicateMatchesFirstElement_expectFirstElementAndNoChangeToList() {
            List<String> list = Arrays.asList("a","b","c");

            assertEquals( FP.option("a"), ListUtils.firstMatch(list, v -> v.equals("a")) );
            assertArrayEquals( new Object[] {"a", "b", "c"}, list.toArray() );
        }

        @Test
        public void givenThreeElementList_predicateMatchesAllElements_expectFirstElementAndNoChangeToList() {
            List<String> list = Arrays.asList("a","b","c");

            assertEquals( FP.option("a"), ListUtils.firstMatch(list, v -> true) );
            assertArrayEquals( new Object[] {"a", "b", "c"}, list.toArray() );
        }

        @Test
        public void givenThreeElementList_predicateMatchesSecondElement_expectSecondElementAndNoChangeToList() {
            List<String> list = Arrays.asList("a","b","c");

            assertEquals( FP.option("b"), ListUtils.firstMatch(list, v -> v.equals("b")) );
            assertArrayEquals( new Object[] {"a", "b", "c"}, list.toArray() );
        }

        @Test
        public void givenThreeElementList_predicateMatchesThirdElement_expectThirdElementAndNoChangeToList() {
            List<String> list = Arrays.asList("a","b","c");

            assertEquals( FP.option("c"), ListUtils.firstMatch(list, v -> v.equals("c")) );
            assertArrayEquals( new Object[] {"a", "b", "c"}, list.toArray() );
        }
    }


    @Nested
    public class MatchAndMapFirstResultTestCases {
        @Test
        public void givenEmptyList_predicateIfCalledWouldReturnMatch_expectNoneNoChangeToList() {
            List<String> list = Collections.emptyList();

            assertEquals( FP.emptyOption(), ListUtils.matchAndMapFirstResult(list, v -> FP.option("v")) );
            assertArrayEquals( new Object[] {}, list.toArray() );
        }

        @Test
        public void givenThreeElementList_predicateNeverMatches_expectNoneAndNoChangeToList() {
            List<String> list = Arrays.asList("a","b","c");

            assertEquals( FP.emptyOption(), ListUtils.matchAndMapFirstResult(list, v -> FP.emptyOption()) );
            assertArrayEquals( new Object[] {"a", "b", "c"}, list.toArray() );
        }

        @Test
        public void givenThreeElementList_predicateMatchesFirstElement_expectMappedFirstElementAndNoChangeToList() {
            List<String> list = Arrays.asList("a","b","c");

            assertEquals( FP.option("1"), ListUtils.matchAndMapFirstResult(list, v -> {if (v.equals("a")) return FP.option("1"); else return FP.emptyOption(); }) );
            assertArrayEquals( new Object[] {"a", "b", "c"}, list.toArray() );
        }

        @Test
        public void givenThreeElementList_predicateMatchesAllElements_expectMappedFirstElementAndNoChangeToList() {
            List<String> list = Arrays.asList("a","b","c");

            assertEquals( FP.option("v"), ListUtils.matchAndMapFirstResult(list, v -> FP.option("v")) );
            assertArrayEquals( new Object[] {"a", "b", "c"}, list.toArray() );
        }

        @Test
        public void givenThreeElementList_predicateMatchesSecondElement_expectMappedSecondElementAndNoChangeToList() {
            List<String> list = Arrays.asList("a","b","c");

            assertEquals( FP.option("2"), ListUtils.matchAndMapFirstResult(list, v -> {if (v.equals("b")) return FP.option("2"); else return FP.emptyOption(); }) );
            assertArrayEquals( new Object[] {"a", "b", "c"}, list.toArray() );
        }

        @Test
        public void givenThreeElementList_predicateMatchesThirdElement_expectThirdElementAndNoChangeToList() {
            List<String> list = Arrays.asList("a","b","c");

            assertEquals( FP.option("3"), ListUtils.matchAndMapFirstResult(list, v -> {if (v.equals("c")) return FP.option("3"); else return FP.emptyOption(); }) );
            assertArrayEquals( new Object[] {"a", "b", "c"}, list.toArray() );
        }
    }


    @Nested
    public class MapTestCases {
        @Test
        public void mapStrings() {
            assertArrayEquals( new Integer[] {}, ListUtils.map("", c -> (int) c ).toArray() );
            assertArrayEquals( new Integer[] {(int) 'a', (int) 'b', (int) 'c'}, ListUtils.map("abc", c -> (int) c ).toArray() );
            assertArrayEquals( new String[] {"aa","bb", "cc"}, ListUtils.map("abc", c -> ""+c+c ).toArray() );
        }

        @Test
        public void mapLists() {
            assertArrayEquals( new String[] {}, ListUtils.mapCollection(Collections.emptyList(), Object::toString ).toArray() );
            assertArrayEquals( new String[] {"1", "2", "3"}, ListUtils.mapCollection(Arrays.asList(1,2,3), Object::toString).toArray() );
        }

        @Test
        public void iterableLists() {
            assertArrayEquals( new String[] {}, ListUtils.mapCollection(Collections.emptySet(), Object::toString ).toArray() );
            assertArrayEquals( new String[] {"1", "2", "3"}, ListUtils.mapIterable(FP.toVector(1,2,3), Object::toString).toArray() );
        }
    }


    @Nested
    public class FlattenTestCases {
        @Test
        public void flattenListOfLists() {
            assertArrayEquals( new Object[] {}, ListUtils.flatten().toArray() );
            assertArrayEquals( new Object[] {}, ListUtils.flatten(Collections.emptyList(), Collections.emptyList()).toArray() );
            assertArrayEquals( new Object[] {"1","2","3"}, ListUtils.flatten(Collections.emptyList(), Arrays.asList("1","2"), Collections.emptyList(), Arrays.asList("3")).toArray() );
            assertArrayEquals( new Object[] {"1","2","3","4"}, ListUtils.flatten(Collections.emptyList(), Arrays.asList("1","2"), Collections.emptyList(), Arrays.asList("3","4")).toArray() );
        }
    }


    @Nested
    public class PartitionTestCases {
        @Test
        public void givenEmptyList_partition_expectEmptyListsBack() {
            List l = new ArrayList();

            Tuple2<List, List> r = ListUtils.partition( l, arg -> true );

            assertEquals( 0, r.getFirst().size() );
            assertEquals( 0, r.getSecond().size() );
        }

        @Test
        public void givenABC_partitionByLowerCase_expectEmptyLHS() {
            List<Character> l = Arrays.asList( 'A', 'B', 'C' );

            Tuple2<List<Character>, List<Character>> r = ListUtils.partition( l, Character::isLowerCase );

            assertEquals( Arrays.<Character>asList(), r.getFirst() );
            assertEquals( Arrays.asList( 'A', 'B', 'C' ), r.getSecond() );
        }

        @Test
        public void givenAbC_partitionByLowerCase_expectEmptyLHS() {
            List<Character> l = Arrays.asList( 'A', 'b', 'C' );

            Tuple2<List<Character>, List<Character>> r = ListUtils.partition( l, Character::isLowerCase );

            assertEquals( Arrays.asList( 'b' ), r.getFirst() );
            assertEquals( Arrays.asList( 'A', 'C' ), r.getSecond() );
        }
    }


    @Nested
    public class PermutationTestCases {
        @Test
        public void givenEmptyList_permute2_expectNoCallbacks() {
            final List<List<Integer>> results = new ArrayList<>();

            ListUtils.permutations( Arrays.<Integer>asList(), 2, results::add );


            assertTrue( results.isEmpty() );
        }

        @Test
        public void givenOneElement_permute2_expectCallbackWithSingleValue() {
            final List<List<Integer>> results = new ArrayList<>();

            ListUtils.permutations( Arrays.<Integer>asList( 1 ), 2, results::add );

            List<List<Integer>> expected = Arrays.asList(
                Arrays.asList( 1 )
            );

            assertEquals( expected, results );
        }

        @Test
        public void givenTwoElements_permute1_expectCallbacks() {
            final List<List<Integer>> results = new ArrayList<>();

            ListUtils.permutations( Arrays.<Integer>asList( 1, 2 ), 1, results::add );

            List<List<Integer>> expected = Arrays.asList(
                Arrays.asList( 1 ),
                Arrays.asList( 2 )
            );

            assertEquals( expected, results );
        }

        @Test
        public void givenTwoElements_permute2_expectCallbacks() {
            final List<List<Integer>> results = new ArrayList<>();

            ListUtils.permutations( Arrays.<Integer>asList( 1, 2 ), 2, results::add );

            List<List<Integer>> expected = Arrays.asList(
                Arrays.asList( 1, 2 ),
                Arrays.asList( 2, 1 )
            );

            assertEquals( expected, results );
        }

        @Test
        public void givenThreeElements_permute2_expectCallbacks() {
            final List<List<Integer>> results = new ArrayList<>();

            ListUtils.permutations( Arrays.<Integer>asList( 1, 2, 3 ), 2, results::add );

            List<List<Integer>> expected = Arrays.asList(
                Arrays.asList( 1, 2 ),
                Arrays.asList( 1, 3 ),
                Arrays.asList( 2, 1 ),
                Arrays.asList( 2, 3 ),
                Arrays.asList( 3, 1 ),
                Arrays.asList( 3, 2 )
            );

            assertEquals( expected, results );
        }

        @Test
        public void givenThreeElements_permute3_expectCallbacks() {
            final List<List<Integer>> results = new ArrayList<>();

            ListUtils.permutations( Arrays.<Integer>asList( 1, 2, 3 ), 3, results::add );

            List<List<Integer>> expected = Arrays.asList(
                Arrays.asList( 1, 2, 3 ),
                Arrays.asList( 1, 3, 2 ),
                Arrays.asList( 2, 1, 3 ),
                Arrays.asList( 2, 3, 1 ),
                Arrays.asList( 3, 1, 2 ),
                Arrays.asList( 3, 2, 1 )
            );

            assertEquals( expected, results );
        }
    }


    @Nested
    public class ForEachReversedTestCases {
        @Test
        public void givenNull_forEachReversed_expectIllegalArgumentException() {
            List<String> audit = new ArrayList<>();

            try {
                ListUtils.<String>forEachReversed( null, audit::add );
                fail( "Expected IllegalArgumentException" );
            } catch ( IllegalArgumentException ex ) {
                assertEquals( "'list' must not be null", ex.getMessage() );
            }
        }

        @Test
        public void givenEmptyList_forEachReversed_expectNoCallbacks() {
            List<String> audit = new ArrayList<>();

            ListUtils.<String>forEachReversed( Arrays.asList(), audit::add );

            assertEquals( 0, audit.size() );
        }

        @Test
        public void givenSingleElementList_forEachReversed_expectOneCallback() {
            List<String> audit = new ArrayList<>();

            ListUtils.forEachReversed( Arrays.asList( "a" ), audit::add );

            List<String> expected = Arrays.asList( "a" );
            assertEquals( expected, audit );
        }

        @Test
        public void givenThreeElementList_forEachReversed_expectThreeCallbacksInReverse() {
            List<String> audit = new ArrayList<>();

            ListUtils.forEachReversed( Arrays.asList( "a", "b", "c" ), audit::add );

            List<String> expected = Arrays.asList( "c", "b", "a" );
            assertEquals( expected, audit );
        }
    }


    @Nested
    public class FilterTestCases {
        @Test
        public void filterStrings() {
            assertArrayEquals( new Object[] {}, ListUtils.filter(null, v -> true).toArray() );
            assertArrayEquals( new Object[] {}, ListUtils.filter(Collections.emptyList(), v -> true).toArray() );
            assertArrayEquals( new Object[] {}, ListUtils.filter(Arrays.asList("aa","b","c"), v -> false).toArray() );
            assertArrayEquals( new Object[] {"aa"}, ListUtils.filter(Arrays.asList("aa","b","c"), v -> v.length() > 1).toArray() );
            assertArrayEquals( new Object[] {"aa","bb"}, ListUtils.filter(Arrays.asList("aa","bb","c"), v -> v.length() > 1).toArray() );
            assertArrayEquals( new Object[] {"aa","bb","cc"}, ListUtils.filter(Arrays.asList("aa","bb","cc"), v -> v.length() > 1).toArray() );
        }
    }


    @Nested
    public class FoldTestCases {
        @Test
        public void foldAnArray() {
            assertEquals( 0, ListUtils.foldArray(new String[] {}, 0, (soFar,v) -> soFar + v.length()).intValue() );
            assertEquals( 1, ListUtils.foldArray(new String[] {"a"}, 0, (soFar,v) -> soFar + v.length()).intValue() );
            assertEquals( 2, ListUtils.foldArray(new String[] {"ab"}, 0, (soFar,v) -> soFar + v.length()).intValue() );
            assertEquals( 4, ListUtils.foldArray(new String[] {"ab"}, 2, (soFar,v) -> soFar + v.length()).intValue() );
            assertEquals( 6, ListUtils.foldArray(new String[] {"ab","cd"}, 2, (soFar,v) -> soFar + v.length()).intValue() );
        }
    }


    @Nested
    public class GroupByTestCases {
        @Test
        public void givenNull_expectEmptyMap() {
            Map<String,List<StringDTO>> actual = ListUtils.groupBy( null, x -> {throw new UnsupportedOperationException();} );

            assertEquals( 0, actual.size() );
        }

        @Test
        public void givenEmptyList_expectEmptyMap() {
            List<StringDTO>             list   = Collections.emptyList();
            Map<String,List<StringDTO>> actual = ListUtils.groupBy( list, x -> {throw new UnsupportedOperationException();} );

            assertEquals( 0, actual.size() );
        }

        @Test
        public void givenAFewElementsWithDifferentKeys_expectThemToBeSeparated() {
            List<StringDTO>             list   = Arrays.asList(new StringDTO("a"),new StringDTO("b"),new StringDTO("c"));
            Map<String,List<StringDTO>> actual = ListUtils.groupBy( list, StringDTO::getName );

            Map<String,List<StringDTO>> expected = MapUtils.asMap(
                "a", Collections.singleton(new StringDTO("a")),
                "b", Collections.singleton(new StringDTO("b")),
                "c", Collections.singleton(new StringDTO("c"))
            );

            assertEquals( 3, actual.size() );
            assertEquals( expected.toString(), actual.toString() );
        }

        @Test
        public void givenAFewElementsWithTheSameLength_groupByLength() {
            List<StringDTO>             list   = Arrays.asList(new StringDTO("a"),new StringDTO("b"),new StringDTO("cc"));
            Map<Integer,List<StringDTO>> actual = ListUtils.groupBy( list, v -> v.getName().length() );

            Map<Integer,List<StringDTO>> expected = MapUtils.asMap(
                1, Arrays.asList(new StringDTO("a"), new StringDTO("b")),
                2, Collections.singleton(new StringDTO("cc"))
            );

            assertEquals( 2, actual.size() );
            assertEquals( expected.toString(), actual.toString() );
        }
    }


    @Nested
    public class HashCodeTestCases {
        @Test
        public void givenTwoDifferentEmptyLists_expectTheSameHashCode() {
            List<String> a = new ArrayList<>();
            List<String> b = new ArrayList<>();

            assertNotSame( a, b );
            assertEquals( ListUtils.deepHashCode(a), ListUtils.deepHashCode(b) );
        }

        @Test
        public void givenTwoDifferentNonEmptyListsThatContainTheSameValues_expectTheSameHashCodes() {
            List<String> a = Arrays.asList( "a", "b" );
            List<String> b = Arrays.asList( "a", "b" );

            assertNotSame( a, b );
            assertEquals( ListUtils.deepHashCode(a), ListUtils.deepHashCode(b) );
        }

        @Test
        public void givenTwoDifferentNonEmptyListsThatAreTheMirrorOrderOfEachOther_expectListUtilsHashCodeToDiffer() {
            List<String> a = Arrays.asList( "a", "b" );
            List<String> b = Arrays.asList( "b", "a" );

            assertNotSame( a, b );
            assertNotEquals( ListUtils.deepHashCode(a), ListUtils.deepHashCode(b) );
        }

        @Test
        public void givenANonEmptyList_expectObjectHashCodeAndListUtilsHashCodeToDiffer() {
            List<String> a = Arrays.asList( "a", "b" );

            assertNotSame( a.hashCode(), ListUtils.deepHashCode(a) );
        }

        @Test
        public void givenAnEmptyList_expectAddingAValueToChangeTheListUtilsHashCode() {
            List<String> list = new ArrayList<>();

            int hashCode1 = ListUtils.deepHashCode( list );

            list.add("v");

            int hashCode2 = ListUtils.deepHashCode( list );

            assertNotEquals( hashCode1, hashCode2 );
        }

        @Test
        public void givenAnEmptyList_expectAddingAValueToChangeTheObjectHashCode() {
            List<String> list = new ArrayList<>();

            int hashCode1 = list.hashCode();

            list.add("v");

            int hashCode2 = list.hashCode();

            assertNotEquals( hashCode1, hashCode2 );
        }
    }


    @Nested
    public class EqualsTestCases {
        @Test
        public void iterableEquals() {
            assertTrue( ListUtils.iterableEquals(Arrays.asList(), Arrays.asList()) );
            assertTrue( ListUtils.iterableEquals(Arrays.asList("a"), Arrays.asList("a")) );
            assertTrue( ListUtils.iterableEquals(Arrays.asList("a", "b"), Arrays.asList("a", "b")) );
            assertFalse( ListUtils.iterableEquals(Arrays.asList("a"), Arrays.asList("a", "b")) );
            assertFalse( ListUtils.iterableEquals(Arrays.asList("a", "b"), Arrays.asList("a")) );
            assertFalse( ListUtils.iterableEquals(Arrays.asList("a", "b"), Arrays.asList("a", "c")) );
            assertFalse( ListUtils.iterableEquals(Arrays.asList("b", "b"), Arrays.asList("a", "b")) );
            assertFalse( ListUtils.iterableEquals(Arrays.asList("b", "a"), Arrays.asList("a", "b")) );
            assertFalse( ListUtils.iterableEquals(null, Arrays.asList("a", "b")) );
            assertFalse( ListUtils.iterableEquals(Arrays.asList("b", "a"), null) );
            assertFalse( ListUtils.iterableEquals(Arrays.asList("b", "a"), "b") );
            assertTrue( ListUtils.iterableEquals(Collections.EMPTY_LIST, Collections.EMPTY_LIST) );
            assertTrue( ListUtils.iterableEquals(null, null) );
        }
    }


    @Nested
    public class ToArrayTestCases {
        @Test
        public void toArraySupportsPrimitives() {
            assertArrayEquals( new int[] {}, (int[]) ListUtils.toArraySupportsPrimitives(Integer.TYPE,Collections.emptyList()) );
            assertArrayEquals( new int[] {1,2,3}, (int[]) ListUtils.toArraySupportsPrimitives(Integer.TYPE,Arrays.asList(1,2,3)) );
        }

        @Test
        public void toArray() {
            assertArrayEquals( new String[] {}, ListUtils.toArray(String.class,Collections.emptyList()) );
            assertArrayEquals( new String[] {"a","b","c"}, ListUtils.toArray(String.class,Arrays.asList("a","b","c")) );
        }
    }


    @Nested
    public class AsLinkedListTestCases {
        @Test
        public void expectContentsOfArrayToBeAsSpecified() {
            assertEquals( Collections.emptyList(), ListUtils.asLinkedList() );
            assertArrayEquals( Collections.singleton("a").toArray(), ListUtils.asLinkedList("a").toArray() );
            assertArrayEquals( Arrays.asList("a","b").toArray(), ListUtils.asLinkedList("a","b").toArray() );
        }

        @Test
        public void expectListToBeAnInstanceOfLinkedList() {
            assertTrue( ListUtils.asLinkedList() instanceof LinkedList );
            assertTrue( ListUtils.asLinkedList("a") instanceof LinkedList );
            assertTrue( ListUtils.asLinkedList("a","b") instanceof LinkedList );
        }
    }


    @Nested
    public class RemoveAllTestCases {
        @Test
        public void givenEmptyList_expectPredicateToNotBeCalled() {
            List<String> list = new ArrayList<>();
            ListUtils.removeAll( list, f -> {throw new UnsupportedOperationException();} );

            assertArrayEquals( new String[] {}, list.toArray() );
        }

        @Test
        public void givenNonEmptyListAndPredicateThatDoesNotMatch_expectListToBeUnchanged() {
            List<String> list = new ArrayList<>(Arrays.asList("a","b","c"));
            ListUtils.removeAll( list, f -> false );

            assertArrayEquals( new String[] {"a","b","c"}, list.toArray() );
        }

        @Test
        public void givenNonEmptyListAndPredicateThatAlwaysMatches_expectListToBeEmptied() {
            List<String> list = new ArrayList<>(Arrays.asList("a","b","c"));
            ListUtils.removeAll( list, f -> true );

            assertArrayEquals( new String[] {}, list.toArray() );
        }

        @Test
        public void givenNonEmptyList_removeFirstValue() {
            List<String> list = new ArrayList<>(Arrays.asList("a","b","c"));
            ListUtils.removeAll( list, f -> f.equals("a") );

            assertArrayEquals( new String[] {"b","c"}, list.toArray() );
        }

        @Test
        public void givenNonEmptyList_removeSecondValue() {
            List<String> list = new ArrayList<>(Arrays.asList("a","b","c"));
            ListUtils.removeAll( list, f -> f.equals("b") );

            assertArrayEquals( new String[] {"a","c"}, list.toArray() );
        }

        @Test
        public void givenNonEmptyList_removeLastValue() {
            List<String> list = new ArrayList<>(Arrays.asList("a","b","c"));
            ListUtils.removeAll( list, f -> f.equals("c") );

            assertArrayEquals( new String[] {"a","b"}, list.toArray() );
        }

        @Test
        public void givenNonEmptyList_removeMultipleButNotAll() {
            List<String> list = new ArrayList<>(Arrays.asList("a","a","c"));
            ListUtils.removeAll( list, f -> f.equals("a") );

            assertArrayEquals( new String[] {"c"}, list.toArray() );
        }
    }


    @Nested
    public class EnsureSizeTestCases {
        @Test
        public void ensureSize() {
            assertArrayEquals( new String[] {}, ListUtils.ensureSize(new ArrayList<>(), 0, "0").toArray() );
            assertArrayEquals( new String[] {"0"}, ListUtils.ensureSize(new ArrayList<>(), 1, "0").toArray() );
            assertArrayEquals( new String[] {"1"}, ListUtils.ensureSize(new ArrayList<>(Arrays.asList("1")), 0, "0").toArray() );
            assertArrayEquals( new String[] {"1"}, ListUtils.ensureSize(new ArrayList<>(Arrays.asList("1")), 1, "0").toArray() );
            assertArrayEquals( new String[] {"1","0"}, ListUtils.ensureSize(new ArrayList<>(Arrays.asList("1")), 2, "0").toArray() );
            assertArrayEquals( new String[] {"1","0","0"}, ListUtils.ensureSize(new ArrayList<>(Arrays.asList("1")), 3, "0").toArray() );

            assertArrayEquals( new String[] {"1","2"}, ListUtils.ensureSize(new ArrayList<>(Arrays.asList("1","2")), 0, "0").toArray() );
            assertArrayEquals( new String[] {"1","2"}, ListUtils.ensureSize(new ArrayList<>(Arrays.asList("1","2")), 1, ".").toArray() );
            assertArrayEquals( new String[] {"1","2"}, ListUtils.ensureSize(new ArrayList<>(Arrays.asList("1","2")), 2, ".").toArray() );
            assertArrayEquals( new String[] {"1","2","."}, ListUtils.ensureSize(new ArrayList<>(Arrays.asList("1","2")), 3, ".").toArray() );
        }
    }

    @Value
    public static class StringDTO {
        private String name;
    }
}
