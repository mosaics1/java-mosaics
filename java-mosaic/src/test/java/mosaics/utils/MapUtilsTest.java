package mosaics.utils;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

import static org.junit.jupiter.api.Assertions.*;


@SuppressWarnings({"UnnecessaryBoxing","ConstantConditions"})
public class MapUtilsTest {

    @SuppressWarnings({"CachedNumberConstructorCall", "deprecation"})
    @Nested
    public class CreateMapTestCases {
        @Test
        public void testCreateWithDefault() {
            Map<String, String> map = MapUtils.createWithDefault( k -> "v:"+k );

            assertEquals( 0, map.size() );

            String a = map.get( "abc" );
            assertEquals( "v:abc", a );
            assertSame( a, map.get( "abc" ) );
            assertEquals( 1, map.size() );

            assertTrue( map.containsKey( "123" ) );;
            assertEquals( "v:123", map.get( "123" ) );
            assertEquals( 2, map.size() );
        }

        @Test
        public void testCreateTreeMapWithDefault() {
            Map<String, String> map = MapUtils.createTreeMapWithDefault( k -> "v:"+k );

            assertTrue( map instanceof TreeMap );

            assertEquals( 0, map.size() );
            assertEquals( "v:abc", map.get("abc") );
            assertEquals( 1, map.size() );

            assertFalse( map.containsKey( "123" ) );
            assertEquals( "v:123", map.get( "123" ) );
            assertEquals( 2, map.size() );
        }

        @Test
        public void asMap() {
            Map<String, Integer> expectedMap1 = new HashMap<>();
            expectedMap1.put( "a", 1 );

            Map<String, Integer> expectedMap2 = new HashMap<>();
            expectedMap2.put( "a", 1 );
            expectedMap2.put( "b", 2 );


            assertEquals( new HashMap(), MapUtils.asMap() );
            assertEquals( expectedMap1, MapUtils.<String, Integer>asMap( "a", 1 ) );
            assertEquals( expectedMap2, MapUtils.<String, Integer>asMap( "a", 1, "b", 2 ) );
        }

        @Test
        public void asLinkedMap() {
            Map<String, Integer> expectedMap1 = new HashMap<>();
            expectedMap1.put( "a", 1 );

            Map<String, Integer> expectedMap2 = new HashMap<>();
            expectedMap2.put( "a", 1 );
            expectedMap2.put( "b", 2 );


            assertEquals( new HashMap(), MapUtils.asLinkedMap() );
            assertEquals( expectedMap1, MapUtils.<String, Integer>asLinkedMap( "a", 1 ) );
            assertEquals( expectedMap2, MapUtils.<String, Integer>asLinkedMap( "a", 1, "b", 2 ) );
            assertTrue( MapUtils.asLinkedMap() instanceof LinkedHashMap );
        }

        @Test
        public void asTreeMap() {
            Map<String, Integer> expectedMap1 = new HashMap<>();
            expectedMap1.put( "a", 1 );

            Map<String, Integer> expectedMap2 = new HashMap<>();
            expectedMap2.put( "a", 1 );
            expectedMap2.put( "b", 2 );


            assertEquals( new HashMap(), MapUtils.asTreeMap(new HashMap<>()) );
            assertEquals( expectedMap1, MapUtils.<String, Integer>asTreeMap(MapUtils.asMap("a", 1)) );
            assertEquals( expectedMap2, MapUtils.<String, Integer>asTreeMap(MapUtils.asMap("a", 1, "b", 2)) );
            assertTrue( MapUtils.asTreeMap(new HashMap<>()) instanceof TreeMap );
        }
    }
}
