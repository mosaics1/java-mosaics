package mosaics.utils;

import mosaics.fp.FP;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;


@SuppressWarnings( "unchecked" )
public class CollectionUtilsTest {

    @Nested
    public class DepthFirstTraversalTests {
        @Test
        public void givenNull_depthFirstTraversal_expectNoAction() {
            List actionCalls = new ArrayList();
            CollectionUtils.depthFirstTraversal( null, actionCalls::add );

            List expected = Collections.emptyList();
            assertEquals( expected, actionCalls );
        }

        @Test
        public void givenInteger_depthFirstTraversal_expectOneCall() {
            List actionCalls = new ArrayList();
            CollectionUtils.depthFirstTraversal( 42, actionCalls::add );

            List expected = Collections.singletonList( 42 );
            assertEquals( expected, actionCalls );
        }

        @Test
        public void givenEmptyPrimitiveArray_depthFirstTraversal_expectNoCalls() {
            List actionCalls = new ArrayList();
            CollectionUtils.depthFirstTraversal( new int[]{}, actionCalls::add );

            List expected = Collections.emptyList();
            assertEquals( expected, actionCalls );
        }

        @Test
        public void givenTwoElementPrimitiveArray_depthFirstTraversal_expectTwoCalls() {
            List actionCalls = new ArrayList();
            CollectionUtils.depthFirstTraversal( new int[]{1, 2}, actionCalls::add );

            List expected = Arrays.asList( 1, 2 );
            assertEquals( expected, actionCalls );
        }

        @Test
        public void givenTwoElementStringArray_depthFirstTraversal_expectTwoCalls() {
            List actionCalls = new ArrayList();
            CollectionUtils.depthFirstTraversal( new String[]{"a", "b"}, actionCalls::add );

            List expected = Arrays.asList( "a", "b" );
            assertEquals( expected, actionCalls );
        }

        @Test
        public void givenTwoElementStringArrayWhereOneElementIsNull_depthFirstTraversal_expectOneCall() {
            List actionCalls = new ArrayList();
            CollectionUtils.depthFirstTraversal( new String[]{"a", null}, actionCalls::add );

            List expected = Arrays.asList( "a" );
            assertEquals( expected, actionCalls );
        }

        @Test
        public void givenArrayWithinArray_depthFirstTraversal_expectThreeCalls() {
            List actionCalls = new ArrayList();
            CollectionUtils.depthFirstTraversal( new Object[]{"a", new int[]{2, 3}}, actionCalls::add );

            List expected = Arrays.asList( "a", 2, 3 );
            assertEquals( expected, actionCalls );
        }

        @Test
        public void givenEmptyJdkOptional_depthFirstTraversal_expectNoCalls() {
            List actionCalls = new ArrayList();
            CollectionUtils.depthFirstTraversal( Optional.empty(), actionCalls::add );

            List expected = Collections.emptyList();
            assertEquals( expected, actionCalls );
        }

        @Test
        public void givenNonEmptyJdkOptional_depthFirstTraversal_expectOneCall() {
            List actionCalls = new ArrayList();
            CollectionUtils.depthFirstTraversal( Optional.of( "42" ), actionCalls::add );

            List expected = Collections.singletonList( "42" );
            assertEquals( expected, actionCalls );
        }

        @Test
        public void givenJdkOptionalContainingAnotherOption_depthFirstTraversal_expectOneCall() {
            List actionCalls = new ArrayList();
            CollectionUtils.depthFirstTraversal( Optional.of( Optional.of( "Hello" ) ), actionCalls::add );

            List expected = Collections.singletonList( "Hello" );
            assertEquals( expected, actionCalls );
        }

        @Test
        public void givenEmptyfpOptional_depthFirstTraversal_expectNoCalls() {
            List actionCalls = new ArrayList();
            CollectionUtils.depthFirstTraversal( FP.emptyOption(), actionCalls::add );

            List expected = Collections.emptyList();
            assertEquals( expected, actionCalls );
        }

        @Test
        public void givenNonEmptyfpOptional_depthFirstTraversal_expectOneCall() {
            List actionCalls = new ArrayList();
            CollectionUtils.depthFirstTraversal( FP.option( "42" ), actionCalls::add );

            List expected = Collections.singletonList( "42" );
            assertEquals( expected, actionCalls );
        }

        @Test
        public void givenfpOptionalContainingAnotherOption_depthFirstTraversal_expectOneCall() {
            List actionCalls = new ArrayList();
            CollectionUtils.depthFirstTraversal( FP.option( FP.option( "Hello" ) ), actionCalls::add );

            List expected = Collections.singletonList( "Hello" );
            assertEquals( expected, actionCalls );
        }

        @Test
        public void givenListOfValues_depthFirstTraversal_expectCalls() {
            List actionCalls = new ArrayList();
            CollectionUtils.depthFirstTraversal( Arrays.asList( "a", null, FP.emptyOption(), Arrays.asList( "b", "c" ) ), actionCalls::add );

            List expected = Arrays.asList( "a", "b", "c" );
            assertEquals( expected, actionCalls );
        }

        @Test
        public void givenMap_depthFirstTraversal_expectKVCall() {
            Set actionCalls = new HashSet();
            Map<String, String> map = MapUtils.asMap( "k", "v" );

            CollectionUtils.depthFirstTraversal( map, actionCalls::add );

            Set expected = map.entrySet();
            assertEquals( expected, actionCalls );
        }

        @Test
        public void givenMapWithEntryThatHasNullValue_depthFirstTraversal_expectNoCalls() {
            Set actionCalls = new HashSet();
            Map<String, String> map = MapUtils.asMap( "k", null );

            CollectionUtils.depthFirstTraversal( map, actionCalls::add );

            Set expected = Collections.emptySet();
            assertEquals( expected, actionCalls );
        }

        @Test
        public void givenMapWithEntryThatHasEmptyOptionValue_depthFirstTraversal_expectNoCalls() {
            Set actionCalls = new HashSet();
            Map<String, String> map = MapUtils.asMap( "k", Optional.empty() );

            CollectionUtils.depthFirstTraversal( map, actionCalls::add );

            Set expected = Collections.emptySet();
            assertEquals( expected, actionCalls );
        }
    }


    @Nested
    public class IsEmptyTests {
        @Test
        public void givenEmptyString_isEmpty_expectTrue() {
            assertTrue( CollectionUtils.isEmpty( "" ) );
        }

        @Test
        public void givenBlankString_isEmpty_expectTrue() {
            assertTrue( CollectionUtils.isEmpty( "   " ) );
        }

        @Test
        public void givenNonBlankString_isEmpty_expectFalse() {
            assertFalse( CollectionUtils.isEmpty( " ab " ) );
        }

        @Test
        public void givenNone_isEmpty_expectTrue() {
            assertTrue( CollectionUtils.isEmpty(Optional.empty()) );
            assertTrue( CollectionUtils.isEmpty(FP.emptyOption()) );
            assertFalse( CollectionUtils.isEmpty(new Object()) );
        }
    }

}
