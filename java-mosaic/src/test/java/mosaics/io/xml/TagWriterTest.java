package mosaics.io.xml;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;


public class TagWriterTest {

    private StringBuilder buf = new StringBuilder();
    private TagWriter     out = TagWriter.xmlWriter(buf);


// SINGLE TAG

    @Test
    public void writeNothing_expectEmptyOutput() throws IOException {
        assertXML("");
    }

    @Test
    public void emptyTag() throws IOException {
        out.tag( "foo" );
        out.closeTag( "foo" );

        assertXML("<foo/>" );
    }

    @Test
    public void rootTagWithAttribute() throws IOException {
        out.tag( "foo" );
        out.attr( "v", "1" );
        out.closeTag( "foo" );

        assertXML("<foo v=\"1\"/>" );
    }

    @Test
    public void rootTagWithAttributeThatNeedsEscaping() throws IOException {
        out.tag( "foo" );
        out.attr( "v", "\"a\"" );
        out.closeTag( "foo" );

        assertXML("<foo v=\"&quot;a&quot;\"/>" );
    }

    @Test
    public void tryAddingAttributeBeforeATag_expectException() throws IOException {
        try {
            out.attr( "v", "1" );
            fail( "Expected IllegalStateException" );
        } catch ( IllegalStateException ex ) {
            assertEquals( "Cannot print an attribute when no tag is currently open", ex.getMessage() );
        }
    }

    @Test
    public void closeAnOpenTwice_expectException() throws IOException {
        out.tag( "foo" );
        out.attr( "v", "1" );
        out.closeTag( "foo" );

        try {
            out.closeTag( "foo" );

            fail( "Expected IllegalStateException" );
        } catch ( IllegalStateException ex ) {
            assertEquals( "Cannot close a tag when none is currently open", ex.getMessage() );
        }
    }

    @Test
    public void closeATagBeforeOpeningAnyTag_expectException() throws IOException {
        try {
            out.closeTag( "foo" );

            fail( "Expected IllegalStateException" );
        } catch ( IllegalStateException ex ) {
            assertEquals( "Cannot close a tag when none is currently open", ex.getMessage() );
        }
    }


// TAG WITH BODY TEXT

    @Test
    public void rootTagWithTextBody() throws IOException {
        out.tag( "foo" );
        out.bodyText( "hello world" );
        out.closeTag( "foo" );

        assertXML("<foo>hello world</foo>" );
    }

    @Test
    public void rootTagWithMultipleCallsToTextBody() throws IOException {
        out.tag( "foo" );
        out.bodyText( "hello" );
        out.bodyText( " " );
        out.bodyText( "world" );
        out.closeTag( "foo" );

        assertXML("<foo>hello world</foo>" );
    }

    @Test
    public void rootTagWithTextBodyThatNeededEscaping() throws IOException {
        out.tag( "foo" );
        out.bodyText( "hello <bold>world</bold>" );
        out.closeTag( "foo" );

        assertXML("<foo>hello &lt;bold&gt;world&lt;/bold&gt;</foo>" );
    }

    @Test
    public void rootTagWithTextBodyAndTwoAttributes() throws IOException {
        out.tag( "foo" );
        out.attr("a", "1" );
        out.attr("b", "2" );
        out.bodyText( "hello world" );
        out.closeTag( "foo" );

        assertXML("<foo a=\"1\" b=\"2\">hello world</foo>" );
    }

    @Test
    public void addAttrAfterBodyText_expectException() throws IOException {
        out.tag( "foo" );
        out.bodyText( "hello world" );

        try {
            out.attr("a", "1" );

            fail( "Expected IllegalStateException" );
        } catch ( IllegalStateException ex ) {
            assertEquals( "Cannot add attributes to a tag after its body has been written to", ex.getMessage() );
        }
    }


// TAG WITH ONE NESTED CHILD TAG

    @Test
    public void rootTagWithOneChildTagBothEmpty() throws IOException {
        out.tag( "foo" );
        out.tag( "bar" );
        out.closeTag( "bar" );
        out.closeTag( "foo" );

        assertXML(
            "<foo>",
            "  <bar/>",
            "</foo>"
        );
    }

    @Test
    public void rootTagWithOneChildThatHasATextBody() throws IOException {
        out.tag( "foo" );
        out.tag( "bar" );
        out.bodyText( "Red" );
        out.closeTag( "bar" );
        out.closeTag( "foo" );

        assertXML(
            "<foo>",
            "  <bar>Red</bar>",
            "</foo>"
        );
    }



// TAG WITH MULTIPLE NESTED CHILDREN


    @Test
    public void rootTagWithTwoChildrenBothWithTextBodies() throws IOException {
        out.tag( "foo" );
        out.tag( "colour", "Red" );
        out.tag( "colour", "Blue" );
        out.closeTag( "foo" );

        assertXML(
            "<foo>",
            "  <colour>Red</colour>",
            "  <colour>Blue</colour>",
            "</foo>"
        );
    }

    @Test
    public void rootTagWithTwoTagsWithinEachOther() throws IOException {
        out.tag( "foo" );
        out.tag( "bar" );
        out.tag( "colour", "Blue" );
        out.closeTag( "bar" );
        out.closeTag( "foo" );

        assertXML(
            "<foo>",
            "  <bar>",
            "    <colour>Blue</colour>",
            "  </bar>",
            "</foo>"
        );
    }

    @Test
    public void rootTagWithAChildTagThatHasTwoChildTextTags() throws IOException {
        out.tag( "foo" );
        out.tag( "bar" );
        out.tag( "colour", "Blue" );
        out.tag( "colour", "Red" );
        out.closeTag( "bar" );
        out.closeTag( "foo" );

        assertXML(
            "<foo>",
            "  <bar>",
            "    <colour>Blue</colour>",
            "    <colour>Red</colour>",
            "  </bar>",
            "</foo>"
        );
    }

    @Test
    public void rootTagWithAChildTagThatHasOneChildTextTagAndAnotherBodyTag() throws IOException {
        out.tag( "foo" );
        out.tag( "bar" );
        out.tag( "colour", "Blue" );
        out.tag( "hello" );
        out.tag( "colour", "Red" );
        out.closeTag( "hello" );
        out.closeTag( "bar" );
        out.closeTag( "foo" );

        assertXML(
            "<foo>",
            "  <bar>",
            "    <colour>Blue</colour>",
            "    <hello>",
            "      <colour>Red</colour>",
            "    </hello>",
            "  </bar>",
            "</foo>"
        );
    }

    private void assertXML( String...expectedXML ) throws IOException {
        out.close();

        String expected = Arrays.stream( expectedXML ).collect( Collectors.joining( "\n" ) );

        assertEquals( expected, buf.toString() );
    }
}
