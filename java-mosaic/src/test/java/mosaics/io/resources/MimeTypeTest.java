package mosaics.io.resources;

import mosaics.fp.collections.RRBVector;
import org.junit.jupiter.api.Test;

import static mosaics.io.resources.MimeType.lookupMimeType;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class MimeTypeTest {

//Accept: text/html
//Accept: image/*
//Accept: text/html, application/xhtml+xml, application/xml;q=0.9, */*;q=0.8


// getMimeTypesFrom

    @Test
    public void givenOneMimeType_callGetMimeTypesFrom_expectOneMimeType() {
        RRBVector<MimeType> actual   = MimeType.getMimeTypesFrom( "text/html" );
        RRBVector<MimeType> expected = RRBVector.toVector( MimeType.HTML );

        assertEquals( expected, actual );
    }

    @Test
    public void givenTwoMimeTypesWithNoQValues_callGetMimeTypesFrom_expectOrderToBeUntouched() {
        RRBVector<MimeType> actual   = MimeType.getMimeTypesFrom( "text/html, text/xml" );
        RRBVector<MimeType> expected = RRBVector.toVector( MimeType.HTML, MimeType.XML );

        assertEquals( expected, actual );
    }

    @Test
    public void givenTwoMimeTypesWithQValues050And045_callGetMimeTypesFrom_expectOrderToBeUntouched() {
        RRBVector<MimeType> actual   = MimeType.getMimeTypesFrom( "text/html;q=0.5, text/xml;q=0.45" );
        RRBVector<MimeType> expected = RRBVector.toVector( MimeType.HTML, MimeType.XML );

        assertEquals( expected, actual );
    }

    @Test
    public void givenTwoMimeTypesWithQValues045And050_callGetMimeTypesFrom_expectOrderToBeReversed() {
        RRBVector<MimeType> actual   = MimeType.getMimeTypesFrom( "text/html;q=0.45, text/xml;q=0.5" );
        RRBVector<MimeType> expected = RRBVector.toVector( MimeType.XML, MimeType.HTML );

        assertEquals( expected, actual );
    }

    @Test
    public void givenTwoMimeTypesWithQValues045AndDefault_callGetMimeTypesFrom_expectOrderToBeReversed() {
        RRBVector<MimeType> actual   = MimeType.getMimeTypesFrom( "text/html;q=0.45, text/xml" );
        RRBVector<MimeType> expected = RRBVector.toVector( MimeType.XML, MimeType.HTML );

        assertEquals( expected, actual );
    }

    @Test
    public void givenTwoMimeTypesOneWithWildCardsWithQValues045AndDefault_callGetMimeTypesFrom_expectOrderToBeReversed() {
        RRBVector<MimeType> actual   = MimeType.getMimeTypesFrom( "text/html;q=0.45, */*" );
        RRBVector<MimeType> expected = RRBVector.toVector( new MimeType("*/*"), MimeType.HTML );

        assertEquals( expected, actual );
    }


// matches

    @Test
    public void testMatch() {
        assertTrue( lookupMimeType("plain", "text").matches( lookupMimeType("plain", "text")) );
        assertFalse( lookupMimeType("plain", "html").matches( lookupMimeType("plain", "text")) );
        assertFalse( lookupMimeType("plain", "text").matches( lookupMimeType("plain", "html")) );
        assertFalse( lookupMimeType("application", "text").matches( lookupMimeType("plain", "text")) );
        assertFalse( lookupMimeType("plain", "text").matches( lookupMimeType("application", "text")) );

        assertTrue( lookupMimeType("plain", "*").matches( lookupMimeType("plain", "text")) );
        assertTrue( lookupMimeType("plain", "text").matches( lookupMimeType("plain", "*")) );
        assertTrue( lookupMimeType("*", "text").matches( lookupMimeType("plain", "text")) );
        assertTrue( lookupMimeType("plain", "text").matches( lookupMimeType("*", "text")) );
        assertTrue( lookupMimeType("*", "*").matches( lookupMimeType("plain", "text")) );
        assertTrue( lookupMimeType("plain", "text").matches( lookupMimeType("*", "*")) );
        assertTrue( lookupMimeType("plain", "*").matches( lookupMimeType("plain", "*")) );
        assertTrue( lookupMimeType("*", "text").matches( lookupMimeType("*", "text")) );
        assertTrue( lookupMimeType("*", "text").matches( lookupMimeType("plain", "*")) );
        assertTrue( lookupMimeType("plain", "*").matches( lookupMimeType("*", "text")) );
    }

}
