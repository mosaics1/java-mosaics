package mosaics.io.resources;

import mosaics.fp.Failure;
import mosaics.lang.ValidationFailure;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class MimeTypeAcceptEntryTest {

//Accept: text/html
//Accept: image/*
//Accept: text/html, application/xhtml+xml, application/xml;q=0.9, */*;q=0.8


// factoryMethod: fromSingleAcceptHeaderElement

    @Test
    public void givenExactMimeType_expectQFactorOf1() {
        MimeTypeAcceptEntry actual   = MimeTypeAcceptEntry.fromSingleAcceptHeaderElement( "text/html" ).getResult();
        MimeTypeAcceptEntry expected = new MimeTypeAcceptEntry( "text", "html", 1.0 );

        assertEquals( expected, actual );
    }

    @Test
    public void givenSecondaryWildCard() {
        MimeTypeAcceptEntry actual   = MimeTypeAcceptEntry.fromSingleAcceptHeaderElement( "text/*" ).getResult();
        MimeTypeAcceptEntry expected = new MimeTypeAcceptEntry( "text", "*", 1.0 );

        assertEquals( expected, actual );
    }

    @Test
    public void givenImplicitSecondaryWildCard() {
        MimeTypeAcceptEntry actual   = MimeTypeAcceptEntry.fromSingleAcceptHeaderElement( "text" ).getResult();
        MimeTypeAcceptEntry expected = new MimeTypeAcceptEntry( "text", "*", 1.0 );

        assertEquals( expected, actual );
    }

    @Test
    public void givenPrimaryWildCard() {
        MimeTypeAcceptEntry actual   = MimeTypeAcceptEntry.fromSingleAcceptHeaderElement( "*/html" ).getResult();
        MimeTypeAcceptEntry expected = new MimeTypeAcceptEntry( "*", "html", 1.0 );

        assertEquals( expected, actual );
    }

    @Test
    public void givenQFactorOf0Point45() {
        MimeTypeAcceptEntry actual   = MimeTypeAcceptEntry.fromSingleAcceptHeaderElement( "text/html;q=0.45" ).getResult();
        MimeTypeAcceptEntry expected = new MimeTypeAcceptEntry( "text", "html", 0.45 );

        assertEquals( expected, actual );
    }

    @Test
    public void givenPrimaryOnlyWithQFactorOf0Point45() {
        MimeTypeAcceptEntry actual   = MimeTypeAcceptEntry.fromSingleAcceptHeaderElement( "text;q=0.45" ).getResult();
        MimeTypeAcceptEntry expected = new MimeTypeAcceptEntry( "text", "*", 0.45 );

        assertEquals( expected, actual );
    }

    @Test
    public void givenBlankAcceptHeader_callFromSingleAcceptHeaderElement_expectParseFailure() {
        Failure actual   = MimeTypeAcceptEntry.fromSingleAcceptHeaderElement( "  " ).getFailure();
        Failure expected = new ValidationFailure("Blank Accept header: '  '");

        assertEquals( expected, actual );
    }

    @Test
    public void givenNonDecimalForQValue_callFromSingleAcceptHeaderElement_expectParseFailure() {
        Failure actual   = MimeTypeAcceptEntry.fromSingleAcceptHeaderElement("text/html;q=abc").getFailure();
        Failure expected = new ValidationFailure("Malformed quality value: 'abc'");

        assertEquals( expected, actual );
    }


// compareTo

    @Test
    public void compareQFactors_lowHigh_expectHighFirst() {
        MimeTypeAcceptEntry lhs = new MimeTypeAcceptEntry( "text", "html", 0.22 );
        MimeTypeAcceptEntry rhs = new MimeTypeAcceptEntry( "application", "json", 0.23 );

        assertEquals( 1, lhs.compareTo(rhs) );
    }

    @Test
    public void compareQFactors_equal_expectNoChange() {
        MimeTypeAcceptEntry lhs = new MimeTypeAcceptEntry( "text", "html", 0.23 );
        MimeTypeAcceptEntry rhs = new MimeTypeAcceptEntry( "application", "json", 0.23 );

        assertEquals( 0, lhs.compareTo(rhs) );
    }

    @Test
    public void compareQFactors_highLow_expectNoChange() {
        MimeTypeAcceptEntry lhs = new MimeTypeAcceptEntry( "text", "html", 0.24 );
        MimeTypeAcceptEntry rhs = new MimeTypeAcceptEntry( "application", "json", 0.23 );

        assertEquals( -1, lhs.compareTo(rhs) );
    }


// matches

    @Test
    public void givenHtmlText_callMatchesHtmlText_expectMatch() {
        MimeTypeAcceptEntry acceptEntry = new MimeTypeAcceptEntry( "text", "html", 0.24 );
        MimeType            mimeType    = MimeType.HTML;

        assertTrue( acceptEntry.matches(mimeType) );
    }

    @Test
    public void givenHtmlText_callMatchesHtmlXml_expectNoMatch() {
        MimeTypeAcceptEntry acceptEntry = new MimeTypeAcceptEntry( "text", "html", 0.24 );
        MimeType            mimeType    = MimeType.XML;

        assertFalse( acceptEntry.matches(mimeType) );
    }

    @Test
    public void givenHtmlWildCard_callMatchesHtmlText_expectMatch() {
        MimeTypeAcceptEntry acceptEntry = new MimeTypeAcceptEntry( "text", "*", 0.24 );
        MimeType            mimeType    = MimeType.HTML;

        assertTrue( acceptEntry.matches(mimeType) );
    }

    @Test
    public void givenWildCardText_callMatchesTextHtml_expectMatch() {
        MimeTypeAcceptEntry acceptEntry = new MimeTypeAcceptEntry( "*", "html", 0.24 );
        MimeType            mimeType    = MimeType.HTML;

        assertTrue( acceptEntry.matches(mimeType) );
    }

    @Test
    public void givenWildCardXML_callMatchesTextHtml_expectNoMatch() {
        MimeTypeAcceptEntry acceptEntry = new MimeTypeAcceptEntry( "*", "xml", 0.24 );
        MimeType            mimeType    = MimeType.HTML;

        assertFalse( acceptEntry.matches(mimeType) );
    }


// toString

    @Test
    public void givenQLessThanOne_callToString_expectFullMimeTypeWithQValue() {
        assertEquals( "*/xml;q=0.24", new MimeTypeAcceptEntry( "*", "xml", 0.24 ).toString() );
    }

    @Test
    public void givenQOfOne_callToString_expectMimeTypeWithNoExplicitQValue() {
        assertEquals( "text/xml", new MimeTypeAcceptEntry( "text", "xml", 1.0 ).toString() );
    }


// toMimeType

    @Test
    public void givenFullMimeType_callToMimeType() {
        MimeType mimeType = new MimeTypeAcceptEntry( "text", "html", 1.0 ).toMimeType();

        assertEquals( new MimeType("text/html", "htm", "html"), mimeType );
    }

    @Test
    public void givenMimeTypeWithSecondaryWildCard_callToMimeType() {
        MimeType mimeType = new MimeTypeAcceptEntry( "text", "*", 1.0 ).toMimeType();

        assertEquals( new MimeType("text/*"), mimeType );
    }

    @Test
    public void givenMimeTypeWithPrimaryWildCard_callToMimeType() {
        MimeType mimeType = new MimeTypeAcceptEntry( "*", "xml", 1.0 ).toMimeType();

        assertEquals( new MimeType("*/xml"), mimeType );
    }

    @Test
    public void givenFullWildCard_callToMimeType() {
        MimeType mimeType = new MimeTypeAcceptEntry( "*", "*", 1.0 ).toMimeType();

        assertEquals( new MimeType("*/*"), mimeType );
    }

}
