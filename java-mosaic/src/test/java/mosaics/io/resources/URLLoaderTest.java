package mosaics.io.resources;

import mosaics.fp.FP;
import mosaics.fp.collections.FPOption;
import mosaics.io.FileUtils;
import mosaics.lang.Backdoor;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.Map;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;


public class URLLoaderTest {

    // UNKNOWN PROTOCOL

    @Test
    public void unknownProtocol_tryAccessingMissingFile() {
        ResourceLoader urlService = new URLLoader();

        try {
            urlService.fetchDocument( String.class, "unknown:///missing_file.txt" );
        } catch ( UnknownProtocolException ex ) {
            assertEquals( "Unsupported protocol 'unknown' in 'unknown:///missing_file.txt'", ex.getMessage() );
        }
    }


    // CLASSPATH

    @Test
    public void classpathProtocol_tryAccessingMissingFile() {
        ResourceLoader urlService = new URLLoader();

        FPOption<Map<String,String>> properties = urlService.fetchProperties( "classpath:/missing_file.properties" );

        assertFalse(properties.hasValue());
    }

    @Test
    public void classpathProtocol_accessAPropertyFile() {
        ResourceLoader urlService = new URLLoader();

        FPOption<Map<String,String>> properties = urlService.fetchProperties("classpath:/mosaics/io/resources/mimetypes.properties");

        assertEquals(MimeType.PROPERTYFILE.getMimeType(), properties.get().get("properties"));
    }


    // FILE

    @Test
    public void fileProtocol_tryAccessingMissingFile() {
        ResourceLoader urlService = new URLLoader();

        FPOption<String> fileContents = urlService.fetchDocument( String.class, "file:///missing_file.txt", FPOption.of(Backdoor.UTF8), FP.emptyOption() );

        assertFalse( fileContents.hasValue() );
    }

    @Test
    public void fileProtocol_accessAPropertyFile() {
        File dir  = FileUtils.makeTempDirectory("junit","fileProtocol_accessAPropertyFile");
        File file = new File(dir, "config.properties");

        FileUtils.writeTextTo(file, "a=1", "b=2", "c=3");

        try {
            ResourceLoader urlService = new URLLoader();

            FPOption<String> fileContents = urlService.fetchDocument(String.class, "file://"+file.getAbsolutePath(), FPOption.of(Backdoor.UTF8), FP.emptyOption());

            assertEquals( "a=1\nb=2\nc=3\n", fileContents.get() );
        } finally {
            FileUtils.deleteAll(dir);
        }
    }

    @Test
    public void defaultFileProtocol_accessAPropertyFile() {
        File dir  = FileUtils.makeTempDirectory("junit","fileProtocol_accessAPropertyFile");
        File file = new File(dir, "config.properties");

        FileUtils.writeTextTo(file, "a=1", "b=2", "c=3");

        try {
            ResourceLoader urlService = new URLLoader().withDefaultProtocol( "file" );

            FPOption<String> fileContents = urlService.fetchDocument(String.class, file.getAbsolutePath(), FPOption.of(Backdoor.UTF8), FP.emptyOption());

            assertEquals( "a=1\nb=2\nc=3\n", fileContents.get() );
        } finally {
            FileUtils.deleteAll(dir);
        }
    }

    @Test
    public void fileProtocol_accessAPropertyFileAsAMapOfStrings() {
        File dir  = FileUtils.makeTempDirectory("junit","fileProtocol_accessAPropertyFile");
        File file = new File(dir, "config.properties");

        FileUtils.writeTextTo(file, "a=1", "b=2", "c=3");

        try {
            ResourceLoader urlService = new URLLoader();

            Map<String,String> properties = urlService.fetchProperties("file://" + file.getAbsolutePath()).get();

            assertEquals( "1", properties.get("a") );
            assertEquals( "2", properties.get("b") );
            assertEquals( "3", properties.get("c") );
            assertEquals( 3, properties.size() );
        } finally {
            FileUtils.deleteAll(dir);
        }
    }

    @Test
    public void fileProtocol_accessAPropertyFileAsPropertiesObj() {
        File dir  = FileUtils.makeTempDirectory("junit","fileProtocol_accessAPropertyFile");
        File file = new File(dir, "config.properties");

        FileUtils.writeTextTo(file, "a=1", "b=2", "c=3");

        try {
            ResourceLoader urlService = new URLLoader();

            Properties properties = urlService.fetchDocument(Properties.class, "file://" + file.getAbsolutePath()).get();

            assertEquals( "1", properties.get("a") );
            assertEquals( "2", properties.get("b") );
            assertEquals( "3", properties.get("c") );
            assertEquals( 3, properties.size() );
        } finally {
            FileUtils.deleteAll(dir);
        }
    }


}
