package mosaics.io;

import mosaics.io.writers.IndentWriter;
import mosaics.lang.Backdoor;
import org.junit.jupiter.api.Test;

import java.io.StringWriter;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;


public class IndentWriterTest {

    @Test
    public void givenANewWriter_writeNothing_expectNothingToBeWritten() {
        StringWriter sw = new StringWriter();

        new IndentWriter( sw );

        assertEquals( "", sw.toString() );
    }

    @Test
    public void givenANewWriter_writeAString_expectNoIndentation() {
        StringWriter    sw = new StringWriter();
        IndentWriter iw = new IndentWriter( sw );

        iw.print("hello");

        assertEquals( "hello", sw.toString() );
    }

    @Test
    public void givenANewWriter_incIndentationThenWriteAString_expectOneLevelOfIndentation() {
        StringWriter    sw = new StringWriter();
        IndentWriter iw = new IndentWriter( sw );

        iw.incIndent();
        iw.print("hello");

        assertEquals( "  hello", sw.toString() );
    }

    @Test
    public void givenANewWriter_incThenDecIndentationThenWriteAString_expectNoIndentation() {
        StringWriter    sw = new StringWriter();
        IndentWriter iw = new IndentWriter( sw );

        iw.incIndent();
        iw.decIndent();
        iw.print("hello");

        assertEquals( "hello", sw.toString() );
    }

    @Test
    public void givenANewWriter_writeSeveralLinesOfTextAtVaryingIndentationLevels() {
        StringWriter    sw = new StringWriter();
        IndentWriter iw = new IndentWriter( sw );

        iw.incIndent();
        iw.println( "hello" );

        iw.incIndent();
        iw.println( "world" );

        iw.decIndent();
        iw.println( "YOU" );
        iw.println( "ROCK" );

        String[] expectation = new String[] {
            "  hello",
            "    world",
            "  YOU",
            "  ROCK"
        };

        assertArrayEquals( expectation, sw.toString().split( Backdoor.NEWLINE) );
    }

}
