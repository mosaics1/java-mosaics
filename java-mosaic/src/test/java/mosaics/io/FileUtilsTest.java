package mosaics.io;


import mosaics.junit.JMFileExtension;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.io.File;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


@ExtendWith(JMFileExtension.class)
public class FileUtilsTest {

    @Test
    public void getNameWithoutExtension() {
        assertEquals( "/a/b/c", FileUtils.getNameWithoutExtension("/a/b/c.html") );
        assertEquals( "/a/b/c", FileUtils.getNameWithoutExtension("/a/b/c") );
        assertEquals( "/a/./c", FileUtils.getNameWithoutExtension("/a/./c") );
    }

    @Test
    public void testReadTextFile(
        @JMFileExtension.FileContents(lines="""
            abc
            defg"""
        ) File file
    ) {
        assertEquals( List.of("abc", "defg"), FileUtils.readTextFile(file));
    }

    @Nested
    public class TouchFileDirectoryTestCases {
        @Test
        public void givenExistingFile_expectNoChange(
            @JMFileExtension.FileContents(lines="""
            abc
            defg"""
            ) File file
        ) {
            long length = file.length();
            long timestamp = file.lastModified();

            FileUtils.touchFileDirectory( file );

            assertEquals( length, file.length() );
            assertEquals( timestamp, file.lastModified() );
            assertEquals( 1, file.getParentFile().listFiles().length );
        }

        @Test
        public void givenNoFileInExistingDir_expectNoChange( File dir ) {
            long timestamp = dir.lastModified();

            FileUtils.touchFileDirectory( new File(dir, "f.txt") );

            assertEquals( timestamp, dir.lastModified() );
            assertEquals( 0, dir.listFiles().length );
        }

        @Test
        public void givenNoFileInMissingDir_expectDirToBeCreated( File dir ) {
            File targetFile = new File( dir, "a/f.txt" );
            FileUtils.touchFileDirectory( targetFile );

            assertEquals( 1, dir.listFiles().length );
            assertEquals( 0, targetFile.getParentFile().listFiles().length );
            assertTrue( targetFile.getParentFile().exists() );
        }
    }

    @Nested
    public class TouchFileTestCases {
        @Test
        public void givenExistingFile_expectNoChange( @JMFileExtension.FileContents() File f ) {
            assertTrue( f.exists() );
            assertTrue( f.isFile() );
            assertEquals( 1, f.getParentFile().listFiles().length );

            long lastModified = f.lastModified();

            FileUtils.touchFile( f );

            assertTrue( f.exists() );
            assertTrue( f.isFile() );
            assertEquals( 1, f.getParentFile().listFiles().length );
            assertEquals( lastModified, f.lastModified() );
        }

        @Test
        public void givenDirectory_expectEmptyFileToBeCreated( File dir ) {
            File f = new File(dir, "file.txt");

            FileUtils.touchFile( f );

            assertTrue( f.exists() );
            assertTrue( f.isFile() );
            assertEquals( 0, f.length() );
            assertEquals( 1, f.getParentFile().listFiles().length );
        }

        @Test
        public void givenMissingDirectory_expectEmptyFileToBeCreated( File dir ) {
            File f = new File(dir, "missingDir/file.txt");

            FileUtils.touchFile( f );

            assertTrue( f.exists() );
            assertTrue( f.isFile() );
            assertEquals( 0, f.length() );
            assertEquals( 1, f.getParentFile().listFiles().length );
            assertEquals( "missingDir", f.getParentFile().getName() );
            assertEquals( "file.txt", f.getName() );
        }
    }
}
