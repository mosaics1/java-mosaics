package mosaics.lang;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.fail;


public class BackdoorTest {

    @Test
    public void testCopy() {
        byte[] source = new byte[] {1,2,3,4,5};

        assertArrayEquals( new byte[] {1,2,3,4,5}, Backdoor.copyBytes(source, 0, 5) );
        assertArrayEquals( new byte[] {1,2,3}, Backdoor.copyBytes(source, 0, 3) );
        assertArrayEquals( new byte[] {2,3}, Backdoor.copyBytes(source, 1, 3) );

        try {
            Backdoor.copyBytes(source, 1, 6);
            fail( "expected ArrayIndexOutOfBoundsException" );
        } catch ( ArrayIndexOutOfBoundsException ex ) {

        }
    }
}
