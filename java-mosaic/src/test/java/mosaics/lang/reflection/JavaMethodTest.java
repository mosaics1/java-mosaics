package mosaics.lang.reflection;

import mosaics.fp.FP;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.lang.annotation.Annotation;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class JavaMethodTest {
    @Test
    public void getAnnotations() {
        JavaMethod method = JavaClass.of(ClassThatHasAMethodWithAnnotations.class).getMethod( "method1" ).get();

        assertEquals( Set.of(Disabled.class), method.getAnnotations().map(Annotation::annotationType).toSet() );
    }

    @Test
    public void getShortSignature() {
        JavaMethod noArgMethod = JavaClass.of(ClassThatHasAMethodWithAnnotations.class).getMethod( "method1" ).get();
        JavaMethod oneStringArgMethod = JavaClass.of(ClassWithRangeOfMethodParameterTypes.class).getMethod( "methodWithOneStringArg", String.class ).get();
        JavaMethod onePrimitiveIntArgMethod = JavaClass.of(ClassWithRangeOfMethodParameterTypes.class).getMethod( "methodWithOnePrimitiveIntArg", int.class ).get();
        JavaMethod onePrimitiveIntArrayArgMethod = JavaClass.of(ClassWithRangeOfMethodParameterTypes.class).getMethod( "methodWithOnePrimitiveIntArrayArg", int[].class ).get();

        assertEquals("method1()", noArgMethod.getShortSignature());
        assertEquals("methodWithOneStringArg(java.lang.String)", oneStringArgMethod.getShortSignature());
        assertEquals("methodWithOnePrimitiveIntArg(int)", onePrimitiveIntArgMethod.getShortSignature());
        assertEquals("methodWithOnePrimitiveIntArrayArg([I)", onePrimitiveIntArrayArgMethod.getShortSignature());
    }

    @Test
    public void getFullSignature() {
        JavaMethod noArgMethod                   = JavaClass.of(ClassThatHasAMethodWithAnnotations.class).getMethod( "method1" ).get();
        JavaMethod oneStringArgMethod            = JavaClass.of(ClassWithRangeOfMethodParameterTypes.class).getMethod( "methodWithOneStringArg", String.class ).get();
        JavaMethod onePrimitiveIntArgMethod      = JavaClass.of(ClassWithRangeOfMethodParameterTypes.class).getMethod( "methodWithOnePrimitiveIntArg", int.class ).get();
        JavaMethod onePrimitiveIntArrayArgMethod = JavaClass.of(ClassWithRangeOfMethodParameterTypes.class).getMethod( "methodWithOnePrimitiveIntArrayArg", int[].class ).get();
        JavaMethod twoArgMethod                  = JavaClass.of(ClassWithRangeOfMethodParameterTypes.class).getMethod( "methodWithTwoArgs", String.class, int.class ).get();

        assertEquals(ClassThatHasAMethodWithAnnotations.class.getName()+"#method1()", noArgMethod.getFullSignature());
        assertEquals(ClassWithRangeOfMethodParameterTypes.class.getName()+"#methodWithOneStringArg(java.lang.String name)", oneStringArgMethod.getFullSignature());
        assertEquals(ClassWithRangeOfMethodParameterTypes.class.getName()+"#methodWithOnePrimitiveIntArg(int age)", onePrimitiveIntArgMethod.getFullSignature());
        assertEquals(ClassWithRangeOfMethodParameterTypes.class.getName()+"#methodWithOnePrimitiveIntArrayArg([I age)", onePrimitiveIntArrayArgMethod.getFullSignature());
        assertEquals(ClassWithRangeOfMethodParameterTypes.class.getName()+"#methodWithTwoArgs(java.lang.String name, int age)", twoArgMethod.getFullSignature());
    }

    @Nested
    public class ReturnTypeTestCases {
        @Test
        public void parameterisedReturnType() {
            JavaMethod m = JavaClass.of( MethodWithParameterisedReturnType.class ).getMethod( "getOrder" ).get();

            assertEquals( List.class, m.getReturnType().getJdkClass() );
            assertEquals( FP.wrapAll( JavaClass.STRING ), m.getReturnType().getClassGenerics() );
        }

        @Test
        public void chainedParameterisedReturnType() {
            JavaMethod m = JavaClass.of( TopLevelForMethodWithPassedDownParameterisedReturnType.class ).getMethod( "getOrder" ).get();

            assertEquals( MethodWithPassedDownParameterisedReturnType.class, m.getReturnType().getJdkClass() );
            assertEquals( FP.wrapAll( JavaClass.INTEGER_OBJECT ), m.getReturnType().getClassGenerics() );
        }
    }

    @Nested
    public class getMatchingSetterTestCases {
        @Test
        public void String_length() {
            assertEquals(FP.emptyOption(), JavaClass.STRING.getMethod("length").get().getMatchingSetter());
        }
    }

    public static class ClassThatHasAMethodWithAnnotations {
        @Disabled
        public void method1() {}
    }

    public static class ClassWithRangeOfMethodParameterTypes {
        public void methodWithOneStringArg(String name) {}
        public void methodWithOnePrimitiveIntArg(int age) {}
        public void methodWithOnePrimitiveIntArrayArg(int[] age) {}

        public void methodWithTwoArgs(String name, int age) {}
    }

    public static class MethodWithParameterisedReturnType {
        public List<String> getOrder() {return null;}
    }

    public static class TopLevelForMethodWithPassedDownParameterisedReturnType {
        public MethodWithPassedDownParameterisedReturnType<Integer> getOrder() { return null; }
    }

    public static class MethodWithPassedDownParameterisedReturnType<T> {
        public List<T> getOrder() {return null;}
    }
}
