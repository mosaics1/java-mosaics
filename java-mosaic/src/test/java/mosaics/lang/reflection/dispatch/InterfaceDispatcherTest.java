package mosaics.lang.reflection.dispatch;

import mosaics.lang.NotFoundException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.fail;


@SuppressWarnings({"UnnecessaryBoxing", "unused"})
public class InterfaceDispatcherTest {

// DISPATCH METHODS WITH RETURN VALUES

    @Test
    public void invokeWithClass_expectExceptionAsNoMatchingMethodDeclared() {
        InterfaceDispatcher<WrappedIntHandler> dispatcher = new InterfaceDispatcher<>(new WrappedIntHandler());

        try {
            dispatcher.dispatch(String.class);

            fail("expected exception");
        } catch ( NotFoundException ex ) {
            assertEquals( "Found no target method that takes 'java.lang.Class' as a parameter on 'mosaics.lang.reflection.dispatch.InterfaceDispatcherTest$WrappedIntHandler'", ex.getMessage() );
        }
    }

    @Test
    public void invokeStringArg_expectIntBack() {
        InterfaceDispatcher<StringHandler> dispatcher = new InterfaceDispatcher<>(new StringHandler());

        assertEquals(Integer.valueOf(3), dispatcher.dispatch("111"));
    }

    @Test
    public void invokePrimitiveIntArg_expectIntBack() {
        InterfaceDispatcher<PrimitiveIntHandler> dispatcher = new InterfaceDispatcher<>(new PrimitiveIntHandler());

        assertEquals(Integer.valueOf(40), dispatcher.dispatch(40));
        assertEquals(Integer.valueOf(42), dispatcher.dispatch(42));
        assertEquals(Integer.valueOf(42), dispatcher.dispatch(Integer.valueOf(42)));
    }

    @Test
    public void invokeWrappedIntArg_expectIntBack() {
        InterfaceDispatcher<WrappedIntHandler> dispatcher = new InterfaceDispatcher<>(new WrappedIntHandler());

        assertEquals(Integer.valueOf(40), dispatcher.dispatch(Integer.valueOf(40)));
        assertEquals(Integer.valueOf(42), dispatcher.dispatch(Integer.valueOf(42)));
        assertEquals(Integer.valueOf(42), dispatcher.dispatch(42));
    }

    @Test
    public void primitiveWrappedIntDistractors() {
        InterfaceDispatcher<HandlerWithPrimitiveWrappedIntDistractor> dispatcher = new InterfaceDispatcher<>(new HandlerWithPrimitiveWrappedIntDistractor());

        assertEquals("wrapped 40", dispatcher.dispatch(Integer.valueOf(40)));

        // NB Java auto wraps the primitive, and it leaves no marker saying that it has been done
        //    thus expect the wrapped version to be run.. as the dispatcher will select the closest
        //    match given the information that it has available
        assertEquals("wrapped 42", dispatcher.dispatch(42));
    }

    @Test
    public void inheritanceTest() {
        InterfaceDispatcher<ObjectHandler> dispatcher = new InterfaceDispatcher<>(new ObjectHandler());

        assertEquals("java.lang.String abc", dispatcher.dispatch("abc"));
        assertEquals("java.lang.Integer 40", dispatcher.dispatch(40));
    }


// DISPATCH METHODS WITH NO RETURN VALUE

    @Test
    public void invokeStringArgOnMethodWithNoReturnValue_expectSuccess() {
        StringHandlerNoReturn                      handler    = new StringHandlerNoReturn();
        InterfaceDispatcher<StringHandlerNoReturn> dispatcher = new InterfaceDispatcher<>( handler );

        assertNull( dispatcher.dispatch("111") );
        assertEquals("111", handler.v );
    }

    @Test
    public void givenDefaultMethod_invokeIntArgOnMethodWithThatDoesNotExist_expectDefaultMethodToBeInvoked() {
        StringHandlerNoReturn                      handler    = new StringHandlerNoReturn();
        InterfaceDispatcher<StringHandlerNoReturn> dispatcher = new InterfaceDispatcher<>( handler );

        dispatcher.dispatchVoid(111, v -> handler.v = "DEFAULT");
        assertEquals("DEFAULT", handler.v );
    }


    public static class StringHandlerNoReturn {
        public String v;

        public void handleString( String str ) {
            this.v = str;
        }
    }


// ambiguousMethods

    public static class StringHandler {
        public int handleString( String str ) {
            return str.length();
        }
    }

    public static class PrimitiveIntHandler {
        public int handlePrimitiveInt( int a ) {
            return a;
        }
    }

    public static class WrappedIntHandler {
        public int handleWrappedInteger( int a ) {
            return a;
        }
    }

    public static class ObjectHandler {
        public String handleEverything( Object o ) {
            return o.getClass().getName() + " " + o;
        }
    }

    public static class HandlerWithPrimitiveWrappedIntDistractor {
        public String handleWrappedInt( Integer a ) {
            return "wrapped "+a;
        }

        public String handlePrimitiveInt( int a ) {
            return "primitive " + a;
        }
    }
}
