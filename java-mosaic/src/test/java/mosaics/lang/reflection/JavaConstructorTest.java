package mosaics.lang.reflection;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class JavaConstructorTest {
    @Nested
    public class ToStringTestCases {
        @Test
        public void toStringTests() {
            assertEquals( "mosaics.lang.reflection.JavaConstructorTest$PojoWithConstructors()", getConstructor(PojoWithConstructors.class).toString() );
            assertEquals( "mosaics.lang.reflection.JavaConstructorTest$PojoWithConstructors(java.lang.String a)", getConstructor(PojoWithConstructors.class, String.class).toString() );
            assertEquals( "mosaics.lang.reflection.JavaConstructorTest$PojoWithConstructors(java.lang.String a, java.lang.Integer b)", getConstructor(PojoWithConstructors.class, String.class, Integer.class).toString() );

            assertEquals( "mosaics.lang.reflection.JavaConstructorTest$TypedPojoWithConstructors<java.lang.Boolean, java.lang.String>(java.lang.String a)", JavaClass.of(TypedPojoWithConstructors.class,Boolean.class,String.class).getConstructor(String.class).get().toString() );
        }
    }


    private static JavaConstructor getConstructor(Class<?> clazz, Class<?>...args) {
        return JavaClass.of(clazz).getConstructor(args).get();
    }

    public static class PojoWithConstructors {
        public PojoWithConstructors() {}
        public PojoWithConstructors(String a) {}
        public PojoWithConstructors(String a, Integer b) {}
    }

    public static class TypedPojoWithConstructors<A,B> {
        public TypedPojoWithConstructors(String a) {}
    }
}
