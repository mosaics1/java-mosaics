package mosaics.lang.reflection;

import mosaics.fp.FP;
import mosaics.fp.collections.FPIterable;
import mosaics.fp.collections.FPOption;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class JavaClassTest {
    @Nested
    public class GetAllMethodsTestCases {
        @Test
        public void withInheritanceAndInterface() {
            Set<String> actual   = JavaClass.of(C.class).getAllMethods().map(JavaMethod::getName).toSet();

            Set<String> expected = Set.of(
                "getB", "getI", "getA", "wait", "equals", "toString", "hashCode", "getClass",
                "notify", "notifyAll",
                "getProtected", "getPrivate", "clone", "finalize"
            );

            assertEquals(expected, actual);
        }
    }

    @Nested
    public class GetAllFieldsTestCases {
        @Test
        public void withInheritanceAndInterface() {
            Set<String> actual   = JavaClass.of(C.class).getAllFields().map(JavaField::getName).toSet();

            Set<String> expected = Set.of( "a", "b", "c" );

            assertEquals(expected, actual);
        }
    }





    @Nested
    public class UnparameterisedClassTests {

    }

    @Nested
    public class ParameterisedClassTests {
        @Nested
        public class ParameterisedFieldTests {
            @Test
            public void givenFieldWhoseTypeIsParameterised() {
                JavaClass       javaClass   = JavaClass.of(ClassParameterUsedInConstructorDeclaration.class);
                JavaField       javaField   = javaClass.getField("field").get();


                assertEquals( "field", javaField.getName() );
                assertEquals( "mosaics.lang.reflection.JavaClassTest$ClassParameterUsedInConstructor<java.lang.Integer>", javaField.getType().toString() );
            }

            @Test
            public void givenFieldWhoseTypeIsAGenericParameter() {
                JavaClass       topJavaClass   = JavaClass.of(ClassParameterUsedInConstructorDeclaration.class);
                JavaField       topJavaField   = topJavaClass.getField("field").get();
                JavaField       childJavaField = topJavaField.getType().getField("v").get();


                assertEquals( "v", childJavaField.getName() );
                assertEquals( "java.lang.Integer", childJavaField.getType().toString() );
            }

            @Test
            public void givenFieldWithNestedGenerics() {
                JavaClass javaClass = JavaClass.of(NestedField.class);
                JavaField javaField = javaClass.getField("field").get();


                assertEquals( "field", javaField.getName() );
                assertEquals( "java.util.Optional<java.util.List<java.lang.String>>", javaField.getType().toString() );
                assertEquals( "[java.util.List<java.lang.String>]", javaField.getType().getClassGenerics().toString() );
            }
        }

        @Nested
        public class ParameterisedConstructorTests {
            @Test
            public void givenConstructorWhoseConstructorHasAParameterWithASpecifiedTypeParameter() {
                JavaClass       javaClass   = JavaClass.of(ClassParameterUsedInConstructorDeclaration.class);
                JavaConstructor constructor = javaClass.getAllConstructors().firstOrNull();


                FPIterable<JavaParameter> expectedParameters = FP.wrapAll(
                    new JavaParameter(constructor, 0, JavaClass.of(ClassParameterUsedInConstructor.class, Integer.class), "v", List.of())
                );

                assertEquals( expectedParameters, constructor.getParameters() );
            }

            @Test
            public void givenConstructorWhoseConstructorHasAParameterIsASpecifiedTypeParameter() {
                JavaClass       topJavaClass     = JavaClass.of(ClassParameterUsedInConstructorDeclaration.class);
                JavaField       topJavaField     = topJavaClass.getField("field").get();
                JavaConstructor childConstructor = topJavaField.getType().getAllConstructors().firstOrNull();


                FPIterable<JavaParameter> expectedParameters = FP.wrapAll(
                    new JavaParameter(childConstructor, 0, JavaClass.of(Integer.class), "v", List.of())
                );

                assertEquals( expectedParameters, childConstructor.getParameters() );
            }
        }

        @Nested
        public class ParameterisedMethodTests {
            @Test
            public void givenMethodWhoseMethodHasAParameterWithASpecifiedTypeParameter() {
                JavaClass  javaClass = JavaClass.of(ClassParameterUsedInMethodDeclaration.class);
                JavaMethod method    = javaClass.getMethod("setValue", ClassParameterUsedInMethod.class).get();


                FPIterable<JavaParameter> expectedParameters = FP.wrapAll(
                    new JavaParameter(method, 0, JavaClass.of(ClassParameterUsedInMethod.class, Integer.class), "v", List.of())
                );

                assertEquals( expectedParameters, method.getParameters() );
            }

            @Test
            public void givenMethodWhoseMethodHasAParameterIsASpecifiedTypeParameter() {
                JavaClass  topJavaClass = JavaClass.of(ClassParameterUsedInMethodDeclaration.class);
                JavaField  topJavaField = topJavaClass.getField("field").get();
                JavaMethod childMethod  = topJavaField.getType().getMethod("setValue", Integer.class).get();


                FPIterable<JavaParameter> expectedParameters = FP.wrapAll(
                    new JavaParameter( childMethod, 0, JavaClass.of( Integer.class ), "v", List.of() )
                );
                expectedParameters.equals( childMethod.getParameters() );
                assertEquals( expectedParameters, childMethod.getParameters() );
            }
        }
    }


    @Nested
    public class GetParentClassTestCases {
        @Test
        public void givenObject_expectNoParent() {
            JavaClass           jc     = JavaClass.of(Object.class);
            FPOption<JavaClass> parent = jc.getParentClass();

            assertEquals( FP.emptyOption(), parent );
        }

        @Test
        public void givenNoExplicitParentClass_expectObject() {
            JavaClass           jc     = JavaClass.of(A.class);
            FPOption<JavaClass> parent = jc.getParentClass();

            assertEquals( FP.option(JavaClass.of(Object.class)), parent );
        }

        @Test
        public void givenExplicitParentClass_expectDeclaredClass() {
            JavaClass           jc     = JavaClass.of(B.class);
            FPOption<JavaClass> parent = jc.getParentClass();

            assertEquals( FP.option(JavaClass.of(A.class)), parent );
        }

        @Test
        public void givenParentClassWithGenerics_expectGenericsToBeVisible() {
            JavaClass           jc       = JavaClass.of(ChildOfParameterisedClass.class);
            FPOption<JavaClass> parent   = jc.getParentClass();
            FPOption<JavaClass> expected = FP.option( JavaClass.of(ParameterisedClass.class,String.class,Boolean.class) );

            assertEquals( expected, parent );
        }

        @Test
        public void givenClassWithGenerics_expectGenericsToBePastDown() {
            JavaClass           jc       = JavaClass.of(ParameterisedParentClass.class,Double.class);
            FPOption<JavaClass> parent   = jc.getParentClass();
            FPOption<JavaClass> expected = FP.option( JavaClass.of(ParameterisedClass.class,Double.class,Boolean.class) );

            assertEquals( expected, parent );
        }

        @Test
        public void givenClassWithGenerics_callGetParentClassWithValidTargetParent_expectGenericsToBePastDown() {
            JavaClass           jc       = JavaClass.of(ParameterisedParentClass.class,Double.class);
            FPOption<JavaClass> parent   = jc.getParentClass(ParameterisedClass.class);
            FPOption<JavaClass> expected = FP.option( JavaClass.of(ParameterisedClass.class,Double.class,Boolean.class) );

            assertEquals( expected, parent );
        }

        @Test
        public void givenClassWithGenerics_callGetParentClassWithTargetParentThatIsNotAParent_expectNone() {
            JavaClass           jc       = JavaClass.of(ParameterisedParentClass.class,Double.class);
            FPOption<JavaClass> parent   = jc.getParentClass(A.class);
            FPOption<JavaClass> expected = FP.emptyOption();

            assertEquals( expected, parent );
        }
    }


    /**
     * Hello World.<p>
     *
     * How are you today?
     *
     * @see java.lang.String
     */
    public static class PojoWithJavaDocs {

    }

    public static class ClassParameterUsedInConstructorDeclaration {
        private ClassParameterUsedInConstructor<Integer> field;

        public ClassParameterUsedInConstructorDeclaration(ClassParameterUsedInConstructor<Integer> v) {
            this.field = v;
        }
    }

    public static class ClassParameterUsedInMethodDeclaration {
        private ClassParameterUsedInMethod<Integer> field;

        public ClassParameterUsedInMethodDeclaration(ClassParameterUsedInMethod<Integer> v) {
            this.field = v;
        }

        public ClassParameterUsedInMethod<Integer> getValue() {
            return field;
        }

        public void setValue(ClassParameterUsedInMethod<Integer> v) {
            this.field = v;
        }
    }

    public static class ClassParameterUsedInConstructor<T> {
        private T v;

        public ClassParameterUsedInConstructor( T v ) {
            this.v = v;
        }
    }

    public static class ClassParameterUsedInMethod<T> {
        private T v;

        public T getValue() {
            return v;
        }

        public void setValue(T v) {
            this.v = v;
        }
    }

    public static class NestedField {
        private Optional<List<String>> field;
    }

    public static class A {
        private String a;

        public String getA() {
            return "A";
        }

        protected String getProtected() {
            return "protected";
        }

        protected String getPrivate() {
            return "protected";
        }
    }

    public static class B extends A {
        private String b;

        public String getB() {
            return "B";
        }
    }

    public static interface I {
        public String getI();
    }

    public static class C extends B implements I {
        public String c;

        public String getB() {
            return "B";
        }

        public String getI() {
            return "I";
        }
    }

    public static class ParameterisedClass<A,B> {

    }

    public static class ChildOfParameterisedClass extends ParameterisedClass<String,Boolean> {

    }

    public static class ParameterisedParentClass<A> extends ParameterisedClass<A,Boolean> {

    }
}
