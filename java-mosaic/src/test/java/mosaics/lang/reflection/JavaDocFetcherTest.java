package mosaics.lang.reflection;

import mosaics.fp.FP;
import mosaics.fp.collections.FPIterable;
import org.junit.jupiter.api.*;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class JavaDocFetcherTest {
    private JavaDocFetcher fetcher = new JavaDocFetcher();


    @Nested
    public class ClassLevelJavaDocTestCases {
        @Test
        public void givenClassWithNoDocs_expectEmpty() {
            assertEquals( FP.emptyOption(), fetcher.fetchJavaDocFor(JavaClass.of(PojoWithNoJavaDocs.class)) );
        }

        @Test
        public void givenClassWithDocs_expectDocs() {
            JavaDoc actual = fetcher.fetchJavaDocFor(JavaClass.of(PojoWithJavaDocsNoTags.class)).get();

            JavaDoc expected = new JavaDoc(Arrays.asList(
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                "",
                "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
                "",
                "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
            ));

            assertEquals(expected, actual);
        }

        @Test
        public void givenClassWithDocsThatHasTags_expectTagsToBeCaptured() {
            JavaDoc actual = fetcher.fetchJavaDocFor(JavaClass.of(PojoWithJavaDocsWithTags.class)).get();

            JavaDoc expected = new JavaDoc(Arrays.asList(
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt",
                "ut labore et dolore magna aliqua."),
                List.of(
                    new JavaDocTag("see", "mosaics.lang.reflection.JavaDocFetcherTest"),
                    new JavaDocTag("tag", List.of("" +
                            "Ut enim ad minim veniam, quis nostrud exercitation",
                        "ullamco laboris nisi ut aliquip ex ea commodo consequat."
                    ))
                )
            );

            assertEquals(expected, actual);
        }

        public class PojoWithNoJavaDocs {

        }

        /**
         * Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
         *
         * Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
         *
         * Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
         */
        public class PojoWithJavaDocsNoTags {

        }

        /**
         * Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
         * ut labore et dolore magna aliqua.
         *
         * @see mosaics.lang.reflection.JavaDocFetcherTest
         * @tag Ut enim ad minim veniam, quis nostrud exercitation
         *      ullamco laboris nisi ut aliquip ex ea commodo consequat.
         */
        public class PojoWithJavaDocsWithTags {

        }
    }


    @Nested
    public class ConstructorLevelJavaDocTestCases {
        @Test
        public void givenNoArgConstructorWithNoDocs_expectEmpty() {
            JavaClass       jc          = JavaClass.of( ConstructorWithNoJavaDocs.class );
            JavaConstructor constructor = jc.getConstructor().get();

            assertEquals( FP.emptyOption(), fetcher.fetchJavaDocFor(constructor) );
        }

        @Test
        public void givenNoArgConstructorWithDocs_expectDocs() {
            JavaClass       jc          = JavaClass.of( ConstructorWithJavaDocsNoTags.class );
            JavaConstructor constructor = jc.getConstructor().get();
            JavaDoc         actual      = fetcher.fetchJavaDocFor(constructor).get();

            JavaDoc expected = new JavaDoc(Arrays.asList(
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                "",
                "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
                "",
                "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
            ));

            assertEquals(expected, actual);
        }

        @Test
        public void givenTwoArgConstructorWithDocs_expectDocs() {
            JavaClass       jc          = JavaClass.of( ConstructorWithJavaDocsNoTags.class );
            JavaConstructor constructor = jc.getConstructor(String.class, Integer.class).get();
            JavaDoc         actual      = fetcher.fetchJavaDocFor( constructor ).get();

            JavaDoc expected = new JavaDoc(Arrays.asList(
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                "",
                "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
                "",
                "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
            ));

            assertEquals(expected, actual);
        }

        @Test
        public void givenConstructorWithDocsThatHasTags_expectTagsToBeCaptured() {
            JavaClass       jc          = JavaClass.of( ConstructorWithJavaDocsWithTags.class );
            JavaConstructor constructor = jc.getConstructor(String.class, Integer.class, Boolean.TYPE).get();
            JavaDoc         actual      = fetcher.fetchJavaDocFor(constructor).get();

            JavaDoc expected = new JavaDoc(Arrays.asList(
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt",
                "ut labore et dolore magna aliqua."),
                List.of(
                    new JavaDocTag("tag", List.of(
                        "Ut enim ad minim veniam, quis nostrud exercitation",
                        "ullamco laboris nisi ut aliquip ex ea commodo consequat."
                    )),
                    new JavaDocTag("deprecated", List.of("Cillum dolore eu fugiat nulla pariatur")),
                    new JavaDocTag("see", "mosaics.lang.reflection.JavaDocFetcherTest"),
                    new JavaDocTag("throws", "IllegalStateException",List.of("Ullamco laboris nisi ut aliquip")),
                    new JavaDocTag("throws", "mosaics.lang.reflection.ReflectionException",List.of("Voluptate velit esse cillum dolore eu fugiat"))
                )
            );

            assertEquals(expected, actual);
        }

        @Test
        public void givenConstructorParamsWithDocs_expectDocsToBeAvailable() {
            JavaClass                 jc          = JavaClass.of( ConstructorWithJavaDocsWithTags.class );
            JavaConstructor           constructor = jc.getConstructor(String.class, Integer.class, Boolean.TYPE).get();
            FPIterable<JavaParameter> parameters  = constructor.getParameters();
            FPIterable<JavaDoc>       actual      = parameters.flatMap(p -> fetcher.fetchJavaDocFor(p));

            FPIterable<JavaDoc> expected = FP.wrapAll(
                new JavaDoc("Lorem ipsum dolor sit amet"),
                new JavaDoc("Sed do eiusmod tempor incididunt"),
                new JavaDoc("In voluptate velit esse cillum dolore eu fugiat nulla pariatur,", "ouis aute irure dolor in reprehenderit")
            );

            assertEquals(expected, actual);
        }
    }


    @Nested
    public class MethodLevelJavaDocTestCases {
        @Test
        public void givenNoArgMethodWithNoDocs_expectEmpty() {
            JavaClass  jc     = JavaClass.of( MethodWithNoJavaDocs.class );
            JavaMethod method = jc.getMethod("m").get();

            assertEquals( FP.emptyOption(), fetcher.fetchJavaDocFor(method) );
        }

        @Test
        public void givenNoArgMethodWithDocs_expectDocs() {
            JavaClass  jc     = JavaClass.of( MethodWithJavaDocsNoTags.class );
            JavaMethod method = jc.getMethod("m").get();
            JavaDoc    actual = fetcher.fetchJavaDocFor(method).get();

            JavaDoc expected = new JavaDoc(Arrays.asList(
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                "",
                "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
                "",
                "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
            ));

            assertEquals(expected, actual);
        }

        @Test
        public void givenTwoArgMethodWithDocs_expectDocs() {
            JavaClass  jc     = JavaClass.of( MethodWithJavaDocsNoTags.class );
            JavaMethod method = jc.getMethod("m", String.class, Integer.class).get();
            JavaDoc    actual = fetcher.fetchJavaDocFor( method ).get();

            JavaDoc expected = new JavaDoc(Arrays.asList(
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                "",
                "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
                "",
                "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
            ));

            assertEquals(expected, actual);
        }

        @Test
        public void givenMethodWithDocsThatHasTags_expectTagsToBeCaptured() {
            JavaClass  jc     = JavaClass.of( MethodWithJavaDocsWithTags.class );
            JavaMethod method = jc.getMethod("m", String.class, Integer.class, Boolean.TYPE).get();
            JavaDoc    actual = fetcher.fetchJavaDocFor(method).get();

            JavaDoc expected = new JavaDoc(Arrays.asList(
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt",
                "ut labore et dolore magna aliqua."),
                List.of(
                    new JavaDocTag("tag", List.of(
                        "Ut enim ad minim veniam, quis nostrud exercitation",
                        "ullamco laboris nisi ut aliquip ex ea commodo consequat."
                    )),
                    new JavaDocTag("deprecated", List.of("Cillum dolore eu fugiat nulla pariatur")),
                    new JavaDocTag("see", "mosaics.lang.reflection.JavaDocFetcherTest"),
                    new JavaDocTag("throws", "IllegalStateException",List.of("Ullamco laboris nisi ut aliquip")),
                    new JavaDocTag("throws", "mosaics.lang.reflection.ReflectionException",List.of("Voluptate velit esse cillum dolore eu fugiat")),
                    new JavaDocTag("return", List.of("Eiusmod tempor"))
                )
            );

            assertEquals(expected, actual);
        }

        @Test
        public void givenMethodParamsWithDocs_expectDocsToBeAvailable() {
            JavaClass                 jc          = JavaClass.of( MethodWithJavaDocsWithTags.class );
            JavaMethod                method      = jc.getMethod("m", String.class, Integer.class, Boolean.TYPE).get();
            FPIterable<JavaParameter> parameters  = method.getParameters();
            FPIterable<JavaDoc>       actual      = parameters.flatMap(p -> fetcher.fetchJavaDocFor(p));

            FPIterable<JavaDoc> expected = FP.wrapAll(
                new JavaDoc("Lorem ipsum dolor sit amet"),
                new JavaDoc("Sed do eiusmod tempor incididunt"),
                new JavaDoc("In voluptate velit esse cillum dolore eu fugiat nulla pariatur,", "ouis aute irure dolor in reprehenderit")
            );

            assertEquals(expected, actual);
        }

        /**
         * Method docs do not appear unless these is a class level doc.
         *
         * I have raised https://github.com/dnault/therapi-runtime-javadoc/issues/41
         */
        public class MethodWithNoJavaDocs {
            public void m() {}
        }

        /**
         * Method docs do not appear unless these is a class level doc.
         *
         * I have raised https://github.com/dnault/therapi-runtime-javadoc/issues/41
         */
        public class MethodWithJavaDocsNoTags {
            /**
             * Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
             *
             * Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
             *
             * Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
             */
            public void m() {}

            /**
             * Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
             *
             * Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
             *
             * Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
             */
            public void m( String a, Integer b ) {}
        }

        /**
         * Method docs do not appear unless these is a class level doc.
         *
         * I have raised https://github.com/dnault/therapi-runtime-javadoc/issues/41
         */
        public class MethodWithJavaDocsWithTags {
            /**
             * Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
             * ut labore et dolore magna aliqua.
             *
             * @see        mosaics.lang.reflection.JavaDocFetcherTest
             * @tag        Ut enim ad minim veniam, quis nostrud exercitation
             *             ullamco laboris nisi ut aliquip ex ea commodo consequat.
             * @param abc  Lorem ipsum dolor sit amet
             * @param b    Sed do eiusmod tempor incididunt
             * @param c    In voluptate velit esse cillum dolore eu fugiat nulla pariatur,
             *             ouis aute irure dolor in reprehenderit
             *
             * @throws     IllegalStateException                       Ullamco laboris nisi ut aliquip
             * @exception  mosaics.lang.reflection.ReflectionException Voluptate velit esse cillum dolore eu fugiat
             *
             * @return     Eiusmod tempor
             * @deprecated Cillum dolore eu fugiat nulla pariatur
             */
            @Deprecated
            public String m( String abc, Integer b, boolean c ) {
                return null;
            }
        }
    }


    @Nested
    public class FieldLevelJavaDocTestCases {
        @Test
        public void givenNoArgFieldWithNoDocs_expectEmpty() {
            JavaClass  jc   = JavaClass.of( FieldLevelJavaDocTestCases.FieldWithNoJavaDocs.class );
            JavaField field = jc.getField("f").get();

            assertEquals( FP.emptyOption(), fetcher.fetchJavaDocFor(field) );
        }

        @Test
        public void givenNoArgFieldWithDocs_expectDocs() {
            JavaClass jc     = JavaClass.of( FieldLevelJavaDocTestCases.FieldWithJavaDocsNoTags.class );
            JavaField field = jc.getField("f").get();
            JavaDoc   actual = fetcher.fetchJavaDocFor(field).get();

            JavaDoc expected = new JavaDoc(Arrays.asList(
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                "",
                "Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
                "",
                "Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
            ));

            assertEquals(expected, actual);
        }

        @Test
        public void givenFieldWithDocsThatHasTags_expectTagsToBeCaptured() {
            JavaClass jc     = JavaClass.of( FieldLevelJavaDocTestCases.FieldWithJavaDocsWithTags.class );
            JavaField field = jc.getField("f").get();
            JavaDoc   actual = fetcher.fetchJavaDocFor(field).get();

            JavaDoc expected = new JavaDoc(Arrays.asList(
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt",
                "ut labore et dolore magna aliqua."),
                List.of(
                    new JavaDocTag("tag", List.of(
                        "Ut enim ad minim veniam, quis nostrud exercitation",
                        "ullamco laboris nisi ut aliquip ex ea commodo consequat."
                    )),
                    new JavaDocTag("deprecated", List.of("Cillum dolore eu fugiat nulla pariatur")),
                    new JavaDocTag("see", "mosaics.lang.reflection.JavaDocFetcherTest")
                )
            );

            assertEquals(expected, actual);
        }

        /**
         * Field docs do not appear unless these is a class level doc.
         *
         * I have raised https://github.com/dnault/therapi-runtime-javadoc/issues/41
         */
        public class FieldWithNoJavaDocs {
            public String f;
        }

        /**
         * Field docs do not appear unless these is a class level doc.
         *
         * I have raised https://github.com/dnault/therapi-runtime-javadoc/issues/41
         */
        public class FieldWithJavaDocsNoTags {
            /**
             * Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
             *
             * Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
             *
             * Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
             */
            public String f;
        }

        /**
         * Field docs do not appear unless these is a class level doc.
         *
         * I have raised https://github.com/dnault/therapi-runtime-javadoc/issues/41
         */
        public class FieldWithJavaDocsWithTags {
            /**
             * Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
             * ut labore et dolore magna aliqua.
             *
             * @see        mosaics.lang.reflection.JavaDocFetcherTest
             * @tag        Ut enim ad minim veniam, quis nostrud exercitation
             *             ullamco laboris nisi ut aliquip ex ea commodo consequat.
             *
             * @deprecated Cillum dolore eu fugiat nulla pariatur
             */
            @Deprecated
            public String f;
        }
    }


    /**
     * Constructor docs do not appear unless these is a class level doc.
     *
     * I have raised https://github.com/dnault/therapi-runtime-javadoc/issues/41
     */
    public static class ConstructorWithNoJavaDocs {
        public ConstructorWithNoJavaDocs() {}
    }

    /**
     * Constructor docs do not appear unless these is a class level doc.
     *
     * I have raised https://github.com/dnault/therapi-runtime-javadoc/issues/41
     */
    public static class ConstructorWithJavaDocsNoTags {
        /**
         * Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
         *
         * Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
         *
         * Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
         */
        public ConstructorWithJavaDocsNoTags() {}

        /**
         * Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
         *
         * Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
         *
         * Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
         */
        public ConstructorWithJavaDocsNoTags( String a, Integer b ) {}
    }

    /**
     * Constructor docs do not appear unless these is a class level doc.
     *
     * I have raised https://github.com/dnault/therapi-runtime-javadoc/issues/41
     */
    public static class ConstructorWithJavaDocsWithTags {
        /**
         * Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
         * ut labore et dolore magna aliqua.
         *
         * @see mosaics.lang.reflection.JavaDocFetcherTest
         * @tag Ut enim ad minim veniam, quis nostrud exercitation
         *      ullamco laboris nisi ut aliquip ex ea commodo consequat.
         * @param abc Lorem ipsum dolor sit amet
         * @param b   Sed do eiusmod tempor incididunt
         * @param c   In voluptate velit esse cillum dolore eu fugiat nulla pariatur,
         *            ouis aute irure dolor in reprehenderit
         *
         * @throws     IllegalStateException                       Ullamco laboris nisi ut aliquip
         * @exception  mosaics.lang.reflection.ReflectionException Voluptate velit esse cillum dolore eu fugiat
         *
         * @deprecated Cillum dolore eu fugiat nulla pariatur
         */
        @Deprecated
        public ConstructorWithJavaDocsWithTags( String abc, Integer b, boolean c ) {}
    }
}
