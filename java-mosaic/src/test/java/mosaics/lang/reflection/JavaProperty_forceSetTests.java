package mosaics.lang.reflection;

import lombok.Value;
import org.junit.jupiter.api.Test;

import static mosaics.lang.Backdoor.cast;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;


public class JavaProperty_forceSetTests {

    @Test
    public void givenSingleLevelReadOnlyLagomDTO_callSetValueOn_expectExceptionAsNoSetterMethodWasFound() {
        JavaClass                                        jc       = JavaClass.of( SingleLevelReadOnlyLagomDTO.class );
        JavaProperty<SingleLevelReadOnlyLagomDTO,String> property = jc.getPropertyMandatory( "name" );

        SingleLevelReadOnlyLagomDTO dto = jc.allocateInstance();

        try {
            property.setValueOn( dto, "ABC" );
            fail( "expected MissingSetterException" );
        } catch ( MissingSetterException ex ) {
            assertEquals( "No setter found for 'name' originating from mosaics.lang.reflection.JavaProperty_forceSetTests$SingleLevelReadOnlyLagomDTO", ex.getMessage() );
        }
    }

    @Test
    public void givenSingleLevelReadOnlyLagomDTO_forceSetProperty_expectValueToBeSet() {
        JavaClass                                        jc       = JavaClass.of( SingleLevelReadOnlyLagomDTO.class );
        JavaProperty<SingleLevelReadOnlyLagomDTO,String> property = cast(jc.getPropertyMandatory( "name" ).withDirectFieldAccessEnabled(true));

        SingleLevelReadOnlyLagomDTO dto = jc.allocateInstance();
        property.setValueOn( dto, "ABC" );

        assertEquals( new SingleLevelReadOnlyLagomDTO("ABC"), dto );
    }

    @Test
    public void givenNestedPojo_forceSetValueOnChild_expectValueToBeSet() {
        JavaClass                       jc       = JavaClass.of( NestedPojo.class );
        JavaProperty<NestedPojo,String> property = cast(jc.getPropertyMandatory( "child.name" ).withDirectFieldAccessEnabled(true));

        NestedPojo dto = jc.allocateInstance();
        property.setValueOn( dto, "ABC" );

        assertEquals( new NestedPojo(new SingleLevelReadOnlyLagomDTO("ABC")), dto );
    }


    @Value
    public static class SingleLevelReadOnlyLagomDTO {
        private String name;
    }

    @Value
    public static class NestedPojo {
        private SingleLevelReadOnlyLagomDTO child;
    }

}
