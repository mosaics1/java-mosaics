package mosaics.lang.reflection;

import mosaics.fp.FP;
import mosaics.utils.MapUtils;
import mosaics.utils.SetUtils;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class ReflectionUtilsTest {
    @Test
    public void testIterate() {
        assertEquals( Collections.emptyList(), mosaics.junit.lang.ReflectionUtils.iterate(null, v -> Optional.empty()).collect( Collectors.toList()));
        assertEquals(Collections.singletonList(1), mosaics.junit.lang.ReflectionUtils.iterate(1, v -> Optional.empty()).collect(Collectors.toList()));
        assertEquals( Arrays.asList(1,2), mosaics.junit.lang.ReflectionUtils.iterate(1, v -> v == 2 ? Optional.empty() : Optional.of(v+1)).collect(Collectors.toList()));
        assertEquals(Arrays.asList(1,2,3), mosaics.junit.lang.ReflectionUtils.iterate(1, v -> v == 3 ? Optional.empty() : Optional.of(v+1)).collect(Collectors.toList()));
    }

    @Test
    public void testIsCastableTo() {
        assertTrue( mosaics.junit.lang.ReflectionUtils.isCastableTo(String.class, String.class));
        assertTrue( mosaics.junit.lang.ReflectionUtils.isCastableTo(String.class, Object.class));
        assertTrue( mosaics.junit.lang.ReflectionUtils.isCastableTo(Object.class, Object.class));

        assertFalse( mosaics.junit.lang.ReflectionUtils.isCastableTo(Object.class, String.class));
    }

    @Test
    public void testToClass() {
        assertSame( String.class, mosaics.junit.lang.ReflectionUtils.toClass(String.class) );
        assertSame( Integer.class, mosaics.junit.lang.ReflectionUtils.toClass(Integer.class) );
        assertSame( Integer[].class, mosaics.junit.lang.ReflectionUtils.toClass(Integer[].class) );
    }

    @Test
    public void testGetCallersStackFrame() {
        // the test is one function call deep because reflection is used to find this method as the caller
        doTestGetCallersStackFrame();
    }

    @Nested
    public class IsZeroOrEmptyTestCases {
        @Test
        public void testBooleans() {
            assertTrue( ReflectionUtils.isZeroOrEmpty(null) );
            assertTrue( ReflectionUtils.isZeroOrEmpty(false) );

            assertFalse( ReflectionUtils.isZeroOrEmpty(true) );
        }

        @Test
        public void testBytes() {
            assertTrue( ReflectionUtils.isZeroOrEmpty(null) );
            assertTrue( ReflectionUtils.isZeroOrEmpty((byte) 0) );

            assertFalse( ReflectionUtils.isZeroOrEmpty((byte) -1) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(Byte.MIN_VALUE) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(Byte.MAX_VALUE) );
            assertFalse( ReflectionUtils.isZeroOrEmpty((byte) 1) );
        }

        @Test
        public void testShorts() {
            assertTrue( ReflectionUtils.isZeroOrEmpty(null) );
            assertTrue( ReflectionUtils.isZeroOrEmpty((short) 0) );

            assertFalse( ReflectionUtils.isZeroOrEmpty((short) -1) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(Short.MIN_VALUE) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(Short.MAX_VALUE) );
            assertFalse( ReflectionUtils.isZeroOrEmpty((short) 1) );
        }

        @Test
        public void testInts() {
            assertTrue( ReflectionUtils.isZeroOrEmpty(null) );
            assertTrue( ReflectionUtils.isZeroOrEmpty(0) );

            assertFalse( ReflectionUtils.isZeroOrEmpty(-1) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(Integer.MIN_VALUE) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(Integer.MAX_VALUE) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(1) );
        }

        @Test
        public void testLongs() {
            assertTrue( ReflectionUtils.isZeroOrEmpty(null) );
            assertTrue( ReflectionUtils.isZeroOrEmpty((long) 0) );

            assertFalse( ReflectionUtils.isZeroOrEmpty((long) -1) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(Long.MIN_VALUE) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(Long.MAX_VALUE) );
            assertFalse( ReflectionUtils.isZeroOrEmpty((long) 1) );
        }

        @Test
        public void testFloats() {
            assertTrue( ReflectionUtils.isZeroOrEmpty(null) );
            assertTrue( ReflectionUtils.isZeroOrEmpty((float) 0) );

            assertFalse( ReflectionUtils.isZeroOrEmpty((float) -1) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(Float.MIN_VALUE) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(Float.MAX_VALUE) );
            assertFalse( ReflectionUtils.isZeroOrEmpty((float) 1) );
        }

        @Test
        public void testDoubles() {
            assertTrue( ReflectionUtils.isZeroOrEmpty(null) );
            assertTrue( ReflectionUtils.isZeroOrEmpty((double) 0) );

            assertFalse( ReflectionUtils.isZeroOrEmpty((double) -1) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(Double.MIN_VALUE) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(Double.MAX_VALUE) );
            assertFalse( ReflectionUtils.isZeroOrEmpty((double) 1) );
        }

        @Test
        public void testChars() {
            assertTrue( ReflectionUtils.isZeroOrEmpty(null) );
            assertTrue( ReflectionUtils.isZeroOrEmpty((char) 0) );
            assertTrue( ReflectionUtils.isZeroOrEmpty(Character.MIN_VALUE) );

            assertFalse( ReflectionUtils.isZeroOrEmpty((char) -1) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(Character.MAX_VALUE) );
            assertFalse( ReflectionUtils.isZeroOrEmpty((char) 1) );
        }

        @Test
        public void testStrings() {
            assertTrue( ReflectionUtils.isZeroOrEmpty(null) );
            assertTrue( ReflectionUtils.isZeroOrEmpty("") );
            assertTrue( ReflectionUtils.isZeroOrEmpty("   ") );
            assertTrue( ReflectionUtils.isZeroOrEmpty("   \t\t  ") );
            assertTrue( ReflectionUtils.isZeroOrEmpty("\t") );
            assertTrue( ReflectionUtils.isZeroOrEmpty("\n") );
            assertTrue( ReflectionUtils.isZeroOrEmpty("\r") );

            assertFalse( ReflectionUtils.isZeroOrEmpty("abc") );
            assertFalse( ReflectionUtils.isZeroOrEmpty("£") );
            assertFalse( ReflectionUtils.isZeroOrEmpty(" s") );
            assertFalse( ReflectionUtils.isZeroOrEmpty("s ") );
        }

        @Test
        public void testObjectArray() {
            assertTrue( ReflectionUtils.isZeroOrEmpty(null) );
            assertTrue( ReflectionUtils.isZeroOrEmpty(new Object[] {}) );

            assertFalse( ReflectionUtils.isZeroOrEmpty(new Object[] {null}) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(new Object[] {"foo"}) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(new Object[] {"i"}) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(new Object[] {10}) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(new Object[] {new SuperCar()}) );
        }

        @Test
        public void testByteArray() {
            assertTrue( ReflectionUtils.isZeroOrEmpty(null) );
            assertTrue( ReflectionUtils.isZeroOrEmpty(new byte[] {}) );

            assertFalse( ReflectionUtils.isZeroOrEmpty(new byte[] {Byte.MIN_VALUE}) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(new byte[] {(byte) 0}) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(new byte[] {(byte) -1}) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(new byte[] {(byte) 1}) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(new byte[] {(byte) 1, (byte) 2}) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(new byte[] {Byte.MAX_VALUE}) );
        }

        @Test
        public void testShortArray() {
            assertTrue( ReflectionUtils.isZeroOrEmpty(null) );
            assertTrue( ReflectionUtils.isZeroOrEmpty(new short[] {}) );

            assertFalse( ReflectionUtils.isZeroOrEmpty(new short[] {Short.MIN_VALUE}) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(new short[] {(short) 0}) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(new short[] {(short) -1}) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(new short[] {(short) 1}) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(new short[] {(short) 1, (short) 2}) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(new short[] {Short.MAX_VALUE}) );
        }

        @Test
        public void testIntArray() {
            assertTrue( ReflectionUtils.isZeroOrEmpty(null) );
            assertTrue( ReflectionUtils.isZeroOrEmpty(new int[] {}) );

            assertFalse( ReflectionUtils.isZeroOrEmpty(new int[] {Integer.MIN_VALUE}) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(new int[] {0}) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(new int[] {-1}) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(new int[] {1}) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(new int[] {1, 2}) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(new int[] {Integer.MAX_VALUE}) );
        }

        @Test
        public void testLongArray() {
            assertTrue( ReflectionUtils.isZeroOrEmpty(null) );
            assertTrue( ReflectionUtils.isZeroOrEmpty(new long[] {}) );

            assertFalse( ReflectionUtils.isZeroOrEmpty(new long[] {Long.MIN_VALUE}) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(new long[] {(long) 0}) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(new long[] {(long) -1}) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(new long[] {(long) 1}) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(new long[] {(long) 1, (long) 2}) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(new long[] {Long.MAX_VALUE}) );
        }

        @Test
        public void testFloatArray() {
            assertTrue( ReflectionUtils.isZeroOrEmpty(null) );
            assertTrue( ReflectionUtils.isZeroOrEmpty(new float[] {}) );

            assertFalse( ReflectionUtils.isZeroOrEmpty(new float[] {Float.MIN_VALUE}) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(new float[] {(float) 0}) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(new float[] {(float) -1}) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(new float[] {(float) 1}) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(new float[] {(float) 1, (float) 2}) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(new float[] {Float.MAX_VALUE}) );
        }

        @Test
        public void testDoubleArray() {
            assertTrue( ReflectionUtils.isZeroOrEmpty(null) );
            assertTrue( ReflectionUtils.isZeroOrEmpty(new double[] {}) );

            assertFalse( ReflectionUtils.isZeroOrEmpty(new double[] {Double.MIN_VALUE}) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(new double[] {(double) 0}) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(new double[] {(double) -1}) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(new double[] {(double) 1}) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(new double[] {(double) 1, (double) 2}) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(new double[] {Double.MAX_VALUE}) );
        }

        @Test
        public void testCharArray() {
            assertTrue( ReflectionUtils.isZeroOrEmpty(null) );
            assertTrue( ReflectionUtils.isZeroOrEmpty(new char[] {}) );

            assertFalse( ReflectionUtils.isZeroOrEmpty(new char[] {Character.MIN_VALUE}) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(new char[] {'a'}) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(new char[] {'£'}) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(new char[] {'a', '£'}) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(new char[] {Character.MAX_VALUE}) );
        }

        @Test
        public void testMaps() {
            assertTrue( ReflectionUtils.isZeroOrEmpty(null) );
            assertTrue( ReflectionUtils.isZeroOrEmpty(MapUtils.asMap()) );
            assertTrue( ReflectionUtils.isZeroOrEmpty(Collections.emptyMap()) );

            assertFalse( ReflectionUtils.isZeroOrEmpty(MapUtils.asMap("a","b" )) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(MapUtils.asMap("a",null )) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(MapUtils.asMap("a","" )) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(MapUtils.asMap("a"," " )) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(MapUtils.asMap("a","b","c","d" )) );
        }

        @Test
        public void testLists() {
            assertTrue( ReflectionUtils.isZeroOrEmpty(null) );
            assertTrue( ReflectionUtils.isZeroOrEmpty(Arrays.asList()) );
            assertTrue( ReflectionUtils.isZeroOrEmpty(Collections.emptyList()) );

            assertFalse( ReflectionUtils.isZeroOrEmpty(Arrays.asList("a","b" )) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(Arrays.asList("a",null )) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(Arrays.asList("" )) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(Arrays.asList(" " )) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(Arrays.asList("a","b","c","d" )) );
        }

        @Test
        public void testSets() {
            assertTrue( ReflectionUtils.isZeroOrEmpty(null) );
            assertTrue( ReflectionUtils.isZeroOrEmpty(new HashSet()) );
            assertTrue( ReflectionUtils.isZeroOrEmpty(Collections.emptySet()) );

            assertFalse( ReflectionUtils.isZeroOrEmpty( SetUtils.asSet( "a", "b" )) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(Arrays.asList("a",null )) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(Arrays.asList("" )) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(Arrays.asList(" " )) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(Arrays.asList("a","b","c","d" )) );
        }

        @Test
        public void testIterators() {
            assertTrue( ReflectionUtils.isZeroOrEmpty(null) );
            assertTrue( ReflectionUtils.isZeroOrEmpty(new HashSet().iterator()) );
            assertTrue( ReflectionUtils.isZeroOrEmpty(Collections.emptySet().iterator()) );

            assertFalse( ReflectionUtils.isZeroOrEmpty(SetUtils.asSet( "a", "b" ).iterator()) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(Arrays.asList("a",null).iterator()) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(Arrays.asList("" ).iterator()) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(Arrays.asList(" " ).iterator()) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(Arrays.asList("a","b","c","d" ).iterator()) );
        }

        @Test
        public void testOptions() {
            assertTrue( ReflectionUtils.isZeroOrEmpty(null) );
            assertTrue( ReflectionUtils.isZeroOrEmpty(Optional.empty()) );

            assertFalse( ReflectionUtils.isZeroOrEmpty(Optional.of("  ")) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(Optional.of("abc")) );
        }

        @Test
        public void testFPOptions() {
            assertTrue( ReflectionUtils.isZeroOrEmpty(null) );
            assertTrue( ReflectionUtils.isZeroOrEmpty(FP.emptyOption()) );

            assertFalse( ReflectionUtils.isZeroOrEmpty(FP.option("  ")) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(FP.option("abc")) );
        }

        @Test
        public void testFPMaps() {
            assertTrue( ReflectionUtils.isZeroOrEmpty(null) );
            assertTrue( ReflectionUtils.isZeroOrEmpty(FP.emptyMap()) );

            assertFalse( ReflectionUtils.isZeroOrEmpty(FP.toMap("a","b" )) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(FP.toMap("a",null )) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(FP.toMap("a","" )) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(FP.toMap("a"," " )) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(FP.toMap("a","b","c","d" )) );
        }

        @Test
        public void testFPLists() {
            assertTrue( ReflectionUtils.isZeroOrEmpty(null) );
            assertTrue( ReflectionUtils.isZeroOrEmpty(FP.consList()) );

            assertFalse( ReflectionUtils.isZeroOrEmpty(FP.consList("a","b" )) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(FP.consList("a",null )) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(FP.consList("" )) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(FP.consList(" " )) );
            assertFalse( ReflectionUtils.isZeroOrEmpty(FP.consList("a","b","c","d" )) );
        }
    }
    
    @Nested
    public class HasValueTestCases {
        // hasValue

        @Test
        public void givenBooleans_callIsTrue() {
            assertFalse( ReflectionUtils.hasValue(false) );
            assertTrue( ReflectionUtils.hasValue(true) );
        }

        @Test
        public void givenStrings_callIsTrue() {
            assertFalse( ReflectionUtils.hasValue(null) );
            assertFalse( ReflectionUtils.hasValue("") );
            assertFalse( ReflectionUtils.hasValue("  ") );
            assertFalse( ReflectionUtils.hasValue("  \t\t ") );

            assertTrue( ReflectionUtils.hasValue("f") );
            assertTrue( ReflectionUtils.hasValue("false") );
            assertTrue( ReflectionUtils.hasValue("no") );
            assertTrue( ReflectionUtils.hasValue("foobar") );
            assertTrue( ReflectionUtils.hasValue("0") );

            assertTrue( ReflectionUtils.hasValue("t") );
            assertTrue( ReflectionUtils.hasValue("true") );
            assertTrue( ReflectionUtils.hasValue("T") );
            assertTrue( ReflectionUtils.hasValue("TRUE") );
            assertTrue( ReflectionUtils.hasValue("yes") );
            assertTrue( ReflectionUtils.hasValue("y") );
            assertTrue( ReflectionUtils.hasValue("1") );
        }

        @Test
        public void givenOptionals_callIsTrue() {
            assertFalse( ReflectionUtils.hasValue(Optional.empty()) );

            assertTrue( ReflectionUtils.hasValue(Optional.of("")) );
            assertTrue( ReflectionUtils.hasValue(Optional.of("  ")) );
            assertTrue( ReflectionUtils.hasValue(Optional.of("yes")) );
            assertTrue( ReflectionUtils.hasValue(Optional.of("no")) );
            assertTrue( ReflectionUtils.hasValue(Optional.of(Collections.emptyList())) );
        }

        @Test
        public void givenFPOptions_callIsTrue() {
            assertFalse( ReflectionUtils.hasValue(FP.emptyOption()) );
            assertFalse( ReflectionUtils.hasValue(FP.option(null)) );

            assertTrue( ReflectionUtils.hasValue(FP.option("")) );
            assertTrue( ReflectionUtils.hasValue(FP.option("  ")) );
            assertTrue( ReflectionUtils.hasValue(FP.option("yes")) );
            assertTrue( ReflectionUtils.hasValue(FP.option("no")) );
            assertTrue( ReflectionUtils.hasValue(FP.option(Collections.emptyList())));
        }

        @Test
        public void givenIterables_callIsTrue() {
            assertFalse( ReflectionUtils.hasValue(Collections.emptyList()) );
            assertFalse( ReflectionUtils.hasValue(Collections.emptySet()) );

            assertTrue( ReflectionUtils.hasValue(Collections.singletonList("foobar")) );
            assertTrue( ReflectionUtils.hasValue(Collections.singletonList("")) );
            assertTrue( ReflectionUtils.hasValue(Collections.singletonList("  ")) );
            assertTrue( ReflectionUtils.hasValue(Collections.singletonList(null)) );
            assertTrue( ReflectionUtils.hasValue(Collections.singleton("foobar")) );
            assertTrue( ReflectionUtils.hasValue(Collections.singleton(null)) );
        }

        @Test
        public void givenRRBVector_callIsTrue() {
            assertFalse( ReflectionUtils.hasValue(FP.emptyVector()) );

            assertTrue( ReflectionUtils.hasValue(FP.toVector("abc") ) );
            assertTrue( ReflectionUtils.hasValue(FP.toVector("1") ) );
            assertTrue( ReflectionUtils.hasValue(FP.toVector(false) ) );
            assertTrue( ReflectionUtils.hasValue(FP.toVector(true) ) );
        }

        @Test
        public void givenMaps_callIsTrue() {
            assertFalse( ReflectionUtils.hasValue(MapUtils.asMap()) );

            assertTrue( ReflectionUtils.hasValue(MapUtils.asMap("a","b")) );
            assertTrue( ReflectionUtils.hasValue(MapUtils.asMap("a",null)) );
            assertTrue( ReflectionUtils.hasValue(MapUtils.asMap("a",true)) );
            assertTrue( ReflectionUtils.hasValue(MapUtils.asMap("a",false)) );
        }

        @Test
        public void givenPOJOs_callIsTrue() {
            assertFalse( ReflectionUtils.hasValue(null) );

            assertTrue( ReflectionUtils.hasValue(new Car("Tesla")) );
            assertTrue( ReflectionUtils.hasValue(new Car(null)) );
        }
    }

    private void doTestGetCallersStackFrame() {
        StackWalker.StackFrame frame = ReflectionUtils.getCallersStackFrame();

        assertEquals( ReflectionUtilsTest.class.getName(), frame.getClassName() );
        assertEquals( "testGetCallersStackFrame", frame.getMethodName() );
    }
}

class Car implements Cloneable {
    public String make;

    public Car( String make ) {
        this.make = make;
    }

    public boolean equals( Object o ) {
        if ( !(o instanceof Car) ) {
            return false;
        }

        Car other = (Car) o;
        return Objects.equals(this.make, other.make);
    }

    public int hashCode() {
        return Objects.hashCode(make.hashCode());
    }
}

class SuperCar extends Car {
    public int age;
    public int numWheels;

    public SuperCar() {
        super("super");
    }
}
