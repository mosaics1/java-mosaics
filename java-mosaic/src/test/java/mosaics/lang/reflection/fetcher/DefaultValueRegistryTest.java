package mosaics.lang.reflection.fetcher;

import mosaics.fp.FP;
import mosaics.fp.collections.FPList;
import mosaics.fp.collections.maps.FPMap;
import mosaics.fp.collections.FPOption;
import mosaics.fp.collections.FPSet;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class DefaultValueRegistryTest {
    @Test
    public void getDefaultValues() {
        assertEquals( Collections.emptyList(), DefaultValueRegistry.getDefaultValueFactoryFor(List.class).get().invoke() );
        assertEquals( Collections.emptySet(), DefaultValueRegistry.getDefaultValueFactoryFor(Set.class).get().invoke() );
        assertEquals( Collections.emptyMap(), DefaultValueRegistry.getDefaultValueFactoryFor(Map.class).get().invoke() );
        assertEquals( Optional.empty(), DefaultValueRegistry.getDefaultValueFactoryFor(Optional.class).get().invoke() );

        assertEquals( FP.emptyOption(), DefaultValueRegistry.getDefaultValueFactoryFor(FPOption.class).get().invoke() );
        assertEquals( FPList.newArrayList(), DefaultValueRegistry.getDefaultValueFactoryFor(FPList.class).get().invoke() );
        assertEquals( FP.toSet(), DefaultValueRegistry.getDefaultValueFactoryFor(FPSet.class).get().invoke() );
        assertEquals( FP.toMap(), DefaultValueRegistry.getDefaultValueFactoryFor(FPMap.class).get().invoke() );
    }
}
