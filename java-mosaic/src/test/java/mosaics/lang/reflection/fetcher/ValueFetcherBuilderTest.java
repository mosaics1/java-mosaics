//package mosaics.lang.reflection_old.fetcher;
//
//
//import com.softwaremosaic.fp.FP;
//import com.softwaremosaic.fp.collections.FPOption;
//import mosaics.lang.reflection_old.JavaClass;
//import mosaics.lang.reflection_old.MissingPropertyException;
//import com.softwaremosaic.utils.MapUtils;
//import lombok.Value;
//import mosaics.lang.reflection_old.fetcher.ValueFetcher;
//import org.junit.jupiter.api.Test;
//
//import java.util.Collections;
//import java.util.Map;
//
//import static mosaics.lang.reflection_old.converters.I18nContext.LONDON;
//import static mosaics.lang.reflection_old.fetcher.ValueFetcherBuilder.newValueFetcherBuilder;
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.jupiter.api.Assertions.fail;
//
//
//public class ValueFetcherBuilderTest {
//
//// BOOLEAN
//
//    @Test
//    public void givenDirectBooleanLookup_miss_expectNone() {
//        ValueFetcher<Object> fetcher = newValueFetcherBuilder("flag").build();
//        Map<String,Object>   values  = Collections.emptyMap();
//
//        assertEquals( FP.emptyOption(), fetcher.fetchFrom(LONDON, values::get) );
//    }
//
//    @Test
//    public void givenDirectBooleanLookup_hitFalse_expectSomeFalse() {
//        ValueFetcher<Object> fetcher = newValueFetcherBuilder("flag").build();
//        Map<String,Object>   values  = MapUtils.asMap( "flag", false );
//
//        assertEquals( FP.option(false), fetcher.fetchFrom(LONDON, values::get) );
//    }
//
//    @Test
//    public void givenDirectBooleanLookup_hitTrue_expectSomeTrue() {
//        ValueFetcher<Object> fetcher = newValueFetcherBuilder("flag").build();
//        Map<String,Object>   values  = MapUtils.asMap( "flag", true );
//
//        assertEquals( FP.option(true), fetcher.fetchFrom(LONDON, values::get) );
//    }
//
//    @Test
//    public void givenDirectBooleanLookup_markItAsBooleanHitTrue_expectSomeTrue() {
//        ValueFetcher<Boolean> fetcher = newValueFetcherBuilder("flag")
//            .asType(Boolean.class)
//            .build();
//
//        Map<String,Object> values = MapUtils.asMap( "flag", true );
//
//        FPOption<Boolean> actual = fetcher.fetchFrom( LONDON, values::get );
//        assertEquals( FP.option(true), actual );
//    }
//
//    @Test
//    public void givenDirectBooleanLookup_markItAsBooleanHitString_expectFalse() {
//        ValueFetcher<Boolean> fetcher = newValueFetcherBuilder("flag")
//            .asType(Boolean.class)
//            .build();
//
//        Map<String,Object> values = MapUtils.asMap( "flag", "foobar" );
//
//        FPOption<Boolean> actual = fetcher.fetchFrom( LONDON, values::get );
//        assertEquals( FP.option(false), actual );
//    }
//
//    @Test
//    public void givenDirectBooleanLookup_markItAsBooleanMatchStringTrue_expectStringToBeConvertedToBoolean() {
//        ValueFetcher<Boolean> fetcher = newValueFetcherBuilder("flag")
//            .asType(Boolean.class)
//            .build();
//
//        Map<String,Object> values = MapUtils.asMap( "flag", "true" );
//
//        assertEquals( FP.option(true), fetcher.fetchFrom(LONDON, values::get) );
//    }
//
//
//// property step (untyped)
//
//    @Test
//    public void givenUntyped_walkPropertyThatDoesNotExist_expectExceptionAsFetcherIsRun() {
//        ValueFetcher<Object> fetcher = newValueFetcherBuilder("book1")
//            .fetchProperty("missingProp")
//            .build();
//
//        Map<String,Object> values = MapUtils.asMap( "book1", new Book("Age of Empires") );
//
//        try {
//            fetcher.fetchFrom( LONDON, values::get );
//
//            fail( "expected MissingPropertyException" );
//        } catch ( MissingPropertyException e ) {
//            assertEquals( Book.class.getName()+" is missing property 'missingProp'", e.getMessage() );
//        }
//    }
//
//    @Test
//    public void givenUntyped_walkPropertyThatMatchesExactly_expectValue() {
//        ValueFetcher<Object> fetcher = newValueFetcherBuilder("book1")
//            .fetchProperty("getName")
//            .build();
//
//        Map<String,Object> values = MapUtils.asMap( "book1", new Book("Age of Empires") );
//
//
//        assertEquals( FP.option("Age of Empires"), fetcher.fetchFrom(LONDON, values::get) );
//    }
//
//    @Test
//    public void givenUntyped_walkPropertyWithoutGetPrefix_expectValue() {
//        ValueFetcher<Object> fetcher = newValueFetcherBuilder("book1")
//            .fetchProperty("name")
//            .build();
//
//        Map<String,Object> values = MapUtils.asMap( "book1", new Book("Age of Empires") );
//
//
//        assertEquals( FP.option("Age of Empires"), fetcher.fetchFrom(LONDON, values::get) );
//    }
//
//    @Test
//    public void givenUntyped_walkPropertyWithoutIsPrefix_expectValue() {
//        ValueFetcher<Object> fetcher = newValueFetcherBuilder("book1")
//            .fetchProperty("longName")
//            .build();
//
//        Map<String,Object> values = MapUtils.asMap( "book1", new Book("Age of Empires") );
//
//
//        assertEquals( FP.option(true), fetcher.fetchFrom(LONDON, values::get) );
//    }
//
//
//    @Test
//    public void givenUntyped_walkPropertyWithoutHasPrefix_expectValue() {
//        ValueFetcher<Object> fetcher = newValueFetcherBuilder("book1")
//            .fetchProperty("oldName")
//            .build();
//
//        Map<String,Object> values = MapUtils.asMap( "book1", new Book("Age of Empires") );
//
//
//        assertEquals( FP.option(true), fetcher.fetchFrom(LONDON, values::get) );
//    }
//
//
//// property step (typed)
//
//    @Test
//    public void givenTyped_walkPropertyThatDoesNotExist_expectExceptionWhileBuildingTheFetcher() {
//        try {
//            newValueFetcherBuilder( "flag" )
//                .asType( Book.class )
//                .fetchProperty( "missingProperty" );
//
//            fail( "expected MissingPropertyException" );
//        } catch ( MissingPropertyException e ) {
//            assertEquals( Book.class.getName()+" is missing property 'missingProperty'", e.getMessage() );
//        }
//    }
//
//    @Test
//    public void givenTyped_walkPropertyThatMatchesExactly_expectValue() {
//        ValueFetcher<Object> fetcher = newValueFetcherBuilder("book1")
//            .asType( Book.class )
//            .fetchProperty("getName")
//            .build();
//
//        Map<String,Object> values = MapUtils.asMap( "book1", new Book("Age of Empires") );
//
//
//        assertEquals( FP.option("Age of Empires"), fetcher.fetchFrom(LONDON, values::get) );
//    }
//
//    @Test
//    public void givenTyped_walkPropertyWithoutGetPrefix_expectValue() {
//        ValueFetcher<Object> fetcher = newValueFetcherBuilder("book1")
//            .asType( Book.class )
//            .fetchProperty("name")
//            .build();
//
//        Map<String,Object> values = MapUtils.asMap( "book1", new Book("Age of Empires") );
//
//
//        assertEquals( FP.option("Age of Empires"), fetcher.fetchFrom(LONDON, values::get) );
//    }
//
//    @Test
//    public void givenTyped_walkPropertyWithoutIsPrefix_expectValue() {
//        ValueFetcher<Object> fetcher = newValueFetcherBuilder("book1")
//            .asType( Book.class )
//            .fetchProperty("longName")
//            .build();
//
//        Map<String,Object> values = MapUtils.asMap( "book1", new Book("Age of Empires") );
//
//
//        assertEquals( FP.option(true), fetcher.fetchFrom(LONDON, values::get) );
//    }
//
//    @Test
//    public void givenTyped_walkPropertyWithoutHasPrefix_expectValue() {
//        ValueFetcher<Object> fetcher = newValueFetcherBuilder("book1")
//            .asType( Book.class )
//            .fetchProperty("oldName")
//            .build();
//
//        Map<String,Object> values = MapUtils.asMap( "book1", new Book("Age of Empires") );
//
//
//        assertEquals( FP.option(true), fetcher.fetchFrom(LONDON, values::get) );
//    }
//
//
//// withDefaultValue
//
//    @Test
//    public void givenParameterNotFound_expectDefaultValue() {
//        ValueFetcher<Object> fetcher = newValueFetcherBuilder("missingBook")
//            .withDefaultValue( "Sneaky" )
//            .build();
//
//        Map<String,Object> values = MapUtils.asMap( "book1", new Book("Age of Empires") );
//
//
//        assertEquals( FP.option("Sneaky"), fetcher.fetchFrom(LONDON, values::get) );
//    }
//
//    @Test
//    public void givenParameterFound_expectParameterValue() {
//        ValueFetcher<Object> fetcher = newValueFetcherBuilder("book1")
//            .withDefaultValue( "Sneaky" )
//            .build();
//
//        Map<String,Object> values = MapUtils.asMap( "book1", new Book("Age of Empires") );
//
//
//        assertEquals( FP.option(new Book("Age of Empires")), fetcher.fetchFrom(LONDON, values::get) );
//    }
//
//    @Test
//    @SuppressWarnings("unchecked")
//    public void givenTypedParameterAndDefaultStringValue_expectDefaultValueToBeConverted() {
//        ValueFetcher<Object> fetcher = newValueFetcherBuilder("missingFlag")
//            .asType( (JavaClass) JavaClass.BOOLEAN_OBJECT )
//            .withDefaultValue( "true" )
//            .build();
//
//        Map<String,Object> values = MapUtils.asMap( "book1", new Book("Age of Empires") );
//
//
//        assertEquals( FP.option(true), fetcher.fetchFrom(LONDON, values::get) );
//    }
//
//
//    @Value
//    public static class Book {
//        private String name;
//
//        public boolean isLongName() {
//            return true;
//        }
//
//        public boolean hasOldName() {
//            return true;
//        }
//    }
//}
