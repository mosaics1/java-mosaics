package mosaics.lang.reflection;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.Value;
import lombok.With;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.io.File;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


public class DTOBuilderTest {

    @Nested
    public class GetterSetterDTOTestCases {
        private final DTOBuilder<GetterSetterDTO> dtoBuilder = DTOBuilder.createDTOBuilderFor( GetterSetterDTO.class );

        @Test
        public void givenNoProperties_expectEmptyDTO() {
            GetterSetterDTO expected = new GetterSetterDTO();

            assertEquals( expected, dtoBuilder.build() );
        }

        @Test
        public void givenNoZeroArgConstruct_expectException() {
            IllegalArgumentException ex = assertThrows( IllegalArgumentException.class, () -> DTOBuilder.createDTOBuilderFor(GetterSetterWithNoZeroArgConstructorDTO.class) );

            assertEquals( "mosaics.lang.reflection.DTOBuilderTest$GetterSetterWithNoZeroArgConstructorDTO has no public zero arg constructor", ex.getMessage() );
        }

        @Test
        public void givenValidProperty_expectDTOWithPropertySet() {
            GetterSetterDTO expected = new GetterSetterDTO();
            expected.setName("Jim");

            dtoBuilder.withProperty( "name","Jim" );

            assertEquals( expected, dtoBuilder.build() );
        }

        @Test
        public void givenPropertyThatDoesNotExist_expectException() {
            GetterSetterDTO expected = new GetterSetterDTO();
            expected.setName("Jim");

            IllegalArgumentException ex = assertThrows(
                IllegalArgumentException.class,
                () -> dtoBuilder.withProperty("missing","Jim")
            );

            assertEquals( "mosaics.lang.reflection.DTOBuilderTest$GetterSetterDTO does not have a property called 'missing'", ex.getMessage() );
        }

        @Test
        public void givenPropertyValueOfWrongTypeThatIsConvertible_expectConversion() {
            GetterSetterDTO expected = new GetterSetterDTO();
            expected.setAge(10);

            dtoBuilder.withProperty( "age","10" );

            assertEquals( expected, dtoBuilder.build() );
        }

        @Test
        public void givenPropertyValueThatIsNotConvertible_expectException() {
            GetterSetterDTO expected = new GetterSetterDTO();
            expected.setAge(10);

            IllegalArgumentException ex = assertThrows( IllegalArgumentException.class, () -> dtoBuilder.withProperty("age","abc") );
            assertEquals(
                "Unable to convert 'abc' of type 'java.lang.String' to property 'age' of type 'int'",
                ex.getMessage()
            );
        }

        @Test
        public void givenMultipleProperties_expectAllPropertiesToBeSet() {
            GetterSetterDTO expected = new GetterSetterDTO();
            expected.setName("Sarah");
            expected.setAge(42);

            dtoBuilder
                .withProperty( "age","42" )
                .withProperty( "name", "Sarah" );

            assertEquals( expected, dtoBuilder.build() );
        }

        @Test
        public void givenNestedSetter_expectNestedDTOToBeCreated() {
            GetterSetterDTO expected = new GetterSetterDTO();
            GetterSetterDTO child    = new GetterSetterDTO();

            child.setName("Sarah");
            expected.setChild( child );

            dtoBuilder
                .withProperty( "child.name", "Sarah" );

            assertEquals( expected, dtoBuilder.build() );
        }

        @Test
        public void givenNestedSetterWhereNonLeafPartOfPathDoesNotExist_expectException() {
            IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> dtoBuilder.withProperty("missing.name","Sarah"));
            assertEquals( "mosaics.lang.reflection.DTOBuilderTest$GetterSetterDTO does not have a property called 'missing.name'", ex.getMessage() );
        }

        @Test
        public void givenNestedSetterWhereLeafPartOfPathDoesNotExist_expectException() {
            IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> dtoBuilder.withProperty("child.missing","Sarah"));
            assertEquals( "mosaics.lang.reflection.DTOBuilderTest$GetterSetterDTO does not have a property called 'child.missing'", ex.getMessage() );
        }

        @Test
        public void givenNestedSetterWhereNonLeafPartWasPreviouslyDeclaredAsAPrimitiveValue_expectException() {
            dtoBuilder.withProperty( "name", "Sarah" );

            IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> dtoBuilder.withProperty("name.foo","Sarah"));
            assertEquals( "mosaics.lang.reflection.DTOBuilderTest$GetterSetterDTO does not have a property called 'name.foo'", ex.getMessage() );
        }

        @Test
        public void givenMultipleNestedSetter_expectAllPropertiesToBeSet() {
            GetterSetterDTO expected = new GetterSetterDTO();
            GetterSetterDTO child    = new GetterSetterDTO();

            child.setName("Sarah");
            child.setAge(42);

            expected.setName( "Jim" );
            expected.setAge( 45 );
            expected.setChild( child );

            dtoBuilder.withProperty( "name", "Jim" );
            dtoBuilder.withProperty( "age", "45" );

            dtoBuilder.withProperty( "child.name", "Sarah" );
            dtoBuilder.withProperty( "child.age", "42" );

            assertEquals( expected, dtoBuilder.build() );
        }

        @Test
        public void givenNestedSetterWhereNonLeafPartWasPreviouslyDeclaredAsADTOValue_expectException() {
            dtoBuilder.withProperty( "child.name", "Sarah" );

            IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> dtoBuilder.withProperty("child","Sarah"));
            assertEquals( "Unable to convert 'Sarah' of type 'java.lang.String' to property 'child' of type 'mosaics.lang.reflection.DTOBuilderTest$GetterSetterDTO'", ex.getMessage() );
        }
    }


    @Nested
    public class WithMethodDTOTestCases {
        private final DTOBuilder<LombokWithMethodsDTO> dtoBuilder = DTOBuilder.createDTOBuilderFor( LombokWithMethodsDTO.class );

        @Test
        public void givenWithMethod_expectPropertyToBeSet() {
            LombokWithMethodsDTO expected = new LombokWithMethodsDTO().withName( "Sarah" );

            dtoBuilder.withProperty( "name", "Sarah" );

            assertEquals( expected, dtoBuilder.build() );
        }

        @Test
        public void givenNoZeroArgConstructor_expectException() {
            IllegalArgumentException ex = assertThrows( IllegalArgumentException.class, () -> DTOBuilder.createDTOBuilderFor(LombokWithMethodsAndNoZeroArgConstructorDTO.class) );
            assertEquals( "mosaics.lang.reflection.DTOBuilderTest$LombokWithMethodsAndNoZeroArgConstructorDTO has no public zero arg constructor", ex.getMessage() );
        }

        @Test
        public void givenMixOfWithAndSetterMethods_expectException() {
            IllegalArgumentException ex = assertThrows( IllegalArgumentException.class, () -> DTOBuilder.createDTOBuilderFor(MixOfSettersAndWithMethodsDTO.class) );
            assertEquals( "mosaics.lang.reflection.DTOBuilderTest$MixOfSettersAndWithMethodsDTO has both setter methods and with methods declared.. DTOBuilder does not support mixing setter methods and with methods because it is unclear whether the DTO is immutable or not.", ex.getMessage() );
        }

        @Test
        public void givenMultiplePropsWithMethod_expectPropertyToBeSet() {
            LombokWithMethodsDTO expected = new LombokWithMethodsDTO()
                .withName( "Sarah" )
                .withAge( 42 );

            dtoBuilder
                .withProperty( "name", "Sarah" )
                .withProperty( "age", 42 );

            assertEquals( expected, dtoBuilder.build() );
        }

        @Test
        public void givenWithMethodThatGoesToADTOWithSettersAndGetters() {
            GetterSetterDTO getterSetterChild = new GetterSetterDTO();
            getterSetterChild.setName( "Jim" );
            getterSetterChild.setAge( 10 );

            LombokWithMethodsDTO expected = new LombokWithMethodsDTO()
                .withName( "Sarah" )
                .withAge( 42 )
                .withGetterSetterChild( getterSetterChild );

            dtoBuilder
                .withProperty( "name", "Sarah" )
                .withProperty( "age", 42 )
                .withProperty( "getterSetterChild.name", "Jim" )
                .withProperty( "getterSetterChild.age", "10" );

            assertEquals( expected, dtoBuilder.build() );
        }

        @Test
        public void givenSettersAndGettersDTOThatGoesToAWithMethod() {
            DTOBuilder<GetterSetterDTO> dtoBuilder = DTOBuilder.createDTOBuilderFor( GetterSetterDTO.class );

            GetterSetterDTO expected = new GetterSetterDTO();
            expected.setName( "Jim" );
            expected.setAge( 10 );
            expected.setLombokWithMethodsChild(
                new LombokWithMethodsDTO()
                    .withName( "Sarah" )
                    .withAge( 42 )
            );

            dtoBuilder
                .withProperty( "name", "Jim" )
                .withProperty( "age", 10 )
                .withProperty( "lombokWithMethodsChild.name", "Sarah" )
                .withProperty( "lombokWithMethodsChild.age", "42" );

            assertEquals( expected, dtoBuilder.build() );
        }
    }


    @Nested
    public class LombokBuilderTestCases{
        private final DTOBuilder<LombokBuilderDTO> dtoBuilder = DTOBuilder.createDTOBuilderFor( LombokBuilderDTO.class );

        @Test
        public void givenMissingProperty_expectException() {
            LombokBuilderDTO expected = new LombokBuilderDTO("Jenni", 42, null, null, null, null);

            IllegalArgumentException ex = assertThrows( IllegalArgumentException.class, () -> dtoBuilder.withProperty( "doesNotExist", "Jenni" ) );
            assertEquals( "mosaics.lang.reflection.DTOBuilderTest$LombokBuilderDTO does not have a property called 'doesNotExist'", ex.getMessage() );
        }

        @Test
        public void givenFieldThatHasBeenAssignedValueWithNoDefaultAnnotation_expectException() {
            LombokBuilderDTO expected = new LombokBuilderDTO("Jenni", 42, null, null, null, null);

            dtoBuilder.withProperty( "missingDefaultAnnotation", "99" );

            IllegalArgumentException ex = assertThrows( IllegalArgumentException.class, dtoBuilder::build );
            assertEquals( "mosaics.lang.reflection.DTOBuilderTest$LombokBuilderDTO$LombokBuilderDTOBuilder does not have setter on its builder called 'missingDefaultAnnotation';  is it missing the lombok @Default annotation?", ex.getMessage() );
        }

        @Test
        public void givenNoProperties_expectDefaults() {
            LombokBuilderDTO expected = new LombokBuilderDTO("Joe Blogs", 42, null, null, null, null);

            assertEquals( expected, dtoBuilder.build() );
        }

        @Test
        public void givenOneProperty_expectDTOWithPropertySet() {
            LombokBuilderDTO expected = new LombokBuilderDTO("Jenni", 42, null, null, null, null);

            dtoBuilder.withProperty( "name", "Jenni" );

            assertEquals( expected, dtoBuilder.build() );
        }

        @Test
        public void givenTwoProperties_expectDTOWithPropertiesSet() {
            LombokBuilderDTO expected = new LombokBuilderDTO("Jenni", 10, null, null, null, null);

            dtoBuilder.withProperty( "name", "Jenni" );
            dtoBuilder.withProperty( "age", "10" );

            assertEquals( expected, dtoBuilder.build() );
        }

        @Test
        public void givenLombokBuilderToLombokBuilderProperties2Deep() {
            LombokBuilderDTO expected = new LombokBuilderDTO(
                "Jenni",
                10,
                new File("f.txt"),
                new LombokBuilderDTO("Charlie", 2, null, null, null, null),
                null,
                null
            );

            dtoBuilder.withProperty( "name", "Jenni" );
            dtoBuilder.withProperty( "age", "10" );
            dtoBuilder.withProperty( "file", "f.txt" );
            dtoBuilder.withProperty( "child.name", "Charlie" );
            dtoBuilder.withProperty( "child.age", "2" );

            assertEquals( expected, dtoBuilder.build() );
        }

        @Test
        public void givenLombokBuilderToLombokBuilderProperties3Deep() {
            LombokBuilderDTO expected = new LombokBuilderDTO(
                "Jenni",
                10,
                null,
                new LombokBuilderDTO(
                    "Charlie",
                    2,
                    null,
                    new LombokBuilderDTO(
                        "Bella",
                        1,
                        null,
                        null,
                        null,
                        null
                    ),
                    null,
                    null
                ),
                null,
                null
            );

            dtoBuilder.withProperty( "name", "Jenni" );
            dtoBuilder.withProperty( "age", "10" );
            dtoBuilder.withProperty( "child.name", "Charlie" );
            dtoBuilder.withProperty( "child.age", "2" );
            dtoBuilder.withProperty( "child.child.name", "Bella" );
            dtoBuilder.withProperty( "child.child.age", "1" );

            assertEquals( expected, dtoBuilder.build() );
        }

        @Test
        public void givenLombokBuilderToReflectiveDTODTO() {
            GetterSetterDTO getterSetterDTO = new GetterSetterDTO();
            getterSetterDTO.setName("Charlie");
            getterSetterDTO.setAge(2);

            LombokBuilderDTO expected = new LombokBuilderDTO(
                "Jenni",
                10,
                null,
                null,
                null,
                getterSetterDTO
            );

            dtoBuilder.withProperty( "name", "Jenni" );
            dtoBuilder.withProperty( "age", "10" );
            dtoBuilder.withProperty( "getterSetterChild.name", "Charlie" );
            dtoBuilder.withProperty( "getterSetterChild.age", "2" );

            assertEquals( expected, dtoBuilder.build() );
        }

        @Test
        public void givenReflectiveDTOToLombokBuilderDTO() {
            DTOBuilder<GetterSetterDTO> dtoBuilder = DTOBuilder.createDTOBuilderFor( GetterSetterDTO.class );

            GetterSetterDTO expected = new GetterSetterDTO();
            expected.setName("Jenni");
            expected.setAge(10);
            expected.setLombokBuilderChild(
                new LombokBuilderDTO(
                    "Charlie",
                    2,
                    null,
                    null,
                    null,
                    null
                )
            );

            dtoBuilder.withProperty( "name", "Jenni" );
            dtoBuilder.withProperty( "age", "10" );
            dtoBuilder.withProperty( "lombokBuilderChild.name", "Charlie" );
            dtoBuilder.withProperty( "lombokBuilderChild.age", "2" );

            assertEquals( expected, dtoBuilder.build() );
        }
    }


    @Nested
    public class PublicFieldDTOTestCases {
        private final DTOBuilder<PublicFieldBuilderDTO> dtoBuilder = DTOBuilder.createDTOBuilderFor( PublicFieldBuilderDTO.class );

        @Test
        public void givenNoProperties_expectDefaults() {
            PublicFieldBuilderDTO expected = new PublicFieldBuilderDTO();

            assertEquals( expected, dtoBuilder.build() );
        }

        @Test
        public void givenOneProperty_expectDTOWithPropertySet() {
            PublicFieldBuilderDTO expected = new PublicFieldBuilderDTO("Jenni", null);

            dtoBuilder.withProperty( "name", "Jenni" );

            assertEquals( expected, dtoBuilder.build() );
        }

        @Test
        public void givenOneEnumProperty_expectDTOWithPropertySet() {
            PublicFieldBuilderDTO expected = new PublicFieldBuilderDTO(null, RGB.RED);

            dtoBuilder.withProperty( "rgb", "RED" );

            assertEquals( expected, dtoBuilder.build() );
        }
    }

    // ConstructorDTOTestCases
    // StaticFactoryMethodTestCases
    //



    /**
     * Lorem ipsum dolor sit amet.
     */
    @Getter
    @Setter
    @EqualsAndHashCode
    public static class GetterSetterDTO {
        /**
         * Mauris vel eros vitae velit.
         */
        private String name;

        /**
         * Tristique tempor lectus.
         */
        private int age;

        /**
         * Fusce at odio eros.
         */
        private GetterSetterDTO child;

        /**
         * Aenean ullamcorper mauris elit.
         */
        public LombokWithMethodsDTO lombokWithMethodsChild;

        /**
         * Aenean ullamcorper mauris elit.
         */
        public LombokBuilderDTO lombokBuilderChild;
    }

    @With
    @Value
    @AllArgsConstructor
    public static class LombokWithMethodsDTO {
        /**
         * Mauris vel eros vitae velit.
         */
        private String name;

        /**
         * Tristique tempor lectus.
         */
        private int age;

        /**
         * Fusce at odio eros.
         */
        private LombokWithMethodsDTO child;

        /**
         * Aenean ullamcorper mauris elit.
         */
        private GetterSetterDTO getterSetterChild;

        public LombokWithMethodsDTO() {
            this( null, 0, null, null );
        }
    }

    public static enum RGB {RED,GREEN,BLUE}

    @ToString
    @EqualsAndHashCode
    @AllArgsConstructor
    public static class PublicFieldBuilderDTO {
        /**
         * Mauris vel eros vitae velit.
         */
        public String name;

        /**
         * Fusce at odio eros.
         */
        public RGB rgb;

        public PublicFieldBuilderDTO() {
            this(null,null);
        }
    }

    @With
    @Value
    @Builder
    public static class LombokBuilderDTO {
        /**
         * Mauris vel eros vitae velit.
         */
        @Builder.Default
        private String name = "Joe Blogs";

        /**
         * Tristique tempor lectus.
         */
        @Builder.Default
        private int age = 42;

        private long missingDefaultAnnotation = 20;

        private File file;

        /**
         * Fusce at odio eros.
         */
        private LombokBuilderDTO child;

        /**
         * Fusce at odio eros.
         */
        private LombokWithMethodsDTO lombokWithMethodsChild;

        /**
         * Aenean ullamcorper mauris elit.
         */
        private GetterSetterDTO getterSetterChild;
    }

    @With
    @Value
    @AllArgsConstructor
    public static class MixOfSettersAndWithMethodsDTO {
        /**
         * Mauris vel eros vitae velit.
         */
        private String name;

        /**
         * Tristique tempor lectus.
         */
        private int age;

        /**
         * Fusce at odio eros.
         */
        private LombokWithMethodsDTO child;

        public MixOfSettersAndWithMethodsDTO() {
            this( null, 0, null );
        }

        public void setAge(int newAge) {}
    }

    @With
    @Value
    @AllArgsConstructor
    public static class LombokWithMethodsAndNoZeroArgConstructorDTO {
        /**
         * Mauris vel eros vitae velit.
         */
        private String name;

        /**
         * Tristique tempor lectus.
         */
        private int age;

        /**
         * Fusce at odio eros.
         */
        private LombokWithMethodsDTO child;
    }

    /**
     * Lorem ipsum dolor sit amet.
     */
    @AllArgsConstructor
    @Getter
    @Setter
    @EqualsAndHashCode
    public static class GetterSetterWithNoZeroArgConstructorDTO {
        /**
         * Mauris vel eros vitae velit.
         */
        private String name;

        /**
         * Tristique tempor lectus.
         */
        private int age;
    }
}
