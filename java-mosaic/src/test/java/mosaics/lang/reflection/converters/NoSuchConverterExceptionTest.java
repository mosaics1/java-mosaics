//package mosaics.lang.reflection_old.converters;
//
//import mosaics.lang.reflection_old.JavaClass;
//import mosaics.lang.reflection_old.converters.NoSuchConverterException;
//import org.junit.jupiter.api.Test;
//
//
//public class NoSuchConverterExceptionTest {
//
//    @Test
//    public void testExceptionMessage() {
//        NoSuchConverterException ex = new NoSuchConverterException( JavaClass.STRING, JavaClass.INTEGER_PRIMITIVE );
//
//        assertEquals( "Unable to find a TypeConverter that converts java.lang.String to int", ex.getMessage() );
//    }
//
//}
