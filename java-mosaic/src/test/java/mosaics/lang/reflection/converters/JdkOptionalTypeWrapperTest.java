//package mosaics.lang.reflection_old.converters;
//
//import com.softwaremosaic.fp.Try;
//import mosaics.lang.reflection_old.JavaClass;
//import mosaics.lang.reflection_old.converters.JdkOptionalTypeWrapper;
//import org.junit.jupiter.api.Test;
//
//import java.util.Optional;
//
//import static com.softwaremosaic.lang.Backdoor.cast;
//import static org.junit.jupiter.api.Assertions.assertEquals;
//
//
//@SuppressWarnings("unchecked")
//public class JdkOptionalTypeWrapperTest {
//
//// Nullable Element Tests
//
//    @Test
//    public void givenNullableElementType_wrapValue_expectSomeValue() {
//        JdkOptionalTypeWrapper<String> wrapper = new JdkOptionalTypeWrapper<>( cast(new JavaClass(Optional.class,JavaClass.STRING)) );
//
//        assertEquals( Try.succeeded(Optional.of("abc")), wrapper.wrap("abc") );
//    }
//
//    @Test
//    public void givenNullableElementType_wrapNull_expectNone() {
//        JdkOptionalTypeWrapper<String> wrapper = new JdkOptionalTypeWrapper<>( cast(new JavaClass(Optional.class,JavaClass.STRING)) );
//
//        assertEquals( Try.succeeded(Optional.empty()), wrapper.wrap(null) );
//    }
//
//
//    @Test
//    public void givenNullableElementType_unwrapNull_expectNull() {
//        JdkOptionalTypeWrapper<String> wrapper = new JdkOptionalTypeWrapper<>( cast(new JavaClass(Optional.class,JavaClass.STRING)) );
//
//        assertEquals( Try.succeeded(null), wrapper.unwrap(null) );
//    }
//
//    @Test
//    public void givenNullableElementType_unwrapNone_expectNull() {
//        JdkOptionalTypeWrapper<String> wrapper = new JdkOptionalTypeWrapper<>( cast(new JavaClass(Optional.class,JavaClass.STRING)) );
//
//        assertEquals( Try.succeeded(null), wrapper.unwrap(Optional.empty()) );
//    }
//
//    @Test
//    public void givenNullableElementType_unwrapSomeValue_expectValue() {
//        JdkOptionalTypeWrapper<String> wrapper = new JdkOptionalTypeWrapper<>( cast(new JavaClass(Optional.class,JavaClass.STRING)) );
//
//        assertEquals( Try.succeeded("abc"), wrapper.unwrap(Optional.of("abc")) );
//    }
//
//
//// Not Nullable Element Tests
//
//    @Test
//    public void givenNonNullableElementType_wrapValue_expectSomeValue() {
//        JdkOptionalTypeWrapper<Integer> wrapper = new JdkOptionalTypeWrapper<>(
//            cast(new JavaClass(Optional.class,JavaClass.INTEGER_PRIMITIVE))
//        );
//
//        assertEquals( Try.succeeded(Optional.of(123)), wrapper.wrap(123) );
//    }
//
//    @Test
//    public void givenNonNullableElementType_wrapNull_expectNone() {
//        JdkOptionalTypeWrapper<Integer> wrapper = new JdkOptionalTypeWrapper<>(
//            cast(new JavaClass(Optional.class,JavaClass.INTEGER_PRIMITIVE))
//        );
//
//        assertEquals( Try.succeeded(Optional.empty()), wrapper.wrap(null) );
//    }
//
//    @Test
//    public void givenNonNullableElementType_unwrapNull_expectError() {
//        JdkOptionalTypeWrapper<Integer> wrapper = new JdkOptionalTypeWrapper<>(
//            cast(new JavaClass(Optional.class,JavaClass.INTEGER_PRIMITIVE))
//        );
//
//        assertEquals( Try.failed("int is not nullable"), wrapper.unwrap(null) );
//    }
//
//    @Test
//    public void givenNonNullableElementType_unwrapNone_expectError() {
//        JdkOptionalTypeWrapper<Integer> wrapper = new JdkOptionalTypeWrapper<>(
//            cast(new JavaClass(Optional.class,JavaClass.INTEGER_PRIMITIVE))
//        );
//
//        assertEquals( Try.failed("int is not nullable"), wrapper.unwrap(Optional.empty()) );
//    }
//
//    @Test
//    public void givenNonNullableElementType_unwrapSomeValue_expectValue() {
//        JdkOptionalTypeWrapper<Integer> wrapper = new JdkOptionalTypeWrapper<>(
//            cast(new JavaClass(Optional.class,JavaClass.INTEGER_PRIMITIVE))
//        );
//
//        assertEquals( Try.succeeded(123), wrapper.unwrap(Optional.of(123)) );
//    }
//
//}
