//package mosaics.lang.reflection_old.converters;
//
//import com.softwaremosaic.fp.Try;
//import mosaics.lang.reflection_old.JavaClass;
//import mosaics.lang.reflection_old.converters.StringCodecTypeConverter;
//import mosaics.lang.reflection_old.converters.TypeConverter;
//import mosaics.strings.StringCodec;
//import org.junit.jupiter.api.Test;
//import org.mockito.Mockito;
//
//import java.text.ParseException;
//
//import static com.softwaremosaic.lang.Backdoor.cast;
//import static mosaics.lang.reflection_old.converters.I18nContext.LONDON;
//import static junit.framework.TestCase.assertNull;
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.jupiter.api.Assertions.assertNotEquals;
//
//
//public class StringCodecTypeConverterTest {
//
//    private StringCodec<Integer> codecMock = cast( Mockito.mock(StringCodec.class) );
//
//
//
//
//    @Test
//    public void testHashCode() {
//        TypeConverter<String,Integer> converter1 = new StringCodecTypeConverter<>( codecMock, JavaClass.INTEGER_OBJECT );
//        TypeConverter<String,Integer> converter2 = new StringCodecTypeConverter<>( codecMock, JavaClass.INTEGER_PRIMITIVE );
//        TypeConverter<String,Integer> converter3 = new StringCodecTypeConverter<>( codecMock, JavaClass.INTEGER_OBJECT );
//
//        assertEquals( converter1.hashCode(), converter3.hashCode() );
//        assertNotEquals( converter1.hashCode(), converter2.hashCode() );
//    }
//
//    @Test
//    public void testGetSourceType() {
//        TypeConverter<String,Integer> converter = new StringCodecTypeConverter<>( codecMock, JavaClass.INTEGER_OBJECT );
//
//        assertEquals( JavaClass.STRING, converter.getSourceType() );
//    }
//
//    @Test
//    public void testGetDestType() {
//        TypeConverter<String,Integer> converter = new StringCodecTypeConverter<>( codecMock, JavaClass.INTEGER_OBJECT );
//
//        assertEquals( JavaClass.INTEGER_OBJECT, converter.getDestType() );
//    }
//
//
//// givenNoDefaultNullableType_
//
//    @Test
//    public void givenNoDefaultNullableType_convert() {
//        Mockito.when( codecMock.decode("42") ).thenReturn( Try.succeeded(42) );
//        Mockito.when( codecMock.decode("0") ).thenReturn( Try.succeeded(0) );
//        Mockito.when( codecMock.decode("-10") ).thenReturn( Try.succeeded(-10) );
//        Mockito.when( codecMock.decode("abc") ).thenReturn( Try.failed("not convertible") );
//
//        TypeConverter<String,Integer> converter = new StringCodecTypeConverter<>( codecMock, JavaClass.INTEGER_OBJECT );
//
//        assertEquals( 42, converter.convert(LONDON, "42").getResult().intValue() );
//        assertEquals( 0, converter.convert(LONDON, "0").getResult().intValue() );
//        assertEquals( -10, converter.convert(LONDON, "-10").getResult().intValue() );
//        assertNull( converter.convert(LONDON, "").getResult() );
//        assertNull( converter.convert(LONDON, "   ").getResult() );
//        assertNull( converter.convert(LONDON, null).getResult() );
//
//        assertEquals( Try.failed("not convertible"), converter.convert(LONDON, "abc") );
//    }
//
//    @Test
//    public void givenNoDefaultNullableType_reverse() {
//        Mockito.when( codecMock.encode(42) ).thenReturn( "42" );
//        Mockito.when( codecMock.encode(0) ).thenReturn( "0" );
//        Mockito.when( codecMock.encode(-10) ).thenReturn( "-10" );
//
//        TypeConverter<String,Integer> converter = new StringCodecTypeConverter<>( codecMock, JavaClass.INTEGER_OBJECT );
//
//        assertEquals( "42", converter.reverse(LONDON, 42).getResult() );
//        assertEquals( "0", converter.reverse(LONDON, 0).getResult() );
//        assertEquals( "-10", converter.reverse(LONDON, -10).getResult() );
//    }
//
//
//// givenNoDefaultNotNullableType_
//
//    @Test
//    public void givenNoDefaultNotNullableType_convert() {
//        Mockito.when( codecMock.decode("42") ).thenReturn( Try.succeeded(42) );
//        Mockito.when( codecMock.decode("0") ).thenReturn( Try.succeeded(0) );
//        Mockito.when( codecMock.decode("-10") ).thenReturn( Try.succeeded(-10) );
//        Mockito.when( codecMock.decode("abc") ).thenReturn( Try.failed("not convertible") );
//
//        TypeConverter<String,Integer> converter = new StringCodecTypeConverter<>( codecMock, JavaClass.INTEGER_PRIMITIVE );
//
//        assertEquals( 42, converter.convert(LONDON, "42").getResult().intValue() );
//        assertEquals( 0, converter.convert(LONDON, "0").getResult().intValue() );
//        assertEquals( -10, converter.convert(LONDON, "-10").getResult().intValue() );
//        assertEquals( Try.failed(new ParseException("",0)), converter.convert(LONDON, "") );
//        assertEquals( Try.failed(new ParseException("   ",0)), converter.convert(LONDON, "   ") );
//        assertEquals( Try.failed(new ParseException(null,0)), converter.convert(LONDON, null) );
//
//        assertEquals( Try.failed("not convertible"), converter.convert(LONDON, "abc") );
//    }
//
//    @Test
//    public void givenNoDefaultNotNullableType_reverse() {
//        Mockito.when( codecMock.encode(42) ).thenReturn( "42" );
//        Mockito.when( codecMock.encode(0) ).thenReturn( "0" );
//        Mockito.when( codecMock.encode(-10) ).thenReturn( "-10" );
//
//        TypeConverter<String,Integer> converter = new StringCodecTypeConverter<>( codecMock, JavaClass.INTEGER_PRIMITIVE );
//
//        assertEquals( "42", converter.reverse(LONDON, 42).getResult() );
//        assertEquals( "0", converter.reverse(LONDON, 0).getResult() );
//        assertEquals( "-10", converter.reverse(LONDON, -10).getResult() );
//    }
//
//
//// givenDefaultNullableType_
//
//    @Test
//    public void givenDefaultNullableType_convert() {
//        Mockito.when( codecMock.decode("42") ).thenReturn( Try.succeeded(42) );
//        Mockito.when( codecMock.decode("0") ).thenReturn( Try.succeeded(0) );
//        Mockito.when( codecMock.decode("-10") ).thenReturn( Try.succeeded(-10) );
//        Mockito.when( codecMock.decode("abc") ).thenReturn( Try.failed("not convertible") );
//
//        JavaClass<Integer> intTypeWithDefaultValue = JavaClass.INTEGER_OBJECT.withDefaultValue( 1 );
//        TypeConverter<String,Integer> converter = new StringCodecTypeConverter<>( codecMock, intTypeWithDefaultValue );
//
//        assertEquals( 42, converter.convert(LONDON, "42").getResult().intValue() );
//        assertEquals( 0, converter.convert(LONDON, "0").getResult().intValue() );
//        assertEquals( -10, converter.convert(LONDON, "-10").getResult().intValue() );
//        assertEquals( Try.succeeded(1), converter.convert(LONDON, "") );
//        assertEquals( Try.succeeded(1), converter.convert(LONDON, "   ") );
//        assertEquals( Try.succeeded(1), converter.convert(LONDON, null) );
//
//        assertEquals( Try.failed("not convertible"), converter.convert(LONDON, "abc") );
//    }
//
//    @Test
//    public void givenDefaultNullableType_reverse() {
//        Mockito.when( codecMock.encode(42) ).thenReturn( "42" );
//        Mockito.when( codecMock.encode(0) ).thenReturn( "0" );
//        Mockito.when( codecMock.encode(-10) ).thenReturn( "-10" );
//
//        JavaClass<Integer> intTypeWithDefaultValue = JavaClass.INTEGER_OBJECT.withDefaultValue( 1 );
//        TypeConverter<String,Integer> converter = new StringCodecTypeConverter<>( codecMock, intTypeWithDefaultValue );
//
//        assertEquals( "42", converter.reverse(LONDON, 42).getResult() );
//        assertEquals( "0", converter.reverse(LONDON, 0).getResult() );
//        assertEquals( "-10", converter.reverse(LONDON, -10).getResult() );
//    }
//
//
//// givenDefaultNotNullableType_
//
//    @Test
//    public void givenDefaultNotNullableType_convert() {
//        Mockito.when( codecMock.decode("42") ).thenReturn( Try.succeeded(42) );
//        Mockito.when( codecMock.decode("0") ).thenReturn( Try.succeeded(0) );
//        Mockito.when( codecMock.decode("-10") ).thenReturn( Try.succeeded(-10) );
//        Mockito.when( codecMock.decode("abc") ).thenReturn( Try.failed("not convertible") );
//
//        JavaClass<Integer> intTypeWithDefaultValue = JavaClass.INTEGER_PRIMITIVE.withDefaultValue( 1 );
//        TypeConverter<String,Integer> converter = new StringCodecTypeConverter<>( codecMock, intTypeWithDefaultValue );
//
//        assertEquals( 42, converter.convert(LONDON, "42").getResult().intValue() );
//        assertEquals( 0, converter.convert(LONDON, "0").getResult().intValue() );
//        assertEquals( -10, converter.convert(LONDON, "-10").getResult().intValue() );
//        assertEquals( Try.succeeded(1), converter.convert(LONDON, "") );
//        assertEquals( Try.succeeded(1), converter.convert(LONDON, "   ") );
//        assertEquals( Try.succeeded(1), converter.convert(LONDON, null) );
//
//        assertEquals( Try.failed("not convertible"), converter.convert(LONDON, "abc") );
//    }
//
//    @Test
//    public void givenDefaultNotNullableType_reverse() {
//        Mockito.when( codecMock.encode(42) ).thenReturn( "42" );
//        Mockito.when( codecMock.encode(0) ).thenReturn( "0" );
//        Mockito.when( codecMock.encode(-10) ).thenReturn( "-10" );
//
//        JavaClass<Integer> intTypeWithDefaultValue = JavaClass.INTEGER_PRIMITIVE.withDefaultValue( 1 );
//        TypeConverter<String,Integer> converter = new StringCodecTypeConverter<>( codecMock, intTypeWithDefaultValue );
//
//        assertEquals( "42", converter.reverse(LONDON, 42).getResult() );
//        assertEquals( "0", converter.reverse(LONDON, 0).getResult() );
//        assertEquals( "-10", converter.reverse(LONDON, -10).getResult() );
//    }
//
//}
