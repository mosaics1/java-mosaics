//package mosaics.lang.reflection_old.converters;
//
//import com.softwaremosaic.fp.FP;
//import com.softwaremosaic.fp.Try;
//import mosaics.lang.reflection_old.JavaClass;
//import mosaics.lang.reflection_old.converters.FPOptionTypeWrapper;
//import org.junit.jupiter.api.Test;
//
//import java.util.Optional;
//
//import static com.softwaremosaic.lang.Backdoor.cast;
//import static org.junit.jupiter.api.Assertions.assertEquals;
//
//
//@SuppressWarnings("unchecked")
//public class FPOptionTypeWrapperTest {
//
//// Nullable Element Tests
//
//    @Test
//    public void givenNullableElementType_wrapValue_expectSomeValue() {
//        FPOptionTypeWrapper<String> wrapper = new FPOptionTypeWrapper<>( cast(new JavaClass(Optional.class,JavaClass.STRING)) );
//
//        assertEquals( Try.succeeded(FP.option("abc")), wrapper.wrap("abc") );
//    }
//
//    @Test
//    public void givenNullableElementType_wrapNull_expectNone() {
//        FPOptionTypeWrapper<String> wrapper = new FPOptionTypeWrapper<>( cast(new JavaClass(Optional.class,JavaClass.STRING)) );
//
//        assertEquals( Try.succeeded(FP.emptyOption()), wrapper.wrap(null) );
//    }
//
//
//    @Test
//    public void givenNullableElementType_unwrapNull_expectNull() {
//        FPOptionTypeWrapper<String> wrapper = new FPOptionTypeWrapper<>( cast(new JavaClass(Optional.class,JavaClass.STRING)) );
//
//        assertEquals( Try.succeeded(null), wrapper.unwrap(null) );
//    }
//
//    @Test
//    public void givenNullableElementType_unwrapNone_expectNull() {
//        FPOptionTypeWrapper<String> wrapper = new FPOptionTypeWrapper<>( cast(new JavaClass(Optional.class,JavaClass.STRING)) );
//
//        assertEquals( Try.succeeded(null), wrapper.unwrap(FP.emptyOption()) );
//    }
//
//    @Test
//    public void givenNullableElementType_unwrapSomeValue_expectValue() {
//        FPOptionTypeWrapper<String> wrapper = new FPOptionTypeWrapper<>( cast(new JavaClass(Optional.class,JavaClass.STRING)) );
//
//        assertEquals( Try.succeeded("abc"), wrapper.unwrap(FP.option("abc")) );
//    }
//
//
//// Not Nullable Element Tests
//
//    @Test
//    public void givenNonNullableElementType_wrapValue_expectSomeValue() {
//        FPOptionTypeWrapper<Integer> wrapper = new FPOptionTypeWrapper<>(
//            cast(new JavaClass(Optional.class,JavaClass.INTEGER_PRIMITIVE))
//        );
//
//        assertEquals( Try.succeeded(FP.option(123)), wrapper.wrap(123) );
//    }
//
//    @Test
//    public void givenNonNullableElementType_wrapNull_expectNone() {
//        FPOptionTypeWrapper<Integer> wrapper = new FPOptionTypeWrapper<>(
//            cast(new JavaClass(Optional.class,JavaClass.INTEGER_PRIMITIVE))
//        );
//
//        assertEquals( Try.succeeded(FP.emptyOption()), wrapper.wrap(null) );
//    }
//
//    @Test
//    public void givenNonNullableElementType_unwrapNull_expectError() {
//        FPOptionTypeWrapper<Integer> wrapper = new FPOptionTypeWrapper<>(
//            cast(new JavaClass(Optional.class,JavaClass.INTEGER_PRIMITIVE))
//        );
//
//        assertEquals( Try.failed("int is not nullable"), wrapper.unwrap(null) );
//    }
//
//    @Test
//    public void givenNonNullableElementType_unwrapNone_expectError() {
//        FPOptionTypeWrapper<Integer> wrapper = new FPOptionTypeWrapper<>(
//            cast(new JavaClass(Optional.class,JavaClass.INTEGER_PRIMITIVE))
//        );
//
//        assertEquals( Try.failed("int is not nullable"), wrapper.unwrap(FP.emptyOption()) );
//    }
//
//    @Test
//    public void givenNonNullableElementType_unwrapSomeValue_expectValue() {
//        FPOptionTypeWrapper<Integer> wrapper = new FPOptionTypeWrapper<>(
//            cast(new JavaClass(Optional.class,JavaClass.INTEGER_PRIMITIVE))
//        );
//
//        assertEquals( Try.succeeded(123), wrapper.unwrap(FP.option(123)) );
//    }
//
//}
