//package mosaics.lang.reflection_old.converters;
//
//import com.softwaremosaic.fp.Try;
//import mosaics.lang.reflection_old.JavaClass;
//import mosaics.lang.reflection_old.converters.I18nContext;
//import mosaics.lang.reflection_old.converters.NoOpConverter;
//import org.junit.jupiter.api.Test;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.jupiter.api.Assertions.assertNotEquals;
//
//
//public class NoOpConverterTest {
//
//    private NoOpConverter<String,String> converter = new NoOpConverter<>( JavaClass.STRING );
//
//
//    @Test
//    public void equalsContract() {
//        NoOpConverter<String,String>   converter1 = new NoOpConverter<>( JavaClass.STRING );
//        NoOpConverter<Integer,Integer> converter2 = new NoOpConverter<>( JavaClass.INTEGER_OBJECT );
//        NoOpConverter<String,String>   converter3 = new NoOpConverter<>( JavaClass.STRING );
//
//        assertEquals( converter1.hashCode(), converter3.hashCode() );
//        assertNotEquals( converter1.hashCode(), converter2.hashCode() );
//    }
//
//    @Test
//    public void givenX_convert_expectXBack() {
//        assertEquals( Try.succeeded("foo"), converter.convert( I18nContext.LONDON, "foo") );
//    }
//
//    @Test
//    public void givenX_reverse_expectXBack() {
//        assertEquals( Try.succeeded("foo"), converter.reverse(I18nContext.LONDON, "foo") );
//    }
//
//}
