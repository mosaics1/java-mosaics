//package mosaics.lang.reflection_old.converters;
//
//import com.softwaremosaic.fp.Try;
//import mosaics.lang.reflection_old.JavaClass;
//import mosaics.lang.reflection_old.converters.ShortToIntConverter;
//import mosaics.lang.reflection_old.converters.TypeConverter;
//import org.junit.jupiter.api.Nested;
//import org.junit.jupiter.api.Test;
//
//import java.text.ParseException;
//
//import static mosaics.lang.reflection_old.converters.I18nContext.LONDON;
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.jupiter.api.Assertions.assertNotEquals;
//
//
//public class ShortToIntConverterTest {
//
//
//    @Test
//    public void equalsContract() {
//        TypeConverter<Short, Integer> converter1 = new ShortToIntConverter( JavaClass.SHORT_PRIMITIVE, JavaClass.INTEGER_PRIMITIVE );
//        TypeConverter<Short, Integer> converter2 = new ShortToIntConverter( JavaClass.SHORT_OBJECT, JavaClass.INTEGER_OBJECT );
//        TypeConverter<Short, Integer> converter3 = new ShortToIntConverter( JavaClass.SHORT_PRIMITIVE, JavaClass.INTEGER_PRIMITIVE );
//
//        assertEquals( converter1.hashCode(), converter3.hashCode() );
//        assertNotEquals( converter1.hashCode(), converter2.hashCode() );
//    }
//
//
//    @Nested
//    public class ShortToIntTestCases {
//        @Test
//        public void shortToIntCases() {
//            TypeConverter<Short, Integer> converter2 = new ShortToIntConverter( JavaClass.SHORT_PRIMITIVE, JavaClass.INTEGER_PRIMITIVE );
//
//            assertEquals( Try.succeeded( 42 ), converter2.convert( LONDON, (short) 42 ) );
//            assertEquals( Try.succeeded( 0 ), converter2.convert( LONDON, (short) 0 ) );
//            assertEquals( Try.succeeded( -1 ), converter2.convert( LONDON, (short) -1 ) );
//            assertEquals( Try.succeeded( (int) Short.MAX_VALUE ), converter2.convert( LONDON, Short.MAX_VALUE ) );
//            assertEquals( Try.succeeded( (int) Short.MIN_VALUE ), converter2.convert( LONDON, Short.MIN_VALUE ) );
//        }
//
//        @Test
//        public void nullObjectShortToObjectInt_expectNull() {
//            TypeConverter<Short, Integer> converter2 = new ShortToIntConverter( JavaClass.SHORT_OBJECT, JavaClass.INTEGER_OBJECT );
//
//            assertEquals( Try.succeeded( null ), converter2.convert( LONDON, null ) );
//        }
//
//        @Test
//        public void nullObjectShortToPrimitiveInt_expectNullError() {
//            TypeConverter<Short, Integer> converter2 = new ShortToIntConverter( JavaClass.SHORT_OBJECT, JavaClass.INTEGER_PRIMITIVE );
//
//            assertEquals( Try.failed( new ParseException( null, 0 ) ), converter2.convert( LONDON, null ) );
//        }
//    }
//
//
//    @Nested
//    public class IntToShortTestCases {
//        @Test
//        public void intToShortCases() {
//            TypeConverter<Integer, Short> converter2 = new ShortToIntConverter( JavaClass.SHORT_PRIMITIVE, JavaClass.INTEGER_PRIMITIVE ).reverse();
//
//            assertEquals( Try.succeeded( (short) 42 ), converter2.convert( LONDON, 42 ) );
//            assertEquals( Try.succeeded( (short) 0 ), converter2.convert( LONDON, 0 ) );
//            assertEquals( Try.succeeded( (short) -1 ), converter2.convert( LONDON, -1 ) );
//            assertEquals( Try.succeeded( Short.MAX_VALUE ), converter2.convert( LONDON, (int) Short.MAX_VALUE ) );
//            assertEquals( Try.succeeded( Short.MIN_VALUE ), converter2.convert( LONDON, (int) Short.MIN_VALUE ) );
//        }
//
//        @Test
//        public void intIsTooLargeToConvertToShort_expectError() {
//            TypeConverter<Integer, Short> converter2 = new ShortToIntConverter( JavaClass.SHORT_PRIMITIVE, JavaClass.INTEGER_PRIMITIVE ).reverse();
//
//            assertEquals( Try.failed( "2147483647 is out of range for a short" ), converter2.convert( LONDON, Integer.MAX_VALUE ) );
//        }
//
//        @Test
//        public void intIsTooSmallToConvertToShort_expectError() {
//            TypeConverter<Integer, Short> converter2 = new ShortToIntConverter( JavaClass.SHORT_PRIMITIVE, JavaClass.INTEGER_PRIMITIVE ).reverse();
//
//            assertEquals( Try.failed( "-2147483648 is out of range for a short" ), converter2.convert( LONDON, Integer.MIN_VALUE ) );
//        }
//
//        @Test
//        public void nullObjectIntToObjectShort_expectNull() {
//            TypeConverter<Integer, Short> converter2 = new ShortToIntConverter( JavaClass.SHORT_OBJECT, JavaClass.INTEGER_PRIMITIVE ).reverse();
//
//            assertEquals( Try.succeeded( null ), converter2.convert( LONDON, null ) );
//        }
//
//        @Test
//        public void nullObjectIntToPrimitiveShort_expectNullError() {
//            TypeConverter<Integer, Short> converter2 = new ShortToIntConverter( JavaClass.SHORT_PRIMITIVE, JavaClass.INTEGER_PRIMITIVE ).reverse();
//
//            assertEquals( Try.failed( new ParseException( null, 0 ) ), converter2.convert( LONDON, null ) );
//        }
//    }
//
//}
