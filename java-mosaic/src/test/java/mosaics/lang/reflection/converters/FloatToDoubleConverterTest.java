//package mosaics.lang.reflection_old.converters;
//
//import com.softwaremosaic.fp.Try;
//import mosaics.lang.reflection_old.converters.FloatToDoubleConverter;
//import mosaics.lang.reflection_old.converters.TypeConverter;
//import org.junit.jupiter.api.Nested;
//import org.junit.jupiter.api.Test;
//
//import java.text.ParseException;
//
//import static mosaics.lang.reflection_old.JavaClass.DOUBLE_OBJECT;
//import static mosaics.lang.reflection_old.JavaClass.DOUBLE_PRIMITIVE;
//import static mosaics.lang.reflection_old.JavaClass.FLOAT_OBJECT;
//import static mosaics.lang.reflection_old.JavaClass.FLOAT_PRIMITIVE;
//import static mosaics.lang.reflection_old.converters.I18nContext.LONDON;
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.jupiter.api.Assertions.assertNotEquals;
//
//
//public class FloatToDoubleConverterTest {
//
//    @Test
//    public void equalsContract() {
//        TypeConverter<Float, Double> converter1 = new FloatToDoubleConverter( FLOAT_PRIMITIVE, DOUBLE_PRIMITIVE );
//        TypeConverter<Float, Double> converter2 = new FloatToDoubleConverter( FLOAT_OBJECT, DOUBLE_OBJECT );
//        TypeConverter<Float, Double> converter3 = new FloatToDoubleConverter( FLOAT_PRIMITIVE, DOUBLE_PRIMITIVE );
//
//        assertEquals( converter1.hashCode(), converter3.hashCode() );
//        assertNotEquals( converter1.hashCode(), converter2.hashCode() );
//    }
//
//    @Nested
//    public class FloatToDoubleTestCases {
//        @Test
//        public void floatToDoubleCases() {
//            TypeConverter<Float, Double> converter2 = new FloatToDoubleConverter( FLOAT_PRIMITIVE, DOUBLE_PRIMITIVE );
//
//            assertEquals( 42.2, converter2.convert( LONDON, 42.2f ).getResult().doubleValue(), 1e-6 );
//            assertEquals( 0.0, converter2.convert( LONDON, 0f ).getResult().doubleValue(), 1e-6 );
//            assertEquals( -1.1, converter2.convert( LONDON, -1.1f ).getResult().doubleValue(), 1e-6 );
//            assertEquals( (double) Float.MAX_VALUE, converter2.convert( LONDON, Float.MAX_VALUE ).getResult().doubleValue(), 1e-6 );
//            assertEquals( (double) Float.MIN_VALUE, converter2.convert( LONDON, Float.MIN_VALUE ).getResult().doubleValue(), 1e-6 );
//        }
//
//        @Test
//        public void nullObjectFloatToObjectDouble_expectNull() {
//            TypeConverter<Float, Double> converter2 = new FloatToDoubleConverter( FLOAT_OBJECT, DOUBLE_OBJECT );
//
//            assertEquals( Try.succeeded( null ), converter2.convert( LONDON, null ) );
//        }
//
//        @Test
//        public void nullObjectFloatToPrimitiveDouble_expectNullError() {
//            TypeConverter<Float, Double> converter2 = new FloatToDoubleConverter( FLOAT_OBJECT, DOUBLE_PRIMITIVE );
//
//            assertEquals( Try.failed( new ParseException( null, 0 ) ), converter2.convert( LONDON, null ) );
//        }
//    }
//
//
//    @Nested
//    public class DoubleToFloatTestCases {
//        @Test
//        public void doubleToFloatCases() {
//            TypeConverter<Double, Float> converter2 = new FloatToDoubleConverter( FLOAT_PRIMITIVE, DOUBLE_PRIMITIVE ).reverse();
//
//            assertEquals( 42.5f, converter2.convert( LONDON, 42.5 ).getResult().doubleValue(), 1e-6 );
//            assertEquals( 0.2f, converter2.convert( LONDON, 0.2 ).getResult().doubleValue(), 1e-6 );
//            assertEquals( -1.2f, converter2.convert( LONDON, -1.2 ).getResult().doubleValue(), 1e-6 );
//            assertEquals( Float.MAX_VALUE, converter2.convert( LONDON, (double) Float.MAX_VALUE ).getResult().doubleValue(), 1e-6 );
//            assertEquals( Float.MIN_VALUE, converter2.convert( LONDON, (double) Float.MIN_VALUE ).getResult().doubleValue(), 1e-6 );
//        }
//
//        @Test
//        public void doubleIsTooLargeToConvertToFloat_expectError() {
//            TypeConverter<Double, Float> converter2 = new FloatToDoubleConverter( FLOAT_PRIMITIVE, DOUBLE_PRIMITIVE ).reverse();
//
//            assertEquals( Try.succeeded( Float.POSITIVE_INFINITY ), converter2.convert( LONDON, Double.MAX_VALUE ) );
//        }
//
//        @Test
//        public void doubleIsTooSmallToConvertToFloat_expectError() {
//            TypeConverter<Double, Float> converter2 = new FloatToDoubleConverter( FLOAT_PRIMITIVE, DOUBLE_PRIMITIVE ).reverse();
//
//            assertEquals( Try.succeeded( 0f ), converter2.convert( LONDON, Double.MIN_VALUE ) );
//        }
//
//        @Test
//        public void nullObjectDoubleToObjectFloat_expectNull() {
//            TypeConverter<Double, Float> converter2 = new FloatToDoubleConverter( FLOAT_OBJECT, DOUBLE_PRIMITIVE ).reverse();
//
//            assertEquals( Try.succeeded( null ), converter2.convert( LONDON, null ) );
//        }
//
//        @Test
//        public void nullObjectDoubleToPrimitiveFloat_expectNullError() {
//            TypeConverter<Double, Float> converter2 = new FloatToDoubleConverter( FLOAT_PRIMITIVE, DOUBLE_PRIMITIVE ).reverse();
//
//            assertEquals( Try.failed( new ParseException( null, 0 ) ), converter2.convert( LONDON, null ) );
//        }
//    }
//
//}
