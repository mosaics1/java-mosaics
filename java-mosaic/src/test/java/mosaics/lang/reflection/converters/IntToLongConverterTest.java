//package mosaics.lang.reflection_old.converters;
//
//import com.softwaremosaic.fp.Try;
//import mosaics.lang.reflection_old.JavaClass;
//import mosaics.lang.reflection_old.converters.IntToLongConverter;
//import mosaics.lang.reflection_old.converters.TypeConverter;
//import org.junit.jupiter.api.Nested;
//import org.junit.jupiter.api.Test;
//
//import java.text.ParseException;
//
//import static mosaics.lang.reflection_old.JavaClass.*;
//import static mosaics.lang.reflection_old.converters.I18nContext.LONDON;
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.jupiter.api.Assertions.assertNotEquals;
//
//
//public class IntToLongConverterTest {
//
//    @Test
//    public void equalsContract() {
//        TypeConverter<Integer, Long> converter1 = new IntToLongConverter( INTEGER_PRIMITIVE, LONG_PRIMITIVE );
//        TypeConverter<Integer, Long> converter2 = new IntToLongConverter( INTEGER_OBJECT, LONG_OBJECT );
//        TypeConverter<Integer, Long> converter3 = new IntToLongConverter( INTEGER_PRIMITIVE, LONG_PRIMITIVE );
//
//        assertEquals( converter1.hashCode(), converter3.hashCode() );
//        assertNotEquals( converter1.hashCode(), converter2.hashCode() );
//    }
//
//    @Nested
//    public class IntegerToLongTestCases {
//        @Test
//        public void integerToLongCases() {
//            TypeConverter<Integer, Long> converter2 = new IntToLongConverter( INTEGER_PRIMITIVE, LONG_PRIMITIVE );
//
//            assertEquals( Try.succeeded( 42L ), converter2.convert( LONDON, 42 ) );
//            assertEquals( Try.succeeded( 0L ), converter2.convert( LONDON, 0 ) );
//            assertEquals( Try.succeeded( -1L ), converter2.convert( LONDON, -1 ) );
//            assertEquals( Try.succeeded( (long) Integer.MAX_VALUE ), converter2.convert( LONDON, Integer.MAX_VALUE ) );
//            assertEquals( Try.succeeded( (long) Integer.MIN_VALUE ), converter2.convert( LONDON, Integer.MIN_VALUE ) );
//        }
//
//        @Test
//        public void nullObjectIntegerToObjectLong_expectNull() {
//            TypeConverter<Integer, Long> converter2 = new IntToLongConverter( INTEGER_OBJECT, JavaClass.LONG_OBJECT );
//
//            assertEquals( Try.succeeded( null ), converter2.convert( LONDON, null ) );
//        }
//
//        @Test
//        public void nullObjectIntegerToPrimitiveLong_expectNullError() {
//            TypeConverter<Integer, Long> converter2 = new IntToLongConverter( INTEGER_OBJECT, LONG_PRIMITIVE );
//
//            assertEquals( Try.failed( new ParseException( null, 0 ) ), converter2.convert( LONDON, null ) );
//        }
//    }
//
//
//    @Nested
//    public class LongToIntegerTestCases {
//        @Test
//        public void longToIntegerCases() {
//            TypeConverter<Long, Integer> converter2 = new IntToLongConverter( INTEGER_PRIMITIVE, LONG_PRIMITIVE ).reverse();
//
//            assertEquals( Try.succeeded( 42 ), converter2.convert( LONDON, 42L ) );
//            assertEquals( Try.succeeded( 0 ), converter2.convert( LONDON, 0L ) );
//            assertEquals( Try.succeeded( -1 ), converter2.convert( LONDON, -1L ) );
//            assertEquals( Try.succeeded( Integer.MAX_VALUE ), converter2.convert( LONDON, (long) Integer.MAX_VALUE ) );
//            assertEquals( Try.succeeded( Integer.MIN_VALUE ), converter2.convert( LONDON, (long) Integer.MIN_VALUE ) );
//        }
//
//        @Test
//        public void longIsTooLargeToConvertToInteger_expectError() {
//            TypeConverter<Long, Integer> converter2 = new IntToLongConverter( INTEGER_PRIMITIVE, LONG_PRIMITIVE ).reverse();
//
//            assertEquals( Try.failed( "9223372036854775807 is out of range for an int" ), converter2.convert( LONDON, Long.MAX_VALUE ) );
//        }
//
//        @Test
//        public void longIsTooSmallToConvertToInteger_expectError() {
//            TypeConverter<Long, Integer> converter2 = new IntToLongConverter( INTEGER_PRIMITIVE, LONG_PRIMITIVE ).reverse();
//
//            assertEquals( Try.failed( "-9223372036854775808 is out of range for an int" ), converter2.convert( LONDON, Long.MIN_VALUE ) );
//        }
//
//        @Test
//        public void nullObjectLongToObjectInteger_expectNull() {
//            TypeConverter<Long, Integer> converter2 = new IntToLongConverter( INTEGER_OBJECT, LONG_PRIMITIVE ).reverse();
//
//            assertEquals( Try.succeeded( null ), converter2.convert( LONDON, null ) );
//        }
//
//        @Test
//        public void nullObjectLongToPrimitiveInteger_expectNullError() {
//            TypeConverter<Long, Integer> converter2 = new IntToLongConverter( INTEGER_PRIMITIVE, LONG_PRIMITIVE ).reverse();
//
//            assertEquals( Try.failed( new ParseException( null, 0 ) ), converter2.convert( LONDON, null ) );
//        }
//    }
//
//}
