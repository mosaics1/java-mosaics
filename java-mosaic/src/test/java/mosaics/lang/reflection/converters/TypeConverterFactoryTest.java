package mosaics.lang.reflection.converters;

import mosaics.fp.Try;
import mosaics.fp.Tryable;
import mosaics.fp.collections.FPOption;
import mosaics.i18n.I18nDateTimeStyle;
import mosaics.lang.Secret;
import mosaics.lang.reflection.JavaClass;
import mosaics.lang.time.DTM;
import mosaics.lang.time.Day;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.Optional;

import static mosaics.i18n.I18nContext.LONDON;
import static mosaics.i18n.I18nContext.ROME;
import static mosaics.lang.Backdoor.cast;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;

//TODO
//GET THESE TESTS WORKING AGAIN
//ADD ENUM SUPPORT
//RETURN TO DTOBuilderTest (enum case should now work)
//RETURN TO APP-MOSAICS (enum cases should now work for AppRunnerTest)


public class TypeConverterFactoryTest {

    @Nested
    public class BooleanTestCases {
        @Test
        public void convertStringToString_expectNoConversion() {
            Tryable<String> actual = TypeConverterFactory.convert( LONDON, "foobar", JavaClass.STRING, JavaClass.STRING );
            Tryable<String> expected = Try.succeeded( "foobar" );

            assertEquals( expected, actual );
            assertSame( expected.getResult(), actual.getResult() );
        }

        @Test
        public void convertStringToObject_expectTypeCastOnly() {
            Tryable<Object> actual = TypeConverterFactory.convert( LONDON, "foobar", JavaClass.STRING, JavaClass.OBJECT );
            Tryable<String> expected = Try.succeeded( "foobar" );

            assertEquals( expected, actual );
            assertSame( expected.getResult(), actual.getResult() );
        }

        @Test
        public void convertStringToBooleanObject_expectConversion() {
            Tryable<Boolean> actual = TypeConverterFactory.convert( LONDON, "true", JavaClass.STRING, JavaClass.BOOLEAN_OBJECT );
            Tryable<Boolean> expected = Try.succeeded( true );

            assertEquals( expected, actual );
            assertSame( expected.getResult(), actual.getResult() );
        }

        @Test
        public void convertStringToBooleanPrimitive_expectConversion() {
            Tryable<Boolean> actual = TypeConverterFactory.convert( LONDON, "true", JavaClass.STRING, JavaClass.BOOLEAN_PRIMITIVE );
            Tryable<Boolean> expected = Try.succeeded( true );

            assertEquals( expected, actual );
        }

        @Test
        public void convertBooleanToString_expectConversion() {
            Tryable<String> actual = TypeConverterFactory.convert( LONDON, true, JavaClass.BOOLEAN_PRIMITIVE, JavaClass.STRING );
            Tryable<String> expected = Try.succeeded( "true" );

            assertEquals( expected, actual );
        }

        @Test
        public void convertNullBooleanToString_expectConversion() {
            TypeConverter<Boolean, String> converter = TypeConverterFactory.<Boolean, String>getConverterFor( JavaClass.BOOLEAN_OBJECT, JavaClass.STRING ).get();
            Tryable<String> actual = converter.convert( LONDON, null );
            Tryable<String> expected = Try.succeeded( null );

            assertEquals( expected, actual );
        }
    }


    @Nested
    public class ShortTestCases {
        @Test
        public void convertStringToShortObject_expectConversion() {
            Tryable<Short> actual = TypeConverterFactory.convert( LONDON, "42", JavaClass.STRING, JavaClass.SHORT_OBJECT );
            Tryable<Short> expected = Try.succeeded( (short) 42 );

            assertEquals( expected, actual );
        }

        @Test
        public void convertStringToShortPrimitive_expectConversion() {
            Tryable<Short> actual = TypeConverterFactory.convert( LONDON, "42", JavaClass.STRING, JavaClass.SHORT_PRIMITIVE );
            Tryable<Short> expected = Try.succeeded( (short) 42 );

            assertEquals( expected, actual );
        }

        @Test
        public void convertShortToString_expectConversion() {
            Tryable<String> actual = TypeConverterFactory.convert( LONDON, (short) 42, JavaClass.SHORT_PRIMITIVE, JavaClass.STRING );
            Tryable<String> expected = Try.succeeded( "42" );

            assertEquals( expected, actual );
        }

        @Test
        public void convertNullShortToString_expectConversion() {
            TypeConverter<Short, String> converter = TypeConverterFactory.<Short, String>getConverterFor( JavaClass.SHORT_OBJECT, JavaClass.STRING ).get();
            Tryable<String> actual = converter.convert( LONDON, null );
            Tryable<String> expected = Try.succeeded( null );

            assertEquals( expected, actual );
        }
    }


    @Nested
    public class IntegerTestCases {
        @Test
        public void convertStringToIntegerObject_expectConversion() {
            Tryable<Integer> actual = TypeConverterFactory.convert( LONDON, "42", JavaClass.STRING, JavaClass.INTEGER_OBJECT );
            Tryable<Integer> expected = Try.succeeded( 42 );

            assertEquals( expected, actual );
        }

        @Test
        public void convertStringToIntegerPrimitive_expectConversion() {
            Tryable<Integer> actual = TypeConverterFactory.convert( LONDON, "42", JavaClass.STRING, JavaClass.INTEGER_PRIMITIVE );
            Tryable<Integer> expected = Try.succeeded( 42 );

            assertEquals( expected, actual );
        }

        @Test
        public void convertIntegerToString_expectConversion() {
            Tryable<String> actual = TypeConverterFactory.convert( LONDON, 42, JavaClass.INTEGER_PRIMITIVE, JavaClass.STRING );
            Tryable<String> expected = Try.succeeded( "42" );

            assertEquals( expected, actual );
        }

        @Test
        public void convertNullIntegerToString_expectConversion() {
            TypeConverter<Integer, String> converter = TypeConverterFactory.<Integer, String>getConverterFor( JavaClass.INTEGER_OBJECT, JavaClass.STRING ).get();
            Tryable<String> actual = converter.convert( LONDON, null );
            Tryable<String> expected = Try.succeeded( null );

            assertEquals( expected, actual );
        }
    }

    @Nested
    public class LongTestCases {
        @Test
        public void convertStringToLongObject_expectConversion() {
            Tryable<Long> actual = TypeConverterFactory.convert( LONDON, "42", JavaClass.STRING, JavaClass.LONG_OBJECT );
            Tryable<Long> expected = Try.succeeded( 42L );

            assertEquals( expected, actual );
        }

        @Test
        public void convertStringToLongPrimitive_expectConversion() {
            Tryable<Long> actual = TypeConverterFactory.convert( LONDON, "42", JavaClass.STRING, JavaClass.LONG_PRIMITIVE );
            Tryable<Long> expected = Try.succeeded( 42L );

            assertEquals( expected, actual );
        }

        @Test
        public void convertLongToString_expectConversion() {
            Tryable<String> actual = TypeConverterFactory.convert( LONDON, 42L, JavaClass.LONG_PRIMITIVE, JavaClass.STRING );
            Tryable<String> expected = Try.succeeded( "42" );

            assertEquals( expected, actual );
        }

        @Test
        public void convertNullLongToString_expectConversion() {
            TypeConverter<Long, String> converter = TypeConverterFactory.<Long, String>getConverterFor( JavaClass.LONG_OBJECT, JavaClass.STRING ).get();
            Tryable<String> actual = converter.convert( LONDON, null );
            Tryable<String> expected = Try.succeeded( null );

            assertEquals( expected, actual );
        }
    }


    @Nested
    public class FloatTestCases {
        @Test
        public void convertStringToFloatObject_expectConversion() {
            Tryable<Float> actual = TypeConverterFactory.convert( LONDON, "42", JavaClass.STRING, JavaClass.FLOAT_OBJECT );
            Tryable<Float> expected = Try.succeeded( 42f );

            assertEquals( expected, actual );
        }

        @Test
        public void convertStringToFloatPrimitive_expectConversion() {
            Tryable<Float> actual = TypeConverterFactory.convert( LONDON, "42", JavaClass.STRING, JavaClass.FLOAT_PRIMITIVE );
            Tryable<Float> expected = Try.succeeded( 42f );

            assertEquals( expected, actual );
        }

        @Test
        public void convertFloatToString_expectConversion() {
            Tryable<String> actual = TypeConverterFactory.convert( LONDON, 42f, JavaClass.FLOAT_PRIMITIVE, JavaClass.STRING );
            Tryable<String> expected = Try.succeeded( "42.0" );

            assertEquals( expected, actual );
        }

        @Test
        public void convertNullFloatToString_expectConversion() {
            TypeConverter<Float, String> converter = TypeConverterFactory.<Float, String>getConverterFor( JavaClass.FLOAT_OBJECT, JavaClass.STRING ).get();
            Tryable<String> actual = converter.convert( LONDON, null );
            Tryable<String> expected = Try.succeeded( null );

            assertEquals( expected, actual );
        }
    }


    @Nested
    public class DoubleTestCases {
        @Test
        public void convertStringToDoubleObject_expectConversion() {
            Tryable<Double> actual = TypeConverterFactory.convert( LONDON, "42", JavaClass.STRING, JavaClass.DOUBLE_OBJECT );
            Tryable<Double> expected = Try.succeeded( 42.0 );

            assertEquals( expected, actual );
        }

        @Test
        public void convertStringToDoublePrimitive_expectConversion() {
            Tryable<Double> actual = TypeConverterFactory.convert( LONDON, "42", JavaClass.STRING, JavaClass.DOUBLE_PRIMITIVE );
            Tryable<Double> expected = Try.succeeded( 42.0 );

            assertEquals( expected, actual );
        }

        @Test
        public void convertDoubleToString_expectConversion() {
            Tryable<String> actual = TypeConverterFactory.convert( LONDON, 42.0, JavaClass.DOUBLE_PRIMITIVE, JavaClass.STRING );
            Tryable<String> expected = Try.succeeded( "42.0" );

            assertEquals( expected, actual );
        }

        @Test
        public void convertNullDoubleToString_expectConversion() {
            TypeConverter<Double, String> converter = TypeConverterFactory.<Double, String>getConverterFor( JavaClass.DOUBLE_OBJECT, JavaClass.STRING ).get();
            Tryable<String> actual = converter.convert( LONDON, null );
            Tryable<String> expected = Try.succeeded( null );

            assertEquals( expected, actual );
        }
    }


    @Nested
    public class CharacterTestCases {
        @Test
        public void convertStringToCharacterObject_expectConversion() {
            Tryable<Character> actual   = TypeConverterFactory.convert( LONDON, "4", JavaClass.STRING, JavaClass.CHARACTER_OBJECT );
            Tryable<Character> expected = Try.succeeded( '4' );

            assertEquals( expected, actual );
        }

        @Test
        public void convertStringToCharacterPrimitive_expectConversion() {
            Tryable<Character> actual   = TypeConverterFactory.convert( LONDON, "4", JavaClass.STRING, JavaClass.CHARACTER_PRIMITIVE );
            Tryable<Character> expected = Try.succeeded( '4' );

            assertEquals( expected, actual );
        }

        @Test
        public void convertCharacterToString_expectConversion() {
            Tryable<String> actual   = TypeConverterFactory.convert( LONDON, '4', JavaClass.CHARACTER_PRIMITIVE, JavaClass.STRING );
            Tryable<String> expected = Try.succeeded( "4" );

            assertEquals( expected, actual );
        }

        @Test
        public void convertNullCharacterToString_expectConversion() {
            TypeConverter<Character, String> converter = TypeConverterFactory.<Character, String>getConverterFor( JavaClass.CHARACTER_OBJECT, JavaClass.STRING ).get();
            Tryable<String> actual   = converter.convert( LONDON, null );
            Tryable<String> expected = Try.succeeded( "" );

            assertEquals( expected, actual );
        }
    }


    @Nested
    public class JdkOptionalsTestCases {
        @Test
        public void convertBooleanPrimitiveFalseToJdkOpt_expectSomeFalse() {
            TypeConverter<Boolean, Optional<Boolean>> converter = cast( TypeConverterFactory.<Boolean, Optional<Boolean>>getConverterFor( JavaClass.BOOLEAN_PRIMITIVE, JavaClass.of( Optional.class, Boolean.class ) ).get() );
            Tryable<Optional<Boolean>> actual   = converter.convert( LONDON, false );
            Tryable<Optional<Boolean>> expected = Try.succeeded( Optional.of( false ) );

            assertEquals( expected, actual );
        }

        @Test
        public void convertBooleanPrimitiveTrueToJdkOpt_expectSomeTrue() {
            TypeConverter<Boolean, Optional<Boolean>> converter = cast( TypeConverterFactory.<Boolean, Optional<Boolean>>getConverterFor( JavaClass.BOOLEAN_PRIMITIVE, JavaClass.of( Optional.class, Boolean.class ) ).get() );
            Tryable<Optional<Boolean>> actual   = converter.convert( LONDON, true );
            Tryable<Optional<Boolean>> expected = Try.succeeded( Optional.of( true ) );

            assertEquals( expected, actual );
        }

        @Test
        public void convertBooleanObjectTrueToJdkOpt_expectSomeTrue() {
            TypeConverter<Boolean, Optional<Boolean>> converter = cast( TypeConverterFactory.<Boolean, Optional<Boolean>>getConverterFor( JavaClass.BOOLEAN_OBJECT, JavaClass.of( Optional.class, Boolean.class ) ).get() );
            Tryable<Optional<Boolean>> actual   = converter.convert( LONDON, true );
            Tryable<Optional<Boolean>> expected = Try.succeeded( Optional.of( true ) );

            assertEquals( expected, actual );
        }

        @Test
        public void convertBooleanObjectFalseToJdkOpt_expectSomeFalse() {
            TypeConverter<Boolean, Optional<Boolean>> converter = cast( TypeConverterFactory.getConverterFor( JavaClass.BOOLEAN_OBJECT, JavaClass.of( Optional.class, Boolean.class ) ).get() );
            Tryable<Optional<Boolean>> actual = converter.convert( LONDON, false );
            Tryable<Optional<Boolean>> expected = Try.succeeded( Optional.of( false ) );

            assertEquals( expected, actual );
        }

        @Test
        public void convertBooleanObjectNullToJdkOpt_expectNone() {
            TypeConverter<Boolean, Optional<Boolean>> converter = cast( TypeConverterFactory.getConverterFor( JavaClass.BOOLEAN_OBJECT, JavaClass.of( Optional.class, Boolean.class ) ).get() );
            Tryable<Optional<Boolean>> actual = converter.convert( LONDON, null );
            Tryable<Optional<Boolean>> expected = Try.succeeded( Optional.empty() );

            assertEquals( expected, actual );
        }


        @Test
        public void convertJDKOptFalse_expectFalse() {
            TypeConverter<Optional<Boolean>, Boolean> converter = cast( TypeConverterFactory.getConverterFor( JavaClass.of( Optional.class, Boolean.class ), JavaClass.BOOLEAN_OBJECT ).get() );
            Tryable<Boolean> actual = converter.convert( LONDON, Optional.of( false ) );
            Tryable<Boolean> expected = Try.succeeded( false );

            assertEquals( expected, actual );
        }

        @Test
        public void convertJDKOptTrue_expectTrue() {
            TypeConverter<Optional<Boolean>, Boolean> converter = cast( TypeConverterFactory.getConverterFor( JavaClass.of( Optional.class, Boolean.class ), JavaClass.BOOLEAN_OBJECT ).get() );
            Tryable<Boolean> actual = converter.convert( LONDON, Optional.of( true ) );
            Tryable<Boolean> expected = Try.succeeded( true );

            assertEquals( expected, actual );
        }

        @Test
        public void convertJDKOptNone_expectNull() {
            TypeConverter<Optional<Boolean>, Boolean> converter = cast( TypeConverterFactory.getConverterFor( JavaClass.of( Optional.class, Boolean.class ), JavaClass.BOOLEAN_OBJECT ).get() );
            Tryable<Boolean> actual = converter.convert( LONDON, Optional.empty() );
            Tryable<Boolean> expected = Try.succeeded( null );

            assertEquals( expected, actual );
        }
    }


    @Nested
    public class FPOptionTestCases {
        @Test
        public void convertBooleanPrimitiveFalseToFPOpt_expectSomeFalse() {
            TypeConverter<Boolean, FPOption<Boolean>> converter = cast( TypeConverterFactory.getConverterFor( JavaClass.BOOLEAN_PRIMITIVE, JavaClass.of( FPOption.class, Boolean.class ) ).get() );
            Tryable<FPOption<Boolean>> actual = converter.convert( LONDON, false );
            Tryable<FPOption<Boolean>> expected = Try.succeeded( FPOption.of( false ) );

            assertEquals( expected, actual );
        }

        @Test
        public void convertBooleanPrimitiveTrueToFPOpt_expectSomeTrue() {
            TypeConverter<Boolean, FPOption<Boolean>> converter = cast( TypeConverterFactory.getConverterFor( JavaClass.BOOLEAN_PRIMITIVE, JavaClass.of( FPOption.class, Boolean.class ) ).get() );
            Tryable<FPOption<Boolean>> actual = converter.convert( LONDON, true );
            Tryable<FPOption<Boolean>> expected = Try.succeeded( FPOption.of( true ) );

            assertEquals( expected, actual );
        }

        @Test
        public void convertBooleanObjectTrueToFPOpt_expectSomeTrue() {
            TypeConverter<Boolean, FPOption<Boolean>> converter = cast( TypeConverterFactory.getConverterFor( JavaClass.BOOLEAN_OBJECT, JavaClass.of( FPOption.class, Boolean.class ) ).get() );
            Tryable<FPOption<Boolean>> actual = converter.convert( LONDON, true );
            Tryable<FPOption<Boolean>> expected = Try.succeeded( FPOption.of( true ) );

            assertEquals( expected, actual );
        }

        @Test
        public void convertBooleanObjectFalseToFPOpt_expectSomeFalse() {
            TypeConverter<Boolean, FPOption<Boolean>> converter = cast( TypeConverterFactory.getConverterFor( JavaClass.BOOLEAN_OBJECT, JavaClass.of( FPOption.class, Boolean.class ) ).get() );
            Tryable<FPOption<Boolean>> actual = converter.convert( LONDON, false );
            Tryable<FPOption<Boolean>> expected = Try.succeeded( FPOption.of( false ) );

            assertEquals( expected, actual );
        }

        @Test
        public void convertBooleanObjectNullToFPOpt_expectNone() {
            TypeConverter<Boolean, FPOption<Boolean>> converter = cast( TypeConverterFactory.getConverterFor( JavaClass.BOOLEAN_OBJECT, JavaClass.of( FPOption.class, Boolean.class ) ).get() );
            Tryable<FPOption<Boolean>> actual = converter.convert( LONDON, null );
            Tryable<FPOption<Boolean>> expected = Try.succeeded( FPOption.none() );

            assertEquals( expected, actual );
        }

        @Test
        public void convertFPOptFalse_expectFalse() {
            TypeConverter<FPOption<Boolean>, Boolean> converter = cast( TypeConverterFactory.getConverterFor( JavaClass.of( FPOption.class, Boolean.class ), JavaClass.BOOLEAN_OBJECT ).get() );
            Tryable<Boolean> actual = converter.convert( LONDON, FPOption.of( false ) );
            Tryable<Boolean> expected = Try.succeeded( false );

            assertEquals( expected, actual );
        }

        @Test
        public void convertFPOptTrue_expectTrue() {
            TypeConverter<FPOption<Boolean>, Boolean> converter = cast( TypeConverterFactory.getConverterFor( JavaClass.of( FPOption.class, Boolean.class ), JavaClass.BOOLEAN_OBJECT ).get() );
            Tryable<Boolean> actual = converter.convert( LONDON, FPOption.of( true ) );
            Tryable<Boolean> expected = Try.succeeded( true );

            assertEquals( expected, actual );
        }

        @Test
        public void convertFPOptNone_expectNull() {
            TypeConverter<FPOption<Boolean>, Boolean> converter = cast( TypeConverterFactory.getConverterFor( JavaClass.of( FPOption.class, Boolean.class ), JavaClass.BOOLEAN_OBJECT ).get() );
            Tryable<Boolean> actual = converter.convert( LONDON, FPOption.none() );
            Tryable<Boolean> expected = Try.succeeded( null );

            assertEquals( expected, actual );
        }
    }


    @Nested
    public class DTMTestCases {
        @Test
        public void convertLondonStringToDTM_expectConversion() {
            Tryable<DTM> actual   = TypeConverterFactory.convert( LONDON, "18 Nov 2017, 16:11:54", JavaClass.STRING, JavaClass.DTM );
            Tryable<DTM> expected = Try.succeeded( new DTM(2017, 11, 18, 16, 11, 54) );

            assertEquals( expected, actual );
        }

        @Test
        public void convertRomeLongStringToDTM_expectConversion() {
            Tryable<DTM> actual   = TypeConverterFactory.convert( ROME.withI18nStyle(I18nDateTimeStyle.LONG),"18 novembre 2017 17:11:54 CET", JavaClass.STRING, JavaClass.DTM );
            Tryable<DTM> expected = Try.succeeded( new DTM(2017,11,18,  16,11,54) );

            assertEquals( expected, actual );
        }

        @Test
        public void convertDTMToLondonString_expectConversion() {
            Tryable<String> actual   = TypeConverterFactory.convert( LONDON,new DTM(2017,11,18,  16,11,54), JavaClass.DTM, JavaClass.STRING );
            Tryable<String> expected = Try.succeeded( "18 Nov 2017, 16:11:54" );

            assertEquals( expected, actual );
        }

        @Test
        public void convertDTOToRomeLongString_expectConversion() {
            Tryable<String> actual   = TypeConverterFactory.convert( ROME.withI18nStyle(I18nDateTimeStyle.LONG), new DTM(2017,11,18,  16,11,54), JavaClass.DTM, JavaClass.STRING );
            Tryable<String> expected = Try.succeeded( "18 novembre 2017 17:11:54 CET" );

            assertEquals( expected, actual );
        }
    }


    @Nested
    public class DayTestCases {
        @Test
        public void convertLondonStringToDay_expectConversion() {
            Tryable<Day> actual   = TypeConverterFactory.convert( LONDON, "18 Nov 2017", JavaClass.STRING, JavaClass.DAY );
            Tryable<Day> expected = Try.succeeded( new Day(2017, 11, 18) );

            assertEquals( expected, actual );
        }

        @Test
        public void convertRomeLongStringToDay_expectConversion() {
            Tryable<Day> actual   = TypeConverterFactory.convert( ROME.withI18nStyle(I18nDateTimeStyle.LONG),"18 novembre 2017", JavaClass.STRING, JavaClass.DAY );
            Tryable<Day> expected = Try.succeeded( new Day(2017,11,18) );

            assertEquals( expected, actual );
        }

        @Test
        public void convertDayToLondonString_expectConversion() {
            Tryable<String> actual   = TypeConverterFactory.convert( LONDON,new Day(2017,11,18), JavaClass.DAY, JavaClass.STRING );
            Tryable<String> expected = Try.succeeded( "18 Nov 2017" );

            assertEquals( expected, actual );
        }

        @Test
        public void convertDayToRomeLongString_expectConversion() {
            Tryable<String> actual   = TypeConverterFactory.convert( ROME.withI18nStyle(I18nDateTimeStyle.LONG), new Day(2017,11,18), JavaClass.DAY, JavaClass.STRING );
            Tryable<String> expected = Try.succeeded( "18 novembre 2017" );

            assertEquals( expected, actual );
        }
    }


    @Nested
    public class TimeTestCases {

    }


    public enum RGB {
        RED, GREEN, BLUE;
    }

    @Nested
    public class EnumTestCases {
        @Test
        public void convertStringToEnum() {
            Tryable<RGB> actual   = TypeConverterFactory.convert( LONDON, "GREEN", JavaClass.STRING, JavaClass.of(RGB.class) );
            Tryable<RGB> expected = Try.succeeded( RGB.GREEN );

            assertEquals( expected, actual );
        }

        @Test
        public void convertEnumToString() {
            Tryable<String> actual   = TypeConverterFactory.convert( LONDON, RGB.BLUE, JavaClass.of(RGB.class), JavaClass.STRING );
            Tryable<String> expected = Try.succeeded( "BLUE" );

            assertEquals( expected, actual );
        }
    }

    @Nested
    public class FileTestCases {
        @Test
        public void convertStringToFile() {
            Tryable<File> actual   = TypeConverterFactory.convert( LONDON, "a/b/c.txt", JavaClass.STRING, JavaClass.FILE );
            Tryable<File> expected = Try.succeeded( new File("a/b/c.txt") );

            assertEquals( expected, actual );
        }

        @Test
        public void convertBlankStringToFile() {
            Tryable<File> actual   = TypeConverterFactory.convert( LONDON, "", JavaClass.STRING, JavaClass.FILE );
            Tryable<File> expected = Try.succeeded( new File("") );

            assertEquals( expected, actual );
        }

        @Test
        public void convertFileToString() {
            Tryable<String> actual   = TypeConverterFactory.convert( LONDON, new File("a/b/c.txt"), JavaClass.FILE, JavaClass.STRING );
            Tryable<String> expected = Try.succeeded( "a/b/c.txt" );

            assertEquals( expected, actual );
        }
    }

    @Nested
    public class SecretTestCases {
        @Test
        public void convertStringToSecret() {
            Tryable<Secret> actual   = TypeConverterFactory.convert( LONDON, "12345", JavaClass.STRING, JavaClass.of(Secret.class) );
            Tryable<Secret> expected = Try.succeeded( new Secret("12345") );

            assertEquals( expected, actual );
        }

        @Test
        public void convertBlankStringToSecret() {
            Tryable<Secret> actual   = TypeConverterFactory.convert( LONDON, "", JavaClass.STRING, JavaClass.of(Secret.class) );
            Tryable<Secret> expected = Try.succeeded( new Secret("") );

            assertEquals( expected, actual );
        }

        @Test
        public void convertSecretToString() {
            // NB I am in two minds of whether to allow this path -- it could potentially
            // lead to a security vulnerability;  it depends on how it gets used...
            Tryable<String> actual   = TypeConverterFactory.convert( LONDON, new Secret("12345"), JavaClass.of(Secret.class), JavaClass.STRING );
            Tryable<String> expected = Try.succeeded( "12345" );

            assertEquals( expected, actual );
        }
    }


    @Nested
    public class MapTestCases {
// TWO GENERIC PARAMETERS
//    @Test
//    public void c() {
//        TypeConverter<Map,String> converter = cast( TypeConverterFactory.getConverterFor( JavaClass.of( Map.class,String.class,String.class), JavaClass.STRING ).get());
//        Tryable<String>           actual    = converter.convert( LONDON, Collections.emptyMap() );
//        Tryable<Map>              expected  = Try.succeeded(Collections.emptyMap());
//
//        assertEquals( expected, actual );
//    }
    }
}
