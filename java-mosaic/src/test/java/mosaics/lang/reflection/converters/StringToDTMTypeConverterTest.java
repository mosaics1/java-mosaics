//package mosaics.lang.reflection_old.converters;
//
//import com.softwaremosaic.fp.Failure;
//import mosaics.lang.reflection_old.JavaClass;
//import com.softwaremosaic.lang.time.DTM;
//import mosaics.lang.reflection_old.converters.I18nContext;
//import mosaics.lang.reflection_old.converters.StringToDTMTypeConverter;
//import mosaics.lang.reflection_old.converters.TypeConverter;
//import org.junit.jupiter.api.Nested;
//import org.junit.jupiter.api.Test;
//
//import java.text.ParseException;
//import java.util.Collections;
//
//import static mosaics.lang.reflection_old.converters.I18nContext.LONDON;
//import static mosaics.lang.reflection_old.converters.I18nContext.NEWYORK;
//import static mosaics.lang.reflection_old.converters.I18nContext.ROME;
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.jupiter.api.Assertions.assertNotEquals;
//import static org.junit.jupiter.api.Assertions.assertSame;
//
//
//public class StringToDTMTypeConverterTest {
//
//    private long whenMillis = DTM.toEpoch( 2017, 11, 18,  16, 11, 54 );
//
//
//    @Test
//    public void testHashCode() {
//        TypeConverter<String,Long> converter1 = new StringToDTMTypeConverter(JavaClass.LONG_PRIMITIVE.withAlias("DTM"));
//        TypeConverter<String,Long> converter2 = new StringToDTMTypeConverter(JavaClass.LONG_OBJECT.withAlias("DTM"));
//        TypeConverter<String,Long> converter3 = new StringToDTMTypeConverter(JavaClass.LONG_PRIMITIVE.withAlias("DTM"));
//
//        assertEquals( converter1.hashCode(), converter3.hashCode() );
//        assertNotEquals( converter1.hashCode(), converter2.hashCode() );
//    }
//
//    @Test
//    public void testEquals() {
//        TypeConverter<String,Long> converter1 = new StringToDTMTypeConverter(JavaClass.LONG_PRIMITIVE.withAlias("DTM"));
//        TypeConverter<String,Long> converter2 = new StringToDTMTypeConverter(JavaClass.LONG_OBJECT.withAlias("DTM"));
//        TypeConverter<String,Long> converter3 = new StringToDTMTypeConverter(JavaClass.LONG_PRIMITIVE.withAlias("DTM"));
//
//        assertEquals( converter1, converter3 );
//        assertNotEquals( converter1, converter2 );
//    }
//
//    @Test
//    public void convertToDefaultFormat() {
//        TypeConverter<Long,String> codec = createCodec();
//
//        assertDTMRoundTrip( codec, "18-Nov-2017 16:11:54", LONDON );
//        assertDTMRoundTrip( codec, "18-nov-2017 17.11.54", ROME );
//        assertDTMRoundTrip( codec, "Nov 18, 2017 11:11:54 AM", NEWYORK );
//    }
//
//    @Test
//    public void convertToBlankValue_expectException() {
//        TypeConverter<String,Long> converter = new StringToDTMTypeConverter(JavaClass.LONG_PRIMITIVE.withAlias("DTM"));
//
//        Failure f = converter.convert(LONDON, "  " ).getFailure();
//        assertEquals( ParseException.class, f.toException().getClass() );
//        assertEquals( 0, ((ParseException) f.toException()).getErrorOffset() );
//    }
//
//    @Test
//    public void reverseNull_expectItToDefaultToZero() {
//        TypeConverter<String,Long> converter = new StringToDTMTypeConverter(JavaClass.LONG_PRIMITIVE.withAlias("DTM"));
//
//        String actual = converter.reverse(LONDON, null).getResult();
//        assertEquals( "01-Jan-1970 01:00:00", actual );
//    }
//
//    @Test
//    public void convertToShortFormat() {
//        TypeConverter<Long,String> codec = createCodec("short");
//
//        assertDTM( codec, "18/11/17 16:11", LONDON );
//        assertDTM( codec, "18/11/17 17.11", ROME );
//        assertDTM( codec, "11/18/17 11:11 AM", NEWYORK);
//    }
//
//    @Test
//    public void convertToIsoFormat() {
//        TypeConverter<Long,String> codec = createCodec("iso");
//
//        assertDTM( codec, "2017-11-18T16:11:54", LONDON );
//        assertDTM( codec, "2017-11-18T17:11:54", ROME );
//        assertDTM( codec, "2017-11-18T11:11:54", NEWYORK);
//    }
//
//    @Test
//    public void convertToMedFormat() {
//        TypeConverter<Long,String> codec = createCodec("med");
//
//        assertDTMRoundTrip( codec, "18-Nov-2017 16:11:54", LONDON );
//        assertDTMRoundTrip( codec, "18-nov-2017 17.11.54", ROME );
//        assertDTMRoundTrip( codec, "Nov 18, 2017 11:11:54 AM", NEWYORK );
//    }
//
//    @Test
//    public void convertToMediumFormat() {
//        TypeConverter<Long,String> codec = createCodec("medium");
//
//        assertDTMRoundTrip( codec, "18-Nov-2017 16:11:54", LONDON );
//        assertDTMRoundTrip( codec, "18-nov-2017 17.11.54", ROME );
//        assertDTMRoundTrip( codec, "Nov 18, 2017 11:11:54 AM", NEWYORK );
//    }
//
//    @Test
//    public void convertToLongFormat() {
//        TypeConverter<Long,String> codec = createCodec("long");
//
//        assertDTM( codec,"18 November 2017 16:11:54 BT", LONDON );
//        assertDTMRoundTrip( codec,"18 novembre 2017 17.11.54 CET", ROME );
//        assertDTMRoundTrip( codec,"November 18, 2017 11:11:54 AM ET", NEWYORK );
//    }
//
//    @Test
//    public void convertToFullFormat() {
//        TypeConverter<Long,String> codec = createCodec("full");
//
//        assertDTM( codec, "Saturday, 18 November 2017 16:11:54 o'clock BT", LONDON );
//        assertDTMRoundTrip( codec, "sabato 18 novembre 2017 17.11.54 CET", ROME );
//        assertDTMRoundTrip( codec, "Saturday, November 18, 2017 11:11:54 AM ET", NEWYORK );
//    }
//
//    @Test
//    public void convertToSpecifiedFormat() {
//        TypeConverter<Long,String> codec = createCodec("yyyy-MM-dd HH:mm:ss");
//
//        assertDTMRoundTrip( codec,"2017-11-18 16:11:54", LONDON );
//        assertDTMRoundTrip( codec,"2017-11-18 17:11:54", ROME );
//        assertDTMRoundTrip( codec,"2017-11-18 11:11:54", NEWYORK );
//    }
//
//
//    @Nested
//    public class ReverseTestCases {
//        @Test
//        public void convertToLongFormat() {
//            TypeConverter<String,Long> converter         = new StringToDTMTypeConverter(JavaClass.LONG_PRIMITIVE.withAlias("DTM"));
//            TypeConverter<Long,String> reversedConverter = converter.reverse();
//
//            assertEquals( converter.getDestType(), reversedConverter.getSourceType() );
//            assertEquals( converter.getSourceType(), reversedConverter.getDestType() );
//            assertSame( converter, reversedConverter.reverse() );
//        }
//    }
//
//    private TypeConverter<Long,String> createCodec() {
//        return createCodec(null);
//    }
//
//    private TypeConverter<Long,String> createCodec( String type ) {
//        JavaClass<Long> dtmType = JavaClass.LONG_PRIMITIVE.withAlias( "DTM" );
//
//        if ( type != null ) {
//            dtmType = dtmType.withTypeArgs( Collections.singletonList( type ) );
//        }
//
//        return new StringToDTMTypeConverter( dtmType ).reverse();
//    }
//
//
//    private void assertDTMRoundTrip( TypeConverter<Long,String> codec, String txt, I18nContext ctx ) {
//        assertEquals(txt, codec.convert(ctx, whenMillis).getResult());
//        assertEquals(whenMillis, codec.reverse(ctx, txt).getResult().longValue());
//    }
//
//    private void assertDTM( TypeConverter<Long,String> codec, String txt, I18nContext ctx ) {
//        assertEquals(txt, codec.convert(ctx, whenMillis).getResult());
//    }
//
//}
