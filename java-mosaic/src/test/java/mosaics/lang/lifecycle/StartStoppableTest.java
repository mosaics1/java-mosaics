package mosaics.lang.lifecycle;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;


public class StartStoppableTest {
    private final List<String> audit = new ArrayList<>();

    @Nested
    public class IsRunningTestCases {
        @Test
        public void givenNewService_callIsRunning_expectFalse() {
            FakeService s1 = new FakeService( "s1" );

            List<String> expectedAudit = Collections.emptyList();

            assertFalse( s1.isRunning() );
            assertEquals( expectedAudit, audit );
        }

        @Test
        public void givenNewService_callStart_expectServiceToStart() {
            FakeService s1 = new FakeService( "s1" );

            s1.start();


            List<String> expectedAudit = Collections.singletonList(
                "s1.doStart()"
            );

            assertTrue( s1.isRunning() );
            assertEquals( expectedAudit, audit );
        }

        @Test
        public void givenNewService_callStartTwice_expectServiceToStartOnce() {
            FakeService s1 = new FakeService( "s1" );

            s1.start();
            s1.start();


            List<String> expectedAudit = Collections.singletonList( "s1.doStart()" );

            assertTrue( s1.isRunning() );
            assertEquals( expectedAudit, audit );
        }


        @Test
        public void givenRunningService_callStop_expectServiceToShutdown() {
            FakeService s1 = new FakeService( "s1" );

            s1.start();
            s1.stop();


            List<String> expectedAudit = asList(
                "s1.doStart()",
                "s1.doStop()"
            );

            assertFalse( s1.isRunning() );
            assertEquals( expectedAudit, audit );
        }
    }

    @Nested
    public class GivedChainedServicesOnCreationTestCases {
        @Test
        public void givenDependentService_callStart_expectStartupToCascade() {
            FakeService s4 = new FakeService( "s4" );
            FakeService s3 = new FakeService( "s3" );
            FakeService s2 = new FakeService( "s2" );
            FakeService s1 = new FakeService( "s1" );

            s4.registerServicesBefore( s2, s3 );
            s2.registerServicesBefore( s1 );


            s4.start();


            assertTrue( s1.isRunning() );
            assertTrue( s2.isRunning() );
            assertTrue( s3.isRunning() );
            assertTrue( s4.isRunning() );

            List<String> expectedAudit = asList(
                "s1.doStart()",
                "s2.doStart()",
                "s3.doStart()",
                "s4.doStart()"
            );

            assertEquals( expectedAudit, audit );
            assertEquals( List.of(s1), s1.getStartChainOrder() );
            assertEquals( List.of(s1,s2), s2.getStartChainOrder() );
            assertEquals( List.of(s3), s3.getStartChainOrder() );
            assertEquals( List.of(s2,s3,s4), s4.getStartChainOrder() );
        }

        @Test
        public void givenDependentServices_callStop_expectStopToCascade() {
            FakeService s4 = new FakeService( "s4" );
            FakeService s3 = new FakeService( "s3" );
            FakeService s2 = new FakeService( "s2" );
            FakeService s1 = new FakeService( "s1" );

            s4.registerServicesBefore( s2, s3 );
            s2.registerServicesBefore( s1 );


            s4.start();
            s4.stop();


            assertFalse( s1.isRunning() );
            assertFalse( s2.isRunning() );
            assertFalse( s3.isRunning() );
            assertFalse( s4.isRunning() );

            List<String> expectedAudit = asList(
                "s1.doStart()",
                "s2.doStart()",
                "s3.doStart()",
                "s4.doStart()",
                "s4.doStop()",
                "s3.doStop()",
                "s2.doStop()",
                "s1.doStop()"
            );

            assertEquals( expectedAudit, audit );
        }

        @Test
        public void givenDependentServices_callStartAndStopTwice_expectStartStopToOnlyBeCalledOnce() {
            FakeService s4 = new FakeService( "s4" );
            FakeService s3 = new FakeService( "s3" );
            FakeService s2 = new FakeService( "s2" );
            FakeService s1 = new FakeService( "s1" );

            s4.registerServicesBefore( s2, s3 );
            s2.registerServicesBefore( s1 );


            s4.start();
            s4.start();
            s4.stop();
            s4.stop();


            assertFalse( s1.isRunning() );
            assertFalse( s2.isRunning() );
            assertFalse( s3.isRunning() );
            assertFalse( s4.isRunning() );

            List<String> expectedAudit = asList(
                "s1.doStart()",
                "s2.doStart()",
                "s3.doStart()",
                "s4.doStart()",
                "s4.doStop()",
                "s3.doStop()",
                "s2.doStop()",
                "s1.doStop()"
            );

            assertEquals( expectedAudit, audit );
        }

        @Test
        public void givenChainedService_callStart_expectStartupToCascade() {
            FakeService s4 = new FakeService( "s4" );
            FakeService s3 = new FakeService( "s3" );
            FakeService s2 = new FakeService( "s2" );
            FakeService s1 = new FakeService( "s1" );

            s1.registerServicesAfter( s2, s4 );
            s2.registerServicesAfter( s3 );


            s1.start();


            assertTrue( s1.isRunning() );
            assertTrue( s2.isRunning() );
            assertTrue( s3.isRunning() );
            assertTrue( s4.isRunning() );

            List<String> expectedAudit = asList(
                "s1.doStart()",
                "s2.doStart()",
                "s3.doStart()",
                "s4.doStart()"
            );

            assertEquals( expectedAudit, audit );
            assertEquals( List.of(s1,s2,s4), s1.getStartChainOrder() );
            assertEquals( List.of(s2,s3), s2.getStartChainOrder() );
            assertEquals( List.of(s3), s3.getStartChainOrder() );
            assertEquals( List.of(s4), s4.getStartChainOrder() );
        }

        @Test
        public void givenChainedServices_callStop_expectStopToCascade() {
            FakeService s4 = new FakeService( "s4" );
            FakeService s3 = new FakeService( "s3" );
            FakeService s2 = new FakeService( "s2" );
            FakeService s1 = new FakeService( "s1" );

            s1.registerServicesAfter( s2, s4 );
            s2.registerServicesAfter( s3 );


            s1.start();
            s1.stop();


            assertFalse( s1.isRunning() );
            assertFalse( s2.isRunning() );
            assertFalse( s3.isRunning() );
            assertFalse( s4.isRunning() );

            List<String> expectedAudit = asList(
                "s1.doStart()",
                "s2.doStart()",
                "s3.doStart()",
                "s4.doStart()",
                "s4.doStop()",
                "s3.doStop()",
                "s2.doStop()",
                "s1.doStop()"
            );

            assertEquals( expectedAudit, audit );
        }

        @Test
        public void givenChainedServices_callStartAndStopTwice_expectStartStopToOnlyBeCalledOnce() {
            FakeService s4 = new FakeService( "s4" );
            FakeService s3 = new FakeService( "s3" );
            FakeService s2 = new FakeService( "s2" );
            FakeService s1 = new FakeService( "s1" );

            s1.registerServicesAfter( s2, s4 );
            s2.registerServicesAfter( s3 );


            s1.start();
            s1.start();
            s1.stop();
            s1.stop();


            assertFalse( s1.isRunning() );
            assertFalse( s2.isRunning() );
            assertFalse( s3.isRunning() );
            assertFalse( s4.isRunning() );

            List<String> expectedAudit = asList(
                "s1.doStart()",
                "s2.doStart()",
                "s3.doStart()",
                "s4.doStart()",
                "s4.doStop()",
                "s3.doStop()",
                "s2.doStop()",
                "s1.doStop()"
            );

            assertEquals( expectedAudit, audit );
        }

        @Test
        public void givenOn2OfEachStartStopFunctions_expectThemToBeInvokedWhenStoppingStartingService() {
            FakeService s4 = new FakeService( "s4" );
            FakeService s3 = new FakeService( "s3" );
            FakeService s2 = new FakeService( "s2" );
            FakeService s1 = new FakeService( "s1" );

            s1.registerServicesAfter( s2, s4 );
            s2.registerServicesAfter( s3 );

            s2.onStartBefore( () -> audit.add( "s2 onStartBefore1" ) );
            s2.onStartBefore( () -> audit.add( "s2 onStartBefore2" ) );

            s2.onStartAfter( () -> audit.add( "s2 onStartAfter1" ) );
            s2.onStartAfter( () -> audit.add( "s2 onStartAfter2" ) );

            s2.onStopBefore( () -> audit.add( "s2 onStopBefore1" ) );
            s2.onStopBefore( () -> audit.add( "s2 onStopBefore2" ) );

            s2.onStopAfter( () -> audit.add( "s2 onStopAfter1" ) );
            s2.onStopAfter( () -> audit.add( "s2 onStopAfter2" ) );


            s1.start();
            s1.start();
            s1.stop();
            s1.stop();


            assertFalse( s1.isRunning() );
            assertFalse( s2.isRunning() );
            assertFalse( s3.isRunning() );
            assertFalse( s4.isRunning() );

            List<String> expectedAudit = asList(
                "s1.doStart()",
                "s2 onStartBefore1",
                "s2 onStartBefore2",
                "s2.doStart()",
                "s2 onStartAfter1",
                "s2 onStartAfter2",
                "s3.doStart()",
                "s4.doStart()",
                "s4.doStop()",
                "s3.doStop()",
                "s2 onStopBefore1",
                "s2 onStopBefore2",
                "s2.doStop()",
                "s2 onStopAfter1",
                "s2 onStopAfter2",
                "s1.doStop()"
            );

            assertEquals( expectedAudit, audit );
        }
    }

    @Nested
    public class GivenDependencyChangesWhileRunningTestCases {
        @Test
        public void addServiceToAServiceThatIsAlreadyRunning_expectChildServiceToStartStraightAway() {
            FakeService s1 = new FakeService( "s1" );
            FakeService s2 = new FakeService( "s2" );

            s1.start();


            s1.registerServicesBefore( s2 );

            assertTrue( s1.isRunning() );
            assertTrue( s2.isRunning() );

            List<String> expectedAudit = asList(
                "s1.doStart()",
                "s2.doStart()"
            );

            assertEquals( expectedAudit, audit );
        }

        @Test
        public void unsubscribeServiceWhileParentIsStillRunning_expectItToStop() {
            FakeService s1 = new FakeService( "s1" );
            FakeService s2 = new FakeService( "s2" );

            s1.start();


            Subscription sub = s1.registerServicesBefore( s2 );
            sub.cancel();

            assertTrue( s1.isRunning() );
            assertFalse( s2.isRunning() );

            List<String> expectedAudit = asList(
                "s1.doStart()",
                "s2.doStart()",
                "s2.doStop()"
            );

            assertEquals( expectedAudit, audit );
            assertEquals( List.of(s1), s1.getStartChainOrder() );
            assertEquals( List.of(s2), s2.getStartChainOrder() );
        }
    }

    @Test
    public void subscribeViaConstructor() {
        FakeService s2 = new FakeService( "s2" );
        FakeService s1 = new FakeService( "s1", s2 );

        s1.start();


        assertTrue( s1.isRunning() );
        assertTrue( s2.isRunning() );

        List<String> expectedAudit = asList(
            "s2.doStart()",
            "s1.doStart()"
        );

        assertEquals( expectedAudit, audit );
    }

    @Nested
    public class ChainedServiceSubscriptionTestCases {
        @Test
        public void givenSubscribedBefore_callCancel_expectNoCallsToDependent() {
            FakeService s2 = new FakeService( "s2" );
            FakeService s1 = new FakeService( "s1" );

            Subscription subscription = s1.registerServicesBefore( s2 );

            assertTrue( subscription.isActive() );
            subscription.cancel();

            assertFalse( subscription.isActive() );

            s1.start();


            assertTrue( s1.isRunning() );
            assertFalse( s2.isRunning() );

            List<String> expectedAudit = Collections.singletonList(
                "s1.doStart()"
            );

            assertEquals( expectedAudit, audit );
        }

        @Test
        public void givenMultipleSubscribedBefore_callCancel_expectNoCallsToDependent() {
            FakeService s3 = new FakeService( "s3" );
            FakeService s2 = new FakeService( "s2" );
            FakeService s1 = new FakeService( "s1" );

            Subscription subscription1 = s1.registerServicesBefore( s2 );
            Subscription subscription2 = s1.registerServicesBefore( s3 );

            assertTrue( subscription1.isActive() );
            assertTrue( subscription2.isActive() );
            subscription1.cancel();

            assertFalse( subscription1.isActive() );
            assertTrue( subscription2.isActive() );

            s1.start();


            assertTrue( s1.isRunning() );
            assertFalse( s2.isRunning() );
            assertTrue( s3.isRunning() );

            List<String> expectedAudit = asList(
                "s3.doStart()",
                "s1.doStart()"
            );

            assertEquals( expectedAudit, audit );
        }

        @Test
        public void givenSubscribedAfter_callCancel_expectNoCallsToDependent() {
            FakeService s2 = new FakeService( "s2" );
            FakeService s1 = new FakeService( "s1" );

            Subscription subscription = s1.registerServicesAfter( s2 );

            assertTrue( subscription.isActive() );
            subscription.cancel();

            assertFalse( subscription.isActive() );

            s1.start();


            assertTrue( s1.isRunning() );
            assertFalse( s2.isRunning() );

            List<String> expectedAudit = Collections.singletonList(
                "s1.doStart()"
            );

            assertEquals( expectedAudit, audit );
        }
    }

    @Nested
    public class CallbackTestCases {
       @Test
       public void givenServiceWithCallback_callStartStop_expectStartStopCallbacksToBeCalledInOrder() {
           FakeService s1 = new FakeService( "s1" ).withCallback( new AuditingCallback() );

           s1.start();
           s1.stop();

           List<String> expected = List.of(
               "starting s1",
               "s1.doStart()",
               "started s1",
               "stopping s1",
               "s1.doStop()",
               "stopped s1"
           );

           assertEquals( expected, audit );
       }

       @Test
       public void givenServiceWithChainedBeforeService_callStartStopWithCallback_expectCallbackToHearBothServices() {
           FakeService s2 = new FakeService( "s2" );
           FakeService s1 = new FakeService( "s1" ).withCallback( new AuditingCallback() );

           s1.registerServicesBefore( s2 );

           s1.start();
           s1.stop();

           List<String> expected = List.of(
               "starting s1",
               "starting s2",
               "s2.doStart()",
               "started s2",
               "s1.doStart()",
               "started s1",
               "stopping s1",
               "s1.doStop()",
               "stopping s2",
               "s2.doStop()",
               "stopped s2",
               "stopped s1"
           );

           assertEquals( expected, audit );
       }

       @Test
       public void givenServiceWithChainedAfterService_callStartStopWithCallback_expectCallbackToHearBothServices() {
           FakeService s2 = new FakeService( "s2" );
           FakeService s1 = new FakeService( "s1" ).withCallback( new AuditingCallback() );

           s1.registerServicesAfter( s2 );

           s1.start();
           s1.stop();

           List<String> expected = List.of(
               "starting s1",
               "s1.doStart()",
               "starting s2",
               "s2.doStart()",
               "started s2",
               "started s1",
               "stopping s1",
               "stopping s2",
               "s2.doStop()",
               "stopped s2",
               "s1.doStop()",
               "stopped s1"
           );

           assertEquals( expected, audit );
       }

       @Test
       public void givenServiceWithAfterServiceRegisteredBeforeCallback_callStartStop_expectCallbackToHearBothServices() {
           FakeService s2 = new FakeService( "s2" );
           FakeService s1 = new FakeService( "s1" );

           s1.registerServicesAfter( s2 );
           s1.withCallback( new AuditingCallback() );

           s1.start();
           s1.stop();

           List<String> expected = List.of(
               "starting s1",
               "s1.doStart()",
               "starting s2",
               "s2.doStart()",
               "started s2",
               "started s1",
               "stopping s1",
               "stopping s2",
               "s2.doStop()",
               "stopped s2",
               "s1.doStop()",
               "stopped s1"
           );

           assertEquals( expected, audit );
       }

       @Test
       public void givenServiceWithAfterServiceRegisteredAfterCallback_callStartStop_expectCallbackToHearBothServices() {
           FakeService s2 = new FakeService( "s2" );
           FakeService s1 = new FakeService( "s1" );

           s1.withCallback( new AuditingCallback() );
           s1.registerServicesAfter( s2 );

           s1.start();
           s1.stop();

           List<String> expected = List.of(
               "starting s1",
               "s1.doStart()",
               "starting s2",
               "s2.doStart()",
               "started s2",
               "started s1",
               "stopping s1",
               "stopping s2",
               "s2.doStop()",
               "stopped s2",
               "s1.doStop()",
               "stopped s1"
           );

           assertEquals( expected, audit );
       }

       @Test
       public void givenServiceWithRemovedCallback_callStartStop_expectNoCallbackCalls() {
           FakeService s1 = new FakeService( "s1" );

           AuditingCallback callback = new AuditingCallback();
           s1.withCallback( callback );

           assertTrue(s1.removeCallback(callback));

           s1.start();
           s1.stop();

           List<String> expected = List.of(
               "s1.doStart()", "s1.doStop()"
           );

           assertEquals( expected, audit );
       }

       @Test
       public void givenChainedServiceWithRemovedCallbackAtRootService_callStartStop_expectNoCallbackCalls() {
           FakeService s2 = new FakeService( "s2" );
           FakeService s1 = new FakeService( "s1" );

           AuditingCallback callback = new AuditingCallback();
           s1.withCallback( callback );
           s1.registerServicesAfter( s2 );

           assertTrue(s1.removeCallback(callback));

           s1.start();
           s1.stop();

           List<String> expected = List.of(
               "s1.doStart()", "s2.doStart()", "s2.doStop()", "s1.doStop()"
           );

           assertEquals( expected, audit );
       }

       @Test
       public void givenChainedServiceWithRemovedCallbackAtLeafService_callStartStop_expectNoCallbackCallsFromLeaf() {
           FakeService s2 = new FakeService( "s2" );
           FakeService s1 = new FakeService( "s1" );

           AuditingCallback callback = new AuditingCallback();
           s1.withCallback( callback );
           s1.registerServicesAfter( s2 );

           assertTrue(s2.removeCallback(callback));

           s1.start();
           s1.stop();

           List<String> expected = List.of(
               "starting s1",
               "s1.doStart()",
               "s2.doStart()",
               "started s1",
               "stopping s1",
               "s2.doStop()",
               "s1.doStop()",
               "stopped s1"
           );

           assertEquals( expected, audit );
       }

    }

    @Nested
    public class GCTestCases {
        @Test
        @SuppressWarnings("UnusedAssignment")
        public void expectNoLogMessageWhenCollectedAndObjectIsNotEverStarted() throws InterruptedException {
            CountDownLatch gcLatch = new CountDownLatch( 1 );

            GCSensitiveService service = new GCSensitiveService( gcLatch ).withCallback( new AuditingCallback() );

            service = null;

            System.gc();

            assertTrue( gcLatch.await( 10, TimeUnit.SECONDS ) );
            assertEquals( List.of("gcSensitiveService has been finalized"), audit );
        }

        @Test
        @SuppressWarnings("UnusedAssignment")
        public void expectNoLogMessageWhenCollectedAndObjectIsShutdownAfterUse() throws InterruptedException {
            CountDownLatch gcLatch = new CountDownLatch( 1 );

            GCSensitiveService service = new GCSensitiveService( gcLatch ).withCallback( new AuditingCallback() );

            service.start();
            service.stop();
            audit.clear();

            service = null;


            System.gc();

            assertTrue( gcLatch.await( 10, TimeUnit.SECONDS ) );
            assertEquals( List.of("gcSensitiveService has been finalized"), audit );
        }
    }

    private class GCSensitiveService extends StartStoppable<GCSensitiveService> {
        public final CountDownLatch gcLatch;

        public GCSensitiveService( CountDownLatch gcLatch ) {
            super( "gcSensitiveService" );

            this.gcLatch = gcLatch;
        }

        @Override
        @SuppressWarnings("deprecation")
        protected void finalize() throws Throwable {
            super.finalize();

            audit.add( getName()+" has been finalized" );
            gcLatch.countDown();
        }
    }

    @SuppressWarnings("unchecked")
    private class FakeService extends StartStoppable<FakeService> {
        public FakeService(String serviceName) {
            super( serviceName );
        }

        public FakeService(String serviceName, Object dep) {
            super( serviceName );

            this.registerServicesBefore( dep );
        }

        protected void doStart() {
            audit.add( getName()+".doStart()" );
        }

        protected void doStop() {
            audit.add( getName()+".doStop()" );
        }
    }

    private class AuditingCallback implements StartStoppableCallback {
        public void starting( StartStoppable service ) {
            audit.add("starting " + service.getName());
        }

        public void started( StartStoppable service ) {
            audit.add("started " + service.getName());
        }

        public void stopping( StartStoppable service ) {
            audit.add("stopping " + service.getName());
        }

        public void stopped( StartStoppable service ) {
            audit.add("stopped " + service.getName());
        }
    }
}
