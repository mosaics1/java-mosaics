package mosaics.lang;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;


public class RandomUtilsTest {

    @Test
    public void rndInt() {
        int rnd = RandomUtils.rnd( 10 );

        assertTrue( rnd >= 0 );
        assertTrue( rnd < 10 );
    }

}
