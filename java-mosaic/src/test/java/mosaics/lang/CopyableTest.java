package mosaics.lang;

import lombok.Value;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class CopyableTest {
    @Test
    public void testCopy() {
        Robot orig  = new Robot("Evie");
        Robot clone = orig.copy();

        assertEquals( orig, clone );
        assertTrue( orig != clone );
    }



    @Value
    public static class Robot implements Copyable<Robot> {
        private String name;
    }
}
