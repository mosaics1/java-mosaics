package mosaics.lang.refcount;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class RefCounterProdModeTest extends BaseRefCounterTestCases {
    protected RefCounter<Resource> createRefCount() {
        return new RefCounter<>( resource, Resource::close );
    }

    @Nested
    public class ShowThatDebugAssertionsAreTurnedOfTestCases {
        @Test
        public void givenNewManager_callDecTwice_expectNoException() {
            refCount.decRefCount();
            refCount.decRefCount();

            assertEquals( -1, refCount.getRefCount() );
        }

        @Test
        public void givenReleasedResource_callInc_expectNoException() {
            refCount.decRefCount();
            refCount.incRefCount();

            assertEquals( 1L, refCount.getRefCount() );
        }
    }
}
