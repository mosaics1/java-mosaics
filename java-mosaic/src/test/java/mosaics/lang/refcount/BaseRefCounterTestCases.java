package mosaics.lang.refcount;

import lombok.AllArgsConstructor;
import lombok.Getter;
import mosaics.fp.FP;
import mosaics.junit.JMAssertions;
import mosaics.lang.TryUtils;
import mosaics.lang.lifecycle.Subscription;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;


@SuppressWarnings("unused")
public abstract class BaseRefCounterTestCases {

    protected Resource              resource = new Resource();
    protected RefCounter<Resource> refCount = createRefCount();


    protected abstract RefCounter<Resource> createRefCount();


    @Nested
    public class RefCountTestCases {
        @Test
        public void givenNewManager_expectRefCountToBeOne() {
            assertEquals( 1, refCount.getRefCount() );
        }

        @Test
        public void givenNewManager_expectResourceToBeActive() {
            assertEquals( 0, resource.getCloseCount() );
        }

        @Test
        public void givenNewManager_callDecRefCount_expectRefCountToBeZero() {
            refCount.decRefCount();

            assertEquals( 0, refCount.getRefCount() );
        }

        @Test
        public void givenNewManager_callDecRefCount_expectResourceToNoLongerBeRunning() {
            refCount.decRefCount();

            assertEquals( 1, resource.getCloseCount() );
        }

        @Test
        public void givenNewManager_callIncRefCount_expectRefCountToBeTwo() {
            refCount.incRefCount();

            assertEquals( 2, refCount.getRefCount() );
        }

        @Test
        public void givenRefCountOfTwo_callDecRefCount_expectRefCountOfOne() {
            refCount.incRefCount();
            refCount.decRefCount();

            assertEquals( 1, refCount.getRefCount() );
        }

        @Test
        public void givenRefCountOfTwo_callIncRefCount_expectRefCountOfThree() {
            refCount.incRefCount();
            refCount.incRefCount();

            assertEquals( 3, refCount.getRefCount() );
        }
    }

    @Nested
    public class OnIncCallbackTestCases {
        @Nested
        public class FunctionalTestCases {
            @Test
            public void givenOnIncCallback_callIncRef_expectCallbackToBeCalled() {
                AtomicLong callbackCounter = new AtomicLong();

                refCount.onRefIncInvoke( ( refCount, r ) -> {
                    assertEquals( 2, refCount );
                    assertSame( r, resource );

                    callbackCounter.incrementAndGet();
                } );

                refCount.incRefCount();

                assertEquals( 1, callbackCounter.get() );
            }

            @Test
            public void givenOnIncCallback_callIncRefTwice_expectCallbackToBeCalledTwice() {
                List<Long> callbackValues = new Vector<>();

                refCount.onRefIncInvoke( ( refCount, r ) -> {
                    callbackValues.add( refCount );

                    assertSame( r, resource );
                } );

                refCount.incRefCount();
                refCount.incRefCount();

                assertEquals( Arrays.asList( 2L, 3L ), callbackValues );
            }

            @Test
            public void givenOnIncCallback_callDecRef_expectCallbackToNotBeCalled() {
                AtomicLong callbackCounter = new AtomicLong();

                refCount.onRefIncInvoke( ( refCount, r ) -> callbackCounter.incrementAndGet() );

                refCount.decRefCount();

                assertEquals( 0, callbackCounter.get() );
            }

            @Test
            public void givenTwoOnIncCallbacks_invokeInc_expectTwoCallbacks() {
                AtomicLong callbackCounter = new AtomicLong();

                refCount.onRefIncInvoke( ( refCount, r ) -> callbackCounter.incrementAndGet() );

                refCount.incRefCount();
                refCount.incRefCount();

                assertEquals( 2, callbackCounter.get() );
            }
        }

        @Nested
        public class SubscriptionRelatedTestCases {
            @Test
            public void givenCancelledOnIncCallback_invokeInc_expectCancelledCallbackToNotBeInvoked() {
                AtomicLong callbackCounter = new AtomicLong();

                Subscription sub = refCount.onRefIncInvoke( ( refCount, r ) -> callbackCounter.incrementAndGet() );

                assertTrue( sub.isActive() );

                sub.cancel();

                assertFalse( sub.isActive() );

                refCount.incRefCount();

                assertEquals( 0, callbackCounter.get() );
            }

            @Test
            public void givenTwoOnIncCallbacks_cancelOneAndInvokeInc_expectOnlyOneCallback() {
                AtomicLong callbackCounter1 = new AtomicLong();
                AtomicLong callbackCounter2 = new AtomicLong();

                Subscription sub1 = refCount.onRefIncInvoke( ( refCount, r ) -> callbackCounter1.incrementAndGet() );
                Subscription sub2 = refCount.onRefIncInvoke( ( refCount, r ) -> callbackCounter2.incrementAndGet() );

                assertTrue( sub1.isActive() );
                assertTrue( sub2.isActive() );

                sub1.cancel();

                assertFalse( sub1.isActive() );
                assertTrue( sub2.isActive() );

                refCount.incRefCount();

                assertEquals( 0, callbackCounter1.get() );
                assertEquals( 1, callbackCounter2.get() );
            }

            @Test
            public void givenCancelledOnIncCallback_dropReferenceToResource_expectTheResourceToBeGCedEvenThoughAReferenceToTheCancelledSubExists() {
                AtomicLong callbackCounter = new AtomicLong();

                Subscription sub = refCount.onRefIncInvoke( ( refCount, r ) -> callbackCounter.incrementAndGet() );

                sub.cancel();

                Reference weakRef = new WeakReference<>( refCount );
                refCount = null;

                JMAssertions.spinUntilReleased( weakRef );
            }
        }
    }

    @Nested
    public class OnDecCallbackTestCases {
        @Nested
        public class RefCountTestCases {
            @Test
            public void givenOnDecCallback_callDecRef_expectCallbackToBeCalled() {
                AtomicLong callbackCounter = new AtomicLong();

                refCount.onRefDecInvoke( (refCount,r) -> {
                    assertEquals( 0, refCount );
                    assertSame( r, resource );

                    callbackCounter.incrementAndGet();
                } );

                refCount.decRefCount();

                assertEquals( 1, callbackCounter.get() );
            }

            @Test
            public void givenOnDecCallback_callDecRefTwice_expectCallbackToBeCalledTwice() {
                List<Long> callbackValues = new Vector<>();

                refCount.onRefDecInvoke( (refCount,r) -> {
                    callbackValues.add( refCount );

                    assertSame( r, resource );
                } );

                refCount.incRefCount();
                refCount.decRefCount();
                refCount.decRefCount();

                assertEquals( Arrays.asList(1L,0L), callbackValues );
            }

            @Test
            public void givenOnDecCallback_callIncRef_expectCallbackToNotBeCalled() {
                AtomicLong callbackCounter = new AtomicLong();

                refCount.onRefDecInvoke( (refCount,r) -> callbackCounter.incrementAndGet() );

                refCount.incRefCount();

                assertEquals( 0, callbackCounter.get() );
            }
        }

        @Nested
        public class SubscriptionRelatedTestCases {
            @Test
            public void givenTwoOnDecCallbacks_invokeDec_expectTwoCallbacks() {
                AtomicLong callbackCounter = new AtomicLong();

                refCount.onRefDecInvoke( ( refCount, r ) -> callbackCounter.incrementAndGet() );
                refCount.onRefDecInvoke( ( refCount, r ) -> callbackCounter.incrementAndGet() );

                refCount.decRefCount();

                assertEquals( 2, callbackCounter.get() );
            }

            @Test
            public void givenCancelledOnDecCallback_invokeDec_expectCancelledCallbackToNotBeInvoked() {
                AtomicLong callbackCounter = new AtomicLong();

                Subscription sub = refCount.onRefDecInvoke( ( refCount, r ) -> callbackCounter.decrementAndGet() );

                assertTrue( sub.isActive() );

                sub.cancel();

                assertFalse( sub.isActive() );

                refCount.decRefCount();

                assertEquals( 0, callbackCounter.get() );
            }

            @Test
            public void givenTwoOnDecCallbacks_cancelOneAndInvokeDec_expectOnlyOneCallback() {
                AtomicLong callbackCounter1 = new AtomicLong();
                AtomicLong callbackCounter2 = new AtomicLong();

                Subscription sub1 = refCount.onRefDecInvoke( ( refCount, r ) -> callbackCounter1.incrementAndGet() );
                Subscription sub2 = refCount.onRefDecInvoke( ( refCount, r ) -> callbackCounter2.incrementAndGet() );

                assertTrue( sub1.isActive() );
                assertTrue( sub2.isActive() );

                sub1.cancel();

                assertFalse( sub1.isActive() );
                assertTrue( sub2.isActive() );

                refCount.decRefCount();

                assertEquals( 0, callbackCounter1.get() );
                assertEquals( 1, callbackCounter2.get() );
            }

            @Test
            public void givenCancelledOnDecCallback_dropReferenceToResource_expectTheResourceToBeGCedEvenThoughAReferenceToTheCancelledSubExists() {
                AtomicLong callbackCounter = new AtomicLong();

                Subscription sub = refCount.onRefDecInvoke( ( refCount, r ) -> callbackCounter.decrementAndGet() );

                sub.cancel();

                Reference weakRef = new WeakReference<>( refCount );
                refCount = null;

                JMAssertions.spinUntilReleased( weakRef );
            }
        }
    }

    @Nested
    public class JustBeforeReleaseCallbackTestCases {
        @Nested
        public class RefCountTestCases {
            @Test
            public void givenJustBeforeReleaseCallback_callIncRef_expectCallbackToNotBeCalled() {
                AtomicLong callbackCounter = new AtomicLong();

                refCount.justBeforeReleaseInvoke( r -> {
                    assertSame( r, resource );

                    callbackCounter.incrementAndGet();
                } );

                refCount.incRefCount();

                assertEquals( 0, callbackCounter.get() );
            }

            @Test
            public void givenJustBeforeReleaseCallbackAndRefCountOf2_callDecRef_expectCallbackToNotBeCalled() {
                AtomicLong callbackCounter = new AtomicLong();

                refCount.incRefCount();

                refCount.justBeforeReleaseInvoke( r -> {
                    assertSame( r, resource );

                    callbackCounter.incrementAndGet();
                } );

                refCount.decRefCount();

                assertEquals( 0, callbackCounter.get() );
            }

            @Test
            public void givenJustBeforeReleaseCallbackAndRefCountOf1_callDecRef_expectCallbackToBeCalled() {
                AtomicLong callbackCounter = new AtomicLong();

                refCount.justBeforeReleaseInvoke( r -> {
                    assertSame( r, resource );

                    callbackCounter.incrementAndGet();
                } );

                refCount.decRefCount();

                assertEquals( 1, callbackCounter.get() );
            }
        }

        @Nested
        public class SubscriptionRelatedTestCases {
            @Test
            public void givenTwoOnJustBeforeReleaseCallbacks_invokeJustBeforeRelease_expectTwoCallbacks() {
                AtomicLong callbackCounter = new AtomicLong();

                refCount.justBeforeReleaseInvoke( r -> callbackCounter.incrementAndGet() );
                refCount.justBeforeReleaseInvoke( r -> callbackCounter.incrementAndGet() );

                refCount.decRefCount();

                assertEquals( 2, callbackCounter.get() );
            }

            @Test
            public void givenCancelledOnJustBeforeReleaseCallback_invokeJustBeforeRelease_expectCancelledCallbackToNotBeInvoked() {
                AtomicLong callbackCounter = new AtomicLong();

                Subscription sub = refCount.justBeforeReleaseInvoke( r -> callbackCounter.incrementAndGet() );

                assertTrue( sub.isActive() );

                sub.cancel();

                assertFalse( sub.isActive() );

                refCount.decRefCount();

                assertEquals( 0, callbackCounter.get() );
            }

            @Test
            public void givenTwoOnJustBeforeReleaseCallbacks_cancelOneAndInvokeJustBeforeRelease_expectOnlyOneCallback() {
                AtomicLong callbackCounter1 = new AtomicLong();
                AtomicLong callbackCounter2 = new AtomicLong();

                Subscription sub1 = refCount.justBeforeReleaseInvoke( r -> callbackCounter1.incrementAndGet() );
                Subscription sub2 = refCount.justBeforeReleaseInvoke( r -> callbackCounter2.incrementAndGet() );

                assertTrue( sub1.isActive() );
                assertTrue( sub2.isActive() );

                sub1.cancel();

                assertFalse( sub1.isActive() );
                assertTrue( sub2.isActive() );

                refCount.decRefCount();

                assertEquals( 0, callbackCounter1.get() );
                assertEquals( 1, callbackCounter2.get() );
            }

            @Test
            public void givenCancelledOnJustBeforeReleaseCallback_dropReferenceToResource_expectTheResourceToBeGCedEvenThoughAReferenceToTheCancelledSubExists() {
                AtomicLong callbackCounter = new AtomicLong();

                Subscription sub = refCount.justBeforeReleaseInvoke( r -> callbackCounter.incrementAndGet() );

                sub.cancel();

                Reference weakRef = new WeakReference<>( refCount );
                refCount = null;

                JMAssertions.spinUntilReleased( weakRef );
            }
        }
    }

    @Nested
    public class JustAfterReleaseCallbackTestCases {
        @Nested
        public class RefCountTestCases {
            @Test
            public void givenJustAfterReleaseCallback_callIncRef_expectCallbackToNotBeCalled () {
                AtomicLong callbackCounter = new AtomicLong();

                refCount.justAfterReleaseInvoke( callbackCounter::incrementAndGet );

                refCount.incRefCount();

                assertEquals( 0, callbackCounter.get() );
            }

            @Test
            public void givenJustAfterReleaseCallbackAndRefCountOf2_callDecRef_expectCallbackToNotBeCalled
                () {
                AtomicLong callbackCounter = new AtomicLong();

                refCount.incRefCount();
                refCount.justAfterReleaseInvoke( callbackCounter::incrementAndGet );

                refCount.decRefCount();

                assertEquals( 0, callbackCounter.get() );
            }

            @Test
            public void givenJustAfterReleaseCallbackAndRefCountOf1_callDecRef_expectCallbackToBeCalled
                () {
                AtomicLong callbackCounter = new AtomicLong();

                refCount.justAfterReleaseInvoke( callbackCounter::incrementAndGet );

                refCount.decRefCount();

                assertEquals( 1, callbackCounter.get() );
            }

            @Test
            public void givenJustBeforeAndAfterReleaseCallbackAndRefCountOf1_callDecRef_expectBeforeCallbackToBeCalledBeforeTheAfterCallback() {
                List<String> callbackCalls = new Vector<>();

                refCount.justBeforeReleaseInvoke( r -> callbackCalls.add( "BEFORE" ) );
                refCount.justAfterReleaseInvoke( () -> callbackCalls.add( "AFTER" ) );

                refCount.decRefCount();

                assertEquals( Arrays.asList( "BEFORE", "AFTER" ), callbackCalls );
            }
        }



        @Nested
        public class SubscriptionRelatedTestCases {
            @Test
            public void givenTwoOnJustAfterReleaseCallbacks_invokeJustAfterRelease_expectTwoCallbacks() {
                AtomicLong callbackCounter = new AtomicLong();

                refCount.justAfterReleaseInvoke( callbackCounter::incrementAndGet );
                refCount.justAfterReleaseInvoke( callbackCounter::incrementAndGet );

                refCount.decRefCount();

                assertEquals( 2, callbackCounter.get() );
            }

            @Test
            public void givenCancelledOnJustAfterReleaseCallback_invokeJustAfterRelease_expectCancelledCallbackToNotBeInvoked() {
                AtomicLong callbackCounter = new AtomicLong();

                Subscription sub = refCount.justAfterReleaseInvoke( callbackCounter::incrementAndGet );

                assertTrue( sub.isActive() );

                sub.cancel();

                assertFalse( sub.isActive() );

                refCount.decRefCount();

                assertEquals( 0, callbackCounter.get() );
            }

            @Test
            public void givenTwoOnJustAfterReleaseCallbacks_cancelOneAndInvokeJustAfterRelease_expectOnlyOneCallback() {
                AtomicLong callbackCounter1 = new AtomicLong();
                AtomicLong callbackCounter2 = new AtomicLong();

                Subscription sub1 = refCount.justAfterReleaseInvoke( callbackCounter1::incrementAndGet );
                Subscription sub2 = refCount.justAfterReleaseInvoke( callbackCounter2::incrementAndGet );

                assertTrue( sub1.isActive() );
                assertTrue( sub2.isActive() );

                sub1.cancel();

                assertFalse( sub1.isActive() );
                assertTrue( sub2.isActive() );

                refCount.decRefCount();

                assertEquals( 0, callbackCounter1.get() );
                assertEquals( 1, callbackCounter2.get() );
            }

            @Test
            public void givenCancelledOnJustAfterReleaseCallback_dropReferenceToResource_expectTheResourceToBeGCedEvenThoughAReferenceToTheCancelledSubExists() {
                AtomicLong callbackCounter = new AtomicLong();

                Subscription sub = refCount.justAfterReleaseInvoke( callbackCounter::incrementAndGet );

                sub.cancel();

                Reference weakRef = new WeakReference<>( refCount );
                refCount = null;

                JMAssertions.spinUntilReleased( weakRef );
            }
        }
    }


    @Nested
    public class ConcurrencyTestCases {
        @Test
        public void givenNewManager_haveTenThreadsCallIncRefAndDecRef_expectFinalCountToBeOne() throws InterruptedException {
            final int threadCount    = 10;
            final int iterationCount = 1000;

            CountDownLatch startLatch = new CountDownLatch(threadCount);
            CountDownLatch doneLatch  = new CountDownLatch(threadCount);

            for ( int i=0; i<threadCount; i++ ) {
                new IncDecThread( iterationCount, startLatch, doneLatch ).start();
            }

            doneLatch.await( 10, TimeUnit.SECONDS );

            assertEquals( 1, refCount.getRefCount() );
            assertEquals( 0, resource.getCloseCount() );
        }

        @Test
        public void givenNewManager_haveTenThreadsCallIncRefAndDecRef_expectCallbacksToBeInvoked() throws InterruptedException {
            final int threadCount    = 10;
            final int iterationCount = 1000;

            CountDownLatch startLatch = new CountDownLatch(threadCount);
            CountDownLatch doneLatch  = new CountDownLatch(threadCount);

            List<CallbackRecord> allCallbackCalls = new Vector<>();

            refCount.onRefIncInvoke( (newCount,r) -> {
                assertSame( r, resource );

                allCallbackCalls.add( new IncCallbackRecord(newCount) );
            } );

            refCount.onRefDecInvoke( (newCount,r) -> {
                assertSame( r, resource );

                allCallbackCalls.add( new DecCallbackRecord(newCount) );
            } );

            refCount.justBeforeReleaseInvoke( r -> {
                assertSame( r, resource );

                allCallbackCalls.add( new BeforeReleaseCallbackRecord() );
            } );

            refCount.justAfterReleaseInvoke( () -> allCallbackCalls.add(new AfterReleaseCallbackRecord()) );

            for ( int i=0; i<threadCount; i++ ) {
                new IncDecThread( iterationCount, startLatch, doneLatch ).start();
            }

            doneLatch.await( 10, TimeUnit.SECONDS );

            assertEquals( 1, refCount.getRefCount() );
            assertEquals( 0, resource.getCloseCount() );

            allCallbackCalls.forEach( cr -> assertTrue( cr.getActualRefCount() > 0 ) );

            assertEquals( threadCount*iterationCount*2, allCallbackCalls.size() );

            long callbackRecordedRefCountTotal = FP.wrap(allCallbackCalls).fold(1L, (refCountSoFar,nextRecord) -> nextRecord.adjustRefCount(refCountSoFar));

            assertEquals( 1L, callbackRecordedRefCountTotal );
        }
    }


    @AllArgsConstructor
    private class IncDecThread extends Thread {
        private int            iterationCount;
        private CountDownLatch startLatch;
        private CountDownLatch doneLatch;

        public void run() {
            startLatch.countDown();
            TryUtils.invokeAndRethrowException( () -> startLatch.await(10,TimeUnit.SECONDS) );

            for ( int i=0; i<iterationCount; i++ ) {
                refCount.incRefCount();

                assertTrue( refCount.getRefCount() > 1 );

                refCount.decRefCount();
            }

            doneLatch.countDown();
        }
    }

    private static interface CallbackRecord {
        public long adjustRefCount( long refCountSoFar );
        public long getActualRefCount();
    }

    private class IncCallbackRecord implements CallbackRecord {

        @Getter private long actualRefCount;


        public IncCallbackRecord( long newCount ) {
            actualRefCount = newCount;
        }

        public long adjustRefCount( long refCountSoFar ) {
            return refCountSoFar + 1;
        }
    }

    private class DecCallbackRecord implements CallbackRecord {

        @Getter private long actualRefCount;

        public DecCallbackRecord( long newCount ) {
            actualRefCount = newCount;
        }

        public long adjustRefCount( long refCountSoFar ) {
            return refCountSoFar - 1;
        }
    }

    private class BeforeReleaseCallbackRecord implements CallbackRecord {
        public long adjustRefCount( long refCountSoFar ) {
            return refCountSoFar;
        }

        public long getActualRefCount() {
            return 0;
        }
    }

    private class AfterReleaseCallbackRecord implements CallbackRecord {
        public long adjustRefCount( long refCountSoFar ) {
            return refCountSoFar;
        }

        public long getActualRefCount() {
            return 0;
        }
    }

    public static class Resource {
        private final AtomicLong closeCounter = new AtomicLong();

        public void close() {
            closeCounter.incrementAndGet();
        }

        public long getCloseCount() {
            return closeCounter.get();
        }
    }

}
