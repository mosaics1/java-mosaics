package mosaics.lang;


import org.junit.jupiter.api.Test;

import static mosaics.lang.QA.fail;
import static org.junit.jupiter.api.Assertions.assertEquals;


public class BigIntegerXTest {

    @Test
    public void testFactorial() {
        assertEquals( new BigIntegerX(1), new BigIntegerX(0).factorial() );
        assertEquals( new BigIntegerX(1), new BigIntegerX(1).factorial() );
        assertEquals( new BigIntegerX(2), new BigIntegerX(2).factorial() );
        assertEquals( new BigIntegerX(6), new BigIntegerX(3).factorial() );
        assertEquals( new BigIntegerX(24), new BigIntegerX(4).factorial() );
        assertEquals( new BigIntegerX(120), new BigIntegerX(5).factorial() );
        assertEquals( new BigIntegerX(720), new BigIntegerX(6).factorial() );
        assertEquals( new BigIntegerX(5040), new BigIntegerX(7).factorial() );
    }

    @Test
    public void testFactorialNegOne_expectError() {
        try {
            new BigIntegerX(-1).factorial();
            fail( "Expected UnsupportedOperationException" );
        } catch ( UnsupportedOperationException ex ) {
            assertEquals( "factorial does not support negative values: -1", ex.getMessage() );
        }
    }

}
