package mosaics.lang;

import mosaics.lang.lifecycle.Subscription;
import org.junit.jupiter.api.Test;

import java.util.concurrent.atomic.AtomicBoolean;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class SubscriptionTest {

    @Test
    public void newSubscription_expectItToBeRunning() {
        Subscription sub = new Subscription();

        assertTrue( sub.isActive() );
    }

    @Test
    public void newSubscription_expectCancelCallbackToNotBeInvokedYet() {
        AtomicBoolean callbackCalled = new AtomicBoolean( false );

        Subscription sub = new Subscription( () -> callbackCalled.set(true) );

        assertTrue( sub.isActive() );
        assertFalse( callbackCalled.get() );
    }

    @Test
    public void cancelSub_expectRequestToBePastToCallback() {
        AtomicBoolean callbackCalled = new AtomicBoolean();

        Subscription sub = new Subscription( () -> callbackCalled.set(true) );

        sub.cancel();

        assertFalse( sub.isActive() );
        assertTrue( callbackCalled.get() );
    }


// and

    @Test
    public void givenNullOther_and_expectAndToReturnItself() {
        Subscription a = new Subscription();

        assertTrue( a == a.and(null) );
    }

    @Test
    public void givenActiveOther_expectCompositeSubscriptionToAlsoBeActive() {
        Subscription a = new Subscription();
        Subscription b = new Subscription();

        Subscription all = a.and(b);

        assertTrue( a.isActive() );
        assertTrue( b.isActive() );
        assertTrue( all.isActive() );
    }

    @Test
    public void givenActiveOther_cancelComposite_expectAllSubsToBecomeInactive() {
        Subscription a = new Subscription();
        Subscription b = new Subscription();

        Subscription all = a.and(b);

        all.cancel();

        assertFalse( a.isActive() );
        assertFalse( b.isActive() );
        assertFalse( all.isActive() );
    }

    @Test
    public void givenActiveOther_cancelA_expectCompositeToRemainActive() {
        Subscription a = new Subscription();
        Subscription b = new Subscription();

        Subscription all = a.and(b);

        a.cancel();

        assertFalse( a.isActive() );
        assertTrue( b.isActive() );
        assertTrue( all.isActive() );
    }

    @Test
    public void givenActiveOther_cancelB_expectCompositeToRemainActive() {
        Subscription a = new Subscription();
        Subscription b = new Subscription();

        Subscription all = a.and(b);

        b.cancel();

        assertTrue( a.isActive() );
        assertFalse( b.isActive() );
        assertTrue( all.isActive() );
    }

}
