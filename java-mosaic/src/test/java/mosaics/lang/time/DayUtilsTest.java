package mosaics.lang.time;


import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.time.DayOfWeek;
import java.time.format.DateTimeFormatter;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class DayUtilsTest {
    @Nested
    public class ParseTestCases {
        @Test
        public void iso() {
            assertEquals( new Day(2020,5,16).getEpochDays(), DayUtils.parse( "2020-05-16", DateTimeFormatter.ISO_DATE) );
        }
    }

    @Nested
    public class ToEpochDaysTestCases {
        @Test
        public void epochDays() {
            assertEquals( -1L, DayUtils.toEpochDays(1969,12,31) );
            assertEquals( 0L, DayUtils.toEpochDays(1970,1,1) );
            assertEquals( 1L, DayUtils.toEpochDays(1970,1,2) );
            assertEquals( 2L, DayUtils.toEpochDays(1970,1,3) );
            assertEquals( new Day(2020,5,16).getEpochDays(), DayUtils.toEpochDays(2020,5,16) );
        }
    }

    @Nested
    public class IsWeekendDaysTestCases {
        private final DayOfWeek[] weekDays    = {DayOfWeek.MONDAY,DayOfWeek.TUESDAY,DayOfWeek.WEDNESDAY,DayOfWeek.THURSDAY,DayOfWeek.FRIDAY};
        private final DayOfWeek[] weekendDays = {DayOfWeek.SATURDAY,DayOfWeek.SUNDAY};

        @Test
        public void testWeekDays() {
            for ( DayOfWeek weekDay : weekDays ) {
                assertFalse( DayUtils.isWeekend(weekDay) );
            }
        }

        @Test
        public void testWeekendDays() {
            for ( DayOfWeek weekDay : weekendDays ) {
                assertTrue( DayUtils.isWeekend(weekDay) );
            }
        }
    }

}
