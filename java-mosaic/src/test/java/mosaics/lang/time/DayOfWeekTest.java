package mosaics.lang.time;

import org.junit.jupiter.api.Test;

import java.util.Calendar;

import static org.junit.jupiter.api.Assertions.*;


public class DayOfWeekTest {
    @Test
    public void testOfCalendarDayOfWeek() {
        assertEquals( DayOfWeek.MONDAY, DayOfWeek.ofCalendarDayOfWeek( Calendar.MONDAY) );
        assertEquals( DayOfWeek.TUESDAY, DayOfWeek.ofCalendarDayOfWeek( Calendar.TUESDAY) );
        assertEquals( DayOfWeek.WEDNESDAY, DayOfWeek.ofCalendarDayOfWeek( Calendar.WEDNESDAY) );
        assertEquals( DayOfWeek.THURSDAY, DayOfWeek.ofCalendarDayOfWeek( Calendar.THURSDAY) );
        assertEquals( DayOfWeek.FRIDAY, DayOfWeek.ofCalendarDayOfWeek( Calendar.FRIDAY) );
        assertEquals( DayOfWeek.SATURDAY, DayOfWeek.ofCalendarDayOfWeek( Calendar.SATURDAY) );
        assertEquals( DayOfWeek.SUNDAY, DayOfWeek.ofCalendarDayOfWeek( Calendar.SUNDAY) );

        assertThrows( IllegalArgumentException.class, () -> DayOfWeek.ofCalendarDayOfWeek(0) );
        assertThrows( IllegalArgumentException.class, () -> DayOfWeek.ofCalendarDayOfWeek(10) );
    }

    @Test
    public void testToJdkCalendarEnum() {
        assertEquals( Calendar.MONDAY, DayOfWeek.MONDAY.toJdkCalendarEnum() );
        assertEquals( Calendar.TUESDAY, DayOfWeek.TUESDAY.toJdkCalendarEnum() );
        assertEquals( Calendar.WEDNESDAY, DayOfWeek.WEDNESDAY.toJdkCalendarEnum() );
        assertEquals( Calendar.THURSDAY, DayOfWeek.THURSDAY.toJdkCalendarEnum() );
        assertEquals( Calendar.FRIDAY, DayOfWeek.FRIDAY.toJdkCalendarEnum() );
        assertEquals( Calendar.SATURDAY, DayOfWeek.SATURDAY.toJdkCalendarEnum() );
        assertEquals( Calendar.SUNDAY, DayOfWeek.SUNDAY.toJdkCalendarEnum() );
    }
}
