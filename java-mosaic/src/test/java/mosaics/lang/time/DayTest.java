package mosaics.lang.time;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.Date;
import java.util.GregorianCalendar;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class DayTest {
    @Nested
    public class ConstructorTestCases {
        @Test
        public void constructors() {
            assertEquals( "2020-05-16", new Day("2020-05-16").toString() );
            assertEquals( "2020-05-16", new Day(new Date(DTMUtils.toEpoch(2020,5,16))).toString() );
            assertEquals( "2020-05-16", new Day(2020,5,16).toString() );
            assertEquals( "2020-05-16", new Day(new GregorianCalendar(2020,5-1,16)).toString() );
            assertEquals( "2020-05-16", new Day(LocalDate.of(2020,5,16)).toString() );
            assertEquals( "1970-01-01", new Day(0L).toString() );
        }
    }


    @Nested
    public class GetComponentTestCases {
        @Test
        public void getYear() {
            assertEquals( 2020, new Day("2020-05-16").getYear() );
            assertEquals( 1970, new Day(0L).getYear() );
        }

        @Test
        public void getMonth() {
            assertEquals( 5, new Day("2020-05-16").getMonth() );
            assertEquals( 1, new Day("2020-01-16").getMonth() );
            assertEquals( 12, new Day("2020-12-16").getMonth() );
            assertEquals( 1, new Day(0L).getMonth() );
        }

        @Test
        public void getDayOfMonth() {
            assertEquals( 16, new Day("2020-05-16").getDayOfMonth() );
            assertEquals( 30, new Day("2020-01-30").getDayOfMonth() );
            assertEquals( 1, new Day("2020-12-01").getDayOfMonth() );
            assertEquals( 1, new Day(0L).getDayOfMonth() );
        }
    }


    @Nested
    public class AddSubtractTestCases {
        @Test
        public void addDays() {
            assertEquals( "2020-05-17", new Day("2020-05-16").addDay().toString() );
            assertEquals( "2020-05-18", new Day("2020-05-16").addDays(2).toString() );
            assertEquals( "2020-06-01", new Day("2020-05-31").addDay().toString() );
        }

        @Test
        public void subtractDays() {
            assertEquals( "2020-05-15", new Day("2020-05-16").subtractDay().toString() );
            assertEquals( "2020-05-14", new Day("2020-05-16").subtractDays(2).toString() );
            assertEquals( "2020-04-30", new Day("2020-05-01").subtractDay().toString() );
        }

        @Test
        public void addMonths() {
            assertEquals( "2020-06-16", new Day("2020-05-16").addMonth().toString() );
            assertEquals( "2020-07-16", new Day("2020-05-16").addMonths(2).toString() );
            assertEquals( "2021-01-04", new Day("2020-12-04").addMonth().toString() );
            assertEquals( "2021-01-31", new Day("2020-12-31").addMonth().toString() );
            assertEquals( "2021-02-28", new Day("2021-01-31").addMonth().toString() );
        }

        @Test
        public void subtractMonths() {
            assertEquals( "2020-04-16", new Day("2020-05-16").subtractMonth().toString() );
            assertEquals( "2020-03-16", new Day("2020-05-16").subtractMonths(2).toString() );
            assertEquals( "2020-11-04", new Day("2020-12-04").subtractMonth().toString() );
            assertEquals( "2019-12-31", new Day("2020-01-31").subtractMonth().toString() );
            assertEquals( "2019-11-30", new Day("2019-12-31").subtractMonth().toString() );
        }

        @Test
        public void addYears() {
            assertEquals( "2021-05-16", new Day("2020-05-16").addYear().toString() );
            assertEquals( "2022-05-16", new Day("2020-05-16").addYears(2).toString() );
            assertEquals( "2021-12-04", new Day("2020-12-04").addYear().toString() );
            assertEquals( "2021-12-31", new Day("2020-12-31").addYear().toString() );
            assertEquals( "2022-01-31", new Day("2021-01-31").addYear().toString() );
            assertEquals( "2021-02-28", new Day("2020-02-29").addYear().toString() );
            assertEquals( "2021-02-28", new Day("2020-02-28").addYear().toString() );
        }

        @Test
        public void subtractYears() {
            assertEquals( "2019-05-16", new Day("2020-05-16").subtractYear().toString() );
            assertEquals( "2018-05-16", new Day("2020-05-16").subtractYears(2).toString() );
            assertEquals( "2019-12-04", new Day("2020-12-04").subtractYear().toString() );
            assertEquals( "2019-12-31", new Day("2020-12-31").subtractYear().toString() );
            assertEquals( "2020-01-31", new Day("2021-01-31").subtractYear().toString() );
            assertEquals( "2019-02-28", new Day("2020-02-29").subtractYear().toString() );
            assertEquals( "2019-02-28", new Day("2020-02-28").subtractYear().toString() );
        }
    }


    @Nested
    public class SetComponentTestCases {
        @Test
        public void setDay() {
            assertEquals( "2020-05-01", new Day("2020-05-16").setDay(1).toString() );
            assertEquals( "2021-01-31", new Day("2021-01-16").setDay(31).toString() );
            assertEquals( "2020-02-15", new Day("2020-02-29").setDay(15).toString() );
            assertEquals( "2020-02-28", new Day("2020-02-28").setDay(28).toString() );
        }
    }


    @Nested
    public class ToTestCases {
        @Test
        public void toDTM() {
            assertEquals( new DTM(2020,5,16), new Day("2020-05-16").toDTM() );
            assertEquals( new DTM(2020,2,29), new Day("2020-02-29").toDTM() );
        }
    }
}
