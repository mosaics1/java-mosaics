package mosaics.lang.clock;

import mosaics.lang.Backdoor;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;


public class OffsetClockTest {

    private long  testStartMillis = System.currentTimeMillis();
    private Clock clock = new OffsetClock( 123L );


    @Test
    public void timeSpecifiedAtConstructionIsRetrievable() {
        Backdoor.sleep( 1L );

        assertClockTimeIsWithinToleranceOf( 124L );
    }

    @Test
    public void isMutableReturnsTrue() {
        assertTrue( clock.isModifiable() );
    }

    @Test
    public void resetTheClockTime() {
        clock.setMillis( 120L );

        assertClockTimeIsWithinToleranceOf( 120L );
    }

    @Test
    public void incTheClockTime() {
        clock.incMillis( 1 );

        assertClockTimeIsWithinToleranceOf( 124L );
    }

    @org.junit.jupiter.api.Test
    public void testNanos() {
        long beforeNanos = clock.currentTimeNanos();

        Backdoor.sleep( 20 );

        long durationNanos = clock.currentTimeNanos() - beforeNanos;

        assertTrue( durationNanos >= 20 * Backdoor.NUMBER_OF_NANOSECONDS_PER_MILLISECOND );
    }


    private void assertClockTimeIsWithinToleranceOf( long targetMillis ) {
        long nowMillis = System.currentTimeMillis();
        long delta     = nowMillis+10 - testStartMillis;

        assertTrue( clock.currentTimeMillis() >= targetMillis, "clock did not move foward with the real system clock");
        assertTrue( targetMillis <= 123L+delta, "clock moved forward too fast for the real system clock" );
    }

}
