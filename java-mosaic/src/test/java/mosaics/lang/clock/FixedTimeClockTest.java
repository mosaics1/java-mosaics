package mosaics.lang.clock;

import mosaics.lang.Backdoor;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class FixedTimeClockTest {

    private Clock clock = new FixedTimeClock( 123 );


    @Test
    public void timeSpecifiedAtConstructionIsRetrievable() {
        Backdoor.sleep( 20L ); // uses a clock step size that is suitable large to be easily detected

        assertEquals( 123L, clock.currentTimeMillis() );
    }

    @Test
    public void isMutableReturnsTrue() {
        assertTrue( clock.isModifiable() );
    }

    @Test
    public void resetTheClockTime() {
        clock.setMillis( 120L );

        assertEquals( 120L, clock.currentTimeMillis() );
    }

    @Test
    public void incTheClockTime() {
        clock.incMillis( 1 );

        assertEquals( 124L, clock.currentTimeMillis() );
    }

    @Test
    public void testNanos() {
        clock = new FixedTimeClock();

        long beforeNanos = clock.currentTimeNanos();

        Backdoor.sleep( 20 );

        long durationNanos = clock.currentTimeNanos() - beforeNanos;

        assertTrue( durationNanos == 0 );
    }

}
