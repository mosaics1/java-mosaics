package mosaics.lang.clock;

import mosaics.lang.Backdoor;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class RealTimeClockTest {

    private long  testStartMillis = System.currentTimeMillis();
    private Clock clock = RealTimeClock.INSTANCE;


    @Test
    public void timeSpecifiedAtConstructionIsRetrievable() {
        Backdoor.sleep( 1L );

        assertClockTimeIsWithinToleranceOf( 124L );
    }

    @Test
    public void isMutableReturnsFalse() {
        assertFalse( clock.isModifiable() );
    }

    @Test
    public void resetTheClockTime() {
        try {
            clock.setMillis( 120L );

            fail( "expected UnsupportedOperationException" );
        } catch ( UnsupportedOperationException e ) {
        }
    }

    @Test
    public void incTheClockTime() {
        try {
            clock.incMillis( 1 );

            fail( "expected UnsupportedOperationException" );
        } catch ( UnsupportedOperationException e ) {
        }
    }

    @Test
    public void testNanos() {
        long beforeNanos = clock.currentTimeNanos();

        Backdoor.sleep( 20 );

        long durationNanos = clock.currentTimeNanos() - beforeNanos;

        assertTrue( durationNanos >= 20 * Backdoor.NUMBER_OF_NANOSECONDS_PER_MILLISECOND );
    }


    @Nested
    public class TimeTestCases {
        @Test
        public void testTime() {
            double durationMillis = clock.timeMillis( () -> Backdoor.sleep( 20 ) );

            assertTrue( durationMillis >= 20 );
        }

        @Test
        public void givenFunctionThrowsException_expectExceptionToBeThrown() {
            RuntimeException ex = new RuntimeException();

            try {
                clock.timeMillis( () -> {throw ex;} );

                fail( "expected RuntimeException" );
            } catch ( RuntimeException e ) {
                assertSame( ex, e );
            }
        }
    }

    private void assertClockTimeIsWithinToleranceOf( long targetMillis ) {
        long nowMillis = System.currentTimeMillis();
        long delta = nowMillis - testStartMillis;

        assertTrue( clock.currentTimeMillis() >= targetMillis, "clock did not move foward with the real system clock" );
        assertTrue( targetMillis <= 123L+delta, "clock moved forward too fast for the real system clock" );
    }

}
