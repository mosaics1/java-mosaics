package mosaics.lang;

import org.junit.jupiter.api.Test;

import static mosaics.lang.MathUtils.constrainWithinRange;
import static mosaics.lang.MathUtils.isPowerOf2;
import static mosaics.lang.MathUtils.roundDownToClosestPowerOf2;
import static mosaics.lang.MathUtils.roundUpToClosestPowerOf2;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class MathUtilsTest {

    @Test
    public void testRoundUpToClosestPowerOf2() {
        assertEquals( 2, roundUpToClosestPowerOf2( -1 ) );
        assertEquals( 2, roundUpToClosestPowerOf2( 0 ) );
        assertEquals( 2, roundUpToClosestPowerOf2( 1 ) );
        assertEquals( 2, roundUpToClosestPowerOf2( 2 ) );
        assertEquals( 4, roundUpToClosestPowerOf2( 3 ) );
        assertEquals( 4, roundUpToClosestPowerOf2( 4 ) );
        assertEquals( 8, roundUpToClosestPowerOf2( 5 ) );
        assertEquals( 8, roundUpToClosestPowerOf2( 6 ) );
        assertEquals( 8, roundUpToClosestPowerOf2( 7 ) );
        assertEquals( 8, roundUpToClosestPowerOf2( 8 ) );
        assertEquals( 16, roundUpToClosestPowerOf2( 9 ) );
        assertEquals( 32, roundUpToClosestPowerOf2( 18 ) );
    }

    @Test
    public void testRoundDownToClosestPowerOf2() {
        assertEquals( 2, roundDownToClosestPowerOf2(-1) );
        assertEquals( 2, roundDownToClosestPowerOf2(0) );
        assertEquals( 2, roundDownToClosestPowerOf2(1) );
        assertEquals( 2, roundDownToClosestPowerOf2(2) );
        assertEquals( 2, roundDownToClosestPowerOf2(3) );
        assertEquals( 4, roundDownToClosestPowerOf2(4) );
        assertEquals( 4, roundDownToClosestPowerOf2(5) );
        assertEquals( 4, roundDownToClosestPowerOf2(6) );
        assertEquals( 4, roundDownToClosestPowerOf2(7) );
        assertEquals( 8, roundDownToClosestPowerOf2(8) );
        assertEquals( 8, roundDownToClosestPowerOf2(9) );
        assertEquals( 16, roundDownToClosestPowerOf2(18) );
    }

    @Test
    public void testIsPowerOf2() {
        assertFalse(isPowerOf2(-1));
        assertFalse(isPowerOf2(0));
        assertFalse(isPowerOf2(1));
        assertTrue(isPowerOf2(2));
        assertFalse(isPowerOf2(3));
        assertTrue(isPowerOf2(4));
        assertFalse(isPowerOf2(5));
        assertFalse(isPowerOf2(6));
        assertFalse(isPowerOf2(7));
        assertTrue(isPowerOf2(8));
        assertFalse(isPowerOf2(9));
        assertFalse(isPowerOf2(18));
    }


    @Test
    public void testConstrainWithinRange() {
        assertEquals( 3, constrainWithinRange(3,Long.MIN_VALUE,6) );
        assertEquals( 3, constrainWithinRange(3,-1,6) );
        assertEquals( 3, constrainWithinRange(3,0,6) );
        assertEquals( 3, constrainWithinRange(3,1,6) );
        assertEquals( 3, constrainWithinRange(3,2,6) );
        assertEquals( 3, constrainWithinRange(3,3,6) );
        assertEquals( 4, constrainWithinRange(3,4,6) );
        assertEquals( 5, constrainWithinRange(3,5,6) );
        assertEquals( 6, constrainWithinRange(3,6,6) );
        assertEquals( 6, constrainWithinRange(3,7,6) );
        assertEquals( 6, constrainWithinRange(3,8,6) );
        assertEquals( 6, constrainWithinRange(3,Long.MAX_VALUE,6) );
    }

}
