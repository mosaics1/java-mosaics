package mosaics.lang.threads;

import mosaics.lang.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;


public class ThreadGuardTest {
    private ThreadGuard threadAsserter = new ThreadGuard();

    @Nested
    public class GivenAssertionsAreEnabled {
        @BeforeEach
        public void setup() {
            Assert.enableAssertionsForCallingThread( true );
        }

        @AfterEach
        public void tearDown() {
            Assert.unsetAssertionsOverrideForCallingThread();
        }


        @Test
        public void callRepeatedlyFromSingleThread_expectNoError() {
            for ( int i=0; i<100; i++ ) {
                threadAsserter.assertSingleThreaded();
            }
        }

        @Test
        public void callFromMultipleThreads_expectException() throws InterruptedException {
            CountDownLatch latch = new CountDownLatch( 1 );

            new Thread( () -> {
                Assert.enableAssertionsForCallingThread( true );

                try {
                    threadAsserter.assertSingleThreaded();
                } finally {
                    Assert.unsetAssertionsOverrideForCallingThread();
                    latch.countDown();
                }
            } ).start();

            latch.await( 5, TimeUnit.SECONDS );

            try {
                threadAsserter.assertSingleThreaded();
                fail( "expected IllegalStateException" );
            } catch ( IllegalStateException ex ) {
                assertEquals( "Multiple threads detected", ex.getMessage() );
            }
        }
    }

    @Nested
    public class GivenAssertionsAreDisabled {
        @BeforeEach
        public void setup() {
            Assert.enableAssertionsForCallingThread( false );
        }

        @AfterEach
        public void tearDown() {
            Assert.unsetAssertionsOverrideForCallingThread();
        }


        @Test
        public void callRepeatedlyFromSingleThread_expectNoError() {
            for ( int i=0; i<100; i++ ) {
                threadAsserter.assertSingleThreaded();
            }
        }

        @Test
        public void callFromMultipleThreads_expectException() throws InterruptedException {
            CountDownLatch latch = new CountDownLatch( 1 );

            new Thread( () -> {
                threadAsserter.assertSingleThreaded();
                latch.countDown();
            } ).start();

            latch.await( 5, TimeUnit.SECONDS );

            threadAsserter.assertSingleThreaded();
        }
    }

}
