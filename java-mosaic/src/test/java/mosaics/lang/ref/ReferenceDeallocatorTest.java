package mosaics.lang.ref;

import mosaics.junit.JMAssertions;
import mosaics.lang.functions.Function3;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.parallel.Isolated;

import java.lang.ref.ReferenceQueue;
import java.util.HashSet;
import java.util.Set;


@Isolated // due to use of createMemoryPressure
public class ReferenceDeallocatorTest extends JMAssertions {
    private static record Book(String name) {}

    private Set<String> gcReleasedBooks = new HashSet<>();


    @Test
    public void softReferenceTest() {
        ReferenceDeallocator<String, Book, SoftReference<String,Book>> deallocator = new ReferenceDeallocator<>(
            (Function3<String, Book, ReferenceQueue<Book>, SoftReference<String, Book>>) SoftReference::new,
            ref -> {
                assertNull(ref.get());

                gcReleasedBooks.add(ref.getMetaData());
            }
        );

        Book book = new Book("LOTR");

        deallocator.register( book.name(), book );

        book = null;

        createMemoryPressure(); // push for the soft reference to get GCed!

        spinUntilTrue( () -> {System.gc(); Thread.yield(); deallocator.poll(); return gcReleasedBooks.size() > 0;} );
        assertEquals( Set.of("LOTR"), gcReleasedBooks );
    }

    @Test
    public void weakReferenceTest() {
        ReferenceDeallocator<String, Book, WeakReference<String,Book>> deallocator = new ReferenceDeallocator<>(
            (Function3<String, Book, ReferenceQueue<Book>, WeakReference<String, Book>>) WeakReference::new,
            ref -> {
                assertNull(ref.get());

                gcReleasedBooks.add(ref.getMetaData());
            }
        );

        Book book = new Book("LOTR");

        deallocator.register( book.name(), book );

        book = null;

        spinUntilTrue( () -> {System.gc(); Thread.yield(); deallocator.poll(); return gcReleasedBooks.size() > 0;} );
        assertEquals( Set.of("LOTR"), gcReleasedBooks );
    }

    @Test
    public void phantomReferenceTest() {
        ReferenceDeallocator<String, Book, PhantomReference<String,Book>> deallocator = new ReferenceDeallocator<>(
            (Function3<String, Book, ReferenceQueue<Book>, PhantomReference<String, Book>>) PhantomReference::new,
            ref -> {
                assertNull(ref.get());

                gcReleasedBooks.add(ref.getMetaData());
            }
        );

        Book book = new Book("LOTR");

        deallocator.register( book.name(), book );

        book = null;

        spinUntilTrue( () -> {System.gc(); Thread.yield(); deallocator.poll(); return gcReleasedBooks.size() > 0;} );
        assertEquals( Set.of("LOTR"), gcReleasedBooks );
    }
}
