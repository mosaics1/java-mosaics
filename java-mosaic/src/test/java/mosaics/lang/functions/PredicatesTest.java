package mosaics.lang.functions;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class PredicatesTest {

    @Nested
    public class EqTestCases {
        @Test
        @SuppressWarnings("RedundantStringConstructorCall")
        public void equalsString() {
            BooleanFunction1<String> isFoo = Predicates.eq( "foo" );

            assertFalse( isFoo.invoke("f") );
            assertFalse( isFoo.invoke("fo") );
            assertFalse( isFoo.invoke("foO") );
            assertFalse( isFoo.invoke("food") );
            assertFalse( isFoo.invoke("") );
            assertFalse( isFoo.invoke(null) );

            assertTrue( isFoo.invoke("foo") );
            assertTrue( isFoo.invoke(new String("foo")) );
        }
    }

}
