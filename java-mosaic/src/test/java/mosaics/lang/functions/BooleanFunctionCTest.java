package mosaics.lang.functions;

import mosaics.fp.FP;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

@SuppressWarnings("unchecked")
public class BooleanFunctionCTest {

    @Nested
    public class InvokeTestCases {
        @Test
        public void givenFunctionReturnsTrue_expectValue() {
            BooleanFunctionC f = v -> true;

            assertTrue( f.invoke('a') );
        }
        @Test
        public void givenFunctionReturnsFalse_expectValue() {
            BooleanFunctionC f = v -> false;

            assertFalse( f.invoke('a') );
        }

        @Test
        public void givenFunctionThatThrowsAnException_expectException() {
            BooleanFunctionC f = v -> {throw new RuntimeException("expected");};

            try {
                f.invoke('a');
                fail( "expected exception" );
            } catch ( RuntimeException ex ) {
                assertEquals( "expected", ex.getMessage() );
            }
        }
    }
    
    
    @Nested
    public class InvertTestCases {
        @Test
        public void expectInvertedMethodsToPassTheInvokeArgsToTheOriginalFunction() {
            BooleanFunctionC f         = c -> c == 'a';
            BooleanFunctionC invertedF = f.invert();

            assertFalse( invertedF.invoke('a' ) );
            assertTrue( invertedF.invoke('A' ) );
            assertTrue( invertedF.invoke('b' ) );
        }

        @Test
        public void givenUnderlyingFunctionReturnsFalse_expectInvertedFunctionToReturnTrue() {
            BooleanFunctionC f         = c -> false;
            BooleanFunctionC invertedF = f.invert();

            assertTrue( invertedF.invoke('v') );
        }

        @Test
        public void givenUnderlyingFunctionReturnsTrue_expectInvertedFunctionToReturnFalse() {
            BooleanFunctionC f         = c -> true;
            BooleanFunctionC invertedF = f.invert();

            assertFalse( invertedF.invoke('v') );
        }

        @Test
        public void invertAnInvertedFunction_expectTheOriginalFunctionBack() {
            BooleanFunctionC f                 = c -> false;
            BooleanFunctionC invertedF         = f.invert();
            BooleanFunctionC invertedInvertedF = invertedF.invert();

            assertSame( invertedInvertedF, f );
        }

        @Test
        public void expectAnInvertedFunctionDoesNotEqualTheOriginalFunction() {
            BooleanFunctionC f         = c -> false;
            BooleanFunctionC invertedF = f.invert();

            assertNotEquals( invertedF, f );
        }

        @Test
        public void expectAnInvertedFunctionDoesNotHaveTheSameHashCodeAsTheOriginalFunction() {
            BooleanFunctionC f         = c -> false;
            BooleanFunctionC invertedF = f.invert();

            assertNotEquals( f.hashCode(), invertedF.hashCode() );
        }
    }


    @Nested
    public class WithDescriptionTestCases {
        @Test
        public void expectNoDefaultDescription() {
            BooleanFunctionC f = c -> true;

            assertEquals( FP.emptyOption(), f.getDescription() );
        }

        @Test
        public void givenNullDescription_expectNoDescription() {
            BooleanFunctionC f                = c -> true;
            BooleanFunctionC fWithDescription = f.withDescription( null );

            assertEquals( FP.emptyOption(), fWithDescription.getDescription() );
            assertSame( f, fWithDescription );
        }

        @Test
        public void givenBlankDescription_expectNoDescription() {
            BooleanFunctionC f                = c -> true;
            BooleanFunctionC fWithDescription = f.withDescription( "    " );

            assertEquals( FP.emptyOption(), fWithDescription.getDescription() );
            assertSame( f, fWithDescription );
        }

        @Test
        public void givenDescription_callGetDescription_expectDescription() {
            BooleanFunctionC f                = c -> true;
            BooleanFunctionC fWithDescription = f.withDescription( "hello world" );

            assertEquals( FP.option("hello world"), fWithDescription.getDescription() );
        }

        @Test
        public void givenSameDescription_callGetDescription_expectNoChange() {
            BooleanFunctionC f                = c -> true;
            BooleanFunctionC fWithDescription = f.withDescription( "hello world" );

            assertSame( fWithDescription, fWithDescription.withDescription("hello world") );
        }

        @Test
        public void givenNewDescription_callGetDescription_expectChange() {
            BooleanFunctionC f                 = c -> true;
            BooleanFunctionC fWithDescription  = f.withDescription( "hello world" );
            BooleanFunctionC fWithDescription2 = fWithDescription.withDescription( "abc" );

            assertEquals( FP.option("abc"), fWithDescription2.getDescription() );
        }

        @Test
        public void givenDescription_callWithDescriptionAgainThisTimeWithNull_expectOriginalFunctionBack() {
            BooleanFunctionC f                = c -> true;
            BooleanFunctionC fWithDescription = f.withDescription( "hello" );

            assertSame( f, fWithDescription.withDescription(null) );
        }

        @Test
        public void givenDescription_callWithDescriptionAgainThisTimeWithBlankString_expectOriginalFunctionBack() {
            BooleanFunctionC f                = c -> true;
            BooleanFunctionC fWithDescription = f.withDescription( "hello" );

            assertSame( f, fWithDescription.withDescription("  ") );
        }

        @Test
        public void testToString() {
            BooleanFunctionC f                = c -> true;
            BooleanFunctionC fWithDescription = f.withDescription( "hello world" );

            assertEquals( "BooleanFunctionCWithDescription(description=Some(hello world), f="+f+")", fWithDescription.toString() );
        }

        @Test
        public void givenFReturnsTrue_showThatDescriptionDoesNotAffectResultOfCallingInvoke() {
            BooleanFunctionC f = c -> true;

            assertEquals( f.invoke('v'), f.withDescription( "hello world" ).invoke('v') );
        }

        @Test
        public void givenFReturnsFalse_showThatDescriptionDoesNotAffectResultOfCallingInvoke() {
            BooleanFunctionC f = c -> false;

            assertEquals( f.invoke('v'), f.withDescription( "hello world" ).invoke('v') );
        }
    }
}
