package mosaics.lang.functions;

import mosaics.junit.JMRandomExtension;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static org.junit.jupiter.api.Assertions.*;


@ExtendWith(JMRandomExtension.class)
public class ObjectFactoryTest {
    @Test
    public void ensureObjectFactoryCanBeInjectedIntoATestMethod( @JMRandomExtension.Random ObjectFactory objectFactory ) {
        assertNotNull( objectFactory );
    }
}
