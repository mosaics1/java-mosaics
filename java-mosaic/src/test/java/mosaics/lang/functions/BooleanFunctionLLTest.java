package mosaics.lang.functions;

import mosaics.fp.FP;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

@SuppressWarnings("unchecked")
public class BooleanFunctionLLTest {

    @Nested
    public class InvokeTestCases {
        @Test
        public void givenFunctionReturnsTrue_expectValue() {
            BooleanFunctionLL f = (a,b) -> true;

            assertTrue( f.invoke(1,1) );
        }
        @Test
        public void givenFunctionReturnsFalse_expectValue() {
            BooleanFunctionLL f = (a,b) -> false;

            assertFalse( f.invoke(1,1) );
        }

        @Test
        public void givenFunctionThatThrowsAnException_expectException() {
            BooleanFunctionLL f = (a,b) -> {throw new RuntimeException("expected");};

            try {
                f.invoke(1,1);
                fail( "expected exception" );
            } catch ( RuntimeException ex ) {
                assertEquals( "expected", ex.getMessage() );
            }
        }
    }
    

    @Nested
    public class InvertTestCases {
        @Test
        public void expectInvertedMethodsToPassTheInvokeArgsToTheOriginalFunction() {
            BooleanFunctionLL f         = (a,b) -> a == 1L && b == 2L;
            BooleanFunctionLL invertedF = f.invert();

            assertFalse( invertedF.invoke(1L, 2L ) );
            assertTrue( invertedF.invoke(2L, 1L ) );
            assertTrue( invertedF.invoke(2L, 2L ) );
            assertTrue( invertedF.invoke(1L, 1L ) );
            assertTrue( invertedF.invoke(-1L, 2L ) );
            assertTrue( invertedF.invoke(1L, -2L ) );
            assertTrue( invertedF.invoke(-1L, -2L ) );
            assertFalse( invertedF.invoke(1L, 2L ) );
        }

        @Test
        public void givenUnderlyingFunctionReturnsFalse_expectInvertedFunctionToReturnTrue() {
            BooleanFunctionLL f         = (a,b) -> false;
            BooleanFunctionLL invertedF = f.invert();

            assertTrue( invertedF.invoke(0,0) );
        }

        @Test
        public void givenUnderlyingFunctionReturnsTrue_expectInvertedFunctionToReturnFalse() {
            BooleanFunctionLL f         = (a,b) -> true;
            BooleanFunctionLL invertedF = f.invert();

            assertFalse( invertedF.invoke(0,0) );
        }

        @Test
        public void invertAnInvertedFunction_expectTheOriginalFunctionBack() {
            BooleanFunctionLL f                 = (a,b) -> false;
            BooleanFunctionLL invertedF         = f.invert();
            BooleanFunctionLL invertedInvertedF = invertedF.invert();

            assertSame( invertedInvertedF, f );
        }

        @Test
        public void expectAnInvertedFunctionDoesNotEqualTheOriginalFunction() {
            BooleanFunctionLL f         = (a,b) -> false;
            BooleanFunctionLL invertedF = f.invert();

            assertNotEquals( invertedF, f );
        }

        @Test
        public void expectAnInvertedFunctionDoesNotHaveTheSameHashCodeAsTheOriginalFunction() {
            BooleanFunctionLL f         = (a,b) -> false;
            BooleanFunctionLL invertedF = f.invert();

            assertNotEquals( f.hashCode(), invertedF.hashCode() );
        }
    }


    @Nested
    public class WithDescriptionTestCases {
        @Test
        public void expectNoDefaultDescription() {
            BooleanFunctionLL f = (a,b) -> true;

            assertEquals( FP.emptyOption(), f.getDescription() );
        }

        @Test
        public void givenNullDescription_expectNoDescription() {
            BooleanFunctionLL f                = (a,b) -> true;
            BooleanFunctionLL fWithDescription = f.withDescription( null );

            assertEquals( FP.emptyOption(), fWithDescription.getDescription() );
            assertSame( f, fWithDescription );
        }

        @Test
        public void givenBlankDescription_expectNoDescription() {
            BooleanFunctionLL f                = (a,b) -> true;
            BooleanFunctionLL fWithDescription = f.withDescription( "    " );

            assertEquals( FP.emptyOption(), fWithDescription.getDescription() );
            assertSame( f, fWithDescription );
        }

        @Test
        public void givenDescription_callGetDescription_expectDescription() {
            BooleanFunctionLL f                = (a,b) -> true;
            BooleanFunctionLL fWithDescription = f.withDescription( "hello world" );

            assertEquals( FP.option("hello world"), fWithDescription.getDescription() );
        }

        @Test
        public void givenSameDescription_callGetDescription_expectNoChange() {
            BooleanFunctionLL f                = (a,b) -> true;
            BooleanFunctionLL fWithDescription = f.withDescription( "hello world" );

            assertSame( fWithDescription, fWithDescription.withDescription("hello world") );
        }

        @Test
        public void givenNewDescription_callGetDescription_expectChange() {
            BooleanFunctionLL f                 = (a,b) -> true;
            BooleanFunctionLL fWithDescription  = f.withDescription( "hello world" );
            BooleanFunctionLL fWithDescription2 = fWithDescription.withDescription( "abc" );

            assertEquals( FP.option("abc"), fWithDescription2.getDescription() );
        }

        @Test
        public void givenDescription_callWithDescriptionAgainThisTimeWithNull_expectOriginalFunctionBack() {
            BooleanFunctionLL f                = (a,b) -> true;
            BooleanFunctionLL fWithDescription = f.withDescription( "hello" );

            assertSame( f, fWithDescription.withDescription(null) );
        }

        @Test
        public void givenDescription_callWithDescriptionAgainThisTimeWithBlankString_expectOriginalFunctionBack() {
            BooleanFunctionLL f                = (a,b) -> true;
            BooleanFunctionLL fWithDescription = f.withDescription( "hello" );

            assertSame( f, fWithDescription.withDescription("  ") );
        }

        @Test
        public void testToString() {
            BooleanFunctionLL f                = (a,b) -> true;
            BooleanFunctionLL fWithDescription = f.withDescription( "hello world" );

            assertEquals( "BooleanFunctionLLWithDescription(description=Some(hello world), f="+f+")", fWithDescription.toString() );
        }

        @Test
        public void givenFReturnsTrue_showThatDescriptionDoesNotAffectResultOfCallingInvoke() {
            BooleanFunctionLL f = (a,b) -> true;

            assertEquals( f.invoke(0,0), f.withDescription( "hello world" ).invoke(0,0) );
        }

        @Test
        public void givenFReturnsFalse_showThatDescriptionDoesNotAffectResultOfCallingInvoke() {
            BooleanFunctionLL f = (a,b) -> false;

            assertEquals( f.invoke(0,0), f.withDescription( "hello world" ).invoke(0,0) );
        }
    }
}
