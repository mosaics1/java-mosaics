package mosaics.lang.functions;

import mosaics.fp.FP;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

@SuppressWarnings("unchecked")
public class BooleanFunctionLTest {

    @Nested
    public class InvokeTestCases {
        @Test
        public void givenFunctionReturnsTrue_expectValue() {
            BooleanFunctionL f = v -> true;

            assertTrue( f.invoke(1) );
        }
        @Test
        public void givenFunctionReturnsFalse_expectValue() {
            BooleanFunctionL f = v -> false;

            assertFalse( f.invoke(1) );
        }

        @Test
        public void givenFunctionThatThrowsAnException_expectException() {
            BooleanFunctionL f = v -> {throw new RuntimeException("expected");};

            try {
                f.invoke(1);
                fail( "expected exception" );
            } catch ( RuntimeException ex ) {
                assertEquals( "expected", ex.getMessage() );
            }
        }
    }
    
    
    @Nested
    public class InvertTestCases {
        @Test
        public void expectInvertedMethodsToPassTheInvokeArgsToTheOriginalFunction() {
            BooleanFunctionL f         = c -> c == 123L;
            BooleanFunctionL invertedF = f.invert();

            assertFalse( invertedF.invoke(123L ) );
            assertTrue( invertedF.invoke(1L ) );
            assertTrue( invertedF.invoke(-1L ) );
        }

        @Test
        public void givenUnderlyingFunctionReturnsFalse_expectInvertedFunctionToReturnTrue() {
            BooleanFunctionL f         = c -> false;
            BooleanFunctionL invertedF = f.invert();

            assertTrue( invertedF.invoke(1L) );
        }

        @Test
        public void givenUnderlyingFunctionReturnsTrue_expectInvertedFunctionToReturnFalse() {
            BooleanFunctionL f         = c -> true;
            BooleanFunctionL invertedF = f.invert();

            assertFalse( invertedF.invoke(1L) );
        }

        @Test
        public void invertAnInvertedFunction_expectTheOriginalFunctionBack() {
            BooleanFunctionL f                 = c -> false;
            BooleanFunctionL invertedF         = f.invert();
            BooleanFunctionL invertedInvertedF = invertedF.invert();

            assertSame( invertedInvertedF, f );
        }

        @Test
        public void expectAnInvertedFunctionDoesNotEqualTheOriginalFunction() {
            BooleanFunctionL f         = c -> false;
            BooleanFunctionL invertedF = f.invert();

            assertNotEquals( invertedF, f );
        }

        @Test
        public void expectAnInvertedFunctionDoesNotHaveTheSameHashCodeAsTheOriginalFunction() {
            BooleanFunctionL f         = c -> false;
            BooleanFunctionL invertedF = f.invert();

            assertNotEquals( f.hashCode(), invertedF.hashCode() );
        }
    }


    @Nested
    public class WithDescriptionTestCases {
        @Test
        public void expectNoDefaultDescription() {
            BooleanFunctionL f = c -> true;

            assertEquals( FP.emptyOption(), f.getDescription() );
        }

        @Test
        public void givenNullDescription_expectNoDescription() {
            BooleanFunctionL f                = c -> true;
            BooleanFunctionL fWithDescription = f.withDescription( null );

            assertEquals( FP.emptyOption(), fWithDescription.getDescription() );
            assertSame( f, fWithDescription );
        }

        @Test
        public void givenBlankDescription_expectNoDescription() {
            BooleanFunctionL f                = c -> true;
            BooleanFunctionL fWithDescription = f.withDescription( "   " );

            assertEquals( FP.emptyOption(), fWithDescription.getDescription() );
            assertSame( f, fWithDescription );
        }

        @Test
        public void givenDescription_callGetDescription_expectDescription() {
            BooleanFunctionL f                = c -> true;
            BooleanFunctionL fWithDescription = f.withDescription( "hello world" );

            assertEquals( FP.option("hello world"), fWithDescription.getDescription() );
        }

        @Test
        public void givenSameDescription_callGetDescription_expectNoChange() {
            BooleanFunctionL f                = c -> true;
            BooleanFunctionL fWithDescription = f.withDescription( "hello world" );

            assertSame( fWithDescription, fWithDescription.withDescription("hello world") );
        }

        @Test
        public void givenNewDescription_callGetDescription_expectChange() {
            BooleanFunctionL f                 = c -> true;
            BooleanFunctionL fWithDescription  = f.withDescription( "hello world" );
            BooleanFunctionL fWithDescription2 = fWithDescription.withDescription( "abc" );

            assertEquals( FP.option("abc"), fWithDescription2.getDescription() );
        }

        @Test
        public void givenDescription_callWithDescriptionAgainThisTimeWithNull_expectOriginalFunctionBack() {
            BooleanFunctionL f                = c -> true;
            BooleanFunctionL fWithDescription = f.withDescription( "hello" );

            assertSame( f, fWithDescription.withDescription(null) );
        }

        @Test
        public void givenDescription_callWithDescriptionAgainThisTimeWithBlankString_expectOriginalFunctionBack() {
            BooleanFunctionL f                = c -> true;
            BooleanFunctionL fWithDescription = f.withDescription( "hello" );

            assertSame( f, fWithDescription.withDescription("  ") );
        }

        @Test
        public void testToString() {
            BooleanFunctionL f                = c -> true;
            BooleanFunctionL fWithDescription = f.withDescription( "hello world" );

            assertEquals( "BooleanFunctionLWithDescription(description=Some(hello world), f="+f+")", fWithDescription.toString() );
        }

        @Test
        public void givenFReturnsTrue_showThatDescriptionDoesNotAffectResultOfCallingInvoke() {
            BooleanFunctionL f = c -> true;

            assertEquals( f.invoke(1L), f.withDescription( "hello world" ).invoke(1L) );
        }

        @Test
        public void givenFReturnsFalse_showThatDescriptionDoesNotAffectResultOfCallingInvoke() {
            BooleanFunctionL f = c -> false;

            assertEquals( f.invoke(1L), f.withDescription( "hello world" ).invoke(1L) );
        }
    }
}
