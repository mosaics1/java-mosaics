package mosaics.lang.functions;

import mosaics.fp.FP;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

@SuppressWarnings("unchecked")
public class BooleanFunction2Test {

    @Nested
    public class InvokeTestCases {
        @Test
        public void givenFunctionReturnsTrue_expectValue() {
            BooleanFunction2 f = (a,b) -> true;

            assertTrue( f.invoke(1,1) );
        }
        @Test
        public void givenFunctionReturnsFalse_expectValue() {
            BooleanFunction2 f = (a,b) -> false;

            assertFalse( f.invoke(1,1) );
        }

        @Test
        public void givenFunctionThatThrowsAnException_expectException() {
            BooleanFunction2 f = (a,b) -> {throw new RuntimeException("expected");};

            try {
                f.invoke(1,1);
                fail( "expected exception" );
            } catch ( RuntimeException ex ) {
                assertEquals( "expected", ex.getMessage() );
            }
        }
    }
    
    
    @Nested
    public class InvertTestCases {
        @Test
        public void expectInvertedMethodsToPassTheInvokeArgsToTheOriginalFunction() {
            BooleanFunction2 f         = (a,b) -> a.equals("hello") && b.equals("world");
            BooleanFunction2 invertedF = f.invert();

            assertFalse( invertedF.invoke("hello", "world") );
            assertTrue( invertedF.invoke("hell", "world") );
            assertTrue( invertedF.invoke("Hello", "world") );
            assertTrue( invertedF.invoke("hello", "worl") );
            assertTrue( invertedF.invoke("hello", "World") );
            assertFalse( invertedF.invoke("hello", "world") );
        }

        @Test
        public void givenUnderlyingFunctionReturnsFalse_expectInvertedFunctionToReturnTrue() {
            BooleanFunction2 f         = (a,b) -> false;
            BooleanFunction2 invertedF = f.invert();

            assertTrue( invertedF.invoke("v", "v") );
        }

        @Test
        public void givenUnderlyingFunctionReturnsTrue_expectInvertedFunctionToReturnFalse() {
            BooleanFunction2 f         = (a,b) -> true;
            BooleanFunction2 invertedF = f.invert();

            assertFalse( invertedF.invoke("v","v") );
        }

        @Test
        public void invertAnInvertedFunction_expectTheOriginalFunctionBack() {
            BooleanFunction2 f                 = (a,b) -> false;
            BooleanFunction2 invertedF         = f.invert();
            BooleanFunction2 invertedInvertedF = invertedF.invert();

            assertSame( invertedInvertedF, f );
        }

        @Test
        public void expectAnInvertedFunctionDoesNotEqualTheOriginalFunction() {
            BooleanFunction2 f         = (a,b) -> false;
            BooleanFunction2 invertedF = f.invert();

            assertNotEquals( invertedF, f );
        }

        @Test
        public void expectAnInvertedFunctionDoesNotHaveTheSameHashCodeAsTheOriginalFunction() {
            BooleanFunction2 f         = (a,b) -> false;
            BooleanFunction2 invertedF = f.invert();

            assertNotEquals( f.hashCode(), invertedF.hashCode() );
        }
    }


    @Nested
    public class WithDescriptionTestCases {
        @Test
        public void expectNoDefaultDescription() {
            BooleanFunction2 f = (a,b) -> true;

            assertEquals( FP.emptyOption(), f.getDescription() );
        }

        @Test
        public void givenNullDescription_expectNoDescription() {
            BooleanFunction2 f                = (a,b) -> true;
            BooleanFunction2 fWithDescription = f.withDescription( null );

            assertEquals( FP.emptyOption(), fWithDescription.getDescription() );
            assertSame( f, fWithDescription );
        }

        @Test
        public void givenBlankDescription_expectNoDescription() {
            BooleanFunction2 f                = (a,b) -> true;
            BooleanFunction2 fWithDescription = f.withDescription( "     " );

            assertEquals( FP.emptyOption(), fWithDescription.getDescription() );
            assertSame( f, fWithDescription );
        }

        @Test
        public void givenDescription_callGetDescription_expectDescription() {
            BooleanFunction2 f                = (a,b) -> true;
            BooleanFunction2 fWithDescription = f.withDescription( "hello world" );

            assertEquals( FP.option("hello world"), fWithDescription.getDescription() );
        }

        @Test
        public void givenSameDescription_callGetDescription_expectNoChange() {
            BooleanFunction2 f                = (a,b) -> true;
            BooleanFunction2 fWithDescription = f.withDescription( "hello world" );

            assertSame( fWithDescription, fWithDescription.withDescription("hello world") );
        }

        @Test
        public void givenNewDescription_callGetDescription_expectChange() {
            BooleanFunction2 f                 = (a,b) -> true;
            BooleanFunction2 fWithDescription  = f.withDescription( "hello world" );
            BooleanFunction2 fWithDescription2 = fWithDescription.withDescription( "abc" );

            assertEquals( FP.option("abc"), fWithDescription2.getDescription() );
        }

        @Test
        public void givenDescription_callWithDescriptionAgainThisTimeWithNull_expectOriginalFunctionBack() {
            BooleanFunction2 f                = (a,b) -> true;
            BooleanFunction2 fWithDescription = f.withDescription( "hello" );

            assertSame( f, fWithDescription.withDescription(null) );
        }

        @Test
        public void givenDescription_callWithDescriptionAgainThisTimeWithBlankString_expectOriginalFunctionBack() {
            BooleanFunction2 f                = (a,b) -> true;
            BooleanFunction2 fWithDescription = f.withDescription( "hello" );

            assertSame( f, fWithDescription.withDescription("  ") );
        }

        @Test
        public void testToString() {
            BooleanFunction2 f                = (a,b) -> true;
            BooleanFunction2 fWithDescription = f.withDescription( "hello world" );

            assertEquals( "BooleanFunction2WithDescription(description=Some(hello world), f="+f+")", fWithDescription.toString() );
        }

        @Test
        public void givenFReturnsTrue_showThatDescriptionDoesNotAffectResultOfCallingInvoke() {
            BooleanFunction2 f = (a,b) -> true;

            assertEquals( f.invoke("v","v"), f.withDescription( "hello world" ).invoke("v","v") );
        }

        @Test
        public void givenFReturnsFalse_showThatDescriptionDoesNotAffectResultOfCallingInvoke() {
            BooleanFunction2 f = (a,b) -> false;

            assertEquals( f.invoke("v","v"), f.withDescription( "hello world" ).invoke("v","v") );
        }
    }
}
