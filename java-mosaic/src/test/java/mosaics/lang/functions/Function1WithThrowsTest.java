package mosaics.lang.functions;

import mosaics.fp.FP;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class Function1WithThrowsTest {

    @Nested
    public class InvokeTestCases {
        @Test
        public void givenFunctionReturnsValue_expectValue() throws Throwable {
            Function1WithThrows<String,String> f = v -> "hello";

            assertEquals( "hello", f.invoke("v") );
        }

        @Test
        public void givenFunctionThatThrowsAnException_expectException() throws Throwable {
            Function1WithThrows<String,String> f = v -> {throw new RuntimeException("expected");};

            try {
                f.invoke("v");
                fail( "expected exception" );
            } catch ( RuntimeException ex ) {
                assertEquals( "expected", ex.getMessage() );
            }
        }
    }

    @Nested
    public class ToFunctionTestCases {
        @Test
        public void givenFunctionReturnsValue_expectValue() {
            Function1WithThrows<String,String> f = v -> "hello";

            assertEquals( "hello", f.toFunction().apply("v") );
        }

        @Test
        public void givenFunctionThatThrowsAnException_expectException() throws Throwable {
            Function1WithThrows<String,String> f = v -> {throw new RuntimeException("expected");};

            try {
                f.toFunction().apply("v");
                fail( "expected exception" );
            } catch ( RuntimeException ex ) {
                assertEquals( "expected", ex.getMessage() );
            }
        }
    }

    @Nested
    public class WithDescriptionTestCases {
        @Test
        public void expectNoDefaultDescription() {
            Function1WithThrows<String,String> f = v -> null;

            assertEquals( FP.emptyOption(), f.getDescription() );
        }

        @Test
        public void givenNullDescription_expectNoDescription() {
            Function1WithThrows f                = v -> null;
            Function1WithThrows fWithDescription = f.withDescription( null );

            assertEquals( FP.emptyOption(), fWithDescription.getDescription() );
            assertSame( f, fWithDescription );
        }

        @Test
        public void givenBlankDescription_expectNoDescription() {
            Function1WithThrows f                = v -> null;
            Function1WithThrows fWithDescription = f.withDescription( "    " );

            assertEquals( FP.emptyOption(), fWithDescription.getDescription() );
            assertSame( f, fWithDescription );
        }

        @Test
        public void givenDescription_callGetDescription_expectDescription() {
            Function1WithThrows f                = v -> null;
            Function1WithThrows fWithDescription = f.withDescription( "hello world" );

            assertEquals( FP.option("hello world"), fWithDescription.getDescription() );
        }

        @Test
        public void givenSameDescription_callGetDescription_expectNoChange() {
            Function1WithThrows f                = v -> null;
            Function1WithThrows fWithDescription = f.withDescription( "hello world" );

            assertSame( fWithDescription, fWithDescription.withDescription("hello world") );
        }

        @Test
        public void givenNewDescription_callGetDescription_expectChange() {
            Function1WithThrows f                 = v -> null;
            Function1WithThrows fWithDescription  = f.withDescription( "hello world" );
            Function1WithThrows fWithDescription2 = fWithDescription.withDescription( "abc" );

            assertEquals( FP.option("abc"), fWithDescription2.getDescription() );
        }

        @Test
        public void givenDescription_callWithDescriptionAgainThisTimeWithNull_expectOriginalFunctionBack() {
            Function1WithThrows f                = v -> null;
            Function1WithThrows fWithDescription = f.withDescription( "hello" );

            assertSame( f, fWithDescription.withDescription(null) );
        }

        @Test
        public void givenDescription_callWithDescriptionAgainThisTimeWithBlankString_expectOriginalFunctionBack() {
            Function1WithThrows f                = v -> null;
            Function1WithThrows fWithDescription = f.withDescription( "hello" );

            assertSame( f, fWithDescription.withDescription("  ") );
        }

        @Test
        public void testToString() {
            Function1WithThrows f                = v -> null;
            Function1WithThrows fWithDescription = f.withDescription( "hello world" );

            assertEquals( "Function1WithThrowsWithDescription(description=Some(hello world), f="+f+")", fWithDescription.toString() );
        }

        @Test
        public void givenFReturnsNull_showThatDescriptionDoesNotAffectResultOfCallingInvoke() throws Throwable {
            Function1WithThrows<String,String> f = v -> null;

            assertEquals( f.invoke("v"), f.withDescription( "hello world" ).invoke("v") );
        }

        @Test
        public void givenFReturnsNonNull_showThatDescriptionDoesNotAffectResultOfCallingInvoke() throws Throwable {
            Function1WithThrows<String,String> f = v -> "foo";

            assertEquals( f.invoke("v"), f.withDescription( "hello world" ).invoke("v") );
        }
    }


    @Nested
    public class InvokeSilentlyTestCases {
        @Test
        public void givenFunctionThatThrowsAnException_invokeSilently_expectException() {
            Function1WithThrows<String,String> f = v -> {throw new RuntimeException("expected");};

            try {
                f.invokeSilently("v");
                fail( "expected exception" );
            } catch ( RuntimeException ex ) {
                assertEquals( "expected", ex.getMessage() );
            }
        }
    }
}
