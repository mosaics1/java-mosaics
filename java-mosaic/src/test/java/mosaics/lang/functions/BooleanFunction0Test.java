package mosaics.lang.functions;

import mosaics.fp.FP;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class BooleanFunction0Test {

    @Nested
    public class InvokeTestCases {
        @Test
        public void givenFunctionReturnsTrue_expectValue() {
            BooleanFunction0 f = () -> true;

            assertTrue( f.invoke() );
        }
        @Test
        public void givenFunctionReturnsFalse_expectValue() {
            BooleanFunction0 f = () -> false;

            assertFalse( f.invoke() );
        }

        @Test
        public void givenFunctionThatThrowsAnException_expectException() {
            BooleanFunction0 f = () -> {throw new RuntimeException("expected");};

            try {
                f.invoke();
                fail( "expected exception" );
            } catch ( RuntimeException ex ) {
                assertEquals( "expected", ex.getMessage() );
            }
        }
    }


    @Nested
    public class InvertTestCases {
        @Test
        public void givenUnderlyingFunctionReturnsFalse_expectInvertedFunctionToReturnTrue() {
            BooleanFunction0 f         = () -> false;
            BooleanFunction0 invertedF = f.invert();

            assertTrue( invertedF.invoke() );
        }

        @Test
        public void givenUnderlyingFunctionReturnsTrue_expectInvertedFunctionToReturnFalse() {
            BooleanFunction0 f         = () -> true;
            BooleanFunction0 invertedF = f.invert();

            assertFalse( invertedF.invoke() );
        }

        @Test
        public void invertAnInvertedFunction_expectTheOriginalFunctionBack() {
            BooleanFunction0 f                 = () -> false;
            BooleanFunction0 invertedF         = f.invert();
            BooleanFunction0 invertedInvertedF = invertedF.invert();

            assertSame( invertedInvertedF, f );
        }

        @Test
        public void expectAnInvertedFunctionDoesNotEqualTheOriginalFunction() {
            BooleanFunction0 f         = () -> false;
            BooleanFunction0 invertedF = f.invert();

            assertFalse( invertedF.equals(f) );
        }

        @Test
        public void expectAnInvertedFunctionDoesNotHaveTheSameHashCodeAsTheOriginalFunction() {
            BooleanFunction0 f         = () -> false;
            BooleanFunction0 invertedF = f.invert();

            assertNotEquals( f.hashCode(), invertedF.hashCode() );
        }
    }


    @Nested
    public class WithDescriptionTestCases {
        @Test
        public void expectNoDefaultDescription() {
            BooleanFunction0 f = () -> true;

            assertEquals( FP.emptyOption(), f.getDescription() );
        }

        @Test
        public void givenNullDescription_expectNoDescription() {
            BooleanFunction0 f                = () -> true;
            BooleanFunction0 fWithDescription = f.withDescription( null );

            assertEquals( FP.emptyOption(), fWithDescription.getDescription() );
            assertSame( f, fWithDescription );
        }

        @Test
        public void givenBlankDescription_expectNoDescription() {
            BooleanFunction0 f                = () -> true;
            BooleanFunction0 fWithDescription = f.withDescription( "    " );

            assertEquals( FP.emptyOption(), fWithDescription.getDescription() );
            assertSame( f, fWithDescription );
        }

        @Test
        public void givenDescription_callGetDescription_expectDescription() {
            BooleanFunction0 f                = () -> true;
            BooleanFunction0 fWithDescription = f.withDescription( "hello world" );

            assertEquals( FP.option("hello world"), fWithDescription.getDescription() );
        }

        @Test
        public void givenSameDescription_callGetDescription_expectNoChange() {
            BooleanFunction0 f                = () -> true;
            BooleanFunction0 fWithDescription = f.withDescription( "hello world" );

            assertSame( fWithDescription, fWithDescription.withDescription("hello world") );
        }

        @Test
        public void givenNewDescription_callGetDescription_expectChange() {
            BooleanFunction0 f                 = () -> true;
            BooleanFunction0 fWithDescription  = f.withDescription( "hello world" );
            BooleanFunction0 fWithDescription2 = fWithDescription.withDescription( "abc" );

            assertEquals( FP.option("abc"), fWithDescription2.getDescription() );
        }

        @Test
        public void givenDescription_callWithDescriptionAgainThisTimeWithNull_expectOriginalFunctionBack() {
            BooleanFunction0 f                = () -> true;
            BooleanFunction0 fWithDescription = f.withDescription( "hello" );

            assertSame( f, fWithDescription.withDescription(null) );
        }

        @Test
        public void givenDescription_callWithDescriptionAgainThisTimeWithBlankString_expectOriginalFunctionBack() {
            BooleanFunction0 f                = () -> true;
            BooleanFunction0 fWithDescription = f.withDescription( "hello" );

            assertSame( f, fWithDescription.withDescription("  ") );
        }

        @Test
        public void testToString() {
            BooleanFunction0 f                = () -> true;
            BooleanFunction0 fWithDescription = f.withDescription( "hello world" );

            assertEquals( "BooleanFunction0WithDescription(description=Some(hello world), f="+f+")", fWithDescription.toString() );
        }

        @Test
        public void givenFReturnsTrue_showThatDescriptionDoesNotAffectResultOfCallingInvoke() {
            BooleanFunction0 f = () -> true;

            assertEquals( f.invoke(), f.withDescription( "hello world" ).invoke() );
        }

        @Test
        public void givenFReturnsFalse_showThatDescriptionDoesNotAffectResultOfCallingInvoke() {
            BooleanFunction0 f = () -> false;

            assertEquals( f.invoke(), f.withDescription( "hello world" ).invoke() );
        }
    }
}
