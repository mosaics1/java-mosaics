package mosaics.lang.functions;

import mosaics.fp.FP;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class Function0Test {

    @Nested
    public class InvokeTestCases {
        @Test
        public void givenFunctionReturnsTrue_expectValue() {
            BooleanFunction0 f = () -> true;

            assertTrue( f.invoke() );
        }
        @Test
        public void givenFunctionReturnsFalse_expectValue() {
            BooleanFunction0 f = () -> false;

            assertFalse( f.invoke() );
        }

        @Test
        public void givenFunctionThatThrowsAnException_expectException() {
            BooleanFunction0 f = () -> {throw new RuntimeException("expected");};

            try {
                f.invoke();
                fail( "expected exception" );
            } catch ( RuntimeException ex ) {
                assertEquals( "expected", ex.getMessage() );
            }
        }
    }
    
    
    @Nested
    public class WithDescriptionTestCases {
        @Test
        public void expectNoDefaultDescription() {
            Function0 f = () -> null;

            assertEquals( FP.emptyOption(), f.getDescription() );
        }

        @Test
        public void givenNullDescription_expectNoDescription() {
            Function0 f                = () -> null;
            Function0 fWithDescription = f.withDescription( null );

            assertEquals( FP.emptyOption(), fWithDescription.getDescription() );
            assertSame( f, fWithDescription );
        }

        @Test
        public void givenBlankDescription_expectNoDescription() {
            Function0 f                = () -> null;
            Function0 fWithDescription = f.withDescription( "    " );

            assertEquals( FP.emptyOption(), fWithDescription.getDescription() );
            assertSame( f, fWithDescription );
        }

        @Test
        public void givenDescription_callGetDescription_expectDescription() {
            Function0 f                = () -> null;
            Function0 fWithDescription = f.withDescription( "hello world" );

            assertEquals( FP.option("hello world"), fWithDescription.getDescription() );
        }

        @Test
        public void givenSameDescription_callGetDescription_expectNoChange() {
            Function0 f                = () -> null;
            Function0 fWithDescription = f.withDescription( "hello world" );

            assertSame( fWithDescription, fWithDescription.withDescription("hello world") );
        }

        @Test
        public void givenNewDescription_callGetDescription_expectChange() {
            Function0 f                 = () -> null;
            Function0 fWithDescription  = f.withDescription( "hello world" );
            Function0 fWithDescription2 = fWithDescription.withDescription( "abc" );

            assertEquals( FP.option("abc"), fWithDescription2.getDescription() );
        }

        @Test
        public void givenDescription_callWithDescriptionAgainThisTimeWithNull_expectOriginalFunctionBack() {
            Function0 f                = () -> null;
            Function0 fWithDescription = f.withDescription( "hello" );

            assertSame( f, fWithDescription.withDescription(null) );
        }

        @Test
        public void givenDescription_callWithDescriptionAgainThisTimeWithBlankString_expectOriginalFunctionBack() {
            Function0 f                = () -> null;
            Function0 fWithDescription = f.withDescription( "hello" );

            assertSame( f, fWithDescription.withDescription("  ") );
        }

        @Test
        public void testToString() {
            Function0 f                = () -> null;
            Function0 fWithDescription = f.withDescription( "hello world" );

            assertEquals( "Function0WithDescription(description=Some(hello world), f="+f+")", fWithDescription.toString() );
        }

        @Test
        public void givenFReturnsNull_showThatDescriptionDoesNotAffectResultOfCallingInvoke() {
            Function0 f = () -> null;

            assertEquals( f.invoke(), f.withDescription( "hello world" ).invoke() );
        }

        @Test
        public void givenFReturnsNonNull_showThatDescriptionDoesNotAffectResultOfCallingInvoke() {
            Function0 f = () -> "foo";

            assertEquals( f.invoke(), f.withDescription( "hello world" ).invoke() );
        }
    }
}
