package mosaics.regex;

import mosaics.fp.collections.FPOption;
import mosaics.utils.BitUtils;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.regex.Pattern.CASE_INSENSITIVE;


/**
 * A helper wrapper around the JDK regexp library.
 */
public class RegExp {

    private final Pattern pattern;

    public RegExp( String regexp ) {
        this.pattern = Pattern.compile( regexp );
    }

    public RegExp( String regexp, int flags ) {
        this.pattern = Pattern.compile( regexp, flags );
    }


    public RegExp setCaseInsenitive( boolean caseInsenitiveFlag ) {
        int flags = pattern.flags();

        if ( caseInsenitiveFlag ) {
            flags = BitUtils.bitSet( flags, CASE_INSENSITIVE );
        } else {
            flags = BitUtils.bitClear( flags, CASE_INSENSITIVE );
        }

        return flags == pattern.flags() ? this : new RegExp( pattern.pattern(), flags );
    }

    public Optional<String> extractMatchFrom( String text ) {
        Matcher m                = pattern.matcher(text);
        String  matchedStringNbl = m.find() ? m.group( 0 ) : null;

        return Optional.ofNullable(matchedStringNbl);
    }

    /**
     *
     * @param group starts from 1
     */
    public FPOption<String> extractMatchFrom( String text, int group ) {
        Matcher m                = pattern.matcher(text);
        String  matchedStringNbl = m.find() ? m.group( group ) : null;

        return FPOption.ofBlankableString( matchedStringNbl );
    }

    public boolean matches( String text ) {
        Matcher m = pattern.matcher(text);

        return m.find();
    }

    public int count( CharSequence in ) {
        Matcher m = pattern.matcher( in );

        int count = 0;

        while ( m.find() ) {
            count += 1;
        }

        return count;
    }


    public String toString() {
        int flags = pattern.flags();

        String pattern = Pattern.quote( this.pattern.pattern() );

        if ( (flags & CASE_INSENSITIVE) > 0 ) {
            return "/" + pattern + "/i";
        } else {
            return "/" + pattern + "/";
        }
    }

}
