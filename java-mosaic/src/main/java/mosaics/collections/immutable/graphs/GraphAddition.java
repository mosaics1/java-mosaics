package mosaics.collections.immutable.graphs;

import lombok.Value;


@Value
public class GraphAddition<N,E> {
    private Graph<N,E> updatedGraph;
    private long       newKey;
}
