package mosaics.collections.immutable.graphs;

import lombok.Value;
import lombok.With;
import mosaics.fp.collections.ConsList;
import mosaics.lang.functions.Function1;


@Value
public class Node<NV,EV> {

          private long               nodeKey;
    @With private NV                 nodeValue;

    @With private ConsList<Edge<EV>> inboundEdges;
    @With private ConsList<Edge<EV>> outboundEdges;


    Node<NV, EV> withOutboundEdgeAdded( Edge<EV> newEdge, Function1<ConsList<Edge<EV>>,ConsList<Edge<EV>>> edgeSorter ) {
        return withOutboundEdges( edgeSorter.invoke(outboundEdges.append(newEdge)) );
    }

    Node<NV, EV> withInboundEdgeAdded( Edge<EV> newEdge, Function1<ConsList<Edge<EV>>,ConsList<Edge<EV>>> edgeSorter ) {
        return withInboundEdges( edgeSorter.invoke(inboundEdges.append(newEdge)) );
    }

    Node<NV, EV> withOutboundEdgeRemoved( Edge<EV> edgeToRemove ) {
        return withOutboundEdges( outboundEdges.remove(edgeToRemove) );
    }

    Node<NV, EV> withInboundEdgeRemoved( Edge<EV> edgeToRemove ) {
        return withInboundEdges( inboundEdges.remove(edgeToRemove) );
    }

}
