package mosaics.collections.immutable.graphs;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.With;
import mosaics.fp.FP;
import mosaics.fp.Tryable;
import mosaics.fp.collections.ConsList;
import mosaics.fp.collections.FPIterable;
import mosaics.fp.collections.FPIterator;
import mosaics.fp.collections.FPOption;
import mosaics.lang.functions.Function1;
import mosaics.lang.functions.Function2;
import mosaics.lang.functions.Function3;

import java.util.Objects;


/**
 * Provides utilities for walking around a Graph.
 */
@AllArgsConstructor
public class GraphWalker<N,E,S> {

    @Getter @With private final Graph<N,E>                                     graph;
    @Getter @With private final boolean                                        autoCreateFlag;
            @With private final Function2<Node<N,E>,String, FPOption<Edge<E>>> selectEdgeFunc;
            @With private final Function1<String, E>                           createEdgeValueForFunc;
            @With private final Function3<S,String,Edge<E>,S>                  updateSessionObjectFunc;


    @Getter @With private final FPOption<Node<N,E>>                            currentNode;
    @Getter @With private final ConsList<Node<N,E>>                            nodesTraversed;
    @Getter @With private final ConsList<Edge<E>>                              edgesTraversed;
    @Getter @With private final ConsList<S> sessionObjects;


    public GraphWalker( Graph<N,E> graph, Node<N,E> initialNode ) {
        this(
            graph,
            false,
            (n,el) -> n.getOutboundEdges().filter( e -> Objects.equals(e.getEdgeLabel(),el) ).first(),
            el -> null,
            (currentObj,pathLabelUsed,edge) -> null,
            FP.option( initialNode ),
            ConsList.singleton( initialNode ),
            ConsList.nil(),
            ConsList.nil()
        );
    }

    public GraphWalker<N,E,S> withEdgeSorter( Function1<ConsList<Edge<E>>,ConsList<Edge<E>>> newEdgeSorter ) {
        return this.withGraph( graph.withEdgeSorter(newEdgeSorter) );
    }

    public GraphWalker<N,E,S> withNodeValidator( Function2<Node<N,E>,Graph<N,E>,Tryable<Void>> newNodeValidator ) {
        return this.withGraph( graph.withNodeValidator(newNodeValidator) );
    }


    public GraphWalker<N,E,S> walk( FPIterable<String> edgeLabelsToTraverse ) {
        return edgeLabelsToTraverse.fold( this, GraphWalker::walk );
    }

    public GraphWalker<N,E,S> walk( FPIterator<String> edgeLabelsToTraverse ) {
        return edgeLabelsToTraverse.fold( this, GraphWalker::walk );
    }

    public GraphWalker<N,E,S> walk( String targetEdgeLabel ) {
        if ( Objects.equals(targetEdgeLabel,".") ) {
            return this;
        } else if ( Objects.equals(targetEdgeLabel,"..") ) {
            if ( getEdgesTraversed().isEOL() ) {
                return this.withCurrentNode( FP.emptyOption() );
            }

            return this.withCurrentNode( graph.getNode(getEdgesTraversed().getHead().getSourceNodeKey()) )
                .withEdgesTraversed( edgesTraversed.tail() )
                .withNodesTraversed( nodesTraversed.tail() )
                .withSessionObjects( sessionObjects.tail() );
        }

        return currentNode
            .flatMap( c -> selectEdgeFunc.invoke(c,targetEdgeLabel) )
            .map( edge -> this.walk(targetEdgeLabel,edge) )
            .orElse( () -> walkMissingEdge(targetEdgeLabel) );
    }

    private GraphWalker<N, E,S> walkMissingEdge( String targetEdgeLabel ) {
        if ( !autoCreateFlag || currentNode.isEmpty()) {
            return this.withCurrentNode( FP.emptyOption() );
        }

        Node<N,E>           sourceNode         = currentNode.get();
        GraphAddition<N, E> nodeAdded          = graph.addNode( null );
        long                destinationNodeKey = nodeAdded.getNewKey();
        E                   edgeValue          = createEdgeValueForFunc.invoke( targetEdgeLabel );
        GraphAddition<N, E> edgeAdded          = nodeAdded.getUpdatedGraph().addEdge( sourceNode.getNodeKey(), destinationNodeKey, targetEdgeLabel, edgeValue );
        Node<N,E>           updatedSourceNode  = edgeAdded.getUpdatedGraph().getNodeMandatory( sourceNode.getNodeKey() );
        Node<N,E>           destinationNode    = edgeAdded.getUpdatedGraph().getNodeMandatory( destinationNodeKey );
        Edge<E>             newEdge            = edgeAdded.getUpdatedGraph().getEdgeMandatory( edgeAdded.getNewKey() );

        return this.withCurrentNode( FP.option(destinationNode) )
            .withNodesTraversed( this.getNodesTraversed().tail().append(updatedSourceNode).append(destinationNode) )
            .withEdgesTraversed( this.getEdgesTraversed().append(newEdge) )
            .withGraph( edgeAdded.getUpdatedGraph() );
    }

    public FPOption<Node<N, E>> getMatchedNode() {
        return FP.emptyOption();
    }

    private GraphWalker<N,E,S> walk( String pathLabelUsed, Edge<E> selectedEdge ) {
        FPOption<Node<N, E>> destinationNode     = graph.getNode( selectedEdge.getDestinationNodeKey() );
        S                    updatdSessionObject = updateSessionObjectFunc.invoke( sessionObjects.firstOrNull(), pathLabelUsed, selectedEdge );

        return this
            .withCurrentNode( destinationNode )
            .withEdgesTraversed( edgesTraversed.append(selectedEdge) )
            .withNodesTraversed( nodesTraversed.append(destinationNode.get()) )
            .withSessionObjects( sessionObjects.append(updatdSessionObject) );
    }

}
