package mosaics.collections.immutable.graphs;

import mosaics.lang.functions.Function1;
import lombok.AllArgsConstructor;

import java.util.HashMap;
import java.util.Map;

import static mosaics.lang.Backdoor.cast;


@AllArgsConstructor
public class GraphBuilder<N,E> {

    private Function1<N,Object> nodeValueToPKFunc;
    private Graph<N,E>          graph;
    private Map<Object,Long>    nodeIndex;


    public GraphBuilder() {
        this( n -> n );
    }

    public <NPK> GraphBuilder( Function1<N,NPK> toPK ) {
        this( cast(toPK), Graph.emptyGraph(), new HashMap<>() );
    }


    public GraphBuilder<N,E> withNode( N nodeValue ) {
        GraphAddition<N,E> tuple = graph.addNode( nodeValue );

        this.graph = tuple.getUpdatedGraph();

        Object nodePK  = nodeValueToPKFunc.invoke( nodeValue );
        long   nodeKey = tuple.getNewKey();

        nodeIndex.put( nodePK, nodeKey );

        return this;
    }

    @SafeVarargs
    public final GraphBuilder<N,E> withNodes( N...newNodeValues ) {
        GraphBuilder<N,E> b = this;

        for ( N newNodeValue : newNodeValues ) {
            b = b.withNode( newNodeValue );
        }

        return b;
    }

    public GraphBuilder<N,E> withEdge( N sourceNodeValue, N destinationNodeValue, String edgeLabel ) {
        return withEdge( sourceNodeValue, destinationNodeValue, edgeLabel, null );
    }

    public GraphBuilder<N,E> withEdge( N sourceNodeValue, N destinationNodeValue, String edgeLabel, E edgeValue ) {
        long sourceNodeKey      = fetchNodeKeyWithConditionalCreation( sourceNodeValue );
        long destinationNodeKey = fetchNodeKeyWithConditionalCreation( destinationNodeValue );

        this.graph = this.graph.addEdge( sourceNodeKey, destinationNodeKey, edgeLabel, edgeValue ).getUpdatedGraph();

        return this;
    }

    private long fetchNodeKeyWithConditionalCreation( N nodeValue ) {
        Object nodePK  = nodeValueToPKFunc.invoke( nodeValue );
        Long   nodeKey = nodeIndex.get( nodePK );

        if ( nodeKey == null ) {
            GraphAddition<N,E> nodeCreationTuple = this.graph.addNode( nodeValue );

            this.graph = nodeCreationTuple.getUpdatedGraph();
            nodeKey = nodeCreationTuple.getNewKey();

            nodeIndex.put( nodePK, nodeKey );
        }

        return nodeKey.longValue();
    }

    public Graph<N,E> build() {
        return graph;
    }

}
