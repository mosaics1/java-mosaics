package mosaics.collections.immutable.graphs;

import mosaics.fp.FP;
import mosaics.fp.collections.RRBVector;
import mosaics.fp.Try;
import mosaics.fp.Tryable;
import mosaics.fp.collections.ConsList;
import mosaics.fp.collections.FPIterable;
import mosaics.fp.collections.FPOption;
import mosaics.lang.NotFoundException;
import mosaics.lang.QA;
import mosaics.lang.functions.Function1;
import mosaics.lang.functions.Function2;

import java.util.Objects;

import static mosaics.lang.Backdoor.cast;


/**
 * An immutable implementation of the Graph interface.  Modifications to the graph create new instances
 * of the graph.
 */
public class RRBGraph<N,E> implements Graph<N,E> {

    public static final Graph EMPTY_INSTANCE = new RRBGraph();


    private final long                                           nodeCount;
    private final long                                           edgeCount;
    private final RRBVector<Node<N,E>>                           allNodes;
    private final RRBVector<Edge<E>>                             allEdges;

    private final Function1<ConsList<Edge<E>>,ConsList<Edge<E>>> edgeSorter;
    private final Function2<Node<N,E>,Graph<N,E>,Tryable<Void>>  nodeValidator;


    public RRBGraph() {
        this( 0, 0, RRBVector.emptyVector(), RRBVector.emptyVector(), edges -> edges, (node,graph) -> Try.succeeded(null) );
    }

    RRBGraph(
        long nodeCount, long edgeCount, RRBVector<Node<N,E>> nodes, RRBVector<Edge<E>> edges,
        Function1<ConsList<Edge<E>>,ConsList<Edge<E>>> edgeSorter,
        Function2<Node<N,E>,Graph<N,E>,Tryable<Void>>  nodeValidator
    ) {
        QA.isGTEZero(nodeCount, "nodeCount");
        QA.isGTEZero(edgeCount, "edgeCount");

        this.nodeCount     = nodeCount;
        this.edgeCount     = edgeCount;
        this.allNodes      = nodes;
        this.allEdges      = edges;
        this.edgeSorter    = edgeSorter;
        this.nodeValidator = nodeValidator;
    }

    public Graph<N,E> withEdgeSorter( Function1<ConsList<Edge<E>>,ConsList<Edge<E>>> newEdgeSorter ) {
        return new RRBGraph<>( nodeCount, edgeCount, allNodes, allEdges, newEdgeSorter, nodeValidator );
    }

    public Graph<N,E> withNodeValidator( Function2<Node<N,E>,Graph<N,E>,Tryable<Void>> newNodeValidator ) {
        return new RRBGraph<>( nodeCount, edgeCount, allNodes, allEdges, edgeSorter, newNodeValidator );
    }

    public long getNodeCount() {
        return nodeCount;
    }

    public long getEdgeCount() {
        return edgeCount;
    }

    public GraphAddition<N,E> addNode( N nodeValue ) {
        long                 newNodeKey         = allNodes.size();
        Node<N,E>            newNode            = new Node<>( newNodeKey, nodeValue, FP.nil(), FP.nil() );

        throwIfInvalidNodeUpdate( newNode );

        RRBVector<Node<N,E>> updatedNodesVector = allNodes.add( newNode );
        RRBGraph<N, E>       updatedGraph       = new RRBGraph<>( nodeCount+1, edgeCount, updatedNodesVector, allEdges, edgeSorter, nodeValidator );

        return new GraphAddition<>( updatedGraph, newNodeKey );
    }

    public GraphAddition<N,E> addEdge( long sourceNodeKey, long destNodeKey, String edgeLabel, E edgeValue ) {
        long                 newEdgeKey             = allEdges.size();
        Node<N,E>            sourceNode             = getNodeMandatory( sourceNodeKey );
        Node<N,E>            destNode               = getNodeMandatory( destNodeKey );
        Edge<E>              newEdge                = new Edge<>(newEdgeKey, edgeLabel, edgeValue, sourceNodeKey, destNodeKey);
        Node<N,E>            updatedSourceNode      = sourceNode.withOutboundEdgeAdded(newEdge, edgeSorter);
        Node<N,E>            updatedDestinationNode = destNode.withInboundEdgeAdded(newEdge, edgeSorter);

        throwIfInvalidNodeUpdate( updatedSourceNode );
        throwIfInvalidNodeUpdate( updatedDestinationNode );

        RRBVector<Edge<E>>   updatedEdgesVector     = allEdges.add( newEdge );
        RRBVector<Node<N,E>> updatedNodesVector     = allNodes
            .set( updatedDestinationNode.getNodeKey(), updatedDestinationNode )
            .set( updatedSourceNode.getNodeKey(), updatedSourceNode );

        RRBGraph<N, E>       updatedGraph           = new RRBGraph<>( nodeCount, edgeCount+1, updatedNodesVector, updatedEdgesVector, edgeSorter, nodeValidator );

        return new GraphAddition<>( updatedGraph, newEdgeKey );
    }

    public FPOption<Node<N,E>> getNode( long nodeKey ) {
        try {
            return nodeKey >= allNodes.size() ? FP.emptyOption() : allNodes.get(nodeKey);
        } catch ( IndexOutOfBoundsException ex ) {
            throw new NotFoundException( "Node " + nodeKey + " does not exist" );
        }
    }

    public FPOption<Edge<E>> getEdge( long edgeKey ) {
        try {
            return edgeKey >= allEdges.size() ? FP.emptyOption() : allEdges.get(edgeKey);
        } catch ( IndexOutOfBoundsException ex ) {
            throw new NotFoundException( "Edge " + edgeKey + " does not exist" );
        }
    }

    public Graph<N, E> updateNode( long nodeKey, N newNodeValue ) {
        Node<N, E>            originalNode       = getNodeMandatory( nodeKey );
        Node<N, E>            updatedNode        = originalNode.withNodeValue( newNodeValue );
        RRBVector<Node<N, E>> updatedNodesVector = allNodes.set( nodeKey, updatedNode );

        throwIfInvalidNodeUpdate( updatedNode );

        return new RRBGraph<>( nodeCount, edgeCount, updatedNodesVector, allEdges, edgeSorter, nodeValidator );
    }

    public Graph<N, E> updateEdge( long edgeKey, String newEdgeLabel, E newEdgeValue ) {
        Edge<E>            origEdge               = getEdgeMandatory( edgeKey );
        Edge<E>            updatedEdge            = origEdge.withEdgeLabel( newEdgeLabel ).withEdgeValue( newEdgeValue );
        RRBVector<Edge<E>> updatedEdgesVector     = allEdges.set( edgeKey, updatedEdge );
        Node<N,E>          sourceNode             = getNodeMandatory( origEdge.getSourceNodeKey() );
        Node<N,E>          destinationNode        = getNodeMandatory( origEdge.getDestinationNodeKey() );

        Node<N,E>          updatedSourceNode      = sourceNode
                                                      .withOutboundEdgeRemoved( origEdge )
                                                      .withOutboundEdgeAdded( updatedEdge, edgeSorter );

        Node<N,E>          updatedDestinationNode = destinationNode
                                                      .withInboundEdgeRemoved( origEdge )
                                                      .withInboundEdgeAdded( updatedEdge, edgeSorter );

        throwIfInvalidNodeUpdate( updatedSourceNode );
        throwIfInvalidNodeUpdate( updatedDestinationNode );


        RRBVector<Node<N,E>> updatedNodesVector   = allNodes
                                                      .set( updatedSourceNode.getNodeKey(), updatedSourceNode )
                                                      .set( updatedDestinationNode.getNodeKey(), updatedDestinationNode );

        return new RRBGraph<>( nodeCount, edgeCount, updatedNodesVector, updatedEdgesVector, edgeSorter, nodeValidator );
    }

    public Graph<N, E> removeNode( long nodeKey ) {
        Node<N,E>             nodeToRemove          = getNodeMandatory( nodeKey );
        FPIterable<Edge<E>>   edgesToRemove         = nodeToRemove.getInboundEdges().and(nodeToRemove.getOutboundEdges());

        RRBGraph<N,E>         graphWithEdgesRemoved = edgesToRemove.fold(this, (g,e) -> cast(g.removeEdge(e.getEdgeKey())) );
        RRBVector<Node<N, E>> updatedNodesVector    = graphWithEdgesRemoved.allNodes.clear( nodeKey );

        return new RRBGraph<>( nodeCount-1, edgeCount-edgesToRemove.count(), updatedNodesVector, graphWithEdgesRemoved.allEdges, edgeSorter, nodeValidator );
    }

    public Graph<N, E> removeEdge( long edgeKey ) {
        Edge<E>   edge               = getEdgeMandatory( edgeKey );
        Node<N,E> sourceNode         = getNodeMandatory( edge.getSourceNodeKey() );
        Node<N,E> destinationNode    = getNodeMandatory( edge.getDestinationNodeKey() );

        Node<N,E> newSourceNode      = sourceNode.withOutboundEdgeRemoved( edge );
        Node<N,E> newDestinationNode = destinationNode.withInboundEdgeRemoved( edge );

        throwIfInvalidNodeUpdate( newSourceNode );
        throwIfInvalidNodeUpdate( newDestinationNode );

        RRBVector<Node<N, E>> updatedNodesVector = allNodes
            .set( newSourceNode.getNodeKey(), newSourceNode )
            .set( newDestinationNode.getNodeKey(), newDestinationNode );

        RRBVector<Edge<E>> updatedEdgesVector = allEdges.clear( edgeKey );

        return new RRBGraph<>( nodeCount, edgeCount-1, updatedNodesVector, updatedEdgesVector, edgeSorter, nodeValidator );
    }


    public int hashCode() {
        return Objects.hash(nodeCount,edgeCount);
    }

    public boolean equals( Object obj ) {
        if ( obj == null || obj.getClass() != RRBGraph.class ) {
            return false;
        }

        RRBGraph other = cast( obj );
        return this.hashCode() == other.hashCode()
            && this.allNodes.equals( other.allNodes )
            && this.allEdges.equals( other.allEdges );
    }

    public String toString() {
        return "RRBGraph("+nodeCount+","+edgeCount+")";
    }


    private void throwIfInvalidNodeUpdate( Node<N,E> n ) {
        nodeValidator.invoke( n, this ).throwIffFailure();
    }

}
