package mosaics.collections.immutable.graphs;

import lombok.Value;
import lombok.With;


@Value
public class Edge<EV> {
          private long   edgeKey;
    @With private String edgeLabel;
    @With private EV     edgeValue;

          private long   sourceNodeKey;
          private long   destinationNodeKey;
}
