package mosaics.collections.immutable.graphs;

import mosaics.fp.Tryable;
import mosaics.fp.collections.ConsList;
import mosaics.fp.collections.FPOption;
import mosaics.lang.Backdoor;
import mosaics.lang.NotFoundException;
import mosaics.lang.functions.Function1;
import mosaics.lang.functions.Function2;


/**
 * An immutable graph representation.  This means that mutations create a new graph, reusing data structures
 * where possible to conserve memory and effort.  The advantage of using an immutable graph is the reduction
 * in contention when reading;  which is a benefit so long as the rate of mutation is low or none.
 *
 * The supported types of graphs are directional acyclic or cyclic graphs with optional contents at each
 * node and edge.  Every node and edge has their own unique identifier (aka key) that may be used to look
 * that node or edge up directly.  Every edge also has a label, which does not have to be unique and is used
 * to identify out edges during path traversal.  Back edges, loops and repeat edges are supported.
 *
 * @param <NV> Node Value Type
 * @param <EV> Edge Value Type
 */
public interface Graph<NV,EV> {

    public static <NV,EV> Graph<NV,EV> emptyGraph() {
        return Backdoor.cast( RRBGraph.EMPTY_INSTANCE );
    }

    public Graph<NV,EV> withEdgeSorter( Function1<ConsList<Edge<EV>>,ConsList<Edge<EV>>> newEdgeSorter );
    public Graph<NV,EV> withNodeValidator( Function2<Node<NV,EV>,Graph<NV,EV>,Tryable<Void>> newNodeValidator );

    public long getNodeCount();
    public long getEdgeCount();

    public GraphAddition<NV,EV> addNode( NV nodeValue );
    public GraphAddition<NV,EV> addEdge( long sourceNodeKey, long destNodeKey, String edgeLabel, EV edgeValue );

    public FPOption<Node<NV,EV>> getNode( long nodeKey );
    public FPOption<Edge<EV>> getEdge( long edgeKey );

    public Graph<NV,EV> updateNode( long nodeKey, NV newNodeValue );
    public Graph<NV,EV> updateEdge( long edgeKey, String newEdgeLabel, EV newEdgeValue );

    public Graph<NV,EV> removeNode( long edgeKey );
    public Graph<NV,EV> removeEdge( long edgeKey );


    public default Node<NV,EV> getNodeMandatory( long nodeKey ) {
        return getNode(nodeKey).orElseThrow( () -> {throw new NotFoundException("Node " + nodeKey + " does not exist");} );
    }

    public default Edge<EV> getEdgeMandatory( long edgeKey ) {
        return getEdge(edgeKey).orElseThrow( () -> {throw new NotFoundException("Edge " + edgeKey + " does not exist");} );
    }

    public default GraphWalker<NV,EV,Object> createGraphWalker() {
        return createGraphWalker( 0, null );
    }

    public default <S> GraphWalker<NV,EV,S> createGraphWalker( S initialSessionObject ) {
        return createGraphWalker( 0, initialSessionObject );
    }

    public default <S> GraphWalker<NV,EV,S> createGraphWalker( long initialNodeKey ) {
        return createGraphWalker( initialNodeKey, null );
    }

    public default <S> GraphWalker<NV,EV,S> createGraphWalker( long initialNodeKey, S initialSessionObject ) {
        return new GraphWalker<NV,EV,S>( this, getNodeMandatory(initialNodeKey) )
            .withSessionObjects( ConsList.singleton(initialSessionObject) );
    }

}

