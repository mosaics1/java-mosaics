package mosaics.collections.immutable.trees;

import mosaics.fp.FP;
import mosaics.fp.collections.FPIterator;
import mosaics.fp.collections.FPOption;
import mosaics.fp.collections.TreeIterators;
import mosaics.lang.functions.BooleanFunction1;
import mosaics.lang.functions.Function1;

import java.io.PrintStream;
import java.util.Map;
import java.util.function.Predicate;


public interface FPTreeMap<K,V> extends Iterable<TreeNode<K,V>> {
    public boolean isEmpty();
    public FPOption<V> get( K key );
    public FPTreeMap<K,V> put( K key, V value );
    public FPTreeMap<K,V> remove( K key );

    public FPOption<Map.Entry<K,V>> getPreviousEntry( K key );
    public FPOption<Map.Entry<K,V>> getNextEntry( K key );
    public FPOption<Map.Entry<K,V>> getClosestEntry( K key );

    /**
     * Returns a description of the binary trees root node.  Useful for custom
     * traversals of the tree.
     */
    public FPOption<TreeNode<K,V>> getRoot();

    /**
     * Returns the number of values stored in this tree.
     */
    public int getValueCount();

    /**
     * Returns the height of this tree.  An empty tree has a height of zero, a tree
     * with a single value has the height of one.
     */
    public int getHeightCount();
    public int getHeightCount( K key );

    public FPOption<FPTreeMap<K,V>> getLHS();
    public FPOption<FPTreeMap<K,V>> getRHS();

    /**
     * Returns a description of the binary trees node that stores the value for the specified
     * key.  Useful for custom traversals of the tree.
     */
    public FPOption<TreeNode<K,V>> getNodeFor( K key );


    public default FPOption<Map.Entry<K,V>> minEntry() {
         return findEntry( t -> t.getLHS().isEmpty(), FPTreeMap::getLHS );
    }

    public default FPOption<Map.Entry<K,V>> maxEntry() {
        return findEntry( t -> t.getRHS().isEmpty(), FPTreeMap::getRHS );
    }

    /**
     * Iterate over every key/value pair in ascending key order.
     */
    public default FPIterator<TreeNode<K,V>> entriesAsc() {
        return TreeIterators.inorderDepthFirstIteratorOpt( getRoot().map(any -> this), FPTreeMap::getLHS, FPTreeMap::getRHS )
            .flatMap( FPTreeMap::getRoot );
    }

    public default FPIterator<K> keys() {
        return entriesAsc().map( TreeNode::getKey );
    }

    public default FPIterator<V> values() {
        return entriesAsc().map( TreeNode::getValue );
    }

    /**
     * Iterate over every key/value pair.  By default this will be a breadth first tree
     * walk.
     */
    public default FPIterator<TreeNode<K,V>> iterator() {
        return TreeIterators.breadthFirstIteratorOpt( getRoot().map(any -> this), FPTreeMap::getLHS, FPTreeMap::getRHS )
            .flatMap( FPTreeMap::getRoot );
    }

    public default boolean hasContents() {
        return !isEmpty();
    }

    public default void dumpTree() {
        dumpTree( System.out );
    }

    public default void dumpTree( PrintStream out ) {
        iterator().forEachRemaining( node -> printAdjacencyListLineFor(out,node) );
    }

    public default FPTreeMap<K,V> filterValues( Predicate<V> predicate ) {
        return entriesAsc().fold(
            this,
            (agg,node) -> predicate.test(node.getValue()) ? agg : agg.remove(node.getKey())
        );
    }

    private static <K,V> void printAdjacencyListLineFor( PrintStream out, TreeNode<K, V> node ) {
        out.print( node.getKey() );
        out.print( " -> " );
        out.print( node.getLhsKey().map(Object::toString).orElse("-") );
        out.print( "," );
        out.print( node.getRhsKey().map(Object::toString).orElse("-") );
        out.println();
    }

    private FPOption<Map.Entry<K,V>> findEntry( BooleanFunction1<FPTreeMap<K,V>> hasGoalBeenReachedF, Function1<FPTreeMap<K,V>,FPOption<FPTreeMap<K,V>>> nextF ) {
        if ( isEmpty() ) {
            return FP.emptyOption();
        }

        FPOption<FPTreeMap<K,V>> currentCandidate = FP.option(this);

        while ( currentCandidate.hasContents() ) {
            FPTreeMap<K, V> currentTree = currentCandidate.get();

            if ( hasGoalBeenReachedF.invoke(currentTree) ) {
                return currentCandidate.flatMap( FPTreeMap::getRoot ).cast();
            }

            currentCandidate = nextF.invoke(currentTree);
        }

        return FP.emptyOption();
    }
}
