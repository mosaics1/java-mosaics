package mosaics.collections.immutable.trees;

import lombok.AllArgsConstructor;
import lombok.Value;
import mosaics.fp.FP;
import mosaics.fp.collections.FPOption;

import java.util.Map;


/**
 * Describes a position within a binary tree.
 */
@Value
@AllArgsConstructor
public class TreeNode<K,V> implements Map.Entry<K,V> {
    private K           key;
    private V           value;
    private FPOption<K> lhsKey;
    private FPOption<K> rhsKey;

    public TreeNode( K key, V value ) {
        this( key, value, FP.emptyOption(), FP.emptyOption() );
    }

    public TreeNode( K key, V value, K lhs, K rhs ) {
        this( key, value, FP.option(lhs), FP.option(rhs) );
    }

    public V setValue( V value ) {
        throw new UnsupportedOperationException();
    }
}
