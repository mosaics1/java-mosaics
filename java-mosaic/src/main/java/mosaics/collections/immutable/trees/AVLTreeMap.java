package mosaics.collections.immutable.trees;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Value;
import lombok.With;
import mosaics.fp.FP;
import mosaics.fp.collections.ConsList;
import mosaics.fp.collections.FPIterable;
import mosaics.fp.collections.FPIterator;
import mosaics.fp.collections.FPOption;
import mosaics.fp.collections.Tuple2;
import mosaics.lang.ComparatorX;
import mosaics.lang.ComparisonResult;
import mosaics.lang.functions.BooleanFunction1;
import mosaics.lang.functions.Function0;
import mosaics.lang.functions.Function1;
import mosaics.lang.functions.Function1WithThrows;

import java.util.Map;
import java.util.Objects;
import java.util.function.Function;

import static mosaics.lang.Backdoor.cast;


/**
 * An immutable implementation of the AVL tree algorithm.  AVL trees are binary trees
 * that remain balanced as new nodes are inserted and old nodes removed by rotating the
 * tree at a node from its 'heavier' side to its 'lighter' side.  AVL trees offer similar
 * performance to Red/Black trees;  where they differ is that Red/Black trees tend to
 * be faster at inserting (less re-balancing occurs) and are slower at reading because
 * they are less balanced than AVL trees.
 */
public class AVLTreeMap<K,V> implements FPTreeMap<K,V> {
    private static final FPTreeMap<?,?> EMPTY = empty( AVLTreeMap::defaultCompare );

    public static final <K,V> FPTreeMap<K,V> empty() {
        return cast(EMPTY);
    }

    public static final <K,V> FPTreeMap<K,V> empty( ComparatorX<K> comparator ) {
        return new AVLTreeMap<>( comparator, FP.emptyOption() );
    }

    public static <K,V> FPTreeMap<K,V> of( K k1, V v1 ) {
        return AVLTreeMap.<K,V>empty().put(k1,v1);
    }

    public static <K,V> FPTreeMap<K,V> of( K k1, V v1, K k2, V v2 ) {
        return AVLTreeMap.<K,V>empty()
            .put(k1,v1)
            .put(k2,v2);
    }

    public static <K,V> FPTreeMap<K,V> of( K k1, V v1, K k2, V v2, K k3, V v3 ) {
        return AVLTreeMap.<K,V>empty()
            .put(k1,v1)
            .put(k2,v2)
            .put(k3,v3);
    }

    public static <K,V> FPTreeMap<K,V> of( K k1, V v1, K k2, V v2, K k3, V v3, K k4, V v4 ) {
        return AVLTreeMap.<K,V>empty()
            .put(k1,v1)
            .put(k2,v2)
            .put(k3,v3)
            .put(k4,v4);
    }

    public static <K,V> FPTreeMap<K,V> of( K k1, V v1, K k2, V v2, K k3, V v3, K k4, V v4, K k5, V v5 ) {
        return AVLTreeMap.<K,V>empty()
            .put(k1,v1)
            .put(k2,v2)
            .put(k3,v3)
            .put(k4,v4)
            .put(k5,v5);
    }

    public static <K,V> FPTreeMap<K,V> of( K k1, V v1, K k2, V v2, K k3, V v3, K k4, V v4, K k5, V v5, K k6, V v6 ) {
        return AVLTreeMap.<K,V>empty()
            .put(k1,v1)
            .put(k2,v2)
            .put(k3,v3)
            .put(k4,v4)
            .put(k5,v5)
            .put(k6,v6);
    }

    public static <K,V> FPTreeMap<K,V> of( K k1, V v1, K k2, V v2, K k3, V v3, K k4, V v4, K k5, V v5, K k6, V v6, K k7, V v7 ) {
        return AVLTreeMap.<K,V>empty()
            .put(k1,v1)
            .put(k2,v2)
            .put(k3,v3)
            .put(k4,v4)
            .put(k5,v5)
            .put(k6,v6)
            .put(k7,v7);
    }


    @SuppressWarnings("unchecked")
    private static int defaultCompare( Object x, Object y ) {
        return ComparatorX.compare( (Comparable) x, (Comparable) y );
    }


    private final ComparatorX<K>         keyComparator;
    private final FPOption<AVLNode<K,V>> root;


    private AVLTreeMap( ComparatorX<K> keyComparator, FPOption<AVLNode<K,V>> root ) {
        this.keyComparator = keyComparator;
        this.root          = root;
    }


    public boolean isEmpty() {
        return root.isEmpty();
    }

    public FPOption<V> get( K key ) {
        return getAVLNodeFor( key ).map( AVLNode::getValue );
    }

    public int getValueCount() {
        return getNodeCountFrom(root);
    }

    public int getHeightCount() {
        return getMaxHeightOf( root );
    }

    public int getHeightCount( K key ) {
        return getMaxHeightOf( getAVLNodeFor(key) );
    }

    public FPOption<TreeNode<K,V>> getRoot() {
        return root.map( AVLNode::toNodeDescription );
    }

    public FPOption<FPTreeMap<K,V>> getLHS() {
        return getSubTree( AVLNode::getLhs );
    }

    public FPOption<FPTreeMap<K,V>> getRHS() {
        return getSubTree( AVLNode::getRhs );
    }

    public FPOption<TreeNode<K,V>> getNodeFor( K key ) {
        return root.flatMap( node -> node.getNodeFor(keyComparator,key) );
    }

    public FPTreeMap<K,V> put( K key, V value ) {
        return this.find( key )
                   .whenFoundUpdate( exactNode -> FP.option(exactNode.withValue(value)) )
                   .elseUpdateClosestMatchIsGreaterThanTarget( closestNode -> closestNode.withLhs(new AVLNode<>(key,value)))
                   .elseUpdateClosestMatchIsLessThanTarget( closestNode -> closestNode.withRhs(new AVLNode<>(key,value)))
                   .elseNoMatchAtAll( () -> FP.option(new AVLNode<>(key,value)) )
                   .updateTree();
    }

    public FPTreeMap<K,V> remove( K key ) {
        return this.find( key )
                   .whenFoundUpdate( AVLNode::removeSelf )
                   .updateTree();
    }

    public FPOption<Map.Entry<K,V>> getPreviousEntry( K targetKey ) {
        return findEntry(
            currentNode -> {
                boolean isTargetLTCurrentNode = keyComparator.isLT(currentNode.getKey(), targetKey);

                // NB the previousEntryKey is always less than the targetKey, thus if it is not here
                //    then we have not hit the goal so exit fast
                if ( !isTargetLTCurrentNode ) {
                    return false;
                } else {
                    // so the currentNodeKey is less than the targetKey. Good.  Now, is there
                    // another key in the right branch that is also less than the targetKey?
                    // if so, we have not finished.
                    boolean isNextNodeABetterCandidate = currentNode.getNextNode().hasFirst(
                        next -> keyComparator.isLT( next.getKey(), targetKey )
                    );

                    return !isNextNodeABetterCandidate;
                }
            },
            currentNode -> keyComparator.isLTE(targetKey,currentNode.getKey()) ? currentNode.getLhs() : currentNode.getRhs()
        ).cast();
    }

    public FPOption<Map.Entry<K,V>> getNextEntry( K targetKey ) {
        return findEntry(
            currentNode -> {
                boolean isTargetGTCurrentNode = keyComparator.isGT(currentNode.getKey(),targetKey);

                // NB the nextEntryKey is always greater than the targetKey, thus if it is not here
                //    then we have not hit the goal so exit fast
                if ( !isTargetGTCurrentNode ) {
                        return false;
                } else {
                    // so the currentNodeKey is greater than the targetKey. Good.  Now, is there
                    // another key in the left branch that is also greater than the targetKey?
                    // if so, we have not finished.
                    boolean isPrevNodeABetterCandidate = currentNode.getPreviousNode().hasFirst(
                        prev -> keyComparator.isGT( prev.getKey(), targetKey )
                    );

                    return !isPrevNodeABetterCandidate;
                }
            },
            t -> keyComparator.isLT(targetKey,t.getKey()) ? t.getLhs() : t.getRhs()
        ).cast();
    }


    public FPOption<Map.Entry<K,V>> getClosestEntry( K targetKey ) {
        return findEntry(
            currentNode -> {
                return switch (keyComparator.compare(currentNode.getKey(),targetKey)) {
                    case LT -> !hasCloserCandidateOnRHS(currentNode,targetKey);
                    case EQ -> true;
                    case GT -> !hasCloserCandidateOnLHS(currentNode,targetKey);
                };
            },
            t -> keyComparator.isLT(targetKey,t.getKey()) ? t.getLhs() : t.getRhs()
        ).cast();
    }

    private boolean hasCloserCandidateOnLHS( AVLNode<K,V> currentNode, K targetKey ) {
        return currentNode.getPreviousNode().hasFirst(
            prev -> isCloser(targetKey,currentNode.getKey(), prev.getKey())
        );
    }

    private boolean hasCloserCandidateOnRHS( AVLNode<K,V> currentNode, K targetKey ) {
        return currentNode.getNextNode().hasFirst(
            next -> isCloser(targetKey,currentNode.getKey(), next.getKey())
        );
    }

    private boolean isCloser( K targetKey, K currentKey, K candidateKey ) {
        int currentNodeDistance = Math.abs(keyComparator.measureDistanceBetween(currentKey, targetKey));
        int otherNodeDistance   = Math.abs(keyComparator.measureDistanceBetween(candidateKey, targetKey));

        return otherNodeDistance < currentNodeDistance;
    }

    private FPOption<AVLNode<K,V>> findEntry( BooleanFunction1<AVLNode<K,V>> hasGoalBeenReachedF, Function1<AVLNode<K,V>,FPOption<AVLNode<K,V>>> nextF ) {
        if ( isEmpty() ) {
            return FP.emptyOption();
        }

        FPOption<AVLNode<K,V>> currentCandidate = root;

        while ( currentCandidate.hasContents() ) {
            AVLNode<K, V> currentNode = currentCandidate.get();

            if ( hasGoalBeenReachedF.invoke(currentNode) ) {
                return currentCandidate;
            }

            currentCandidate = nextF.invoke(currentNode);
        }

        return FP.emptyOption();
    }

    private TreeUpdater<K,V> find( K targetKey ) {
        ConsList<TreeStep<K,V>> pathVisitedSoFar = cast(ConsList.NIL);

        if ( root.isEmpty() ) {
            return new EmptyTreeUpdater<>(this);
        } else {
            AVLNode<K,V> current = root.get();

            while (true) {
                ComparisonResult comparisonResult = keyComparator.compare( targetKey, current.key );

                if ( comparisonResult.isEQ() ) {
                    return new FoundTargetNodeTreeUpdater<>( this, pathVisitedSoFar, current );
                } else if ( comparisonResult.isLT() ) {
                    if ( current.getLhs().isEmpty() ) {
                        return new ClosestMatchIsGreaterThanTargetTreeUpdater<>( this, pathVisitedSoFar, current );
                    } else {
                        pathVisitedSoFar = pathVisitedSoFar.append( new LHS<>(current) );

                        current = current.getLhs().get();
                    }
                } else {
                    if ( current.getRhs().isEmpty() ) {
                        return new ClosestMatchIsLessThanTargetTreeUpdater<>( this, pathVisitedSoFar, current );
                    } else {
                        pathVisitedSoFar = pathVisitedSoFar.append( new RHS<>(current) );

                        current = current.getRhs().get();
                    }
                }
            }
        }
    }

    public String toString() {
        return "AVLTreeMap("+root.map(AVLNode::toString).orElse("-")+")";
    }

    @Override
    public int hashCode() {
        return 17 + hashCodeOf( root );
    }

    @Override
    public boolean equals( Object o ) {
        if ( o instanceof FPTreeMap other ) {
            FPIterator<TreeNode<K,V>> it1 = this.entriesAsc();
            FPIterator<TreeNode<K,V>> it2 = cast(other.entriesAsc());

            return it1.zipOpt(it2).ifEvery( AVLTreeMap::doOptionalTreeNodesEqual );
        } else {
            return false;
        }
    }

    private static <K,V> boolean doOptionalTreeNodesEqual( Tuple2<FPOption<TreeNode<K, V>>, FPOption<TreeNode<K, V>>> tuple ) {
        FPOption<TreeNode<K, V>> a = tuple.getFirst();
        FPOption<TreeNode<K, V>> b = tuple.getSecond();

        if ( a.isEmpty() || b.isEmpty() ) {
            return false;
        } else {
            return Objects.equals( a.get().getKey(), b.get().getKey() )
                && Objects.equals( a.get().getValue(), b.get().getValue() );
        }
    }

    private FPOption<AVLNode<K,V>> getAVLNodeFor( K key ) {
        return root.flatMap( node -> node.get(keyComparator,key) );
    }

    private FPOption<FPTreeMap<K,V>> getSubTree( Function1WithThrows<AVLNode<K,V>, FPIterable<AVLNode<K,V>>> subtreeGetter ) {
        return root.flatMap( subtreeGetter )
            .map( node -> new AVLTreeMap<>(keyComparator, FP.option(node)) );
    }

    private static <K,V> boolean areIdentical( FPOption<AVLNode<K, V>> node1, FPOption<AVLNode<K, V>> node2 ) {
        if ( node1.isEmpty() && node2.isEmpty() ) {
            return true;
        } else if ( node1.hasContents() && node2.hasContents() ) {
            return node1.get() == node2.get();
        } else {
            return false;
        }
    }

    private static <K,V> int getNodeCountFrom( FPOption<AVLNode<K,V>> node ) {
        return node.map(AVLNode::getNodeCount ).orElse(0);
    }

    private static <K,V> int getMaxHeightOf( FPOption<AVLNode<K,V>> node ) {
        return node.map(AVLNode::getMaxHeight ).orElse(0);
    }

    private static <K,V> int hashCodeOf( FPOption<AVLNode<K,V>> node ) {
        return node.map( AVLNode::hashCode ).orElse( 0 );
    }

    private static <K,V> AVLTreeMap<K, V> updateParentNodes( AVLTreeMap<K,V> originalTree, ConsList<TreeStep<K,V>> path, FPOption<AVLNode<K,V>> node, FPOption<AVLNode<K, V>> updatedNode ) {
        if ( areIdentical(updatedNode,node) ) {
            return originalTree;
        }

        FPOption<AVLNode<K,V>> updatedRoot = path.fold(
            updatedNode,
            (child,oldParent) -> FP.option(oldParent.updateChild(child).rebalance())
        );

        return new AVLTreeMap<>( originalTree.keyComparator, updatedRoot );
    }

    @Getter
    @AllArgsConstructor
    static class AVLNode<K,V> implements Map.Entry<K,V> {
        private final K                      key;
        private final V                      value;

        private final int                    nodeCount;
        private final int                    maxHeight;
        private final int                    hashCode;

        private final FPOption<AVLNode<K,V>> lhs;
        private final FPOption<AVLNode<K,V>> rhs;


        public AVLNode( K key, V value ) {
            this( key, value, 1, 1, key.hashCode(), FP.emptyOption(), FP.emptyOption() );
        }

        @Override
        public int hashCode() {
            return hashCode;
        }

        public String toString() {
            return "Entry("+key+","+value+")";
        }

        public FPOption<AVLNode<K,V>> get( ComparatorX<K> keyComparator, K searchKey ) {
            return switch (keyComparator.compare(searchKey,this.key)) {
                case LT -> lhs.flatMap(childNode -> childNode.get(keyComparator,searchKey));
                case EQ -> FP.option( this );
                case GT -> rhs.flatMap(childNode -> childNode.get(keyComparator,searchKey));
            };
        }

        public FPOption<TreeNode<K,V>> getNodeFor( ComparatorX<K> keyComparator, K searchKey ) {
            return switch (keyComparator.compare(searchKey,this.key)) {
                case LT -> lhs.flatMap(childNode -> childNode.getNodeFor(keyComparator,searchKey));
                case EQ -> FP.option(toNodeDescription());
                case GT -> rhs.flatMap(childNode -> childNode.getNodeFor(keyComparator,searchKey));
            };
        }

        public FPOption<AVLNode<K,V>> getPreviousNode() {
            FPOption<AVLNode<K,V>> result = this.getLhs();

            while (result.hasValue() && result.get().getRhs().hasValue() ) {
                result = result.flatMap(AVLNode::getRhs);
            }

            return result;
        }

        public FPOption<AVLNode<K,V>> getNextNode() {
            FPOption<AVLNode<K,V>> result = this.getRhs();

            while (result.hasValue() && result.get().getLhs().hasValue() ) {
                result = result.flatMap(AVLNode::getLhs);
            }

            return result;
        }

        public TreeNode<K, V> toNodeDescription() {
            return new TreeNode<>(key, value, lhs.map(AVLNode::getKey), rhs.map(AVLNode::getKey));
        }

        public AVLNode<K,V> put( ComparatorX<K> keyComparator, K newKey, V newValue ) {
            return switch (keyComparator.compare(newKey,this.key)) {
                case LT -> putChild( this::withLhs, lhs, keyComparator, newKey, newValue );
                case EQ -> withValue( newValue );
                case GT -> putChild( this::withRhs, rhs, keyComparator, newKey, newValue );
            };
        }

        public AVLNode<K,V> withValue( V newValue ) {
            return Objects.equals( this.value, newValue ) ? this : new AVLNode<>(key,newValue,nodeCount,maxHeight,hashCode,lhs,rhs);
        }

        public AVLNode<K,V> withLhs( AVLNode<K,V> newLHS ) {
            return withLhs( FP.option(newLHS) );
        }

        public AVLNode<K,V> withLhs( FPOption<AVLNode<K,V>> newLHS ) {
            if ( areIdentical(lhs,newLHS) ) {
                return this;
            }

            int newDescendantCount = getNodeCountFrom(newLHS) + getNodeCountFrom(rhs);
            int newHeight          = Math.max( getMaxHeightOf(newLHS), getMaxHeightOf(rhs) );
            int newHashCode        = key.hashCode() + hashCodeOf(newLHS) + hashCodeOf(rhs);

            return new AVLNode<>( key, value, newDescendantCount + 1, newHeight + 1, newHashCode, newLHS, rhs );
        }

        public AVLNode<K,V> withRhs( AVLNode<K,V> newRHS ) {
            return withRhs( FP.option(newRHS) );
        }

        public AVLNode<K,V> withRhs( FPOption<AVLNode<K,V>> newRHS ) {
            if ( areIdentical(rhs,newRHS) ) {
                return this;
            }

            int newDescendantCount = getNodeCountFrom(lhs) + getNodeCountFrom(newRHS);
            int newHeight          = Math.max( getMaxHeightOf(lhs), getMaxHeightOf(newRHS) );
            int newHashCode        = key.hashCode() + hashCodeOf(lhs) + hashCodeOf(newRHS);

            return new AVLNode<>( key, value, newDescendantCount + 1, newHeight + 1, newHashCode, lhs, newRHS );
        }

        public AVLNode<K,V> rotateLeft() {
            if ( getRhs().isEmpty() ) {
                return this;
            }

            AVLNode<K,V> newLHS = this.withRhs( this.getRhs().flatMap(AVLNode::getLhs) );

            return this.getRhs().get().withLhs( newLHS );
        }

        public AVLNode<K,V> rotateRight() {
            if ( getLhs().isEmpty() ) {
                return this;
            }

            AVLNode<K,V> newRHS = this.withLhs( this.getLhs().flatMap(AVLNode::getRhs) );

            return this.getLhs().get().withRhs( newRHS );
        }

        public AVLNode<K,V> insertSubTreeToFarLHSNoRebalance( FPOption<AVLNode<K,V>> subtree ) {
            return withLhs(
                lhs.map(child -> child.insertSubTreeToFarLHSNoRebalance(subtree) )
                   .or( subtree )
            );
        }

        public FPOption<AVLNode<K,V>> remove( ComparatorX<K> keyComparator, K keyToRemove ) {
            return switch (keyComparator.compare(keyToRemove,this.key)) {
                case LT -> FP.option( this.withLhs(lhs.flatMap(child -> child.remove(keyComparator,keyToRemove))) );
                case EQ -> merge(this.lhs,this.rhs);
                case GT -> FP.option( this.withRhs(rhs.flatMap(child -> child.remove(keyComparator,keyToRemove))) );
            };
        }

        private AVLNode<K,V> putChild( Function<FPOption<AVLNode<K,V>>,AVLNode<K,V>> childSetter, FPOption<AVLNode<K,V>> child, ComparatorX<K> keyComparator, K newKey, V newValue ) {
            if ( child.isEmpty() ) {
                return childSetter.apply( FP.option(new AVLNode<>(newKey,newValue)) );
            } else {
                FPOption<AVLNode<K,V>> update = child.map( l -> l.put(keyComparator,newKey,newValue) );

                if ( update.get() == child.get() ) {
                    return this;
                } else {
                    return childSetter.apply( update );
                }
            }
        }

        private AVLNode<K,V> rebalance() {
            return switch (compareSubtreeHeights()) {
                case LL  -> this.rotateRight();
                case LR  -> this.withLhs( this.getLhs().get().rotateLeft() ).rotateRight();
                case Balanced -> this;
                case RL -> this.withRhs( this.getRhs().get().rotateRight() ).rotateLeft();
                case RR -> this.rotateLeft();
            };
        }

        private SubtreeHeightComparison compareSubtreeHeights() {
            int heightDiff = getHeightDiff();

            if ( heightDiff > 1 ) {
                int childHeightDiff = getRhs().map(AVLNode::getHeightDiff).orElse(0);

                return childHeightDiff >= 0 ? SubtreeHeightComparison.RR : SubtreeHeightComparison.RL;
            } else if ( heightDiff < -1 ) {
                int childHeightDiff = getLhs().map(AVLNode::getHeightDiff).orElse(0);

                return childHeightDiff <= 0 ? SubtreeHeightComparison.LL : SubtreeHeightComparison.LR;
            } else {
                return SubtreeHeightComparison.Balanced;
            }
        }

        private int getHeightDiff() {
            return getRhsHeight() - getLhsHeight();
        }

        private int getLhsHeight() {
            return getMaxHeightOf( lhs );
        }

        private int getRhsHeight() {
            return getMaxHeightOf( rhs );
        }

        private static <K,V> FPOption<AVLNode<K, V>> merge( FPOption<AVLNode<K, V>> lhs, FPOption<AVLNode<K, V>> rhs ) {
            if ( lhs.isEmpty() ) {
                return rhs;
            } else if ( rhs.isEmpty() ) {
                return lhs;
            }

            return rhs.map( r -> r.insertSubTreeToFarLHSNoRebalance(lhs).rebalance() );
        }

        public V setValue( V value ) {
            throw new UnsupportedOperationException();
        }

        public FPOption<AVLNode<K,V>> removeSelf() {
            if ( getLhs().isEmpty() ) {
                return getRhs();
            } else if ( getRhs().isEmpty() ) {
                return getLhs();
            } else {
                return merge( getLhs(),getRhs() );
            }
        }
    }

    private static enum SubtreeHeightComparison {
        /**
         * The tree is unbalanced to the left and the unbalanced child node has a 1 or more
         * nodes in its left branch than its right.
         */
        LL,
        /**
         * The tree is unbalanced to the left and the unbalanced child has a 0 or more
         * nodes in its left branch than its right.
         */
        LR,
        /**
         * Both children are within 1 height of each other.
         */
        Balanced,
        /**
         * The tree is unbalanced to the right and the unbalanced child has a 1 or more
         * nodes in its left branch than its right.
         */
        RL,
        /**
         * The tree is unbalanced to the right and the unbalanced child has a 0 or more
         * nodes in its right branch than its right.
         */
        RR;
    }

    private static interface TreeStep<K,V> {
        public AVLNode<K, V> updateChild( FPOption<AVLNode<K, V>> child );
    }

    @Value
    private static class LHS<K,V> implements TreeStep<K,V> {
        private final AVLNode<K,V> node;

        public AVLNode<K, V> updateChild( FPOption<AVLNode<K, V>> child ) {
            return node.withLhs( child );
        }
    }

    @Value
    private static class RHS<K,V> implements TreeStep<K,V> {
        private final AVLNode<K,V> node;

        public AVLNode<K, V> updateChild( FPOption<AVLNode<K, V>> child ) {
            return node.withRhs( child );
        }
    }

    private static interface TreeUpdater<K,V> {
        public TreeUpdater<K,V> whenFoundUpdate( Function1<AVLNode<K,V>,FPOption<AVLNode<K,V>>> newNodeUpdater );
        public TreeUpdater<K,V> elseUpdateClosestMatchIsGreaterThanTarget( Function1<AVLNode<K,V>,AVLNode<K,V>> newNodeUpdater );
        public TreeUpdater<K,V> elseUpdateClosestMatchIsLessThanTarget( Function1<AVLNode<K,V>,AVLNode<K,V>> newNodeUpdater );
        public TreeUpdater<K,V> elseNoMatchAtAll( Function0<FPOption<AVLNode<K,V>>> nodeFactory );
        public AVLTreeMap<K,V> updateTree();
    }


    @With
    @Value
    @AllArgsConstructor
    private static class EmptyTreeUpdater<K,V> implements TreeUpdater<K,V> {
        private final AVLTreeMap<K,V>                    originalTree;
        private final Function0<FPOption<AVLNode<K, V>>> nodeUpdater;

        public EmptyTreeUpdater( AVLTreeMap<K,V> tree ) {
            this( tree, FP::emptyOption );
        }

        public TreeUpdater<K, V> whenFoundUpdate( Function1<AVLNode<K, V>, FPOption<AVLNode<K, V>>> newNodeUpdater ) {
            return this;
        }

        public TreeUpdater<K, V> elseUpdateClosestMatchIsGreaterThanTarget( Function1<AVLNode<K, V>, AVLNode<K, V>> newNodeUpdater ) {
            return this;
        }

        public TreeUpdater<K, V> elseUpdateClosestMatchIsLessThanTarget( Function1<AVLNode<K, V>, AVLNode<K, V>> newNodeUpdater ) {
            return this;
        }

        public TreeUpdater<K, V> elseNoMatchAtAll( Function0<FPOption<AVLNode<K, V>>> nodeUpdater ) {
            return withNodeUpdater( nodeUpdater );
        }

        public AVLTreeMap<K, V> updateTree() {
            return new AVLTreeMap<>( originalTree.keyComparator, nodeUpdater.invoke() );
        }
    }

    @With
    @Value
    @AllArgsConstructor
    private static class FoundTargetNodeTreeUpdater<K,V> implements TreeUpdater<K,V> {
        private final AVLTreeMap<K,V>                                   originalTree;
        private final Function1<AVLNode<K, V>, FPOption<AVLNode<K, V>>> nodeUpdater;
        private final ConsList<TreeStep<K,V>>                           path;
        private final AVLNode<K,V>                                      node;

        public FoundTargetNodeTreeUpdater( AVLTreeMap<K,V> tree, ConsList<TreeStep<K,V>> path, AVLNode<K, V> node ) {
            this( tree, FP::option, path, node );
        }

        public TreeUpdater<K, V> whenFoundUpdate( Function1<AVLNode<K, V>, FPOption<AVLNode<K, V>>> newNodeUpdater ) {
            return withNodeUpdater( newNodeUpdater );
        }

        public TreeUpdater<K, V> elseUpdateClosestMatchIsGreaterThanTarget( Function1<AVLNode<K, V>, AVLNode<K, V>> newNodeUpdater ) {
            return this;
        }

        public TreeUpdater<K, V> elseUpdateClosestMatchIsLessThanTarget( Function1<AVLNode<K, V>, AVLNode<K, V>> newNodeUpdater ) {
            return this;
        }

        public TreeUpdater<K,V> elseNoMatchAtAll( Function0<FPOption<AVLNode<K,V>>> nodeFactory ) {
            return this;
        }

        public AVLTreeMap<K, V> updateTree() {
            FPOption<AVLNode<K, V>> updatedNode = nodeUpdater.invoke( node );

            return updateParentNodes( originalTree, path, FP.option(node), updatedNode );
        }
    }

    @With
    @Value
    @AllArgsConstructor
    private static class ClosestMatchIsGreaterThanTargetTreeUpdater<K,V> implements TreeUpdater<K,V> {
        private final AVLTreeMap<K,V>                         originalTree;
        private final Function1<AVLNode<K, V>, AVLNode<K, V>> nodeUpdater;
        private final ConsList<TreeStep<K,V>>                 path;
        private final AVLNode<K,V>                            node;

        public ClosestMatchIsGreaterThanTargetTreeUpdater( AVLTreeMap<K,V> tree, ConsList<TreeStep<K,V>> path, AVLNode<K, V> node ) {
            this( tree, Function1.identity(), path, node );
        }

        public TreeUpdater<K, V> whenFoundUpdate( Function1<AVLNode<K, V>, FPOption<AVLNode<K, V>>> newNodeUpdater ) {
            return this;
        }

        public TreeUpdater<K, V> elseUpdateClosestMatchIsGreaterThanTarget( Function1<AVLNode<K, V>, AVLNode<K, V>> newNodeUpdater ) {
            return withNodeUpdater( newNodeUpdater );
        }

        public TreeUpdater<K, V> elseUpdateClosestMatchIsLessThanTarget( Function1<AVLNode<K, V>, AVLNode<K, V>> newNodeUpdater ) {
            return this;
        }

        public TreeUpdater<K,V> elseNoMatchAtAll( Function0<FPOption<AVLNode<K,V>>> nodeFactory ) {
            return this;
        }

        public AVLTreeMap<K, V> updateTree() {
            AVLNode<K, V> updatedNode = nodeUpdater.invoke(node);

            return updateParentNodes( originalTree, path, FP.option(node), FP.option(updatedNode) );
        }
    }

    @With
    @Value
    @AllArgsConstructor
    private static class ClosestMatchIsLessThanTargetTreeUpdater<K,V> implements TreeUpdater<K,V> {
        private final AVLTreeMap<K,V>                         originalTree;
        private final Function1<AVLNode<K, V>, AVLNode<K, V>> nodeUpdater;
        private final ConsList<TreeStep<K,V>>                 path;
        private final AVLNode<K,V>                            node;

        public ClosestMatchIsLessThanTargetTreeUpdater( AVLTreeMap<K,V> tree, ConsList<TreeStep<K,V>> path, AVLNode<K, V> node ) {
            this( tree, Function1.identity(), path, node );
        }

        public TreeUpdater<K, V> whenFoundUpdate( Function1<AVLNode<K, V>, FPOption<AVLNode<K, V>>> newNodeUpdater ) {
            return this;
        }

        public TreeUpdater<K, V> elseUpdateClosestMatchIsGreaterThanTarget( Function1<AVLNode<K, V>, AVLNode<K, V>> newNodeUpdater ) {
            return this;
        }

        public TreeUpdater<K, V> elseUpdateClosestMatchIsLessThanTarget( Function1<AVLNode<K, V>, AVLNode<K, V>> newNodeUpdater ) {
            return withNodeUpdater( newNodeUpdater );
        }

        public TreeUpdater<K,V> elseNoMatchAtAll( Function0<FPOption<AVLNode<K,V>>> nodeFactory ) {
            return this;
        }

        public AVLTreeMap<K, V> updateTree() {
            AVLNode<K, V> updatedNode = nodeUpdater.invoke(node);

            return updateParentNodes( originalTree, path, FP.option(node), FP.option(updatedNode) );
        }
    }
}
