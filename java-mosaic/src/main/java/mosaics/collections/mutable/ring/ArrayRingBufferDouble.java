package mosaics.collections.mutable.ring;

import mosaics.collections.SeqDouble;
import mosaics.fp.collections.DoubleIterator;
import mosaics.lang.Backdoor;
import mosaics.lang.MathUtils;


/**
 * A Ring Buffer implemented over an array of double values.  The buffer supports pushing on both the
 * left and right hand sides.  Pushing onto the right hand side extends the max index by one.  If
 * the underlying array has been filled, then the index on the LHS will be dropped and it will become
 * an error case for the dropped index to be accessed.
 */
public class ArrayRingBufferDouble implements RingBufferDouble {
    private       double[] array;
    private final int    moduloBitmask;
    private final int    bufferSize;

    private long rhsExc;


    public ArrayRingBufferDouble( int suggestedSize ) {
        this( suggestedSize, 0 );
    }

    public ArrayRingBufferDouble( int size, long initialLHS ) {
        this( new double[MathUtils.roundUpToClosestPowerOf2(size)], size, initialLHS );
    }

    private ArrayRingBufferDouble( double[] array, int bufferSize, long initialLHS ) {
        this.array         = array;
        this.moduloBitmask = array.length - 1;
        this.bufferSize    = bufferSize;         // Array may be of a different size so that we can benefit from the speed of using a moduloBitmask
        this.rhsExc        = initialLHS;
    }


    public double get( long i ) {
        if ( isEmpty() ) {
            throw new IndexOutOfBoundsException("index " + i + " is out of bounds, as the buffer is currently empty");
        }

        if ( i < lhsIndex() || i >= rhsExc ) {
            throw new IndexOutOfBoundsException("index "+i+" is out of bounds, currently supported indexes are "+lhsIndex()+" to "+ rhsIndexInc()+" (inc)");
        }

        int index = wrapIndex(i);

        return array[index];
    }

    public long append( double v ) {
        array[wrapIndex(rhsExc)] = v;

        long index = rhsExc;

        rhsExc = index + 1;

        return index;
    }

    public void set( long i, double v ) {
        if ( i < lhsIndex() || i >= rhsExc ) {
            throw new IndexOutOfBoundsException("index "+i+" is out of bounds, currently supported indexes are "+lhsIndex()+" to "+ rhsIndexInc()+" (inc)");
        }

        array[wrapIndex(i)] = v;
    }

    public long rhsIndexExc() {
        return rhsExc;
    }

    public long size() {
        return bufferSize;
    }

    public DoubleIterator iterator() {
        return new RingBufferIteratorDouble(this);
    }

    public boolean equals( Object other ) {
        if ( !(other instanceof SeqDouble) ) {
            return false;
        }

        SeqDouble otherSeq = Backdoor.cast(other);
        return this.contentsCount() == otherSeq.contentsCount() && this.mkString().equals(otherSeq.mkString());
    }

    public int hashCode() {
        return this.mkString().hashCode();
    }

    private int wrapIndex( long i ) {
        // & is apx three times faster than using %; it just limits us to buffer sizes of n^2 (and the mask must be n^2-1)
        return Backdoor.toInt((i + array.length) & moduloBitmask);   // NB always doing + then % is faster than using an if block
    }
}
