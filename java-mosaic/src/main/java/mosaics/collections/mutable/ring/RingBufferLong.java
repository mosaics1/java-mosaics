package mosaics.collections.mutable.ring;

import mosaics.collections.SeqDouble;
import mosaics.collections.SeqLong;
import mosaics.fp.collections.DoubleIterator;
import mosaics.fp.collections.LongIterable;
import mosaics.fp.collections.LongIterator;
import mosaics.lang.Assert;
import mosaics.lang.Backdoor;
import mosaics.lang.QA;
import lombok.AllArgsConstructor;

import java.util.function.LongBinaryOperator;
import java.util.function.LongUnaryOperator;


public interface RingBufferLong extends SeqLong {

    public default SeqLong copy() {
        SeqLong clone = new ArrayRingBufferLong( Backdoor.toInt(this.size()), this.lhsIndex() );

        this.iterator().forEach( clone::append );

        return clone;
    }


    public default SeqLong map( LongUnaryOperator op ) {
        return new MappedRingBufferLong(this, op);
    }

    public default SeqLong mapWithIndex( LongBinaryOperator op ) {
        return new MappedWithIndexRingBufferLong(this, op);
    }

    public default SeqDouble toDouble() {
        return new ToDoubleRingBufferLong(this);
    }

    public default SeqLong add( SeqLong rhs ) {
        return new AggregatingRingBufferLong( this, rhs, (a,b) -> a + b );
    }

    public default SeqLong sub( SeqLong rhs ) {
        return new AggregatingRingBufferLong( this, rhs, (a,b) -> a - b );
    }

    public default SeqLong shiftLeft( int n ) {
        return new ShiftedRingBufferLong( this, -n );
    }

    public default SeqLong shiftRight( int n ) {
        return new ShiftedRingBufferLong( this, n );
    }

    public default SeqLong sumPrevious( int n ) {
        return new SumPreviousRingBufferLong( this, n );
    }
}


class SumPreviousRingBufferLong extends ReadOnlyRingBufferLong {
    private SeqLong orig;
    private int       n;

    public SumPreviousRingBufferLong( SeqLong orig, int n ) {
        QA.argIsGTZero( n, "n" );

        this.orig = orig;
        this.n    = n;
    }

    public long get( long i ) {
        long acc      = orig.get(i);
        long   minIndex = Math.max(orig.lhsIndex(), i - n + 1);

        for ( long pos=i-1; pos>=minIndex; pos-- ) {
            acc += orig.get(pos);
        }

        return acc;
    }

    public long rhsIndexExc() {
        return orig.rhsIndexExc();
    }

    public long size() {
        return orig.size();
    }

    public LongIterator iterator() {
        return new RingBufferIteratorLong(this);
    }
}


class AggregatingRingBufferLong extends ReadOnlyRingBufferLong {
    private final SeqLong     lhs;
    private final SeqLong     rhs;
    private final LongBinaryOperator aggregator;

    public AggregatingRingBufferLong( SeqLong lhs, SeqLong rhs, LongBinaryOperator aggregator ) {
        this.lhs        = lhs;
        this.rhs        = rhs;
        this.aggregator = aggregator;
    }

    public long get( long i ) {
        long a = lhs.get(i);
        long b = rhs.get(i);

        return aggregator.applyAsLong(a, b);
    }


    public long lhsIndex() {
        assertSizesMatch();

        return lhs.lhsIndex();
    }

    public long rhsIndexInc() {
        assertSizesMatch();

        return lhs.rhsIndexInc();
    }

    public long rhsIndexExc() {
        assertSizesMatch();

        return lhs.rhsIndexExc();
    }

    public long size() {
        assertSizesMatch();

        return lhs.size();
    }

    public LongIterator iterator() {
        assertSizesMatch();

        return LongIterable.combine(lhs.iterator(), rhs.iterator(), aggregator);
    }

    private void assertSizesMatch() {
        Assert.isEqualTo( lhs.size(), rhs.size(), "lhs.size", "rhs.size" );
    }
}

class MappedRingBufferLong extends ReadOnlyRingBufferLong {
    private final SeqLong           wrappedInstance;
    private final LongUnaryOperator op;

    MappedRingBufferLong( SeqLong wrappedInstance, LongUnaryOperator op ) {
        this.wrappedInstance = wrappedInstance;
        this.op = op;
    }

    public long get( long i ) {
        return op.applyAsLong(wrappedInstance.get(i));
    }

    public long lhsIndex() {
        return wrappedInstance.lhsIndex();
    }

    public long rhsIndexInc() {
        return wrappedInstance.rhsIndexInc();
    }

    public long rhsIndexExc() {
        return wrappedInstance.rhsIndexExc();
    }

    public long size() {
        return wrappedInstance.size();
    }

    public LongIterator iterator() {
        return wrappedInstance.iterator().map( op );
    }
}

class MappedWithIndexRingBufferLong extends ReadOnlyRingBufferLong {
    private final SeqLong            wrappedInstance;
    private final LongBinaryOperator op;

    MappedWithIndexRingBufferLong( SeqLong wrappedInstance, LongBinaryOperator op ) {
        this.wrappedInstance = wrappedInstance;
        this.op              = op;
    }

    public long get( long i ) {
        return op.applyAsLong(i, wrappedInstance.get(i));
    }

    public long lhsIndex() {
        return wrappedInstance.lhsIndex();
    }

    public long rhsIndexInc() {
        return wrappedInstance.rhsIndexInc();
    }

    public long rhsIndexExc() {
        return wrappedInstance.rhsIndexExc();
    }

    public long size() {
        return wrappedInstance.size();
    }

    public LongIterator iterator() {
        return new RingBufferIteratorLong( this );
    }
}

@AllArgsConstructor
class ToDoubleRingBufferLong extends ReadOnlyRingBufferDouble {
    private final SeqLong wrappedInstance;


    public double get( long i ) {
        return wrappedInstance.get(i);
    }

    public long lhsIndex() {
        return wrappedInstance.lhsIndex();
    }

    public long rhsIndexInc() {
        return wrappedInstance.rhsIndexInc();
    }

    public long rhsIndexExc() {
        return wrappedInstance.rhsIndexExc();
    }

    public long size() {
        return wrappedInstance.size();
    }

    public DoubleIterator iterator() {
        return wrappedInstance.iterator().toDouble();
    }
}


class ShiftedRingBufferLong extends ReadOnlyRingBufferLong {
    private SeqLong orig;
    private int     shiftedBy;

    public ShiftedRingBufferLong( SeqLong orig, int shiftedBy ) {
        this.orig      = orig;
        this.shiftedBy = shiftedBy;
    }

    public long get( long i ) {
        long shiftedIndex = i - shiftedBy;

        if ( shiftedIndex < orig.lhsIndex() ) {
            return 0;
        }

        return orig.get(shiftedIndex);
    }

    public long rhsIndexExc() {
        if ( orig.isEmpty() ) {
            return 0;
        }

        return Math.max(0, orig.rhsIndexExc()+shiftedBy);
    }

    public long size() {
        if ( shiftedBy > 0 ) {
            return orig.size();
        } else {
            return orig.size() + shiftedBy;
        }
    }

    public LongIterator iterator() {
        return new RingBufferIteratorLong(this);
    }
}


abstract class ReadOnlyRingBufferLong implements RingBufferLong {
    public long append( long v ) {
        throw new UnsupportedOperationException("This ring buffer has been set to read only");
    }

    public void set( long i, long v ) {
        throw new UnsupportedOperationException("This ring buffer has been set to read only");
    }

    public boolean equals( Object other ) {
        if ( !(other instanceof SeqLong) ) {
            return false;
        }

        SeqLong otherSeq = Backdoor.cast(other);
        return this.contentsCount() == otherSeq.contentsCount() && this.mkString().equals(otherSeq.mkString());
    }

    public int hashCode() {
        return this.mkString().hashCode();
    }
}

class RingBufferIteratorLong implements LongIterator {
    private final long    toIndexExc;
    private final SeqLong buf;

    private long    i;
    private boolean hasNext;


    RingBufferIteratorLong( SeqLong buf ) {
        this(buf, buf.lhsIndex(), buf.rhsIndexExc());
    }

    RingBufferIteratorLong( SeqLong buf, long fromIndex, long toIndexExc ) {
        QA.argIsGTE( fromIndex, buf.lhsIndex(), "fromIndex", "buf.lhsIndex" );
        QA.argIsLTE( toIndexExc, buf.rhsIndexExc(), "toIndexExc", "buf.rhsIndexExc");

        this.buf        = buf;
        this.toIndexExc = toIndexExc;

        this.i       = fromIndex;
        this.hasNext = buf.isValidIndex( fromIndex );
    }

    public boolean hasNext() {
        return hasNext;
    }

    public long next() {
        long next = i;

        i      += 1;
        hasNext = i < toIndexExc;

        return buf.get(next);
    }
}
