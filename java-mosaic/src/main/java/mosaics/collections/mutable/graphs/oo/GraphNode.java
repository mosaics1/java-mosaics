package mosaics.collections.mutable.graphs.oo;

import mosaics.fp.collections.FPIterable;
import mosaics.fp.collections.FPOption;


/**
 *
 * @param <P> path  (the type of the key used to transition edges)
 * @param <V> value (the type of data stored within the node)
 */
@SuppressWarnings("unchecked")
public interface GraphNode<P,V> {

    public static <P,V> GraphNode<P,V> createRootNode() {
        return new OOGraphNode<>();
    }


    public FPOption<V> getNodeContents();
    public void setNodeContents( V contents );
    public void clearNodeContents();

    public FPOption<GraphNode<P,V>> get( P...path );
    public GraphNode<P,V> getOrCreate( P...path );

    public FPIterable<GraphEdge<P,V>> getOutEdges();
    public FPOption<GraphEdge<P,V>> getEdge( P targetKey );
    public GraphEdge<P,V> getOrCreateEdge( P targetKey );

}
