package mosaics.collections.mutable.graphs.oo;

import lombok.Value;


@Value
public class OOGraphEdge<P,V> implements GraphEdge<P,V> {

    private P key;
    private GraphNode<P,V> node;

}
