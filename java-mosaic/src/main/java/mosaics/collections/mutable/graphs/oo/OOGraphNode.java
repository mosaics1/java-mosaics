package mosaics.collections.mutable.graphs.oo;

import lombok.ToString;
import mosaics.fp.FP;
import mosaics.fp.collections.FPIterable;
import mosaics.fp.collections.FPOption;
import mosaics.utils.ListUtils;

import java.util.LinkedList;
import java.util.List;


/**
 * Represents a graph using objects.  One object per node.  One object per link.
 */
@ToString(exclude = "outEdges")
public class OOGraphNode<P,V> implements GraphNode<P,V> {
    private final List<GraphEdge<P,V>> outEdges     = new LinkedList<>();
    private       FPOption<V>          nodeContents = FP.emptyOption();


    public FPOption<V> getNodeContents() {
        return nodeContents;
    }

    public void setNodeContents( V contents ) {
        nodeContents = FP.option( contents );
    }

    public void clearNodeContents() {
        this.nodeContents = FP.emptyOption();
    }

    @SafeVarargs
    public final FPOption<GraphNode<P, V>> get( P... path ) {
        FPOption<GraphNode<P,V>> next = FP.option( this );

        for ( P nextEdgeKey : path ) {
            next = next.flatMap( n -> n.getEdge(nextEdgeKey).map(GraphEdge::getNode) );
        }

        return next;
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    @SafeVarargs
    public final GraphNode<P, V> getOrCreate( P... path ) {
        GraphNode<P,V> currentNode = this;

        for ( P nextEdgeKey : path ) {
            FPOption<GraphEdge<P,V>> nextEdge = currentNode.getEdge( nextEdgeKey );

            if ( nextEdge.isEmpty() ) {
                GraphEdge<P,V> newEdge = new OOGraphEdge<>(nextEdgeKey, new OOGraphNode<>() );

                ((OOGraphNode) currentNode).outEdges.add( newEdge );

                currentNode = newEdge.getNode();
            } else {
                currentNode = nextEdge.get().getNode();
            }
        }

        return currentNode;
    }

    public FPIterable<GraphEdge<P,V>> getOutEdges() {
        return FP.wrap( outEdges );
    }

    public FPOption<GraphEdge<P,V>> getEdge( P targetKey ) {
        return ListUtils.firstMatch( outEdges, edge -> edge.getKey().equals(targetKey) );
    }

    public GraphEdge<P,V> getOrCreateEdge( P targetKey ) {
        FPOption<GraphEdge<P,V>> edge = getEdge( targetKey );

        if ( edge.hasValue() ) {
            return edge.get();
        }

        GraphEdge<P,V> newEdge = new OOGraphEdge<>(targetKey, new OOGraphNode<>() );
        outEdges.add( newEdge );

        return newEdge;
    }

}
