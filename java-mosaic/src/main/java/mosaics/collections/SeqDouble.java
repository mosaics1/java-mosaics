package mosaics.collections;

import mosaics.fp.collections.DoubleIterable;
import mosaics.fp.collections.DoubleIterator;
import mosaics.lang.Assert;
import mosaics.lang.MathUtils;
import mosaics.lang.functions.DoubleFunctionLD;

import java.util.function.DoubleUnaryOperator;


public interface SeqDouble extends DoubleIterable {
    public double get( long i );

    public default boolean isValidIndex( long i ) {
        return MathUtils.isInRangeExc(lhsIndex(), i, rhsIndexExc());
    }

    /**
     * Store the specified value into the buffer in the next available slot.
     *
     * @return index where the value was stored
     */
    public long append( double v );

    /**
     * Short hand for updating the most recently pushed value.
     */
    public default void updateRHS( double v ) {
        set( rhsIndexInc(), v );
    }


    /**
     * Sets the value stored at an existing position within the ring.
     */
    public void set( long i, double v );

    public default void appendAll( double...values ) {
        for ( double v : values ) {
            append( v );
        }
    }

    public default double getRHS() {
        return getRHSByOffset(0);
    }

    /**
     * Returns the value stored in the furthest right hand position.  Specifying an offset of zero
     * will return the right most position.  An offset of +1 will return the value one position to
     * the left of that.  Negative offsets are not allowed.
     */
    public default double getRHSByOffset( long offset ) {
        Assert.argIsGTEZero( offset, "offset" );

        return get(rhsIndexInc() - offset);
    }

    public default boolean isEmpty() {
        return rhsIndexExc() == lhsIndex();
    }

    /**
     * Returns true if values have been dropped from the lhs.
     */
    public default boolean hasOverflowed() {
        return lhsIndex() > 0;
    }

    public default boolean isFull() {
        return rhsIndexExc() - lhsIndex() >= size();
    }

    public default long lhsIndex() {
        long lhs = rhsIndexExc() - size();

        return Math.max(0, lhs);
    }

    public default long rhsIndexInc() {
        return Math.max(0,rhsIndexExc()-1);
    }

    public long rhsIndexExc();

    /**
     * Returns how many values this buffer could contain in total.  eg similar to array.length.
     */
    public long size();

    /**
     * How many values can be pushed until the buffer will wrap around and start overwriting its
     * oldest (left most) values?
     */
    public default long remaining() {
        return isFull() ? 0 : size() - rhsIndexExc();
    }

    /**
     * How many values are currently stored within the ring buffer?  Does not include values that
     * have been overwritten when the buffer over flowed.
     */
    public default long contentsCount() {
        return Math.min(rhsIndexExc(), size());
    }

    /**
     * Iterate from RHS to LHS.
     */
    public default DoubleIterator reverseIterator() {
        return new DoubleIterator() {
            private long lhs          = SeqDouble.this.lhsIndex();
            private long nextIndexExc = SeqDouble.this.rhsIndexExc();

            public boolean hasNext() {
                return nextIndexExc > lhs;
            }

            public double next() {
                nextIndexExc -= 1;

                return SeqDouble.this.get( nextIndexExc );
            }
        };
    }

    /**
     * Copies the contents of this ring buffer to a new instance.  If this ring buffer was mirroring
     * another ring buffer then the cloned copy will be a copy of the original buffer after all
     * transformations.  When new values are appended to the original buffer, then this buffer will
     * not see the changes.  It will however now be possible to append new values to this buffer
     * separate of the original.
     */
    public SeqDouble copy();

    public default String mkString() {
        return iterator().mkString();
    }

    /**
     * Creates a new instance of SeqDouble that mirrors this instance.  The difference is
     * that each value of the original instance is transformed by op.  It is illegal to append new
     * values to mirroring ring buffers.
     *
     * @param op takes a value from the original buffer and returns a new value that is to be returned
     *           at the same index from the new buffer
     * @return the new seq which is a view over the original seq
     */
    public SeqDouble map( DoubleUnaryOperator op );

    /**
     * Creates a new SeqDouble where each value of the original seq has been modified by the specified
     * operation.
     *
     * @param op given the index and value of an element within the original seq and returns a new value
     *           to be used in the new seq
     * @return the new seq which is a view over the original seq
     */
    public SeqDouble mapWithIndex( DoubleFunctionLD op );

    /**
     * Creates a new SeqDouble that combines the specified two sequences.  Position 1 of the lhs is
     * added to position 1 of the rhs, position 2 to position 2 and so on.
     *
     * @return the new seq which is a view over the original seq
     */
    public SeqDouble add( SeqDouble rhs );

    /**
     * Creates a new SeqDouble that combines the specified two sequences.  Position 1 of the lhs is
     * subtracted from position 1 of the rhs, position 2 to position 2 and so on.
     *
     * @return the new seq which is a view over the original seq
     */
    public SeqDouble sub( SeqDouble rhs );

    /**
     * Creates a new SeqDouble that has the contents of this sequence shifted left n places.
     *
     * @return the new seq which is a view over the original seq
     */
    public SeqDouble shiftLeft( int n );

    /**
     * Creates a new SeqDouble that has the contents of this sequence shifted right n places.
     *
     * @return the new seq which is a view over the original seq
     */
    public SeqDouble shiftRight( int n );

    /**
     * Creates a new SeqDouble that combines the specified two sequences.  Every value within this
     * sequence is multiplied by n.
     *
     * @return the new seq which is a view over the original seq
     */
    public default SeqDouble mult( int n ) {
        return map(v -> v * n);
    }

    /**
     * Creates a new SeqDouble that combines the specified two sequences.  Every value within this
     * sequence is multiplied by n.
     *
     * @return the new seq which is a view over the original seq
     */
    public default SeqDouble mult( double n ) {
        return map(v -> v * n);
    }

    /**
     * Creates a new SeqDouble that combines the specified two sequences.  Every value within this
     * sequence is divided by n.
     *
     * @return the new seq which is a view over the original seq
     */
    public default SeqDouble div( int n ) {
        return map(v -> v / n);
    }

    /**
     * Creates a new SeqDouble that combines the specified two sequences.  Every value within this
     * sequence is divided by n.
     *
     * @return the new seq which is a view over the original seq
     */
    public default SeqDouble div( double n ) {
        return map(v -> v / n);
    }

    /**
     * Creates a new SeqDouble that adds the previous n values together.  For example, given n of 2
     * then index 0 will be valueAt(0) + valueAt(1), index 1 will be valueAt(1)+valueAt(2), and
     * so on.
     *
     * <pre>
     * index:                  0   1      2        3         4
     * originalSeqValue:       1   2      3        4         5
     * sumPreviousSeqValue:    1  1+2   1+2+3    2+3+4     3+4+5
     * </pre>
     *
     * @return the new seq which is a view over the original seq
     */
    public SeqDouble sumPrevious( int n );

    /**
     * Creates a new SeqDouble that is the moving average of the previous n values from this sequence.
     * If this sequence contains less than n values then it will remain empty.
     */
//    public default SeqDouble movingAverages( int n ) {
//         return sumPrevious(n).div(n);
//    }
}
