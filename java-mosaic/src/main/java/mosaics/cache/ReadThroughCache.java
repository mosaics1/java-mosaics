package mosaics.cache;

import lombok.AllArgsConstructor;
import mosaics.fp.FP;
import mosaics.fp.Try;
import mosaics.fp.Tryable;
import mosaics.fp.collections.FPOption;
import mosaics.lang.annotations.ThreadSafe;
import mosaics.lang.functions.Function1;
import mosaics.lang.functions.Function1WithThrows;


@ThreadSafe
@AllArgsConstructor
public class ReadThroughCache<K,V> implements Cache<K,V> {
    private final Cache<K, Tryable<FPOption<V>>>     wrappedCache;
    private final Function1<K, Tryable<FPOption<V>>> fetcher;


    public ReadThroughCache( Function1WithThrows<K,FPOption<V>> fetcher ) {
        this( new PermCache<>(), fetcher.liftToTryable() );
    }


    public FPOption<V> get( K key ) {
        FPOption<Tryable<FPOption<V>>> currentValue = wrappedCache.get(key);

        if ( currentValue.isEmpty() ) {
            Tryable<FPOption<V>> newValue = fetcher.invoke( key );

            wrappedCache.put( key, newValue );

            currentValue = FP.option(newValue);
        }

        return currentValue.flatMap( Tryable::getResult );
    }

    public void remove( K key ) {
        wrappedCache.remove( key );
    }

    public void put( K key, V value ) {
        wrappedCache.put( key, Try.succeeded(FP.option(value)) );
    }

    public void clear() {
        wrappedCache.clear();
    }

    public int size() {
        return wrappedCache.size();
    }
}
