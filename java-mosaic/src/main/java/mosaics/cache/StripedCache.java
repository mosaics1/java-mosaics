package mosaics.cache;

import mosaics.fp.FP;
import mosaics.fp.collections.FPOption;
import mosaics.lang.annotations.ThreadSafe;
import mosaics.lang.functions.Function0;


@ThreadSafe
public class StripedCache<K,V> implements Cache<K,V> {
    private Cache<K,V>[] wrappedCaches;

    @SuppressWarnings("unchecked")
    public StripedCache( Function0<Cache<K,V>> cacheFactory, int numStripes ) {
        wrappedCaches = FP.repeat(numStripes, cacheFactory).toArray(Cache.class);
    }


    public FPOption<V> get( K key ) {
        Cache<K,V> selectedCache = selectCacheFor(key);

        return selectedCache.get(key);
    }

    public void remove( K key ) {
        Cache<K,V> selectedCache = selectCacheFor(key);

        selectedCache.remove( key );
    }

    public void put( K key, V value ) {
        Cache<K,V> selectedCache = selectCacheFor(key);

        selectedCache.put( key, value );
    }

    public void clear() {
        FP.wrapArray(wrappedCaches).forEach( Cache::clear );
    }

    public int size() {
        return FP.wrapArray(wrappedCaches).mapToInt( Cache::size ).sum();
    }

    private Cache<K, V> selectCacheFor( K key ) {
        int selectedBucketIndex = key.hashCode() % wrappedCaches.length;

        return wrappedCaches[selectedBucketIndex];
    }
}
