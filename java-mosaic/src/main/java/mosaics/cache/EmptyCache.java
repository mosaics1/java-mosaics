package mosaics.cache;

import mosaics.fp.FP;
import mosaics.fp.collections.FPOption;
import mosaics.lang.annotations.ThreadSafe;


@ThreadSafe
public class EmptyCache<K,V> implements Cache<K,V> {
    public FPOption<V> get( K key ) {
        return FP.emptyOption();
    }

    public void remove( K key ) {}

    public void put( K key, V value ) {}

    public void clear() {}

    public int size() {
        return 0;
    }
}
