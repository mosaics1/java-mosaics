package mosaics.cache;

import mosaics.fp.Failure;


public interface CacheListener<K> {
    public static final CacheListener NO_OP = new CacheListener() {
        public void cacheHit( Object key, double durationMillis ) {}
        public void cacheMiss( Object key, double durationMillis ) {}

        public void valueRemoved( Object key, double durationMillis ) {}
        public void valueFetched( Object key, double durationMillis ) {}
        public void valuePut( Object key, double durationMillis ) {}
        public void cacheCleared( double durationMillis ) {}
        public void cacheSize( int numEntries, double durationMillis ) {}

        public void errorWhileFetchingValue( Object key, Failure failure, double durationMillis ) {}
        public void errorWhileReleasingValue( Object key, Failure failure, double durationMillis ) {}
    };

    public void cacheHit( K key, double durationMillis );
    public void cacheMiss( K key, double durationMillis );

    public void valueRemoved( K key, double durationMillis );
    public void valueFetched( K key, double durationMillis );
    public void valuePut( K key, double durationMillis );
    public void cacheCleared( double durationMillis );
    public void cacheSize( int numEntries, double durationMillis );

    public void errorWhileFetchingValue( K key, Failure failure, double durationMillis );
    public void errorWhileReleasingValue( K key, Failure failure, double durationMillis );
}
