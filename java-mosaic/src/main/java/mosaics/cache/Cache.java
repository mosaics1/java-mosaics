package mosaics.cache;

import mosaics.fp.collections.FPOption;
import mosaics.lang.functions.Function1;


public interface Cache<K,V> {
    public FPOption<V> get( K key );

    public void remove( K key );

    public void put( K key, V value );

    public void clear();

    public int size();

    public default V getMandatory( K key ) {
        return get(key).get();
    }

    public default V get( K key, Function1<K,V> factory ) {
        FPOption<V> result = get(key);

        if ( result.hasValue() ) {
            return result.get();
        }

        V newValue = factory.invoke( key );
        put( key, newValue );

        return newValue;
    }

    public default boolean contains( K key ) {
        return get(key).hasValue();
    }
}
