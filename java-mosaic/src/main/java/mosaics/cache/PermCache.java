package mosaics.cache;

import mosaics.fp.FP;
import mosaics.fp.collections.FPOption;
import mosaics.lang.annotations.ThreadSafe;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


@ThreadSafe
public class PermCache<K,V> implements Cache<K,V> {
    private final Map<K,V> cache = new ConcurrentHashMap<>();

    public FPOption<V> get( K key ) {
        return FP.option(cache.get(key));
    }

    public void remove( K key ) {
        cache.remove( key );
    }

    public void put( K key, V value ) {
        cache.put( key, value );
    }

    public void clear() {
        cache.clear();
    }

    public int size() {
        return cache.size();
    }
}
