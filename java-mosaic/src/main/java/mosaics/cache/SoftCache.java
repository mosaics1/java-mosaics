package mosaics.cache;

import mosaics.fp.FP;
import mosaics.fp.collections.FPOption;
import mosaics.lang.QA;
import mosaics.lang.annotations.ThreadSafe;
import mosaics.lang.functions.Function3;
import mosaics.lang.ref.ReferenceDeallocator;
import mosaics.lang.ref.SoftReference;

import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;


@ThreadSafe
public class SoftCache<K,V> implements Cache<K,V> {
    private Cache<K, Reference<V>>                               wrappedCache;
    private final ReferenceDeallocator<K, V, SoftReference<K,V>> deallocator = new ReferenceDeallocator<>(
        // NB this cast is only required by IntelliJ!!! -- experiment with removing ever now and again
        (Function3<K, V, ReferenceQueue<V>, SoftReference<K, V>>) SoftReference::new,  // ref constructor
        this::clearRef       // ref teardown -- called when the soft ref goes out of scope
    );


    public SoftCache() {
        this( new PermCache<>() );
    }

    public SoftCache( Cache<K, Reference<V>> wrappedCache ) {
        QA.notNull( wrappedCache, "wrappedCache" );

        this.wrappedCache = wrappedCache;
    }


    public FPOption<V> get( K key ) {
        return wrappedCache.get(key).flatMap( ref -> FP.option(ref.get()) );
    }

    public void remove( K key ) {
        deallocator.poll();

        wrappedCache.remove( key );
    }

    public void put( K key, V value ) {
        deallocator.poll();

        Reference<V> ref = deallocator.register( key, value );

        wrappedCache.put(key, ref);
    }

    public void clear() {
        wrappedCache.clear();
    }

    public int size() {
        return wrappedCache.size();
    }

    private void clearRef( SoftReference<K,V> ref ) {
        if ( doesGCedRefMatchRefInWrappedCache(ref) ) {
            // NB: This has a very small window where an updated value could be removed.
            // We expect that this will rarely happen, and when it does the occasional cost of
            // re-fetching data into the cache will be less than the overhead of always locking here
            wrappedCache.remove( ref.getMetaData() );
        }
    }

    /**
     * Returns true when the wrappedCache still contains the specified expectedRef.
     */
    private boolean doesGCedRefMatchRefInWrappedCache( SoftReference<K, V> expectedRef ) {
        K                      key           = expectedRef.getMetaData();
        FPOption<Reference<V>> currentRefOpt = wrappedCache.get( key );

        if ( currentRefOpt.isEmpty() ) {
            return false;
        } else {
            return currentRefOpt.get() == expectedRef;
        }
    }
}
