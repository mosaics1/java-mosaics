package mosaics.xml.ast;

import mosaics.io.xml.TagWriter;
import mosaics.lang.functions.VoidFunction2;
import lombok.Value;


/**
 * Use in cases when a tag is to be included that has no attributes or body.
 */
@Value
public class XmlEmptyTagAST<T> implements XmlBodyAST<T> {

    private String tagName;


    public boolean hasContents( T pojo ) {
        return true;
    }

    public void writeTo( T pojo, VoidFunction2<Object,TagWriter> nestedSerializer, TagWriter out ) {
        out.tag( tagName );
        out.closeTag( tagName );
    }

}
