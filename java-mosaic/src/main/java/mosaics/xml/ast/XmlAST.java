package mosaics.xml.ast;

import mosaics.io.xml.TagWriter;
import mosaics.lang.functions.VoidFunction2;


/**
 * An AST for writing a POJO to a TagWriter.
 */
public interface XmlAST<T> {

    /**
     * Returns true if this AST will write data for the supplied POJO.  When a POJO is empty then
     * there is no need to output any Xml, inwhich case this method will return false.
     */
    public boolean hasContents( T pojo );

    /**
     * Convert the specified pojo into calls to the specified TagWriter.
     */
    public void writeTo( T pojo, VoidFunction2<Object,TagWriter> nestedSerializer, TagWriter out );

}
