package mosaics.xml.ast;

import mosaics.lang.functions.Function1;
import mosaics.lang.reflection.JavaClass;


public class XmlASTBuilder {

    public XmlASTBuilder startTag( String tagName ) {

        return this;
    }

    public <R,A> XmlASTBuilder tagAttribute( String attributeName, Function1<R, A> valueFetcher, JavaClass type ) {
        return this;
    }

    public <R,B> XmlASTBuilder tagBody( Function1<R,B> bodyFetcher, JavaClass type ) {
        return this;
    }

    public XmlASTBuilder closeTag( String tagName ) {
        return this;
    }

    public XmlAST build() {
        return null;
    }

}
