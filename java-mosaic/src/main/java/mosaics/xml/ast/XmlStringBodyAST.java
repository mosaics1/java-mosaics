package mosaics.xml.ast;

import mosaics.fp.collections.FPOption;
import mosaics.io.xml.TagWriter;
import mosaics.lang.functions.Function1;
import mosaics.lang.functions.VoidFunction2;
import lombok.NonNull;
import lombok.Value;


@Value
public class XmlStringBodyAST<T> implements XmlBodyAST<T> {

    @NonNull private Function1<T,String> bodyFetcher;


    public boolean hasContents( T pojo ) {
        return toBody(pojo).hasValue();
    }

    public void writeTo( T pojo, VoidFunction2<Object,TagWriter> nestedSerializer, TagWriter out ) {
        toBody(pojo).ifPresent( out::bodyText );
    }


    private FPOption<String> toBody( T pojo ) {
        String body = bodyFetcher.invoke( pojo );

        return FPOption.ofBlankableString( body );
    }

}
