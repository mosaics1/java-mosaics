package mosaics.xml.ast;

import mosaics.fp.collections.RRBVector;
import mosaics.io.xml.TagWriter;
import mosaics.lang.Backdoor;
import mosaics.lang.functions.VoidFunction2;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.Value;


@Value
@AllArgsConstructor
public class XmlTagAST<T> implements XmlBodyAST<T> {

    @NonNull private String                   tagName;
    @NonNull private RRBVector<XmlAttrAST<T>> attributeASTs;
    @NonNull private RRBVector<XmlBodyAST<T>> bodyASTs;


    public XmlTagAST( String tagName ) {
        this.tagName       = tagName;
        this.attributeASTs = RRBVector.emptyVector();
        this.bodyASTs      = RRBVector.emptyVector();
    }

    public XmlTagAST<T> appendBodyAST( XmlBodyAST<T> newBodyToAppend ) {
        return withBodyASTs( bodyASTs.add(newBodyToAppend) );
    }

    @SafeVarargs
    public final XmlTagAST<T> withBodyASTs( XmlBodyAST<T>...bodyASTs ) {
        return withBodyASTs( RRBVector.toVector(bodyASTs) );
    }

    public XmlTagAST<T> withBodyASTs( RRBVector<XmlBodyAST<T>> bodyASTs ) {
        return new XmlTagAST<>( tagName, attributeASTs, bodyASTs );
    }

    public XmlTagAST<T> appendAttributeAST( XmlAttrAST<T> attributeToAppend ) {
        return withAttributeASTs(  attributeASTs.add(attributeToAppend) );
    }

    @SafeVarargs
    public final XmlTagAST<T> withAttributeASTs( XmlAttrAST<T>...attributes ) {
        return withAttributeASTs( RRBVector.toVector(attributes) );
    }

    public XmlTagAST<T> withAttributeASTs( RRBVector<XmlAttrAST<T>> attributes ) {
        return new XmlTagAST<>( tagName, attributes, bodyASTs );
    }


    public boolean hasContents( T pojo ) {
        RRBVector<XmlAST<T>> t1 = Backdoor.cast(attributeASTs);
        RRBVector<XmlAST<T>> t2 = Backdoor.cast(bodyASTs);

        return t1.and(t2).first( ast -> ast.hasContents(pojo) ).hasValue();
    }

    public void writeTo( T pojo, VoidFunction2<Object,TagWriter> nestedSerializer, TagWriter out ) {
        if ( hasContents(pojo) ) {
            out.tag( tagName );

            attributeASTs.forEach( ast -> ast.writeTo(pojo,nestedSerializer,out) );
            bodyASTs.forEach( ast -> ast.writeTo(pojo,nestedSerializer,out) );

            out.closeTag( tagName );
        }
    }

}
