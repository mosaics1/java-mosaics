package mosaics.xml.ast;

import mosaics.fp.collections.RRBVector;
import mosaics.io.xml.TagWriter;
import mosaics.lang.functions.Function1;
import mosaics.lang.functions.VoidFunction2;
import lombok.Value;


@Value
public class XmlForAST<T,K> implements XmlBodyAST<T> {

    private final Function1<T,Iterable<K>> iterableFetcher;
    private final RRBVector<XmlAST<K>>     forLoopBody;


    public boolean hasContents( T pojo ) {
        Iterable v = iterableFetcher.invoke( pojo );

        return v.iterator().hasNext();
    }

    public void writeTo( T pojo, VoidFunction2<Object,TagWriter> nestedSerializer, TagWriter out ) {
        Iterable<K> v = iterableFetcher.invoke( pojo );

        v.forEach( e -> forLoopBody.forEach(ast -> ast.writeTo(e,nestedSerializer,out)) );
    }

}
