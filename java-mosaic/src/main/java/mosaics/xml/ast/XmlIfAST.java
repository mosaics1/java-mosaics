package mosaics.xml.ast;

import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import mosaics.fp.collections.RRBVector;
import mosaics.io.xml.TagWriter;
import mosaics.lang.functions.BooleanFunction1;
import mosaics.lang.functions.VoidFunction2;


@Value
@With
@AllArgsConstructor
public class XmlIfAST<T> implements XmlBodyAST<T>, XmlAttrAST<T> {

    @NonNull private BooleanFunction1<T>  predicate;
    @NonNull private RRBVector<XmlAST<T>> ifTrueBlock;
    @NonNull private RRBVector<XmlAST<T>> elseBlock;


    public XmlIfAST( BooleanFunction1<T> predicate ) {
        this.predicate   = predicate;
        this.ifTrueBlock = RRBVector.emptyVector();
        this.elseBlock   = RRBVector.emptyVector();
    }


    public XmlIfAST<T> appendIfTrueBodyAST( XmlBodyAST<T> element ) {
        return withIfTrueBlock( ifTrueBlock.add(element) );
    }

    public XmlIfAST<T> appendElseBodyAST( XmlBodyAST<T> element ) {
        return withElseBlock( elseBlock.add(element) );
    }


    public boolean hasContents( T pojo ) {
        if ( predicate.invoke(pojo) ) {
            return ifTrueBlock.first(ast -> ast.hasContents(pojo)).hasValue();
        } else {
            return elseBlock.first(ast -> ast.hasContents(pojo)).hasValue();
        }
    }

    public void writeTo( T pojo, VoidFunction2<Object,TagWriter> nestedSerializer, TagWriter out ) {
        RRBVector<XmlAST<T>> selectedBlock = predicate.invoke(pojo) ? ifTrueBlock : elseBlock;

        selectedBlock.forEach( ast -> ast.writeTo(pojo,nestedSerializer,out) );
    }

}
