package mosaics.xml;

import mosaics.io.xml.TagWriter;
import mosaics.lang.functions.VoidFunction2;
import mosaics.xml.ast.XmlAST;
import lombok.Value;


@Value
public class XmlTemplate<T> {

    private XmlAST<T> rootAST;


    public void invoke( T pojo, VoidFunction2<Object,TagWriter> nestedSerializer, TagWriter out ) {
        rootAST.writeTo( pojo, nestedSerializer, out );
    }

}
