package mosaics.xml;

import mosaics.fp.FP;
import mosaics.fp.collections.FPOption;
import mosaics.fp.collections.RRBVector;
import mosaics.lang.QA;
import mosaics.lang.functions.BooleanFunction1;
import mosaics.lang.functions.Function1;
import mosaics.lang.functions.Function2;
import mosaics.lang.reflection.JavaClass;
import mosaics.lang.reflection.JavaProperty;
import mosaics.lang.reflection.ReflectionUtils;
import mosaics.strings.CodecNotFoundException;
import mosaics.strings.StringCodec;
import mosaics.strings.StringCodecs;
import mosaics.strings.parser.CharacterIterator;
import mosaics.strings.parser.CharacterMatcher;
import mosaics.strings.parser.CharacterMatchers;
import mosaics.strings.parser.CharacterPosition;
import mosaics.strings.parser.ParseException;
import mosaics.strings.parser.PullParser;
import mosaics.xml.ast.XmlAST;
import mosaics.xml.ast.XmlAttrAST;
import mosaics.xml.ast.XmlAttributeAST;
import mosaics.xml.ast.XmlBodyAST;
import mosaics.xml.ast.XmlDoubleDispatchBodyAST;
import mosaics.xml.ast.XmlEmptyTagAST;
import mosaics.xml.ast.XmlForAST;
import mosaics.xml.ast.XmlIfAST;
import mosaics.xml.ast.XmlStringBodyAST;
import mosaics.xml.ast.XmlTagAST;

import static mosaics.lang.Backdoor.cast;


public class XmlTemplateFactory {

    private static final CharacterMatcher TAGNAME_MATCHER                       = CharacterMatchers.everythingExcept( " ", "/", ">" );
    private static final CharacterMatcher ATTRNAME_MATCHER                      = CharacterMatchers.everythingExcept( " ", "=", "/", ">" );
    private static final CharacterMatcher ATTRIBUTE_NAMEVALUE_SEPARATOR_MATCHER = CharacterMatchers.constant( '=' );
    private static final CharacterMatcher QUOTEDTEXT_MATCHER                    = CharacterMatchers.delimitedText( '\"' );
    private static final CharacterMatcher PROPERTYNAME_MATCHER                  = CharacterMatchers.regexp( "[a-zA-Z0-9][a-zA-Z0-9_]*" );
    private static final CharacterMatcher BRACKETEDPROPERTYPATH_MATCHER         = CharacterMatchers.delimitedText( '{', '}' );
    private static final CharacterMatcher EOF_NOBODY_TAG_MATCHER                = CharacterMatchers.constant( "/>" );
    private static final CharacterMatcher BODYTEXT_MATCHER                      = CharacterMatchers.everythingExcept( '<' );
    private static final CharacterMatcher IFSTART_MATCHER                       = CharacterMatchers.constant( "#if" );
    private static final CharacterMatcher ELSEIF_MATCHER                        = CharacterMatchers.constant( "#elseif" );
    private static final CharacterMatcher ELSE_MATCHER                          = CharacterMatchers.constant( "#else" );
    private static final CharacterMatcher IFEND_MATCHER                         = CharacterMatchers.constant( "#endif" );
    private static final CharacterMatcher FORSTART_MATCHER                      = CharacterMatchers.constant( "#foreach" );
    private static final CharacterMatcher FOREND_MATCHER                        = CharacterMatchers.constant( "#endforeach" );


    private StringCodecs stringCodecs;

    public XmlTemplateFactory() {
        this( StringCodecs.DEFAULT_CODECS );
    }

    public XmlTemplateFactory( StringCodecs stringCodecs ) {
        this.stringCodecs = stringCodecs;
    }


    public <T> XmlTemplate<T> parse( String templateName, String in, Class<T> type ) {
        return parse(templateName, in, JavaClass.of(type));
    }

    public <T> XmlTemplate<T> parse( String templateName, String in, JavaClass type ) {
        QA.notBlank(in, "in");

        return parse(templateName, CharacterIterator.stringIterator(in), type);
    }

    public <T> XmlTemplate<T> parse( String templateName, CharacterIterator in, JavaClass type ) {
        PullParser pp = new PullParser( templateName, in );

        pp.setSkipMatcher( CharacterMatchers.whitespace() );
        pp.skipWhitespace();

        CharacterPosition       pos  = pp.getPosition();
        FPOption<XmlBodyAST<T>> root = parseTag(pp, type);

        return root.map(XmlTemplate::new).orElseThrow( () -> new ParseException(pos,"No tag found") );
    }

    private <T> XmlBodyAST<T> parseTagMandatory( PullParser pp, JavaClass type ) {
        CharacterPosition initialPos = pp.getPosition();

        return cast(parseTag( pp, type ).orElseThrow( () -> new ParseException(initialPos, "Expected tag but found none") ));
    }

    private <T> FPOption<XmlBodyAST<T>> parseTag( PullParser pp, JavaClass type ) {
        if ( !pp.peekAndPull('<') ) {
            return FP.emptyOption();
        }

        String tagName = pp.pullMandatory( TAGNAME_MATCHER );

        RRBVector<XmlAttrAST<T>> attributes = parseAttributes( pp, type );
        RRBVector<XmlBodyAST<T>> body;

        if ( pp.peekAndPull('>') ) {
            body = cast( parseTagBody(pp,type) );

            pp.pullMandatory( "</" );
            pp.pullMandatory( tagName );
            pp.pullMandatory( '>' );
        } else {
            body = FP.emptyVector();

            pp.pullMandatory( EOF_NOBODY_TAG_MATCHER );
        }

        if ( attributes.isEmpty() && body.isEmpty() ) {
            return FP.option(new XmlEmptyTagAST<>(tagName));
        } else {
            return FP.option(
                new XmlTagAST<T>(tagName)
                    .withAttributeASTs(attributes)
                    .withBodyASTs(body)
            );
        }
    }

    @SuppressWarnings("unchecked")
    private <T> RRBVector<XmlAttrAST<T>> parseAttributes( PullParser pp, JavaClass type ) {
        RRBVector<XmlAttrAST<T>> attributes = FP.emptyVector();

        while ( pp.peek(ATTRNAME_MATCHER) || pp.peek(IFSTART_MATCHER) ) {
            if ( pp.peek(IFSTART_MATCHER) ) {
                XmlAttrAST<T> attr = parseIfBlockMandatory( pp, type, (x,y) -> cast(this.parseAttributes(x,y)) );

                attributes = attributes.add( attr );
            } else if ( pp.peek('#') ) {
                return attributes;
            } else {
                String attrName = pp.pullMandatory(ATTRNAME_MATCHER);

                pp.skip(ATTRIBUTE_NAMEVALUE_SEPARATOR_MATCHER);

                pp.skipWhitespace();

                if (pp.peek('$')) {
                    Function1<T, String> attrValueFetcher = parseStringParameterMandatory(pp, type);

                    attributes = attributes.add(new XmlAttributeAST<>(attrName, attrValueFetcher));
                } else {
                    String quotedValue = pp.pullMandatory(QUOTEDTEXT_MATCHER);
                    String fixedValue = quotedValue.substring(1, quotedValue.length() - 1);

                    attributes = attributes.add(new XmlAttributeAST<>(attrName, pojo -> fixedValue));
                }
            }
        }

        return attributes;
    }

    @SuppressWarnings("unchecked")
    private <T> BooleanFunction1<T> parsePredicateMandatory( PullParser pp, JavaClass type ) {
        JavaProperty javaProperty = parsePropertyRefMandatory( pp, type );

        return pojo -> {
            FPOption<Object> value = javaProperty.getValueFrom(pojo);

            return value.map(ReflectionUtils::hasValue).orElse(false);
        };
    }

    @SuppressWarnings("unchecked")
    private <T> Function1<T,String> parseStringParameterMandatory( PullParser pp, JavaClass type ) {
        JavaProperty          javaProperty = parsePropertyRefMandatory( pp, type );
        FPOption<StringCodec> codecOption  = stringCodecs.getCodecFor( javaProperty.getValueType() );
        StringCodec<Object>   codec        = codecOption.orElseThrow( () -> new CodecNotFoundException(javaProperty.getValueType().getFullName()+", referenced from "+type + "#"+javaProperty.toString().replace("this.","")) );

        return pojo -> {
            FPOption<String> value = javaProperty.getValueFrom(pojo).map( codec::encode );

            return value.map(String::trim).orNull();
        };
    }

    private <T> JavaProperty parsePropertyRefMandatory( PullParser pp, JavaClass type ) {
        pp.skipWhitespace();

        CharacterPosition propertyValuePos = pp.getPosition();

        pp.pullMandatory('$');

        String propertyPath;
        if ( pp.peek('{') ) {
            String p = pp.pullMandatory( BRACKETEDPROPERTYPATH_MATCHER );

            propertyPath = p.substring( 1, p.length()-1 );
        } else {
            propertyPath = pp.pullMandatory( PROPERTYNAME_MATCHER );
        }

        if ( propertyPath.equals("this") ) {
            return JavaProperty.wrap( type, "this" );
        }

        return type.walkPropertyPath( propertyPath )
            .orElseThrow( () -> new ParseException(propertyValuePos, "Invalid property path '"+propertyPath+"' for " + type.getFullName()) );
    }

    private <T> RRBVector<XmlAST<T>> parseTagBody( PullParser pp, JavaClass type ) {
        RRBVector<XmlAST<T>> bodyParts = FP.emptyVector();

        pp.skipWhitespace();
        while ( !pp.peek("</") && !pp.peek(IFEND_MATCHER) && !pp.peek( ELSE_MATCHER ) ) {
            pp.skipWhitespace();

            XmlBodyAST<T> bodyPart = parseBodyPartMandatory( pp, type );

            bodyParts = bodyParts.add( bodyPart );
        }

        return bodyParts;
    }

    @SuppressWarnings("unchecked")
    private <T> XmlBodyAST<T> parseBodyPartMandatory( PullParser pp, JavaClass type ) {
        if ( pp.peek('$') ) {
            JavaProperty javaProperty = parsePropertyRefMandatory( pp, type );

            if ( javaProperty.getValueType().isBaseType() ) {
                FPOption<StringCodec> codecOption = stringCodecs.getCodecFor( javaProperty.getValueType() );
                StringCodec<Object>   codec       = codecOption.get();

                Function1<T, String> attrValueFetcher =  pojo -> {
                    FPOption<String> value = javaProperty.getValueFrom(pojo).map(codec::encode);

                    return value.map(String::trim).orNull();
                };

                return new XmlStringBodyAST<>(attrValueFetcher);
            } else {
                return new XmlDoubleDispatchBodyAST(javaProperty);
            }
        } else if ( pp.peek('<') ) {
            return parseTagMandatory(pp, type);
        } else if ( pp.peek(IFSTART_MATCHER) ) {
            return parseIfBlockMandatory(pp, type, this::parseTagBody);
        } else if ( pp.peek(FORSTART_MATCHER) ) {
            return parseForBlockMandatory(pp, type);
        } else {
            String txt = pp.pullMandatoryNoAutoSkip(BODYTEXT_MATCHER).trim();

            return new XmlStringBodyAST<>(x -> txt);
        }
    }

    @SuppressWarnings("unchecked")
    private <T,K> XmlForAST<T,K> parseForBlockMandatory( PullParser pp, JavaClass type ) {
        pp.pullMandatory( FORSTART_MATCHER );

        CharacterPosition propPosition = pp.getPosition();
        JavaProperty<T,Iterable<K>> javaProperty = parsePropertyRefMandatory( pp, type );
        if ( !javaProperty.getValueType().isIterable() ) {
            throw new ParseException( propPosition, javaProperty.getName() + " is of type '" + javaProperty.getValueType().getFullName() + "';  expected something that implements Iterable." );
        }

        JavaClass elementType = javaProperty.getValueType()
            .getClassGenerics().first()
            .orElseThrow( () -> new ParseException(propPosition, javaProperty.getName() + " is of type '" + javaProperty.getValueType().getFullName() + "';  notice that is has no generic type.  Please capture the generic by placing @Capture on " + type.getFullName()) );

        RRBVector<XmlAST<K>> bodyASTs = FP.emptyVector();

        while ( !pp.peek(FOREND_MATCHER) ) {
            XmlBodyAST<K> bodyAST = parseBodyPartMandatory(pp, elementType);

            bodyASTs = bodyASTs.add(bodyAST);
        }

        pp.pullMandatory( FOREND_MATCHER );

        return new XmlForAST<>( s -> javaProperty.getValueFrom(s).orElse(FP.emptyVector()), bodyASTs );
    }

    private <T> XmlIfAST<T> parseIfBlockMandatory( PullParser pp, JavaClass type, Function2<PullParser,JavaClass,RRBVector<XmlAST<T>>> blockParserFunc ) {
        pp.pullMandatory( IFSTART_MATCHER );

        XmlIfAST<T> ast = parseIfBlockFromPredicate( pp, type, blockParserFunc );

        pp.pullMandatory( IFEND_MATCHER );

        return ast;
    }

    private <T> XmlIfAST<T> parseIfBlockFromPredicate( PullParser pp, JavaClass type, Function2<PullParser,JavaClass,RRBVector<XmlAST<T>>> blockParserFunc ) {
        BooleanFunction1<T>  propertyValueFetcher = parsePredicateMandatory( pp, type );
        RRBVector<XmlAST<T>> ifTrueBlock          = blockParserFunc.invoke( pp, type );
        RRBVector<XmlAST<T>> elseBlock            = parseElseBlock( pp, type, blockParserFunc );

        return new XmlIfAST<>( propertyValueFetcher, ifTrueBlock, elseBlock );
    }

    @SuppressWarnings("unchecked")
    private <T> RRBVector<XmlAST<T>> parseElseBlock( PullParser pp, JavaClass type, Function2<PullParser,JavaClass,RRBVector<XmlAST<T>>> blockParserFunc ) {
        if ( pp.peekAndPull(ELSEIF_MATCHER) ) {
            return FP.toVector( (XmlAST<T>) parseIfBlockFromPredicate(pp,type,blockParserFunc) );
        } else if ( pp.peekAndPull(ELSE_MATCHER) ) {
            return blockParserFunc.invoke(pp,type);
        } else {
            return FP.emptyVector();
        }
    }

}
