package mosaics.i18n;

import lombok.Value;
import lombok.With;

import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.ConcurrentHashMap;


@With
@Value
public class I18nContext {
    public static final I18nContext UTC     = i18nContext( Locale.UK,    TimeZone.getTimeZone("UTC"), I18nDateTimeStyle.ISO );
    public static final I18nContext LONDON  = i18nContext( Locale.UK,    TimeZone.getTimeZone("Europe/London") );
    public static final I18nContext ROME    = i18nContext( Locale.ITALY, TimeZone.getTimeZone("Europe/Rome") );
    public static final I18nContext NEWYORK = i18nContext( Locale.US,    TimeZone.getTimeZone("America/New_York") );

    private static final Map<I18nContext,DateTimeFormatter> dtmFormatters  = new ConcurrentHashMap<>();
    private static final Map<I18nContext,DateTimeFormatter> dateFormatters = new ConcurrentHashMap<>();
    private static final Map<I18nContext,DateTimeFormatter> timeFormatters = new ConcurrentHashMap<>();


    public static I18nContext i18nContext( Locale locale, TimeZone tz ) {
        return i18nContext( locale, tz, I18nDateTimeStyle.MEDIUM );
    }

    public static I18nContext i18nContext( Locale locale, TimeZone tz, I18nDateTimeStyle style ) {
        return new I18nContext( locale, tz, style );
    }


    private Locale            locale;
    private TimeZone          timeZone;
    private I18nDateTimeStyle i18nStyle;


    public ZoneId getZoneId() {
        return getTimeZone().toZoneId();
    }

    public DateTimeFormatter getDtmFormatter() {
        return dtmFormatters.computeIfAbsent(
            this,
            ctx -> createDateTimeFormatter(ctx.getI18nStyle().getDtmStyle())
        );
    }

    public DateTimeFormatter getDateFormatter() {
        return dateFormatters.computeIfAbsent(
            this,
            ctx -> createDayFormatter(ctx.getI18nStyle().getDateStyle())
        );
    }

    public DateTimeFormatter getTimeFormatter() {
        return timeFormatters.computeIfAbsent(
            this,
            ctx -> createDateTimeFormatter(ctx.getI18nStyle().getTimeStyle())
        );
    }

    private DateTimeFormatter createDateTimeFormatter( String style ) {
        return switch ( style.toLowerCase() ) {
            case "iso"           -> DateTimeFormatter.ISO_DATE_TIME;
            case "short"         -> createDateTimeFormatterUsingStyle( FormatStyle.SHORT  );
            case "med", "medium" -> createDateTimeFormatterUsingStyle( FormatStyle.MEDIUM );
            case "long"          -> createDateTimeFormatterUsingStyle( FormatStyle.LONG   );
            case "full"          -> createDateTimeFormatterUsingStyle( FormatStyle.FULL   );
            default              -> createDateTimeFormatterUsingPattern( style );
        };
    }

    private DateTimeFormatter createDayFormatter( String style ) {
        return switch ( style.toLowerCase() ) {
            case "iso"           -> DateTimeFormatter.ISO_DATE;
            case "short"         -> createDayFormatterUsingStyle( FormatStyle.SHORT  );
            case "med", "medium" -> createDayFormatterUsingStyle( FormatStyle.MEDIUM );
            case "long"          -> createDayFormatterUsingStyle( FormatStyle.LONG   );
            case "full"          -> createDayFormatterUsingStyle( FormatStyle.FULL   );
            default              -> createDateTimeFormatterUsingPattern( style );
        };
    }

    private DateTimeFormatter createDateTimeFormatterUsingStyle( FormatStyle aShort ) {
        return DateTimeFormatter
            .ofLocalizedDateTime( aShort, aShort )
            .withLocale( getLocale() )
            .withZone( getZoneId() );
    }

    private DateTimeFormatter createDayFormatterUsingStyle( FormatStyle aShort ) {
        return DateTimeFormatter
            .ofLocalizedDate( aShort )
            .withLocale( getLocale() )
            .withZone( getZoneId() );
    }

    private DateTimeFormatter createDateTimeFormatterUsingPattern( String pattern ) {
        return DateTimeFormatter
            .ofPattern( pattern, locale )
            .withZone( getZoneId() );
    }

}
