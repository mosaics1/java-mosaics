package mosaics.i18n;

import lombok.AllArgsConstructor;
import lombok.Value;

import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;


/**
 * Enum that provides a hint to how a user wants date and time formatting to occur.
 *
 * All formatting for dates comes from {@link DateTimeFormatter}.
 *
 * The style may be one of: iso, short, medium, long, full or a date formatter pattern.
 * The definition of short, medium, long and full comes from {@link DateTimeFormatter#ofLocalizedDateTime}
 * and iso comes from {@link DateTimeFormatter#ISO_DATE_TIME}.
 *
 * DateTimeFormatter patterns may also be used.
 *
 * @see DateTimeFormatter#ofLocalizedDateTime(FormatStyle)
 */
@Value
@AllArgsConstructor
public class I18nDateTimeStyle {
    public static final I18nDateTimeStyle ISO    = new I18nDateTimeStyle( "iso" );
    public static final I18nDateTimeStyle SHORT  = new I18nDateTimeStyle( "short" );
    public static final I18nDateTimeStyle MEDIUM = new I18nDateTimeStyle( "medium" );
    public static final I18nDateTimeStyle LONG   = new I18nDateTimeStyle( "long" );
    public static final I18nDateTimeStyle FULL   = new I18nDateTimeStyle( "full" );


    private String dateStyle;    // accepts 'iso', 'short', 'med|medium', 'long', 'full' or pattern
    private String timeStyle;
    private String dtmStyle;

    public I18nDateTimeStyle( String style ) {
        this( style, style, style );
    }
}
