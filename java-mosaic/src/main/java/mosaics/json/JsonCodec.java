package mosaics.json;

import com.google.gson.Gson;
import mosaics.lang.Backdoor;
import mosaics.lang.QA;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Type;


/**
 * Serialize and deserialize json.
 *
 * Has default behaviour for most types.  If custom behaviour is needed, then either register
 * a TypeAdapter directly with this codec or annotate the target class with @GsonCodec.
 */
public class JsonCodec {
    /**
     * Writes newline delimited json.
     */
    public static JsonCodec ndJsonCodec = new JsonCodecBuilder().create();

    /**
     * Writes pretty printed json.
     */
    public static JsonCodec ppJsonCodec = new JsonCodecBuilder().withPrettyPrinting().create();


    private final Gson gson;

    JsonCodec( Gson gson ) {
        QA.notNull(gson, "gson");

        this.gson = gson;
    }


    public String toJson( Object obj ) {
        return gson.toJson( obj );
    }

    public String toJson( Object obj, Class<?> type ) {
        return gson.toJson( obj, type );
    }

    public void toJson( Object obj, Appendable out ) {
        gson.toJson( obj, out );
    }

    public <T> T fromJson( String str, Class<T> type ) {
        return gson.fromJson( str, type );
    }

    public <T> T fromJson( String str, Type type ) {
        return gson.fromJson( str, type );
    }

    public <T> T fromJson( Reader in, Class<T> type ) {
        return gson.fromJson( in, type );
    }

    public <T> T fromJson( Reader in, Type type ) {
        return gson.fromJson( in, type );
    }

    public <T> T fromJson( InputStream in, Class<T> type ) {
        return fromJson( new InputStreamReader(in, Backdoor.UTF8), type );
    }

    public <T> T fromJson( InputStream in, Type type ) {
        return fromJson( new InputStreamReader(in, Backdoor.UTF8), type );
    }
}
