package mosaics.json;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface GsonCodec {
    /**
     *
     * @return class extends com.google.gson.TypeAdapter
     */
    public Class<?> value();
}
