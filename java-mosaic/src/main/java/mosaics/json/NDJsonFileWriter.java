package mosaics.json;

import lombok.SneakyThrows;
import mosaics.io.FileUtils;
import mosaics.lang.Backdoor;
import mosaics.lang.QA;

import java.io.Closeable;
import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.util.List;


/**
 * A file containing new line delimited json.  Use this class to read and write pojos to
 * a file as nd json.
 */
public class NDJsonFileWriter<T> implements Closeable {
    public static <T> void writeAllTo( File f, List<T> entries ) {
            writeAllTo( f, false, entries );
    }

    public static <T> void writeAllTo( File f, boolean append, List<T> entries ) {
        try (NDJsonFileWriter<T> out = new NDJsonFileWriter<>(f, append)) {
            entries.forEach( out::append );
        }
    }

    private final JsonCodec codec;
    private final File      file;
    private final boolean   append;

    private       boolean   isClosed;
    private       Writer    out;

    public NDJsonFileWriter( File f ) {
        this( JsonCodec.ndJsonCodec, f, false );
    }

    public NDJsonFileWriter( File f, boolean append ) {
        this( JsonCodec.ndJsonCodec, f, append );
    }

    public NDJsonFileWriter( JsonCodec codec, File f ) {
        this( codec, f, false );
    }

    @SneakyThrows
    public NDJsonFileWriter( JsonCodec codec, File f, boolean append ) {
        QA.argNotNull( codec, "codec" );
        QA.argNotNull( f, "f" );

        this.codec  = codec;
        this.file   = f;
        this.append = append;
    }

    @SneakyThrows
    public void append( T pojo ) {
        openOnFirstWrite();

        codec.toJson( pojo, out );
        out.append( Backdoor.NEWLINE );
    }

    @SneakyThrows
    public void flush() {
        out.flush();
    }

    @SneakyThrows
    public void close() {
        if ( out != null ) {
            out.flush();
            out.close();
        }

        out      = null;
        isClosed = true;
    }

    @SneakyThrows
    private void openOnFirstWrite() {
        if ( out != null ) {
            return;
        } else if ( isClosed ) {
            throw new IllegalStateException("closed");
        }

        FileUtils.touchFileDirectory( file );

        out = new FileWriter(file, append);
    }
}
