package mosaics.json.gson;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import mosaics.fp.FP;
import mosaics.fp.LazyVal;
import mosaics.fp.collections.ConsList;
import mosaics.fp.collections.FPIterable;
import mosaics.fp.collections.FPList;
import mosaics.fp.collections.maps.FPMap;
import mosaics.fp.collections.FPOption;
import mosaics.fp.collections.FPSet;
import mosaics.fp.collections.RRBVector;
import mosaics.lang.TryUtils;
import mosaics.lang.functions.Function1WithThrows;
import mosaics.lang.reflection.JavaClass;
import mosaics.lang.time.DTM;
import mosaics.lang.time.Day;
import mosaics.strings.codecs.DTMCodecLong;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static mosaics.lang.Backdoor.cast;


public class FPTypeFactory implements TypeAdapterFactory {

    @SuppressWarnings("unchecked")
    public <T> TypeAdapter<T> create( Gson gson, TypeToken<T> typeToken) {
        Class<T> rawType = (Class<T>) typeToken.getRawType();

        if ( FPOption.class.isAssignableFrom(rawType) ) {
            return createTypeAdapterFactoryFor( gson, typeToken, FPOptionTypeAdapter::new );
        } else if ( DTM.class == rawType ) {
            return cast(DTMTypeAdapter.INSTANCE);
        } else if ( Day.class == rawType ) {
            return cast(DayTypeAdapter.INSTANCE);
        } else if ( FPList.class.isAssignableFrom(rawType) ) {
            return createTypeAdapterFactoryFor( gson, typeToken, FPListTypeAdapter::new );
        } else if ( RRBVector.class.isAssignableFrom(rawType) ) {
            return createTypeAdapterFactoryFor( gson, typeToken, RRBVectorTypeAdapter::new );
        } else if ( FPSet.class.isAssignableFrom(rawType) ) {
            return createTypeAdapterFactoryFor( gson, typeToken, FPSetTypeAdapter::new );
        } else if ( FPMap.class.isAssignableFrom(rawType) ) {
            return cast(getTypeParameterGsonDelegatesFor(gson, typeToken).second().map(FPMapTypeAdapter::new).orNull());
        } else if ( ConsList.class.isAssignableFrom(rawType) ) {
            return createTypeAdapterFactoryFor( gson, typeToken, ConsListTypeAdapter::new );
        } else if ( LazyVal.class.isAssignableFrom(rawType) ) {
            return createTypeAdapterFactoryFor( gson, typeToken, LazyValTypeAdapter::new );
        } else {
            return null;
        }
    }

    private <T> TypeAdapter<T> createTypeAdapterFactoryFor( Gson gson, TypeToken<T> typeToken, Function1WithThrows<TypeAdapter, Object> typeAdapterFactory ) {
        return cast(getTypeParameterGsonDelegatesFor(gson, typeToken).first().map( typeAdapterFactory ).orNull());
    }

    @SuppressWarnings("unchecked")
    public static FPIterable<TypeAdapter> getTypeParameterGsonDelegatesFor( Gson gson, TypeToken typeToken ) {
        JavaClass jc = JavaClass.of( typeToken.getType() );

        return jc.getClassGenerics()
            .map( FPTypeFactory::toTypeToken )
            .map( gson::getAdapter );
    }

    private static TypeToken toTypeToken( JavaClass javaClass ) {
        if ( javaClass.getClassGenerics().isEmpty() ) {
            return TypeToken.get( cast(javaClass.getJdkClass()) );
        } else {
            Type[] parameters = javaClass.getClassGenerics().map( FPTypeFactory::toTypeToken ).toArray(Type.class);

            return TypeToken.getParameterized( javaClass.getJdkClass(), parameters );
        }
    }


    private static class DTMTypeAdapter extends TypeAdapter<DTM> {
        public static final DTMTypeAdapter INSTANCE = new DTMTypeAdapter();

        public void write( JsonWriter out, DTM value) throws IOException {
            if (value == null) {
                out.nullValue();
            } else {
                out.value( DTMCodecLong.DTM_ISO8601_CODEC.encode(value.getMillisSinceEpoch()) );
            }
        }

        public DTM read( JsonReader in) throws IOException {
            if (in.peek() == JsonToken.NULL) {
                in.nextNull();

                return null;
            }

            String v = in.nextString();

            return new DTM( DTMCodecLong.DTM_ISO8601_CODEC.decode(v).getResult() );
        }
    }


    private static class DayTypeAdapter extends TypeAdapter<Day> {
        public static final DayTypeAdapter INSTANCE = new DayTypeAdapter();

        public void write( JsonWriter out, Day value) throws IOException {
            if (value == null) {
                out.nullValue();
            } else {
                out.value( value.toString() );
            }
        }

        public Day read( JsonReader in) throws IOException {
            if (in.peek() == JsonToken.NULL) {
                in.nextNull();

                return null;
            }

            String v = in.nextString();

            return new Day( v );
        }
    }

    private static class FPOptionTypeAdapter<T> extends TypeAdapter<FPOption<T>> {
        private final TypeAdapter<T> delegate;

        public FPOptionTypeAdapter(TypeAdapter<T> delegate) {
            this.delegate = delegate;
        }

        public void write( JsonWriter out, FPOption<T> value) throws IOException {
            if (value == null) {
                out.nullValue();
            } else if (value.isEmpty()) {
                out.nullValue();
            } else {
                delegate.write( out, value.get() );
            }
        }

        public FPOption<T> read( JsonReader in) throws IOException {
            if (in.peek() == JsonToken.NULL) {
                in.nextNull();

                return FP.emptyOption();
            }

            return FP.option( delegate.read(in) );
        }
    }

    private static class LazyValTypeAdapter<T> extends TypeAdapter<LazyVal<T>> {
        private final TypeAdapter<T> delegate;

        public LazyValTypeAdapter(TypeAdapter<T> delegate) {
            this.delegate = delegate;
        }

        public void write( JsonWriter out, LazyVal<T> value) throws IOException {
            T fetchedValue = value.fetch();

            delegate.write( out, fetchedValue );
        }

        public LazyVal<T> read( JsonReader in) throws IOException {
            T fetchedValue = delegate.read(in);

            return new LazyVal<>( () -> fetchedValue );
        }
    }

    private static class FPListTypeAdapter<T> extends TypeAdapter<FPList<T>> {
        private final TypeAdapter<T> delegate;

        public FPListTypeAdapter(TypeAdapter<T> delegate) {
            this.delegate = delegate;
        }

        public void write( JsonWriter out, FPList<T> value) throws IOException {
            if (value == null) {
                out.nullValue();
            } else {
                out.beginArray();
                value.forEach( v -> TryUtils.invokeAndRethrowException( () -> delegate.write(out,v)) );
                out.endArray();
            }
        }

        public FPList<T> read( JsonReader in) throws IOException {
            if (in.peek() == JsonToken.NULL) {
                in.nextNull();

                return FP.emptyList();
            }

            List<T> list = new ArrayList<>();
            in.beginArray();
            while (in.peek() != JsonToken.END_ARRAY ) {
                T v = delegate.read( in );

                list.add(v);
            }

            in.endArray();

            return FP.toList( list );
        }
    }


    private static class RRBVectorTypeAdapter<T> extends TypeAdapter<RRBVector<T>> {
        private final TypeAdapter<T> delegate;

        public RRBVectorTypeAdapter(TypeAdapter<T> delegate) {
            this.delegate = delegate;
        }

        public void write( JsonWriter out, RRBVector<T> value) throws IOException {
            if (value == null) {
                out.nullValue();
            } else {
                out.beginArray();
                value.forEach( v -> TryUtils.invokeAndRethrowException( () -> delegate.write(out,v)) );
                out.endArray();
            }
        }

        public RRBVector<T> read( JsonReader in) throws IOException {
            if (in.peek() == JsonToken.NULL) {
                in.nextNull();

                return FP.toVector();
            }

            List<T> list = new ArrayList<>();
            in.beginArray();
            while (in.peek() != JsonToken.END_ARRAY ) {
                T v = delegate.read( in );

                list.add(v);
            }

            in.endArray();

            return RRBVector.toVector( list );
        }
    }


    private static class ConsListTypeAdapter<T> extends TypeAdapter<ConsList<T>> {
        private final TypeAdapter<T> delegate;

        public ConsListTypeAdapter(TypeAdapter<T> delegate) {
            this.delegate = delegate;
        }

        public void write( JsonWriter out, ConsList<T> value) throws IOException {
            if (value == null) {
                out.nullValue();
            } else {
                out.beginArray();
                value.forEach( v -> TryUtils.invokeAndRethrowException( () -> delegate.write(out,v)) );
                out.endArray();
            }
        }

        public ConsList<T> read( JsonReader in) throws IOException {
            if (in.peek() == JsonToken.NULL) {
                in.nextNull();

                return FP.toConsList();
            }

            List<T> list = new ArrayList<>();
            in.beginArray();
            while (in.peek() != JsonToken.END_ARRAY ) {
                T v = delegate.read( in );

                list.add(v);
            }

            in.endArray();
            Collections.reverse(list);

            return ConsList.consList( list );
        }
    }

    private static class FPSetTypeAdapter<T> extends TypeAdapter<FPSet<T>> {
        private final TypeAdapter<T> delegate;

        public FPSetTypeAdapter(TypeAdapter<T> delegate) {
            this.delegate = delegate;
        }

        public void write( JsonWriter out, FPSet<T> value) throws IOException {
            if (value == null) {
                out.nullValue();
            } else {
                out.beginArray();
                value.forEach( v -> TryUtils.invokeAndRethrowException( () -> delegate.write(out,v)) );
                out.endArray();
            }
        }

        public FPSet<T> read( JsonReader in) throws IOException {
            if (in.peek() == JsonToken.NULL) {
                in.nextNull();

                return FP.emptySet();
            }

            Set<T> set = new HashSet<>();
            in.beginArray();
            while (in.peek() != JsonToken.END_ARRAY ) {
                T v = delegate.read( in );

                set.add(v);
            }
            in.endArray();

            return FP.toSet( set );
        }
    }

    private static class FPMapTypeAdapter<V> extends TypeAdapter<FPMap<String,V>> {
        private final TypeAdapter<V> valueTypeAdapter;

        public FPMapTypeAdapter(TypeAdapter<V> valueTypeAdapter) {
            this.valueTypeAdapter = valueTypeAdapter;
        }

        public void write( JsonWriter out, FPMap<String,V> value) throws IOException {
            if (value == null) {
                out.nullValue();
            } else {
                out.beginObject();
                value.forEach( (k,v) -> TryUtils.invokeAndRethrowException( () -> {
                    out.name(k);
                    valueTypeAdapter.write(out,v);
                }) );
                out.endObject();
            }
        }

        public FPMap<String,V> read( JsonReader in) throws IOException {
            if (in.peek() == JsonToken.NULL) {
                in.nextNull();

                return FP.toMap();
            }

            Map<String,V> map = new HashMap<>();
            in.beginObject();
            while (in.peek() != JsonToken.END_OBJECT ) {
                String k = in.nextName();
                V      v = valueTypeAdapter.read( in );

                map.put( k, v );
            }

            in.endObject();

            return FP.wrapMap( map );
        }
    }
}
