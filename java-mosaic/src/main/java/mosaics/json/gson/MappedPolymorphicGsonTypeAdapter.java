package mosaics.json.gson;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;


/**
 * A GSON object serializer/deserializer that supports an object hierarchy by inserting a synthetic
 * JSON property that holds an identifying key.  The identifying key has to be declared before use.
 */
public class MappedPolymorphicGsonTypeAdapter<T> implements JsonSerializer<T>, JsonDeserializer<T> {

    private String jsonPropertyName = "type";

    private Map<String, Type> typeValueToClassMappings = new HashMap<>();
    private Map<Type,String> classToTypeValueMappings = new HashMap<>();

    public MappedPolymorphicGsonTypeAdapter() {}

    public MappedPolymorphicGsonTypeAdapter( Map<Type,String> mappings ) {
        classToTypeValueMappings.putAll( mappings );

        mappings.forEach( (type,key) -> typeValueToClassMappings.put(key,type) );
    }


    public MappedPolymorphicGsonTypeAdapter<T> withTypeJsonPropertyName( String propertyName ) {
        this.jsonPropertyName = propertyName;

        return this;
    }

    public <C extends T> MappedPolymorphicGsonTypeAdapter<T> withChildMapping( Class<C> type, String key ) {
        typeValueToClassMappings.put( key, type );
        classToTypeValueMappings.put( type, key );

        return this;
    }

    public JsonElement serialize( T obj, Type typeOfSrc, JsonSerializationContext context) {
        String typeValue = classToTypeValueMappings.get( obj.getClass() );

        JsonObject elem = context.serialize(obj).getAsJsonObject();
        elem.addProperty( jsonPropertyName, typeValue );

        return elem;
    }

    public T deserialize( JsonElement json, Type typeOfT, JsonDeserializationContext context ) throws JsonParseException {
        try {
            JsonObject jsonObject = json.getAsJsonObject();
            String     typeValue  = jsonObject.get(jsonPropertyName).getAsString();

            Type klass = typeValueToClassMappings.get(typeValue);

            return context.deserialize( json, klass );
        } catch ( UnsupportedOperationException ex ) {
            throw new JsonParseException( "Expected property '" + jsonPropertyName + "' to hold a String", ex );
        } catch ( NullPointerException ex ) {
            throw new JsonParseException( "Missing JSON property: '" + jsonPropertyName + "'", ex );
        }
    }

}
