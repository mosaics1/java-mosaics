package mosaics.json.gson;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;


/**
 * A GSON object serializer/deserializer that supports an object hierarchy by inserting a synthetic
 * JSON property that holds that Java classname of the DTO where the data came from.
 */
public class ClassNamePolymorphicGsonTypeAdapter<T> implements JsonSerializer<T>, JsonDeserializer<T> {
    private String jsonPropertyName = "type";


    public ClassNamePolymorphicGsonTypeAdapter<T> withJsonPropertyName( String propertyName ) {
        this.jsonPropertyName = propertyName;

        return this;
    }

    public JsonElement serialize( T obj, Type typeOfSrc, JsonSerializationContext context) {
        if ( obj == null ) {
            return JsonNull.INSTANCE;
        }

        Class<?> objectType = obj.getClass();
        String   className  = objectType.getName();

        JsonObject elem = context.serialize(obj).getAsJsonObject();
        elem.addProperty( jsonPropertyName, className );

        return elem;
    }

    public T deserialize( JsonElement json, Type typeOfT, JsonDeserializationContext context ) throws JsonParseException {
        JsonObject jsonObject = json.getAsJsonObject();

        try {
            String   className  = jsonObject.get(jsonPropertyName).getAsString();
            Class<?> klass      = Class.forName(className);

            return context.deserialize( json, klass );
        } catch ( ClassNotFoundException ex ) {
            throw new JsonParseException( "Unable to find class: '" + jsonObject.get(jsonPropertyName) + "'", ex );
        } catch ( UnsupportedOperationException ex ) {
            throw new JsonParseException( "Expected property '" + jsonPropertyName + "' to hold a String", ex );
        } catch ( NullPointerException ex ) {
            throw new JsonParseException( "Missing JSON property: '" + jsonPropertyName + "'", ex );
        }
    }
}
