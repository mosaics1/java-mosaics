package mosaics.json;

import lombok.SneakyThrows;
import mosaics.fp.FP;
import mosaics.lang.QA;
import mosaics.strings.StringUtils;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.File;
import java.io.FileReader;
import java.util.Iterator;
import java.util.List;


public class NDJsonFileReader<T> implements Iterator<T>, Closeable {
    public static <T> List<T> readAll( Class<T> pojoType, File f ) {
        try (NDJsonFileReader<T> in = new NDJsonFileReader<>(pojoType,f)) {
            return in.toList();
        }
    }

    private final Class<T>       pojoType;
    private final JsonCodec      codec;
    private final File           file;

    private       boolean        isClosed;
    private       BufferedReader in;
    private       Iterator<T>    it;


    public NDJsonFileReader( Class<T> pojoType, File f ) {
        this( pojoType, JsonCodec.ndJsonCodec, f );
    }

    public NDJsonFileReader( Class<T> pojoType, JsonCodec codec, File f ) {
        QA.argNotNull( pojoType, "pojoType" );
        QA.argNotNull( codec, "codec" );
        QA.argNotNull( f, "f" );

        this.pojoType = pojoType;
        this.codec    = codec;
        this.file     = f;
    }

    @SneakyThrows
    public boolean hasNext() {
        if ( it == null ) {
            if ( isClosed ) {
                throw new IllegalStateException("reader has been closed");
            } else if ( !file.exists() ) {
                return false;
            }

            in = new BufferedReader( new FileReader(file) );
            it = in.lines().map(String::trim)
                           .map(line -> line.replaceFirst("^#.*$", ""))
                           .filter(StringUtils::isNotBlank)
                           .map(line -> codec.fromJson(line, pojoType))
                           .iterator();
        }

        return it.hasNext();
    }

    public T next() {
        return it.next();
    }

    public List<T> toList() {
        return FP.wrap(this).toList();
    }

    @SneakyThrows
    public void close() {
        if ( in != null ) {
            in.close();
        }

        in       = null;
        it       = null;
        isClosed = true;
    }
}
