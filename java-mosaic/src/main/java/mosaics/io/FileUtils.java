package mosaics.io;

import lombok.SneakyThrows;
import mosaics.fp.FP;
import mosaics.fp.collections.FPOption;
import mosaics.lang.Backdoor;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;


public class FileUtils {

    @SneakyThrows
    public static List<String> readTextFile(File f) {
        return Files.lines(f.toPath()).collect(Collectors.toList());
    }

    /**
     * Delete all files in a directory whose name matches the specified predicate.
     */
    public static void deleteMatchingFiles( File directory, Predicate<String> predicate ) {
        File[] candidateFiles = directory.listFiles();
        if ( candidateFiles == null ) {
            return;
        }

        for ( File f : candidateFiles ) {
            if ( f.isFile() && predicate.test(f.getName()) ) {
                f.delete();
            }
        }
    }

    /**
     * Ensure that a file and all of its directories are present.
     */
    @SneakyThrows
    public static final void touchFile( File file ) {
        touchFileDirectory( file );

        file.createNewFile();
    }

    /**
     * Ensure that the directory for a file exists.
     */
    public static final void touchFileDirectory( File file ) {
        if ( file.exists() ) {
            return;
        }

        File dir = file.getParentFile();
        if ( dir != null ) {
            dir.mkdirs();
        }
    }

    public static File makeTempDirectory( String prefix ) {
        return makeTempDirectory( prefix, "" );
    }

    public static File makeTempDirectory( String prefix, String postfix ) {
        try {
            File f = File.createTempFile( prefix, postfix );

            f.delete();
            f.mkdir();

            return f;
        } catch ( IOException ex ) {
            throw Backdoor.throwException(ex);
        }
    }

    public static int deleteAll( File f ) {
        int count = 0;

        File[] children = f.listFiles();
        if ( children != null ) {
            for ( File child : children ) {
                if ( child.isDirectory() ) {
                    count += deleteAll(child);
                } else {
                    child.delete();
                    count += 1;
                }
            }
        }

        if ( f.delete() ) {
            count += 1;
        }

        return count;
    }

    public static FPOption<String> getExtension( String path ) {
        int i = path.lastIndexOf('.');

        if ( i < 0 ) {
            return FP.emptyOption();
        }

        return FP.option( path.substring( i + 1 ) );
    }

    public static String getNameWithoutExtension( String path ) {
        for ( int i=path.length()-1; i>=0; i-- ) {
            int c = path.charAt(i);

            if ( c == '.' ) {
                return path.substring( 0, i );
            } else if ( c == '/' ) {
                return path;
            }
        }

        return path;
    }

    public static void writeTextTo( File file, String...text ) {
        try {
            FileWriter out = new FileWriter(file, false);

            try {
                for ( String line : text ) {
                    out.write(line);
                    out.write(Backdoor.NEWLINE);
                }
            } finally {
                out.close();
            }
        } catch ( IOException e ) {
            throw Backdoor.throwException(e);
        }
    }

    public static File getFirstExistingParentDirectory( File f ) {
        if ( f.exists() ) {
            return f;
        }

        return getFirstExistingParentDirectory( f.getParentFile() );
    }

//    public static void copyResourceTo( String resource, File destFile ) {
//        try (
//            InputStream  in  = ReflectionUtils.getCallersClass().toJDK().getResourceAsStream( resource );
//            OutputStream out = new FileOutputStream( destFile )
//        ) {
//            IOUtils.copy( in, out );
//        } catch ( IOException ex ) {
//            throw Backdoor.throwException( ex );
//        }
//    }
}
