package mosaics.io;


import mosaics.fp.FP;
import mosaics.fp.collections.FPOption;
import mosaics.lang.lifecycle.Subscription;
import mosaics.utils.OSUtils;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;

import static mosaics.lang.Backdoor.UTF8;


/**
 * Uses a file to enforce a cross process pessimistic lock.
 */
public class LockFile {

    private File    file;
    private boolean isLocked;


    public LockFile( File dir, String fileName ) {
        this.file = new File( dir, fileName+".lock" );
    }


    public synchronized boolean isAvailable() {
        if ( isLocked ) {
            return false;
        } else if ( file.exists() ) {
            try {
                try (
                    RandomAccessFile raf     = new RandomAccessFile( file, "rwd" );
                    FileChannel      channel = raf.getChannel()
                ) {
                    FileLock lock = channel.tryLock();

                    if ( lock == null ) {
                        return false;
                    } else {
                        lock.release();
                        return true;
                    }
                } finally {
                    file.delete();
                }
            } catch ( IOException ex ) {
                throw new LockException( "unable to fully release lock: " + file.getAbsolutePath(), ex );
            }
        } else {
            return true;
        }
    }

    public Subscription lock() {
        return tryLock().orElseThrow( () -> new LockException("unable to lock "+file.getAbsolutePath()) );
    }

    public synchronized FPOption<Subscription> tryLock() {
        if ( !isAvailable() ) {
            return FP.emptyOption();
        }

        Thread.interrupted(); // clear interrupted flag

        file.delete();

        try {
            file.getParentFile().mkdirs();

            String           pid = Integer.toString(OSUtils.getCurrentPid());
            RandomAccessFile raf = new RandomAccessFile( file, "rwd" );

            FileChannel channel = raf.getChannel();
            FileLock    lock    = channel.tryLock();

            if ( lock == null ) {
                throw new LockException( "unable to lock: " + file );
            }

            channel.write( ByteBuffer.wrap(pid.getBytes(UTF8)) );

            this.isLocked = true;

            return FP.option( new Subscription( () -> {
                synchronized (LockFile.this) {
                    try {
                        lock.release();

                        channel.close();
                        raf.close();

                        file.delete();

                        LockFile.this.isLocked = false;
                    } catch ( IOException ex ) {
                        throw new LockException( "unable to fully release lock: " + file.getAbsolutePath(), ex );
                    }
                }
            } ) );
        } catch ( IOException ex ) {
            throw new LockException( "unable to lock: " + file, ex );
        }
    }

}
