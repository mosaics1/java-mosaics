package mosaics.io.xml;

import mosaics.io.writers.IndentWriter;
import mosaics.lang.functions.Function1;
import org.apache.commons.text.StringEscapeUtils;

import java.io.Closeable;
import java.io.Flushable;
import java.util.Map;
import java.util.Stack;


/**
 *
 */
public class TagWriter implements Flushable, Closeable {

    public static TagWriter xmlWriter( Appendable out ) {
        return new TagWriter( new IndentWriter(out), StringEscapeUtils::escapeXml11 );
    }

    public static TagWriter htmlWriter( Appendable out ) {
        return new TagWriter( new IndentWriter(out), StringEscapeUtils::escapeHtml4 );
    }


    private IndentWriter              out;
    private Function1<String, String> escaper;
    private Stack<TagContext>         stack = new Stack<>();


    public TagWriter( IndentWriter writer, Function1<String,String> escaper ) {
        this.out     = writer;
        this.escaper = escaper;
    }


    public void tag( String tagName, Map<String,String> attributes ) {
        tag( tagName );

        attributes.forEach( this::attr );

        closeTag( tagName );
    }

    public void tag( String tagName, String...attributes ) {
        tag( tagName );

        for ( int i=0; i<attributes.length; i+=2 ) {
            attr( attributes[i], attributes[i+1] );
        }

        closeTag( tagName );
    }

    public void tag( String tagName, String bodyText ) {
        tag( tagName );
        bodyText( bodyText );
        closeTag(tagName);
    }

    public void tag( String tagName ) {
        boolean indentTag = !stack.isEmpty();

        if ( indentTag ) {
            stack.peek().enterBody();
        }

        TagContext tag = new TagContext( tagName, indentTag );
        stack.push( tag );

        tag.openTag();
    }

    public void attr( String name, String value ) {
        throwIfNoOpenTags( "Cannot print an attribute when no tag is currently open" );

        stack.peek().attr( name, escaper.invoke(value) );
    }

    public void bodyText( String txt ) {
        throwIfNoOpenTags( "Cannot append a body to a tag that is not currently open" );

        stack.peek().bodyText( escaper.invoke(txt) );
    }

    public void closeTag( String expectedTagName ) {
        throwIfNoOpenTags( "Cannot close a tag when none is currently open" );

        stack.pop().closeTag( expectedTagName );
    }


    public void flush() {
        out.flush();
    }

    public void close() {
        if ( !stack.isEmpty() ) {
            throw new IllegalStateException( stack.size() + " tags remain open.  Be sure to explicitely close them." );
        }

        out.close();
    }


    private void throwIfNoOpenTags( String msg ) {
        if ( stack.isEmpty() ) {
            throw new IllegalStateException( msg );
        }
    }


    private class TagContext {
        private final String  tagName;
        private final boolean indentedTag;

        private       boolean hasBody;
        private       boolean isTextBody;


        public TagContext( String tagName, boolean indentedTag ) {
            this.tagName     = tagName;
            this.indentedTag = indentedTag;
        }

        public void openTag() {
            if ( indentedTag ) {
                out.incIndent();
                out.newLine();
            }

            out.append( '<' );
            out.append( tagName );
        }

        public void attr( String name, String value ) {
            if ( hasBody ) {
                throw new IllegalStateException( "Cannot add attributes to a tag after its body has been written to" );
            }

            out.append( ' ' );
            out.append( name );
            out.append( "=\"" );
            out.append( value );
            out.append( '\"' );
        }

        public void bodyText( String txt ) {
            if ( !isTextBody ) {
                out.append('>');
            }

            out.append( txt );

            this.hasBody    = true;
            this.isTextBody = true;
        }

        public void closeTag( String expectedTagName ) {
            if ( !expectedTagName.equals(tagName) ) {
                throw new IllegalStateException( "Mismatched tag names.  Expected to be closing '"+expectedTagName+"' but instead found '"+tagName+"'" );
            }

            if ( hasBody ) {
                if ( !isTextBody ) {
                    out.newLine();
                }

                out.append( "</" );
                out.append( tagName );
                out.append( '>' );
            } else {
                out.append( "/>" );
            }

            if ( indentedTag ) {
                out.decIndent();
            }
        }

        public void enterBody() {
            if ( hasBody ) {
                return;
            }

            out.append( '>' );

            hasBody = true;
        }
    }
}
