package mosaics.io.readers;

import mosaics.lang.Backdoor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;


/**
 * Reader for reading strings that have been split across multiple lines.  The format mirrors the
 * Unix convention of ending strings that are continued on the next line with '\'.
 */
public class MultiLineReader extends BufferedReader {
    public MultiLineReader( Reader in, int sz ) {
        super( in, sz );
    }

    public MultiLineReader( Reader in ) {
        super( in );
    }

    @Override
    public String readLine() {
        try {
            String line = super.readLine();

            if ( line == null || !line.endsWith("/") ) {
                return line;
            }

            StringBuilder buf = new StringBuilder();
            buf.append( line, 0, line.length()-1 );

            line = super.readLine();
            while ( line != null && line.endsWith("/") ) {
                buf.append( Backdoor.NEWLINE );
                buf.append( line, 0, line.length()-1 );

                line = super.readLine();
            }

            if ( line != null ) {
                buf.append( Backdoor.NEWLINE );
                buf.append( line );
            }

            return buf.toString();
        } catch ( IOException ex ) {
            throw Backdoor.throwException( ex );
        }
    }
}
