package mosaics.io;


public class RuntimeIOException extends RuntimeException {
    public RuntimeIOException( String msg ) {
        super( msg );
    }

    public RuntimeIOException( String msg, Throwable ex ) {
        super( msg, ex );
    }
}
