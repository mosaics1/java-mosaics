package mosaics.io;

import lombok.SneakyThrows;
import mosaics.lang.Backdoor;
import mosaics.lang.MissingEnvironmentVariable;
import mosaics.lang.QA;

import java.io.*;
import java.nio.charset.Charset;
import java.util.Optional;


public class IOUtils {
    public static final int BUF_SIZE = 0x1000; // 4K


    public static final Charset UTF8    = Backdoor.UTF8;
    public static final String  NEWLINE = System.getProperty( "line.separator" );


    public static File getUsersHomeDirectory() {
        return Optional.of(System.getProperty("user.home")).map( File::new).orElseThrow( () -> new MissingEnvironmentVariable("user.home") );
    }

    @SneakyThrows
    public static String toString( File in ) {
        return toString(new FileReader(in) );
    }

    public static String toString( Reader in ) {
        char[] buf = new char[8 * 1024];

        StringBuilder buffer = new StringBuilder();

        try {
            int count;
            while ( (count = in.read( buf, 0, buf.length )) != -1 ) {
                buffer.append( buf, 0, count );
            }
        } catch ( IOException ex ) {
            // convert to the equivalent of a RuntimeException, this is a debatable practice
            // in this case, I have done it because I want to avoid adding 'throws Throwable' to the
            // FunctionX class of interfaces.  naturally other options exist, this one was chosen
            // as the least invasive and smelly of them.
            throw Backdoor.throwException(ex);
        }

        return buffer.toString();
    }


    public static void flush( Appendable o ) {
        if ( o instanceof Flushable ) {
            try {
                ((Flushable) o).flush();
            } catch ( IOException e ) {

            }
        }
    }

    public static void close( Closeable c ) {
        if ( c == null ) {
            return;
        }

        try {
            c.close();
        } catch ( IOException e ) {
        }
    }

    public static void close( Appendable o ) {
        if ( o instanceof Closeable ) {
            close( (Closeable) o );
        }
    }

    public static long copy( InputStream in, OutputStream out ) {
        QA.argNotNull( in, "in" );
        QA.argNotNull( out, "out" );

        try {
            byte[] buf   = new byte[BUF_SIZE];
            long   total = 0;

            while ( true ) {
                int r = in.read( buf );
                if ( r == -1 ) {
                    break;
                }

                out.write( buf, 0, r );
                total += r;
            }

            return total;
        } catch ( IOException ex ) {
            throw Backdoor.throwException( ex );
        }
    }

    public static long copy( Reader in, Writer out ) {
        QA.argNotNull( in, "in" );
        QA.argNotNull( out, "out" );

        try {
            char[] buf   = new char[BUF_SIZE];
            long   total = 0;

            while ( true ) {
                int r = in.read( buf );
                if ( r == -1 ) {
                    break;
                }

                out.write( buf, 0, r );
                total += r;
            }

            return total;
        } catch ( IOException ex ) {
            throw Backdoor.throwException( ex );
        }
    }

    public static void copyResource( Class source, String resourcePath, File dest ) {
        try {
            InputStream in   = source.getResourceAsStream( resourcePath );

            String fileName = new File(resourcePath).getName();

            File destDir = dest.getParentFile();
            destDir.mkdirs();

            QA.throwIfNotDirectory( destDir, "destDir" );
            QA.throwIfNotWritable( destDir, "destDir" );

            FileOutputStream out = new FileOutputStream( dest, false );

            try {
                copy( in, out );
            } finally {
                out.flush();
                out.close();
            }
        } catch ( IOException ex ) {
            throw Backdoor.throwException( ex );
        }
    }

    @SuppressWarnings("unused")
    public static <T extends Serializable> Optional<T> deserializeFrom( Class<T> expectedType, File sourceFile ) {
        if ( !sourceFile.exists() ) {
            return Optional.empty();
        }

        try {
            ObjectInputStream in = new ObjectInputStream( new FileInputStream(sourceFile) );

            try {
                return Optional.ofNullable( Backdoor.cast(in.readObject()) );
            } finally {
                in.close();
            }
        } catch ( IOException | ClassCastException | ClassNotFoundException ex ) {
            throw Backdoor.throwException( ex );
        }
    }


    /**
     * Overwrites the specified file with the specified object.
     */
    public static <T extends Serializable> void serializeTo( File targetFile, T v ) {
        targetFile.getParentFile().mkdirs();

        try {
            ObjectOutputStream out = new ObjectOutputStream( new FileOutputStream(targetFile) );

            try {
                out.writeObject( v );
            } finally {
                out.flush();
                out.close();
            }
        } catch ( IOException e ) {
            throw Backdoor.throwException( e );
        }
    }

    public static BufferedReader bufferedReader( Reader reader ) {
        if ( reader instanceof BufferedReader ) {
            return (BufferedReader) reader;
        } else {
            return new BufferedReader( reader );
        }
    }
}
