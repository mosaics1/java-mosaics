package mosaics.io.writers;

import java.io.Closeable;
import java.io.Flushable;


public interface Writer extends Appendable, Closeable, Flushable, NewLineMixin {
}
