package mosaics.io.writers;

import mosaics.lang.Backdoor;
import mosaics.lang.QA;
import mosaics.lang.TryUtils;
import mosaics.lang.functions.VoidFunction0WithThrows;

import java.io.Closeable;
import java.io.Flushable;
import java.io.IOException;
import java.util.List;


/**
 * Enhances an existing Writer with automatic prefixing of each line with an indentation string.
 * The level of indentation may be varied as required.
 */
public class IndentWriter extends java.io.Writer implements Writer {

    public static IndentWriter toIndentingWriter( Appendable buf ) {
        if ( buf instanceof IndentWriter ) {
            return (IndentWriter) buf;
        } else {
            return new IndentWriter( buf );
        }
    }


    private final Appendable delegate;
    private final String indentText;

    private int     indentationLevel      = 0;
    private boolean indentBeforeNextWrite = true;


    public IndentWriter( Appendable delegate ) {
        this( delegate, "  " );
    }

    public IndentWriter( Appendable delegate, String indentText ) {
        QA.argNotNull(delegate, "delegate");

        this.indentText = indentText;
        this.delegate   = delegate;
    }

    public int getIndentLevel() {
        return indentationLevel;
    }

    public int incIndent() {
        return ++indentationLevel;
    }

    public int decIndent() {
        indentationLevel -= 1;

        QA.isGTEZero(indentationLevel, "indentationLevel");

        return indentationLevel;
    }

    public void print( char c ) {
        append( c );
    }

    public void print( String txt ) {
        append(txt);
    }

    public void println( char c ) {
        print(c);
        newLine();
    }

    public void println( String txt ) {
        print(txt);
        newLine();
    }

    public void newLine() {
        TryUtils.invokeAndRethrowException(() -> delegate.append( Backdoor.NEWLINE));

        indentBeforeNextWrite = true;
    }

    public void print( CharSequence str ) {
        append(str);
    }

    public void println( CharSequence str ) {
        append( str );
        newLine();
    }

    public void write( char[] cbuf, int off, int len ) {
        prefixOutputIffRequired();

        try {
            for ( int i = off; i < off + len; i++ ) {
                delegate.append( cbuf[i] );
            }
        } catch ( IOException ex ) {
            throw Backdoor.throwException( ex );
        }
    }

    public java.io.Writer append( CharSequence csq ) {
        if ( csq.equals(Backdoor.NEWLINE) ) {
            newLine();
        } else if ( csq.length() != 0 ) {
            prefixOutputIffRequired();

            try {
                delegate.append(csq);
            } catch ( IOException ex ) {
                throw Backdoor.throwException( ex );
            }


            indentBeforeNextWrite = csq.toString().endsWith(Backdoor.NEWLINE);
        }

        return this;
    }

    public java.io.Writer append( CharSequence csq, int start, int end ) {
        prefixOutputIffRequired();

        try {
            delegate.append( csq, start, end );
        } catch ( IOException ex ) {
            throw Backdoor.throwException( ex );
        }

        return this;
    }

    public java.io.Writer append( char c ) {
        prefixOutputIffRequired();

        try {
            delegate.append( c );
        } catch ( IOException ex ) {
            throw Backdoor.throwException( ex );
        }

        return this;
    }

    public java.io.Writer appendNull() {
        return append( "null" );
    }

    public java.io.Writer append( int c ) {
        return append( Integer.toString(c) );
    }

    public java.io.Writer append( long v ) {
        return append( Long.toString(v) );
    }

    public java.io.Writer append( float v ) {
        return append( Float.toString(v) );
    }

    public java.io.Writer append( double v ) {
        return append( Double.toString(v) );
    }

    public void flush() {
        if ( delegate instanceof Flushable ) {
            TryUtils.invokeAndRethrowException(((Flushable) delegate)::flush);
        }
    }

    public void close() {
        if ( delegate instanceof Flushable ) {
            TryUtils.invokeAndRethrowException(((Flushable) delegate)::flush);
        }

        if ( delegate instanceof Closeable ) {
            TryUtils.invokeAndRethrowException(((Closeable) delegate)::close);
        }
    }


    private void prefixOutputIffRequired() {
        if ( indentBeforeNextWrite ) {
            try {
                for ( int i=0; i<indentationLevel; i++ ) {
                    delegate.append( indentText );
                }
            } catch ( IOException ex ) {
                throw Backdoor.throwException( ex );
            }

            indentBeforeNextWrite = false;
        }
    }

    public void indent( VoidFunction0WithThrows f ) {
        incIndent();

        try {
            f.invoke();
        } catch ( Throwable ex ) {
            throw Backdoor.throwException(ex);
        } finally {
            decIndent();
        }
    }

    public void printLines( List<String> lines ) {
        lines.forEach( this::println );
    }

    public void printEach( List<String> values, String separator ) {
        boolean printSeparatorFlag = false;

        for ( String v : values ) {
            if ( printSeparatorFlag ) {
                print( separator );
            } else {
                printSeparatorFlag = true;
            }

            print(v);
        }
    }
}
