package mosaics.io.writers;

import java.io.PrintWriter;


public class NullPrintWriter {

    public static final PrintWriter INSTANCE = new PrintWriter( NullWriter.nullWriter() );

    public static final PrintWriter nullPrintWriter() {
        return INSTANCE;
    }

}
