package mosaics.io.writers;

import java.io.Writer;


public class NullWriter extends Writer implements NewLineMixin {

    public static final Writer INSTANCE = new NullWriter();

    public static Writer nullWriter() {
        return INSTANCE;
    }

    private NullWriter() {}


    public void write( char[] cbuf, int off, int len ) {}

    public void flush() {}

    public void close() {}

}
