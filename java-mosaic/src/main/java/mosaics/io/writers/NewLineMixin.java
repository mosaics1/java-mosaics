package mosaics.io.writers;


import lombok.SneakyThrows;
import mosaics.io.IOUtils;


public interface NewLineMixin extends Appendable {
    @SneakyThrows
    public default void newLine() {
        append( IOUtils.NEWLINE );
    }
}
