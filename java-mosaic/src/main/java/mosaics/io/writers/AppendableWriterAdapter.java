package mosaics.io.writers;


import java.io.Closeable;
import java.io.Flushable;
import java.io.IOException;


// lifted from: https://stackoverflow.com/questions/21200421/how-to-wrap-a-java-lang-appendable-into-a-java-io-writer
public class AppendableWriterAdapter extends java.io.Writer implements NewLineMixin, Writer {
    private final Appendable appendable;

    public AppendableWriterAdapter(Appendable appendable) {
        this.appendable = appendable;
    }

    @Override
    public void write(char[] cbuf, int off, int len) throws IOException {
        appendable.append(String.valueOf(cbuf), off, len);
    }

    @Override
    public void flush() throws IOException {
        if (appendable instanceof Flushable) {
            ((Flushable) appendable).flush();
        }
    }

    @Override
    public void close() throws IOException {
        flush();
        if (appendable instanceof Closeable ) {
            ((Closeable) appendable).close();
        }
    }

}
