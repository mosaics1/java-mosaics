package mosaics.io;

import mosaics.lang.Secret;
import lombok.AllArgsConstructor;
import lombok.Value;


@Value
@AllArgsConstructor
public class CredentialConfig {
    private String username;
    private Secret password;

    public CredentialConfig( String username, String password ) {
        this( username, new Secret(password) );
    }
}
