package mosaics.io;

import mosaics.lang.TryUtils;
import org.apache.commons.lang3.StringUtils;

import java.math.BigInteger;
import java.security.MessageDigest;


public class Md5Utils {

    public static String calcMd5For( String txt )  {
        MessageDigest md = createMd5Builder();

        md.update( txt.getBytes() );

        return toHash( md );
    }


    private static MessageDigest createMd5Builder() {
        return TryUtils.invokeAndRethrowException( () -> MessageDigest.getInstance("MD5") );
    }

    private static String toHash( MessageDigest md ) {
        BigInteger bigInt = new BigInteger( 1, md.digest() );

        return StringUtils.leftPad( bigInt.toString(16), 32, "0" );
    }

}
