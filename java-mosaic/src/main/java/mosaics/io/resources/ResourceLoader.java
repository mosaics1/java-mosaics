package mosaics.io.resources;

import mosaics.fp.FP;
import mosaics.fp.collections.FPOption;
import mosaics.io.IOUtils;
import mosaics.lang.Backdoor;
import mosaics.lang.functions.Function1;

import java.io.InputStream;
import java.io.Reader;
import java.nio.charset.Charset;
import java.util.Map;


public interface ResourceLoader {

    public <T> FPOption<T> fetchDocument( Class<T> targetModelType, String url );
    public <T> FPOption<T> fetchDocument( Class<T> targetModelType, String url, FPOption<Charset> defaultCharset, FPOption<MimeType> defaultMimeType );



    public default FPOption<Map<String,String>> fetchProperties( String url ) {
        return Backdoor.cast( fetchDocument(Map.class, url, FP.option( Backdoor.UTF8 ), FP.emptyOption()) );
    }

    public default FPOption<String> fetchString( String url ) {
        return fetchDocument(String.class, url);
    }

    public default FPOption<InputStream> openBinaryStream( String url ) {
        return fetchDocument(InputStream.class, url);
    }

    public default <T> FPOption<T> processBinaryStream( String url, Function1<InputStream,T> streamHandler ) {
        return openBinaryStream(url).map(in -> {
            try {
                return streamHandler.invoke(in);
            } finally {
                IOUtils.close(in);
            }
        });
    }

    public default FPOption<Reader> openTextStream( String url ) {
        return fetchDocument(Reader.class, url);
    }

    public default <T> FPOption<T> processTextStream( String url, Function1<Reader,T> streamHandler ) {
        return openTextStream(url).map(in -> {
            try {
                return streamHandler.invoke(in);
            } finally {
                IOUtils.close(in);
            }
        });
    }

}
