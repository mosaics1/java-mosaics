package mosaics.io.resources;

import mosaics.fp.FP;
import mosaics.fp.Tryable;
import mosaics.fp.collections.FPOption;
import mosaics.fp.collections.RRBVector;
import lombok.Value;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


@Value
public class MimeType {
    private static final Map<String,MimeType> MIME_TYPES = new ConcurrentHashMap<>();

    public static final MimeType BINARY       = createAndRegisterMimeType("application/octet-stream", "bin", "dat");
    public static final MimeType HTML         = createAndRegisterMimeType("text/html", "htm", "html");
    public static final MimeType JSON         = createAndRegisterMimeType("application/json", "json");
    public static final MimeType NDJSON       = createAndRegisterMimeType("application/x-ndjson", "ndjson");
    public static final MimeType XML          = createAndRegisterMimeType("text/xml", "xml");
    public static final MimeType TEXT         = createAndRegisterMimeType("text/plain", "txt");
    public static final MimeType PROPERTYFILE = createAndRegisterMimeType("text/x-java-properties", "properties");


    public static final RRBVector<MimeType> getMimeTypesFrom( String acceptHeader ) {
        return FP.wrapAll(acceptHeader.split(","))
            .map( String::trim )
            .map( MimeTypeAcceptEntry::fromSingleAcceptHeaderElement )
            .filter( Tryable::hasResult )
            .map( Tryable::getResult )
            .sort()
            .map( MimeTypeAcceptEntry::toMimeType )
            .toRRBVector();
    }

    public static final FPOption<MimeType> getMimeTypeFrom( String contentTypeHeader ) {
        return FPOption.ofBlankableString(contentTypeHeader)
            .flatMap( str -> {
                String mimeType1 = before(contentTypeHeader, ';').trim();

                return FP.option( MIME_TYPES.get(mimeType1) );
            });
    }

    private static String before( String str, char c ) {
        int i = str.indexOf( c );

        return i < 0 ? str : str.substring( 0, i );
    }

    static final MimeType createAndRegisterMimeType( String mimeType, String...fileExtensions ) {
        return MIME_TYPES.computeIfAbsent( mimeType, m -> new MimeType(mimeType,fileExtensions) );
    }

    public static final MimeType lookupMimeType( String primary, String secondary ) {
        String   key      = primary + "/" + secondary;
        MimeType mimeType = MIME_TYPES.get( key );

        return mimeType == null ? new MimeType( key ) : mimeType;
    }


    private final String       mimeType;
    private final List<String> fileExtensions;


    MimeType( String mimeType, String...fileExtensions ) {
        this.mimeType       = mimeType;
        this.fileExtensions = Arrays.asList( fileExtensions );
    }

    public String getPrimary() {
        return mimeType.substring( 0, mimeType.indexOf('/') );
    }

    public String getSecondary() {
        return mimeType.substring( mimeType.indexOf('/')+1 );
    }

    public boolean hasWildCard() {
        return mimeType.contains( "*" );
    }

    /**
     * Returns true if lhs matches rhs.  eg &ast;&sol;&ast;.matches(text/html) returns true.
     */
    public boolean matches( MimeType other ) {
        return matches(this.getPrimary(), other.getPrimary()) && matches(this.getSecondary(),other.getSecondary());
    }

    private boolean matches( String lhs, String rhs ) {
        return lhs.equals(rhs) || lhs.equals("*") || rhs.equals("*");
    }

}
