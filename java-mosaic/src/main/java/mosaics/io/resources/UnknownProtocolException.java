package mosaics.io.resources;

import mosaics.lang.NotFoundException;


public class UnknownProtocolException extends NotFoundException {
    public UnknownProtocolException( String mimeType ) {
        super(mimeType);
    }
}
