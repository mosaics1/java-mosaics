package mosaics.io.resources;

import mosaics.fp.collections.FPOption;

import java.io.InputStream;
import java.nio.charset.Charset;


public interface ResourceDecoder {
    public <T> FPOption<T> decode( Class<T> targetModelType, InputStream inputStream, FPOption<Charset> characterSet, MimeType mimeType );

    public MimeType getResourceMimeType();
}
