package mosaics.io.resources;

import mosaics.lang.NotFoundException;


public class UnknownDecoderException extends NotFoundException {
    public UnknownDecoderException( String msg ) {
        super( msg );
    }
}
