package mosaics.io.resources;

import mosaics.fp.FP;
import mosaics.fp.collections.FPOption;
import mosaics.io.resources.extensions.ClassPathTransportProtocol;
import mosaics.io.resources.extensions.FileTransportProtocol;
import mosaics.io.resources.extensions.PropertyDecoder;
import mosaics.lang.Backdoor;
import mosaics.lang.NotFoundException;
import mosaics.net.HostUtils;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URI;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;


/**
 * A pluggable implementation of ResourceLoader.  This service offers two extension points, the first
 * is the transport layer.  The transport layer is declared by the first part of the URL which
 * is officially known as the 'scheme'.  For example 'http' or 'file'.<p/>
 *
 * The second extension point controls how the contents of the resource gets converted into
 * Java objects, and is called the ResourceDecoder.  The resource decoder is selected by the mime
 * type associated with the resource.  If no mime type can be identified then decoding will fail.
 */
public class URLLoader implements ResourceLoader {

    private Map<String,TransportProtocolHandler> protocols       = new HashMap<>();
    private Map<MimeType,ResourceDecoder>        decoders        = new HashMap<>();
    private FPOption<String>                     defaultProtocol = FP.emptyOption();

    public URLLoader() {
//        withProtocolHandler( HttpTransportProtocol );
//        withProtocolHandler( HttpsTransportProtocol );
        withProtocolHandler( FileTransportProtocol.INSTANCE );
        withProtocolHandler( ClassPathTransportProtocol.INSTANCE );
//        withProtocolHandler( VaultTransportProtocol );
//        withProtocolHandler( ConsolTransportProtocol );

//        withResourceDecoder( JsonDecoder );
//        withResourceDecoder( XMLDecoder );
//        withResourceDecoder( CSVDecoder );
        withResourceDecoder( PropertyDecoder.INSTANCE );
    }

    public URLLoader withDefaultProtocol( String defaultProtocol ) {
        this.defaultProtocol = FP.option( defaultProtocol );

        return this;
    }

    public URLLoader withProtocolHandler( TransportProtocolHandler transportProtocolHandler ) {
        protocols.put( transportProtocolHandler.getProtocolName(), transportProtocolHandler );

        return this;
    }

    public URLLoader withResourceDecoder( ResourceDecoder resourceDecoder ) {
        decoders.put( resourceDecoder.getResourceMimeType(), resourceDecoder );

        return this;
    }


    public <T> FPOption<T> fetchDocument( Class<T> targetModelType, String url ) {
        return fetchDocument( targetModelType, url, FP.emptyOption(), FP.emptyOption() );
    }

    public <T> FPOption<T> fetchDocument( Class<T> targetModelType, String url, FPOption<Charset> defaultCharset, FPOption<MimeType> defaultMimeType ) {
        URI                      uri      = HostUtils.toURI(url);
        FPOption<String>         scheme   = FP.option(uri.getScheme());
        TransportProtocolHandler protocol = protocols.get(scheme.or(defaultProtocol).orNull());

        if ( protocol == null ) {
            throw new UnknownProtocolException("Unsupported protocol '"+scheme.orElse("")+"' in '"+url+"'");
        }

        String modifiedUrl = scheme.map(u -> url)
            .or( () -> defaultProtocol.map(d -> d+"://"+url) )
            .orElse( url );

        FPOption<ResourceInputStream> in = protocol.fetchURL(modifiedUrl);

        return in
            .map( r -> r.withDefaults(defaultCharset, defaultMimeType) ).flatMap(r -> decode(targetModelType, r))
            .toFPOption();
    }

    private <T> FPOption<T> decode( Class<T> targetModelType, ResourceInputStream in ) {
        FPOption result = _decodeInternal(targetModelType, in);

        return Backdoor.cast(result);
    }

    @SuppressWarnings( "unchecked" )
    private FPOption _decodeInternal( Class targetModelType, ResourceInputStream in ) {
        if ( targetModelType == InputStream.class ) {
            return FP.option(in.asInputStream());
        } else if ( targetModelType == Reader.class ) {
            InputStreamReader reader = in.asReader();

            return FP.option(reader);
        } else if ( targetModelType == String.class ) {
            return FP.option(in.asString());
        } // todo byte[] or ByteBuffer
          // what about Iterable, IterableX, MapX


        MimeType        mimeType = in.getMimeType().orElseThrow(() -> new NotFoundException("Found no mimetype for '"+in.getURL()+"'"));
        ResourceDecoder decoder  = decoders.get(mimeType);

        if ( decoder == null ) {
            throw new UnknownDecoderException( "No decoder found for mime type '"+mimeType.toString()+"' from '"+in.getURL()+"'" );
        }

        return in.decodeUsing( targetModelType, decoder, mimeType );
    }

}
