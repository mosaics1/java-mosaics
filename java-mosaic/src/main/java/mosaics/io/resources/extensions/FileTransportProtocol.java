package mosaics.io.resources.extensions;

import mosaics.fp.FP;
import mosaics.fp.StreamUtils;
import mosaics.fp.collections.FPOption;
import mosaics.io.resources.MimeType;
import mosaics.io.resources.MimeTypes;
import mosaics.io.resources.ResourceInputStream;
import mosaics.io.resources.TransportProtocolHandler;
import mosaics.lang.Backdoor;
import mosaics.net.HostUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class FileTransportProtocol implements TransportProtocolHandler {

    public static final TransportProtocolHandler INSTANCE = new FileTransportProtocol();


    public String getProtocolName() {
        return "file";
    }

    public FPOption<ResourceInputStream> fetchURL( String url ) {
        File f = toFile( url );

        try {
            if ( !f.exists() ) {
                return FP.emptyOption();
            }

            FPOption<MimeType> mimeType = MimeTypes.fromFile(f);
            FileInputStream    in       = new FileInputStream(f);

            return FP.option( new ResourceInputStream(url, in, FP.emptyOption(), mimeType) );
        } catch ( IOException e ) {
            throw Backdoor.throwException(e);
        }
    }

    public List<String> fetchChildDirectories( String parentUrl ) {
        File f = toFile( parentUrl );

        return fetchChildrenOf( f )
            .filter( File::isDirectory )
            .map( cf -> "file://"+cf.getAbsolutePath() )
            .collect( Collectors.toList() );
    }

    public List<String> fetchChildFiles( String parentUrl ) {
        File f = toFile( parentUrl );

        return fetchChildrenOf(f)
            .filter( File::isFile )
            .map( cf -> "file://"+cf.getAbsolutePath() )
            .collect( Collectors.toList() );
    }

    public List<String> fetchChildFilesRecursively( String parentUrl ) {
        File root = toFile( parentUrl );

        return StreamUtils.breadthFirst(root, this::fetchChildrenOf )
            .filter( File::isFile )
            .map( cf -> "file://"+cf.getAbsolutePath() )
            .collect( Collectors.toList() );
    }


    private Stream<File> fetchChildrenOf( File f ) {
        File[] children = f.listFiles();

        if ( children == null ) {
            return Stream.empty();
        }

        return Arrays.stream( children );
    }

    private File toFile( String url ) {
        URI uri = HostUtils.toURI(url);

        try {
            return new File(uri.toURL().getFile());
        } catch ( MalformedURLException e ) {
            throw Backdoor.throwException( e );
        }
    }

}
