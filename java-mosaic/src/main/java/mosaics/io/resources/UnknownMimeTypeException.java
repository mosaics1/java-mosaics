package mosaics.io.resources;


import mosaics.lang.NotFoundException;


public class UnknownMimeTypeException extends NotFoundException {
    public UnknownMimeTypeException( String mimeType ) {
        super(mimeType);
    }
}
