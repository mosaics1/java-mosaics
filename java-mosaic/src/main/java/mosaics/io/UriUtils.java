package mosaics.io;

import mosaics.lang.ArrayUtils;
import mosaics.lang.Backdoor;
import mosaics.net.HostUtils;

import java.net.URI;


public class UriUtils {

    public static String[] extractPathArrayFrom( String uri ) {
        String   path      = extractPathFrom( uri );
        String[] fragments = path.split( Backdoor.FILE_SEPARATOR );

        return ArrayUtils.filter( String.class, fragments, str -> str != null && str.trim().length() > 0 );
    }

    public static String extractPathFrom( String uri ) {
        URI uri1 = HostUtils.toURI( uri );

        return uri1.getPath() == null ? uri1.getSchemeSpecificPart() : uri1.getPath();
    }

    public static String extractFileNameFrom( String uri ) {
        String[] path = extractPathArrayFrom( uri );

        if ( path == null || path.length == 0 ) {
            return null;
        }

        return path[path.length-1];
    }

}
