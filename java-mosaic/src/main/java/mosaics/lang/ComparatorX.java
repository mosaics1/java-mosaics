package mosaics.lang;

import java.util.Comparator;

import static mosaics.lang.Backdoor.cast;


public interface ComparatorX<T>  {
    public static <T extends Comparable<T>> int compare( T a, T b ) {
        return a.compareTo(b);
    }

    public int measureDistanceBetween( T a, T b );

    public default ComparisonResult compare( T a, T b ) {
        int distance = measureDistanceBetween( a, b );

        return ComparisonResult.fromInt( distance );
    }


    public default Comparator<T> toComparator() {
        return (a,b) -> compare(a,b).toInt();
    }

    public default T max( T a, T b ) {
        return this.compare(a,b).isGTE() ? a : b;
    }

    public default T min( T a, T b ) {
        return this.compare(a,b).isLTE() ? cast(this) : b;
    }

    public default boolean isGT( T a, T b ) {
        return this.compare(a,b).isGT();
    }

    public default boolean isGTE( T a, T b ) {
        return this.compare(a,b).isGTE();
    }

    public default boolean isEQ( T a, T b ) {
        return this.compare(a,b).isEQ();
    }

    public default boolean isLT( T a, T b ) {
        return this.compare(a,b).isLT();
    }

    public default boolean isLTE( T a, T b ) {
        return this.compare(a,b).isLTE();
    }
}
