package mosaics.lang;

public class NotFoundException extends RuntimeException {
    public NotFoundException( String message ) {
        super( message );
    }

    public NotFoundException( String message, String...args ) {
        super( String.format(message,(Object[]) args) );
    }

    public NotFoundException( String message, Throwable cause ) {
        super( message, cause );
    }
}

