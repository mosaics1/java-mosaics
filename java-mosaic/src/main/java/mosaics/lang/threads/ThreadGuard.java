package mosaics.lang.threads;

import mosaics.lang.Assert;


/**
 * Enforce that only a single thread is being used to pass through this 'condition guard'.
 */
public class ThreadGuard {
    private Thread previousNbl;

    public void assertSingleThreaded() {
        if ( Assert.areAssertionsEnabled() ) {
            Thread currentThread = Thread.currentThread();

            synchronized (this) {
                if ( previousNbl == null ) {
                    previousNbl = currentThread;
                }

                Assert.isTrueState( previousNbl == currentThread, "Multiple threads detected" );
            }
        }
    }
}
