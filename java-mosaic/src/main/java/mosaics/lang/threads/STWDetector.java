package mosaics.lang.threads;

import mosaics.lang.Backdoor;
import mosaics.lang.QA;
import mosaics.lang.functions.VoidFunction0;
import mosaics.lang.functions.VoidFunction0;


/**
 * Infers system 'stop the world' freezes by measuring delays in thread scheduling.  Small delays
 * are normal, however when the system locks up due to GC or swap usage then thread scheduling will
 * be impacted quite badly.  This class schedules a thread to wake up every millisecond.  It tracks
 * how long after its expected wake up time it actually gets woken up and records the delay.<p/>
 *
 * This class starts tracking delays from the moment that this class is loaded into the JVM.<p/>
 *
 * Example:
 *
 * <pre>
 *     long delay0                 = getTotalDelaySoFarMillis();
 *
 *     // do work
 *
 *     long delay1                 = getTotalDelaySoFarMillis();
 *     long delayExperiencedMillis = delay1 - delay0;
 * </pre>
 */
public class STWDetector {
    private static final long MEASURE_INTERVAL_MILLIS = 1;

    private static STWDetector global;

    public static synchronized STWDetector global() {
        if ( global == null ) {
            global = new STWDetector( "STWDetector-Global" ).start();
        }

        return global;
    }


    private final String threadName;

    private volatile boolean keepRunning = true;
    private volatile long    totalDelaySoFarMillis = 0;

    private volatile Thread  thread;


    public STWDetector( String name ) {
        this.threadName = name;
    }

    public STWDetector start() {
        if ( thread != null ) {
            return this;
        }

        keepRunning = true;

        this.thread = new Thread( this::run );

        return this;
    }

    public void stop() {
        keepRunning = false;
    }

    public long getTotalDelaySoFarMillis() {
        return totalDelaySoFarMillis;
    }

    public STWCounter stwCounter() {
        return new STWCounter() {
            private long t0 = totalDelaySoFarMillis;

            public long getSTWTotal() {
                long total = totalDelaySoFarMillis - t0;

                QA.isGTEZero(total, "STW counter has overflowed");

                return total;
            }
        };
    }

    public long time( VoidFunction0 f ) {
        long t0    = System.currentTimeMillis();
        long stwT0 = getTotalDelaySoFarMillis();

        f.invoke();

        long jobDuration = System.currentTimeMillis() - t0;
        long stwDuration = stwT0 - getTotalDelaySoFarMillis();

        return jobDuration - stwDuration;
    }

    private void run() {
        while (keepRunning) {
            long t0 = System.nanoTime();

            Backdoor.sleep( MEASURE_INTERVAL_MILLIS );

            // NB sub millisecond delays are considered noise, so we round those away
            long t1         = System.nanoTime();
            long delayMillis = nanosToMillis(t0, t1) - MEASURE_INTERVAL_MILLIS;

            totalDelaySoFarMillis = Math.max(0, totalDelaySoFarMillis+delayMillis);
        }
    }


    private long nanosToMillis( long t0, long t1 ) {
        return (t1-t0)/1000000;
    }

    public static void main(String[] args) throws InterruptedException {
        STWDetector stwDetector = new STWDetector( "STWDetector" ).start();

        while (true) {
            Thread.sleep(1000);

            System.out.println( "totalDelaySoFarMillis = " + stwDetector.getTotalDelaySoFarMillis() );
        }
    }

}
