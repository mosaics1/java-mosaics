package mosaics.lang.threads;


import mosaics.fp.FP;
import mosaics.fp.collections.FPOption;
import mosaics.lang.Backdoor;
import mosaics.lang.RandomUtils;
import mosaics.lang.functions.BooleanFunction0;
import mosaics.lang.functions.Function0;
import mosaics.lang.functions.LongFunction0;

import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;


public class ThreadUtils {

    public static Set<Thread> allRunningThreads() {
        return Thread.getAllStackTraces().keySet();
    }

    /**
     * Block the current thread until the specified condition is true or at least timeoutMillis of
     * active CPU time has past.  System stalls, such as stop the world GCs or swap usage will not
     * count towards timeoutMillis.
     */
    public static void spinUntilTrue( BooleanFunction0 predicate ) {
        spinUntilTrue( 10_000, predicate );
    }

    public static void spinUntilFalse( BooleanFunction0 predicate ) {
        spinUntilTrue( 10_000, predicate.invert() );
    }

    /**
     * Block the current thread until the specified condition is true or at least timeoutMillis of
     * active CPU time has past.  System stalls, such as stop the world GCs or swap usage will not
     * count towards timeoutMillis.
     */
    public static void spinUntilTrue( long timeoutMillis, BooleanFunction0 predicate ) {
        spinUntilTrue( timeoutMillis, 0, predicate );
    }


    public static void spinUntilTrue( long timeoutMillis, long retryIntervalMillis, BooleanFunction0 predicate ) {
        spinUntilTrue( timeoutMillis, () -> retryIntervalMillis, predicate );
    }

    public static void spinUntilTrue( long timeoutMillis, long retryIntervalBaseMillis, int rndRetryDeltaMillis, BooleanFunction0 predicate ) {
        spinUntilTrue( timeoutMillis, () -> retryIntervalBaseMillis+RandomUtils.rnd(rndRetryDeltaMillis), predicate );
    }

    public static void spinUntilTrue( long timeoutMillis, LongFunction0 retryIntervalMillis, BooleanFunction0 predicate ) {
        long startMillis  = System.currentTimeMillis();
        long systemDelay0 = STWDetector.global().getTotalDelaySoFarMillis();

        while ( !predicate.invoke() ) {
            long durationMillis = System.currentTimeMillis() - startMillis;

            if ( durationMillis > timeoutMillis ) {
                // we have exceeded the max duration via the wall clock time;  however has there
                // been any major stop the world GCs or OS stalls due to IO etc?  If so, give this
                // spin more time.
                long systemDelay1     = STWDetector.global().getTotalDelaySoFarMillis();
                long totalDelayMillis = systemDelay1 - systemDelay0;

                if ( durationMillis-totalDelayMillis > timeoutMillis ) {
                    throw new TimeoutException(timeoutMillis);
                }
            }

            long sleepMillis = retryIntervalMillis.invoke();
            if ( sleepMillis > 0 ) {
                Backdoor.sleep( sleepMillis );
            } else {
                Thread.yield();
            }
        }
    }

    /**
     * Call a function that blocks until it returns a result.  Do so from another thread and error
     * if it takes longer than a timeout.
     */
    @SuppressWarnings("deprecation")
    public static <T> T fetchBlockingActionFromAnotherThread( Function0<T> blockingAction ) {
        AtomicReference<Optional<T>> result = new AtomicReference<>(null);

        Thread thread = new DaemonThread("ThreadUtils.spinInAnotherThread") {

            @Override
            public void run() {
                result.set( Optional.ofNullable(blockingAction.invoke()) );
            }
        };

        thread.start();

        try {
            return spinUntilNotNull( result::get ).orElse( null );
        } catch ( Throwable ex ) {
            thread.interrupt();

            try {
                spinUntilTrue( () -> !thread.isAlive() );
            } catch ( Throwable ex2 ) {
                thread.stop(); // last resort;  the thread is not playing nice
            }

            throw Backdoor.throwException( ex );
        }
    }

    public static <T> T spinUntilNotNull( Function0<T> action ) {
        return spinUntilNotNull( action, 10_000, () -> 10+RandomUtils.rnd(50) );
    }

    public static <T> T spinUntilNotNull( Function0<T> action, long timeoutMillis, LongFunction0 retryIntervalMillis ) {
        long startMillis  = System.currentTimeMillis();
        long systemDelay0 = STWDetector.global().getTotalDelaySoFarMillis();

        T result = action.invoke();
        while ( result == null ) {
            long durationMillis = System.currentTimeMillis() - startMillis;

            if ( durationMillis > timeoutMillis ) {
                // we have exceeded the max duration via the wall clock time;  however has there
                // been any major stop the world GCs or OS stalls due to IO etc?  If so, give this
                // spin more time.
                long systemDelay1     = STWDetector.global().getTotalDelaySoFarMillis();
                long totalDelayMillis = systemDelay1 - systemDelay0;

                if ( durationMillis-totalDelayMillis > timeoutMillis ) {
                    throw new TimeoutException(timeoutMillis);
                }
            }

            long sleepMillis = retryIntervalMillis.invoke();
            if ( sleepMillis > 0 ) {
                Backdoor.sleep( sleepMillis );
            } else {
                Thread.yield();
            }

            result = action.invoke();
        }

        return result;
    }


    public static FPOption<Thread> fetchThreadByName( String targetThreadName ) {
        return FP.wrap(allRunningThreads()).first( t -> Objects.equals(t.getName(), targetThreadName) );
    }

}
