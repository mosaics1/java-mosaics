package mosaics.lang.threads;

public interface STWCounter {
    public long getSTWTotal();
}
