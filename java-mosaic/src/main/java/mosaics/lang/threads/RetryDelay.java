package mosaics.lang.threads;

import mosaics.lang.Backdoor;

import java.time.Duration;


public class RetryDelay {
    private static final long MIN = 1;
    private static final long MAX = Duration.ofSeconds(30).toMillis();

    public static long exponentialBackoffSeries( int attemptNumber ) {
        // seq: 25+rnd(50), 50+rnd(100), 100+rnd(200), 200+rnd(200) ...
        long b = (long) (Math.pow(2,attemptNumber) * 25);
        long a = b/2;

        long delayMillis = a + Backdoor.nextRandomLong(b);

        if ( delayMillis < 0 || delayMillis > MAX ) {
            return MAX;
        }

        return delayMillis;
    }
}
