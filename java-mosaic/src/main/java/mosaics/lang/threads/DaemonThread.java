package mosaics.lang.threads;

public class DaemonThread extends Thread {
    public DaemonThread( String threadName ) {
        setName( threadName );
        setDaemon( true );
    }
}
