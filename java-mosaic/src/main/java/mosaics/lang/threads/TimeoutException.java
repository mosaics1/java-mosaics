package mosaics.lang.threads;

import lombok.Getter;


public class TimeoutException extends RuntimeException {
    @Getter private long timeoutMillis;

    public TimeoutException( long timeoutMillis ) {
        super( "Timeout exceeded: " + timeoutMillis );
        this.timeoutMillis = timeoutMillis;
    }
}
