package mosaics.lang.time;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import mosaics.lang.ComparableMixin;

import java.util.Comparator;

import static mosaics.lang.Backdoor.toInt;

@Getter
@Builder
@EqualsAndHashCode
public class DTMInterval implements ComparableMixin<DTMInterval> {
    public static final DTMInterval ZERO            = DTMInterval.builder().build();
    public static final DTMInterval ONE_MILLISECOND = millisecond();
    public static final DTMInterval ONE_SECOND      = second();
    public static final DTMInterval ONE_MINUTE      = minute();
    public static final DTMInterval ONE_HOUR        = hour();
    public static final DTMInterval ONE_DAY         = day();
    public static final DTMInterval ONE_WEEK        = week();
    public static final DTMInterval ONE_MONTH       = month();
    public static final DTMInterval ONE_YEAR        = year();

    private static final Comparator<DTMInterval> COMPARATOR = Comparator.comparingInt( DTMInterval::getNumYears )
        .thenComparingInt( DTMInterval::getNumMonths )
        .thenComparingInt( DTMInterval::getNumWeeks )
        .thenComparingInt( DTMInterval::getNumDays )
        .thenComparingInt( DTMInterval::getNumHours )
        .thenComparingInt( DTMInterval::getNumMinutes )
        .thenComparingInt( DTMInterval::getNumSeconds )
        .thenComparingLong( DTMInterval::getNumMilliseconds );


    public static DTMInterval millisecond() {
        return milliseconds( 1 );
    }

    public static DTMInterval milliseconds( long numMilliseconds ) {
        return new DTMInterval(0,0,0,0,0,0,0,numMilliseconds);
    }

    public static DTMInterval second() {
        return seconds(1);
    }

    public static DTMInterval seconds( int numSeconds ) {
        return DTMInterval.builder().numSeconds( numSeconds ).build();
    }

    public static DTMInterval minute() {
        return minutes(1);
    }

    public static DTMInterval minutes( int numMinutes ) {
        return DTMInterval.builder().numMinutes( numMinutes ).build();
    }


    public static DTMInterval hour() {
        return hours(1);
    }

    public static DTMInterval hours( int numHours ) {
        return DTMInterval.builder().numHours( numHours ).build();
    }

    public static DTMInterval day() {
        return days(1);
    }

    public static DTMInterval days( int numDays ) {
        return DTMInterval.builder().numDays( numDays ).build();
    }

    public static DTMInterval week() {
        return weeks(1);
    }

    public static DTMInterval weeks( int numWeeks ) {
        return DTMInterval.builder().numWeeks( numWeeks ).build();
    }

    public static DTMInterval month() {
        return months(1);
    }

    public static DTMInterval months( int numMonths ) {
        return DTMInterval.builder().numMonths( numMonths ).build();
    }

    public static DTMInterval year() {
        return years(1);
    }

    public static DTMInterval years( int numYears ) {
        return DTMInterval.builder().numYears( numYears ).build();
    }



    private final int  numYears;
    private final int  numMonths;
    private final int  numWeeks;
    private final int  numDays;
    private final int  numHours;
    private final int  numMinutes;
    private final int  numSeconds;
    private final long numMilliseconds;

    public DTMInterval( int numYears, int numMonths, int numWeeks, int numDays, int numHours, int numMinutes, int numSeconds ) {
        this(numYears, numMonths, numWeeks, numDays, numHours, numMinutes, numSeconds, 0);
    }

    public DTMInterval( int numYears, int numMonths, int numWeeks, int numDays, int numHours, int numMinutes, int numSeconds, long numMilliseconds ) {
        this.numYears        = numYears + numMonths/12;
        this.numMonths       = numMonths%12;
        this.numWeeks        = toInt(numWeeks + numDays/7 + numHours/(24*7) + numMinutes/(60*24*7) + numSeconds/(60*60*24*7) + (numMilliseconds/(1000*60*60*24*7)));
        this.numDays         = toInt(numDays%7 + (numHours/24)%7 + (numMinutes/(60*24))%7 + (numSeconds/(60*60*24))%7 + (numMilliseconds/(1000*60*60*24))%7);
        this.numHours        = toInt(numHours%24 + (numMinutes/60)%24 + (numSeconds/(60*60))%24 + (numMilliseconds/(1000*60*60))%24);
        this.numMinutes      = toInt(numMinutes%60 + (numSeconds/60)%60 + (numMilliseconds/(1000*60))%60);
        this.numSeconds      = toInt(numSeconds%60 + (numMilliseconds/1000)%60);
        this.numMilliseconds = toInt(numMilliseconds%1000);
    }

    public DTM incDTM( DTM dtm ) {
        return dtm
            .addYears(numYears)
            .addMonths(numMonths)
            .addWeeks(numWeeks)
            .addDays(numDays)
            .addHours( numHours )
            .addMinutes( numMinutes )
            .addSeconds( numSeconds );
    }

    public DTM decDTM( DTM dtm ) {
        return dtm
            .subtractYears(numYears)
            .subtractMonths(numMonths)
            .subtractWeeks(numWeeks)
            .subtractDays(numDays)
            .subtractHours( numHours )
            .subtractMinutes( numMinutes )
            .subtractSeconds( numSeconds );
    }

    public DTMInterval multiplyBy( int m ) {
        if ( m == 1 ) {
            return this;
        }

        return new DTMInterval( numYears*m, numMonths*m, numWeeks*m, numDays*m, numHours*m, numMinutes*m, numSeconds*m, numMilliseconds*m );
    }

    public DTMInterval addInterval( DTMInterval other ) {
        if ( this.equals(ZERO) ) {
            return other;
        } else if ( other.equals(ZERO) ) {
            return this;
        }

        return new DTMInterval(
            this.numYears+other.numYears,
            this.numMonths+other.numMonths,
            this.numWeeks+other.numWeeks,
            this.numDays+other.numDays,
            this.numHours+other.numHours,
            this.numMinutes+other.numMinutes,
            this.numSeconds+other.numSeconds
        );
    }

    public DTMInterval subtractInterval( DTMInterval other ) {
        if ( this.equals(ZERO) ) {
            return other;
        } else if ( other.equals(ZERO) ) {
            return this;
        }

        return new DTMInterval(
            this.numYears-other.numYears,
            this.numMonths-other.numMonths,
            this.numWeeks-other.numWeeks,
            this.numDays-other.numDays,
            this.numHours-other.numHours,
            this.numMinutes-other.numMinutes,
            this.numSeconds-other.numSeconds
        );
    }

    public long toMillis() {
        long result = numMilliseconds;

        result += numSeconds * DTMUtils.SECOND_MILLIS;
        result += numMinutes * DTMUtils.MINUTE_MILLIS;
        result += numHours * DTMUtils.HOUR_MILLIS;
        result += numDays * DTMUtils.DAY_MILLIS;
        result += numWeeks * DTMUtils.WEEK_MILLIS;
        result += numMonths * DTMUtils.DAY_MILLIS * 30;
        result += numYears * DTMUtils.DAY_MILLIS * 365;

        return result;
    }

    /**
     * What is the remainder after subtracting other as many times as it will go without
     * going negative.  The result does not take into account that different months
     * have different lengths, or leap years or leap seconds etc.  As such it gives an
     * estimate that is consistent and close to the truth.  If not exact in many cases.
     */
    public DTMInterval remainder( DTMInterval other ) {
        long millisA = this.toMillis();
        long millisB = other.toMillis();

        return DTMInterval.milliseconds( millisA % millisB );
    }

    public boolean isZero() {
        return toMillis() == 0L;
    }

    public String toString() {
        if ( this.equals(ZERO) ) {
            return "0";
        }

        StringBuilder buf = new StringBuilder();

        append( buf, numYears, "Y" );
        append( buf, numMonths, "M" );
        append( buf, numWeeks, "w" );
        append( buf, numDays, "d" );
        append( buf, numHours, "h" );
        append( buf, numMinutes, "m" );
        append( buf, numSeconds, "s" );
        append( buf, numMilliseconds, "ms" );

        return buf.toString();
    }

    private void append( StringBuilder buf, long n, String unit ) {
        if ( n != 0 ) {
            buf.append( n );
            buf.append( unit );
        }
    }

    public int compareTo( DTMInterval o ) {
        return COMPARATOR.compare( this, o );
    }

    public double divideByInterval( DTMInterval other ) {
        double millisA = this.toMillis();
        double millisB = other.toMillis();

        return millisA / millisB;  // NB this is a bit of an estimate, does not account for leap years etc
    }
}
