package mosaics.lang.time;

import lombok.Value;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.temporal.TemporalAccessor;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;


@Value
public final class Day {
    public static final Day EPOCH = new Day( 0L );


    private final long epochDays;


    public Day( String str ) {
        this( DayUtils.parse(str,DayUtils.DATE_ISO8601_FORMATTER) );
    }

    public Day( Date date ) {
        this( date.toInstant() );
    }

    public Day( int year, int month, int day ) {
        this( DayUtils.toEpochDays(year,month,day) );
    }

    public Day( Calendar cal ) {
        this( LocalDate.ofInstant(cal.toInstant(), cal.getTimeZone().toZoneId()) );
    }

    public Day( TemporalAccessor temporalAccessor ) {
        this( Instant.from(temporalAccessor) );
    }

    public Day( Instant instant ) {
        this( LocalDate.ofInstant(instant, ZoneId.of("UTC")) );
    }

    public Day( LocalDate localDate ) {
        this( localDate.toEpochDay() );
    }

    public Day( long epochDays ) {
        this.epochDays = epochDays;
    }


    public Calendar toJDKCalendar() {
        return toJDKCalendar(TZ.UTC);
    }

    public Calendar toJDKCalendar( TZ timeZone ) {
        Calendar c = GregorianCalendar.getInstance();

        c.setTimeZone( timeZone.toJDKTimeZone() );
        c.setTimeInMillis( epochDays );

        return c;
    }

    public LocalDate toJdkLocalDate() {
        return LocalDate.ofEpochDay( epochDays );
    }

    public int getYear() {
        return toJdkLocalDate().getYear();
    }

    public int getMonth() {
        return toJdkLocalDate().getMonthValue();
    }

    public int getDayOfMonth() {
        return toJdkLocalDate().getDayOfMonth();
    }

    public Day addDay() {
        return addDays(1);
    }

    public Day addDays( int i ) {
        return new Day( toJdkLocalDate().plusDays(i) );
    }

    public Day addMonth() {
        return addMonths(1);
    }

    public Day addMonths( int i ) {
        return new Day( toJdkLocalDate().plusMonths(i) );
    }


    public Day addYear() {
        return addYears(1);
    }

    public Day addYears( int i ) {
        return new Day( toJdkLocalDate().plusYears(i) );
    }

    public Day setDay( int i ) {
        return new Day( toJdkLocalDate().withDayOfMonth(i) );
    }

    public Day subtractYear() {
        return subtractYears(1);
    }

    public Day subtractYears( int i ) {
        return new Day( toJdkLocalDate().minusYears(i) );
    }

    public Day subtractMonth() {
        return subtractMonths(1);
    }

    public Day subtractMonths( int i ) {
        return new Day( toJdkLocalDate().minusMonths(i) );
    }

    public Day subtractDay() {
        return subtractDays(1);
    }

    public Day subtractDays( int i ) {
        return new Day( toJdkLocalDate().minusDays(i) );
    }

//    public Day rollForwardToNextSpecifiedDay( int targetDay ) {
//        return rollForwardToNextSpecifiedDay(targetDay, TZ.UTC);
//    }
//
//    public Day rollForwardToNextSpecifiedDay( int targetDay, TZ timeZone ) {
//        Calendar cal = toJDKCalendar(timeZone);
//
//        int currentDayOfMonth = cal.get(Calendar.DAY_OF_MONTH);
//        if ( currentDayOfMonth == targetDay ) {
//            return this;
//        }
//
//        int gap;
//        if ( currentDayOfMonth < targetDay ) {
//            gap = targetDay - currentDayOfMonth;
//
//            cal.add(Calendar.DAY_OF_MONTH, gap);
//        } else {
//            gap = targetDay + 28 - currentDayOfMonth;
//            cal.add(Calendar.DAY_OF_MONTH, gap);
//
//            while ( cal.get(Calendar.DAY_OF_MONTH) != targetDay ) {
//                cal.add( Calendar.DAY_OF_MONTH, 1 );
//            }
//        }
//
//        return cal.getTimeInMillis() == epochDays ? this : new Day(cal);
//    }
//
//    public Day rollForwardDaysWhile( Predicate<Day> predicate ) {
//        Day day = this;
//        while ( predicate.test(day) ) {
//            day = day.addDay();
//        }
//
//        return day;
//    }

    public boolean isWeekend() {
        return DayUtils.isWeekend( LocalDate.ofEpochDay(epochDays).getDayOfWeek() );
    }

    public DTM toDTM() {
        return new DTM( toJdkLocalDate().atTime(0,0,0,0).toInstant(ZoneOffset.UTC) );
    }

    public String toString() {
        return DayUtils.DATE_ISO8601_FORMATTER.format( toJdkLocalDate() );
    }
}
