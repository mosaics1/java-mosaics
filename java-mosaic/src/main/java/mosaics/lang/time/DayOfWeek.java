package mosaics.lang.time;

import mosaics.lang.ComparableMixin;

import java.util.Calendar;


public enum DayOfWeek implements ComparableMixin<DayOfWeek> {
    MONDAY(Calendar.MONDAY),
    TUESDAY(Calendar.TUESDAY),
    WEDNESDAY(Calendar.WEDNESDAY),
    THURSDAY(Calendar.THURSDAY),
    FRIDAY(Calendar.FRIDAY),
    SATURDAY(Calendar.SATURDAY),
    SUNDAY(Calendar.SUNDAY);

    public static DayOfWeek ofCalendarDayOfWeek( final int v ) {
        return switch (v) {
            case Calendar.MONDAY    -> MONDAY;
            case Calendar.TUESDAY   -> TUESDAY;
            case Calendar.WEDNESDAY -> WEDNESDAY;
            case Calendar.THURSDAY  -> THURSDAY;
            case Calendar.FRIDAY    -> FRIDAY;
            case Calendar.SATURDAY  -> SATURDAY;
            case Calendar.SUNDAY    -> SUNDAY;
            default -> throw new IllegalArgumentException(v + " is not a recognised day of the week");
        };
    }

    private final int calendarValue;

    private DayOfWeek( int calendarValue ) {
        this.calendarValue = calendarValue;
    }

    public int toJdkCalendarEnum() {
        return calendarValue;
    }
}
