package mosaics.lang.time;

import java.util.Calendar;


public class CalendarUtils {
    public static Calendar rollForwardToNextSpecifiedSecond( Calendar cal, int targetSecond ) {
        int currentSeconds = cal.get(Calendar.SECOND);
        if ( currentSeconds == targetSecond ) {
            return cal;
        }

        int gap;
        if ( currentSeconds < targetSecond ) {
            gap = targetSecond - currentSeconds;
        } else {
            gap = targetSecond + 60 - currentSeconds;
        }

        cal.add(Calendar.SECOND, gap);

        return cal;
    }

    public static Calendar rollForwardToNextSpecifiedMinute( Calendar cal, int targetMinute ) {
        int currentMinutes = cal.get(Calendar.MINUTE);
        if ( currentMinutes == targetMinute ) {
            return cal;
        }

        int gap;
        if ( currentMinutes < targetMinute ) {
            gap = targetMinute - currentMinutes;
        } else {
            gap = targetMinute + 60 - currentMinutes;
        }

        cal.add(Calendar.MINUTE, gap);

        return cal;
    }

    public static Calendar rollForwardToNextSpecifiedDay( Calendar cal, int targetDay ) {
        int currentDayOfMonth = cal.get(Calendar.DAY_OF_MONTH);
        if ( currentDayOfMonth == targetDay ) {
            return cal;
        }

        int gap;
        if ( currentDayOfMonth < targetDay ) {
            gap = targetDay - currentDayOfMonth;

            cal.add(Calendar.DAY_OF_MONTH, gap);
        } else {
            gap = targetDay + 28 - currentDayOfMonth;
            cal.add(Calendar.DAY_OF_MONTH, gap);

            while ( cal.get(Calendar.DAY_OF_MONTH) != targetDay ) {
                cal.add( Calendar.DAY_OF_MONTH, 1 );
            }
        }

        return cal;
    }

    public static Calendar rollForwardToNextSpecifiedHour( Calendar cal, int targetHour ) {
        return rollForwardToNextSpecifiedHour(cal, targetHour, TZ.UTC);
    }

    public static Calendar rollForwardToNextSpecifiedHour( Calendar cal, int targetHour, TZ timeZone ) {
        int currentHourOfDay = cal.get(Calendar.HOUR_OF_DAY);
        if ( currentHourOfDay == targetHour ) {
            return cal;
        }

        int gap;
        if ( currentHourOfDay < targetHour ) {
            gap = targetHour - currentHourOfDay;
        } else {
            gap = targetHour + 24 - currentHourOfDay;
        }

        cal.add(Calendar.HOUR_OF_DAY, gap);

        return cal;
    }
}
