package mosaics.lang.time;

import mosaics.lang.Backdoor;
import mosaics.lang.QA;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.function.LongPredicate;


public final class DTMUtils {//2018-05-01T16:41:10Z

    public static final SimpleDateFormat DEFAULT_FORMATTER = createFormatter( "yyyy-MM-dd HH:mm:ss", TZ.UTC );
    public static final SimpleDateFormat DTM_ISO8601_FORMATTER = createFormatter( "yyyy-MM-dd'T'HH:mm:ss.S'Z'", TZ.UTC );
    public static final SimpleDateFormat DATE_ISO8601_FORMATTER = createFormatter( "yyyy-MM-dd", TZ.UTC );

    public static final long SECOND_MILLIS = 1000;
    public static final long MINUTE_MILLIS = 60*1000;
    public static final long HOUR_MILLIS   = 60*MINUTE_MILLIS;
    public static final long DAY_MILLIS    = 24*HOUR_MILLIS;
    public static final long WEEK_MILLIS   = 7*DAY_MILLIS;



    public static long parse( String dtm )  {
        synchronized (DEFAULT_FORMATTER) {
            try {
                return DEFAULT_FORMATTER.parse(dtm).getTime();
            } catch ( ParseException e ) {
                throw Backdoor.throwException( e );
            }
        }
    }

    public static long parseISO8601( String dtm ) {
        if ( dtm.contains("T") ) {
            return ZonedDateTime.parse( dtm ).toInstant().toEpochMilli();
        } else {
            return parse( dtm, DATE_ISO8601_FORMATTER );
        }
    }

    public static long parse( String dtm, String format, TZ tz ) {
        try {
            return createFormatter(format,tz).parse(dtm).getTime();
        } catch ( ParseException e ) {
            throw Backdoor.throwException( e );
        }
    }

    public static long parse( String dtm, DateFormat df ) {
        try {
            synchronized (df) {
                return df.parse(dtm).getTime();
            }
        } catch ( ParseException e ) {
            throw Backdoor.throwException( e );
        }
    }

    public static String toString( long epoch ) {
        Date date = new Date(epoch);

        synchronized (DEFAULT_FORMATTER) {
            return DEFAULT_FORMATTER.format(date);
        }
    }

    public static String toString( long epoch, String format, TZ tz ) {
        SimpleDateFormat formatter = createFormatter( format, tz );

        Date date = new Date(epoch);

        return formatter.format(date);
    }

    public static String toString( long epoch, DateFormat formatter ) {
        return formatter.format(epoch);
    }

    public static long toEpoch( int year, int month, int day ) {
        return toEpoch(year, month, day, 0, 0, 0, 0, TZ.UTC);
    }

    public static long toEpoch( int year, int month, int day, TZ tz ) {
        return toEpoch(year, month, day, 0, 0, 0, 0, tz);
    }

    public static long toEpoch( int year, int month, int day, int hour, int minute ) {
        return toEpoch(year, month, day, hour, minute, 0);
    }

    public static long toEpoch( int year, int month, int day, int hour, int minute, int seconds ) {
        return toEpoch(year, month, day, hour, minute, seconds, 0, TZ.UTC);
    }

    public static long toEpoch( int year, int month, int day, int hour, int minute, int seconds, int millis ) {
        return toEpoch( year, month, day, hour, minute, seconds, millis, TZ.UTC );
    }

    public static long toEpoch( int year, int month, int day, int hour, int minute, int seconds, int millis, TZ tz ) {
        QA.argInclusiveBetween(1, month, 12, "month");
        QA.argInclusiveBetween( 1, day, 31, "day" );
        QA.argInclusiveBetween( 0, hour, 23, "hour" );
        QA.argInclusiveBetween( 0, minute, 59, "minutes" );
        QA.argInclusiveBetween( 0, seconds, 59, "seconds" );
        QA.argInclusiveBetween( 0, millis, 999, "millis" );

        Calendar c = GregorianCalendar.getInstance();

        c.setTimeZone( tz.toJDKTimeZone() );

        c.set( Calendar.YEAR,         year );
        c.set( Calendar.MONTH,        month-1 );
        c.set( Calendar.DAY_OF_MONTH, day );

        c.set( Calendar.HOUR_OF_DAY,  hour );
        c.set( Calendar.MINUTE,       minute );
        c.set( Calendar.SECOND,       seconds );

        c.set( Calendar.MILLISECOND,  millis );


        return c.getTimeInMillis();
    }

    public static Calendar toJDKCalendar( long millisSinceEpoch ) {
        return toJDKCalendar(millisSinceEpoch, TZ.UTC);
    }



    public static Calendar toJDKCalendar( long millisSinceEpoch, TZ timeZone ) {
        Calendar c = GregorianCalendar.getInstance();

        c.setTimeZone( timeZone.toJDKTimeZone() );
        c.setTimeInMillis( millisSinceEpoch );

        return c;
    }

    public static int getYear( long millisSinceEpoch ) {
        return toJDKCalendar(millisSinceEpoch).get(Calendar.YEAR);
    }

    public static int getMonth( long millisSinceEpoch ) {
        return toJDKCalendar(millisSinceEpoch).get( Calendar.MONTH ) + 1;
    }

    public static int getDayOfMonth( long millisSinceEpoch ) {
        return toJDKCalendar(millisSinceEpoch).get( Calendar.DAY_OF_MONTH );
    }

    public static int getHour( long millisSinceEpoch ) {
        return toJDKCalendar(millisSinceEpoch).get(Calendar.HOUR_OF_DAY);
    }


    public static int getMinutes( long millisSinceEpoch ) {
        return toJDKCalendar(millisSinceEpoch).get(Calendar.MINUTE);
    }

    public static int getSeconds( long millisSinceEpoch ) {
        return toJDKCalendar(millisSinceEpoch).get(Calendar.SECOND);
    }

    public static int getMillis( long millisSinceEpoch ) {
        return toJDKCalendar(millisSinceEpoch).get(Calendar.MILLISECOND);
    }

    public static long addDay( long millisSinceEpoch ) {
        return addDays(millisSinceEpoch, 1);
    }

    public static long addDays( long millisSinceEpoch, int i ) {
        Calendar cal = toJDKCalendar(millisSinceEpoch);
        cal.add( Calendar.DAY_OF_MONTH, i );

        return cal.getTimeInMillis();
    }

    public static long addMonth( long millisSinceEpoch ) {
        return addMonths(millisSinceEpoch, 1);
    }

    public static long addMonths( long millisSinceEpoch, int i ) {
        Calendar cal = toJDKCalendar(millisSinceEpoch);
        cal.add( Calendar.MONTH, i );

        return cal.getTimeInMillis();
    }


    public static long addYear( long millisSinceEpoch ) {
        return addYears(millisSinceEpoch, 1);
    }

    public static long addYears( long millisSinceEpoch, int i ) {
        Calendar cal = toJDKCalendar(millisSinceEpoch);
        cal.add( Calendar.YEAR, i );

        return cal.getTimeInMillis();
    }

    public static long addHour( long millisSinceEpoch ) {
        return addHours(millisSinceEpoch, 1);
    }

    public static long addHours( long millisSinceEpoch, int i ) {
        Calendar cal = toJDKCalendar(millisSinceEpoch);
        cal.add( Calendar.HOUR_OF_DAY, i );

        return cal.getTimeInMillis();
    }

    public static long setHour( long millisSinceEpoch, int i ) {
        Calendar cal = toJDKCalendar(millisSinceEpoch);
        cal.set(Calendar.HOUR_OF_DAY, i);

        return cal.getTimeInMillis();
    }

    public static long setDay( long millisSinceEpoch, int i ) {
        Calendar cal = toJDKCalendar(millisSinceEpoch);
        cal.set(Calendar.DAY_OF_MONTH, i);

        return cal.getTimeInMillis();
    }

    public static long subtractYear( long millisSinceEpoch ) {
        return subtractYears(millisSinceEpoch, 1);
    }

    public static long subtractYears( long millisSinceEpoch, int i ) {
        Calendar cal = toJDKCalendar(millisSinceEpoch);
        cal.add(Calendar.YEAR, -1 * i);

        return cal.getTimeInMillis();
    }

    public static long subtractMonth( long millisSinceEpoch ) {
        return subtractMonths(millisSinceEpoch, 1);
    }

    public static long subtractMonths( long millisSinceEpoch, int i ) {
        Calendar cal = toJDKCalendar(millisSinceEpoch);
        cal.add(Calendar.MONTH, -1 * i);

        return cal.getTimeInMillis();
    }

    public static long subtractDay( long millisSinceEpoch ) {
        return subtractDays(millisSinceEpoch, 1);
    }

    public static long subtractDays( long millisSinceEpoch, int i ) {
        Calendar cal = toJDKCalendar(millisSinceEpoch);
        cal.add(Calendar.DAY_OF_MONTH, -1 * i);

        return cal.getTimeInMillis();
    }

    public static long subtractHour( long millisSinceEpoch ) {
        return subtractHours(millisSinceEpoch, 1);
    }

    public static long subtractHours( long millisSinceEpoch, int i ) {
        Calendar cal = toJDKCalendar(millisSinceEpoch);
        cal.add(Calendar.HOUR_OF_DAY, -1 * i);

        return cal.getTimeInMillis();
    }

    public static long addMinute( long millisSinceEpoch ) {
        return addMinutes(millisSinceEpoch, 1);
    }

    public static long addMinutes( long millisSinceEpoch, int i ) {
        Calendar cal = toJDKCalendar(millisSinceEpoch);
        cal.add( Calendar.MINUTE, i );

        return cal.getTimeInMillis();
    }

    public static long setMinutes( long millisSinceEpoch, int i ) {
        Calendar cal = toJDKCalendar(millisSinceEpoch);
        cal.set(Calendar.MINUTE, i);

        return cal.getTimeInMillis();
    }

    public static long subtractMinute( long millisSinceEpoch ) {
        return subtractMinutes(millisSinceEpoch, 1);
    }

    public static long subtractMinutes( long millisSinceEpoch, int i ) {
        Calendar cal = toJDKCalendar(millisSinceEpoch);
        cal.add( Calendar.MINUTE, -1*i );

        return cal.getTimeInMillis();
    }

    public static long setSeconds( long millisSinceEpoch, int i ) {
        Calendar cal = toJDKCalendar(millisSinceEpoch);
        cal.set(Calendar.SECOND, i);

        return cal.getTimeInMillis();
    }

    public static long setMillis( long millisSinceEpoch, int i ) {
        Calendar cal = toJDKCalendar(millisSinceEpoch);
        cal.set(Calendar.MILLISECOND, i);

        return cal.getTimeInMillis();
    }

    public static long setTime( long millisSinceEpoch, int h, int m, int s ) {
        return setTime( millisSinceEpoch, h, m, s, 0 );
    }

    public static long setTime( long millisSinceEpoch, int h, int m, int s, int millis ) {
        Calendar cal = toJDKCalendar(millisSinceEpoch);
        cal.set(Calendar.HOUR_OF_DAY, h);
        cal.set(Calendar.MINUTE, m);
        cal.set(Calendar.SECOND, s);
        cal.set(Calendar.MILLISECOND, millis);

        return cal.getTimeInMillis();
    }

    public static long rollForwardToHMS( long millisSinceEpoch, int targetHour, int targetMinute, int targetSecond, TZ timeZone ) {
        millisSinceEpoch = DTMUtils.rollForwardToNextSpecifiedSecond(millisSinceEpoch, targetSecond);
        millisSinceEpoch = DTMUtils.rollForwardToNextSpecifiedMinute(millisSinceEpoch, targetMinute);
        millisSinceEpoch = DTMUtils.rollForwardToNextSpecifiedHour(millisSinceEpoch, targetHour, timeZone);

        return millisSinceEpoch;
    }

    public static long rollForwardToNextSpecifiedSecond( long millisSinceEpoch, int targetSecond ) {
        Calendar cal = toJDKCalendar(millisSinceEpoch);

        int currentSeconds = cal.get(Calendar.SECOND);
        if ( currentSeconds == targetSecond ) {
            return millisSinceEpoch;
        }

        int gap;
        if ( currentSeconds < targetSecond ) {
            gap = targetSecond - currentSeconds;
        } else {
            gap = targetSecond + 60 - currentSeconds;
        }

        cal.add(Calendar.SECOND, gap);

        return cal.getTimeInMillis();
    }

    public static long rollForwardToNextSpecifiedMinute( long millisSinceEpoch, int targetMinute ) {
        Calendar cal = toJDKCalendar(millisSinceEpoch);

        int currentMinutes = cal.get(Calendar.MINUTE);
        if ( currentMinutes == targetMinute ) {
            return millisSinceEpoch;
        }

        int gap;
        if ( currentMinutes < targetMinute ) {
            gap = targetMinute - currentMinutes;
        } else {
            gap = targetMinute + 60 - currentMinutes;
        }

        cal.add(Calendar.MINUTE, gap);

        return cal.getTimeInMillis();
    }

    public static long rollForwardToNextSpecifiedDay( long millisSinceEpoch, int targetDay ) {
        return rollForwardToNextSpecifiedDay(millisSinceEpoch, targetDay, TZ.UTC);
    }

    public static long rollForwardToNextSpecifiedDay( long millisSinceEpoch, int targetDay, TZ timeZone ) {
        Calendar cal = toJDKCalendar(millisSinceEpoch, timeZone);

        int currentDayOfMonth = cal.get(Calendar.DAY_OF_MONTH);
        if ( currentDayOfMonth == targetDay ) {
            return millisSinceEpoch;
        }

        int gap;
        if ( currentDayOfMonth < targetDay ) {
            gap = targetDay - currentDayOfMonth;

            cal.add(Calendar.DAY_OF_MONTH, gap);
        } else {
            gap = targetDay + 28 - currentDayOfMonth;
            cal.add(Calendar.DAY_OF_MONTH, gap);

            while ( cal.get(Calendar.DAY_OF_MONTH) != targetDay ) {
                cal.add( Calendar.DAY_OF_MONTH, 1 );
            }
        }

        return cal.getTimeInMillis();
    }

    public static long rollForwardToNextSpecifiedHour( long millisSinceEpoch, int targetHour ) {
        return rollForwardToNextSpecifiedHour(millisSinceEpoch, targetHour, TZ.UTC);
    }

    public static long rollForwardToNextSpecifiedHour( long millisSinceEpoch, int targetHour, TZ timeZone ) {
        Calendar cal = toJDKCalendar(millisSinceEpoch, timeZone);

        int currentHourOfDay = cal.get(Calendar.HOUR_OF_DAY);
        if ( currentHourOfDay == targetHour ) {
            return millisSinceEpoch;
        }

        int gap;
        if ( currentHourOfDay < targetHour ) {
            gap = targetHour - currentHourOfDay;
        } else {
            gap = targetHour + 24 - currentHourOfDay;
        }

        cal.add(Calendar.HOUR_OF_DAY, gap);

        return cal.getTimeInMillis();
    }

    public static long rollForwardDaysWhile( long millisSinceEpoch, LongPredicate predicate ) {
        while ( predicate.test(millisSinceEpoch) ) {
            millisSinceEpoch = DTMUtils.addDay(millisSinceEpoch);
        }

        return millisSinceEpoch;
    }

    public static boolean isWeekend( long when ) {
        Calendar cal = toJDKCalendar(when);

        int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);

        return dayOfWeek == Calendar.SATURDAY || dayOfWeek == Calendar.SUNDAY;
    }

    public static long toDay( long when ) {
        Calendar cal = toJDKCalendar(when);

        cal.set( Calendar.HOUR_OF_DAY, 0 );
        cal.set( Calendar.MINUTE, 0 );
        cal.set( Calendar.SECOND, 0 );
        cal.set( Calendar.MILLISECOND, 0 );

        return cal.getTimeInMillis();
    }

    /**
     * Returns true if a and b are times on the same calendar day.
     */
    public static boolean isSameDay( long a, long b, TZ tz ) {
        Calendar calA = toJDKCalendar(a, tz);
        Calendar calB = toJDKCalendar(b, tz);

        return calA.get(Calendar.DAY_OF_MONTH) == calB.get(Calendar.DAY_OF_MONTH)
            && calA.get(Calendar.MONTH) == calB.get(Calendar.MONTH)
            && calA.get(Calendar.YEAR) == calB.get(Calendar.YEAR);
    }


    public static SimpleDateFormat createFormatter( String format, TZ tz ) {
        SimpleDateFormat formatter = new SimpleDateFormat( format );
        formatter.setTimeZone( tz.toJDKTimeZone() );

        return formatter;
    }

}
