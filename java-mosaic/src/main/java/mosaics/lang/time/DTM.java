package mosaics.lang.time;

import lombok.AllArgsConstructor;
import lombok.Value;
import mosaics.lang.ComparableMixin;
import mosaics.lang.functions.ObjectFactory;
import mosaics.strings.codecs.DTMCodecLong;
import mosaics.utils.ComparatorUtils;

import java.time.Instant;
import java.time.temporal.TemporalAccessor;
import java.util.Calendar;
import java.util.Date;
import java.util.function.Predicate;


@Value
@AllArgsConstructor
public final class DTM implements ComparableMixin<DTM> {
    public static final DTM EPOCH = new DTM( 0L );

    public static DTM rnd( ObjectFactory factory ) {
        return new DTM(2020, factory.rndInt(1,13), factory.rndInt(1,28), factory.rndInt(0,24), factory.rndInt(0,60), factory.rndInt(0,60), factory.rndInt(0,1000), factory.rndOneOf(TZ.UTC,TZ.LONDON,TZ.NEW_YORK));
    }

    private final long millisSinceEpoch;

    public DTM( String str ) {
        this( DTMCodecLong.DEFAULT_CODEC.decode(str).getResult() );
    }

    public DTM( int year, int month, int day ) {
        this( year, month, day, 0, 0, 0, 0, TZ.UTC );
    }

    public DTM( int year, int month, int day, TZ tz ) {
        this( year, month, day, 0, 0, 0, 0, tz );
    }

    public DTM( int year, int month, int day, int hour, int minute ) {
        this( year, month, day, hour, minute, 0 );
    }

    public DTM( int year, int month, int day, int hour, int minute, int seconds ) {
        this( year, month, day, hour, minute, seconds, 0, TZ.UTC );
    }

    public DTM( int year, int month, int day, int hour, int minute, int seconds, int millisSinceEpoch ) {
        this( year, month, day, hour, minute, seconds, millisSinceEpoch, TZ.UTC );
    }

    public DTM( int year, int month, int day, int hour, int minute, int seconds, int millisSinceEpoch, TZ tz ) {
        this( DTMUtils.toEpoch(year, month, day, hour, minute, seconds, millisSinceEpoch, tz) );
    }

    public DTM( Date jdkDate ) {
        this( jdkDate.getTime() );
    }

    public DTM( Calendar cal ) {
        this( cal.getTimeInMillis() );
    }

    public DTM( TemporalAccessor temporalAccessor ) {
        this( Instant.from(temporalAccessor).toEpochMilli() );
    }


    public Calendar toJDKCalendar() {
        return toJDKCalendar(TZ.UTC);
    }

    public Calendar toJDKCalendar( TZ timeZone ) {
        return DTMUtils.toJDKCalendar( millisSinceEpoch, timeZone );
    }

    public Date toJDKDate() {
        return new Date(millisSinceEpoch);
    }

    public int getYear() {
        return toJDKCalendar().get(Calendar.YEAR);
    }

    public int getMonth() {
        return toJDKCalendar().get( Calendar.MONTH ) + 1;
    }

    public DayOfWeek getDayOfWeek() {
        int jdkDOW = toJDKCalendar().get( Calendar.DAY_OF_WEEK );
        return DayOfWeek.ofCalendarDayOfWeek( jdkDOW );
    }

    public int getDayOfMonth() {
        return toJDKCalendar().get( Calendar.DAY_OF_MONTH );
    }

    public int getDayOfYear() {
        return toJDKCalendar().get( Calendar.DAY_OF_YEAR );
    }

    public int getHour() {
        return toJDKCalendar().get(Calendar.HOUR_OF_DAY);
    }


    public DTMInterval toInterval( DTM other ) {
        long thisEpoch  = this.getMillisSinceEpoch();
        long otherEpoch = other.getMillisSinceEpoch();

        return DTMInterval.milliseconds( Math.abs(thisEpoch - otherEpoch) );
    }

    public int getMinutes() {
        return toJDKCalendar().get(Calendar.MINUTE);
    }

    public int getSeconds() {
        return toJDKCalendar().get(Calendar.SECOND);
    }

    public int getMillis() {
        return toJDKCalendar().get(Calendar.MILLISECOND);
    }

    public DTM addWeek() {
        return addWeeks(1);
    }

    public DTM addWeeks( int i ) {
        return applyCalendarUpdate( Calendar.DAY_OF_MONTH, i*7 );
    }

    public DTM addDay() {
        return addDays(1);
    }

    public DTM addDays( int i ) {
        return applyCalendarUpdate( Calendar.DAY_OF_MONTH, i );
    }

    public DTM addMonth() {
        return addMonths(1);
    }

    public DTM addMonths( int i ) {
        return applyCalendarUpdate( Calendar.MONTH, i );
    }

    public DTM addYear() {
        return addYears(1);
    }

    public DTM addYears( int i ) {
        return applyCalendarUpdate( Calendar.YEAR, i );
    }

    public DTM addHour() {
        return addHours(1);
    }

    public DTM addHours( int i ) {
        return applyCalendarUpdate( Calendar.HOUR_OF_DAY, i );
    }

    public DTM setHour( int i ) {
        return setCalendarUpdate( Calendar.HOUR_OF_DAY, i );
    }

    public DTM setDayOfWeek( DayOfWeek newDayOfWeek ) {
        // NB used to use setCalendarUpdate( Calendar.DAY_OF_WEEK, newDayOfWeek.toJdkCalendarEnum() );
        // changed because the direction of the move (eg given sunday, would the jdk calendar
        // roll to the next sunday or the previous sunday) was ambiguous and has been known to change.

        DayOfWeek currentDayOfWeek = getDayOfWeek();

        if ( newDayOfWeek.isGT(currentDayOfWeek) ) {
            return addDays( newDayOfWeek.ordinal() - currentDayOfWeek.ordinal() );
        } else {
            return subtractDays( currentDayOfWeek.ordinal() - newDayOfWeek.ordinal() );
        }
    }

    public DTM setDayOfMonth( int i ) {
        return setCalendarUpdate( Calendar.DAY_OF_MONTH, i );
    }

    public DTM setDayOfYear( int i ) {
        return setCalendarUpdate( Calendar.DAY_OF_YEAR, i );
    }

    public DTM subtractYear() {
        return subtractYears(1);
    }

    public DTM subtractYears( int i ) {
        return applyCalendarUpdate( Calendar.YEAR, i*-1 );
    }

    public DTM subtractMonth() {
        return subtractMonths(1);
    }

    public DTM subtractMonths( int i ) {
        return applyCalendarUpdate( Calendar.MONTH, i*-1 );
    }

    public DTM subtractWeek() {
        return subtractWeeks(1);
    }

    public DTM subtractWeeks( int i ) {
        return applyCalendarUpdate( Calendar.DAY_OF_MONTH, i*-7 );
    }

    public DTM subtractDay() {
        return subtractDays(1);
    }

    public DTM subtractDays( int i ) {
        return applyCalendarUpdate( Calendar.DAY_OF_MONTH, i*-1 );
    }

    public DTM subtractHour() {
        return subtractHours(1);
    }

    public DTM subtractHours( int i ) {
        return applyCalendarUpdate( Calendar.HOUR_OF_DAY, i*-1 );
    }

    public DTM addMinute() {
        return addMinutes( 1);
    }

    public DTM addMinutes( int i ) {
        return applyCalendarUpdate( Calendar.MINUTE, i );
    }

    public DTM setMinutes( int i ) {
        return setCalendarUpdate( Calendar.MINUTE, i );
    }

    public DTM subtractMinute() {
        return subtractMinutes( 1);
    }

    public DTM subtractMinutes( int i ) {
        return applyCalendarUpdate( Calendar.MINUTE, i*-1 );
    }

    public DTM addSecond() {
        return addSeconds( 1 );
    }

    public DTM addSeconds( int i ) {
        return applyCalendarUpdate( Calendar.SECOND, i );
    }
    
    public DTM subtractSecond() {
        return subtractSeconds( 1);
    }

    public DTM subtractSeconds( int i ) {
        return applyCalendarUpdate( Calendar.SECOND, i*-1 );
    }

    public DTM setSeconds( int i ) {
        return setCalendarUpdate( Calendar.SECOND, i );
    }

    public DTM addMillis() {
        return addMillis( 1 );
    }

    public DTM addMillis( int i ) {
        return applyCalendarUpdate( Calendar.MILLISECOND, i );
    }
    
    public DTM subtractMillis() {
        return subtractMillis( 1);
    }

    public DTM subtractMillis( int i ) {
        return applyCalendarUpdate( Calendar.MILLISECOND, i*-1 );
    }

    public DTMInterval subtractDTM( DTM dtm ) {
        return DTMInterval.milliseconds( this.millisSinceEpoch - dtm.millisSinceEpoch );
    }

    public DTM setMillis( int i ) {
        return setCalendarUpdate( Calendar.MILLISECOND, i );
    }

    public DTM setMillisSinceEpoch( int i ) {
        Calendar cal = toJDKCalendar();
        cal.set(Calendar.MILLISECOND, i);

        return cal.getTimeInMillis() == millisSinceEpoch ? this : new DTM(cal);
    }

    public DTM setTime( int h, int m, int s ) {
        return setTime( h, m, s, 0 );
    }

    public DTM setTime( int h, int m, int s, int millis ) {
        Calendar cal = toJDKCalendar();
        cal.set(Calendar.HOUR_OF_DAY, h);
        cal.set(Calendar.MINUTE, m);
        cal.set(Calendar.SECOND, s);
        cal.set(Calendar.MILLISECOND, millis);

        return cal.getTimeInMillis() == millis ? this : new DTM(cal);
    }

    public DTM addInterval( DTMInterval interval ) {
        return interval.incDTM( this );
    }

    public DTM subtractInterval( DTMInterval interval ) {
        return interval.decDTM( this );
    }

    public DTM rollForwardToHMS( int targetHour, int targetMinute, int targetSecond, TZ timeZone ) {
        Calendar cal = CalendarUtils.rollForwardToNextSpecifiedSecond( toJDKCalendar(), targetSecond);
        cal = CalendarUtils.rollForwardToNextSpecifiedMinute( cal, targetMinute);
        cal = CalendarUtils.rollForwardToNextSpecifiedHour( cal, targetHour, timeZone);

        return cal.getTimeInMillis() == millisSinceEpoch ? this : new DTM(cal);
    }

    public DTM rollForwardToNextSpecifiedSecond( int targetSecond ) {
        Calendar cal = toJDKCalendar();

        CalendarUtils.rollForwardToNextSpecifiedSecond( cal, targetSecond );

        return cal.getTimeInMillis() == millisSinceEpoch ? this : new DTM(cal);
    }

    public DTM rollForwardToNextSpecifiedMinute( int targetMinute ) {
        Calendar cal = toJDKCalendar();

        CalendarUtils.rollForwardToNextSpecifiedMinute( cal, targetMinute );

        return cal.getTimeInMillis() == millisSinceEpoch ? this : new DTM(cal);
    }

    public DTM rollForwardToNextSpecifiedDay( int targetDay ) {
        Calendar cal = toJDKCalendar();

        CalendarUtils.rollForwardToNextSpecifiedDay( cal, targetDay );

        return cal.getTimeInMillis() == millisSinceEpoch ? this : new DTM(cal);
    }

    public DTM rollForwardToNextSpecifiedHour( int targetHour ) {
        return rollForwardToNextSpecifiedHour( targetHour, TZ.UTC);
    }

    public DTM rollForwardToNextSpecifiedHour( int targetHour, TZ timeZone ) {
        Calendar cal = toJDKCalendar( timeZone);

        CalendarUtils.rollForwardToNextSpecifiedHour( cal, targetHour );

        return cal.getTimeInMillis() == millisSinceEpoch ? this : new DTM(cal);
    }

    public DTM rollForwardDaysWhile( Predicate<DTM> predicate ) {
        DTM dtm = this;

        while ( predicate.test(dtm) ) {
             dtm = dtm.addDay();
        }

        return dtm;
    }

    public boolean isWeekend() {
        return DTMUtils.isWeekend( millisSinceEpoch );
    }

    public Day toDay() {
        return new Day( millisSinceEpoch );
    }

    /**
     * Returns true if a and b are times on the same calendar day.
     */
    public boolean isSameDay( DTM other ) {
        Calendar calA = this.toJDKCalendar();
        Calendar calB = other.toJDKCalendar();

        return calA.get(Calendar.DAY_OF_MONTH) == calB.get(Calendar.DAY_OF_MONTH)
            && calA.get(Calendar.MONTH) == calB.get(Calendar.MONTH)
            && calA.get(Calendar.YEAR) == calB.get(Calendar.YEAR);
    }


    public String toString() {
        return DTMCodecLong.DEFAULT_CODEC.encode( millisSinceEpoch );
    }

    public int compareTo( DTM o ) {
        return ComparatorUtils.compareAsc( this.millisSinceEpoch, o.millisSinceEpoch );
    }


    private DTM applyCalendarUpdate( int field, int delta ) {
        if ( delta == 0 ) {
            return this;
        }

        Calendar cal = toJDKCalendar();
        cal.add( field, delta );

        return new DTM(cal);
    }

    private DTM setCalendarUpdate( int field, int i ) {
        Calendar cal = toJDKCalendar();
        cal.set(field, i);

        return cal.getTimeInMillis() == this.millisSinceEpoch ? this : new DTM(cal);
    }
}
