package mosaics.lang;

import mosaics.lang.functions.BooleanFunction1;
import mosaics.lang.functions.Function0;
import mosaics.lang.functions.Function1;
import mosaics.lang.functions.VoidFunction1;
import mosaics.lang.functions.BooleanFunction1;
import mosaics.lang.functions.Function0;
import mosaics.lang.functions.Function1;
import mosaics.lang.functions.VoidFunction1;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;


@SuppressWarnings( "unchecked" )
public class ArrayUtils {

    /**
     * Create a new array with each element filled with a new instance of T created by the factory.
     */
    public static <T> T[] fill( Class<T> elementType, int arrayLength, Function0<T> elementFactory ) {
        T[] array = (T[]) Array.newInstance(elementType, arrayLength);

        for ( int i=0; i<arrayLength; i++ ) {
            array[i] = elementFactory.invoke();
        }

        return array;
    }

    public static <T> void forEach( T[] array, VoidFunction1<T> action ) {
        int arrayLength = array.length;

        for ( int i=0; i<arrayLength; i++ ) {
            action.invoke( array[i] );
        }
    }

    public static <T> void forEach( Object array, VoidFunction1<T> action ) {
        int arrayLength = Array.getLength( array );

        for ( int i=0; i<arrayLength; i++ ) {
            action.invoke( (T) Array.get(array,i) );
        }
    }

    /**
     * Creates a new array containing the result of calling mapFunction on each element of the original array.
     */
    public static <A,B> Object[] map( A[] array, Function1<A,B> mapFunction ) {
        int      arrayLength = array.length;
        Object[] newArray    = new Object[arrayLength];

        for ( int i=0; i<arrayLength; i++ ) {
            newArray[i] = mapFunction.invoke( array[i] );
        }

        return newArray;
    }

    /**
     * Creates a new array containing the result of calling mapFunction on each element of the original array.
     */
    public static <A,B> B[] map( Class<B> type, A[] array, Function1<A,B> mapFunction ) {
        int arrayLength = array.length;
        B[] newArray    = (B[]) Array.newInstance( type, arrayLength );

        for ( int i=0; i<arrayLength; i++ ) {
            newArray[i] = mapFunction.invoke( array[i] );
        }

        return newArray;
    }

    public static <T> T[] filter( Class<T> type, T[] array, Predicate<T> predicate ) {
        List<T> matches = new ArrayList<>();

        for ( T v : array ) {
            if ( predicate.test(v) ) {
                matches.add( v );
            }
        }

        if ( matches.size() == array.length ) {
            return array;
        }

        return matches.toArray( ArrayUtils.newArray(type, matches.size()) );
    }

    /**
     * Replace each element of the array with the result of calling mapFunction with the previous element value.
     */
    public static <T> T[] mapInline( T[] array, Function1<T,T> mapFunction ) {
        int arrayLength = array.length;

        for ( int i=0; i<arrayLength; i++ ) {
            array[i] = mapFunction.invoke( array[i] );
        }

        return array;
    }

    public static String toString( Object[] array, String seperator ) {
        StringBuilder buf = new StringBuilder(100);

        toString( buf, array, seperator );

        return buf.toString();
    }

    public static String toString( int[] array, String seperator ) {
        StringBuilder buf = new StringBuilder(100);

        toString( buf, array, seperator );

        return buf.toString();
    }

    public static void toString( StringBuilder buf, Object[] array, String seperator ) {
        for ( int i=0; i<array.length; i++ ) {
            if ( i > 0 ) {
                buf.append( seperator );
            }

            buf.append( array[i] );
        }
    }

    public static void toString( StringBuilder buf, int[] array, String seperator ) {
        for ( int i=0; i<array.length; i++ ) {
            if ( i > 0 ) {
                buf.append( seperator );
            }

            buf.append( array[i] );
        }
    }

    public static Object[] newArray( int len, Function0 factory ) {
        Object[] array = new Object[len];

        for ( int i=0; i<len; i++ ) {
            array[i] = factory.invoke();
        }

        return array;
    }

    public static <T> T[] newArray( Class<T> elementType, int length ) {
        return (T[]) Array.newInstance( elementType, length );
    }

    public static <T> T[] flatten( T[]...arrays ) {
        int totalLength = sumLengths(arrays);
        T[] result      = newArray( (Class<T>) arrays[0].getClass().getComponentType(), totalLength );

        int i = 0;
        for ( T[] a : arrays ) {
            for ( int j=0; j<a.length; j++ ) {
                result[i++] = a[j];
            }
        }

        return result;
    }

    private static <T>int sumLengths( T[][] arrays ) {
        int l = 0;

        for ( T[] a : arrays ) {
            l += a.length;
        }

        return l;
    }

    public static Object[] resize( Object[] prev, int newSize ) {
        Object[] newArray = newArray(prev.getClass().getComponentType(), newSize);

        System.arraycopy( prev, 0, newArray, 0, Math.min(newSize,prev.length) );

        return newArray;
    }

    public static List<Long> toList( long[] a ) {
        List<Long> list = new ArrayList<>(a.length);

        for ( long v : a ) {
            list.add(v);
        }

        return list;
    }

    public static <T> boolean ifEvery( T[] array, BooleanFunction1<T> predicate ) {
        if ( array == null ) {
            return true;
        }

        for ( T v : array ) {
            if ( !predicate.invoke(v) ) {
                return false;
            }
        }

        return true;
    }

}
