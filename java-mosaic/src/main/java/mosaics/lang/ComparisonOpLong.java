package mosaics.lang;

import mosaics.lang.functions.BooleanFunctionLL;
import mosaics.lang.functions.BooleanFunctionLL;


public enum ComparisonOpLong implements BooleanFunctionLL {
    GT( (a,b) -> a > b ),
    GTE( (a,b) -> a >= b ),
    LT( (a,b) -> a < b ),
    LTE( (a,b) -> a <= b ),
    EQ( (a,b) -> a == b );

    private BooleanFunctionLL comparisonOp;

    ComparisonOpLong( BooleanFunctionLL comparisonOp ) {
        this.comparisonOp = comparisonOp;
    }

    public boolean invoke( long a, long b ) {
        return comparisonOp.invoke( a, b );
    }
}
