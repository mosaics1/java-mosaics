package mosaics.lang.ref;

import lombok.Getter;

import java.lang.ref.ReferenceQueue;


public class SoftReference<M,V> extends java.lang.ref.SoftReference<V> implements HasMetaData<M> {
    @Getter
    private M metaData;

    public SoftReference( M metaData, V referent, ReferenceQueue<V> q ) {
        super( referent, q );

        this.metaData = metaData;
    }
}
