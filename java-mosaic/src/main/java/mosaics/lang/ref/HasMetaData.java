package mosaics.lang.ref;

public interface HasMetaData<M> {
    public M getMetaData();
}
