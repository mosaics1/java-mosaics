package mosaics.lang;

import lombok.SneakyThrows;
import mosaics.fp.Try;
import mosaics.fp.Tryable;
import mosaics.lang.functions.Function0;
import mosaics.lang.functions.Function0WithThrows;
import sun.misc.Unsafe;

import java.lang.reflect.Field;
import java.nio.charset.Charset;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;


/**
 * A utility wrapper around sun.misc.Unsafe.  Until such time that Unsafe
 * becomes standardised; Java 9?
 */
@SuppressWarnings({"unused", "SynchronizationOnLocalVariableOrMethodParameter", "sunapi"})
public class Backdoor {

    public static final Charset UTF8    = Charset.forName( "UTF-8" );
    public static final String  NEWLINE = System.getProperty("line.separator");
    public static final String  FILE_SEPARATOR = System.getProperty("file.separator");

    public static final int KILOBYTE              = 1024;
    public static final int MEGABYTE              = KILOBYTE*1024;
    public static final int GIGABYTE              = MEGABYTE*1024;

    public static final int SIZEOF_BOOLEAN        = 1;
    public static final int SIZEOF_BYTE           = 1;
    public static final int SIZEOF_CHAR           = 2;
    public static final int SIZEOF_SHORT          = 2;
    public static final int SIZEOF_INT            = 4;
    public static final int SIZEOF_LONG           = 8;
    public static final int SIZEOF_FLOAT          = 4;
    public static final int SIZEOF_DOUBLE         = 8;

    public static final int SIZEOF_UNSIGNED_BYTE  = 1;
    public static final int SIZEOF_UNSIGNED_SHORT = 2;
    public static final int SIZEOF_UNSIGNED_INT   = 4;


    public static final int  UNSIGNED_BYTE_MASK  = 0xFF;
    public static final int  UNSIGNED_SHORT_MASK = 0xFFFF;
    public static final long UNSIGNED_INT_MASK   = 0xFFFFFFFFL;

    public static final short MAX_UNSIGNED_BYTE  = (((short) Byte.MAX_VALUE) << 1)+1;
    public static final int   MAX_UNSIGNED_SHORT = (((int) Short.MAX_VALUE) << 1)+1;
    public static final long  MAX_UNSIGNED_INT   = (((long)Integer.MAX_VALUE) << 1)+1;
    public static final byte  NULL_BYTE = 0;

    public static final long NUMBER_OF_NANOSECONDS_PER_MILLISECOND = 1_000_000;

    public static final short UNSIGNED_BYTE_MAX = (short) 0xFF;
    public static final short UNSIGNED_BYTE_MIN = (short) 0;

    public static final int UNSIGNED_SHORT_MAX = 0xFFFF;
    public static final int UNSIGNED_SHORT_MIN = 0;

    public static final long UNSIGNED_INT_MAX = 0xFFFFFFFFL;
    public static final long UNSIGNED_INT_MIN = 0;


    public static final float  FLOAT_PRECISION  = 0.000001f;
    public static final double DOUBLE_PRECISION = 0.00000000000001f;

//module: jdk.unsupported
    private static final Unsafe     unsafe        = fetchUnsafe();
    private static final AtomicLong mallocCounter = new AtomicLong(0);
    private static final Random     RND           = new Random();



    private static Unsafe fetchUnsafe() {
        try {
            Field field = Unsafe.class.getDeclaredField( "theUnsafe" );

            field.setAccessible(true);

            return (Unsafe) field.get(null);
        } catch ( Throwable e ) {
            throw new RuntimeException(e);
        }
    }

// HARDWARE

    // The latest Intel processors have 3 layers (L1D, L2, and L3); with
    // sizes 32KB, 256KB, and 4-30MB; and ~1ns, ~4ns, and ~15ns latency respectively for a 3.0GHz CPU.
    // => 512, 4096, >131k cache lines for each layer

    public static int getCacheLineLengthBytes() {
        return 64;  // todo detect or configure this
    }

    public static int getCoreCount() {
        return Math.max( 1, Runtime.getRuntime().availableProcessors()/2 );  // assumes hyper threading is turned on
    }

    /**
     * Moves the suppress warning when casting without compiler support to one place.  Such casting
     * is a smell and should be avoided.  However there are times, especially when working with
     * legacy apis that this kind of casting is necessary.  For example, Map.get(Object) does not
     * support generics.
     */
    @SuppressWarnings("unchecked")
    public static <K> K cast( Object key ) {
        return (K) key;
    }

    /**
     * A wrapper around synchronized.  WTF would we want this?  It is a work around for code coverage
     * tools that fail to report that the code within the synchronized block is fully covered.
     * Ugly, annoying and blah.
     *
     * @see https://github.com/jacoco/jacoco/wiki/filtering-JAVAC.SYNC
     */
    public static <R> R invokeSynchronized( Object lock, Function0<R> f ) {
        synchronized (lock) {
            return f.invoke();
        }
    }

    /**
     * A wrapper around synchronized.  WTF would we want this?  It is a work around for code coverage
     * tools that fail to report that the code within the synchronized block is fully covered.
     * Ugly, annoying and blah.   Given this ugliness has already been accepted, this version of the
     * method wraps in try for good measure.
     *
     * @see https://github.com/jacoco/jacoco/wiki/filtering-JAVAC.SYNC
     */
    public static <R> Tryable<R> invokeSynchronizedTry( Object lock, Function0WithThrows<R> f ) {
        synchronized (lock) {
            return Try.invoke(f);
        }
    }

    /**
     * Returns a random long.
     *
     * @return between -MIN and +MAX
     */
    public static long nextRandomLong() {
        return RND.nextLong();
    }


    /**
     * Returns a random long between 0 and max.
     */
    public static long nextRandomLong( long max ) {
        return Math.abs(RND.nextLong()) % (max+1);
    }


    public static long alloc( long numBytes ) {
        QA.argIsGTZero(numBytes, "numBytes");

        long ptr = unsafe.allocateMemory( numBytes );

        mallocCounter.incrementAndGet();

        return ptr;
    }

    public static void free( long address ) {
        unsafe.freeMemory(address);

        mallocCounter.decrementAndGet();
    }

    /**
     * Ensures lack of reordering of stores before the fence with loads or stores after the fence.
     *
     * On x86 this means: next instruction will execute after all SOBs are processed
     *
     * http://stackoverflow.com/questions/23603304/java-8-unsafe-xxxfence-instructions
     */
    public static void storeFence() {
        unsafe.storeFence();
    }

    /**
     * Ensures lack of reordering of loads before the fence with loads or stores after the fence.
     *
     * NB The x86 load queue stores invalidation requests, not actual loads.
     *
     * http://stackoverflow.com/questions/23603304/java-8-unsafe-xxxfence-instructions
     */

    public static void loadFence() {
        unsafe.loadFence();
    }

    /**
     * Applies both a load and a store fence.
     *
     * http://stackoverflow.com/questions/23603304/java-8-unsafe-xxxfence-instructions
     */
    public static void fullFence() {
        unsafe.fullFence();
    }

    /**
     * Returns a count of how many more calls to allocOffHeap() than free().  The counter
     * is incremented when allocOffHeap() is called, and decremented when free() is
     * called.
     */
    public static long getActiveAllocCounter() {
        return mallocCounter.get();
    }

    private static final RuntimeException DUMMY_EX = new RuntimeException();
    public static RuntimeException throwException( Throwable ex ) {
        QA.argNotNull(ex, "ex");

        unsafe.throwException( ex );

        // NB returns null so that callers can write 'return Backdoor.throwException(ex)'
        // all to avoid compiler errors, bah.
        // NBB replaced null with a dummy instance because IDEA became clever enough to spot and
        // warn about it
        return DUMMY_EX;
    }

    public static <T> T invokeAndRethrowException( Function0WithThrows<T> f ) {
        try {
            return f.invoke();
        } catch ( Throwable ex ) {
            throw throwException( ex );
        }
    }

    @SuppressWarnings("ThrowableNotThrown")
    public static void sleep( long millis ) {
        QA.argIsGTEZero(millis, "millis");

        try {
            Thread.sleep( millis );
        } catch ( InterruptedException ex ) {
            throwException( ex );
        }
    }


    public static short toUnsignedByte( byte b ) {
        return (short) (b & UNSIGNED_BYTE_MASK);
    }

    public static int toUnsignedShort( short b ) {
        return b & UNSIGNED_SHORT_MASK;
    }

    public static long toUnsignedInt( int b ) {
        return b & UNSIGNED_INT_MASK;
    }


// PRIMITIVE VALUE GETTER/SETTERS


    public boolean getBoolean( long address ) {
        return unsafe.getByte( address ) > 0;
    }

    public static byte getByte( long address ) {
        return unsafe.getByte( address );
    }

    public static short getUnsignedByte( long address ) {
        return toUnsignedByte(unsafe.getByte(address));
    }

    public static int getUnsignedShort( long address ) {
        return toUnsignedShort(unsafe.getShort(address));
    }

    public static long getUnsignedInt( long address ) {
        return toUnsignedInt(unsafe.getInt(address));
    }

    public static char getCharacter( long address ) {
        return unsafe.getChar( address );
    }

    public static short getShort( long address ) {
        return unsafe.getShort(address);
    }

    public static int getInteger( long address ) {
        return unsafe.getInt(address);
    }

    public static long getLong( long address ) {
        return unsafe.getLong(address);
    }

    public static float getFloat( long address ) {
        return unsafe.getFloat(address);
    }

    public static double getDouble( long address ) {
        return unsafe.getDouble(address);
    }

    public static void setBoolean( long address, boolean v ) {
        debugAddress( address, SIZEOF_BOOLEAN );

        unsafe.putByte( address, (byte) (v ? 1 : 0) );
    }

    public static int setByte( long address, byte v ) {
        debugAddress( address, SIZEOF_BYTE );

        unsafe.putByte( address, v );

        return 1;
    }

    public static void setCharacter( long address, char v ) {
        debugAddress( address, SIZEOF_CHAR );

        unsafe.putChar(address, v);
    }

    public static void setShort( long address, short v ) {
        debugAddress( address, SIZEOF_SHORT );

        unsafe.putShort(address, v);
    }

    public static void setInteger( long address, int v ) {
        debugAddress( address, SIZEOF_INT );

        unsafe.putInt(address, v);
    }

    public static void setLong( long address, long v ) {
        debugAddress( address, SIZEOF_LONG );

        unsafe.putLong(address, v);
    }

    public static void setFloat( long address, float v ) {
        debugAddress( address, SIZEOF_FLOAT );

        unsafe.putFloat(address, v);
    }

    public static void setDouble( long address, double v ) {
        debugAddress( address, SIZEOF_DOUBLE );

        unsafe.putDouble(address, v);
    }

    public static void copyBytes( long fromAddress, long toAddress, long numBytes ) {
        debugAddress( toAddress, numBytes );

        unsafe.copyMemory(fromAddress, toAddress, numBytes);
    }

    public static int copyBytes( byte[] fromArray, int fromInc, long toAddress, long numBytes ) {
        if ( Assert.areAssertionsEnabled() ) {
            QA.argIsBetweenInc( 0, fromInc, fromArray.length, "fromInc" );
            QA.argIsBetweenInc( 0, fromInc + numBytes, fromArray.length, "fromInc+numBytes" );

            debugAddress( toAddress, numBytes );
        }

        unsafe.copyMemory( fromArray, BYTE_ARRAY_BASE_OFFSET+fromInc, null, toAddress, numBytes );

        return toInt(numBytes);
    }

    public static int copyBytes( long fromAddress, byte[] toArray, int arrayIndex, int numBytes ) {
        if ( Assert.areAssertionsEnabled() ) {
            QA.argIsBetween(0, arrayIndex, toArray.length, "arrayIndex");
            QA.argIsBetweenInc( 0, arrayIndex + numBytes, toArray.length, "arrayIndex+numBytes" );

            debugAddress( arrayIndex, numBytes );
        }

        unsafe.copyMemory(null, fromAddress, toArray, BYTE_ARRAY_BASE_OFFSET + arrayIndex, numBytes);

        return numBytes;
    }

    public static void copyBytes( byte[] fromArray, long fromArrayIndex, byte[] toArray, long toArrayIndex, long numBytes ) {
        copyBytes(fromArray, toInt(fromArrayIndex), toArray, toInt(toArrayIndex), numBytes);
    }

    public static void copyBytes( byte[] fromArray, int fromArrayIndex, byte[] toArray, int toArrayIndex, long numBytes ) {
        if ( Assert.areAssertionsEnabled() ) {
            QA.argIsBetween( 0, fromArrayIndex, fromArray.length, "fromArrayIndex" );
            QA.argIsBetweenInc( 0, fromArrayIndex + numBytes, fromArray.length, "fromArrayIndex+numBytes" );

            QA.argIsBetween( 0, toArrayIndex, toArray.length, "toArrayIndex" );
            QA.argIsBetweenInc( 0, toArrayIndex + numBytes, toArray.length, "toArrayIndex+numBytes" );

            debugAddress( toArrayIndex, numBytes );
        }

        unsafe.copyMemory(fromArray, BYTE_ARRAY_BASE_OFFSET + fromArrayIndex, toArray, BYTE_ARRAY_BASE_OFFSET + toArrayIndex, numBytes);
    }

    public static byte[] copyBytes( byte[] source, int fromInc, int toExc ) {
        int    numBytes = toExc-fromInc;
        byte[] dest     = new byte[numBytes];

        System.arraycopy(source, fromInc, dest, 0, numBytes);

        return dest;
    }


    public static long alignAddress( long ptr, int n ) {
        // commented out code has been left in to document what the maths
        // version does below;  the maths version is faster because it avoids
        // any conditional jumps that the if block can cause
        // NB it may be worth replacing the % operators with & operators..
        // after all, we are always going to be aligning to 2^n boundaries.

//        long remainder = ptr % n;
//
//        if ( remainder == 0 ) {
//            return ptr;
//        }
//
//        long padding = n - remainder;
//        return ptr + padding;

        return ptr + ((n-(ptr % n)) % n);
    }

    public static void fill( long ptr, long numBytes, byte v ) {
        debugAddress( ptr, numBytes );

        unsafe.setMemory( ptr, numBytes, v );
    }

    public static void fill( long ptr, long numBytes, int v ) {
        fill( ptr, numBytes, (byte) v );
    }

    public static void fillArray( byte[] array, long offset, long numBytes, byte v ) {
        debugAddress( offset, numBytes );

        unsafe.setMemory( array, BYTE_ARRAY_BASE_OFFSET +offset, numBytes, v );
    }


    private static final long BYTE_ARRAY_BASE_OFFSET = unsafe.arrayBaseOffset( byte[].class );
    private static final long BYTE_ARRAY_SCALE       = unsafe.arrayIndexScale( byte[].class );


    static {
        QA.isNotZero( BYTE_ARRAY_BASE_OFFSET, "BYTE_ARRAY_BASE_OFFSET" );
    }


    @SneakyThrows
    public static <T> T allocateInstance( Class<T> type ) {
        return cast(unsafe.allocateInstance(type));
    }

    public static byte getByteFrom( byte[] array, long offset ) {
        return unsafe.getByte( array, BYTE_ARRAY_BASE_OFFSET + offset*BYTE_ARRAY_SCALE );
    }

    public static short getUnsignedByteFrom( byte[] array, long offset ) {
        return (short) (Backdoor.getByteFrom(array,offset) & UNSIGNED_BYTE_MASK);
    }

    public static short getShortFrom( byte[] array, long offset ) {
        return unsafe.getShort( array, BYTE_ARRAY_BASE_OFFSET + offset*BYTE_ARRAY_SCALE );
    }

    public static int getUnsignedShortFrom( byte[] array, long offset ) {
        return Backdoor.getShortFrom(array,offset) & UNSIGNED_SHORT_MASK;
    }

    public static char getCharacterFrom( byte[] array, long offset ) {
        return unsafe.getChar(array, BYTE_ARRAY_BASE_OFFSET + offset * BYTE_ARRAY_SCALE);
    }

    public static int getIntegerFrom( byte[] array, long offset ) {
        return unsafe.getInt( array, BYTE_ARRAY_BASE_OFFSET + offset * BYTE_ARRAY_SCALE );
    }

    public static long getUnsignedIntegerFrom( byte[] array, long offset ) {
        return Backdoor.getIntegerFrom(array,offset) & UNSIGNED_INT_MASK;
    }

    public static long getLongFrom( byte[] array, long offset ) {
        return unsafe.getLong(array, BYTE_ARRAY_BASE_OFFSET + offset * BYTE_ARRAY_SCALE);
    }

    public static float getFloatFrom( byte[] array, long offset ) {
        return unsafe.getFloat(array, BYTE_ARRAY_BASE_OFFSET + offset * BYTE_ARRAY_SCALE);
    }

    public static double getDoubleFrom( byte[] array, long offset ) {
        return unsafe.getDouble( array, BYTE_ARRAY_BASE_OFFSET + offset*BYTE_ARRAY_SCALE );
    }


    public static byte signUnsignedByte( short v ) {
        return (byte) (v & UNSIGNED_BYTE_MASK);
    }

    public static short signUnsignedShort( int v ) {
        return (short) (v & UNSIGNED_SHORT_MASK);
    }

    public static int signUnsignedInt( long v ) {
        return (int) (v & UNSIGNED_INT_MASK);
    }

    public static int setByteIn( byte[] array, long offset, byte v ) {
        debugAddress( offset, SIZEOF_BYTE );

        unsafe.putByte(array, BYTE_ARRAY_BASE_OFFSET + offset * BYTE_ARRAY_SCALE, v);

        return 1;
    }

    public static void setUnsignedByteIn( byte[] array, long offset, short v ) {
        setByteIn(array, offset, signUnsignedByte(v));
    }

    public static void setUnsignedByte( long offset, short v ) {
        setByte(offset, signUnsignedByte(v));
    }

    public static void setCharacterIn( byte[] array, long offset, char v ) {
        debugAddress( offset, SIZEOF_CHAR );

        unsafe.putChar(array, BYTE_ARRAY_BASE_OFFSET + offset * BYTE_ARRAY_SCALE, v);
    }

    public static void setShortIn( byte[] array, long offset, short v ) {
        debugAddress( offset, SIZEOF_SHORT );

        unsafe.putShort(array, BYTE_ARRAY_BASE_OFFSET + offset * BYTE_ARRAY_SCALE, v);
    }

    public static void setUnsignedShortIn( byte[] array, long offset, int v ) {
        setShortIn(array, offset, signUnsignedShort(v));
    }

    public static void setUnsignedShort( long offset, int v ) {
        setShort(offset, signUnsignedShort(v));
    }

    public static void setUnsignedInt( long offset, long v ) {
        setInteger(offset, signUnsignedInt(v));
    }

    public static void setIntegerIn( byte[] array, long offset, int v ) {
        debugAddress( offset, SIZEOF_INT );

        unsafe.putInt(array, BYTE_ARRAY_BASE_OFFSET + offset * BYTE_ARRAY_SCALE, v);
    }

    public static void setUnsignedIntegerIn( byte[] array, long offset, long v ) {
        setIntegerIn(array, offset, signUnsignedInt(v));
    }

    public static void setLongIn( byte[] array, long offset, long v ) {
        debugAddress( offset, SIZEOF_LONG );

        unsafe.putLong(array, BYTE_ARRAY_BASE_OFFSET + offset * BYTE_ARRAY_SCALE, v);
    }

    public static void setFloatIn( byte[] array, long offset, float v ) {
        debugAddress( offset, SIZEOF_FLOAT );

        unsafe.putFloat(array, BYTE_ARRAY_BASE_OFFSET + offset * BYTE_ARRAY_SCALE, v);
    }

    public static void setDoubleIn( byte[] array, long offset, double v ) {
        debugAddress( offset, SIZEOF_DOUBLE );

        unsafe.putDouble( array, BYTE_ARRAY_BASE_OFFSET + offset * BYTE_ARRAY_SCALE, v );
    }


    // NB I found setting a watch break point too slow; so to track down memory corrupts set this field instead
    public volatile static long targetAddress = 0;
    /**
     * This function is used as a place to capture writes to a memory address via a debugger.
     *
     *
     * offset <= 4681707554L && (offset+numBytes) >= 4681707554L
     */
    private static void debugAddress( long offset, long numBytes ) {
        if ( Assert.areAssertionsEnabled() ) {
            // todo also consider verifying against a registry of allocated addresses

            if ( targetAddress > 0 && offset <= targetAddress && (offset+numBytes) > targetAddress ) {
                new RuntimeException( "wrote to "+offset+" "+numBytes+" bytes (was watching "+targetAddress+")" ).printStackTrace();
            }
        }
    }

    public static long toLong( double v ) {
        return (long) v;
    }

    public static int toInt( double v ) {
        return toInt(toLong(v));
    }

    public static int toInt( long v ) {
        Assert.argIsLTE( v, Integer.MAX_VALUE, "v" );
        Assert.argIsGTE( v, Integer.MIN_VALUE, "v" );

        return (int) v;
    }

    public static short toShort( int v ) {
        Assert.argIsLTE( v, Short.MAX_VALUE, "v" );
        Assert.argIsGTE( v, Short.MIN_VALUE, "v" );

        return (short) v;
    }

    public static byte toByte( int v ) {
        Assert.argIsLTE( v, Byte.MAX_VALUE, "v" );
        Assert.argIsGTE( v, Byte.MIN_VALUE, "v" );

        return (byte) v;
    }

    public static long calculateOffsetForField( Class clazz, String fieldName ) {
        try {
            return unsafe.objectFieldOffset( clazz.getDeclaredField( fieldName ) );
        } catch ( NoSuchFieldException ex ) {
            throw throwException( ex );
        }
    }

    public static void setFloat( Object obj, long fieldOffset, float v ) {
        unsafe.putFloat( obj, fieldOffset, v );
    }

    public static float getFloat( Object obj, long fieldOffset ) {
        return unsafe.getFloat( obj, fieldOffset );
    }

    public static void setDouble( Object obj, long fieldOffset, double v ) {
        unsafe.putDouble( obj, fieldOffset, v );
    }

    public static double getDouble( Object obj, long fieldOffset ) {
        return unsafe.getDouble( obj, fieldOffset );
    }

    public static boolean compareBytes( long offset, byte[] targetBytes ) {
        for ( int i=0; i<targetBytes.length; i++ ) {
            byte actual = getByte( offset+i );

            if ( actual != targetBytes[i] ) {
                return false;
            }
        }

        return true;
    }

    public static boolean compareBytes( byte[] sourceArray, int fromIndex, byte[] targetBytes ) {
        for ( int i=0; i<targetBytes.length; i++ ) {
            byte actual = sourceArray[fromIndex+i];

            if ( actual != targetBytes[i] ) {
                return false;
            }
        }

        return true;
    }

    public static String getTempDirectory() {
        return System.getProperty( "java.io.tmpdir" );
    }

    @SuppressWarnings("InfiniteLoopStatement")
    public static void blockForever() {
        synchronized (Backdoor.class) {
            while ( true ) {
                try {
                    Backdoor.class.wait();
                } catch ( InterruptedException e ) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * This method was created because IntelliJ was failing to get the code coverage around
     * the synchronized keyword correct.  So to not pollute the stats this method was created.
     */
    public static <T> T sync( Object monitor, Function0<T> f ) {
        T result;

        synchronized (monitor) {
            result = f.invoke();
        }

        return result;
    }
}
