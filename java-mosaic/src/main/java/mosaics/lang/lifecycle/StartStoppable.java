package mosaics.lang.lifecycle;

import mosaics.fp.FP;
import mosaics.lang.Assert;
import mosaics.lang.Backdoor;
import mosaics.lang.QA;
import mosaics.lang.functions.VoidFunction0;
import mosaics.utils.ListUtils;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import static mosaics.lang.Backdoor.cast;


public abstract class StartStoppable<T extends StartStoppable> {
    private Set<StartStoppableCallback> callbacks = new LinkedHashSet<>();

    private List<StartStoppable> servicesBefore = new ArrayList<>(3);
    private List<StartStoppable> servicesAfter  = new ArrayList<>(3);

    private VoidFunction0 onStartBeforeFunc = VoidFunction0.noOp();
    private VoidFunction0 onStartAfterFunc  = VoidFunction0.noOp();
    private VoidFunction0 onStopBeforeFunc  = VoidFunction0.noOp();
    private VoidFunction0 onStopAfterFunc   = VoidFunction0.noOp();

    private AtomicInteger initCounter = new AtomicInteger(0);

    private AtomicBoolean isShuttingDown  = new AtomicBoolean(false);

    private String        serviceName;


    public StartStoppable( String serviceName ) {
        QA.argNotNull( serviceName, "serviceName" );

        this.serviceName = serviceName;
    }

    protected void doStart() throws Exception {}
    protected void doStop() throws Exception {}


    public String getName() {
        return serviceName;
    }

    public String toString() {
        return serviceName;
    }

    /**
     * Return a list of all services in the order that they will be started in when
     * StartStoppable.start() is called.  This method is used for introspection, especially
     * unit tests;
     */
    public List<Object> getStartChainOrder() {
        List<Object> chain = new ArrayList<>();

        chain.addAll(servicesBefore);
        chain.add(this);
        chain.addAll(servicesAfter);

        return chain;
    }

    public T withCallback( StartStoppableCallback callback ) {
        callbacks.add( callback );

        servicesBefore.forEach( s -> s.withCallback(callback) );
        servicesAfter.forEach( s -> s.withCallback(callback) );

        return cast(this);
    }

    public boolean removeCallback( StartStoppableCallback callback ) {
        boolean removed = callbacks.remove( callback );

        removed = FP.wrap(servicesBefore).fold( removed, (soFar,service) -> service.removeCallback(callback) || soFar );
        removed = FP.wrap(servicesAfter).fold( removed, (soFar,service) -> service.removeCallback(callback) || soFar );

        return removed;
    }

    /**
     * Start the service.  If the service is already running, then a counter is bumped and stop()
     * will have to be called an extra time.  The number of calls to start() always determines how
     * many times stop() has to be called before a shutdown will occur.
     *
     * Starting this service will trigger a start of all chained services.  Services that are
     * registered to start before this service will be triggered first.  Then this service will be
     * started and then lastly services that are registered to start after this service will be
     * triggered.  Any error from a service during startup will cause the whole chain to error
     * and will leave the chain of services in an unknown state.  Services are not designed
     * to be able to recover from exceptions duirng startup/shutdown.
     *
     * During startup callbacks will be notified.  Again callbacks are not to throw exceptions.  If
     * they do then the startup chain will abort in an unknown state.
     */
    public final T start() {
        throwIfShuttingDown();

        int initCount = initCounter.incrementAndGet();

        if ( initCount == 1 ) {
            callbacks.forEach( callback -> callback.starting(this) );
            servicesBefore.forEach( StartStoppable::start );

            try {
                onStartBeforeFunc.invoke();

                doStart();

                onStartAfterFunc.invoke();
            } catch ( Exception ex ) {
                throw Backdoor.throwException( ex );
            }

            servicesAfter.forEach( StartStoppable::start );
            callbacks.forEach( callback -> callback.started(this) );
        }

        return cast(this);
    }

    public final T stop() {
        int initCount = initCounter.decrementAndGet();

        if ( initCount == 0 ) {
            this.isShuttingDown.set(true);

            callbacks.forEach( callback -> callback.stopping(this) );
            ListUtils.forEachReversed(servicesAfter, StartStoppable::stop );

            try {
                onStopBeforeFunc.invoke();

                doStop();

                onStopAfterFunc.invoke();
            } catch ( Exception ex ) {
                throw Backdoor.throwException( ex );
            } finally {
                ListUtils.forEachReversed( servicesBefore, StartStoppable::stop );

                this.isShuttingDown.set(false);

                callbacks.forEach( callback -> callback.stopped(this) );
            }
        }

        return cast(this);
    }

    public final boolean isRunning() {
        return initCounter.get() > 0;
    }

    public final boolean isShuttingDown() {
        return isShuttingDown.get();
    }

    public Subscription registerServicesBefore( Object... newServices ) {
        return appendServices( newServices, servicesBefore );
    }


    public Subscription registerServicesAfter( Object... otherServices ) {
        throwIfRunning();

        return appendServices( otherServices, servicesAfter );
    }

    public T onStartBefore( VoidFunction0 callback ) {
        onStartBeforeFunc = onStartBeforeFunc.and( callback );

        return cast(this);
    }

    public T onStartAfter( VoidFunction0 callback ) {
        onStartAfterFunc = onStartAfterFunc.and( callback );

        return cast(this);
    }

    public T onStopBefore( VoidFunction0 callback ) {
        onStopBeforeFunc = onStopBeforeFunc.and( callback );

        return cast(this);
    }

    public T onStopAfter( VoidFunction0 callback ) {
        onStopAfterFunc = onStopAfterFunc.and( callback );

        return cast(this);
    }

    public T withServiceName( String name ) {
        throwIfRunning();

        this.serviceName = name;

        return cast(this);
    }

    protected final void throwIfNotRunning() {
        Assert.isTrueState( isRunning(), "'%s' is not running", this );
    }

    protected final void throwIfRunning() {
        Assert.isFalseState( isRunning(), "'%s' is running", this );
    }

    private void throwIfShuttingDown() {
        if ( isShuttingDown() ) {
            throw new IllegalStateException( this + " is currently in the process of shutting down" );
        }
    }


    private Subscription appendServices( Object[] newServices, List<StartStoppable> existingServices ) {
        QA.argHasNoNullElements( newServices, "newServices" );

        Subscription compositeSub = new Subscription( () -> {
            existingServices.remove(this);
        } );

        for ( Object candidateService : newServices ) {
            if ( !(candidateService instanceof StartStoppable) ) {
                continue;
            }

            StartStoppable newService = (StartStoppable) candidateService;

            this.callbacks.forEach( newService::withCallback );

            Subscription sub = new Subscription( () -> {
                existingServices.remove(newService);

                if ( this.isRunning() ) {
                    QA.isTrueState(newService.isRunning(), this + " is running, so we expect its chained dependencies to also be running;  however " + newService + " was not.");

                    newService.stop();
                }
            } );

            compositeSub = sub.and(compositeSub);

            existingServices.add( newService );

            if ( this.isRunning() ) {
                newService.start();
            }
        }

        return compositeSub;
    }

}
