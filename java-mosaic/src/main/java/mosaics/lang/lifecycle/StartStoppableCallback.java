package mosaics.lang.lifecycle;

public interface StartStoppableCallback {
    public void starting(StartStoppable service);
    public void started(StartStoppable service);
    public void stopping(StartStoppable service);
    public void stopped(StartStoppable service);
}
