package mosaics.lang;

public class MathUtils {
    private static final double COMPARISON_THRESHOLD = 1e-6;


    public static boolean equals( double a, double b ) {
        return Math.abs(a-b) < COMPARISON_THRESHOLD;
    }

    public static boolean isGT( double a, double b ) {
        return a > b;
    }

    public static boolean isLT( double a, double b ) {
        return a < b;
    }

    public static boolean isGTZero( double gradient ) {
        return gradient > COMPARISON_THRESHOLD;
    }

    public static boolean isLTZero( double gradient ) {
        return gradient < COMPARISON_THRESHOLD;
    }

    public static <T extends Comparable<T>> T max( T a, T b ) {
        if ( a == null ) {
            return b;
        } else if ( b == null ) {
            return a;
        }

        int c = a.compareTo(b);
        if ( c == 0 ) {
            return a;
        }

        return c > 0 ? a : b;
    }

    public static int roundUpToClosestPowerOf2( int v ) {
        int n = 2;

        while ( v > n ) {
            n *= 2;
        }

        return n;
    }

    public static long roundUpToClosestPowerOf2( long v ) {
        long n = 2;

        while ( v > n ) {
            n *= 2;
        }

        return n;
    }

    public static int roundDownToClosestPowerOf2( int v ) {
        int n = 2;

        while ( (n*2) <= v ) {
            n *= 2;
        }

        return Math.max(2,n);
    }

    public static boolean isPowerOf2( int v ) {
        return v == roundUpToClosestPowerOf2( v );
    }

    public static boolean isPowerOf2( long v ) {
        return v == roundUpToClosestPowerOf2( v );
    }

    /**
     * If v falls within the specified min..max range then v is returned untouched.  However, if
     * v falls outside of the min..max range then v will be replaced with either min or max.
     * If v is less than min, then it will be replaced with min.  On the other hand if v is greater
     * than max, then it will be replaced with max.
     */
    public static long constrainWithinRange( long min, long v, long max ) {
        if ( v < min ) {
            return min;
        } else if ( v > max ) {
            return max;
        } else {
            return v;
        }
    }

    public static boolean isInRangeExc( long min, long v, long maxExc ) {
        return v >= min && v < maxExc;
    }

    public static byte[] longToByteArray(long l) {
        byte[] retVal = new byte[8];

        for (int i = 0; i < 8; i++) {
            retVal[i] = (byte) l;
            l >>= 8;
        }

        return retVal;
    }
}
