package mosaics.lang;

public class MissingEnvironmentVariable extends RuntimeException {
    public MissingEnvironmentVariable( String envKey ) {
        super( "Missing environment variable'"+envKey+"'" );
    }
}
