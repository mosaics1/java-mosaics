package mosaics.lang.refcount;

import mosaics.lang.Assert;
import mosaics.lang.Backdoor;
import mosaics.lang.functions.VoidFunction0;
import mosaics.lang.functions.VoidFunction1;
import mosaics.lang.functions.VoidFunctionLO;
import mosaics.lang.lifecycle.Subscription;

import java.io.Closeable;


public abstract class RefCountedMixin<T> implements RefCounted<T>, Closeable {
    private final RefCounter<T> refCounter;


    protected RefCountedMixin() {
        this( v -> {} );
    }

    protected RefCountedMixin( VoidFunction1<T> releaseF ) {
        this.refCounter = new RefCounter<>( Backdoor.cast(this), releaseF, Assert.areAssertionsEnabled() );
    }


    public T incRefCount() {
        refCounter.incRefCount();

        return Backdoor.cast( this );
    }

    public T decRefCount() {
        refCounter.decRefCount();

        return Backdoor.cast( this );
    }

    public long getRefCount() {
        return refCounter.getRefCount();
    }

    /**
     * Implemente to call decRefCount().  This is done to support the following usage pattern:
     *
     * <code>
     *     try ( Resource r = new Resource() ) {
     *         // use r
     *     }
     * </code>
     */
    public void close() {
        decRefCount();
    }

    public Subscription onRefIncInvoke( VoidFunctionLO<T> callback ) {
        return refCounter.onRefIncInvoke( callback );
    }

    public Subscription onRefDecInvoke( VoidFunctionLO<T> callback ) {
        return refCounter.onRefDecInvoke( callback );
    }

    public Subscription justBeforeReleaseInvoke( VoidFunction1<T> callback ) {
        return refCounter.justBeforeReleaseInvoke( callback );
    }

    public Subscription justAfterReleaseInvoke( VoidFunction0 callback ) {
        return refCounter.justAfterReleaseInvoke( callback );
    }

    protected void throwIfReleased() {
        if ( !refCounter.isActive() ) {
            throw new IllegalStateException( "has already been released" );
        }
    }
}
