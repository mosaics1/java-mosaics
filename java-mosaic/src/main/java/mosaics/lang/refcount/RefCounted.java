package mosaics.lang.refcount;

import mosaics.fp.FP;
import mosaics.lang.Backdoor;
import mosaics.lang.functions.VoidFunction0;
import mosaics.lang.functions.VoidFunction1;
import mosaics.lang.functions.VoidFunctionLO;
import mosaics.lang.lifecycle.Subscription;


/**
 * Implementations of this interface manage destruction of a resource by ref counting.  Usually
 * Java uses the Garbage Collector to manage resource cleanup for us however when we choose to go
 * off heap, we loose the benefits that the Garbage Collector provide.  Thus going off heap is
 * a decision that should not be taken lightly.  In the case of using Memory Mapped files, we have
 * no choice as there is no onheap support for memory mapped files.<p/>
 *
 * The approach to ref counting is straight forward, with a few demons lurking in the details.  The
 * top level overview is that every object under ref counting management maintains its own ref counter
 * which starts at 1.  It starts at 1 because the caller that created the resource is assumed to
 * want a reference to the resource.  Once the caller is done with the resource, it must call 'decRefCount()'
 * which will subtract 1 from the ref count.  Once the ref count reaches zero, the resource will be released.<p/>
 *
 * If another location wants to keep a reference to the resource, then to prevent anybody else
 * from releasing the resource it must call 'incRefCount()'.  'incRefCount()' will increase the ref count
 * by one.  The calls to incRefCount and decRefCount must be perfectly balanced, otherwise the ref count
 * will become meaningless and the object will either get released too soon (while still in use) or
 * will get released to late (if at all).  To ensure that the calls are kept balanced, the following
 * pattern is recommended.
 *
 * <code>
 *     incRefCount();  // nb creating the resource counts as an implicit call to incRefCount
 *
 *     try {
 *
 *     } finally {
 *         decRefCount();
 *     }
 * </code>
 */
public interface RefCounted<T> {

    public static <T> void inc( T v ) {
        if ( v instanceof RefCounted ) {
            ((RefCounted) v).incRefCount();
        }
    }

    public static <T> void dec( T v ) {
        if ( v instanceof RefCounted ) {
            ((RefCounted) v).decRefCount();
        }
    }


    /**
     * Increment this objects ref counter by one.  The call will fail if the counter is already at zero.
     */
    public T incRefCount();

    /**
     * Decrement this objects ref counter by one.  The call will fail if the counter is already at zero.<p/>
     *
     * If this call takes the ref counter to zero, then 1) any callbacks registered by justBeforeReleaseInvoke(C)
     * will be invoked, 2) this resource will be released and 3) any callbacks registered by justAfterReleaseInvoke(C)
     * will be invoked.
     */
    public T decRefCount();

    public long getRefCount();

    /**
     * Registers the supplied callback to be invoked every time incRefCount() is called.
     */
    public Subscription onRefIncInvoke( VoidFunctionLO<T> callback );

    /**
     * Registers the supplied callback to be invoked every time decRefCount() is called.
     */
    public Subscription onRefDecInvoke( VoidFunctionLO<T> callback );

    /**
     * Registers the supplied callback to be invoked when the ref count reaches zero.  It will be
     * invoked just before the resource is released.
     */
    public Subscription justBeforeReleaseInvoke( VoidFunction1<T> callback );

    /**
     * Registers the supplied callback to be invoked when the ref count reaches zero.  It will be
     * invoked just after the resource has been released.
     */
    public Subscription justAfterReleaseInvoke( VoidFunction0 callback );


    public default <X extends RefCounted<X>> Subscription dependsUpon( X other ) {
        other.incRefCount();

        return this.justAfterReleaseInvoke( other::decRefCount );
    }

    @SuppressWarnings({"unchecked"})
    public default <X extends RefCounted<X>> Subscription dependsUpon( X...others ) {
        return FP.wrapArray(others).fold(
            Subscription.noOp(),
            (soFar,next) -> soFar.and( this.dependsUpon(next) )
        );
    }

    public default T invokeWithRef( VoidFunction1<T> action ) {
        this.incRefCount();

        try {
            action.invoke( Backdoor.cast(this) );

            return Backdoor.cast( this );
        } finally {
            this.decRefCount();
        }
    }

}
