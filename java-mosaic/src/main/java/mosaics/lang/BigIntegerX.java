package mosaics.lang;

import java.math.BigInteger;


/**
 * Convenience wrapper for BigInteger.
 */
public class BigIntegerX implements ComparableMixin<BigIntegerX> {
    public static final BigIntegerX ZERO = new BigIntegerX(BigInteger.ZERO);
    public static final BigIntegerX ONE  = new BigIntegerX(BigInteger.ONE);


    private final BigInteger bigInteger;

    public BigIntegerX( int v ) {
        this( toBigInteger(v) );
    }

    public BigIntegerX( long v ) {
        this( toBigInteger(v) );
    }

    public BigIntegerX( BigInteger v ) {
        this.bigInteger = v;
    }

    
///// subtract
    
    public BigIntegerX subtract( int v ) {
        return this.subtract(toBigInteger(v));
    }

    public BigIntegerX subtract( long v ) {
        return this.subtract(toBigInteger(v));
    }

    public BigIntegerX subtract( BigIntegerX v ) {
        return subtract( v.bigInteger );
    }

    public BigIntegerX subtract( BigInteger v ) {
        return new BigIntegerX( this.bigInteger.subtract(v) );
    }

///// add
    
    public BigIntegerX add( int v ) {
        return this.add( toBigInteger(v) );
    }

    public BigIntegerX add( long v ) {
        return this.add(toBigInteger(v));
    }

    public BigIntegerX add( BigIntegerX v ) {
        return add(v.bigInteger );
    }

    public BigIntegerX add( BigInteger v ) {
        return new BigIntegerX( this.bigInteger.add(v) );
    }

///// factorial

    public BigIntegerX factorial() {
        if ( isLT(ZERO) ) {
            throw new UnsupportedOperationException( "factorial does not support negative values: " + this.bigInteger );
        }

        BigInteger  sum = BigInteger.ONE;
        BigInteger  n   = BigInteger.ONE;

        while ( n.compareTo(this.bigInteger) <= 0) {
            sum = sum.multiply(n);
            n   = n.add(BigInteger.ONE);
        }

        return new BigIntegerX(sum);
    }

///// comparisons

    public int compareTo( BigIntegerX o ) {
        return this.bigInteger.compareTo(o.bigInteger);
    }

    public boolean isLTEZero() {
        return isLTE(ZERO);
    }

    public boolean isGTZero() {
        return isGT(ZERO);
    }

    public int hashCode() {
        return this.bigInteger.hashCode();
    }

    public boolean equals( Object obj ) {
        if ( obj instanceof BigIntegerX ) {
            return this.bigInteger.equals( ((BigIntegerX) obj).bigInteger );
        }

        return this.bigInteger.equals(obj);
    }

    public String toString() {
        return bigInteger.toString();
    }

    private static BigInteger toBigInteger( int v ) {
        return new BigInteger(Integer.toString(v));
    }

    private static BigInteger toBigInteger( long v ) {
        return new BigInteger(Long.toString(v));
    }

}
