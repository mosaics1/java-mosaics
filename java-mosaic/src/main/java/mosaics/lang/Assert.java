package mosaics.lang;

import mosaics.lang.functions.BooleanFunction0;
import mosaics.lang.functions.Function0;
import mosaics.lang.functions.VoidFunction0;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Supplier;


/**
 * A suite of assertion utilities.  Use to enforce developer assumptions that if violated represent
 * a fatal programming defect.  Like the assert keyword these checks are only invoked when -ea is
 * passed to the JVM at startup, otherwise HotSpot will strip them out of the optimised assembler
 * code.  Which means that they may be used for fairly expensive checks without impacting
 * released code.  However there is a school of thought that assertions should always be executed,
 * as detecting a fatal problem early limits the damage of the bug and makes diagnosing the bug
 * much easier.  The recommendation here is to use QA over Assert for exactly this reason, as well
 * as always use QA for behaviour that is defined as part of a methods API contract.  However for
 * more indepth tests, such as implementing a linear scan to test a binary search then the
 */
@SuppressWarnings("unused")
public class Assert {

    /**
     * Returns true when the JVM is in debug mode.  Toggled via the assertions
     * flag -ea.  This flag can be used to strip out debug checks that slow
     * the JVM down.
     */
    private static final boolean areAssertionsEnabled = detectWhetherAssertionsAreEnabled();


    private static final Map<Thread,Boolean> perThreadAssertionStatus = new ConcurrentHashMap<>();
    private static volatile BooleanFunction0 areAssertionsEnabledF = () -> areAssertionsEnabled;

    /**
     * To be used from unit tests only.
     */
    public static synchronized void enableAssertionsForCallingThread( boolean flag ) {
        Thread currentThread = Thread.currentThread();

        perThreadAssertionStatus.put( currentThread, flag );

        areAssertionsEnabledF = Assert::areAssertionsEnabledForThisThread;
    }

    public static synchronized void unsetAssertionsOverrideForCallingThread() {
        Thread currentThread = Thread.currentThread();

        perThreadAssertionStatus.remove( currentThread );

        if ( perThreadAssertionStatus.isEmpty() ) {
            areAssertionsEnabledF = () -> areAssertionsEnabled;
        }
    }

    public static final synchronized boolean areAssertionsEnabled() {
        return areAssertionsEnabledF.invoke();
    }

    public static final synchronized <T> T ifAssertionsAreEnabled( Function0<T> ifEnabled, Function0<T> ifDisabled ) {
        if ( areAssertionsEnabled() ) {
            return ifEnabled.invoke();
        } else {
            return ifDisabled.invoke();
        }
    }

    public static final void enableAssertionsWhileCalling( VoidFunction0 f ) {
        boolean prevState = areAssertionsEnabled();

        enableAssertionsForCallingThread( true );
        try {
            f.invoke();
        } finally {
            enableAssertionsForCallingThread( prevState );
        }
    }

    private static boolean areAssertionsEnabledForThisThread() {
        return perThreadAssertionStatus.getOrDefault( Thread.currentThread(), false );
    }

// argIsPowerOf2

    public static void argIsPowerOf2( int v, String argName ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsPowerOf2(v, argName);
        }
    }

    public static void argIsPowerOf2( long v, String argName ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsPowerOf2(v, argName);
        }
    }

// isZero

    public static void isZero( byte v, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.isZero(v, message, args);
        }
    }

    public static void isZero( char v, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.isZero(v, message, args);
        }
    }

    public static void isZero( short v, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.isZero(v, message, args);
        }
    }

    public static void isZero( int v, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.isZero(v, message, args);
        }
    }

    public static void isZero( long v, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.isZero(v, message, args);
        }
    }

    public static void isZero( float v, float tolerance, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.isZero(v, tolerance, message, args);
        }
    }

    public static void isZero( double v, double tolerance, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.isZero(v, tolerance, message, args);
        }
    }

// isNotZero

    public static void isNotZero( byte v, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.isNotZero(v, message, args);
        }
    }

    public static void isNotZero( char v, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.isNotZero(v, message, args);
        }
    }

    public static void isNotZero( short v, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.isNotZero(v, message, args);
        }
    }

    public static void isNotZero( int v, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.isNotZero(v, message, args);
        }
    }

    public static void isNotZero( long v, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.isNotZero(v, message, args);
        }
    }

    public static void isNotZero( float v, float tolerance, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.isNotZero(v, tolerance, message, args);
        }
    }

    public static void isNotZero( double v, double tolerance, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.isNotZero(v, tolerance, message, args);
        }
    }

// isGTE

    public static void argIsGTE( byte a, byte b, String argName ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsGTE(a, b, argName);
        }
    }

    public static void argIsGTE( short a, short b, String argName ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsGTE(a, b, argName);
        }
    }

    public static void argIsGTE( int a, int b, String argName ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsGTE(a, b, argName);
        }
    }

    public static void argIsGTE( long a, long b, String argName ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsGTE(a, b, argName);
        }
    }

    public static void argIsGTE( float a, float b, String argName, float tolerance ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsGTE(a, b, argName);
        }
    }

    public static void argIsGTE( double a, double b, String argName, double tolerance ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsGTE(a, b, argName);
        }
    }

    public static <T extends Comparable<T>> void argIsGTE( T a, T b, String argName ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsGTE(a, b, argName);
        }
    }

    public static void isGTE( byte a, byte b, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.isGTE(a, b, message, args);
        }
    }

    public static void isGTE( short a, short b, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.isGTE(a, b, message, args);
        }
    }

    public static void isGTE( int a, int b, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.isGTE(a, b, message, args);
        }
    }

    public static void isGTE( long a, long b, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.isGTE(a, b, message, args);
        }
    }

    public static void isGTE( float a, float b, float tolerance, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.isGTE(a, b, tolerance, message, args);
        }
    }

    public static void isGTE( double a, double b, double tolerance, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.isGTE(a, b, tolerance, message, args);
        }
    }

    public static <T extends Comparable<T>> void GTE( T a, T b, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.GTE(a, b, message, args);
        }
    }


// isGTZero

    public static void argIsGTZero( byte a, String argName ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsGTZero(a, argName);
        }
    }

    public static void argIsGTZero( short a, String argName ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsGTZero(a, argName);
        }
    }

    public static void argIsGTZero( int a, String argName ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsGTZero(a, argName);
        }
    }

    public static void argIsGTZero( long a, String argName ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsGTZero(a, argName);
        }
    }

    public static void argIsGTZero( float a, String argName, float tolerance ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsGTZero(a, argName, tolerance);
        }
    }

    public static void argIsGTZero( double a, String argName, double tolerance ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsGTZero(a, argName, tolerance);
        }
    }


    public static void isGTZero( byte a, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.isGTZero(a, message, args);
        }
    }

    public static void isGTZero( short a, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.isGTZero(a, message, args);
        }
    }

    public static void isGTZero( int a, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.isGTZero(a, message, args);
        }
    }

    public static void isGTZero( long a, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.isGTZero(a, message, args);
        }
    }

    public static void isGTZero( float a, float tolerance, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.isGTZero(a, tolerance, message, args);
        }
    }

    public static void isGTZero( double a, double tolerance, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.isGTZero(a, tolerance, message, args);
        }
    }



// isGTEZero

    public static void argIsGTEZero( byte a, String argName ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsGTEZero(a, argName);
        }
    }

    public static void argIsGTEZero( short a, String argName ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsGTEZero(a, argName);
        }
    }

    public static void argIsGTEZero( int a, String argName ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsGTEZero(a, argName);
        }
    }

    public static void argIsGTEZero( long a, String argName ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsGTEZero(a, argName);
        }
    }

    public static void argIsGTEZero( float a, String argName, float tolerance ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsGTEZero(a, argName, tolerance);
        }
    }

    public static void argIsGTEZero( double a, String argName, double tolerance ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsGTEZero(a, argName, tolerance);
        }
    }

    public static void isGTEZero( byte a, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.isGTEZero(a, message, args);
        }
    }

    public static void isGTEZero( short a, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.isGTEZero(a, message, args);
        }
    }

    public static void isGTEZero( int a, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.isGTEZero(a, message, args);
        }
    }

    public static void isGTEZero( long a, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.isGTEZero(a, message, args);
        }
    }

    public static void isGTEZero( float a, float tolerance, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.isGTEZero(a, tolerance, message, args);
        }
    }

    public static void isGTEZero( double a, double tolerance, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.isGTEZero(a, tolerance, message, args);
        }
    }



// isGT

    public static void argIsGT( byte a, byte b, String argName ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsGT(a, b, argName);
        }
    }

    public static void argIsGT( short a, short b, String argName ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsGT(a, b, argName);
        }
    }

    public static void argIsGT( int a, int b, String argName ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsGT(a, b, argName);
        }
    }

    public static void argIsGT( byte a, byte b, String argNameA, String argNameB ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsGT(a, b, argNameA, argNameB);
        }
    }

    public static void argIsGT( int a, int b, String argNameA, String argNameB ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsGT(a, b, argNameA, argNameB);
        }
    }

    public static void argIsGT( long a, long b, String argNameA, String argNameB ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsGT(a, b, argNameA, argNameB);
        }
    }

    public static <T extends Throwable> void argIsGT( int a, int b, String argName, Class<T> exceptionType ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsGT(a, b, argName, exceptionType);
        }
    }

    public static void argIsGT( long a, long b, String argName ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsGT(a, b, argName);
        }
    }

    public static void argIsGT( float a, float b, String argName, float tolerance ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsGT(a, b, argName, tolerance);
        }
    }

    public static void argIsGT( double a, double b, String argName, double tolerance ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsGT(a, b, argName, tolerance);
        }
    }

    public static <T extends Comparable<T>> void argIsGT( T a, T b, String argName ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsGT(a, b, argName);
        }
    }

    public static void isGT( byte a, byte b, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.isGT(a, b, message, args);
        }
    }

    public static void isGT( short a, short b, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.isGT(a, b, message, args);
        }
    }

    public static void isGT( int a, int b, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.isGT(a, b, message, args);
        }
    }

    public static void isGT( long a, long b, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.isGT(a, b, message, args);
        }
    }

    public static void isGT( float a, float b, float tolerance, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.isGT(a, b, tolerance, message, args);
        }
    }

    public static void isGT( double a, double b, double tolerance, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.isGT(a, b, tolerance, message, args);
        }
    }

    public static <T extends Comparable<T>> void isGT( T a, T b, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.isGT(a, b, message, args);
        }
    }

// isLT

    public static void argIsLT( byte a, byte b, String argName ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsLT(a, b, argName);
        }
    }

    public static void argIsLT( short a, short b, String argName ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsLT(a, b, argName);
        }
    }

    public static void argIsLT( int a, int b, String argName ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsLT(a, b, argName);
        }
    }

    public static void argIsLT( long a, long b, String argName ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsLT(a, b, argName);
        }
    }

    public static void argIsLT( long a, long b, String argName1, String argName2 ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsLT(a, b, argName1, argName2);
        }
    }

    public static void argAIsLTArgB( long a, long b, String argNameA, String argNameB ) {
        if ( areAssertionsEnabled() ) {
            QA.argAIsLTArgB(a, b, argNameA, argNameB);
        }
    }

    public static void argIsLT( float a, float b, String argName, float tolerance ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsLT(a, b, argName, tolerance);
        }
    }

    public static void argIsLT( double a, double b, String argName, double tolerance ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsLT(a, b, argName, tolerance);
        }
    }

    public static <T extends Comparable<T>> void argIsLT( T a, T b, String argName ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsLT(a, b, argName);
        }
    }

    public static void isLT( byte a, byte b, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.isLT(a, b, message, args);
        }
    }

    public static void isLT( short a, short b, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.isLT(a, b, message, args);
        }
    }

    public static void isLT( int a, int b, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.isLT(a, b, message, args);
        }
    }

    public static void isLT( long a, long b, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.isLT(a, b, message, args);
        }
    }

    public static void isLT( float a, float b, float tolerance, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.isLT(a, b, tolerance, message, args);
        }
    }

    public static void isLT( double a, double b, double tolerance, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.isLT(a, b, tolerance, message, args);
        }
    }

    public static <T extends Comparable<T>> void LT( T a, T b, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.LT(a, b, message, args);
        }
    }

// isLTE

    public static void argIsLTE( byte a, byte b, String argName ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsLTE(a, b, argName);
        }
    }

    public static void argIsLTE( short a, short b, String argName ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsLTE(a, b, argName);
        }
    }

    public static void argIsLTE( int a, int b, String argName ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsLTE(a, b, argName);
        }
    }

    public static void argIsLTE( long a, long b, String argName ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsLTE(a, b, argName);
        }
    }

    public static void argIsLTE( long a, long b, String argName1, String argName2 ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsLTE(a, b, argName1, argName2);
        }
    }

    public static void argIsLTE( float a, float b, String argName, float tolerance ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsLTE(a, b, argName, tolerance);
        }
    }

    public static void argIsLTE( double a, double b, String argName, double tolerance ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsLTE(a, b, argName, tolerance);
        }
    }

    public static <T extends Comparable<T>> void argIsLTE( T a, T b, String argName ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsLTE(a, b, argName);
        }
    }

    public static <T extends Comparable<T>> void argIsLTE( T a, T b, String argNameA, String argNameB ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsLTE(a, b, argNameA, argNameB);
        }
    }

    public static void isLTE( byte a, byte b, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.isLTE(a, b, message, args);
        }
    }

    public static void isLTE( short a, short b, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.isLTE(a, b, message, args);
        }
    }

    public static void isLTE( int a, int b, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.isLTE(a, b, message, args);
        }
    }

    public static void isLTE( long a, long b, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.isLTE(a, b, message, args);
        }
    }

    public static void isLTE( float a, float b, float tolerance, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.isLTE(a, b, tolerance, message, args);
        }
    }

    public static void isLTE( double a, double b, double tolerance, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.isLTE(a, b, tolerance, message, args);
        }
    }

    public static <T extends Comparable<T>> void isLTEObjects( T a, T b, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.isLTEObjects(a, b, message, args);
        }
    }

// isLTZero

    public static void argIsLTZero( byte a, String argName ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsLTZero(a, argName);
        }
    }

    public static void argIsLTZero( short a, String argName ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsLTZero(a, argName);
        }
    }

    public static void argIsLTZero( int a, String argName ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsLTZero(a, argName);
        }
    }

    public static void argIsLTZero( long a, String argName ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsLTZero(a, argName);
        }
    }

    public static void argIsLTZero( float a, String argName, float tolerance ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsLTZero(a, argName, tolerance);
        }
    }

    public static void argIsLTZero( double a, String argName, double tolerance ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsLTZero(a, argName, tolerance);
        }
    }

    public static void isLTZero( byte a, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.isLTZero(a, message, args);
        }
    }

    public static void isLTZero( short a, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.isLTZero(a, message, args);
        }
    }

    public static void isLTZero( int a, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.isLTZero(a, message, args);
        }
    }

    public static void isLTZero( long a, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.isLTZero(a, message, args);
        }
    }

    public static void isLTZero( float a, float tolerance, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.isLTZero(a, tolerance, message, args);
        }
    }

    public static void isLTZero( double a, double tolerance, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.isLTZero(a, tolerance, message, args);
        }
    }


// isLTEZero

    public static void argIsLTEZero( byte a, String argName ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsLTEZero(a, argName);
        }
    }

    public static void argIsLTEZero( short a, String argName ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsLTEZero(a, argName);
        }
    }

    public static void argIsLTEZero( int a, String argName ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsLTEZero(a, argName);
        }
    }

    public static void argIsLTEZero( long a, String argName ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsLTEZero(a, argName);
        }
    }

    public static void argIsLTEZero( float a, String argName, float tolerance ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsLTEZero(a, argName, tolerance);
        }
    }

    public static void argIsLTEZero( double a, String argName, double tolerance ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsLTEZero(a, argName, tolerance);
        }
    }

    public static void isLTEZero( byte a, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.isLTEZero(a, message, args);
        }
    }

    public static void isLTEZero( short a, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.isLTEZero(a, message, args);
        }
    }

    public static void isLTEZero( int a, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.isLTEZero(a, message, args);
        }
    }

    public static void isLTEZero( long a, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.isLTEZero(a, message, args);
        }
    }

    public static void isLTEZero( float a, float tolerance, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.isLTEZero(a, tolerance, message, args);
        }
    }

    public static void isLTEZero( double a, double tolerance, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.isLTEZero(a, tolerance, message, args);
        }
    }



/////////

    public static void isMultipleOf2( int v, String argName ) {
        if ( areAssertionsEnabled() ) {
            QA.isMultipleOf2(v, argName);
        }
    }

    public static void isMultipleOf2( long v, String argName ) {
        if ( areAssertionsEnabled() ) {
            QA.isMultipleOf2(v, argName);
        }
    }

    public static void argNotBlank( String chars, String argName ) {
        if ( areAssertionsEnabled() ) {
            QA.argNotBlank(chars, argName);
        }
    }

    public static void notBlank( String chars, String message, Object...args) {
        if ( areAssertionsEnabled() ) {
            QA.notBlank(chars, message, args);
        }
    }

    public static void argNotEmpty( CharSequence chars, String argName ) {
        if ( areAssertionsEnabled() ) {
            QA.argNotEmpty(chars, argName);
        }
    }

    public static void notEmpty( CharSequence chars, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.notEmpty(chars, message, args);
        }
    }

    public static void argIsInterface( Class c, String argName ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsInterface(c, argName);
        }
    }

    public static void argNotNull( Object o, String argName ) {
        if ( areAssertionsEnabled() ) {
            QA.argNotNull(o, argName);
        }
    }

    public static void notNull( Object o, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.notNull(o, message, args);
        }
    }

    public static void argIsNull( Object o, String argName ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsNull(o, argName);
        }
    }

    public static void isNull( Object o, String message, Object...args ) {
        if ( areAssertionsEnabled() ) {
            QA.isNull(o, message, args);
        }
    }

    public static void argIsTrue( boolean v, String argName ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsTrue(v, argName);
        }
    }

    public static void argIsFalse( boolean v, String argName ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsFalse(v, argName);
        }
    }

    public static void isTrueState( boolean condition, String msg, Object... values ) {
        if ( areAssertionsEnabled() ) {
            QA.isTrueState(condition, msg, values);
        }
    }

    public static void isTrueArg( boolean condition, String msg, Object... values ) {
        if ( areAssertionsEnabled() ) {
            QA.isTrueArg(condition, msg, values);
        }
    }

    public static <T extends Throwable> void isTrueState( boolean condition, Class<T> exceptionType, String msg, Object... values ) {
        if ( areAssertionsEnabled() ) {
            QA.isTrueState(condition, exceptionType, msg, values);
        }
    }

    public static void isFalseState( boolean condition, String msg, Object... values ) {
        if ( areAssertionsEnabled() ) {
            QA.isFalseState(condition, msg, values);
        }
    }

    public static void isFalseArg( boolean condition, String msg, Object... values ) {
        if ( areAssertionsEnabled() ) {
            QA.isFalseArg(condition, msg, values);
        }
    }

    public static void argInclusiveBetween( int minInc, int v, int maxInc, String argName ) {
        if ( areAssertionsEnabled() ) {
            QA.argInclusiveBetween(minInc, v, maxInc, argName);
        }
    }

    public static void argHasNoNullElements( Object[] array, String argName ) {
        if ( areAssertionsEnabled() ) {
            QA.argHasNoNullElements(array, argName);
        }
    }

// isEqualTo

    public static void argIsEqualTo( byte a, byte b, String argName ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsEqualTo(a, b, argName);
        }
    }

    public static void argIsEqualTo( short a, short b, String argName ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsEqualTo(a, b, argName);
        }
    }

    public static void argIsEqualTo( int a, int b, String argName ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsEqualTo(a, b, argName);
        }
    }

    public static void argIsEqualTo( long a, long b, String argName ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsEqualTo(a, b, argName);
        }
    }

    public static void argIsEqualTo( long a, long b, String argName1, String argName2 ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsEqualTo(a, b, argName1, argName2);
        }
    }

    public static void argIsEqualTo( float a, float b, String argName, float tolerance ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsEqualTo(a, b, argName, tolerance);
        }
    }

    public static void argIsEqualTo( double a, double b, String argName, double tolerance ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsEqualTo(a, b, argName, tolerance);
        }
    }

    public static <T extends Comparable<T>> void argIsEqualTo( T a, T b, String argName ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsEqualTo(a, b, argName);
        }
    }

    public static void isEqualTo( byte a, byte b, String msg ) {
        if ( areAssertionsEnabled() ) {
            QA.isEqualTo(a, b, msg);
        }
    }

    public static void isEqualTo( short a, short b, String msg ) {
        if ( areAssertionsEnabled() ) {
            QA.isEqualTo(a, b, msg);
        }
    }

    public static void isEqualTo( int a, int b, String msg ) {
        if ( areAssertionsEnabled() ) {
            QA.isEqualTo(a, b, msg);
        }
    }

    public static void isEqualTo( long a, long b, String msg ) {
        if ( areAssertionsEnabled() ) {
            QA.isEqualTo(a, b, msg);
        }
    }

    public static void isEqualTo( long a, long b, String nameA, String nameB ) {
        if ( areAssertionsEnabled() ) {
            QA.isEqualTo(a, b, nameA, nameB);
        }
    }

    public static void isEqualTo( float a, float b, String msg, float tolerance ) {
        if ( areAssertionsEnabled() ) {
            QA.isEqualTo(a, b, msg, tolerance);
        }
    }

    public static void isEqualTo( double a, double b, String msg, double tolerance ) {
        if ( areAssertionsEnabled() ) {
            QA.isEqualTo(a, b, msg, tolerance);
        }
    }

    public static <T extends Comparable<T>> void isEqualTo( T a, T b, String msg ) {
        if ( areAssertionsEnabled() ) {
            QA.isEqualTo(a, b, msg);
        }
    }

    public static <T> void argIsNotEqualTo( T a, T b, String argName ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsNotEqualTo(a, b, argName);
        }
    }

    public static void argIsNotEqualTo( long a, long b, String argName1, String argName2 ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsNotEqualTo(a, b, argName1, argName2);
        }
    }

    /**
     * Validates that minInc &lt;= n &lt; maxExc and throws IndexOutOfBoundsException if it fails.
     */
    public static void argIsBetween( byte minInc, byte n, byte maxExc, String argName ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsBetween(minInc, n, maxExc, argName);
        }
    }

    /**
     * Validates that minInc &lt;= n &lt; maxExc and throws IndexOutOfBoundsException if it fails.
     */
    public static void argIsBetween( short minInc, short n, short maxExc, String argName ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsBetween(minInc, n, maxExc, argName);
        }
    }

    /**
     * Validates that minInc &lt;= n &lt; maxExc and throws IndexOutOfBoundsException if it fails.
     */
    public static void argIsBetween( char minInc, char n, char maxExc, String argName ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsBetween(minInc, n, maxExc, argName);
        }
    }

    /**
     * Validates that minInc &lt;= n &lt; maxExc and throws IndexOutOfBoundsException if it fails.
     */
    public static void argIsBetween( int minInc, int n, int maxExc, String argName ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsBetween(minInc, n, maxExc, argName);
        }
    }

    /**
     * Validates that minInc &lt;= n &lt; maxExc and throws IndexOutOfBoundsException if it fails.
     */
    public static void argIsBetween( long minInc, long n, long maxExc, String argName ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsBetween(minInc, n, maxExc, argName);
        }
    }

    /**
     * Validates that minInc &lt;= n &lt;= maxExc and throws IndexOutOfBoundsException if it fails.
     */
    public static void argIsBetweenInc( byte minInc, byte n, byte maxInc, String argName ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsBetweenInc(minInc, n, maxInc, argName);
        }
    }

    /**
     * Validates that minInc &lt;= n &lt;= maxInc and throws IndexOutOfBoundsException if it fails.
     */
    public static void argIsBetweenInc( short minInc, short n, short maxInc, String argName ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsBetweenInc(minInc, n, maxInc, argName);
        }
    }

    /**
     * Validates that minInc &lt;= n &lt;= maxInc and throws IndexOutOfBoundsException if it fails.
     */
    public static void argIsBetweenInc( char minInc, char n, char maxInc, String argName ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsBetweenInc(minInc, n, maxInc, argName);
        }
    }

    /**
     * Validates that minInc &lt;= n &lt;= maxInc and throws IndexOutOfBoundsException if it fails.
     */
    public static void argIsBetweenInc( int minInc, int n, int maxInc, String argName ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsBetweenInc(minInc, n, maxInc, argName);
        }
    }

    /**
     * Validates that minInc &lt;= n &lt;= maxInc and throws IndexOutOfBoundsException if it fails.
     */
    public static void argIsBetweenInc( long minInc, long n, long maxInc, String argName ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsBetweenInc(minInc, n, maxInc, argName);
        }
    }

    /**
     * The specified range must equal or be within specified range.
     */
    public static void argIsWithinRange( int minInc, int minValue, int maxValue, int maxExc, String minValueName, String maxValueName ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsWithinRange(minInc, minValue, maxValue, maxExc, minValueName, maxValueName);
        }
    }

    /**
     * The specified range must equal or be within specified range.
     */
    public static void argIsWithinRange( long minInc, long v1, long v2, long maxExc, String minValueName, String maxValueName ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsWithinRange(minInc, v1, v2, maxExc, minValueName, maxValueName);
        }
    }

    public static void argIsUnsignedByte( int v, String name ) {
        if ( areAssertionsEnabled() ) {
            QA.argIsUnsignedByte(v, name);
        }
    }

    public static void isInt( long v, String argName ) {
        if ( areAssertionsEnabled() ) {
            QA.isInt(v, argName);
        }
    }

    public static void isUnsignedInt( long v, String argName ) {
        if ( areAssertionsEnabled() ) {
            QA.isUnsignedInt(v, argName);
        }
    }

    public static void isUnsignedShort( int v, String argName ) {
        if ( areAssertionsEnabled() ) {
            QA.isUnsignedInt(v, argName);
        }
    }

    /**
     * Fails if the supplied function returns a String.  The string is a failure message.
     * Because function0 is invoked 'lazily', use this method when the arguments
     * to a normal call to QA cannot be hard coded.  Thus when checks are disabled, there
     * will be no runtime cost.
     */
    public static void assertCondition( Supplier<String> function0 ) {
        if ( areAssertionsEnabled() ) {
            QA.assertCondition(function0);
        }
    }




    @SuppressWarnings({"AssertWithSideEffects", "UnusedAssignment", "ConstantConditions"})
    private static boolean detectWhetherAssertionsAreEnabled() {
        boolean flag = false;

        assert (flag = true); // NB sets flag to true by side effect

        return flag;
    }
}
