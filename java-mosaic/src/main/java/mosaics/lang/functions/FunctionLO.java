package mosaics.lang.functions;

import java.util.Objects;


public interface FunctionLO<B,R> {
    public R invoke( long a, B b );

    public default FunctionLO<B,R> withDescription( String desc ) {
        return new FunctionLOWithDescription<>( this, desc );
    }
}

class FunctionLOWithDescription<B,R> implements FunctionLO<B,R> {
    private String          description;
    private FunctionLO<B,R> f;

    public FunctionLOWithDescription( FunctionLO<B,R> f, String description ) {
        this.description = description;
        this.f           = f;
    }

    public R invoke( long a, B b ) {
        return f.invoke(a,b);
    }

    public FunctionLO<B,R> withDescription( String desc ) {
        if ( Objects.equals(this.description, desc) ) {
            return this;
        }

        return new FunctionLOWithDescription<>( this, desc );
    }
}
