package mosaics.lang.functions;

public interface ByteFunctionI {
    public byte invoke( int v );
}
