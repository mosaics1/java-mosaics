package mosaics.lang.functions;

import mosaics.fp.FP;
import mosaics.fp.collections.FPOption;
import lombok.Value;

import java.util.Objects;


public interface BooleanFunctionL {
    public boolean invoke( long v );


    public default FPOption<String> getDescription() {
        return FP.emptyOption();
    }

    public default BooleanFunctionL withDescription( String desc ) {
        if ( desc == null || desc.trim().length() == 0 ) {
            return this;
        }

        return new BooleanFunctionLWithDescription( FP.option(desc), this );
    }

    public default BooleanFunctionL invert() {
        return new InvertedBooleanFunctionL( this );
    }
}

@Value
class InvertedBooleanFunctionL implements BooleanFunctionL {
    private BooleanFunctionL f;

    public boolean invoke( long v ) {
        return !f.invoke(v);
    }

    public BooleanFunctionL invert() {
        return f;
    }
}


@Value
class BooleanFunctionLWithDescription implements BooleanFunctionL {
    private FPOption<String> description;
    private BooleanFunctionL f;


    public boolean invoke( long v ) {
        return f.invoke(v);
    }

    public BooleanFunctionL withDescription( String desc ) {
        if ( Objects.equals(this.description.get(), desc) ) {
            return this;
        } else if ( desc == null || desc.trim().length() == 0 ) {
            return f;
        }

        return new BooleanFunctionLWithDescription( FP.option(desc), f );
    }
}
