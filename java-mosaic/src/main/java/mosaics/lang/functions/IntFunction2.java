package mosaics.lang.functions;

import java.util.Objects;


public interface IntFunction2<A,B> {
    public int invoke( A a, B b );

    public default IntFunction2<A,B> withDescription( String desc ) {
        return new IntFunction2WithDescription<>( this, desc );
    }
}

class IntFunction2WithDescription<A,B> implements IntFunction2<A,B> {
    private String                description;
    private IntFunction2<A,B> f;

    public IntFunction2WithDescription( IntFunction2<A,B> f, String description ) {
        this.description = description;
        this.f           = f;
    }

    public int invoke( A a, B b ) {
        return f.invoke(a, b);
    }

    public IntFunction2<A,B> withDescription( String desc ) {
        if ( Objects.equals(this.description, desc) ) {
            return this;
        }

        return new IntFunction2WithDescription<>( this, desc );
    }
}
