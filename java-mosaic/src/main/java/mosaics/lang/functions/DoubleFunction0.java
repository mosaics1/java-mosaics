package mosaics.lang.functions;

import java.util.Objects;


public interface DoubleFunction0 {
    public double invoke();

    public default DoubleFunction0 withDescription( String desc ) {
        return new DoubleFunction0WithDescription( this, desc );
    }
}

class DoubleFunction0WithDescription implements DoubleFunction0 {
    private String       description;
    private DoubleFunction0 f;

    public DoubleFunction0WithDescription( DoubleFunction0 f, String description ) {
        this.description = description;
        this.f           = f;
    }

    public double invoke() {
        return f.invoke();
    }

    public DoubleFunction0 withDescription( String desc ) {
        if ( Objects.equals(this.description, desc) ) {
            return this;
        }

        return new DoubleFunction0WithDescription( this, desc );
    }
}

