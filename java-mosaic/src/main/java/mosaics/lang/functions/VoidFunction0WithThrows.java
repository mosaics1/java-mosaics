package mosaics.lang.functions;

import mosaics.lang.TryUtils;

import java.lang.reflect.InvocationTargetException;


public interface VoidFunction0WithThrows {
    public void invoke() throws Throwable;

    public default VoidFunction0WithThrows withDescription( String desc ) {
        return new VoidFunction0WithThrowsAndDescription(this,desc);
    }

    public default void invokeQuietly() {
        TryUtils.invokeAndRethrowException( this::invoke );
    }

    public default void invokeAndWrapException() throws InvocationTargetException {
        try {
            invoke();
        } catch ( Throwable ex ) {
            throw new InvocationTargetException(ex);
        }
    }
}


class VoidFunction0WithThrowsAndDescription implements VoidFunction0WithThrows {
    private VoidFunction0WithThrows orig;
    private String                  desc;

    public VoidFunction0WithThrowsAndDescription( VoidFunction0WithThrows orig, String desc ) {
        this.orig = orig;
        this.desc = desc;
    }

    public void invoke() throws Throwable {
        orig.invoke();
    }

    public VoidFunction0WithThrows withDescription( String desc ) {
        return new VoidFunction0WithThrowsAndDescription( orig, desc );
    }

    public String toString() {
        return desc;
    }
}
