package mosaics.lang.functions;

import java.util.Objects;


public interface VoidFunctionIO<T> {
    public void invoke( int i, T v );

    public default VoidFunctionIO<T> withDescription( String desc ) {
        return new VoidFunctionIOWithDescription<>( this, desc );
    }
}

class VoidFunctionIOWithDescription<T> implements VoidFunctionIO<T> {
    private String          description;
    private VoidFunctionIO<T> f;

    public VoidFunctionIOWithDescription( VoidFunctionIO<T> f, String description ) {
        this.description = description;
        this.f           = f;
    }

    public void invoke( int a, T b ) {
        f.invoke(a,b);
    }

    public VoidFunctionIO<T> withDescription( String desc ) {
        if ( Objects.equals(this.description, desc) ) {
            return this;
        }

        return new VoidFunctionIOWithDescription<>( this, desc );
    }
}
