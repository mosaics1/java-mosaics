package mosaics.lang.functions;

public interface DoubleFunctionI {
    public double invoke( int v );
}
