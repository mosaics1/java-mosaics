package mosaics.lang.functions;

import java.util.Objects;


public interface IntFunctionOII<A> {
    public int invoke( A a1, int b, int c );

    public default IntFunctionOII<A> withDescription( String desc ) {
        return new IntFunctionOIIWithDescription<>( this, desc );
    }
}

class IntFunctionOIIWithDescription<A> implements IntFunctionOII<A> {
    private String              description;
    private IntFunctionOII<A> f;

    public IntFunctionOIIWithDescription( IntFunctionOII<A> f, String description ) {
        this.description = description;
        this.f           = f;
    }

    public int invoke( A a, int b, int c ) {
        return f.invoke(a,b,c);
    }

    public IntFunctionOII<A> withDescription( String desc ) {
        if ( Objects.equals(this.description, desc) ) {
            return this;
        }

        return new IntFunctionOIIWithDescription<>( this, desc );
    }
}
