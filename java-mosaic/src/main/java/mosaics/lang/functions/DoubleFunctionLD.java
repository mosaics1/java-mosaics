package mosaics.lang.functions;

public interface DoubleFunctionLD {
    public double invoke( long v1, double v2 );
}
