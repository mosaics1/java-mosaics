package mosaics.lang.functions;

import java.io.IOException;
import java.util.Objects;


public interface VoidFunction1WithIOThrows<T> {
    public void invoke( T v ) throws IOException;

    public default VoidFunction1WithIOThrows<T> withDescription( String desc ) {
        return new VoidFunction1WithIOThrowsWithDescription<>( this, desc );
    }
}

class VoidFunction1WithIOThrowsWithDescription<T> implements VoidFunction1WithIOThrows<T> {
    private String                       description;
    private VoidFunction1WithIOThrows<T> f;

    public VoidFunction1WithIOThrowsWithDescription( VoidFunction1WithIOThrows<T> f, String description ) {
        this.description = description;
        this.f           = f;
    }

    public void invoke( T a ) throws IOException {
        f.invoke(a);
    }

    public VoidFunction1WithIOThrows<T> withDescription( String desc ) {
        if ( Objects.equals(this.description, desc) ) {
            return this;
        }

        return new VoidFunction1WithIOThrowsWithDescription<>( this, desc );
    }
}
