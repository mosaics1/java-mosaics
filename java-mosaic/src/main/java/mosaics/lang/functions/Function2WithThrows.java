package mosaics.lang.functions;


import java.util.Objects;


public interface Function2WithThrows<A,B, R> {
    public R invoke( A arg1, B arg2 ) throws Throwable;

    public default Function2WithThrows<A,B,R> withDescription( String desc ) {
        return new Function2WithThrowsWithDescription<>( this, desc );
    }
}

class Function2WithThrowsWithDescription<A,B,R> implements Function2WithThrows<A,B,R> {
    private String          description;
    private Function2WithThrows<A,B,R> f;

    public Function2WithThrowsWithDescription( Function2WithThrows<A,B,R> f, String description ) {
        this.description = description;
        this.f           = f;
    }

    public R invoke( A a, B b ) throws Throwable {
        return f.invoke(a,b);
    }

    public Function2WithThrows<A,B,R> withDescription( String desc ) {
        if ( Objects.equals(this.description, desc) ) {
            return this;
        }

        return new Function2WithThrowsWithDescription<>( this, desc );
    }
}
