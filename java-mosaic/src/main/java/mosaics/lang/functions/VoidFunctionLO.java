package mosaics.lang.functions;

import java.util.Objects;


public interface VoidFunctionLO<T> {
    public void invoke( long i, T v );

    public default VoidFunctionLO<T> withDescription( String desc ) {
        return new VoidFunctionLOWithDescription<>( this, desc );
    }
}

class VoidFunctionLOWithDescription<T> implements VoidFunctionLO<T> {
    private String          description;
    private VoidFunctionLO<T> f;

    public VoidFunctionLOWithDescription( VoidFunctionLO<T> f, String description ) {
        this.description = description;
        this.f           = f;
    }

    public void invoke( long a, T b ) {
        f.invoke(a,b);
    }

    public VoidFunctionLO<T> withDescription( String desc ) {
        if ( Objects.equals(this.description, desc) ) {
            return this;
        }

        return new VoidFunctionLOWithDescription<>( this, desc );
    }
}
