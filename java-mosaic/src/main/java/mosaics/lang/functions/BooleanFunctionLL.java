package mosaics.lang.functions;

import mosaics.fp.FP;
import mosaics.fp.collections.FPOption;
import lombok.Value;

import java.util.Objects;


public interface BooleanFunctionLL {
    public boolean invoke( long a, long b );

    public default FPOption<String> getDescription() {
        return FP.emptyOption();
    }

    public default BooleanFunctionLL withDescription( String desc ) {
        if ( desc == null || desc.trim().length() == 0 ) {
            return this;
        }

        return new BooleanFunctionLLWithDescription( FP.option(desc), this );
    }

    public default BooleanFunctionLL invert() {
        return new InvertedBooleanFunctionLL( this );
    }
}


@Value
class InvertedBooleanFunctionLL implements BooleanFunctionLL {
    private BooleanFunctionLL f;

    public boolean invoke( long a, long b ) {
        return !f.invoke(a,b);
    }

    public BooleanFunctionLL invert() {
        return f;
    }
}


@Value
class BooleanFunctionLLWithDescription implements BooleanFunctionLL {
    private FPOption<String>  description;
    private BooleanFunctionLL f;


    public boolean invoke( long a, long b ) {
        return f.invoke(a, b);
    }

    public BooleanFunctionLL withDescription( String desc ) {
        if ( Objects.equals(this.description.get(), desc) ) {
            return this;
        } else if ( desc == null || desc.trim().length() == 0 ) {
            return f;
        }

        return new BooleanFunctionLLWithDescription( FP.option(desc), f );
    }
}
