package mosaics.lang.functions;

import java.util.Objects;


public interface IntFunction1<A> {
    public int invoke( A a );

    public default IntFunction1<A> withDescription( String desc ) {
        return new IntFunction1WithDescription<>( this, desc );
    }
}

class IntFunction1WithDescription<A> implements IntFunction1<A> {
    private String              description;
    private IntFunction1<A> f;

    public IntFunction1WithDescription( IntFunction1<A> f, String description ) {
        this.description = description;
        this.f           = f;
    }

    public int invoke( A arg ) {
        return f.invoke(arg);
    }

    public IntFunction1<A> withDescription( String desc ) {
        if ( Objects.equals(this.description, desc) ) {
            return this;
        }

        return new IntFunction1WithDescription<>( this, desc );
    }
}
