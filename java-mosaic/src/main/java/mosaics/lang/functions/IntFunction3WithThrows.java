package mosaics.lang.functions;

import java.util.Objects;


public interface IntFunction3WithThrows<A,B,C> {
    public int invoke( A a, B b, C c ) throws Exception;

    public default IntFunction3WithThrows<A,B,C> withDescription( String desc ) {
        return new IntFunction3WithThrowsWithDescription<>( this, desc );
    }
}

class IntFunction3WithThrowsWithDescription<A,B,C> implements IntFunction3WithThrows<A,B,C> {
    private String              description;
    private IntFunction3WithThrows<A,B,C> f;

    public IntFunction3WithThrowsWithDescription( IntFunction3WithThrows<A,B,C> f, String description ) {
        this.description = description;
        this.f           = f;
    }

    public int invoke( A a, B b, C c ) throws Exception {
        return f.invoke(a, b, c);
    }

    public IntFunction3WithThrows<A,B,C> withDescription( String desc ) {
        if ( Objects.equals(this.description, desc) ) {
            return this;
        }

        return new IntFunction3WithThrowsWithDescription<>( this, desc );
    }
}
