package mosaics.lang.functions;


import mosaics.fp.FP;
import mosaics.fp.collections.FPOption;
import lombok.Value;

import java.util.Objects;


public interface BooleanFunction0 {
    public boolean invoke();

    public default FPOption<String> getDescription() {
        return FP.emptyOption();
    }

    public default BooleanFunction0 withDescription( String desc ) {
        if ( desc == null || desc.trim().length() == 0 ) {
            return this;
        }

        return new BooleanFunction0WithDescription( this, desc );
    }

    public default BooleanFunction0 invert() {
        return new InvertedBooleanFunction0(this);
    }
}

@Value
class InvertedBooleanFunction0 implements BooleanFunction0 {
    private BooleanFunction0 f;

    public boolean invoke() {
        return !f.invoke();
    }

    public BooleanFunction0 invert() {
        return f;
    }
}

@Value
class BooleanFunction0WithDescription implements BooleanFunction0 {
    private String           description;
    private BooleanFunction0 f;

    public BooleanFunction0WithDescription( BooleanFunction0 f, String description ) {
        this.description = description;
        this.f           = f;
    }

    public boolean invoke() {
        return f.invoke();
    }

    public BooleanFunction0 withDescription( String desc ) {
        if ( Objects.equals(this.description, desc) ) {
            return this;
        } else if ( desc == null || desc.trim().length() == 0 ) {
            return f;
        }

        return new BooleanFunction0WithDescription( f, desc );
    }

    public FPOption<String> getDescription() {
        return FP.option( description );
    }
}
