package mosaics.lang.functions;

import mosaics.fp.FP;
import mosaics.fp.collections.FPOption;
import mosaics.lang.TryUtils;
import lombok.Value;

import java.util.Objects;


public interface Function0WithThrows<R> {
    public R invoke() throws Throwable;

    public default FPOption<String> getDescription() {
        return FP.emptyOption();
    }

    public default Function0WithThrows<R> withDescription( String desc ) {
        if ( desc == null || desc.trim().length() == 0 ) {
            return this;
        }

        return new Function0WithThrowsWithDescription<>( FP.option(desc), this );
    }

    public default R invokeSilently() {
        return TryUtils.invokeAndRethrowException( this );
    }

}


@Value
class Function0WithThrowsWithDescription<R> implements Function0WithThrows<R> {
    private FPOption<String>       description;
    private Function0WithThrows<R> f;

    public R invoke() throws Throwable {
        return f.invoke();
    }

    public Function0WithThrows<R> withDescription( String desc ) {
        if ( desc == null || desc.trim().length() == 0 ) {
            return f;
        } else if ( Objects.equals(this.description.get(), desc) ) {
            return this;
        }

        return new Function0WithThrowsWithDescription<>( FP.option(desc), this );
    }
}
