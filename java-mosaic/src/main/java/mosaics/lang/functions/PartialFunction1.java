package mosaics.lang.functions;


import java.util.Objects;


public interface PartialFunction1<A1, R> {

    public R invoke(A1 a1);

    public boolean coversArg(A1 a1);


    public default PartialFunction1<A1,R> withDescription( String desc ) {
        return new PartialFunction1WithDescription<>( this, desc );
    }
}

class PartialFunction1WithDescription<A1,R> implements PartialFunction1<A1,R> {
    private String          description;
    private PartialFunction1<A1,R> f;

    public PartialFunction1WithDescription( PartialFunction1<A1,R> f, String description ) {
        this.description = description;
        this.f           = f;
    }

    public R invoke( A1 v ) {
        return f.invoke(v);
    }

    public boolean coversArg( A1 a1 ) {
        return f.coversArg( a1 );
    }

    public PartialFunction1<A1,R> withDescription( String desc ) {
        if ( Objects.equals(this.description, desc) ) {
            return this;
        }

        return new PartialFunction1WithDescription<>( this, desc );
    }
}
