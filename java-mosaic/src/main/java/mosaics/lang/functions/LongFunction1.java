package mosaics.lang.functions;

import java.util.Objects;


public interface LongFunction1<A1> {
    public long invoke(A1 arg1);

    public default LongFunction1<A1> withDescription( String desc ) {
        return new LongFunction1WithDescription<>( this, desc );
    }
}

class LongFunction1WithDescription<A> implements LongFunction1<A> {
    private String              description;
    private LongFunction1<A> f;

    public LongFunction1WithDescription( LongFunction1<A> f, String description ) {
        this.description = description;
        this.f           = f;
    }

    public long invoke( A arg ) {
        return f.invoke(arg);
    }

    public LongFunction1<A> withDescription( String desc ) {
        if ( Objects.equals(this.description, desc) ) {
            return this;
        }

        return new LongFunction1WithDescription<>( this, desc );
    }
}
