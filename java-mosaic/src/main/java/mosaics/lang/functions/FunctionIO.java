package mosaics.lang.functions;

import java.util.Objects;


public interface FunctionIO<B,R> {
    public R invoke( int a, B b );

    public default FunctionIO<B,R> withDescription( String desc ) {
        return new FunctionIOWithDescription<>( this, desc );
    }
}

class FunctionIOWithDescription<B,R> implements FunctionIO<B,R> {
    private String          description;
    private FunctionIO<B,R> f;

    public FunctionIOWithDescription( FunctionIO<B,R> f, String description ) {
        this.description = description;
        this.f           = f;
    }

    public R invoke( int a, B b ) {
        return f.invoke(a,b);
    }

    public FunctionIO<B,R> withDescription( String desc ) {
        if ( Objects.equals(this.description, desc) ) {
            return this;
        }

        return new FunctionIOWithDescription<>( this, desc );
    }
}
