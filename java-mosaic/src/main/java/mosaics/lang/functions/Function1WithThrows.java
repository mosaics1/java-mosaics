package mosaics.lang.functions;


import mosaics.fp.FP;
import mosaics.fp.Try;
import mosaics.fp.Tryable;
import mosaics.fp.collections.FPOption;
import mosaics.lang.Backdoor;
import mosaics.lang.TryUtils;
import lombok.Value;

import java.util.Objects;
import java.util.function.Function;


public interface Function1WithThrows<A, R> {
    public R invoke( A arg1 ) throws Throwable;


    public default FPOption<String> getDescription() {
        return FP.emptyOption();
    }

    public default Function1WithThrows<A,R> withDescription( String desc ) {
        if ( desc == null || desc.trim().length() == 0 ) {
            return this;
        }

        return new Function1WithThrowsWithDescription<>( FP.option(desc), this );
    }

    public default R invokeSilently( A v ) {
        return TryUtils.invokeAndRethrowException( () -> this.invoke(v) );
    }

    public default Function<A,R> toFunction() {
        return v -> {
            try {
                return this.invoke(v);
            } catch ( Throwable ex ) {
                throw Backdoor.throwException( ex );
            }
        };
    }

    public default Function1<A,Tryable<R>> liftToTryable() {
        return arg -> Try.invoke( () -> this.invoke(arg) );
    }
}


@Value
class Function1WithThrowsWithDescription<A,R> implements Function1WithThrows<A,R> {
    private FPOption<String>         description;
    private Function1WithThrows<A,R> f;


    public R invoke( A v ) throws Throwable {
        return f.invoke(v);
    }

    public Function1WithThrows<A,R> withDescription( String desc ) {
        if ( desc == null || desc.trim().length() == 0 ) {
            return f;
        } if ( Objects.equals(this.description.get(), desc) ) {
            return this;
        }

        return new Function1WithThrowsWithDescription<>( FP.option(desc), this );
    }
}
