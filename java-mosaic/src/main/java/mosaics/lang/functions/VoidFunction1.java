package mosaics.lang.functions;

import java.util.Objects;


public interface VoidFunction1<A> {
    public static <T> VoidFunction1<T> noOp() {
        return v -> {};
    }

    public void invoke(A arg);

    public default <T> Function1<A,T> toFunction1() {
        return v -> {
            VoidFunction1.this.invoke( v );

            return null;
        };
    };

    public default VoidFunction1<A> and( VoidFunction1<A> b ) {
        if ( b == null ) {
            return this;
        }

        VoidFunction1<A> a = this;

        return v -> {
            a.invoke(v);
            b.invoke(v);
        };
    }

    public default VoidFunction1<A> withDescription( String desc ) {
        return new VoidFunction1WithDescription<>( this, desc );
    }
}

class VoidFunction1WithDescription<A> implements VoidFunction1<A> {
    private String           description;
    private VoidFunction1<A> f;

    public VoidFunction1WithDescription( VoidFunction1<A> f, String description ) {
        this.description = description;
        this.f           = f;
    }

    public void invoke( A a ) {
        f.invoke(a);
    }

    public VoidFunction1<A> withDescription( String desc ) {
        if ( Objects.equals(this.description, desc) ) {
            return this;
        }

        return new VoidFunction1WithDescription<>( this, desc );
    }
}
