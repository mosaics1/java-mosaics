package mosaics.lang.functions;


import mosaics.fp.FP;
import mosaics.fp.collections.FPOption;
import lombok.Value;

import java.util.Objects;
import java.util.function.Function;


public interface Function1<A1,R> {
    public static <T> Function1<T,T> identity() {
        return a -> a;
    }

    public R invoke( A1 v );
    
    public default Function1<A1,R> withDescription( String desc ) {
        if ( desc == null || desc.trim().length() == 0 ) {
            return this;
        }

        return new Function1WithDescription<>( FP.option(desc), this );
    }

    public default FPOption<String> getDescription() {
        return FP.emptyOption();
    }

    public default Function<A1,R> toFunction() {
        return this::invoke;
    }

    public default <B> Function1<A1,B> mapResult( Function1<R,B> mappingFunction ) {
        return arg -> mappingFunction.invoke(this.invoke(arg));
    }
}


@Value
class Function1WithDescription<A1,R> implements Function1<A1,R> {
    private FPOption<String> description;
    private Function1<A1,R>  f;


    public R invoke( A1 v ) {
        return f.invoke(v);
    }

    public Function1<A1,R> withDescription( String desc ) {
        if ( desc == null || desc.trim().length() == 0 ) {
            return f;
        } else if ( Objects.equals(this.description.get(), desc) ) {
            return this;
        }

        return new Function1WithDescription<>( FP.option(desc), this );
    }
}
