package mosaics.lang.functions;

import java.util.function.Function;

import static mosaics.lang.Backdoor.cast;


/**
 * An interface for methods that know how to create objects of different types.<p/>
 *
 * Used in combination with junit-mosaics @Random.
 */
public class ObjectFactory {
    private Function<Class,Object> f;

    public ObjectFactory( Function<Class,Object> f ) {
        this.f = f;
    }

    public<T> T create( Class<T> type ) {
        return cast( f.apply(type) );
    }

    public int rndInt( int min, int maxExc ) {
        double range = maxExc - min;
        return min + (int) (rndUnitDouble() * range);
    }

    public long rndLong( long min, long maxExc ) {
        double range = maxExc - min;
        return min + (long) (rndUnitDouble() * range);
    }

    /**
     * Return a random float value between 0 and 1.
     */
    public float rndUnitFloat() {
        return Math.abs(create(float.class)) / Float.MAX_VALUE;
    }

    public float rndFloat( float min, float maxExc ) {
        float range = maxExc - min;
        return min + rndUnitFloat() * range;
    }

    /**
     * Return a random double value between 0 and 1.
     */
    public double rndUnitDouble() {
        return Math.abs(create(double.class)) / Double.MAX_VALUE;
    }

    public double rndDouble( double min, double maxExc ) {
        double range = maxExc - min;
        return min + rndUnitDouble() * range;
    }

    @SafeVarargs
    public final <T> T rndOneOf( T...candidates ) {
        int len = candidates.length;

        return candidates[rndInt(0,len)];
    }
}
