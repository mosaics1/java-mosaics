package mosaics.lang.functions;

import java.util.Objects;


public interface LongFunctionOLL<A> {
    public long invoke( A a1, long b, long c );

    public default LongFunctionOLL<A> withDescription( String desc ) {
        return new LongFunctionOLLWithDescription<>( this, desc );
    }
}

class LongFunctionOLLWithDescription<A> implements LongFunctionOLL<A> {
    private String              description;
    private LongFunctionOLL<A> f;

    public LongFunctionOLLWithDescription( LongFunctionOLL<A> f, String description ) {
        this.description = description;
        this.f           = f;
    }

    public long invoke( A a, long b, long c ) {
        return f.invoke(a,b,c);
    }

    public LongFunctionOLL<A> withDescription( String desc ) {
        if ( Objects.equals(this.description, desc) ) {
            return this;
        }

        return new LongFunctionOLLWithDescription<>( this, desc );
    }
}
