package mosaics.lang.functions;

import mosaics.lang.TryUtils;

import java.lang.reflect.InvocationTargetException;
import java.util.Objects;


public interface VoidFunction1WithThrows<A> {
    public void invoke( A arg1 ) throws Throwable;

    public default void invokeQuietly( A arg1 ) {
        TryUtils.invokeAndRethrowException( () -> this.invoke(arg1) );
    }

    public default <R> Function1WithThrows<A,R> toFunction1WithThrows() {
        return arg1 -> {
            this.invoke(arg1);

            return null;
        };
    }

    public default VoidFunction1WithThrows<A> withDescription( String desc ) {
        return new VoidFunction1WithThrowsWithDescription<>( this, desc );
    }

    public default void invokeAndWrapException( A v ) throws InvocationTargetException {
        try {
            invoke( v );
        } catch ( Throwable ex ) {
            throw new InvocationTargetException(ex);
        }
    }
}

class VoidFunction1WithThrowsWithDescription<A> implements VoidFunction1WithThrows<A> {
    private String                       description;
    private VoidFunction1WithThrows<A> f;

    public VoidFunction1WithThrowsWithDescription( VoidFunction1WithThrows<A> f, String description ) {
        this.description = description;
        this.f           = f;
    }

    public void invoke( A a ) throws Throwable {
        f.invoke(a);
    }

    public VoidFunction1WithThrows<A> withDescription( String desc ) {
        if ( Objects.equals(this.description, desc) ) {
            return this;
        }

        return new VoidFunction1WithThrowsWithDescription<>( this, desc );
    }
}
