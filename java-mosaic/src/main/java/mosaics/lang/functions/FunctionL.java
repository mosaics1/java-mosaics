package mosaics.lang.functions;

import java.util.Objects;


public interface FunctionL<R> {
    public R invoke( long a1 );

    public default FunctionL<R> withDescription( String desc ) {
        return new FunctionLWithDescription<>( this, desc );
    }
}

class FunctionLWithDescription<R> implements FunctionL<R> {
    private String       description;
    private FunctionL<R> f;

    public FunctionLWithDescription( FunctionL<R> f, String description ) {
        this.description = description;
        this.f           = f;
    }

    public R invoke( long v ) {
        return f.invoke(v);
    }

    public FunctionL<R> withDescription( String desc ) {
        if ( Objects.equals(this.description, desc) ) {
            return this;
        }

        return new FunctionLWithDescription<>( this, desc );
    }
}
