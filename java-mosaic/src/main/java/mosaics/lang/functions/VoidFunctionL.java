package mosaics.lang.functions;

import java.util.Objects;


public interface VoidFunctionL {
    public static VoidFunctionL NO_OP =  a -> {};

    public void invoke( long a );

    public default VoidFunctionL withDescription( String desc ) {
        return new VoidFunctionLWithDescription( this, desc );
    }
}

class VoidFunctionLWithDescription implements VoidFunctionL {
    private String       description;
    private VoidFunctionL f;

    public VoidFunctionLWithDescription( VoidFunctionL f, String description ) {
        this.description = description;
        this.f           = f;
    }

    public void invoke( long a ) {
        f.invoke(a);
    }

    public VoidFunctionL withDescription( String desc ) {
        if ( Objects.equals(this.description, desc) ) {
            return this;
        }

        return new VoidFunctionLWithDescription( this, desc );
    }
}
