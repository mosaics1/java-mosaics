package mosaics.lang.functions;

public interface DoubleFunctionID {
    public double invoke( int v1, double v2 );
}
