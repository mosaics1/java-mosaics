package mosaics.lang.functions;

import java.util.Objects;


public interface VoidFunctionD {
    public static VoidFunctionD NO_OP =  a -> {};

    public void invoke( double a );

    public default VoidFunctionD withDescription( String desc ) {
        return new VoidFunctionDWithDescription( this, desc );
    }
}

class VoidFunctionDWithDescription implements VoidFunctionD {
    private String        description;
    private VoidFunctionD f;

    public VoidFunctionDWithDescription( VoidFunctionD f, String description ) {
        this.description = description;
        this.f           = f;
    }

    public void invoke( double a ) {
        f.invoke(a);
    }

    public VoidFunctionD withDescription( String desc ) {
        if ( Objects.equals(this.description, desc) ) {
            return this;
        }

        return new VoidFunctionDWithDescription( this, desc );
    }
}
