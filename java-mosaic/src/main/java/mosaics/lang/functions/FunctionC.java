package mosaics.lang.functions;


import java.util.Objects;


public interface FunctionC<T> {
    public T invoke( char c );

    public default FunctionC<T> withDescription( String desc ) {
        return new FunctionCWithDescription<>( this, desc );
    }
}

class FunctionCWithDescription<T> implements FunctionC<T> {
    private String       description;
    private FunctionC<T> f;

    public FunctionCWithDescription( FunctionC<T> f, String description ) {
        this.description = description;
        this.f           = f;
    }

    public T invoke( char c ) {
        return f.invoke(c);
    }

    public FunctionC<T> withDescription( String desc ) {
        if ( Objects.equals(this.description, desc) ) {
            return this;
        }

        return new FunctionCWithDescription<>( this, desc );
    }
}
