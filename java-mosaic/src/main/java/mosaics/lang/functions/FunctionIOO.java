package mosaics.lang.functions;

import java.util.Objects;


public interface FunctionIOO<B,C,R> {
    public R invoke( int arg1, B arg2, C arg3 );

    public default FunctionIOO<B,C,R> withDescription( String desc ) {
        return new FunctionIOOWithDescription<>( this, desc );
    }
}

class FunctionIOOWithDescription<B,C,R> implements FunctionIOO<B,C,R> {
    private String             description;
    private FunctionIOO<B,C,R> f;

    public FunctionIOOWithDescription( FunctionIOO<B,C,R> f, String description ) {
        this.description = description;
        this.f           = f;
    }

    public R invoke( int a, B b, C c ) {
        return f.invoke(a,b,c);
    }

    public FunctionIOO<B,C,R> withDescription( String desc ) {
        if ( Objects.equals(this.description, desc) ) {
            return this;
        }

        return new FunctionIOOWithDescription<>( this, desc );
    }
}
