package mosaics.lang.functions;

import lombok.Value;
import mosaics.fp.FP;
import mosaics.fp.collections.FPOption;

import java.util.Objects;


public interface Factory<R> {
    public static <T> Factory<T> of( T value ) {
        return () -> value;
    }

    public R invoke();

    public default Factory<R> withDescription( String desc ) {
        if ( desc == null || desc.trim().length() == 0 ) {
            return this;
        }

        return new FactoryWithDescription<>( FP.option(desc), this );
    }

    public default FPOption<String> getDescription() {
        return FP.emptyOption();
    }

    public default Function0<R> toFunction() {
        return this::invoke;
    }
}

@Value
class FactoryWithDescription<A1,R> implements Factory<R> {
    private FPOption<String> description;
    private Factory<R>       f;


    public R invoke() {
        return f.invoke();
    }

    public Factory<R> withDescription( String desc ) {
        if ( desc == null || desc.trim().length() == 0 ) {
            return f;
        } else if ( Objects.equals(this.description.get(), desc) ) {
            return this;
        }

        return new FactoryWithDescription<>( FP.option(desc), this );
    }
}
