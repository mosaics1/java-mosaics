package mosaics.lang.functions;

public interface IntFunctionI {
    public int invoke( int v );
}
