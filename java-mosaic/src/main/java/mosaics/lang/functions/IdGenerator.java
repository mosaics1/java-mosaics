package mosaics.lang.functions;

import java.util.Objects;


public interface IdGenerator {

    public long nextId();


    public default IdGenerator withDescription( String desc ) {
        return new IdGeneratorWithDescription( this, desc );
    }
}

class IdGeneratorWithDescription implements IdGenerator {
    private String       description;
    private IdGenerator f;

    public IdGeneratorWithDescription( IdGenerator f, String description ) {
        this.description = description;
        this.f           = f;
    }

    public long nextId() {
        return f.nextId();
    }

    public IdGenerator withDescription( String desc ) {
        if ( Objects.equals(this.description, desc) ) {
            return this;
        }

        return new IdGeneratorWithDescription( this, desc );
    }
}
