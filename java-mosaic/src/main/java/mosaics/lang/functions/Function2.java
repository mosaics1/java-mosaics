package mosaics.lang.functions;

import java.util.Objects;


public interface Function2<A,B,R> {
    public R invoke( A a, B b );

    public default boolean coversArg(A a, B b) {
        return true;
    }

    public default Function2<A,B,R> withDescription( String desc ) {
        return new Function2WithDescription<>( this, desc );
    }
}

class Function2WithDescription<A,B,R> implements Function2<A,B,R> {
    private String          description;
    private Function2<A,B,R> f;

    public Function2WithDescription( Function2<A,B,R> f, String description ) {
        this.description = description;
        this.f           = f;
    }

    public R invoke( A a, B b ) {
        return f.invoke(a,b);
    }

    public Function2<A,B,R> withDescription( String desc ) {
        if ( Objects.equals(this.description, desc) ) {
            return this;
        }

        return new Function2WithDescription<>( this, desc );
    }
}
