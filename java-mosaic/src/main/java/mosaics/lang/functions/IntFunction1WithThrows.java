package mosaics.lang.functions;

import java.util.Objects;


public interface IntFunction1WithThrows<A> {
    public int invoke( A a ) throws Exception;

    public default IntFunction1WithThrows<A> withDescription( String desc ) {
        return new IntFunction1WithThrowsWithDescription<>( this, desc );
    }
}

class IntFunction1WithThrowsWithDescription<A> implements IntFunction1WithThrows<A> {
    private String                    description;
    private IntFunction1WithThrows<A> f;

    public IntFunction1WithThrowsWithDescription( IntFunction1WithThrows<A> f, String description ) {
        this.description = description;
        this.f           = f;
    }

    public int invoke( A a ) throws Exception {
        return f.invoke(a);
    }

    public IntFunction1WithThrows<A> withDescription( String desc ) {
        if ( Objects.equals(this.description, desc) ) {
            return this;
        }

        return new IntFunction1WithThrowsWithDescription<>( this, desc );
    }
}
