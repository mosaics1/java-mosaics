package mosaics.lang.functions;

public interface VoidFunctionCWithThrows {
    public void invoke(char v) throws Throwable;
}
