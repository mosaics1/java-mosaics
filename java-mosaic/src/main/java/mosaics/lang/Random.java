package mosaics.lang;

import mosaics.lang.clock.Clock;

import java.security.SecureRandom;
import java.util.Objects;
import java.util.UUID;


/**
 * Generates random data.  The random numbers can be either predictable or not.  Specify a seed
 * if you want to be able to predict or reproduce the same sequence.  Do not specify a seed if
 * you want some pretty freaking random data.
 */
public class Random {
    private java.util.Random rnd;

    /**
     * Create an instance of Random that is pretty freaking random.
     */
    public Random() {
        this( new SecureRandom() );
    }

    /**
     * Create an instance of random that is seeded from the specified clock.  The seed will start
     * the random sequence from a random place, but then the sequence generated will be predictable.
     */
    public Random( Clock clock ) {
        this( clock.currentTimeNanos() );
    }

    /**
     * Creates an instance of random that is predictable.
     */
    public Random( long seed ) {
        this( new java.util.Random(seed) );
    }

    private Random( java.util.Random rnd ) {
        this.rnd = rnd;
    }


    public int nextInt() {
        return rnd.nextInt();
    }

    public int nextInt( int upperBoundExc ) {
        return rnd.nextInt( upperBoundExc );
    }

    public int nextInt( int lowerBoundInc, int upperBoundExc ) {
        int diff = upperBoundExc - lowerBoundInc;

        return rnd.nextInt(diff) + lowerBoundInc;
    }

    
    public long nextLong() {
        return rnd.nextLong();
    }

    public long nextLong( long upperBoundExc ) {
        return Math.abs(rnd.nextLong()) % upperBoundExc;
    }

    public long nextLong( long lowerBoundInc, long upperBoundExc ) {
        long diff = upperBoundExc - lowerBoundInc;

        return nextLong(diff) + lowerBoundInc;
    }
    

    public double nextDouble( double lowerBoundInc, double upperBoundExc ) {
        double diff = upperBoundExc - lowerBoundInc;

        return rnd.nextDouble()*diff + lowerBoundInc;
    }

    @SafeVarargs
    public final <T> T nextOneOf( T...values ) {
        int i = nextInt(values.length);

        return values[i];
    }

    @SafeVarargs
    public final <T> T nextOneOfExcluding( T notThisValue, T...values ) {
        int maxAttempts = 10;
        int attemptCount = 0;

        T rndValue;

        do {
            rndValue = nextOneOf(values);
            attemptCount += 1;
        } while ( Objects.equals(notThisValue,rndValue) && attemptCount < maxAttempts );

        if ( Objects.equals(notThisValue,rndValue) ) {
            throw new IllegalStateException( "failed to randomly select a new value" );
        }

        return rndValue;
    }

    public String randomString( int maxLength ) {
        int           len = nextInt( maxLength );
        StringBuilder buf = new StringBuilder( len );

        for ( int i=0; i<len; i++ ) {
            buf.append( randomChar() );
        }

        return buf.toString();
    }

    public char randomChar() {
        return (char) nextInt('0','z'+1);
    }

    public String nextUUID() {
        // code taken from UUID.randomUUID(), and tweaked to use a specified instance of secure random
        // this lets us choose between being cryptographically random and predictably psuedo random.
        byte[] randomBytes = new byte[16];
        rnd.nextBytes(randomBytes);

        randomBytes[6]  &= 0x0f;  /* clear version        */
        randomBytes[6]  |= 0x40;  /* set to version 4     */
        randomBytes[8]  &= 0x3f;  /* clear variant        */
        randomBytes[8]  |= 0x80;  /* set to IETF variant  */

        long msb = 0;
        long lsb = 0;
        for (int i=0; i<8; i++) {
            msb = (msb << 8) | (randomBytes[i] & 0xff);
        }

        for (int i=8; i<16; i++) {
            lsb = (lsb << 8) | (randomBytes[i] & 0xff);
        }

        return new UUID(msb,lsb).toString();
    }
}
