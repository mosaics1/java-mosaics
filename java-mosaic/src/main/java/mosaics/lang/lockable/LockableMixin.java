package mosaics.lang.lockable;

/**
 * Convenience base class for implementing IsLockable.
 */
@SuppressWarnings("unchecked")
public abstract class LockableMixin<T extends LockableMixin<T>> extends CloneMixin<T> implements Lockable<T> {


    protected transient volatile boolean isLocked;



    public T lock() {
        if ( !isLocked ) {
            this.isLocked = true;
            onLock();
        }

        return (T) this;
    }

    public boolean isLocked() {
        return isLocked;
    }

    public boolean isUnlocked() {
        return !isLocked;
    }

    @SuppressWarnings({"unchecked"})
    public T cloneUnlocked() {
        T copy = super.clone();

        copy.isLocked = false;
        copy.onUnlock();

        return copy;
    }

    public boolean isCopyable() {
        return this instanceof java.lang.Cloneable;
    }

    protected void onLock() {}
    protected void onUnlock() {}



    protected void throwIfLocked() {
        throwIfLocked( this );
    }

    /**
     * Call from every mutating method.  This will prevent mutations when the
     * object has been locked.
     */
    protected void throwIfNotLocked() {
        throwIfUnlocked( this );
    }

}
