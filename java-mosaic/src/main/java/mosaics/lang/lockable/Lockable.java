package mosaics.lang.lockable;

import java.util.Map;
import java.util.Optional;


/**
 * Interface representing the lockable object pattern.  Objects that implement this interface
 * start life as being mutable allowing easy configuration.  However before sharing the object,
 * it can be locked.  Which prevents any further modifications to the object.<p/>
 *
 * Requires every method that could mutate state to verify that the object is currently unlocked
 * before proceeding.
 */
public interface Lockable<T extends Lockable<T>> {

    public T lock();
    public boolean isLocked();
    public boolean isUnlocked();

    /**
     * Returns true if the object supports being copied.
     *
     * @see #cloneUnlocked()
     */
    public boolean isCopyable();

    /**
     * Creates a copy of this object that is ready for modification.
     */
    public T cloneUnlocked();





    /**
     * Lock every object within the specified collection.
     */
    public default <T extends Lockable> void lockAll( Iterable<T> collection ) {
        for ( T v : collection ) {
            v.lock();
        }
    }

    public default <T extends Lockable> void lockAll( Optional<T> option ) {
        option.ifPresent( Lockable::lock );
    }

    /**
     * Conditionally lock the specified object, handling the null case gracefully.
     */
    public default <T extends Lockable> void lockNbl( T o ) {
        if ( o != null ) {
            o.lock();
        }
    }

    /**
     * Lock every value in the specified map.
     */
    public default <T extends Lockable> void lockAll( Map<?,T> map) {
        for ( T v : map.values() ) {
            v.lock();
        }
    }

    /**
     * Throw an exception if the specified object is mutable.
     */
    public default void throwIfUnlocked( Lockable o ) {
        if ( o.isUnlocked() ) {
            throw new IllegalStateException( o + " is unlocked" );
        }
    }

    /**
     * Throw an exception if the specified object is immutable.
     */
    public default void throwIfLocked( Lockable o ) {
        if ( o.isLocked() ) {
            throw new IllegalStateException( o + " is locked" );
        }
    }
}
