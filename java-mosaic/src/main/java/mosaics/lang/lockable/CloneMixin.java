package mosaics.lang.lockable;

import mosaics.lang.Backdoor;

import java.io.Serializable;


public abstract class CloneMixin<T> implements Serializable, java.lang.Cloneable {

    @SuppressWarnings("unchecked")
    public T clone() {
        try {
            return (T) super.clone();
        } catch ( CloneNotSupportedException ex ) {
            throw Backdoor.throwException(ex);
        }
    }

}
