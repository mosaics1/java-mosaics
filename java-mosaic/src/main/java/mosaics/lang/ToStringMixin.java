package mosaics.lang;

import java.io.IOException;


/**
 * A ToString mixin interface.  Standardises the pattern for adding toString(Appendable)
 * to classes.
 */
public interface ToStringMixin {
    public void toString( Appendable out ) throws IOException;

    public default void toStringSilent( Appendable out ) {
        try {
            toString(out);
        } catch ( IOException ex ) {
            throw Backdoor.throwException( ex );
        }
    };

    /**
     * I wish that Java let us override Objects.toString() from here.
     *
     * Instead every class has to add this code:
     *
     * <code>
     *     public String toString() {
     *         return toStringWithStringBuilder();
     *     }
     * </code>>
     */
    public default String toStringWithStringBuilder() {
        StringBuilder buf = new StringBuilder(200);

        toStringSilent(buf);

        return buf.toString();
    }
}
