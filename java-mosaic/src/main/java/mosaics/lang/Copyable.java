package mosaics.lang;

import lombok.SneakyThrows;
import mosaics.lang.reflection.JavaClass;

import static mosaics.lang.Backdoor.cast;


/**
 * Makes the objects protected clone() method available via a public copy() method.
 */
public interface Copyable<T extends Cloneable> extends Cloneable {
    @SneakyThrows
    public default T copy() {
        // it is a sad state of affairs that one has to use reflection here
        return cast( JavaClass.of(this).getMethod( "clone" ).get().invokeAgainst(this) );
    }
}
