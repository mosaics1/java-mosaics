package mosaics.lang.clock;

import mosaics.lang.time.DTM;


public class Clocks {
    public static Clock newRealTimeClock() {
        return RealTimeClock.INSTANCE;
    }

    public static Clock newFixedTimeClock( DTM dtm ) {
        return new FixedTimeClock( dtm.getMillisSinceEpoch() );
    }

    public static Clock newFixedTimeClock( long epochMillis ) {
        return new FixedTimeClock(epochMillis);
    }

    public static Clock newOffsetClock( DTM initialDTM ) {
        return new OffsetClock( initialDTM.getMillisSinceEpoch() );
    }

    public static Clock newOffsetClock( long startTimeMillis ) {
        return new OffsetClock( startTimeMillis );
    }
}
