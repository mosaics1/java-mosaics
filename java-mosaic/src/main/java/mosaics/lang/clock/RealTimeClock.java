package mosaics.lang.clock;


public class RealTimeClock implements Clock {
    public static final Clock INSTANCE = new RealTimeClock();

    private RealTimeClock() {}

    public long currentTimeMillis() {
        return System.currentTimeMillis();
    }

    public long currentTimeNanos() {
        return System.nanoTime();
    }

    public boolean isModifiable() {
        return false;
    }

    public long incMillis( long millis ) {
        throw new UnsupportedOperationException();
    }

    public void setMillis( long millisSinceEpoch ) {
        throw new UnsupportedOperationException();
    }
}
