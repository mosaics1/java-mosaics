package mosaics.lang;

import mosaics.lang.functions.Function0;
import mosaics.lang.functions.Function0WithThrows;
import mosaics.lang.functions.VoidFunction0WithThrows;

import java.lang.reflect.InvocationTargetException;


public class TryUtils {

    public static void invokeAndIgnoreException( Function0WithThrows f ) {
        try {
            f.invoke();
        } catch ( Throwable ex ) {
            // ignored - no need to log or rethrow
        }
    }

    public static <T> T invokeAndIgnoreExceptionR( Function0<T> f ) {
        try {
            return f.invoke();
        } catch ( Throwable ex ) {
            // ignored - no need to log or rethrow
            return null;
        }
    }

    public static void invokeAndIgnoreException( VoidFunction0WithThrows f ) {
        try {
            f.invoke();
        } catch ( Throwable ex ) {
            // ignored - no need to log or rethrow
        }
    }

    public static <R> R invokeAndRethrowException( Function0WithThrows<R> f ) {
        try {
            return f.invoke();
        } catch ( InvocationTargetException ex ) {
            throw Backdoor.throwException( ex.getTargetException() );
        } catch ( Throwable ex ) {
            throw Backdoor.throwException(ex);
        }
    }

    public static void invokeAndRethrowException( VoidFunction0WithThrows f ) {
        try {
            f.invoke();
        } catch ( InvocationTargetException ex ) {
            throw Backdoor.throwException( ex.getTargetException() );
        } catch ( Throwable ex ) {
            throw Backdoor.throwException( ex );
        }
    }

    /**
     * Used to work around compiler issues.
     *
     * Sometimes we silence compile time exceptions, and sometimes we want to unsilence
     * them.
     */
    public static <T extends Throwable> void fakeExceptionThrow( Class<T> exceptionType ) throws T {

    }
}
