package mosaics.lang.reflection;

import java.lang.reflect.InvocationTargetException;


public class ReflectionException extends RuntimeException {
    public static RuntimeException recast( String msg, Throwable e ) {
        return new ReflectionException( msg, e );
    }

    public static RuntimeException create( String msg ) {
        return new ReflectionException( msg, null  );
    }

    public static RuntimeException recast( Throwable e ) {
        if ( e instanceof RuntimeException ) {
            return (RuntimeException) e;
        } else if ( e instanceof InvocationTargetException ) {
            return recast( e.getCause() );
        } else {
            return new ReflectionException( e );
        }
    }

    public ReflectionException( String msg ) {
        this( msg, null );
    }

    private ReflectionException( Throwable e ) {
        super( e );
    }

    private ReflectionException( String msg, Throwable e ) {
        super( msg, e );
    }
}
