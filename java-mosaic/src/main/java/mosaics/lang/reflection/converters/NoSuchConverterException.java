package mosaics.lang.reflection.converters;

import mosaics.lang.reflection.JavaClass;


public class NoSuchConverterException extends RuntimeException {
    public NoSuchConverterException( JavaClass from, JavaClass to ) {
        super( "Unable to find a TypeConverter that converts "+from.getFullName()+" to "+to.getFullName() );
    }
}
