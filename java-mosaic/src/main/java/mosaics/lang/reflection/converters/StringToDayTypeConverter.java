package mosaics.lang.reflection.converters;

import mosaics.fp.FP;
import mosaics.fp.Try;
import mosaics.fp.Tryable;
import mosaics.i18n.I18nContext;
import mosaics.lang.reflection.JavaClass;
import mosaics.lang.time.Day;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static mosaics.strings.StringUtils.isBlank;


public class StringToDayTypeConverter implements TypeConverter<String, Day> {
    public static final StringToDayTypeConverter INSTANCE = new StringToDayTypeConverter();

    public JavaClass getSourceType() {
        return JavaClass.STRING;
    }

    public JavaClass getDestType() {
        return JavaClass.DAY;
    }

    public Tryable<Day> convert( I18nContext ctx, String value ) {
        if ( isBlank(value) ) {
            return Try.failed( new ParseException(value,0) );
        }

        return Try.invoke( () -> format(ctx,value) );
    }

    public Tryable<String> reverse( I18nContext ctx, Day value ) {
        Day day = FP.getOrElse( value, Day.EPOCH );

        LocalDate when = LocalDate.ofEpochDay(day.getEpochDays());

        return Try.invoke( () -> ctx.getDateFormatter().format(when) );
    }

    private Day format( I18nContext ctx, String value ) {
        DateTimeFormatter formatter = ctx.getDateFormatter();

        return new Day( LocalDate.parse(value,formatter) );
    }
}
