package mosaics.lang.reflection.converters;

import lombok.AllArgsConstructor;
import lombok.Value;
import mosaics.fp.Try;
import mosaics.fp.Tryable;
import mosaics.i18n.I18nContext;
import mosaics.lang.reflection.JavaClass;
import mosaics.strings.StringCodec;

import java.text.ParseException;


@Value
@AllArgsConstructor
public class StringCodecTypeConverter<T> implements TypeConverter<String, T> {

    private StringCodec<T> codec;
    private JavaClass      destType;


    public StringCodecTypeConverter( StringCodec<T> codec ) {
        this( codec, codec.getType() );
    }


    public JavaClass getSourceType() {
        return JavaClass.STRING;
    }

    public JavaClass getDestType() {
        return destType;
    }

    public Tryable<T> convert( I18nContext ctx, String value ) {
        if ( isBlank( value ) ) {
//            if ( destType.hasDefaultValue() ) {
//                return Try.succeeded( destType.getDefaultValue().get() );
//            } else
            if ( destType.isNullable() ) {
                return Try.succeeded( null );
            } else {
                return Try.failed( new ParseException(value,0) );
            }
        }

        return codec.decode( value );
    }

    public Tryable<String> reverse( I18nContext ctx, T value ) {
        return Try.succeeded( codec.encode(value) );
    }

    private boolean isBlank( String value ) {
        return value == null || value.trim().length() == 0;
    }

}
