package mosaics.lang.reflection.converters;

import mosaics.fp.Tryable;
import mosaics.i18n.I18nContext;
import mosaics.lang.reflection.JavaClass;

import static mosaics.lang.Backdoor.cast;


/**
 * Knows how to 'lift' a value into a container class.   That is W must be a type that takes one
 * generic value that is of type T.  For example Optional&lt;T&gt;, List&lt;T&gt; or Future&lt;T&gt;.
 */
public interface TypeWrapper<T,W> {

    public default JavaClass getElementType() {
        return cast( getWrappedType().getClassGenerics().firstOrNull() );
    }

    public JavaClass getWrappedType();

    /**
     * Wrap the specified value.  For example String becomes Optional&lt;String&gt;.
     */
    public Tryable<W> wrap( T value );

    /**
     * Unwrap the specified value.  For example Optional&lt;String&gt; becomes String.
     */
    public Tryable<T> unwrap( W value );


    public default TypeConverter<T,W> toConverter() {
        return new TypeConverter<T,W>() {
            public JavaClass getSourceType() {
                return getElementType();
            }

            public JavaClass getDestType() {
                return getWrappedType();
            }

            public Tryable<W> convert( I18nContext ctx, T value ) {
                return wrap( value );
            }

            public Tryable<T> reverse( I18nContext ctx, W value ) {
                return unwrap( value );
            }
        };
    }
}
