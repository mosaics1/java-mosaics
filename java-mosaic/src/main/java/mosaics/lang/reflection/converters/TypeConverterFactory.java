package mosaics.lang.reflection.converters;

import mosaics.fp.FP;
import mosaics.fp.Try;
import mosaics.fp.Tryable;
import mosaics.fp.collections.FPList;
import mosaics.fp.collections.FPOption;
import mosaics.i18n.I18nContext;
import mosaics.lang.functions.Function1;
import mosaics.lang.functions.Function2;
import mosaics.lang.reflection.JavaClass;
import mosaics.strings.StringCodec;
import mosaics.strings.StringCodecs;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import static mosaics.lang.Backdoor.cast;


@SuppressWarnings("unchecked")
public class TypeConverterFactory {

    /**
     * Primary TypeConverterCache.
     */
    private static final Map<JavaClass,Map<JavaClass,FPOption<TypeConverter>>> converterCache   = new ConcurrentHashMap<>();

    /**
     * Special case cache used to handle container (aka wrapper) classes such as FPList,
     * Optional etc.
     */
    private static final Map<Class,Function1<JavaClass,TypeWrapper>>           wrapperFactories = new ConcurrentHashMap<>();


    private static final FPList<Function2<JavaClass,JavaClass,FPOption<TypeConverter>>> typeConverterSearchPath = FP.toList(
        EnumStringTypeConverter::createEnumTypeConverterFor,
        DefaultStringConstructorTypeConverter::createDefaultConverterFor
    );

    static {
        registerStringCodecConverters(
            JavaClass.BOOLEAN_OBJECT,      JavaClass.BOOLEAN_PRIMITIVE,
            JavaClass.SHORT_PRIMITIVE,     JavaClass.SHORT_OBJECT,
            JavaClass.INTEGER_PRIMITIVE,   JavaClass.INTEGER_OBJECT,
            JavaClass.LONG_PRIMITIVE,      JavaClass.LONG_OBJECT,
            JavaClass.FLOAT_PRIMITIVE,     JavaClass.FLOAT_OBJECT,
            JavaClass.DOUBLE_PRIMITIVE,    JavaClass.DOUBLE_OBJECT,
            JavaClass.CHARACTER_PRIMITIVE, JavaClass.CHARACTER_OBJECT
        );

        wrapperFactories.put( Optional.class, JdkOptionalTypeWrapper::new );
        wrapperFactories.put( FPOption.class, FPOptionTypeWrapper::new );

        registerTypeConverter( StringToSecretConverter.INSTANCE );
        registerTypeConverter( StringToSecretConverter.INSTANCE.reverse() );

        registerTypeConverter( StringToDTMTypeConverter.INSTANCE );
        registerTypeConverter( StringToDTMTypeConverter.INSTANCE.reverse() );

        registerTypeConverter( StringToDayTypeConverter.INSTANCE );
        registerTypeConverter( StringToDayTypeConverter.INSTANCE.reverse() );
    }

    private static void registerStringCodecConverters( JavaClass...classes ) {
        FP.wrapArray(classes).forEach( TypeConverterFactory::registerStringCodecConverter );
    }


    public static <S,D> void registerTypeConverter( TypeConverter<S,D> typeConverter ) {
        converterCache
            .computeIfAbsent( typeConverter.getSourceType(), key -> new ConcurrentHashMap<>() )
            .put( typeConverter.getDestType(), FP.option(typeConverter) );
    }

    public static <S,D> Tryable<D> convert( I18nContext ctx, S sourceValue, JavaClass sourceType, JavaClass destinationType ) {
        FPOption<TypeConverter<S,D>> converterOpt = getConverterFor( sourceType, destinationType );

        return converterOpt.map( c -> c.convert(ctx,sourceValue) )
            .orElse( () -> Try.failed(new NoSuchConverterException(sourceType, destinationType)) );
    }

    public static <S,D> FPOption<TypeConverter<S, D>> getConverterFor( JavaClass from, JavaClass to ) {
        return cast(
            converterCache.computeIfAbsent(from, key -> new ConcurrentHashMap<>())
                .computeIfAbsent( to, key -> createConverterFor(from,to) )
        );
    }


    private static <T> void registerStringCodecConverter( JavaClass type ) {
        StringCodec<T>              codec     = StringCodecs.DEFAULT_CODECS.getCodecFor(type).get();
        StringCodecTypeConverter<T> converter = new StringCodecTypeConverter<>( codec );

        registerTypeConverter( converter );
        registerTypeConverter( converter.reverse() );
    }

    private static FPOption<TypeWrapper> getTypeWrapperFor( JavaClass type ) {
        if ( type.getClassGenerics().count() == 1 ) {
            return FP.option( wrapperFactories.get(type.getJdkClass()) ).map(f -> f.invoke(type) );
        } else {
            return FP.emptyOption();
        }
    }

    private static FPOption<TypeConverter> createConverterFor( JavaClass from, JavaClass to ) {
        return getTypeWrapperFor(to).map( TypeWrapper::toConverter )
            .or( () -> getTypeWrapperFor(from).map(TypeWrapper::toConverter).map(TypeConverter::reverse) )
            .or( () -> NoOpConverter.tryToCreateCompatibleConverterFor(from,to) )
            .or( () -> scanSearchPathFor(from,to) )
            .or( () -> scanSearchPathFor(to,from).map(TypeConverter::reverse) );
    }

    private static FPOption<TypeConverter> scanSearchPathFor( JavaClass from, JavaClass to ) {
        return typeConverterSearchPath.firstOpt( f -> f.invoke(from,to) );
    }

}
