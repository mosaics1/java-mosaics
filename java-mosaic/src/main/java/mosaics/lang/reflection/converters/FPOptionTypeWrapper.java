package mosaics.lang.reflection.converters;


import mosaics.fp.FP;
import mosaics.fp.Try;
import mosaics.fp.Tryable;
import mosaics.fp.collections.FPOption;
import mosaics.lang.reflection.JavaClass;
import lombok.Value;


@Value
public class FPOptionTypeWrapper<T> implements TypeWrapper<T,FPOption<T>> {

    private JavaClass wrappedType;


    public Tryable<FPOption<T>> wrap( T value ) {
        return Try.succeeded( FP.option(value) );
    }

    public Tryable<T> unwrap( FPOption<T> value ) {
        if ( value == null || value.isEmpty() ) {
            if ( getElementType().isNullable() ) {
                return Try.succeeded( null );
            } else {
                return Try.failed( "%s is not nullable", getElementType() );
            }
        }

        return Try.succeeded( value.get() );
    }

}
