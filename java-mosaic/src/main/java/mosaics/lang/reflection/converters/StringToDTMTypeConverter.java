package mosaics.lang.reflection.converters;

import lombok.EqualsAndHashCode;
import mosaics.fp.FP;
import mosaics.fp.Try;
import mosaics.fp.Tryable;
import mosaics.i18n.I18nContext;
import mosaics.lang.reflection.JavaClass;
import mosaics.lang.time.DTM;

import java.text.ParseException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static mosaics.strings.StringUtils.isBlank;


@EqualsAndHashCode
public class StringToDTMTypeConverter implements TypeConverter<String, DTM> {
    public static final StringToDTMTypeConverter INSTANCE = new StringToDTMTypeConverter();

    public JavaClass getSourceType() {
        return JavaClass.STRING;
    }

    public JavaClass getDestType() {
        return JavaClass.DTM;
    }

    public Tryable<DTM> convert( I18nContext ctx, String value ) {
        if ( isBlank(value) ) {
            return Try.failed( new ParseException(value,0) );
        }

        return Try.invoke( () -> format(ctx,value) );
    }

    public Tryable<String> reverse( I18nContext ctx, DTM value ) {
        DTM dtm = FP.getOrElse( value, DTM.EPOCH );

        LocalDateTime when = LocalDateTime.ofInstant(
            Instant.ofEpochMilli(dtm.getMillisSinceEpoch()),
            ctx.getZoneId()
        );

        return Try.invoke( () -> ctx.getDtmFormatter().format(when) );
    }

    private DTM format( I18nContext ctx, String value ) {
        DateTimeFormatter formatter = ctx.getDtmFormatter();

        return new DTM( formatter.parse(value) );
    }
}
