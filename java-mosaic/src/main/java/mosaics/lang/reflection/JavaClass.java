package mosaics.lang.reflection;

import lombok.SneakyThrows;
import mosaics.fp.FP;
import mosaics.fp.collections.FPIterable;
import mosaics.fp.collections.FPIterator;
import mosaics.fp.collections.maps.FPMap;
import mosaics.fp.collections.FPOption;
import mosaics.fp.collections.Tuple2;
import mosaics.lang.Backdoor;
import mosaics.lang.TryUtils;
import mosaics.lang.functions.Factory;
import mosaics.lang.reflection.fetcher.DefaultValueRegistry;
import mosaics.lang.time.DTM;
import mosaics.lang.time.Day;
import mosaics.utils.MapUtils;
import mosaics.utils.SetUtils;

import java.io.File;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;
import java.time.Duration;
import java.util.Arrays;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.Future;
import java.util.function.Supplier;

import static mosaics.lang.reflection.ReflectionUtils.toClass;


@SuppressWarnings({"rawtypes", "unchecked"})
public class JavaClass {
    private static final Set<Class> BASE_TYPES = SetUtils.asImmutableSet(
        Boolean.class, Byte.class, Short.class, Integer.class, Long.class, Float.class, Double.class, Character.class, String.class,
        boolean.class, byte.class, short.class, int.class, long.class, float.class, double.class, char.class
    );

    public static final JavaClass OBJECT              = of( Object.class );
    public static final JavaClass BOOLEAN_OBJECT      = of( Boolean.class );
    public static final JavaClass BOOLEAN_PRIMITIVE   = of( Boolean.TYPE );
    public static final JavaClass BYTE_OBJECT         = of( Byte.class );
    public static final JavaClass BYTE_PRIMITIVE      = of( Byte.TYPE );
    public static final JavaClass SHORT_OBJECT        = of( Short.class );
    public static final JavaClass SHORT_PRIMITIVE     = of( Short.TYPE );
    public static final JavaClass INTEGER_OBJECT      = of( Integer.class );
    public static final JavaClass INTEGER_PRIMITIVE   = of( Integer.TYPE );
    public static final JavaClass LONG_OBJECT         = of( Long.class );
    public static final JavaClass LONG_PRIMITIVE      = of( Long.TYPE );
    public static final JavaClass FLOAT_OBJECT        = of( Float.class );
    public static final JavaClass FLOAT_PRIMITIVE     = of( Float.TYPE );
    public static final JavaClass DOUBLE_OBJECT       = of( Double.class );
    public static final JavaClass DOUBLE_PRIMITIVE    = of( Double.TYPE );
    public static final JavaClass CHARACTER_OBJECT    = of( Character.class );
    public static final JavaClass CHARACTER_PRIMITIVE = of( Character.TYPE );
    public static final JavaClass DURATION            = of( Duration.class );
    public static final JavaClass STRING              = of( String.class );
    public static final JavaClass FILE                = of( File.class );
    public static final JavaClass VOID                = of( Void.class );
    public static final JavaClass JDK_FUTURE          = of( Future.class );
    public static final JavaClass THROWABLE           = of( Throwable.class );
    public static final JavaClass COMPARABLE          = of( Comparable.class );
    public static final JavaClass DTM                 = of( DTM.class );
    public static final JavaClass DAY                 = of( Day.class );


    private static final Map<Class,Tuple2<Class,Class>> NUMBER_TYPES = MapUtils.asMap(
        Byte.class,          new Tuple2<>(Byte.class,Byte.TYPE),
        Byte.TYPE,           new Tuple2<>(Byte.class,Byte.TYPE),

        Short.class,         new Tuple2<>(Short.class, Short.TYPE),
        Short.TYPE,          new Tuple2<>(Short.class, Short.TYPE),

        Character.class,     new Tuple2<>(Character.class, Character.TYPE),
        Character.TYPE,      new Tuple2<>(Character.class, Character.TYPE),

        Integer.class,       new Tuple2<>(Integer.class, Integer.TYPE),
        Integer.TYPE,        new Tuple2<>(Integer.class, Integer.TYPE),

        Long.class,          new Tuple2<>(Long.class,   Long.TYPE),
        Long.TYPE,           new Tuple2<>(Long.class,   Long.TYPE),

        Float.class,         new Tuple2<>(Long.class,   Long.TYPE),
        Float.TYPE,          new Tuple2<>(Float.class,  Float.TYPE),

        Double.class,        new Tuple2<>(Long.class,   Long.TYPE),
        Double.class,        new Tuple2<>(Double.class, Double.class)
    );

    private static final FPMap<String, JavaClass> CLASS_BY_NAME_MAP = FP.toMap(
        "bool", BOOLEAN_PRIMITIVE,
        "boolean", BOOLEAN_PRIMITIVE,
        "Boolean", BOOLEAN_OBJECT,
        "byte", BYTE_PRIMITIVE,
        "Byte", BYTE_OBJECT,
        "short", SHORT_PRIMITIVE,
        "Short", SHORT_OBJECT,
        "char", CHARACTER_PRIMITIVE,
        "Character", CHARACTER_OBJECT,
        "int", INTEGER_PRIMITIVE,
        "Integer", INTEGER_OBJECT,
        "long", LONG_PRIMITIVE,
        "Long", LONG_OBJECT,
        "float", FLOAT_PRIMITIVE,
        "Float", FLOAT_OBJECT,
        "double", DOUBLE_PRIMITIVE,
        "Double", DOUBLE_OBJECT,
        "String", STRING,

        "millis", LONG_PRIMITIVE,
        "gbp", LONG_PRIMITIVE
    );

    public static JavaClass forName( String type ) {
        return CLASS_BY_NAME_MAP.get(type).orElse( () -> of(TryUtils.invokeAndRethrowException(() -> Class.forName(type))) );
    }

    public static JavaClass of( Object t ) {
        return of(t.getClass());
    }

    public static JavaClass of( Class t ) {
        return new JavaClass(t, Collections.emptyMap());
    }

    public static JavaClass of( ParameterizedType t, Map<String,JavaClass> reifiedGenerics ) {
        return new JavaClass(t, reifiedGenerics);
    }

    public static JavaClass of( Type type ) {
        return of( type, Map.of() );
    }

    public static JavaClass of( Class type, Class...typeParameters ) {
        if (typeParameters.length == 0) {
            return of(type);
        } else {
            return of(
                new ParameterizedType() {
                    public Type[] getActualTypeArguments() {
                        return typeParameters;
                    }

                    public Type getRawType() {
                        return type;
                    }

                    public Type getOwnerType() {
                        return null;
                    }
                }
            );
        }
    }

    public static JavaClass of( Type type, Map<String,JavaClass> reifiedGenerics ) {
        if ( type instanceof ParameterizedType t) {
            return of( t, reifiedGenerics );
        } else if ( type instanceof WildcardType t ) {
            if ( t.getLowerBounds().length > 0) {
                return of( t.getLowerBounds()[0], reifiedGenerics );
            } else {
                return of( t.getUpperBounds()[0], reifiedGenerics );
            }
        } else if ( type instanceof Class t ) {
            return of( t );
        } else if ( type instanceof TypeVariable t ) {
            return Optional.ofNullable( reifiedGenerics.get(t.getName()) )
                .orElse(JavaClass.OBJECT );
        } else {
            throw new UnsupportedOperationException("Unknown type "+type.getClass()+" ("+type+")");
        }
    }

    private final Class                  clazz;
    private final Map<String, JavaClass> generics;

    private JavaClass( ParameterizedType type, Map<String,JavaClass> reifiedGenerics ) {
        this(toClass(type), extractGenericsFrom(type,reifiedGenerics));
    }

    private JavaClass( Class clazz, Map<String, JavaClass> generics ) {
        this.clazz    = clazz;
        this.generics = generics;
    }

    public String getShortName() {
        return getJdkClass().getSimpleName();
    }

    public Class getJdkClass() {
        return clazz;
    }

    public FPIterable<JavaClass> getClassGenerics() {
        return FP.wrapArray(clazz.getTypeParameters()).map(v -> generics.get(v.getName()));
    }

    @SneakyThrows
    @SuppressWarnings("ConstantConditions")
    public FPOption<JavaField> getField( String fieldName ) {
        try {
            return FP.option( clazz.getDeclaredField( fieldName ) )
                .map( field -> JavaField.of( field, generics ) );
        } catch (NoSuchFieldException ex) {
            return FP.emptyOption();
        }
    }

    public FPIterable<JavaConstructor> getAllConstructors() {
        return FP.wrapArray(clazz.getDeclaredConstructors())
            .map(constructor -> new JavaConstructor(constructor,generics));
    }

    public FPOption<JavaConstructor> getPublicConstructor( Class...argTypes ) {
        return getConstructor( argTypes ).filter( JavaConstructor::isPublic ).toFPOption();
    }

    public FPOption<JavaConstructor> getConstructor( Class...argTypes ) {
        return getAllConstructors()
            .filter( c -> c.getParameterCount() == argTypes.length )
            .filter( c -> Objects.equals(toBaseClassesOnly(c.getParameters()), Arrays.asList(argTypes)) )
            .first();
    }

    /**
     * Returns all methods of any scope.  Public, private, protected and package.
     */
    public FPIterable<JavaMethod> getAllMethods() {
        return walkClassHierarchy().flatMap( JavaClass::getDeclaredMethods );
    }

    public FPIterable<JavaMethod> getDeclaredMethods() {
        return FP.wrapArray(clazz.getDeclaredMethods()).map(method -> new JavaMethodJdk(method,generics));
    }

    public FPIterable<JavaField> getAllFields() {
        return walkClassHierarchy().flatMap( JavaClass::getDeclaredFields );
    }

    public FPIterable<JavaField> getDeclaredFields() {
        return FP.wrapArray(clazz.getDeclaredFields()).map(f -> JavaField.of(f, generics));
    }

    public String toString() {
        StringBuilder buf = new StringBuilder();

        buf.append(clazz.getName());

        if ( clazz.getTypeParameters().length >0 ) {
            buf.append( '<' );

            String separator = "";
            for ( TypeVariable v : clazz.getTypeParameters() ) {
                buf.append( separator );
                buf.append( generics.get( v.getName() ) );

                separator = ", ";
            }

            buf.append( '>' );
        }

        return buf.toString();
    }

    public int hashCode() {
        return Objects.hash(clazz.hashCode(), generics);
    }

    public boolean equals( Object o ) {
        if ( o == null || !(o instanceof JavaClass) ) {
            return false;
        }

        JavaClass other = (JavaClass) o;
        return this.clazz.equals(other.clazz) && this.generics.equals(other.generics);
    }

    public String getFullName() {
        return clazz.getName();
    }

    @SuppressWarnings("rawtypes")
    private static Map<String, JavaClass> extractGenericsFrom( ParameterizedType type, Map<String,JavaClass> reifiedParentGenerics ) {
        Class          clazz         = toClass(type);
        Type[]         generics      = type.getActualTypeArguments();
        TypeVariable[] typeVariables = clazz.getTypeParameters();

        Map<String,JavaClass> extractedGenerics = new HashMap<>();

        for ( int i=0; i<typeVariables.length; i++ ) {
            extractedGenerics.put( typeVariables[i].getTypeName(), JavaClass.of(generics[i],reifiedParentGenerics) );
        }

        return extractedGenerics;
    }

    public<T,O> T cast( O obj ) {
        return Backdoor.cast(obj);
    }

    /**
     * Returns true if v is a primitive, or a wrapped primitive type.
     */
    public boolean isBaseType() {
        return isPrimitive() || BASE_TYPES.contains( getJdkClass() );
    }

    public boolean isNullable() {
        return !isPrimitive() && !isOptional();
    }

    public boolean isAbstract() {
        return Modifier.isAbstract( getJdkClass().getModifiers() );
    }

    public FPIterable<JavaConstructor> allPublicConstructors() {
        return getAllConstructors().filter( JavaExecutable::isPublic );
    }

    public boolean isRecord() {
        return clazz.isRecord();
    }

    public boolean isNotRecord() {
        return !isRecord();
    }

    public boolean isEnum() {
        return ReflectionUtils.isEnum( getJdkClass() );
    }

    public boolean isBoolean() {
        return ReflectionUtils.isBoolean( getJdkClass() );
    }

    public boolean isByte() {
        return ReflectionUtils.isByte( getJdkClass() );
    }

    public boolean isShort() {
        return ReflectionUtils.isShort( getJdkClass() );
    }

    public boolean isInteger() {
        return ReflectionUtils.isInteger( getJdkClass() );
    }

    public boolean isLong() {
        return ReflectionUtils.isLong( getJdkClass() );
    }

    public boolean isFloat() {
        return ReflectionUtils.isFloat( getJdkClass() );
    }

    public boolean isDouble() {
        return ReflectionUtils.isDouble( getJdkClass() );
    }

    public boolean isCharacter() {
        return ReflectionUtils.isCharacter( getJdkClass() );
    }

    public boolean isString() {
        return getJdkClass().isAssignableFrom( String.class );
    }

    public boolean isArray() {
        return ReflectionUtils.isArray( getJdkClass() );
    }

    public boolean isJdkSet() {
        return ReflectionUtils.isJdkSet( getJdkClass() );
    }

    public boolean isJdkList() {
        return ReflectionUtils.isJdkList( getJdkClass() );
    }

    public boolean isJdkMap() {
        return ReflectionUtils.isJdkMap( getJdkClass() );
    }

    public boolean isFPOption() {
        return getJdkClass() == FPOption.class || FPOption.class.isAssignableFrom(getJdkClass());
    }

    public boolean isIterable() {
        return Iterable.class.isAssignableFrom(getJdkClass());
    }

    public boolean isJdkOptional() {
        return getJdkClass() == Optional.class || Optional.class.isAssignableFrom( getJdkClass() );
    }

    public boolean isJdkCollection() {
        return isJdkOptional() || isJdkSet() || isJdkList() || isJdkMap();
    }

    public boolean isInterface() {
        return getJdkClass().isInterface();
    }

    public boolean isPartOfTheCoreJavaLibs() {
        String fqn = getJdkClass().getName();

        return fqn.startsWith("java.") || fqn.startsWith("javax.") || fqn.startsWith("sun.");
    }

    public boolean isOptional() {
        return isFPOption() || isJdkOptional();
    }

    public boolean isPrimitive() {
        return getJdkClass().isPrimitive();
    }

    public boolean isVoid() {
        return getJdkClass().equals(Void.class) || getJdkClass().equals(Void.TYPE);
    }

    public boolean isObject() {
        if ( isArray() ) {
            return getArrayComponentType().orElse( this ).isObject();
        } else {
            return !isPrimitive();
        }
    }

    public boolean isPojo() {
        return !isPartOfTheCoreJavaLibs() && getProperties().hasContents();
    }

    public FPOption<JavaClass> getArrayComponentType() {
        return cast(FP.option(getJdkClass().getComponentType()).map(JavaClass::of));
    }

    public FPOption<JavaMethod> getMethod( String methodName, Class...argTypes ) {
        return getAllMethods()
            .filter( m -> m.getName().equals(methodName) )
            .filter( m -> m.getParameters().count() == argTypes.length )
            .filter( m -> Objects.equals(toBaseClassesOnly(m.getParameters()), Arrays.asList(argTypes)) )
            .first();
    }

    public FPOption<JavaMethod> getFirstMethod( String methodName, int numArgs ) {
        return getAllMethods()
            .filter( m -> m.getName().equals(methodName) )
            .filter( m -> m.getParameters().count() == numArgs )
            .first();
    }

    public <R> FPOption<JavaMethod> getStaticMethod(Class<R> returnType, String methodName, Class...argTypes) {
        return getMethod( methodName, argTypes )
            .filter( JavaExecutable::isStatic )
            .toFPOption();
    }

    public <R> FPOption<JavaMethod> getStaticMethod(String methodName, Class...argTypes) {
        return getMethod( methodName, argTypes )
            .filter( JavaExecutable::isStatic )
            .toFPOption();
    }

    private static FPIterable<Class> toBaseClassesOnly( FPIterable<JavaParameter> parameters ) {
        return parameters
            .map( p -> p.getType().getJdkClass() );
    }

    public JavaClass getComponentType() {
        return JavaClass.of( getJdkClass().getComponentType() ); // NB arrays do not support generics
    }

    public Object newArray( int arrayLength, Supplier componentFactory ) {
        return ReflectionUtils.newArray( getJdkClass(), arrayLength, componentFactory );
    }

    @SneakyThrows
    public<T> T newInstance() {
        return Backdoor.cast( getJdkClass().getDeclaredConstructor().newInstance() );
    }

    public boolean isInstanceOf(Class targetClass) {
        return clazz.isAssignableFrom( targetClass );
    }

    /**
     * Create a new instance of the object without invoking the constructor.
     */
    public <T> T allocateInstance() {
        return Backdoor.cast(Backdoor.allocateInstance(getJdkClass()));
    }

    public boolean hasZeroArgConstructor() {
        return getAllConstructors().first(c -> c.getParameterCount() == 0).hasValue();
    }

    public <B extends Enum<B>> List<B> getEnumValues() {
        return cast( Arrays.asList(getJdkClass().getEnumConstants()) );
    }

    public<T,D> FPOption<JavaProperty<T,D>> getProperty( String propertyName ) {
        return Backdoor.cast( JavaProperty.getPropertyFor(this, propertyName) );
    }

    public FPOption<JavaProperty> walkPropertyPath( String propertyPath ) {
        return FP.wrapArray( propertyPath.split("\\.") )
            .foldWhile( FP.option(JavaProperty.wrap(this,"this")), (soFar,nextProperty) -> soFar.getProperty(nextProperty) );
    }

    public<T,D> JavaProperty<T,D> getPropertyMandatory( String propertyName ) {
        return Backdoor.cast(
            JavaProperty.getPropertyFor(this, propertyName)
                .orElseThrow( () -> new MissingPropertyException(this.getFullName(),propertyName) )
        );
    }

    public<T,D> FPOption<JavaProperty<T,D>> getPublicProperty( Class<D> expectedType, String propertyName ) {
        return cast(getProperty( expectedType,propertyName ).filter( JavaProperty::isPublic ).toFPOption());
    }

    public<T,D> FPOption<JavaProperty<T,D>> getProperty( Class<D> expectedType, String propertyName ) {
        if ( propertyName.contains(".") ) {
            String[] steps = propertyName.split( "\\." );

            return FP.wrapArray(steps).tail().foldWhile(getProperty(steps[0]), JavaProperty::getProperty );
        } else {
            return Backdoor.cast( JavaProperty.getPropertyFor( this, propertyName ) );
        }
    }

    public<T,D> FPOption<JavaProperty<T,D>> getProperty( JavaClass expectedType, String propertyName ) {
        return Backdoor.cast( JavaProperty.getPropertyFor(this, propertyName) );
    }

    public<T> FPIterable<JavaProperty<T,Object>> getProperties() {
        if ( this.isEnum() ) {
            return FPIterable.empty();
        }

        FPIterable<String> fromGetterMethods = this.allGetterMethods()
            .filterNot( m -> m.getDeclaringClass().getFullName().startsWith( "java" ) )
            .map( m -> JavaProperty.dropGetterLabelPrefixFrom(m.getName()) );

        FPIterable<String> fromPublicFields = this.getAllPublicInstanceFields().map( JavaField::getName );
        FPIterable<String> propertyNames    = fromGetterMethods.and( fromPublicFields );

        return Backdoor.cast( propertyNames.toSetFP().flatMap(this::getProperty) );
    }

    public FPIterable<JavaMethod> allGetterMethods() {
        return getAllMethods()
            .filter(JavaMethod::isGetterMethod)
            .filterNot(m -> m.getJdkMethod().isEmpty() || m.getJdkMethod().get().getDeclaringClass() == Object.class);
    }

    public FPIterable<GetterSetterPair> allGetterAndSetterMethods() {
        return allGetterMethods().map(getter -> {
            FPOption<JavaMethod> setter = getter.getMatchingSetter();

            return new GetterSetterPair(getter, setter);
        });
    }

    public FPIterable<JavaMethod> getAllInstanceMethods() {
        return getAllMethods().filterNot( JavaExecutable::isStatic );
    }

    public FPIterable<JavaMethod> getAllStaticMethods() {
        return getAllMethods().filter( JavaExecutable::isStatic );
    }

    public FPIterable<JavaMethod> getAllPublicInstanceMethods() {
        return getAllInstanceMethods().filter( JavaExecutable::isPublic  );
    }

    public FPIterable<JavaMethod> getAllPublicInstanceMethods( String targetMethodName ) {
        return getAllPublicInstanceMethods().filter( m -> m.getName().equals(targetMethodName) );
    }

    public FPIterable<JavaField> getAllInstanceFields() {
        return getAllFields().filterNot(JavaField::isStatic);
    }

    public FPIterable<JavaField> getAllStaticFields() {
        return getAllFields().filter(JavaField::isStatic);
    }

    public FPIterable<JavaField> getAllPublicInstanceFields() {
        return getAllInstanceFields().filter(JavaField::isPublic);
    }

    public<A extends Annotation> FPOption<A> getAnnotation(Class<A> annotationType) {
        return FP.option( (A) getJdkClass().getAnnotation(annotationType) );
    }

    public boolean isInstance( Object o ) {
        return getJdkClass().isInstance( o );
    }

    /**
     * Return a factory object that knows how to create default values for this type.<p>
     *
     * For example, the default value of an int is zero and the default value of a list is an
     * empty list.
     */
    public FPOption<Factory<Object>> getDefaultValueFactory() {
        return DefaultValueRegistry.getDefaultValueFactoryFor(getJdkClass());
    }

    /**
     * Returns parent class and its interfaces all the way to the top of the hierarchy.
     */
    public FPIterable<JavaClass> classHierarchy() {
        return ReflectionUtils.classHierarchyIterable(getJdkClass());
    }

    public boolean isAssignableTo( JavaClass b ) {
        return assignmentDistanceFrom(b) >= 0;
    }

    /**
     * Returns zero if b is the exact same class as this instance.  -1 if a cannot be assigned to
     * b.  After that, the number of classes separating a from b in the inheritance tree is
     * returned.
     */
    public int assignmentDistanceFrom( JavaClass b ) {
        if ( this.equals(b) ) {
            return 0;
        }

        // Special handling for numbers (bytes, ints, longs, floats etc)
        Tuple2<Class,Class> numberTypes = NUMBER_TYPES.get(getJdkClass());
        if ( numberTypes != null ) {
            if ( numberTypes.getFirst() == b.getJdkClass() || numberTypes.getSecond() == b.getJdkClass() ) {
                return 1;
            } else if ( b.getJdkClass() == Object.class ) {
                return 2;
            }

            return -1;
        }

        // hierarchy scan
        FPIterable<JavaClass> it = this.classHierarchy();
        int distance = 1;
        for ( JavaClass x : it ) {
            if ( b.equals(x) ) {
                return distance;
            }

            distance += 1;
        }

        return -1;
    }

    public JavaConstructor getMandatoryPublicConstructor( Class... targetParameterTypes ) {
        return allPublicConstructors().filter( c -> c.matches(targetParameterTypes) ).firstOrThrow(
            () -> new ReflectionException( String.format("Unable to find constructor %s(%s)", getJdkClass().getSimpleName(), Arrays.asList(targetParameterTypes) ) )
        );
    }

    public<R> JavaMethod findPublicMethod( Class<R> returnType, String targetMethodName, Class...targetMethodParameterTypes ) {
        return getAllPublicInstanceMethods().filter( m -> m.matches(returnType, targetMethodName, targetMethodParameterTypes) ).firstOrThrow(
            () -> new ReflectionException(String.format("Unable to find method %s.%s(%s)", getShortName(), targetMethodName, Arrays.asList(targetMethodParameterTypes)))
        );
    }

    public FPOption<InputStream> getResourceAsStream( String resourcePath ) {
        return FPOption.of( getJdkClass().getResourceAsStream(resourcePath) );
    }

    public FPOption<JavaClass> getParentClass() {
        return FP.option(clazz.getGenericSuperclass()).map(c -> JavaClass.of(c,generics) );
    }

    /**
     * Return the parent class that equals the specified class.
     */
    public FPOption<JavaClass> getParentClass( Class<?> targetParentClass ) {
        return walkClassHierarchy().first( p -> p.getJdkClass().equals(targetParentClass) );
    }

    public FPIterable<JavaClass> getInterfaces() {
        return FP.wrapArray(clazz.getInterfaces()).map(i -> JavaClass.of(i, generics));
    }

    public FPIterable<JavaClass> walkClassHierarchy() {
        return () -> {
            Deque<JavaClass> queue = new LinkedList<>();
            queue.push(JavaClass.this);

            return new FPIterator<>() {
                public boolean _hasNext() {
                    return !queue.isEmpty();
                }

                public JavaClass _next() {
                    JavaClass next = queue.removeLast();

                    next.getInterfaces().forEach( queue::add );
                    next.getParentClass().ifPresent( queue::addFirst );

                    return next;
                }
            };
        };
    }

    public FPOption<JavaDoc> getJavaDoc() {
        return JavaDocFetcher.INSTANCE.fetchJavaDocFor( this );
    }
}
