package mosaics.lang.reflection.fetcher;

import lombok.AllArgsConstructor;
import lombok.Value;
import mosaics.fp.FP;
import mosaics.fp.collections.ConsList;
import mosaics.fp.collections.FPList;
import mosaics.fp.collections.FPOption;
import mosaics.i18n.I18nContext;
import mosaics.lang.functions.Function1;
import mosaics.lang.reflection.JavaClass;
import mosaics.lang.reflection.JavaProperty;
import mosaics.lang.reflection.converters.NoOpConverter;
import mosaics.lang.reflection.converters.TypeConverter;
import mosaics.lang.reflection.converters.TypeConverterFactory;
import mosaics.utils.CollectionUtils;

import static mosaics.lang.Backdoor.cast;


/**
 * Fetch a value from an object.  It is capable of following a chain of properties to reach
 * the desired value and if a value is not found then a default value may be specified.
 */
@Value
@AllArgsConstructor
@SuppressWarnings({"unchecked", "rawtypes"})
public class ValueFetcherBuilder<T> {

    public static ValueFetcherBuilder<Object> newValueFetcherBuilder( String initialValueName ) {
        return new ValueFetcherBuilder<>( initialValueName, FP.emptyOption() );
    }

    private final String         initialValueName;

    private final ConsList<ValueOp<?,T>> conversionSteps;


    private ValueFetcherBuilder( String initialValueName, FPOption<JavaClass>  type) {
        this.initialValueName = initialValueName;

        if ( type.isEmpty() ) {
            this.conversionSteps = cast(ConsList.singleton(new UntypedOps.NoOp()));
        } else {
            this.conversionSteps = cast((new TypedOps.TypedNoOpStep<T,T>(type.get())));
        }
    }


    public <B> ValueFetcherBuilder<B> asType( Class<B> type ) {
        return asType( JavaClass.of(type) );
    }

//    public <B> ValueFetcherBuilder<B> asType( TypeMeta type ) {
//        return cast( asType(JavaClass.of(type)) );
//    }

    @SuppressWarnings("unchecked")
    public <B> ValueFetcherBuilder<B> asType( JavaClass type ) {
        ValueOp  nextStep  = conversionSteps.getHead().toType(type);
        ConsList nextSteps = conversionSteps.append( nextStep );

        return new ValueFetcherBuilder<>( initialValueName, nextSteps );
    }

    @SuppressWarnings("unchecked")
    public <B> ValueFetcherBuilder<B> fetchProperty( String propertyName ) {
        ValueOp  nextStep  = conversionSteps.getHead().fetchProperty( propertyName );
        ConsList nextSteps = conversionSteps.append( nextStep );

        return new ValueFetcherBuilder<>( initialValueName, nextSteps );
    }

    public ValueFetcherBuilder<T> withDefaultValue( T defaultValue ) {
        ValueOp  nextStep  = conversionSteps.getHead().withDefaultValue( defaultValue );
        ConsList nextSteps = conversionSteps.append( nextStep );

        return new ValueFetcherBuilder<>( initialValueName, nextSteps );
    }

    public ValueFetcher<T> build() {
        FPList<ValueOp> ops = cast(conversionSteps.reverse().toFPList());

        return new ValueFetcher<T>() {
            @SuppressWarnings("unchecked")
            public FPOption<T> fetchFrom( I18nContext ctx, Function1<String, ?> source ) {
                Object sourceValue    = source.invoke( initialValueName );
                Object processedValue = ops.fold( sourceValue, ( soFar, nextOp ) -> nextOp.process(ctx,soFar) );

                return cast( FP.option(processedValue) );
            }
        };
    }


    private static interface ValueOp<S,D> {
        public D process( I18nContext ctx, S value );

        public <T> ValueOp<D,T> toType( JavaClass newType );
        public <T> ValueOp<D,T> fetchProperty( String propertyName );
        public ValueOp<D,D> withDefaultValue( D defaultValue );
    }


    // NOT TYPED

    private static class UntypedOps {
        @Value
        private static class NoOp implements ValueOp<Object, Object> {
            public Object process( I18nContext ctx, Object value ) {
                return value;
            }

            public <T> ValueOp<Object, T> toType( JavaClass type ) {
                return new ConvertOp<>( type );
            }

            public <T> ValueOp<Object,T> fetchProperty( String propertyName ) {
                return cast( new FetchPropertyOp(propertyName) );
            }

            public ValueOp<Object,Object> withDefaultValue( Object defaultValue ) {
                return new DefaultValueOp(defaultValue);
            }
        }

        @Value
        private static class ConvertOp<D> implements ValueOp<Object, D> {
            private final JavaClass destType;

            public D process( I18nContext ctx, Object value ) {
                if ( value == null ) {
                    return null;
                }

                JavaClass               sourceType    = cast(JavaClass.of(value.getClass()));
                TypeConverter<Object,D> typeConverter = getMandatoryConverterFor( sourceType, destType );

                return typeConverter.convert(ctx,value).getResult();
            }

            public <T> ValueOp<D, T> toType( JavaClass newType ) {
                TypeConverter<D,T> typeConverter = getMandatoryConverterFor( this.destType, newType );

                return new TypedOps.TypedConvertOp<>(typeConverter);
            }

            public <T> ValueOp<D,T> fetchProperty( String propertyName ) {
                JavaProperty<D,T> prop = destType.getPropertyMandatory( propertyName );

                return new TypedOps.TypedFetchProperty<>( prop );
            }

            public ValueOp<Object,Object> withDefaultValue( Object defaultValue ) {
                JavaClass      fromType   = JavaClass.of( defaultValue );
                TypeConverter converter = getMandatoryConverterFor( fromType, this.destType );

                return new TypedOps.TypedDefaultValueOp(defaultValue, converter);
            }
        }

        @Value
        private static class FetchPropertyOp implements ValueOp<Object, Object> {
            private String propertyName;

            public Object process( I18nContext ctx, Object value ) {
                if ( value == null ) {
                    return null;
                }

                JavaClass                   sourceType = cast(JavaClass.of(value.getClass()));
                JavaProperty<Object,Object> property   = sourceType.getPropertyMandatory( propertyName );

                return property.getValueFrom( value ).orNull();
            }

            public <T> ValueOp<Object, T> toType( JavaClass newType ) {
                return new ConvertOp<>( newType );
            }

            public <T> ValueOp<Object,T> fetchProperty( String propertyName ) {
                return cast( new FetchPropertyOp(propertyName) );
            }

            public ValueOp<Object,Object> withDefaultValue( Object defaultValue ) {
                return new DefaultValueOp(defaultValue);
            }
        }

        @Value
        private static class DefaultValueOp implements ValueOp<Object, Object> {
            private Object defaultValue;

            public Object process( I18nContext ctx, Object value ) {
                if ( CollectionUtils.isEmpty(value) ) {
                    return defaultValue;
                }

                return value;
            }

            public <T> ValueOp<Object, T> toType( JavaClass newType ) {
                return new ConvertOp<>( newType );
            }

            public <T> ValueOp<Object,T> fetchProperty( String propertyName ) {
                return cast( new FetchPropertyOp(propertyName) );
            }

            public ValueOp<Object,Object> withDefaultValue( Object defaultValue ) {
                return new DefaultValueOp(defaultValue);
            }
        }
    }

    private static <S,D> TypeConverter<S,D> getMandatoryConverterFor( JavaClass from, JavaClass to ) {
        return TypeConverterFactory.<S,D>getConverterFor( from, to )
            .orElseThrow( () -> new IllegalArgumentException(String.format("No type converter found for %s -> %s",from,to)) );
    }


    //// TYPED

    private static class TypedOps {
        @Value
        public static class TypedNoOpStep<S, D> implements ValueOp<S, D> {
            private JavaClass type;

            public D process( I18nContext ctx, S value ) {
                if ( type.isInstance( value ) ) {
                    return cast( value );
                } else {
                    throw new ClassCastException( String.format( "Found '%s' when expecting '%s'", value, type.getFullName() ) );
                }
            }

            public <T> ValueOp<D, T> toType( JavaClass newType ) {
                if ( this.type.isAssignableTo(newType) ) {
                    return cast(this);
                }

                TypeConverter<D,T> typeConverter = getMandatoryConverterFor( this.type, newType );
                return new TypedConvertOp<>(typeConverter);
            }

            public <T> ValueOp<D,T> fetchProperty( String propertyName ) {
                JavaProperty<D,T> prop = type.getPropertyMandatory( propertyName );

                return new TypedFetchProperty<>( prop );
            }

            public ValueOp<D,D> withDefaultValue( D defaultValue ) {
                return new TypedDefaultValueOp<>(defaultValue, new NoOpConverter<>(type));
            }
        }

        @Value
        public static class TypedConvertOp<S,D> implements ValueOp<S,D> {
            private TypeConverter<S,D> converter;

            public D process( I18nContext ctx, S value ) {
                return converter.convert(ctx,value).getResult();
            }

            public <T> ValueOp<D, T> toType( JavaClass newType ) {
                JavaClass thisDestType = this.converter.getDestType();

                if ( thisDestType.isAssignableTo(newType) ) {
                    return cast(this);
                }

                TypeConverter<D,T> typeConverter = getMandatoryConverterFor( thisDestType, newType );
                return new TypedConvertOp<>(typeConverter);
            }

            public <T> ValueOp<D,T> fetchProperty( String propertyName ) {
                JavaProperty<D,T> prop = converter.getDestType().getPropertyMandatory( propertyName );

                return new TypedFetchProperty<>( prop );
            }

            public ValueOp<D,D> withDefaultValue( D defaultValue ) {
                return new TypedDefaultValueOp<>(defaultValue, new NoOpConverter<>(converter.getDestType()));
            }
        }

        @Value
        public static class TypedFetchProperty<S,D> implements ValueOp<S,D> {
            private JavaProperty<S,D> property;

            public D process( I18nContext ctx, S value ) {
                return property.getValueFrom( value ).orNull();
            }

            public <T> ValueOp<D, T> toType( JavaClass newType ) {
                JavaClass thisDestType = property.getValueType();

                if ( thisDestType.isAssignableTo(newType) ) {
                    return cast(this);
                }

                TypeConverter<D,T> typeConverter = getMandatoryConverterFor( thisDestType, newType );
                return new TypedConvertOp<>(typeConverter);
            }

            public <T> ValueOp<D,T> fetchProperty( String propertyName ) {
                JavaProperty<D,T> prop = property.getValueType().getPropertyMandatory( propertyName );

                return new TypedFetchProperty<>( prop );
            }

            public ValueOp<D,D> withDefaultValue( D defaultValue ) {
                return new TypedDefaultValueOp<>(defaultValue, new NoOpConverter<>(property.getValueType()));
            }
        }

        @Value
        public static class TypedDefaultValueOp<T> implements ValueOp<T,T> {
            private Object              defaultValue;
            private TypeConverter<?,T> defaultValueConverter;

            public T process( I18nContext ctx, T value ) {
                if ( CollectionUtils.isEmpty(value) ) {
                    return cast( defaultValueConverter.convert( ctx, cast(defaultValue) ).getResult() );
                }

                return value;
            }

            public <D> ValueOp<T, D> toType( JavaClass toType ) {
                JavaClass fromType = defaultValueConverter.getDestType();

                if ( fromType.isAssignableTo(toType) ) {
                    return cast(this);
                }

                TypeConverter<T,D> typeConverter = getMandatoryConverterFor( fromType, toType );
                return new TypedConvertOp<>(typeConverter);
            }

            public <D> ValueOp<T,D> fetchProperty( String propertyName ) {
                JavaClass         fromType = defaultValueConverter.getDestType();
                JavaProperty<T,D> prop     = fromType.getPropertyMandatory( propertyName );

                return new TypedFetchProperty<>( prop );
            }

            public ValueOp<T,T> withDefaultValue( T defaultValue ) {
                return new TypedDefaultValueOp<>(defaultValue, defaultValueConverter);
            }
        }
    }
}

