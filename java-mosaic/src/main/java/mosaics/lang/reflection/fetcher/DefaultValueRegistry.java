package mosaics.lang.reflection.fetcher;


import mosaics.cache.Cache;
import mosaics.cache.PermCache;
import mosaics.fp.FP;
import mosaics.fp.collections.ConsList;
import mosaics.fp.collections.FPList;
import mosaics.fp.collections.maps.FPMap;
import mosaics.fp.collections.FPOption;
import mosaics.fp.collections.FPSet;
import mosaics.fp.collections.RRBVector;
import mosaics.lang.functions.Factory;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static mosaics.lang.Backdoor.cast;


/**
 * Default value registry stores factories for the default value of fields and parameters by type.
 * For example, the default value of an Integer is Zero and the default value for an Optional is
 * empty.
 */
public class DefaultValueRegistry {
    private static final Cache<Class<?>, Factory<Object>> defaultValueFactoryCache = new PermCache<>();

    static {
        registerDefaultValueFor( Optional.class, Optional.empty() );
        registerDefaultFactoryFor( List.class, Collections::emptyList );
        registerDefaultFactoryFor( Set.class, Collections::emptySet );
        registerDefaultFactoryFor( Map.class, Collections::emptyMap );
        registerDefaultValueFor( FPOption.class, FP.emptyOption() );
        registerDefaultValueFor( FPList.class, FP.toList() );
        registerDefaultValueFor( FPSet.class, FP.toSet() );
        registerDefaultFactoryFor( FPMap.class, FP::toMap );
        registerDefaultValueFor( RRBVector.class, FP.toVector() );
        registerDefaultValueFor( ConsList.class, FP.toConsList() );
    }

    public static <T> FPOption<Factory<T>> getDefaultValueFactoryFor( Class<T> c ) {
        return cast( defaultValueFactoryCache.get(c) );
    }

    public static <T> void registerDefaultFactoryFor( Class<T> c, Factory<T> factory ) {
        if ( defaultValueFactoryCache.contains(c) ) {
            throw new IllegalStateException(c.getName()+" is already registered within "+DefaultValueRegistry.class.getSimpleName() );
        }

        defaultValueFactoryCache.put( c, cast(factory) );
    }

    public static <T> void registerDefaultValueFor( Class<T> c, T value ) {
        DefaultValueRegistry.registerDefaultFactoryFor( c, Factory.of(value) );
    }
}
