package mosaics.lang.reflection;

import lombok.Synchronized;
import mosaics.fp.FP;
import mosaics.fp.collections.FPOption;
import mosaics.lang.functions.Function1;
import mosaics.lang.lifecycle.StartStoppable;
import mosaics.utils.ComparatorUtils;
import mosaics.utils.SetUtils;

import java.io.File;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.Optional;
import java.util.Set;

import static mosaics.lang.Backdoor.cast;


/**
 * An object factory that instantiates and caches objects.  It supports constructor creation
 * only and can look up constructor parameters by name or type.  The name lookup uses the parameter
 * name as given by the constructor.  Usually Java does not make the parameter name available at
 * runtime, however if the -parameter flag is past to javac then this class will have access to it.
 * When a parameter cannot be found by name its
 * type is looked up and if previously cached then that value will be used.  If that is not found
 * then DI will try to create it and place it within the cache.  If a value cannot be found or
 * created (because it cannot find a suitable constructor) then it will throw an exception.<p/>
 */
public class DI {

    private static Set<Class> excludeAutoConstructorSet = SetUtils.asSet( File.class);

    private IdentityHashMap<Class, FPOption> typeCache = new IdentityHashMap<>();
    private HashMap<String,FPOption> nameCache = new HashMap<>();


    public DI() {}

    /**
     * Copy of another DI.  The new DI will be a copy of the one past in, and any changes made after
     * the copy to the original DI will not affect this copy.
     */
    public DI( DI other ) {
        this.typeCache = new IdentityHashMap<>( other.typeCache );
        this.nameCache = new HashMap<>( other.nameCache );
    }


    @Synchronized
    public <T> void putByType( Class<T> type, T v ) {
        typeCache.put( type, FPOption.of(v) );
    }

    @Synchronized
    public <T> void putByName( String key, T v ) {
        nameCache.put( key, FPOption.of(v) );
    }

    public <T> DI with( Class<T> type, T v ) {
        putByType( type, v );

        return this;
    }

    public <T> DI with( JavaClass type, T v ) {
        putByType( cast(type.getJdkClass()), v );

        return this;
    }

    public <T> DI with( String name, T v ) {
        putByName( name, v );

        return this;
    }

    @Synchronized
    @SuppressWarnings("unchecked")
    public <T> T fetchMandatory( Class<T> type ) {
        return fetchByType( type ).orElseThrow(
            () -> new ReflectionException("Unable to populate arguments for any of "+type.getName()+"' constructors")
        );
    }

    public <T> FPOption<T> fetchByType( Class<T> type ) {
        return fetchByType( JavaClass.of(type) );
    }

    public <T> FPOption<T> fetchByType( JavaClass type ) {
        FPOption<T> value;

        if ( type.isOptional() ) {
            FPOption<JavaClass> generic = type.getClassGenerics().first();

            if ( !generic.hasValue() ) {
                return FP.emptyOption();
            }

            value = cast( typeCache.get( generic.get().getJdkClass() ) );
        } else {
            value = cast( typeCache.get( type.getJdkClass() ) );
        }

        if ( type.isString() ) {
            value = FPOption.none();
        } else if ( value == null ) {
            if ( !type.isAbstract() ) {
                typeCache.put( type.getJdkClass(), FPOption.none() );  // prevents circular dependencies from creating a stack overflow

                value = cast(type.allPublicConstructors()
                    .sort( (a,b) -> ComparatorUtils.compareDesc(a.getParameters().count(), b.getParameters().count()) )
                    .flatMap( this::tryToInstantiateConstructor )
                    .first());

                typeCache.put( type.getJdkClass(), value );
            } else {
                value = FP.emptyOption();
            }
        }

        return value;
    }

    public <T> boolean fetchAndUpdateByType( Class<T> type, Function1<T,T> updateFunction ) {
        FPOption<T> updatedValue = fetchByType(type).map( updateFunction::invoke );

        if ( updatedValue.hasValue() ) {
            typeCache.put( type, updatedValue );

            return true;
        } else {
            return false;
        }
    }

    /**
     * Create a new JavaMethod that populates another JavaMethod's args from DI.  Parameters that are
     * not within the DI context will remain as parameters that can be specified by the caller.
     */
    public JavaMethod curry( JavaMethod m ) {
        CurriedJavaMethod curriedJavaMethod = new CurriedJavaMethod( m, this::curryParameter );

        return curriedJavaMethod.isCurried() ? curriedJavaMethod : m;
    }

    /**
     * Inject the specified objects fields with objects taken from this DI context.  The DI context
     * has two search paths, the first is by name and the second is by type.  Fields that match
     * an object within the DI by name will always be injected, matches by type will only be
     * injected if the field's existing value is null.
     */
    public <T> void populateFields( T dto ) {
        JavaClass.of(dto).getAllFields()
            .filter( f -> !f.isFinal() )
            .forEach( f -> populateField(dto, f) );
    }

    public <T> FPOption<T> fetch( JavaParameter p ) {
        return cast( fetch(p.getJdkType(), p.getName()) );
    }

    /**
     * Fetch either by name or by type.  Name takes precedence.  If type is not found then DI will
     * try to instantiate it directly via its no args constructor.
     */
    public <T> FPOption<T> fetch( Class<T> type, String name ) {
        FPOption byName = nameCache.get(name);
        if ( byName != null && byName.hasValue() ) {
            return cast(byName);
        }

        return fetchByType( type );
    }

    public boolean accepts( JavaParameter p ) {
        return accepts( p.getJdkType(), p.getName() );
    }

    public <T> boolean accepts( Class<T> type, String name ) {
        return fetch( type, name ).hasValue();
    }

    public <T> T newInstance( Class<T> type, Class...argTypes ) {
        return newInstance( JavaClass.of(type), argTypes );
    }

    public <T> T newInstance( JavaClass type, Class...argTypes ) {
        return cast(
            type.getPublicConstructor( argTypes )
                .map( this::newInstance )
                .orElseThrow( () -> new IllegalArgumentException(
                    "Unable to instantiate constructor" + type.getFullName() + "(" + FP.wrapArray(argTypes).mkString()+")"
                )
            )
        );
    }

    public <T> T newInstance( JavaConstructor constructor ) {
        return cast(tryToInstantiateConstructor( constructor ).orElseThrow(
            () -> new IllegalArgumentException( "Unable to instantiate constructor " + constructor )
        ));
    }


    @SuppressWarnings("unchecked")
    private <T> void populateField( T dto, JavaField f ) {
        FPOption<T> byName = nameCache.get( f.getName() );

        if ( isExcluded(f.getType()) ) {
            // skip
        } else if ( byName != null && byName.hasValue() ) {
            setField( dto, f, byName.get() );
        } else {
            FPOption<T> byType = fetchByType( f.getType() );

            if ( byType != null && byType.hasValue() ) {
                if ( f.isEmptyOn(dto) ) {
                    setField( dto, f, byType.get() );
                }
            } else if ( f.isFPOption() ) {
                f.setValueOn( dto, FP.emptyOption() );
            } else if ( f.isJdkOptional() ) {
                f.setValueOn( dto, Optional.empty() );
            }
        }
    }

    @SuppressWarnings("unchecked")
    private boolean isExcluded( JavaClass type ) {
        return excludeAutoConstructorSet.contains(type.getJdkClass()) || type.isOptional() && (Boolean) type.getClassGenerics().first().map( jc -> excludeAutoConstructorSet.contains(jc.getJdkClass()) ).orElse( false );
    }

    private <T> void setField( T dto, JavaField f, Object v ) {
        Object newValue;

        if ( f.isJdkOptional() ) {
            if ( v == null ) {
                newValue = Optional.empty();
            } else if ( v instanceof Optional ) {
                newValue = v;
            } else {
                newValue = Optional.of( v );
            }
        } else if ( f.isFPOption() ) {
            if ( v == null ) {
                newValue = FP.emptyOption();
            } else if ( v instanceof FPOption ) {
                newValue = v;
            } else {
                newValue = FP.option( v );
            }
        } else {
            newValue = v;
        }

        if ( dto instanceof StartStoppable ) {
            ((StartStoppable) dto).registerServicesBefore( v );
        }

        f.setValueOn(dto, newValue);
    }

    private CurriedJavaMethod.CurriedParameter curryParameter( int postCurryIndex, JavaParameter parameterMeta ) {
        FPOption byName = nameCache.get(parameterMeta.getName());
        FPOption byType = typeCache.get(parameterMeta.getType().getJdkClass());

        if ( byName != null && byName.hasValue() ) {
            return new CurriedJavaMethod.SuppliedParameter(byName.get());
        } else if ( byType != null && byType.hasValue() ) {
            return new CurriedJavaMethod.SuppliedParameter(byType.get());
        } else {
            return new CurriedJavaMethod.PassThroughParameter(parameterMeta, postCurryIndex);
        }
    }

    private <T> FPOption<T> tryToInstantiateConstructor( JavaConstructor constructor ) {
        Object[] params = FP.wrap(constructor.getParameters()).map( this::fetchOrCreateParameterFor ).flatten().toArray();

        if ( constructor.getParameters().count() == params.length && !excludeAutoConstructorSet.contains(constructor.getJdkConstructor().getDeclaringClass()) ) {
            return FPOption.of( cast(constructor.newInstance(params)) );
        } else {
            return FPOption.none();
        }
    }

    @SuppressWarnings("unchecked")
    private <T> FPOption<T> fetchOrCreateParameterFor( JavaParameter param ) {
        FPOption<T> result = cast( nameCache.get(param.getName()) );

        if ( result != null ) {
            return result;
        }

        return fetchByType( (Class) param.getType().getJdkClass() );
    }
}
