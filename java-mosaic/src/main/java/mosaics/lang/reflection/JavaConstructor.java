package mosaics.lang.reflection;

import lombok.EqualsAndHashCode;
import mosaics.fp.FP;
import mosaics.fp.collections.FPIterable;
import mosaics.fp.collections.FPOption;
import mosaics.lang.Backdoor;

import java.lang.reflect.Constructor;
import java.lang.reflect.Executable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.TypeVariable;
import java.util.Arrays;
import java.util.Map;


@EqualsAndHashCode
public class JavaConstructor implements JavaExecutable {
    private Constructor wrappedConstructor;
    private Map<String, JavaClass> generics;

    JavaConstructor( Constructor constructor) {
        this(constructor, Map.of());
    }

     JavaConstructor( Constructor constructor, Map<String, JavaClass> generics ) {
         this.wrappedConstructor = constructor;
         this.generics    = generics;

         constructor.setAccessible( true );
     }

    public FPOption<Executable> getJdkExecutable() {
        return FP.option(wrappedConstructor);
    }

     public Constructor getJdkConstructor() {
        return wrappedConstructor;
     }

    public FPIterable<JavaParameter> getParameters() {
        return FP.wrapArray( wrappedConstructor.getParameters())
            .mapWithIndex((i,p) -> JavaParameter.of(this,i,p,generics));
    }

    public int getModifiers() {
        return wrappedConstructor.getModifiers();
    }

    public int getParameterCount() {
        return wrappedConstructor.getParameterCount();
    }

    public Object newInstance( Object...args ) {
        try {
            return wrappedConstructor.newInstance( args );
        } catch ( InstantiationException | IllegalAccessException e ) {
            throw Backdoor.throwException( e );
        } catch ( InvocationTargetException e ) {
            throw Backdoor.throwException( e.getTargetException() );
        }
    }

    public boolean matches( Class...targetParameterTypes ) {
        return Arrays.equals(wrappedConstructor.getParameterTypes(), targetParameterTypes);
    }

    public FPOption<JavaDoc> getJavaDoc() {
        return JavaDocFetcher.INSTANCE.fetchJavaDocFor( this );
    }

    public String toString() {
        StringBuilder buf = new StringBuilder();

        Class declaringClass = wrappedConstructor.getDeclaringClass();
        buf.append( declaringClass.getName());

        if ( declaringClass.getTypeParameters().length > 0 ) {
            buf.append( '<' );

            String separator = "";
            for ( TypeVariable v : declaringClass.getTypeParameters() ) {
                buf.append( separator );
                buf.append( generics.get( v.getName() ) );

                separator = ", ";
            }

            buf.append( '>' );
        }

        buf.append( '(' );

        boolean needsDelimiter = false;
        for ( JavaParameter p : getParameters() ) {
            if ( needsDelimiter ) {
                buf.append( ", " );
            }

            buf.append( p.getType() );
            buf.append( ' ' );
            buf.append( p.getName() );

            needsDelimiter = true;
        }

        buf.append( ')' );

        return buf.toString();
    }
}
