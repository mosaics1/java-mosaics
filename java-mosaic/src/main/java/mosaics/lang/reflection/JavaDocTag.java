package mosaics.lang.reflection;

import lombok.Value;
import mosaics.fp.FP;
import mosaics.fp.collections.FPOption;
import mosaics.strings.StringUtils;

import java.util.Collections;
import java.util.List;


@Value
public class JavaDocTag {
    private String           tagName;
    private FPOption<String> tagArg;
    private List<String>     text;


    public JavaDocTag( String tagName, String arg ) {
        this( tagName, FP.optionalString(arg), Collections.emptyList() );
    }

    public JavaDocTag( String tagName, List<String> text ) {
        this( tagName, FP.emptyOption(), text );
    }

    public JavaDocTag( String tagName, String tagArg, List<String> text ) {
        this( tagName, FP.option(tagArg), text );
    }

    public JavaDocTag( String tagName, FPOption<String> tagArg, List<String> text ) {
        this.tagName = tagName;
        this.tagArg  = tagArg;
        this.text    = StringUtils.isBlank(text) ? Collections.emptyList() : text;
    }
}
