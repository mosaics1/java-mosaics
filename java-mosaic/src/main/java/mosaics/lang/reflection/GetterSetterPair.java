package mosaics.lang.reflection;

import mosaics.fp.collections.FPOption;
import lombok.Value;


@Value
public class GetterSetterPair {
    private JavaMethod         getter;
    private FPOption<JavaMethod> setter;

    GetterSetterPair( JavaMethod getter, FPOption<JavaMethod> setter ) {
        this.getter = getter;
        this.setter = setter;
    }

    public String toString() {
        StringBuilder buf = new StringBuilder();

        if ( setter.hasValue() ) {
            buf.append( '(' );
            buf.append( getter.getName() );
            buf.append( "(), " );
            buf.append( setter.get().getName() );
            buf.append( "())" );
        } else {
            buf.append( getter.getName() );
            buf.append( "()" );
        }

        return buf.toString();
    }
}
