package mosaics.lang.reflection;

import mosaics.fp.FP;
import mosaics.fp.collections.FPIterable;
import mosaics.fp.collections.FPOption;
import mosaics.lang.functions.FunctionIO;

import java.lang.annotation.Annotation;
import java.lang.reflect.Executable;
import java.lang.reflect.Method;
import java.util.List;


public class CurriedJavaMethod implements JavaMethod {

    private final JavaMethod         wrappedMethod;
    private final CurriedParameter[] parameters;


    public CurriedJavaMethod( JavaMethod wrappedMethod, FunctionIO<JavaParameter,CurriedParameter> currier ) {
        this.wrappedMethod = wrappedMethod;
        this.parameters    = curryParameters( wrappedMethod.getParameters().toList(), currier );
    }

    public boolean isCurried() {
        return FP.wrapArray(parameters).first( CurriedParameter::isFixedParameter ).hasValue();
    }

    public String getName() {
        return wrappedMethod.getName();
    }

    public FPIterable<JavaParameter> getParameters() {
        return FP.wrapArray(parameters).flatMap( CurriedParameter::getJavaParameter );
    }

    public int getModifiers() {
        return wrappedMethod.getModifiers();
    }

    public FPOption<Executable> getJdkExecutable() {
        return FP.emptyOption();
    }

    public FPIterable<Annotation> getAnnotations() {
        return wrappedMethod.getAnnotations();
    }

    public JavaClass getReturnType() {
        return wrappedMethod.getReturnType();
    }

    public FPOption<Method> getJdkMethod() {
        return FP.emptyOption();
    }

    public <T extends Annotation> FPOption<T> getAnnotation( Class<T> annotationType ) {
        return wrappedMethod.getAnnotation( annotationType );
    }

    public Object invokeStaticAgainst( Object... args ) {
        Object[] collectedArgs = FP.wrapArray(parameters).map( p -> p.invoke(args) ).toArray();

        return wrappedMethod.invokeStaticAgainst( collectedArgs );
    }

    public JavaClass getDeclaringClass() {
        return wrappedMethod.getDeclaringClass();
    }

    public Object invokeAgainst( Object object, Object... args ) {
        Object[] collectedArgs = FP.wrapArray(parameters).map( p -> p.invoke(args) ).toArray();

        return wrappedMethod.invokeAgainst( object, collectedArgs );
    }

    public FPOption<JavaDoc> getJavaDoc() {
        return JavaDocFetcher.INSTANCE.fetchJavaDocFor( wrappedMethod );
    }

    private CurriedParameter[] curryParameters( List<JavaParameter> javaParameters, FunctionIO<JavaParameter,CurriedParameter> currier ) {
        int                numParameters     = javaParameters.size();
        CurriedParameter[] curriedParameters = new CurriedParameter[numParameters];

        int passthroughCount = 0;
        for ( int i=0; i<numParameters; i++ ) {
            JavaParameter javaParameter = javaParameters.get( i );

            curriedParameters[i] = currier.invoke(passthroughCount,javaParameter);

            if ( curriedParameters[i].isPassThroughParameter() ) {
                passthroughCount += 1;
            }
        }

        return curriedParameters;
    }





    public static interface CurriedParameter {
        public Object invoke( Object[] args );
        public FPOption<JavaParameter> getJavaParameter();

        public default boolean isFixedParameter() {
            return getJavaParameter().isEmpty();
        }

        public default boolean isPassThroughParameter() {
            return getJavaParameter().hasValue();
        }
    }

    public static  class SuppliedParameter implements CurriedParameter {
        private Object value;

        public SuppliedParameter( Object v ) {
            this.value = v;
        }

        public Object invoke( Object[] args ) {
            return value;
        }

        public FPOption<JavaParameter> getJavaParameter() {
            return FPOption.none();
        }
    }

    public static class PassThroughParameter implements CurriedParameter {
        private final FPOption<JavaParameter> javaParameter;
        private final int                     curriedMethodIndex;

        public PassThroughParameter( JavaParameter javaParameter, int curriedMethodIndex ) {
            this.javaParameter      = FPOption.of(javaParameter);
            this.curriedMethodIndex = curriedMethodIndex;
        }

        public Object invoke( Object[] args ) {
            return args[curriedMethodIndex];
        }

        public FPOption<JavaParameter> getJavaParameter() {
            return javaParameter;
        }
    }

}
