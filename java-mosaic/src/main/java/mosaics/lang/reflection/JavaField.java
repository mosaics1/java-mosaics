package mosaics.lang.reflection;

import lombok.EqualsAndHashCode;
import lombok.SneakyThrows;
import mosaics.fp.FP;
import mosaics.fp.collections.FPOption;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Map;


@EqualsAndHashCode
public class JavaField {
    public static FPOption<JavaField> of(Class c, String fieldName) {
        return JavaClass.of(c).getField( fieldName );
    }

    static JavaField of(Field field) {
        return of(field, Map.of());
    }

    static JavaField of(Field field, Map<String,JavaClass> reifiedGenerics) {
        JavaClass type = JavaClass.of( field.getGenericType(), reifiedGenerics );

        return new JavaField( type, field, reifiedGenerics );
    }

    private final JavaClass              fieldType;
    private final Field                  wrappedField;
    private final Map<String, JavaClass> generics;

    JavaField( JavaClass fieldType, Field field, Map<String, JavaClass> generics ) {
        this.fieldType    = fieldType;
        this.wrappedField = field;
        this.generics     = generics;
    }

    public JavaClass getType() {
        return fieldType;
    }

    public String getName() {
        return wrappedField.getName();
    }

    @SneakyThrows
    public void clear() {
        wrappedField.setAccessible( true );
        wrappedField.set(null, null);
    }

    @SneakyThrows
    public void setValueOn( Object targetObject, Object newValue ) {
        wrappedField.setAccessible( true );
        wrappedField.set( targetObject, newValue );
    }

    public <T extends Annotation> FPOption<T> getAnnotation( Class<T> annotationType ) {
        return FP.option( wrappedField.getAnnotation(annotationType) );
    }

    @SneakyThrows
    public <S> Object getValueFrom( S o ) {
        wrappedField.setAccessible( true );

        return wrappedField.get( o );
    }

    public boolean isStatic() {
        return Modifier.isStatic(wrappedField.getModifiers());
    }

    public boolean isTransient() {
        return Modifier.isTransient( wrappedField.getModifiers() );
    }

    public boolean isPublic() {
        return Modifier.isPublic( wrappedField.getModifiers() );
    }

    public boolean isPrivate() {
        return Modifier.isPrivate( wrappedField.getModifiers() );
    }

    public boolean isMutable() {
        return !isFinal();
    }

    public boolean isFinal() {
        return Modifier.isFinal( wrappedField.getModifiers() );
    }

    public boolean isPrimitive() {
        return getType().isPrimitive();
    }

    public JavaClass getDeclaringClass() {
        return JavaClass.of(wrappedField.getDeclaringClass(),generics);
    }

    public Field getJdkField() {
        return wrappedField;
    }

    public FPOption<JavaDoc> getJavaDoc() {
        return JavaDocFetcher.INSTANCE.fetchJavaDocFor( this );
    }

    public boolean isJdkOptional() {
        return fieldType.isJdkOptional();
    }

    public boolean isFPOption() {
        return fieldType.isFPOption();
    }

    public <T> boolean isEmptyOn( T object ) {
        if ( getType().isPrimitive() ) {
            return false;
        }

        Object value = getValueFrom( object );

        return ReflectionUtils.isZeroOrEmpty( value );
    }

    public String toString() {
        return getName();
    }
}
