package mosaics.lang.reflection;

public class MissingSetterException extends ReflectionException {
    public <S,D> MissingSetterException( JavaProperty<S,D> p ) {
        super( "No setter found for '"+p+"' originating from " + p.getSourceType().getFullName() );
    }
}
