package mosaics.lang.reflection;

import mosaics.fp.collections.FPIterable;
import mosaics.fp.collections.FPOption;

import java.lang.reflect.Executable;
import java.lang.reflect.Modifier;


public interface JavaExecutable {
    public FPIterable<JavaParameter> getParameters();
    public int getModifiers();
    public int getParameterCount();
    public FPOption<Executable> getJdkExecutable();
    public FPOption<JavaDoc> getJavaDoc();

    public default boolean isStatic() {
        return Modifier.isStatic(getModifiers());
    }

    public default boolean isPublic() {
        return Modifier.isPublic(getModifiers());
    }
}
