package mosaics.lang.reflection;

public class MissingPropertyException extends RuntimeException {

    public MissingPropertyException( String source, String property ) {
        super( source + " is missing property '"+property+"'" );
    }

}
