package mosaics.lang.reflection;

import lombok.EqualsAndHashCode;
import mosaics.fp.FP;
import mosaics.fp.collections.FPIterable;
import mosaics.fp.collections.FPOption;
import mosaics.lang.Backdoor;

import java.lang.annotation.Annotation;
import java.lang.reflect.Executable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;


@EqualsAndHashCode
public class JavaMethodJdk implements JavaMethod {
    public static JavaMethod of( Method method) {
        return new JavaMethodJdk(method);
    }

    private Method                 wrappedMethod;
    private Map<String, JavaClass> generics;

    protected JavaMethodJdk( Method method ) {
        this( method, Map.of());
    }

    protected JavaMethodJdk( Method method, Map<String, JavaClass> generics ) {
        this.wrappedMethod = method;
        this.generics      = generics;
    }

    public String getName() {
        return wrappedMethod.getName();
    }

    public FPIterable<JavaParameter> getParameters() {
        return FP.wrapArray( wrappedMethod.getParameters())
            .mapWithIndex((i,p) -> JavaParameter.of(this,i,p,generics));
    }

    public int getModifiers() {
        return wrappedMethod.getModifiers();
    }

    public FPIterable<Annotation> getAnnotations() {
        return FP.wrapArray( wrappedMethod.getAnnotations());
    }

    public JavaClass getReturnType() {
        return JavaClass.of(wrappedMethod.getGenericReturnType());
    }

    public FPOption<Executable> getJdkExecutable() {
        return FP.option( wrappedMethod );
    }

    public FPOption<Method> getJdkMethod() {
        return FP.option( wrappedMethod );
    }

    public <T extends Annotation> FPOption<T> getAnnotation( Class<T> annotationType ) {
        return FP.option( wrappedMethod.getAnnotation(annotationType) );
    }

    public <S> Object invokeAgainst( S o, Object...args ) {
        wrappedMethod.setAccessible( true );

        try {
            return wrappedMethod.invoke( o, args );
        } catch ( IllegalAccessException e ) {
            throw Backdoor.throwException( e );
        } catch ( InvocationTargetException e ) {
            throw Backdoor.throwException( e.getTargetException() );
        }
    }

    public Object invokeStaticAgainst( Object...args ) {
        wrappedMethod.setAccessible( true );

        try {
            return wrappedMethod.invoke( null, args );
        } catch ( IllegalAccessException e ) {
            throw Backdoor.throwException( e );
        } catch ( InvocationTargetException e ) {
            throw Backdoor.throwException( e.getTargetException() );
        }
    }

    public JavaClass getDeclaringClass() {
        return JavaClass.of(wrappedMethod.getDeclaringClass(), generics);
    }

    public String toString() {
        return getFullSignature();
    }
}
