package mosaics.lang.reflection;

import mosaics.fp.FP;
import mosaics.fp.collections.FPIterable;
import mosaics.fp.collections.FPIterator;
import mosaics.fp.collections.FPList;
import mosaics.fp.collections.FPOption;
import mosaics.fp.collections.maps.FPMap;
import mosaics.lang.Backdoor;
import mosaics.lang.QA;

import java.lang.reflect.Type;
import java.util.Map;

public class JavaRecord implements FPIterable<JavaRecordPart> {
    public static JavaRecord of( JavaClass type ) {
        return new JavaRecord( type );
    }

    public static JavaRecord of( Class type ) {
        return new JavaRecord( JavaClass.of( type ) );
    }

    public static JavaRecord of( Type type ) {
        return new JavaRecord( JavaClass.of( type ) );
    }


    private final JavaClass                     javaClass;
    private final FPList<JavaRecordPart>        javaRecordParts;
    private final FPMap<String, JavaRecordPart> javaRecordPartsByName;
    private final JavaConstructor               primaryConstructor;

    private JavaRecord( JavaClass jc ) {
        QA.isTrueArg( jc.isRecord(), "is not a java record" );

        this.javaClass             = jc;
        this.javaRecordParts       = FP.wrapArray( jc.getJdkClass().getRecordComponents() )
            .mapWithIndex( ( i, p ) -> new JavaRecordPart( p, i ) )
            .toFPList();
        this.javaRecordPartsByName = javaRecordParts.toMapByKey( JavaRecordPart::getName ).toFPMap();
        this.primaryConstructor    = javaClass.getConstructor( javaRecordParts.map(p -> p.getComponentPartType().getJdkClass()).toArray(Class.class) ).get();
    }

    public FPOption<JavaRecordPart> getJavaRecordPart( String name ) {
        return javaRecordPartsByName.get( name );
    }

    public JavaRecordPart getJavaRecordPartByIndex( int i ) {
        return javaRecordParts.get( i );
    }

    public FPOption<JavaClass> getJavaClassFor( String name ) {
        return getJavaRecordPart( name ).map( JavaRecordPart::getComponentPartType );
    }

    public FPOption<Type> getTypeOf( String name ) {
        return getJavaRecordPart( name ).map( JavaRecordPart::getJdkType );
    }

    public FPIterator<JavaRecordPart> iterator() {
        return javaRecordParts.iterator();
    }

    public int getNumberOfParts() {
        return javaRecordParts.size();
    }

    public <T> T createInstance( Map<String, Object> args ) {
        Object[] preprocessedArgs = extractArgsFrom(args);

        return Backdoor.cast( primaryConstructor.newInstance(preprocessedArgs) );
    }

    private Object[] extractArgsFrom( Map<String, Object> args ) {
        Object[] extractedArgs = new Object[javaRecordParts.size()];

        for ( JavaRecordPart p : this ) {
            Object value;
            if ( args.containsKey(p.getName()) ) {
                value = args.get( p.getName() );
            } else {
                value = p.getDefaultValue().orNull();
            }

            extractedArgs[p.getIndex()] = value;
        }

        return extractedArgs;
    }
}
