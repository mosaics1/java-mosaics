package mosaics.lang.reflection;

import com.github.therapi.runtimejavadoc.BaseJavadoc;
import com.github.therapi.runtimejavadoc.ClassJavadoc;
import com.github.therapi.runtimejavadoc.Comment;
import com.github.therapi.runtimejavadoc.CommentElement;
import com.github.therapi.runtimejavadoc.CommentText;
import com.github.therapi.runtimejavadoc.FieldJavadoc;
import com.github.therapi.runtimejavadoc.InlineLink;
import com.github.therapi.runtimejavadoc.InlineTag;
import com.github.therapi.runtimejavadoc.InlineValue;
import com.github.therapi.runtimejavadoc.MethodJavadoc;
import com.github.therapi.runtimejavadoc.OtherJavadoc;
import com.github.therapi.runtimejavadoc.ParamJavadoc;
import com.github.therapi.runtimejavadoc.RuntimeJavadoc;
import com.github.therapi.runtimejavadoc.SeeAlsoJavadoc;
import com.github.therapi.runtimejavadoc.ThrowsJavadoc;
import mosaics.cache.Cache;
import mosaics.cache.PermCache;
import mosaics.fp.FP;
import mosaics.fp.collections.FPIterable;
import mosaics.fp.collections.FPOption;
import mosaics.strings.StringUtils;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;


/**
 * Retrieves the JavaDocs of a class at runtime.
 *
 * This class is internal to this package and is used by JavaClass et al.  To expose the javadocs at runtime, we are using
 * therapi-runtime-javadoc from https://github.com/dnault/therapi-runtime-javadoc.  They provide
 * a compile time annotation processor that grabs the javadocs and slides them at compile time into the class
 * files.  Thus to make this work, just add <code>annotationProcessor("com.github.therapi:therapi-runtime-javadoc-scribe:0.11.0")</code>
 * to your gradle build and ensure that annotation processing is enabled within intellij (just like lombok).
 */
class JavaDocFetcher {
    public static final JavaDocFetcher INSTANCE = new JavaDocFetcher();

    private static final Set<String> TAGS_WITH_ARGS = Set.of(
        "throws", "exception", "param", "see"
    );

    private final Cache<Object,FPOption<JavaDoc>> javaDocCache = new PermCache<>();


    public FPOption<JavaDoc> fetchJavaDocFor( JavaClass jc ) {
        return javaDocCache.get( jc, c -> generateJavaDocForClass((JavaClass) c) );
    }

    public FPOption<JavaDoc> fetchJavaDocFor( JavaConstructor constructor ) {
        return javaDocCache.get( constructor, c -> generateJavaDocForConstructor((JavaConstructor) c) );
    }

    public FPOption<JavaDoc> fetchJavaDocFor( JavaMethod method ) {
        return javaDocCache.get( method, c -> generateJavaDocForMethod((JavaMethod) c) );
    }

    public FPOption<JavaDoc> fetchJavaDocFor( JavaParameter parameter ) {
        if ( parameter.getSource().getJdkExecutable().isEmpty() ) {
            return FP.emptyOption();
        }

        return javaDocCache.get( parameter, c -> generateJavaDocForParameter((JavaParameter) c) );
    }

    public FPOption<JavaDoc> fetchJavaDocFor( JavaField field ) {
        return javaDocCache.get( field, f -> generateJavaDocForField((JavaField) f) );
    }


    private FPOption<JavaDoc> generateJavaDocForClass( JavaClass c ) {
        ClassJavadoc classDoc = RuntimeJavadoc.getJavadoc( c.getFullName() );
        if ( classDoc.isEmpty() ) {
            return FP.emptyOption();
        }


        List<String>     lines = toText(classDoc.getComment() ).toList();
        List<JavaDocTag> tags  = FP.wrap(classDoc.getOther()).map( this::toTag ).toList();

        return FP.option( new JavaDoc(lines, tags) );
    }

    private FPOption<JavaDoc> generateJavaDocForConstructor( JavaConstructor c ) {
        MethodJavadoc methodDoc = RuntimeJavadoc.getJavadoc( c.getJdkConstructor() );

        return getJavaDocsFor( methodDoc );
    }

    private FPOption<JavaDoc> generateJavaDocForMethod( JavaMethod jm ) {
        return jm.getJdkMethod()
                 .map( RuntimeJavadoc::getJavadoc )
                 .flatMap( this::getJavaDocsFor );
    }

    private FPOption<JavaDoc> getJavaDocsFor( MethodJavadoc methodDoc ) {
        if ( methodDoc.isEmpty() ) {
            return FP.emptyOption();
        }

        return toJavaDocFor( methodDoc );
    }

    private FPOption<JavaDoc> generateJavaDocForField( JavaField targetField ) {
        FieldJavadoc fieldDoc = RuntimeJavadoc.getJavadoc( targetField.getJdkField() );
        if ( fieldDoc.isEmpty() ) {
            return FP.emptyOption();
        }

        return toJavaDocFor( fieldDoc );
    }

    private FPOption<JavaDoc> toJavaDocFor( MethodJavadoc methodDoc ) {
        List<String> lines = toText( methodDoc.getComment() ).toList();
        FPIterable<JavaDocTag> tags1 = FP.wrap( methodDoc.getOther() ).map( this::toTag );
        FPIterable<JavaDocTag> tags2 = FP.wrap( methodDoc.getSeeAlso() ).map( this::toTag );
        FPIterable<JavaDocTag> tags3 = FP.wrap( methodDoc.getThrows() ).map( this::toTag );
        FPIterable<JavaDocTag> tags4 = FP.option( methodDoc.getReturns() ).flatMap( this::toReturnTag );
        List<JavaDocTag> tags = tags1.and(tags2).and(tags3).and(tags4).toList();

        return FP.option( new JavaDoc( lines, tags ) );
    }

    private FPOption<JavaDocTag> toReturnTag( Comment comment ) {
        if (comment == null) {
            return FP.emptyOption();
        }

        List<String> lines = toText( comment ).toList();

        return lines.isEmpty() ? FP.emptyOption() : FP.option(new JavaDocTag("return",lines));
    }

    private FPOption<JavaDoc> toJavaDocFor( BaseJavadoc doc ) {
        List<String> lines = toText( doc.getComment() ).toList();
        FPIterable<JavaDocTag> tags1 = FP.wrap( doc.getOther() ).map( this::toTag );
        FPIterable<JavaDocTag> tags2 = FP.wrap( doc.getSeeAlso() ).map( this::toTag );
        List<JavaDocTag> tags = tags1.and( tags2 ).toList();

        return FP.option( new JavaDoc( lines, tags ) );
    }

    private FPOption<JavaDoc> generateJavaDocForParameter( JavaParameter targetParameter ) {
        MethodJavadoc methodDoc = getJavadocFor( targetParameter.getSource() );
        if ( methodDoc.isEmpty() || targetParameter.getIndex() >= methodDoc.getParams().size() ) {
            return FP.emptyOption();
        }

        ParamJavadoc paramDoc = methodDoc.getParams().get(targetParameter.getIndex());
        List<String> text     = trimLeft(toText(paramDoc.getComment())).toList();

        return FP.option( new JavaDoc(text) );
    }

    private MethodJavadoc getJavadocFor( JavaExecutable executable ) {
        if ( executable instanceof JavaConstructor c ) {
            return RuntimeJavadoc.getJavadoc( c.getJdkConstructor() );
        } else if ( executable instanceof JavaMethod m ) {
            return RuntimeJavadoc.getJavadoc( m.getJdkMethod().get() );
        } else {
            throw new UnsupportedOperationException( executable.getClass().toString() );
        }
    }

    private FPIterable<String> toText( Comment comment ) {
        return FP.wrap(comment.getElements()).flatMap( this::toText );
    }

    private FPIterable<String> toText( CommentElement commentElement ) {
        if ( commentElement instanceof CommentText e ) {
            return trimLeft( splitText(renderText(e)) );
        } else if ( commentElement instanceof InlineLink e ) {
            return FP.wrapSingleton( renderLink(e) );
        } else if ( commentElement instanceof InlineValue e ) {
            return FP.wrapSingleton( renderValue(e) );
        } else if ( commentElement instanceof InlineTag e ) {
            return FP.wrapSingleton( renderTag(e) );
        } else {
            return trimLeft( splitText(renderUnrecognized(commentElement)) );
        }
    }

    private JavaDocTag toTag( SeeAlsoJavadoc tag ) {
        String text = switch (tag.getSeeAlsoType()) {
            case STRING_LITERAL -> tag.getStringLiteral();
            case HTML_LINK      -> tag.getHtmlLink().getLink();
            case JAVADOC_LINK   -> tag.getLink().getLabel();
        };

        return toTag( "see", StringUtils.isBlank(text) ? Collections.emptyList() : Collections.singletonList(text.trim()) );
    }

    private JavaDocTag toTag( OtherJavadoc tag ) {
        List<String> text = trimLeft(toText(tag.getComment())).toList();

        return toTag( tag.getName(), text );
    }

    private JavaDocTag toTag( ThrowsJavadoc tag ) {
        List<String> text = trimLeft(toText(tag.getComment())).toList();

        return new JavaDocTag( "throws", tag.getName(), text );
    }

    private JavaDocTag toTag( String tagName, List<String> text ) {
        if ( TAGS_WITH_ARGS.contains(tagName) ) {
            FPOption<String> tagArg       = extractTagArgFromText(text);
            List<String>     modifiedText = dropTagArgFromText(tagArg, text);

            return new JavaDocTag( tagName, tagArg, modifiedText );
        } else {
            return new JavaDocTag( tagName, text );
        }
    }

    private FPOption<String> extractTagArgFromText( List<String> text ) {
        return selectFirstNonBlankLineFrom(text).flatMap(this::selectFirstWordFrom);
    }

    private FPOption<String> selectFirstNonBlankLineFrom( List<String> text ) {
        return FP.wrap(text).first( StringUtils::isNotBlank );
    }

    private FPOption<String> selectFirstWordFrom( String text ) {
        String trimmedText = text.trim();
        int    i           = trimmedText.indexOf( ' ' );

        if ( i < 0 ) {
            return FP.option( trimmedText );
        } else {
            return FP.option( trimmedText.substring(0,i) );
        }
    }

    private List<String> dropTagArgFromText( FPOption<String> tagArg, List<String> text ) {
        if ( tagArg.isEmpty() ) {
            return text;
        }

        String arg       = tagArg.get();
        String argRegExp = Pattern.quote(arg);

        return trimLeft(FP.wrap(text).mapFirst(
            line -> line.contains(arg), line -> line.replaceFirst(argRegExp,"")
        )).toList();
    }

    private FPIterable<String> trimLeft( FPIterable<String> toText ) {
        return toText.map( StringUtils::trimLeft );
    }

    private String renderText( CommentText e ) {
        return e.getValue();
    }

    private String renderLink( InlineLink e ) {
        return e.getLink().toString();
    }

    private String renderValue( InlineValue e ) {
        return e.getValue().getReferencedMemberName() == null ? "" : e.getValue().toString();
    }

    private String renderTag( InlineTag e ) {
        return switch (e.getName()) {
            case    "code", "literal" -> e.getValue();
            default                   -> "{@"+e.getName()+" "+e.getValue()+"}";
        };
    }

    private String renderUnrecognized( CommentElement commentElement ) {
        return commentElement.toString();
    }

    private FPIterable<String> splitText( String newText ) {
        String[] extraLines = newText.split( "\\r?\\n ?" );

        return FP.wrapArray(extraLines);
    }
}
