package mosaics.lang.reflection;

import lombok.Value;
import lombok.With;
import mosaics.fp.FP;
import mosaics.fp.collections.FPOption;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;


@Value
@With
public class JavaDoc {
    private List<String>     text;
    private List<JavaDocTag> tags;

    public JavaDoc( String...text ) {
        this( Arrays.asList(text) );
    }

    public JavaDoc( List<String> text ) {
        this( text, Collections.emptyList() );
    }

    public JavaDoc( List<String> text, List<JavaDocTag> tags ) {
        this.text = text;
        this.tags = tags;
    }

    public FPOption<JavaDocTag> getTag( String tagNameTarget ) {
        return FP.wrap(tags).first(t -> tagNameTarget.equals(t.getTagName()));
    }

    public FPOption<JavaDocTag> getTag( String targetTagName, String targetTagArg ) {
        FPOption<String> targetTagArgOpt = FP.option( targetTagArg );

        return FP.wrap(tags).first(t -> targetTagName.equals(t.getTagName()) && targetTagArgOpt.equals(t.getTagArg()));
    }

}
