package mosaics.lang;

import mosaics.fp.Failure;
import lombok.Value;


@Value
public class ValidationFailure implements Failure {
    private String msg;

    public Throwable toException() {
        return new ValidationException( msg );
    }
}
