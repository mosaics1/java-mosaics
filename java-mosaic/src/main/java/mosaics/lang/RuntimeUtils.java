package mosaics.lang;

public class RuntimeUtils {

    /**
     * Convenience util for writing the key and value of all System properties to stdout.
     */
    public void dumpSystemProperties() {
        System.getProperties().keySet().forEach( key -> System.out.println(key + " -> " + System.getProperty(key.toString())) );
    }

}
