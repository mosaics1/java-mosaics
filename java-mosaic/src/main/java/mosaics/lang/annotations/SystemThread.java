package mosaics.lang.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * Marks a thread as a System thread.  System threads are background daemon threads that stay in
 * the background for the entire duration of the JVM doing periodic work.  They are singletons,
 * thus only a max of one instance of each one can be around at a time.
 *
 * Threads marked as a System thread will not be flagged as an error by the ThreadCleanupAsserter.
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface SystemThread {
}
