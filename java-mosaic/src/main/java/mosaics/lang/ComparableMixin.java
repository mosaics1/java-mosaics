package mosaics.lang;



@SuppressWarnings("unchecked")
public interface ComparableMixin<T extends ComparableMixin> extends Comparable<T> {
    public default T max( T b ) {
        return this.compareTo(b) >= 0 ? (T) this : b;
    }

    public default T min( T b ) {
        return this.compareTo(b) <= 0 ? (T) this : b;
    }

    public default boolean isGT( T b ) {
        return this.compareTo(b) > 0;
    }

    public default boolean isGTE( T b ) {
        return this.compareTo(b) >= 0;
    }

    public default boolean isLT( T b ) {
        return this.compareTo(b) < 0;
    }

    public default boolean isLTE( T b ) {
        return this.compareTo(b) <= 0;
    }
}
