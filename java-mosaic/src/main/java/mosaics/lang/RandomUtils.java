package mosaics.lang;

import mosaics.regex.RegExp;

import java.util.Objects;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;


public class RandomUtils {

    private static final Random rnd = ThreadLocalRandom.current();

    /**
     * Return a number between 0 and v-1.
     */
    public static int rnd(int v) {
        return rnd.nextInt( v );
    }

    public static int rnd( int min, int max ) {
        int diff = max - min;

        return rnd.nextInt(diff) + min;
    }

    public static double rnd( double min, double max ) {
        double diff = max - min;

        return rnd.nextDouble()*diff + min;
    }

    @SafeVarargs
    public static <T> T rnd( T...values ) {
        int i = rnd(values.length);

        return values[i];
    }

    @SafeVarargs
    public static <T> T not( T notThisValue, T...values ) {
        int maxAttempts = 10;
        int attemptCount = 0;

        T rndValue;

        do {
            rndValue = rnd(values);
            attemptCount += 1;
        } while ( Objects.equals(notThisValue,rndValue) && attemptCount < maxAttempts );

        if ( Objects.equals(notThisValue,rndValue) ) {
            throw new IllegalStateException( "failed to randomly select a new value" );
        }

        return rndValue;
    }

    public static String randomString( String regExp, int maxLength ) {
        RegExp r = new RegExp( regExp );

        int attemptCount = 0;
        do {
            String str = randomString( maxLength );

            if ( r.matches(str) ) {
                return str;
            }

            attemptCount++;
        } while ( attemptCount < 1000 );

        throw new IllegalStateException();
    }

    public static String randomString( int maxLength ) {
        int           len = rnd( maxLength );
        StringBuilder buf = new StringBuilder( len );

        for ( int i=0; i<len; i++ ) {
            buf.append( randomChar() );
        }

        return buf.toString();
    }

    public static char randomChar() {
        return (char) rnd('0','z');
    }
}
