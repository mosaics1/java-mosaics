package mosaics.utils;

import mosaics.lang.Backdoor;
import mosaics.lang.QA;
import mosaics.lang.functions.Function1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;


@SuppressWarnings("unchecked")
public class MapUtils {

    /**
     * Creates an empty Map that will create a default value for every key that is requested
     * that does not yet have a value by invoking the provided factory method.
     */
    public static <K,V> Map<K,V> createWithDefault( Function1<K,V> defaultValueFactory ) {
        return new ConcurrentHashMap<K,V>() {
            @Override
            public V get( Object key ) {
                V v = super.get( key );

                if ( v == null ) {
                    K k = Backdoor.cast(key);

                    v = defaultValueFactory.invoke( k );

                    super.put( k, v );
                }

                return v;
            }
        };
    }

    public static <K,V> TreeMap<K,V> createTreeMapWithDefault( Function1<K,V> defaultValueFactory ) {
        return new TreeMap<K,V>() {
            @Override
            public V get( Object key ) {
                V v = super.get( key );

                if ( v == null ) {
                    K k = Backdoor.cast( key );

                    v = defaultValueFactory.invoke( k );

                    super.put( k, v );
                }

                return v;
            }
        };
    }

    /**
     * Append the specified V to a list of values under the specified K.  If the
     * key is not in the map, then create a new list for it.
     */
    public static <K,V> void appendToMultiMap( Map<K, List<V>> map, K k, V v ) {
        List<V> list = map.get(k);

        if ( list == null ) {
            list = new ArrayList<>();
            map.put(k, list);
        }

        list.add( v );
    }

    /**
     * Util for creating hash maps.  Offers convenience over type safety.
     *
     * @param kvPairs an array of key/value pairs. For example ["a",1,"b",2] would
     *                create a hash map containing key a mapping to value 1 and
     *                key b mapping to value 2.
     */
    public static <K,V> Map<K,V> asMap( Object...kvPairs ) {
        QA.isTrueState(kvPairs.length % 2 == 0, "kvPairs must be an even length of kv encoded pairs: $v", "v", Arrays.asList(kvPairs));

        Map<K,V> result = new HashMap<>();
        for ( int i=0; i<kvPairs.length; i+=2 ) {
            result.put( (K) kvPairs[i], (V) kvPairs[i+1] );
        }

        return result;
    }

    public static <K,V> Map<K,V> asLinkedMap( Object...kvPairs ) {
        QA.isTrueState(kvPairs.length % 2 == 0, "kvPairs must be an even length of kv encoded pairs: $v", "v", Arrays.asList(kvPairs));

        Map result = new LinkedHashMap();
        for ( int i=0; i<kvPairs.length; i+=2 ) {
            result.put( kvPairs[i], kvPairs[i+1] );
        }

        return (Map<K,V>) result;
    }

    public static <K,V> Map<K, V> asTreeMap( Map<K,V> source ) {
        Map<K,V> result = new TreeMap<>();

        for ( Map.Entry<K,V> e : source.entrySet() ) {
            result.put( e.getKey(), e.getValue() );
        }

        return result;
    }

    /**
     * Return a copy of a that has had all elements of b appended.
     */
    public static <K,V> Map<K,V> copyAndMerge( Map<K,V> a, Map<K,V> b ) {
        Map<K,V> copy = new HashMap<>(a);

        copy.putAll( b );

        return copy;
    }
}
