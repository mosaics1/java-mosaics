package mosaics.utils;

/**
 *
 */
public class BitUtils {

    public static int bitSet( int bits, int mask ) {
        return bits | mask;
    }

    public static int bitClear( int bits, int mask ) {
        return bits & (~mask);
    }

}
