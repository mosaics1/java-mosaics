package mosaics.utils;

import mosaics.fp.collections.FPOption;
import mosaics.fp.collections.None;
import mosaics.lang.ArrayUtils;
import mosaics.lang.functions.VoidFunction1;

import java.util.Map;
import java.util.Optional;


public class CollectionUtils {

    /**
     * Iterate over every element contained in v, and in turn if any of those elements are also
     * collections then iterate over each of those elements.  Action is invoked for each of the
     * leaf elements starting at the "bottom left" of the collection graph.
     *
     * This method does not check for cycles.
     */
    @SuppressWarnings("unchecked")
    public static void depthFirstTraversal( Object v, VoidFunction1 action ) {
        if ( v == null || v == FPOption.none() || v == Optional.empty() ) {
            return;
        } else if ( v instanceof Iterable ) {
            ((Iterable) v).forEach( e -> depthFirstTraversal(e, action) );
        } else if ( v.getClass() == Optional.class ) {
            Optional opt = (Optional) v;

            if ( opt.isPresent() ) {
                depthFirstTraversal( opt.get(), action );
            }
        } else if ( v.getClass().isArray() ) {
            ArrayUtils.forEach( v, e -> depthFirstTraversal(e, action) );
        } else if ( v instanceof Map ) {
            ((Map<?,?>) v).entrySet().stream()
                .filter( kv -> hasContents(kv.getValue()) )
                .forEach( kv -> depthFirstTraversal(kv,action) );
        } else {
            action.invoke( v );
        }
    }

    public static <T> boolean isEmpty( T object ) {
        if ( object == null ) {
            return true;
        } else if ( object instanceof Optional ) {
            return !((Optional) object).isPresent();
        } else if ( object instanceof None ) {
            return true;
        } else if ( object instanceof String ) {
            return ((String) object).trim().length() == 0;
        }

        return false;
    }

    public static <T> boolean hasContents( T object ) {
        return !isEmpty( object );
    }

}
