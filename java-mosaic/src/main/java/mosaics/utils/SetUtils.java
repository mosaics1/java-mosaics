package mosaics.utils;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;


public class SetUtils {

    @SafeVarargs
    public static <T> Set<T> asSet( T...elements ) {
        return Arrays.stream( elements ).collect( Collectors.toSet() );
    }

    @SafeVarargs
    public static <T> Set<T> asImmutableSet( T...elements ) {
        return Collections.unmodifiableSet(asSet(elements));
    }

}
