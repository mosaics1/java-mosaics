package mosaics.utils;

import java.lang.management.ManagementFactory;


public class OSUtils {

    public static int getCurrentPid() {
        // java 9:  ProcessHandle.current().pid();

        String runtimeName = ManagementFactory.getRuntimeMXBean().getName();

        return Integer.parseInt( upto(runtimeName, '@') );
    }


    private static String upto( String str, char c ) {
        if ( str == null ) {
            return null;
        }

        int i = str.indexOf(c);

        return i >= 0 ? str.substring( 0, i ) : str;
    }
}
