package mosaics.utils;

import mosaics.lang.functions.LongFunction1;

import java.util.Comparator;
import java.util.function.Function;

import static mosaics.lang.Backdoor.cast;


public class ComparatorUtils {
    public static final int LT = -1;
    public static final int EQ = 0;
    public static final int GT = 1;

    public static <T> Comparator<T> and( Comparator<T> a, Comparator<T> b ) {
        return (o1,o2) -> {
            int ac = a.compare( o1, o2 );
            if ( ac == 0 ) {
                return b.compare( o1, o2 );
            }

            return ac;
        };
    }

    public static <T> Comparator<T> identityComparator() {
        return new Comparator<T>() {
            public int compare( T o1, T o2 ) {
                if ( o1 == o2 ) {
                    return EQ;
                }

                return compareAsc( System.identityHashCode(o1), System.identityHashCode(o2) );
            }
        };
    }

    public static <T> Comparator<T> compareAscLong( LongFunction1<T> extractValueFunction ) {
        return new Comparator<T>() {
            public int compare( T o1, T o2 ) {
                long a = extractValueFunction.invoke(o1);
                long b = extractValueFunction.invoke(o2);

                return compareAsc( a, b );
            }
        };
    }

    public static <T extends Comparable<T>> int compareAsc( T a, T b ) {
        int comparisonResult = a.compareTo( b );
        if ( comparisonResult < 0 ) {
            return LT;
        }

        return comparisonResult > 0 ? GT : EQ;
    }

    public static <T extends Comparable<T>> int compareDesc( T a, T b ) {
        return compareAsc( b, a );
    }

    public static int compareAsc( long a, long b ) {
        if ( a < b ) {
            return LT;
        }

        return a > b ? GT : EQ;
    }

    public static int compareDesc( long a, long b ) {
        return compareAsc( b, a );
    }

    public static int compareAsc( byte a, byte b ) {
        if ( a < b ) {
            return LT;
        }

        return a > b ? GT : EQ;
    }

    public static int compareDesc( byte a, byte b ) {
        return compareAsc( b, a );
    }

    public static int compareAsc( String a, String b ) {
        return a.compareTo(b);
    }

    public static int compareDesc( String a, String b ) {
        return compareAsc( b, a );
    }

    public static int compareAsc( float a, float b ) {
        if ( a < b ) {
            return LT;
        }

        return a > b ? GT : EQ;
    }

    public static int compareAsc( double a, double b ) {
        if ( a < b ) {
            return LT;
        }

        return a > b ? GT : EQ;
    }

    public static int compareDesc( float a, float b ) {
        return compareAsc( b, a );
    }

    public static int compareDesc( double a, double b ) {
        return compareAsc( b, a );
    }

    public static <T> Comparable<T> toComparable( T v ) {
        if ( v instanceof Comparable<?> c ) {
            return cast(c);
        } else {
            return cast(v.toString());
        }
    }

    public static <T,V> Comparator<T> comparing(
        Function<T, V> keyExtractor)
    {
        return Comparator.comparing( v -> cast(toComparable(keyExtractor.apply(v))) );
    }
}
