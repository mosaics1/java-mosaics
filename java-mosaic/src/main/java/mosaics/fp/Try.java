package mosaics.fp;


import mosaics.lang.Backdoor;
import mosaics.lang.functions.Function0WithThrows;
import mosaics.lang.functions.Function1;
import mosaics.lang.functions.VoidFunction0;
import mosaics.lang.functions.VoidFunction0WithThrows;
import mosaics.lang.functions.VoidFunction1;
import lombok.Value;


/**
 * A Try is similar to the try block in Java.  The Try executes a closure and captures its result,
 * successful or not.
 */
public class Try {

    private static final Tryable<Void> SUCCEEDED_VOID = succeeded( null );

    public static final Tryable<String> BLANK_STRING_SUCCESS = Try.succeeded( "" );

    public static Tryable<Void> invokeVoid( VoidFunction0 f ) {
        try {
            f.invoke();

            return new Succeeded<>(null);
        } catch ( Throwable ex ) {
            return failed(ex);
        }
    }

    public static Tryable<Void> invoke( VoidFunction0WithThrows f ) {
        try {
            f.invoke();

            return new Succeeded<>(null);
        } catch ( Throwable ex ) {
            return failed(ex);
        }
    }

    public static <T> Tryable<T> invoke( Function0WithThrows<T> f ) {
        try {
            T result = f.invoke();

            return new Succeeded<>(result);
        } catch ( Throwable ex ) {
            return failed(ex);
        }
    }

    public static <T> Tryable<T> flatInvoke( Function0WithThrows<Tryable<T>> f ) {
        try {
            return f.invoke();
        } catch ( Throwable ex ) {
            return failed(ex);
        }
    }

    public static Tryable<Void> succeeded() {
        return SUCCEEDED_VOID;
    }

    public static <T> Tryable<T> succeeded( T result ) {
        return new Succeeded<>( result );
    }

    public static <T> Tryable<T> failed( String msg, Object...args ) {
        return failed( new ExceptionFailure(msg,args) );
    }

    public static <T> Tryable<T> failed( Throwable ex ) {
        return failed( new ExceptionFailure( ex ) );
    }

    public static <T> Tryable<T> failed( Failure f ) {
        return new Failed<>( f );
    }


    @Value
    private static class Succeeded<T> implements Tryable<T> {
        private final T result;


        public boolean isComplete() {
            return true;
        }

        public boolean hasResult() {
            return true;
        }

        public boolean hasFailure() {
            return false;
        }

        public T getResult() {
            return result;
        }

        public Failure getFailure() {
            throw new IllegalStateException();
        }

        public <B> Tryable<B> mapResult( Function1<T, B> mappingFunction ) {
            try {
                return new Succeeded<>( mappingFunction.invoke( result ) );
            } catch ( Exception ex ) {
                return failed(ex);
            }
        }

        public <B> Tryable<B> flatMapResult( Function1<T, Tryable<B>> mappingFunction ) {
            try {
                return mappingFunction.invoke( result );
            } catch ( Exception ex ) {
                return failed(ex);
            }
        }

        public Tryable<T> recover( Function1<Failure, T> recoveryFunction ) {
            return this;
        }

        public Tryable<T> flatRecover( Function1<Failure, Tryable<T>> recoveryFunction ) {
            return this;
        }

        public Tryable<T> mapFailure( Function1<Failure, Failure> mappingFunction ) {
            return this;
        }

        public Tryable<T> flatMapFailure( Function1<Failure, Tryable<T>> mappingFunction ) {
            return this;
        }

        public Tryable<T> onSuccess( VoidFunction1<T> callback ) {
            callback.invoke( result );

            return this;
        }

        public Tryable<T> onFailure( VoidFunction1<Failure> callback ) {
            return this;
        }

        public Tryable<T> throwIffFailure() {
            return this;
        }
    }

    @Value
    private static class Failed<T> implements Tryable<T> {
        private final Failure failure;


        public boolean isComplete() {
            return true;
        }

        public boolean hasResult() {
            return false;
        }

        public boolean hasFailure() {
            return true;
        }

        public T getResult() {
            throw Backdoor.throwException( failure.toException() );
        }

        public Failure getFailure() {
            return failure;
        }

        public <B> Tryable<B> mapResult( Function1<T, B> mappingFunction ) {
            return Backdoor.cast( this );
        }

        public <B> Tryable<B> flatMapResult( Function1<T, Tryable<B>> mappingFunction ) {
            return Backdoor.cast( this );
        }

        public Tryable<T> recover( Function1<Failure, T> recoveryFunction ) {
            try {
                return new Succeeded<>( recoveryFunction.invoke( failure ) );
            } catch ( Exception ex ) {
                return failed( ex );
            }
        }

        public Tryable<T> flatRecover( Function1<Failure, Tryable<T>> recoveryFunction ) {
            try {
                return recoveryFunction.invoke( failure );
            } catch ( Exception ex ) {
                return failed( ex );
            }
        }

        public Tryable<T> mapFailure( Function1<Failure, Failure> mappingFunction ) {
            try {
                return failed( mappingFunction.invoke( failure ) );
            } catch ( Exception ex ) {
                return failed( ex );
            }
        }

        public Tryable<T> flatMapFailure( Function1<Failure, Tryable<T>> mappingFunction ) {
            try {
                return mappingFunction.invoke(failure);
            } catch ( Exception ex ) {
                return failed( ex );
            }
        }

        public Tryable<T> onSuccess( VoidFunction1<T> callback ) {
            return this;
        }

        public Tryable<T> onFailure( VoidFunction1<Failure> callback ) {
            callback.invoke( failure );

            return this;
        }

        public Tryable<T> throwIffFailure() {
            throw Backdoor.throwException( failure.toException() );
        }
    }

}
