package mosaics.fp;

import mosaics.fp.collections.FPOption;
import mosaics.lang.Backdoor;

import java.util.Objects;


/**
 * Represents an error.  Cheaper and less exceptional than java.lang.Exception and less
 * fatal than java.lang.Error; making this class suitable for representing less
 * exceptional cases such as parse exceptions for human input and so forth.<p/>
 *
 * That said, a Failure may still be caused by a more expensive exception and thus
 * this class can also wrap an instance of java.lang.Throwable.<p/>
 *
 * Failures may also be chained together, after all an error may occur while handing
 * a failure.  In which case we do not want to loose information on the root of
 * the problem.
 */
public class ExceptionFailure implements Failure {

    private String    message;
    private Throwable ex;
    private Failure   chained;


    public ExceptionFailure( String message, Object...args ) {
        this.message = String.format(message, args);
    }


    public ExceptionFailure( Throwable ex ) {
        this.message = ex.getClass().getName() + ": " + ex.getMessage();
        this.ex      = ex;
    }

    /**
     * Chain a series of failures together.  While handling the failure f a new
     * exception ex occurred.
     */
    public ExceptionFailure( Throwable newException, Failure previousFailure ) {
        this.message = previousFailure.toString();
        this.chained = previousFailure;
        this.ex      = newException;
    }

    /**
     * Chain a series of failures together.  While handling the failure f a new
     * exception ex occurred.
     */
    public ExceptionFailure( ExceptionFailure newFailure, Failure previousFailure ) {
        this.message = newFailure.getMessage();
        this.chained = previousFailure;
    }

    /**
     * A free text description of the problem.
     */
    public String getMessage() {
        return message;
    }

    public Throwable toException() {
        return ex == null ? new RuntimeException(message) : ex;
    }

    public Throwable getException() {
        return ex;
    }

    public Failure getChained() {
        return chained;
    }

    public <T extends Throwable> FPOption<T> toException( Class<T> type ) {
        if ( ex.getClass().isAssignableFrom(type) ) {
            return FP.option( Backdoor.cast( toException() ) );
        } else {
            return FP.emptyOption();
        }
    }

    @Override
    public int hashCode() {
        return message.hashCode();
    }

    public String toString() {
        return getMessage();
    }

    public boolean equals( Object o ) {
        if ( !(o instanceof ExceptionFailure) ) {
            return false;
        } else if ( o == this ) {
            return true;
        }

        ExceptionFailure other = (ExceptionFailure) o;
        return Objects.equals(this.message, other.message)
            && Objects.equals(this.chained, other.chained)
            && equalsExceptions(this.ex, other.ex);
    }

    private boolean equalsExceptions( Throwable a, Throwable b ) {
        if ( a == b ) {
            return true;
        } else if ( a == null || b == null ) {
            return false;
        }

        return Objects.equals(a.getClass(), b.getClass() )
            && Objects.equals(a.getMessage(), b.getMessage() );
    }

}
