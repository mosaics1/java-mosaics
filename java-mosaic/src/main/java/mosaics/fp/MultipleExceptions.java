package mosaics.fp;

import lombok.EqualsAndHashCode;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;


@EqualsAndHashCode(callSuper = false)
public class MultipleExceptions extends RuntimeException {

    private Throwable[] exceptions;

    public MultipleExceptions( Throwable...exceptions ) {
        super( exceptions.length+" exceptions occurred" );

        this.exceptions = exceptions;
    }

    public List<Throwable> getExceptions() {
        return Arrays.asList(exceptions);
    }

    public void printStackTrace( PrintWriter s ) {
        s.println(exceptions.length+" exceptions occurred");

        for ( int i=0; i<=exceptions.length; i++ ) {
            s.print( i );
            s.print( ": ");

            exceptions[i].printStackTrace(s);
        }

        s.println('.');
    }

    public void printStackTrace( PrintStream s ) {
        s.println(exceptions.length+" exceptions occurred");

        for ( int i=0; i<=exceptions.length; i++ ) {
            s.print( i );
            s.print( ": ");

            exceptions[i].printStackTrace(s);
        }

        s.println('.');
    }
}
