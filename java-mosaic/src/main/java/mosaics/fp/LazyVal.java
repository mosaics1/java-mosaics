package mosaics.fp;

import mosaics.fp.collections.FPOption;
import mosaics.lang.functions.Function0;
import lombok.Synchronized;

import java.util.concurrent.atomic.AtomicReference;


/**
 * Lazily fetches a value in a thread safe manner.  The fetcher is guaranteed to only be invoked once
 * unless an exception is thrown by the fetcher.  In which case each call to fetch will invoke the
 * fetcher again until no exception is thrown.
 */
public class LazyVal<T> {

    private final Function0<T> fetcher;
    private final AtomicReference<FPOption<T>> ref = new AtomicReference<>(FPOption.none());


    public LazyVal( Function0<T> fetcher ) {
        this.fetcher = fetcher;
    }


    public T fetch() {
        FPOption<T> v = ref.get();

        if ( v.isEmpty() ) {
            v = doFetch();
        }

        return v.get();
    }

    @Synchronized
    public void clear() {
        ref.set( FPOption.none() );
    }

    @Synchronized
    private FPOption<T> doFetch() {
        FPOption<T> v = ref.get();

        if ( v.hasValue() ) {
            return v;  // race condition -- somebody else fetched it while we were blocked
        }

        v = FPOption.of( fetcher.invoke() );
        ref.set( v );

        return v;
    }

}
