package mosaics.fp;

import mosaics.fp.collections.FPOption;
import mosaics.lang.Backdoor;


public interface Failure {
    public static Failure toFailure( Throwable ex ) {
        return new ExceptionFailure(ex);
    }

    public Throwable toException();

    public default <T extends Throwable> FPOption<T> toException( Class<T> type ) {
        Throwable ex = toException();

        if ( ex.getClass().isAssignableFrom(type) ) {
            return FP.option( Backdoor.cast(ex) );
        } else {
            return FP.emptyOption();
        }
    }
}
