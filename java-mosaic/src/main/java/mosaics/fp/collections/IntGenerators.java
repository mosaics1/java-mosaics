package mosaics.fp.collections;

/**
 *
 */
public class IntGenerators {
    public static IntIterator incrementingId( int startsFrom ) {
        return new IntIterator() {
            private int nextInt = startsFrom;

            public boolean hasNext() {
                return true;
            }

            public int next() {
                return nextInt++;
            }
        };
    }
}
