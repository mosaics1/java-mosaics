package mosaics.fp.collections;


import mosaics.lang.functions.Function1;
import mosaics.lang.functions.Function2;

import java.util.Objects;

import static mosaics.lang.Backdoor.cast;


public class Tuple2<V1,V2> {

    private final V1 v1;
    private final V2 v2;

    public Tuple2( V1 v1, V2 v2 ) {
        this.v1 = v1;
        this.v2 = v2;
    }

    public V1 getFirst() {
        return v1;
    }

    public V2 getSecond() {
        return v2;
    }

    public boolean doAllValuesMatch() {
        return Objects.equals( v1, v2 );
    }

    public <C> Tuple2<C,V2> mapFirst( Function1<V1,C> f ) {
        C updatedV1 = f.invoke( v1 );

        return Objects.equals(this.v1,updatedV1) ? cast(this) : new Tuple2<>( updatedV1,v2 );
    }

    public <C> Tuple2<V1,C> mapSecond( Function1<V2,C> f ) {
        C updatedV2 = f.invoke( v2 );

        return Objects.equals(this.v2,updatedV2) ? cast(this) : new Tuple2<>( v1,updatedV2 );
    }

    public <C> C flatMap( Function2<V1,V2,C> f ) {
        C r = f.invoke( v1, v2 );

        return Objects.equals(this,r) ? cast(this) : r;
    }

    public String toString() {
        return "("+v1+","+v2+")";
    }


    public int hashCode() {
        return Objects.hash(v1, v2);
    }

    public boolean equals( Object o ) {
        if ( !(o instanceof Tuple2) ) {
            return false;
        } else if ( o == this ) {
            return true;
        }

        Tuple2 other = (Tuple2) o;
        return Objects.equals(this.v1, other.v1) && Objects.equals(this.v2, other.v2);
    }
}
