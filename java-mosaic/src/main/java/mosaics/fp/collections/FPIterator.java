package mosaics.fp.collections;

import lombok.SneakyThrows;
import mosaics.fp.FP;
import mosaics.fp.collections.maps.FPMutableMap;
import mosaics.lang.ArrayUtils;
import mosaics.lang.functions.BooleanFunction1;
import mosaics.lang.functions.BooleanFunction2;
import mosaics.lang.functions.Function1;
import mosaics.lang.functions.Function2;
import mosaics.lang.functions.LongFunction1;
import mosaics.lang.Backdoor;
import mosaics.lang.functions.VoidFunction1WithThrows;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;


@SuppressWarnings({"Duplicates", "unchecked"})
public abstract class FPIterator<T> implements Iterator<T> {

    public static <T> FPIterator<T> wrap( T...elements ) {
        return wrap( Arrays.asList(elements) );
    }

    public static <T> FPIterator<T> wrap( Enumeration<T> en ) {
        return wrap( new Iterator() {
            @Override
            public boolean hasNext() {
                return en.hasMoreElements();
            }

            @Override
            public Object next() {
                return en.nextElement();
            }
        } );
    }

    public static <T> FPIterator<T> wrap( Iterable<T> it ) {
        return wrap( it.iterator() );
    }

    public static <T> FPIterator<T> wrap( Iterator<T> it ) {
        return new FPIterator<T>() {
            protected boolean _hasNext() {
                return it.hasNext();
            }

            protected T _next() {
                return it.next();
            }
        };
    }

    public static <T> FPIterator<T> emptyIterator() {
        return new FPIterator<T>() {
            protected boolean _hasNext() {
                return false;
            }

            protected T _next() {
                throw new UnsupportedOperationException();
            }

            public String toString() {
                return "EmptyFPIterator";
            }
        };
    }


    private FPOption<T> cachedNextValue = FPOption.none();

    public static <T> FPIterator<T> createIteratorFor( Object x ) {
        if (x == null) {
            return emptyIterator();
        } else if ( x.getClass().isArray() ) {
            return arrayIterator( x );
        } else if ( x instanceof Iterable ) {
            return wrap( (Iterable<T>) x );
        } else {
            return singletonIterator( Backdoor.cast(x));
        }
    }

    public static <T> FPIterator<T> arrayIterator( Object array ) {
        return new FPIterator<T>() {
            private int maxExc = Array.getLength( array );
            private int i;

            protected boolean _hasNext() {
                return i < maxExc;
            }

            protected T _next() {
                return Backdoor.cast( Array.get(array,i++) );
            }
        };
    }

    public static <T> FPIterator<T> singletonIterator( T v ) {
        return new FPIterator<T>() {
            private boolean spent;

            protected boolean _hasNext() {
                return !spent;
            }

            protected T _next() {
                if ( spent ) {
                    throw new IllegalStateException( "next() called at end of iterable" );
                }

                spent = true;

                return v;
            }
        };
    }

    public T peek() {
        if ( !cachedNextValue.hasValue() ) {
            cachedNextValue = FPOption.of(next());
        }

        return cachedNextValue.get();
    }

    public boolean hasNext() {
        return cachedNextValue.hasValue() || _hasNext();
    }

    public void skip() {
        next();
    }

    public T next() {
        if ( !cachedNextValue.hasValue() ) {
            return _next();
        }

        T v = cachedNextValue.get();

        cachedNextValue = FPOption.none();

        return v;
    }


    protected abstract boolean _hasNext();
    protected abstract T _next();


    /**
     * Returns how many items were in the iterator.  It will consume the iterator in the process
     * of counting.
     */
    public int count() {
        int count = 0;

        while ( hasNext() ) {
            count += 1;

            next();
        }

        return count;
    }

    public FPOption<T> min( LongFunction1<T> costFunction ) {
        FPOption<T> resultSoFar  = FP.emptyOption();
        long        minCostSoFar = Long.MAX_VALUE;

        while ( hasNext() ) {
            T    candidate      = next();
            long candidatesCost = costFunction.invoke( candidate );

            if ( candidatesCost < minCostSoFar ) {
                resultSoFar  = FP.option( candidate );
                minCostSoFar = candidatesCost;
            }
        }

        return resultSoFar;
    }

    public FPIterator<List<T>> partitionAsWeGo( BooleanFunction2<T,T> partitionFunction ) {
        FPIterator<T> it = this;

        return new FPIterator<List<T>>() {
            protected boolean _hasNext() {
                return it.hasNext();
            }

            protected List<T> _next() {
                List<T> results = new ArrayList<>();

                T prevValue = it.peek();
                while ( it.hasNext() ) {
                    T nextValue = it.peek();

                    if ( partitionFunction.invoke(prevValue,nextValue) ) {
                        it.next();

                        results.add(nextValue);

                        prevValue = nextValue;
                    } else {
                        return results;
                    }
                }

                return results;
            }
        };
    }

    public Set<T> toSet() {
        Set<T> set = new HashSet<>();

        forEachRemaining( set::add );

        return set;
    }

    public List<T> toList() {
        List<T> list = new ArrayList<>();

        forEachRemaining( list::add );

        return list;
    }

    public ConsList<T> toConsList() {
        ConsList<T> list = FP.nil();

        while ( hasNext() ) {
            list = list.append( next() );
        }

        return list;
    }

    public <D> FPIterator<D> map( Function1<T,D> mapper ) {
        FPIterator<T> orig = this;

        return new FPIterator<D>() {
            protected boolean _hasNext() {
                return orig.hasNext();
            }

            protected D _next() {
                return mapper.invoke( orig.next() );
            }
        };
    }

    public <D> FPIterator<D> flatMap( Function1<T,? extends Iterable<? extends D>> mappingFunction ) {
        return flatMapIterator( mappingFunction.mapResult(Iterable::iterator) );
    }

    public <D> FPIterator<D> flatMapIterator( Function1<T,? extends Iterator<? extends D>> mappingFunction ) {
        FPIterator<T> main = this;

        return new FPIterator<>() {
            private FPIterator<? extends Iterator<? extends D>> mainIterator = main.map(mappingFunction).filter( e -> e != null && e.hasNext());
            private Iterator<? extends D>                       subIterator  = Collections.emptyIterator();

            public boolean _hasNext() {
                return subIterator.hasNext() || mainIterator.hasNext();
            }

            public D _next() {
                if ( !subIterator.hasNext() && mainIterator.hasNext() ) {
                    subIterator = mainIterator.next();
                }

                return subIterator.next();
            }
        };
    }

    public <K,V> FPMutableMap<K,V> toMap() {
        FPMutableMap map = FPMutableMap.newMap();

        while ( hasNext() ) {
            T value = next();

            if ( value instanceof Map.Entry ) {
                Map.Entry e = (Map.Entry) value;
                map.put( e.getKey(), e.getValue() );
            } else if ( value instanceof Tuple2 ) {
                Tuple2 tuple = (Tuple2) value;
                map.put( tuple.getFirst(), tuple.getSecond() );
            } else {
                throw new IllegalArgumentException( "Unknown type: " + value.getClass() );
            }
        }

        return map;
    }

    public String mkString() {
        return mkString("[", ", ", "]");
    }

    public String mkString( String separator ) {
        return mkString("", separator, "");
    }

    public String mkString( String prefix, String separator, String postfix ) {
        StringBuilder buf = new StringBuilder();

        buf.append(prefix);

        boolean requiresSeparator = false;
        while ( hasNext() ) {
            if ( requiresSeparator ) {
                buf.append( separator );
            } else {
                requiresSeparator = true;
            }

            buf.append(next());
        }

        buf.append(postfix);

        return buf.toString();
    }

    public boolean contains( T targetValue ) {
        while ( hasNext() ) {
            T v = next();

            if ( Objects.equals(v,targetValue) ) {
                return true;
            }
        }

        return false;
    }

    /**
     * Combine two iterators.  When one iterator completes before the other iterator, then
     * the gaps will be filled in with FP.emptyOption().
     */
    public <B> FPIterator<Tuple2<FPOption<T>,FPOption<B>>> zipOpt( FPIterator<B> otherIt ) {
        FPIterator<T> it1 = this;
        FPIterator<B> it2 = otherIt;

        return new FPIterator<>() {
            protected boolean _hasNext() {
                return it1.hasNext() || it2.hasNext();
            }

            protected Tuple2<FPOption<T>, FPOption<B>> _next() {
                FPOption<T> a = it1.hasNext() ? FP.option(it1.next()) : FP.emptyOption();
                FPOption<B> b = it2.hasNext() ? FP.option(it2.next()) : FP.emptyOption();

                return new Tuple2<>( a, b );
            }
        };
    }

    public FPIterator<T[]> slice( Class<T> expectedType, int windowSize ) {
        Iterator<T> it = this;

        return new FPIterator<T[]>() {
            private T[] next = calculateNext();


            protected boolean _hasNext() {
                return next != null;
            }

            protected T[] _next() {
                T[] next = this.next;

                this.next = calculateNext();
                return next;
            }

            private T[] calculateNext() {
                T[] r = ArrayUtils.newArray(expectedType, windowSize);
                int    i = 0;

                while ( i < windowSize && it.hasNext() ) {
                    r[i++] = it.next();
                }

                if ( i != windowSize ) {
                    r = Arrays.copyOf(r, i);
                }

                return r;
            }
        };
    }

    public boolean ifEvery( BooleanFunction1<T> predicate ) {
        while ( hasNext() ) {
            T v = next();

            if ( !predicate.invoke(v) ) {
                return false;
            }
        }

        return true;
    }

    public <R> R fold( R initialValue, Function2<R,T,R> aggregator ) {
        R soFar = initialValue;

        while ( this.hasNext() ) {
            T v = this.next();

            soFar = aggregator.invoke( soFar, v );
        }

        return soFar;
    }

    public FPOption<T> first( BooleanFunction1<T> predicate ) {
        return filter(predicate).first();
    }

    public FPOption<T> first() {
        return hasNext() ? FPOption.of(next()) : FPOption.none();
    }

    /**
     * Remove the elements that do not match the predicate.
     *
     * @return only the elements whose predicate returns true
     */
    public FPIterator<T> filter( BooleanFunction1<T> predicate ) {
        FPIterator<T> mainIterator = this;

        return new FPIterator<T>() {
            private boolean needsToScroll = true;
            private T nextValue;

            public boolean _hasNext() {

                if ( needsToScroll ) {
                    nextValue = scrollToNext();

                    needsToScroll = false;
                }

                return nextValue != null;
            }

            public T _next() {
                T result = nextValue;

                needsToScroll = true;

                return result;
            }

            private T scrollToNext() {
                while ( mainIterator.hasNext() ) {
                    T candidate = mainIterator.next();

                    if ( predicate.invoke(candidate) ) {
                        return candidate;
                    }
                }

                return null;
            }
        };
    }

    @SneakyThrows
    public void forEachWithThrows( VoidFunction1WithThrows<T> f ) {
        while (this.hasNext()) {
            T v = next();

            f.invoke( v );
        }
    }

    public String toString() {
        return "FPIterator(hasNext="+this.hasNext()+")";
    }
}
