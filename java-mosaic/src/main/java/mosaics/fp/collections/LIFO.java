package mosaics.fp.collections;

import mosaics.lang.functions.BooleanFunction1;
import mosaics.lang.QA;

import java.util.LinkedList;


/**
 * A LIFO stack.
 */
public class LIFO<T> implements FPIterable<T> {
    private LinkedList<T> queue = new LinkedList<>();

    public LIFO() {}

    public LIFO( Iterable<T> initialValues ) {
        initialValues.forEach( this::push );
    }

    @SafeVarargs
    public LIFO( T...initialValues ) {
        pushAll( initialValues );
    }



    public void push( T v ) {
        QA.argNotNull(v, "v");

        queue.addLast( v );
    }

    public T pop() {
        return queue.removeLast();
    }

    public T peek() {
        return queue.getLast();
    }

    public boolean isEmpty() {
        return queue.isEmpty();
    }

    public boolean hasContents() {
        return !isEmpty();
    }

    public FPIterator<T> iterator() {
        return FPIterator.wrap(queue.descendingIterator());
    }

    public void pushAll( T[] elements ) {
        if ( elements == null ) {
            return;
        }

        for ( T e : elements ) {
            push(e);
        }
    }


    @Override
    public LIFO<T> dropWhile( BooleanFunction1<T> predicate ) {
        while ( hasContents() && predicate.invoke(peek()) ) {
            pop();
        }

        return this;
    }
}
