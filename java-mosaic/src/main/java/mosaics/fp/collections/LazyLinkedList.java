package mosaics.fp.collections;

import mosaics.lang.functions.Function0;


/**
 * A lazy self generating cons list.  The constructor is supplied a 'factory method', which is
 * invoked once per element within the cons list.  The invocation of the factory method is delayed
 * until the current element in the cons list is accessed.  Once invoked, the cons list element will
 * fix the value by storing it within the cons element.
 */
public class LazyLinkedList<T> implements ConsList<T> {

    private final Function0<FPOption<T>> nextValueFactory;

    private FPOption<T> head;
    private ConsList<T> tail;



    public LazyLinkedList( Function0<FPOption<T>> nextValueFactory ) {
        this.nextValueFactory = nextValueFactory;
    }


    public boolean isEOL() {
        init();

        return !head.hasValue();
    }

    public T getHead() {
        init();

        return head.get();
    }

    public ConsList<T> getTail() {
        init();

        return tail;
    }

    public ConsList<T> append( T v ) {
        throw new UnsupportedOperationException();
    }

    public ConsList<T> remove( T valueToRemove ) {
        throw new UnsupportedOperationException();
    }

    @SuppressWarnings( "unchecked" )
    private void init() {
        if ( head == null ) {
            this.head = nextValueFactory.invoke();
            this.tail = head.hasValue() ? new LazyLinkedList<>(nextValueFactory) : NIL;
        }
    }

}
