package mosaics.fp.collections;


import java.util.Objects;


public class Tuple4<V1,V2,V3,V4> {

    private final V1 v1;
    private final V2 v2;
    private final V3 v3;
    private final V4 v4;

    public Tuple4( V1 v1, V2 v2, V3 v3, V4 v4 ) {
        this.v1 = v1;
        this.v2 = v2;
        this.v3 = v3;
        this.v4 = v4;
    }

    public V1 getFirst() {
        return v1;
    }

    public V2 getSecond() {
        return v2;
    }

    public V3 getThird() {
        return v3;
    }

    public V4 getForth() {
        return v4;
    }

    public String toString() {
        return "("+v1+","+v2+","+v3+","+v4+")";
    }


    public int hashCode() {
        return Objects.hash(v1, v2, v3, v4);
    }

    public boolean equals( Object o ) {
        if ( !(o instanceof Tuple4) ) {
            return false;
        } else if ( o == this ) {
            return true;
        }

        Tuple4 other = (Tuple4) o;
        return Objects.equals(this.v1, other.v1) && Objects.equals(this.v2, other.v2)
            && Objects.equals(this.v3, other.v3) && Objects.equals(this.v4, other.v4);
    }

}
