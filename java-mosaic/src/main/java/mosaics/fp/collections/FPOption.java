package mosaics.fp.collections;

import mosaics.lang.Backdoor;
import mosaics.lang.functions.Function0;
import mosaics.lang.functions.Function0WithThrows;
import mosaics.lang.functions.Function1WithThrows;
import mosaics.lang.functions.VoidFunction0WithThrows;
import mosaics.lang.functions.VoidFunction1WithThrows;

import java.util.List;
import java.util.Optional;


/**
 *
 */
public abstract class FPOption<T> implements FPIterable<T> {

    public static final FPOption<Boolean> FALSE = FPOption.of( false );
    public static final FPOption<Boolean> TRUE  = FPOption.of( true );
    public static final FPOption<Integer> ONE   = FPOption.of( 1 );


    public static <T> FPOption<T> none() {
        return Backdoor.cast( None.INSTANCE );
    }

    public static <T> FPOption<T> of( T v ) {
        return v == null ? none() : new Some<>(v);
    }

    public static <T> FPOption<T> createFPOption( T v ) {
        return of(v);
    }

    public static <T> FPOption<T> ofOptional( Optional<T> v ) {
        return v.<FPOption<T>>map( Some::new ).orElseGet( FPOption::none );
    }

    public static <T> FPOption<T> ofFPOption( FPOption<T> v ) {
        return v == null ? none() : v;
    }

    public static FPOption<String> ofBlankableString( String s ) {
        return s == null || s.trim().length() == 0 ? none() : new Some<>(s);
    }

    public <C> C cast() {
        return Backdoor.cast( this );
    }

    public abstract T get();
    public abstract boolean hasValue();

    /**
     *
     * @return true if this Option contains no value of any kind
     */
    public abstract boolean isEmpty();

    /**
     *
     * @return returns true if this Option either contains no value or the value is a whitespace string
     */
    public abstract boolean isBlank();

    public abstract <B> FPOption<B> map( Function1WithThrows<T,? extends B> mappingFunction );
    public abstract <B> FPOption<B> flatMap( Function1WithThrows<T,FPIterable<B>> mappingFunction );

    public <B> FPOption<B> flatMapJdkOptional( Function1WithThrows<T,Optional<B>> mappingFunction ) {
        return flatMap( v -> FPOption.ofOptional(mappingFunction.invoke(v)) );
    }

    public abstract T orElse( T value );
    public abstract T orElse( Function0WithThrows<T> fetcher );
    public abstract FPOption<T> or( FPOption<T> alt );
    public abstract FPOption<T> or( Function0<FPOption<T>> fetcher );
    public abstract <X extends Throwable> FPOption<T> orThrow( Function0<X> exceptionFactory );

    public abstract FPIterable<T> toIterable();
    public abstract List<T> toList();

    public abstract <X extends Throwable> T orElseThrow( Function0<X> exceptionFactory );

    public abstract void ifPresent( VoidFunction1WithThrows<T> op );
    public abstract void ifEmpty( VoidFunction0WithThrows op );

    public abstract <B> FPOption<B> ifElse( Function1WithThrows<T,B> ifPresentFunc, Function0WithThrows<B> ifEmptyFunc );

    public abstract T orNull();

    public abstract FPIterable<T> and( FPOption<T> other );
}
