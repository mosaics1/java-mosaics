package mosaics.fp.collections.maps;

import lombok.AllArgsConstructor;
import lombok.Value;

import java.util.Map;
import java.util.Objects;


@Value
@AllArgsConstructor
class FPMapEntry<K, V> implements Map.Entry<K, V> {
    private final int keyHashCode;
    private final K   key;
    private final V   value;

    public V setValue( V value ) {
        throw new UnsupportedOperationException();
    }

    public FPMapEntry( K key, V value ) {
        this.keyHashCode = key.hashCode();
        this.key         = key;
        this.value       = value;
    }

    public boolean matchesTargetKey( K targetKey, int targetKeyHashCode ) {
        return getKeyHashCode() == targetKeyHashCode && Objects.equals(targetKey, getKey() );
    }
}
