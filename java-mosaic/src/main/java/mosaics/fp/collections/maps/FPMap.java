package mosaics.fp.collections.maps;

import mosaics.fp.collections.FPIterable;
import mosaics.fp.collections.FPOption;
import mosaics.fp.collections.Tuple2;
import mosaics.lang.functions.Function1;

import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.BiConsumer;


public interface FPMap<K,V> {

    public static <K,V> FPMap<K,V> emptyMap() {
        return new FPMapRRBVector<>();
    }

    public static <K,V> FPMap<K,V> wrapMap( Map<K,V> orig ) {
        FPMap<K,V> map = emptyMap();

        for ( Map.Entry<K,V> e : orig.entrySet() ) {
            map = map.put( e.getKey(), e.getValue() );
        }

        return map;
    }

    @SuppressWarnings("unchecked")
    public static <K,V> FPMap<K,V> wrapMap( Object...kvPairs ) {
        FPMap map = emptyMap();

        for ( int i=0; i<kvPairs.length; i+=2 ) {
            map = map.put( kvPairs[i], kvPairs[i+1] );
        }

        return map;
    }


    public FPOption<V> get( K key );
    public FPMap<K,V> remove( K key );
    public FPMap<K,V> put( K key, V v );
    public FPIterable<Map.Entry<K,V>> getEntries();
    public int size();


    public default FPIterable<Tuple2<K,V>> getEntryTuples() {
        return getEntries().map(entry -> new Tuple2<>(entry.getKey(),entry.getValue()) );
    }

    public default FPIterable<K> getKeys() {
        return getEntries().map(Map.Entry::getKey);
    }

    public default FPIterable<V> getValues() {
        return getEntries().map(Map.Entry::getValue);
    }

    public default boolean containsKey( K key ) {
        return get(key).hasValue();
    }

    public default void forEach( BiConsumer<K, V> action ) {
        getEntries().forEach( entry -> action.accept(entry.getKey(),entry.getValue()) );
    }

    public default V getMandatory( K key ) {
        return get(key).get();
    }

    public default boolean isEmpty() {
        return size() == 0;
    }

    public default boolean hasContents() {
        return !isEmpty();
    }

    public default V get( K key, V defaultValue ) {
        return get(key).orElse( defaultValue );
    }

    public default boolean contains( K key ) {
        return get(key).hasValue();
    }

    public default FPMap<K,V> updateValue( K key, Function1<FPOption<V>,FPOption<V>> updateFunc ) {
        FPOption<V> currentValue = get( key );
        FPOption<V> newValue     = updateFunc.invoke( currentValue );

        if ( !newValue.equals(currentValue) ) {
            if ( newValue.isEmpty() ) {
                return remove( key );
            } else {
                return put( key, newValue.get() );
            }
        }

        return this;
    }

    public default <V2> FPMap<K,V2> mapValues( Function1<V,V2> valueMapper ) {
        AtomicReference<FPMap<K,V2>> newMap = new AtomicReference<>( FPMap.emptyMap() );

        this.forEach( (k,v) -> newMap.set(newMap.get().put(k, valueMapper.invoke(v))) );

        return newMap.get();
    }

    public default FPMap<K,V> appendAll( FPMap<K,V> other ) {
        FPMap<K,V> soFar = this;

        for ( Tuple2<K,V> kv : other.getEntryTuples() ) {
            soFar = soFar.put( kv );
        }

        return soFar;
    }

    public default FPMap<K,V> put( Tuple2<K,V> kv ) {
        return this.put( kv.getFirst(), kv.getSecond() );
    }
}
