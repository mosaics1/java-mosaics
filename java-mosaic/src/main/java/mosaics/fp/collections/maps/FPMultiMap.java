package mosaics.fp.collections.maps;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Value;
import mosaics.fp.collections.FPOption;
import mosaics.fp.collections.RRBVector;
import mosaics.lang.Backdoor;


@Value
@AllArgsConstructor
public class FPMultiMap<K,V> {

    private static final FPMultiMap EMPTY_MAP = new FPMultiMap<>(0, FPMap.emptyMap());

    public static <K,V> FPMultiMap<K,V> emptyMap() {
        return Backdoor.cast(EMPTY_MAP);
    }


    @Getter private final long                  elementCount;
            private final FPMap<K, RRBVector<V>> map;


    public int getKeyCount() {
        return map.size();
    }

    public boolean isEmpty() {
        return elementCount == 0;
    }

    public boolean hasContents() {
        return !isEmpty();
    }

    public RRBVector<V> get( K key ) {
        return map.get( key, RRBVector.emptyVector() );
    }

    public FPOption<V> getHead( K key ) {
        return get(key).getHead();
    }

    public V getHead( K key, V defaultValue ) {
        return getHead(key).orElse( defaultValue );
    }

    public FPMultiMap<K,V> put( K key, RRBVector<V> newVector ) {
        RRBVector<V> existing = get(key);

        long                  newElementCount = elementCount - existing.size() + newVector.size();
        FPMap<K,RRBVector<V>> newMap          = newVector.isEmpty() ? map.remove(key) : map.put(key,newVector);

        return new FPMultiMap<>( newElementCount, newMap );
    }

    public FPMultiMap<K,V> appendElement( K key, V v ) {
        RRBVector<V> existing      = get(key);
        RRBVector<V> updatedVector = existing.add(v);

        return new FPMultiMap<>( elementCount+1, map.put(key,updatedVector) );
    }

    public FPMultiMap<K,V> removeKey( K key ) {
        RRBVector<V> existingVector = get(key);

        if ( existingVector.isEmpty() ) {
            return this;
        }

        return new FPMultiMap<>( elementCount-existingVector.size(), map.remove(key) );
    }

    public FPMultiMap<K,V> removeElement( K key, V element ) {
        RRBVector<V> originalVector = get(key);
        RRBVector<V> updatedVector  = originalVector.remove(element);

        if ( updatedVector == originalVector ) {
            return this;
        } else if ( updatedVector.isEmpty() ) {
            return this.removeKey( key );
        }

        return new FPMultiMap<>( elementCount-1, map.put(key,updatedVector) );
    }

}
