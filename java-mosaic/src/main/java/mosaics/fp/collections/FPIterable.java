package mosaics.fp.collections;

import mosaics.fp.FP;
import mosaics.fp.collections.maps.FPMap;
import mosaics.fp.collections.maps.FPMutableMap;
import mosaics.lang.Backdoor;
import mosaics.lang.QA;
import mosaics.lang.TryUtils;
import mosaics.lang.functions.BooleanFunction1;
import mosaics.lang.functions.BooleanFunction2;
import mosaics.lang.functions.DoubleFunction1;
import mosaics.lang.functions.Function0;
import mosaics.lang.functions.Function1;
import mosaics.lang.functions.Function1WithThrows;
import mosaics.lang.functions.Function2;
import mosaics.lang.functions.FunctionIO;
import mosaics.lang.functions.IntFunction1;
import mosaics.lang.functions.LongFunction1;
import mosaics.lang.functions.VoidFunction1;
import mosaics.lang.functions.VoidFunction2WithThrows;
import mosaics.lang.functions.VoidFunctionIO;
import mosaics.utils.CollectionUtils;
import mosaics.utils.ComparableUtils;
import mosaics.utils.ListUtils;
import mosaics.utils.PeekableIterator;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Stream;

import static mosaics.lang.Backdoor.cast;


@SuppressWarnings( "unchecked" )
public interface FPIterable<T> extends java.lang.Iterable<T> {

    public static <T extends Enum> FPIterable<T> wrapEnum( Class<T> enumClass) {
        return wrapArray( enumClass.getEnumConstants() );
    }

    public static <T> FPIterable<T> wrapSingleton( T v ) {
        return wrapAll(v);
    }

    public static <T> FPIterable<T> wrapArray( T[] array ) {
        if ( array == null ) {
            return FPIterable.empty();
        }

        return wrapArray( array, 0, array.length );
    }

    public static FPIterable<Integer> wrapArray( int[] array ) {
        return wrapArray( array, 0, array.length );
    }

    public static <T> FPIterable<T> wrapStream( Stream<T> stream ) {
        return new BaseIterable<>() {
            public FPIterator<T> iterator() {
                return FPIterator.wrap(stream.iterator());
            }
        };
    }

    public static FPIterable<Integer> wrapArray( int[] array, int fromInc, int toExc ) {
        return new BaseIterable<Integer>() {
            public FPIterator<Integer> iterator() {
                int maxExc = array == null ? 0 : Math.min(array.length, toExc);

                return new FPIterator<Integer>() {
                    int i = fromInc;

                    public boolean _hasNext() {
                        return i < maxExc;
                    }

                    @SuppressWarnings("ConstantConditions")
                    public Integer _next() {
                        return array[i++];
                    }
                };
            }
        };
    }

    public static FPIterable<Long> wrapArray( long[] array ) {
        return wrapArray( array, 0, array.length );
    }

    public static FPIterable<Long> wrapArray( long[] array, int fromInc, int toExc ) {
        return new BaseIterable<Long>() {
            public FPIterator<Long> iterator() {
                int maxExc = array == null ? 0 : Math.min(array.length, toExc);

                return new FPIterator<Long>() {
                    int i = fromInc;

                    public boolean _hasNext() {
                        return i < maxExc;
                    }

                    @SuppressWarnings("ConstantConditions")
                    public Long _next() {
                        return array[i++];
                    }
                };
            }
        };
    }

    public static <T> FPIterable<T> wrapArray( T[] array, int fromInc, int toExc ) {
        return new BaseIterable<T>() {
            public FPIterator<T> iterator() {
                int maxExc = array == null ? 0 : Math.min(array.length, toExc);

                return new FPIterator<T>() {
                    int i = fromInc;

                    public boolean _hasNext() {
                        return i < maxExc;
                    }

                    @SuppressWarnings("ConstantConditions")
                    public T _next() {
                        return array[i++];
                    }
                };
            }
        };
    }

    public static <T> FPIterable<T> repeat( int numTimes, T v ) {
        return wrap( Collections.nCopies(numTimes, v) );
    }

    public static <T> FPIterable<T> generate( int numTimes, Function0<T> generator ) {
        List<T> list = new ArrayList<>( numTimes );

        for ( int i=0; i<numTimes; i++ ) {
            list.add( generator.invoke() );
        }

        return wrap( list );
    }

    /**
     * Creates a new Stream that generates the next value one at a time on an as needed basis from
     * a generator function.
     *
     * @param fetchNextFunction  a stateful function that will generate the next value in the series.. returning None will end the Stream.
     */
    public static <T> FPIterable<T> generate( Function0<FPOption<T>> fetchNextFunction ) {
        return generate( fetchNextFunction.invoke().orNull(), p -> fetchNextFunction.invoke(), v -> v );
    }

    /**
     * Creates a new FPIterable that generates the next value one at a time on an as needed basis from
     * a generator function.
     *
     * @param firstValueNbl      null will create an empty stream
     * @param fetchNextFunction  given the previous value, generate the next value in the series.. returning None will end the Stream.
     */
    public static <T> FPIterable<T> generate( T firstValueNbl, Function<T,FPOption<T>> fetchNextFunction ) {
        return generate( firstValueNbl, fetchNextFunction::apply, v -> v );
    }
    

    /**
     * Creates a new FPIterable that generates the next value one at a time on an as needed basis from
     * a generator function.  This version of the generate methods uses a stream of S to generate the
     * desired Stream&lt;R&gt;.  S is some form of internal state that describes both the value R and
     * maintains extra state used to generate the next value in the series.
     *
     * For example, to generate the fibonacci sequence the next value is created by summing the previous
     * two values.  To model this with this function, S could be a Tuple2<Long>.  fetchNextFunction would
     * return the next pair of values needed to create the next value and toValueF would select the current
     * value from the Tuple.
     *
     * eg to generate the fibonacci sequence:
     *
     * <code>
     * generate(new Tuple2(1,1), t -&gt; new Tuple2(t.getSecond(), t.getFirst()+t.getSecond(), t -&gt; t.getSecond())
     * </code>
     *
     * @param initialState       null will create an empty stream
     * @param fetchNextFunction  given the previous value, generate the next value in the series.. returning None will end the Stream.
     * @param toValueF           converts the internal stream representation into the target value
     */
    public static <S,R> FPIterable<R> generate( S initialState, Function<S,FPOption<S>> fetchNextFunction, Function1<S,R> toValueF ) {
        Objects.requireNonNull(fetchNextFunction);

        return new BaseIterable<>() {
            public FPIterator<R> iterator() {
                return FPIterator.wrap(
                    new Iterator<S>() {
                        private FPOption<S> t = FPOption.of(initialState);

                        public boolean hasNext() {
                            return t.hasValue();
                        }

                        public S next() {
                            S v = t.get();

                            t = fetchNextFunction.apply(v);

                            return v;
                        }
                    }
                ).map( toValueF );
            }
        };
    }

    public static <T> FPIterable<T> wrapOption( FPOption<T> opt ) {
        if ( opt.hasValue() ) {
            return wrap( Arrays.asList(opt.get()) );
        } else {
            return wrap( Arrays.asList() );
        }
    }

    public static <T> FPIterable<T> empty() {
        return wrapAll();
    }

    @SafeVarargs
    public static <T> FPIterable<T> wrapAll( T...items ) {
        return wrap( Arrays.asList(items) );
    }

    public static <T> FPIterable<T> wrap( java.lang.Iterable<T> wrappedIterable ) {
        QA.argNotNull(wrappedIterable, "wrappedIterable");

        if ( wrappedIterable instanceof FPIterable ) {
            return (FPIterable<T>) wrappedIterable;
        }

        return new BaseIterable<T>() {
            public FPIterator<T> iterator() {
                return FPIterator.wrap(wrappedIterable.iterator());
            }
        };
    }

    public default boolean isEmpty() {
        return !iterator().hasNext();
    }

    public default boolean hasContents() {
        return iterator().hasNext();
    }

    public default FPIterable<T> and( FPOption<T> other ) {
        return other.hasValue() ? and(other.get()) : this;
    }

    public default FPIterable<T> and( T...others ) {
        return and(Arrays.asList(others));
    }

    public default FPIterable<T> and( Iterable<? extends T> others ) {
        if ( others == null ) {
            return this;
        }

        Iterable<T>           a = this;
        Iterable<? extends T> b = others;

        return new BaseIterable<T>() {
            public FPIterator<T> iterator() {
                Iterator<T> it1 = a.iterator();
                Iterator<? extends T> it2 = b.iterator();

                return new FPIterator<T>() {
                    public boolean _hasNext() {
                        return it1.hasNext() || it2.hasNext();
                    }

                    public T _next() {
                        Iterator<? extends T> it = it1.hasNext() ? it1 : it2;

                        return it.next();
                    }
                };
            }
        };
    }

    public FPIterator<T> iterator();

    public default FPOption<T> minByCost( LongFunction1<T> costFunction ) {
        return iterator().min( costFunction );
    }

    /**
     * Combines two iterators into a single iterator of tuples where each element from
     * each iterator is aligned with the other iterator.  For example:
     *
     * LHS:  a,b,c,d
     * RHS:  1,2,3,4,5
     *
     * becomes: (a,1), (b,2), (c,3), (d,4), (null,5)
     */
    public default <B> FPIterable<Tuple2<T,B>> zip( Iterable<B> rhs ) {
        return new BaseIterable<Tuple2<T,B>>() {
            public FPIterator<Tuple2<T, B>> iterator() {
                return new FPIterator<Tuple2<T, B>>() {
                    private Iterator<T> lhsIt = FPIterable.this.iterator();
                    private Iterator<B> rhsIt = rhs.iterator();

                    public boolean _hasNext() {
                        return lhsIt.hasNext() || rhsIt.hasNext();
                    }

                    public Tuple2<T, B> _next() {
                        T a = lhsIt.hasNext() ? lhsIt.next() : null;
                        B b = rhsIt.hasNext() ? rhsIt.next() : null;

                        return new Tuple2<>(a,b);
                    }
                };
            }
        };
    }

    /**
     * Combines two iterators mapping each of the elements together as it goes creating a new
     * iterable of values.  For example:
     *
     * LHS:  a,b,c,d
     * RHS:  1,2,3,4,5
     * OP:   +
     *
     * becomes: a1, b2, c3, d4, null5
     */
    public default <B,R> FPIterable<R> zip( Iterable<B> rhs, Function2<T,B,R> aggregator ) {
        return new BaseIterable<R>() {
            public FPIterator<R> iterator() {
                return new FPIterator<R>() {
                    private Iterator<T> lhsIt = FPIterable.this.iterator();
                    private Iterator<B> rhsIt = rhs.iterator();

                    public boolean _hasNext() {
                        return lhsIt.hasNext() || rhsIt.hasNext();
                    }

                    public R _next() {
                        T a = lhsIt.hasNext() ? lhsIt.next() : null;
                        B b = rhsIt.hasNext() ? rhsIt.next() : null;

                        return aggregator.invoke( a, b );
                    }
                };
            }
        };
    }

    public default FPIterable<T> tail() {
        return new BaseIterable<T>() {
            public FPIterator<T> iterator() {
                return new FPIterator<T>() {
                    private Iterator<T> it = FPIterable.this.iterator();

                    {
                        if ( it.hasNext() ) {
                            it.next();
                        }
                    }

                    public boolean _hasNext() {
                        return it.hasNext();
                    }

                    public T _next() {
                        return it.next();
                    }
                };
            }
        };
    }

    /**
     * Groups the values in this iterable into pairs.  If the iterable contains an odd number of
     * values then the last value will be dropped.
     */
    public default FPIterable<Tuple2<T,T>> pairs() {
        return new BaseIterable<Tuple2<T,T>>() {
            public FPIterator<Tuple2<T,T>> iterator() {
                return new FPIterator<Tuple2<T,T>>() {
                    private FPOption<Tuple2<T,T>> nextTuple;
                    private Iterator<T> underlyingIterator = FPIterable.this.iterator();

                    {
                        nextTuple = readNextPair();
                    }

                    public boolean _hasNext() {
                        return nextTuple.hasValue();
                    }

                    public Tuple2<T,T> _next() {
                        Tuple2<T, T> ttTuple2 = this.nextTuple.get();

                        this.nextTuple = readNextPair();

                        return ttTuple2;
                    }

                    private FPOption<Tuple2<T,T>> readNextPair() {
                        if ( !underlyingIterator.hasNext() ) {
                            return FP.emptyOption();
                        }

                        T key = underlyingIterator.next();
                        if ( !underlyingIterator.hasNext() ) {
                            return FP.emptyOption();
                        }

                        T value = underlyingIterator.next();

                        return FP.option( new Tuple2<>(key,value) );
                    }
                };
            }
        };
    }


    /**
     * Groups the values in this iterable into pairs.  If the iterable contains an odd number of
     * values then the last value will be dropped.
     */
    public default FPIterable<T> compact(
        BooleanFunction2<T,T> mergePredicate,
        Function2<T,T,T> mergeOp
    ) {
        return new BaseIterable<>() {
            public FPIterator<T> iterator() {
                return new FPIterator<>() {
                    private PeekableIterator<T> underlyingIterator = new PeekableIterator<>(FPIterable.this.iterator());
                    private FPOption<T>         nextValue          = consumeNext( underlyingIterator );

                    public boolean _hasNext() {
                        return nextValue.hasValue();
                    }

                    public T _next() {
                        T next = nextValue.get();

                        nextValue = consumeNext( underlyingIterator );

                        return next;
                    }

                    private FPOption<T> consumeNext( PeekableIterator<T> it ) {
                        if ( !it.hasNext() ) {
                            return FP.emptyOption();
                        }

                        T agg = it.next();
                        while ( it.hasNext() ) {
                            T next = it.peek();

                            if ( mergePredicate.invoke(agg,next) ) {
                                agg = mergeOp.invoke( agg, it.next() );
                            } else {
                                return FP.option( agg );
                            }
                        }

                        return FP.option( agg );
                    }
                };
            }
        };
    }

    /**
     * Restrict the length of this iterable to n elements.
     */
    public default FPIterable<T> limit( int maxNumElements ) {
        QA.argIsGTEZero(maxNumElements, "maxNumElements");

        FPIterable<T> sourceIterable = this;

        return new BaseIterable<>() {
            public FPIterator<T> iterator() {
                return FPIterator.wrap(
                    new Iterator<T>() {
                        private final Iterator<T> wrappedIterator = sourceIterable.iterator();
                        private int countDown = maxNumElements;

                        public boolean hasNext() {
                            return countDown > 0 && wrappedIterator.hasNext();
                        }

                        public T next() {
                            QA.isTrueState( countDown > 0, "countDown is "+countDown );

                            countDown--;

                            return wrappedIterator.next();
                        }
                    }
                );
            }
        };
    }

    public default Object[] toArray() {
        // nb we are not preallocating the size of the array because the iterable might be lazy;
        // which means invoking count could resolve all of the elements twice (once for the count,
        // and again to copy the elements out)

        List arrayList = new ArrayList();

        forEach( arrayList::add );

        return arrayList.toArray();
    }

    public default <B> B[] toArray( Class<B> type ) {
        int size  = Backdoor.toInt(this.count());
        B[] array = cast(Array.newInstance(type, size));

        int i=0;
        for ( T v : this ) {
            array[i++] = cast(v);
        }

        return array;
    }

    public default List<T> toList() {
        List<T> list = new ArrayList<>();

        this.forEach( list::add );

        return list;
    }

    public default RRBVector<T> toRRBVector() {
        if ( this instanceof RRBVector ) {
            return (RRBVector<T>) this;
        }

        return this.fold( RRBVector.emptyVector(), RRBVector::add );
    }

    public default FPList<T> toFPList() {
        if ( this instanceof FPList ) {
            return (FPList<T>) this;
        }

        List<T> list = new ArrayList<>();

        this.forEach( list::add );

        return FPList.wrapList( list );
    }

    public default FPSet<T> toSetFP() {
        if ( this instanceof FPSet ) {
            return (FPSet<T>) this;
        }

        Set<T> set = new HashSet<>();

        this.forEach( set::add );

        return FPSet.wrapSet( set );
    }

    public default Set<T> toSet() {
        Set<T> set = new HashSet<>();

        this.forEach( set::add );

        return set;
    }

    /**
     * Sorts all elements within the iterable using a default comparator.  If the elements of
     * the iterable implements Comparable, then Comparable.compareTo will be used for the comparisons.
     * On the otherhand if Comparable is not implemented then the elements will be sorted based
     * on the result of their toString methods.
     */
    @SuppressWarnings("unchecked")
    public default FPIterable<T> sort() {
        return sort(
            new Comparator<T>() {
                public int compare( T o1, T o2 ) {
                    if ( o1 instanceof Comparable ) {
                        return ((Comparable) o1).compareTo(o2);
                    } else {
                        return Objects.toString(o1).compareTo(Objects.toString(o2));
                    }
                }
            }
        );
    }

    @SuppressWarnings("unchecked")
    public default <B extends Comparable<B>> FPIterable<T> sortBy( Function1<T,B> f ) {
        return sort(
            (o1,o2) -> {
                B a = f.invoke( o1 );
                B b = f.invoke( o2 );

                return a.compareTo( b );
            }
        );
    }

    @SuppressWarnings("unchecked")
    public default <B extends Comparable<B>> FPIterable<T> sortByDesc( Function1<T,B> f ) {
        return sort(
            (o1,o2) -> {
                B a = f.invoke( o1 );
                B b = f.invoke( o2 );

                return b.compareTo( a );
            }
        );
    }

    public default FPIterable<T> sort( Comparator<T> comparator ) {
        List<T> allElements = toList();

        allElements.sort( comparator );

        return FPIterable.wrap(allElements);
    }

    public default <B> FPIterable<B> filter( Class<B> targetType ) {
        return cast( filter(x -> targetType.isAssignableFrom(x.getClass())) );
    }

    /**
     * Remove the elements that do not match the predicate.
     *
     * @return only the elements whose predicate returns true
     */
    public default FPIterable<T> filter( BooleanFunction1<T> predicate ) {
        FPIterable<T> main = this;

        return new BaseIterable<T>() {
            public FPIterator<T> iterator() {
                return new FPIterator<T>() {
                    private Iterator<T> mainIterator = main.iterator();

                    private boolean needsToScroll = true;
                    private T nextValue;

                    public boolean _hasNext() {

                        if ( needsToScroll ) {
                            nextValue = scrollToNext();

                            needsToScroll = false;
                        }

                        return nextValue != null;
                    }

                    public T _next() {
                        T result = nextValue;

                        needsToScroll = true;

                        return result;
                    }

                    private T scrollToNext() {
                        while ( mainIterator.hasNext() ) {
                            T candidate = mainIterator.next();

                            if ( predicate.invoke(candidate) ) {
                                return candidate;
                            }
                        }

                        return null;
                    }
                };
            }
        };
    }

    public default FPIterator<T> iteratorX() {
        return FPIterator.wrap(this);
    }

    public default FPIterable<T> drop( int count ) {
        return dropWhile( new BooleanFunction1<>() {
            private int skipCounter = count;

            public boolean invoke( T arg ) {
                return skipCounter-- > 0;
            }
        } );
    }

    /**
     * Remove the first elements that match the given predicate.  Removal stops at the point
     * where predicate first returns false.
     *
     * @return the tail of the iterable with the head elements that match the predicate removed
     */
    public default FPIterable<T> dropWhile( BooleanFunction1<T> predicate ) {
        FPIterable<T> main = this;

        return new BaseIterable<T>() {
            public FPIterator<T> iterator() {
                return new FPIterator<T>() {
                    private FPIterator<T> mainIterator = main.iteratorX();

                    {
                        skipMatchingElements();
                    }

                    public boolean _hasNext() {
                        return mainIterator.hasNext();
                    }

                    public T _next() {
                        return mainIterator.next();
                    }

                    private void skipMatchingElements() {
                        while ( mainIterator.hasNext() ) {
                            T candidate = mainIterator.peek();

                            if ( !predicate.invoke(candidate) ) {
                                return;
                            }

                            mainIterator.skip();
                        }
                    }
                };
            }
        };
    }

    /**
     * Remove the elements that do not match the predicate.
     *
     * @return only the elements whose predicate returns true
     */
    public default FPIterable<T> filterNot( BooleanFunction1<T> predicate ) {
        return filter(predicate.invert());
    }

    public default FPIterable<T> filterNotNull() {
        return filter(x -> x != null);
    }


    public default <R> R fold( R initialValue, Function2<R,T,R> op ) {
        R aggregatedValue = initialValue;

        for ( T v : this ) {
            aggregatedValue = op.invoke(aggregatedValue, v);
        }

        return aggregatedValue;
    }

    /**
     * Keep folding while the aggregating value is not empty.  As soon as the initial value or
     * the op returns FPOption.none(), then the folding will stop with the value of none().
     */
    public default <R> FPOption<R> foldWhile( FPOption<R> initialValue, Function2<R,T,FPOption<R>> op ) {
        FPOption<R> aggregatedValue = initialValue;

        if ( aggregatedValue.isEmpty() ) {
            return aggregatedValue;
        }

        for ( T v : this ) {
            aggregatedValue = op.invoke( aggregatedValue.get(), v );

            if ( aggregatedValue.isEmpty() ) {
                return aggregatedValue;
            }
        }

        return aggregatedValue;
    }

    /**
     * Converts every element in this iterable to another element by passing every element through
     * the supplied mappingFunction.
     */
    public default <B> FPIterable<B> map( Function1WithThrows<T,? extends B> mappingFunction ) {
        return new BaseIterable<B>() {
            public FPIterator<B> iterator() {
                return new FPIterator<B>() {
                    private Iterator<T> it = FPIterable.this.iterator();

                    public boolean _hasNext() {
                        return it.hasNext();
                    }

                    public B _next() {
                        T v = it.next();
                        B m = TryUtils.invokeAndRethrowException( () -> mappingFunction.invoke(v) );

                        return m;
                    }
                };
            }
        };
    }

    /**
     * Maps only one element.
     *
     * @param elementToChange a predicate to identify which element to change
     * @param mappingFunction the function that will apply the change
     */
    public default FPIterable<T> mapFirst( Predicate<T> elementToChange, Function1WithThrows<T,T> mappingFunction ) {
        return new BaseIterable<T>() {
            public FPIterator<T> iterator() {
                return new FPIterator<T>() {
                    private Iterator<T> it         = FPIterable.this.iterator();
                    private boolean     hasMatched = false;

                    public boolean _hasNext() {
                        return it.hasNext();
                    }

                    public T _next() {
                        T v = it.next();

                        if ( hasMatched || !elementToChange.test(v) ) {
                            return v;
                        } else {
                            hasMatched = true;

                            return TryUtils.invokeAndRethrowException( () -> mappingFunction.invoke( v ) );
                        }
                    }
                };
            }
        };
    }



    /**
     * Converts every element in this iterable to another element by passing every element through
     * the supplied mappingFunction.
     */
    public default <B> FPIterable<B> mapWithIndex( FunctionIO<T,B> mappingFunction ) {
        return new BaseIterable<B>() {
            public FPIterator<B> iterator() {
                return new FPIterator<B>() {
                    private Iterator<T> it = FPIterable.this.iterator();
                    private int index = 0;

                    public boolean _hasNext() {
                        return it.hasNext();
                    }

                    public B _next() {
                        return mappingFunction.invoke(index++, it.next());
                    }
                };
            }
        };
    }

    public default void forEachWithIndex( VoidFunctionIO<T> handler ) {
        int i = 0;

        for ( T v : this ) {
            handler.invoke( i, v );

            i++;
        }
    }

    /**
     * Converts every element in this iterable to another element by passing every element through
     * the supplied mappingFunction.  Note that the mapping function itself returns an Iterable.
     * The contents of each of these iterables are concatentated together into a single iterable.
     */
    public default <B> FPIterable<B> flatMap( Function1WithThrows<T,FPIterable<B>> mappingFunction ) {
        FPIterable<T> main = this;

        return new BaseIterable<>() {
            public FPIterator<B> iterator() {
                return new FPIterator<>() {
                    private Iterator<FPIterable<B>> mainIterator = main.map(mappingFunction).filter( e -> e != null && e.hasContents()).iterator();
                    private Iterator<B>             subIterator  = Collections.emptyIterator();

                    public boolean _hasNext() {
                        return subIterator.hasNext() || mainIterator.hasNext();
                    }

                    public B _next() {
                        if ( !subIterator.hasNext() && mainIterator.hasNext() ) {
                            FPIterable<B> next = mainIterator.next();

                            subIterator = next.iterator();
                        }

                        return subIterator.next();
                    }
                };
            }
        };
    }

    public default <B> FPIterable<B> flatMapOpt( Function1WithThrows<T,FPOption<? extends B>> mappingFunction ) {
        FPIterable<T> main = this;

        return new BaseIterable<B>() {
            public FPIterator<B> iterator() {
                return new FPIterator<B>() {
                    private Iterator<FPOption<? extends B>> mainIterator = main.map(mappingFunction).filter( e -> e != null && e.hasValue()).iterator();

                    public boolean _hasNext() {
                        return  mainIterator.hasNext();
                    }

                    public B _next() {
                        if ( mainIterator.hasNext() ) {
                            return mainIterator.next().get();
                        } else {
                            return null;
                        }
                    }
                };
            }
        };
    }

    public default <B> FPIterable<B> flatMapJdkList( Function1WithThrows<T,List<B>> mappingFunction ) {
        FPIterable<T> main = this;

        return new BaseIterable<B>() {
            public FPIterator<B> iterator() {
                return new FPIterator<B>() {
                    private Iterator<B> mainIterator = main.flatMap(v -> FPList.wrapList(mappingFunction.invoke(v))).iterator();

                    public boolean _hasNext() {
                        return mainIterator.hasNext();
                    }

                    public B _next() {
                        return mainIterator.next();
                    }
                };
            }
        };
    }

    /**
     * Converts every element in this iterable to another element by passing every element through
     * the supplied mappingFunction.  Note that the mapping function itself returns an Iterable.
     * The contents of each of these iterables are concatentated together into a single iterable.
     */
    public default <B> FPIterable<B> flatMapArray( Function1WithThrows<T,B[]> mappingFunction ) {
        FPIterable<T> main = this;

        return new BaseIterable<B>() {
            public FPIterator<B> iterator() {
                return new FPIterator<B>() {
                    private Iterator<B[]> mainIterator = main.map(mappingFunction).filter(e -> e != null && e.length > 0).iterator();
                    private Iterator<B>   subIterator  = Collections.emptyIterator();

                    public boolean _hasNext() {
                        return subIterator.hasNext() || mainIterator.hasNext();
                    }

                    public B _next() {
                        if ( !subIterator.hasNext() ) {
                            subIterator = FPIterable.wrapArray(mainIterator.next()).iterator();
                        }

                        return subIterator.next();
                    }
                };
            }
        };
    }

    public default IntIterable mapToInt() {
        return mapToInt(x -> (int) x);
    }

    public default IntIterable mapToInt( IntFunction1<T> mappingFunction ) {
        return new BaseIntIterable() {
            public IntIterator iterator() {
                return new IntIterator() {
                    private Iterator<T> it = FPIterable.this.iterator();

                    public boolean hasNext() {
                        return it.hasNext();
                    }

                    public int next() {
                        return mappingFunction.invoke(it.next());
                    }
                };
            }
        };
    }

    public default LongIterable mapToLong() {
        return mapToLong(x -> (long) x);
    }

    public default LongIterable mapToLong( LongFunction1<T> mappingFunction ) {
        return new BaseLongIterable() {
            public LongIterator iterator() {
                return new LongIterator() {
                    private Iterator<T> it = FPIterable.this.iterator();

                    public boolean hasNext() {
                        return it.hasNext();
                    }

                    public long next() {
                        return mappingFunction.invoke(it.next());
                    }
                };
            }
        };
    }

    public default DoubleIterable mapToDouble() {
        return mapToDouble(x -> (double) x);
    }

    public default DoubleIterable mapToDouble( DoubleFunction1<T> mappingFunction ) {
        return new BaseDoubleIterable() {
            public DoubleIterator iterator() {
                return new DoubleIterator() {
                    private Iterator<T> it = FPIterable.this.iterator();

                    public boolean hasNext() {
                        return it.hasNext();
                    }

                    public double next() {
                        return mappingFunction.invoke(it.next());
                    }
                };
            }
        };
    }

    public default <K> FPMutableMap<K,FPIterable<T>> groupBy( Function1<T, K> selectKeyFunction ) {
        FPMutableMap<K,RRBVector<T>> map = groupBy( selectKeyFunction, FP::emptyVector, RRBVector::add );

        return cast(map);
    }

    public default <K,E> FPMutableMap<K,E> groupBy( Function1<T, K> selectKeyFunction, Supplier<E> emptyCollectionFactory, Function2<E,T,E> appender) {
        FPMutableMap<K,E> map = FPMutableMap.newMap();

        for ( T value : this ) {
            K key = selectKeyFunction.invoke( value );

            E collection        = map.computeIfAbsent( key, k -> emptyCollectionFactory.get() );
            E updatedCollection = appender.invoke( collection, value );

            map.put( key, updatedCollection );
        }

        return cast(map);
    }

    public default <K> FPMap<K,T> groupByUnique( Function1<T, K> selectKeyFunction, Function1<K,Throwable> toDuplicateException ) {
        FPMap<K,T> map = FPMap.emptyMap();

        for ( T value : this ) {
            K key = selectKeyFunction.invoke( value );

            if ( map.containsKey(key) ) {
                throw Backdoor.throwException( toDuplicateException.invoke(key) );
            }

            map = map.put( key, value );
        }


        return map;
    }

    public default FPOption<T> toFPOption() {
        return first();
    }

    public default Optional<T> toJDKOption() {
        FPOption o = toFPOption();

        if ( o.isEmpty() ) {
            return Optional.empty();
        } else {
            return cast( Optional.ofNullable(o.get()) );
        }
    }

    public default <K,V> FPMutableMap<K,V> toMap() {
        FPMutableMap map = FPMutableMap.newMap();

        for ( T value : this ) {
            if ( value instanceof HasKey ) {
                HasKey keyFetcher = (HasKey) value;

                map.put( keyFetcher.getKey().toString(), value );
            } else if ( value instanceof Map.Entry ) {
                Map.Entry e = (Map.Entry) value;
                map.put( e.getKey(), e.getValue() );
            } else if ( value instanceof Tuple2 ) {
                Tuple2 tuple = (Tuple2) value;
                map.put( tuple.getFirst(), tuple.getSecond() );
            } else {
                throw new IllegalArgumentException( "Unknown type: " + value.getClass() );
            }
        }

        return map;
    }

    public default <K,V> FPMap<K,V> toFPMap() {
        return toFPMap( VoidFunction1.noOp() );
    }

    public default <K,V> FPMap<K,V> toFPMap( VoidFunction1<K> duplicateKeyCallback ) {
        FPMap map = FPMap.emptyMap();

        for ( T value : this ) {
            if ( value instanceof HasKey ) {
                HasKey keyFetcher = (HasKey) value;
                String key        = keyFetcher.getKey().toString();

                if ( map.contains(key) ) {
                    duplicateKeyCallback.invoke( (K) key );
                }

                map = map.put( key, value );
            } else if ( value instanceof Map.Entry ) {
                Map.Entry e   = (Map.Entry) value;
                Object    key = e.getKey();

                if ( map.contains(key) ) {
                    duplicateKeyCallback.invoke( (K) key );
                }

                map = map.put( key, e.getValue() );
            } else if ( value instanceof Tuple2 ) {
                Tuple2 tuple = (Tuple2) value;
                Object key   = tuple.getFirst();

                if ( map.contains(key) ) {
                    duplicateKeyCallback.invoke( (K) key );
                }

                map = map.put( key, tuple.getSecond() );
            } else {
                throw new IllegalArgumentException( "Unknown type: " + value.getClass() );
            }
        }

        return map;
    }

    public default <K,V> Map<K,V> toJdkMap() {
        Map map = new HashMap();

        for ( T value : this ) {
            if ( value instanceof HasKey ) {
                HasKey keyFetcher = (HasKey) value;

                map.put( keyFetcher.getKey().toString(), value );
            } else if ( value instanceof Map.Entry ) {
                Map.Entry e = (Map.Entry) value;
                map.put( e.getKey(), e.getValue() );
            } else if ( value instanceof Tuple2 ) {
                Tuple2 tuple = (Tuple2) value;
                map.put( tuple.getFirst(), tuple.getSecond() );
            } else {
                throw new IllegalArgumentException( "Unknown type: " + value.getClass() );
            }
        }

        return map;
    }

    public default <K,V> FPMap<K,V> toMap( Function1<T, Tuple2<K,V>> mappingFunction ) {
        return this.fold( FPMap.emptyMap(), (soFar,next) -> {
            Tuple2<K, V> tuple = mappingFunction.invoke( next );

            return soFar.put( tuple.getFirst(), tuple.getSecond() );
        });
    }

    public default <K> FPMutableMap<K,T> toMapByKey( Function1<T, K> keyFunction ) {
        FPMutableMap<K,T> map = FPMutableMap.newMap();

        for ( T value : this ) {
            K key = keyFunction.invoke( value );

            map.put( key, value );
        }

        return map;
    }

    public default <K> FPMap<K,T> toFPMapByKey( Function1<T, K> keyFunction ) {
        return fold( FPMap.<K,T>emptyMap(), (map,v) -> map.put(keyFunction.invoke(v),v) );
    }

    public default <T> FPIterable<T> flatten() {
        List queue = new ArrayList();

        CollectionUtils.depthFirstTraversal( this, queue::add );

        return FPIterable.wrap( queue );
    }

    public default FPIterable<T> dedup() {
        return dedup( v -> v );
    }

    public default <K> FPIterable<T> dedup( Function1<T,K> keySelector ) {
        Set<K> hasSeenBefore = new HashSet<>();

        return filterNot( v -> {
            // returns true if we have seen this key before.. has a memory, updates hasSeenBefore as it goes
            K key = keySelector.invoke(v);

            if ( hasSeenBefore.contains(key) ) {
                return true;
            } else {
                hasSeenBefore.add( key );

                return false;
            }
        } );
    }

    public default long count() {
        Iterator it = iterator();
        long count = 0;

        while ( it.hasNext() ) {
            it.next();

            count++;
        }

        return count;
    }

    public default <X extends Throwable> T firstOrThrow( Function0<X> exceptionFactory ) {
        Iterator<T> it = iterator();

        if ( it.hasNext() ) {
            return it.next();
        }

        throw Backdoor.throwException(exceptionFactory.invoke());
    }

    public default void mkString( Appendable out, String separator ) {
        mkString( out, "", separator );
    }

    public default String mkString( String separator ) {
        StringBuilder buf = new StringBuilder();

        mkString( buf, separator );

        return buf.toString();
    }

    public default void mkString( Appendable out, String endOfFirstRow, String rowDelimiter ) {
        mkString( out, endOfFirstRow, rowDelimiter, (buf,v) -> buf.append(v.toString()) );
    }

    public default void mkString( Appendable out, String endOfFirstRow, String rowDelimiter, VoidFunction2WithThrows<Appendable,T> elementFormatter ) {
        boolean isFirstColumn = true;
        for ( T e : this) {
            try {
                if ( isFirstColumn ) {
                    out.append( endOfFirstRow );

                    isFirstColumn = false;
                } else {
                    out.append( rowDelimiter );
                }

                elementFormatter.invoke( out, e);
            } catch ( Throwable ex ) {
                throw Backdoor.throwException( ex );
            }
        }
    }

    public default String mkString() {
        return iterator().mkString();
    }

    public default FPOption<T> first() {
        Iterator<T> it = iterator();

        return it.hasNext() ? FPOption.of(it.next()) : FPOption.none();
    }

    public default T firstOrNull() {
        Iterator<T> it = iterator();

        return it.hasNext() ? it.next() : null;
    }

    public default T firstOrNull( BooleanFunction1<T> predicate ) {
        return filter( predicate ).firstOrNull();
    }

    public default FPOption<T> first( BooleanFunction1<T> predicate ) {
        return filter(predicate).first();
    }

    public default boolean hasFirst( BooleanFunction1<T> predicate ) {
        return filter(predicate).first().hasValue();
    }

    public default <B> FPOption<B> firstOpt( Function1WithThrows<T, FPOption<B>> mapFunc ) {
        return map(mapFunc).filter( FPOption::hasValue ).map(  FPOption::get ).first();
    }

    public default FPOption<T> last() {
        Iterator<T> it   = iterator();
        FPOption<T> prev = FPOption.none();

        while ( it.hasNext() ) {
            prev = FP.option(it.next());
        }

        return prev;
    }

    public default FPOption<T> min() {
        return min( cast(Function1.identity()) );
    }

    public default <E extends Comparable<E>> FPOption<T> min( Function1<T,E> fetcherComparableFor ) {
        return fold(
            FP.emptyOption(),
            (soFar, next) -> soFar.map( prev -> ComparableUtils.min(prev,next,fetcherComparableFor) )
                                  .or( () -> FP.option(next) )
        );
    }

    public default FPOption<T> max() {
        return max( cast(Function1.identity()) );
    }

    public default <E extends Comparable<E>> FPOption<T> max( Function1<T,E> fetcherComparableFor ) {
        return fold(
            FP.emptyOption(),
            (soFar, next) -> soFar.map( prev -> ComparableUtils.max(prev,next,fetcherComparableFor) )
                                  .or( () -> FP.option(next) )
        );
    }

    public default boolean any( BooleanFunction1<T> predicate ) {
        return first(predicate).hasValue();
    }

    public default FPOption<T> second() {
        Iterator<T> it = iterator();

        if ( it.hasNext() ) {
            it.next();

            if ( it.hasNext() ) {
                return FPOption.of(it.next());
            }
        }

        return FPOption.none();
    }

    public default T secondOrNull() {
        Iterator<T> it = iterator();

        if ( it.hasNext() ) {
            it.next();

            if ( it.hasNext() ) {
                return it.next();
            }
        }

        return null;
    }

    public default T secondOrNull( BooleanFunction1<T> predicate ) {
        return filter( predicate ).secondOrNull();
    }

    public default FPOption<T> second( BooleanFunction1<T> predicate ) {
        return filter(predicate).second();
    }

    public default boolean matches( T...targetValues ) {
        Iterator<T> it = this.iterator();
        int i = 0;

        while ( it.hasNext() ) {
            if ( targetValues.length <= i ) {
                return false;
            }

            T a = it.next();
            T b = targetValues[i];

            if ( !Objects.equals(a,b) ) {
                return false;
            }

            i++;
        }

        if ( targetValues.length != i ) {
            return false;
        }

        return true;
    }

    public default boolean contains( T targetValue ) {
        return iteratorX().contains( targetValue );
    }

    public default boolean isEvery( BooleanFunction1<T> predicate ) {
        return iteratorX().ifEvery( predicate );
    }

    public default ConsList<T> toConsList() {
        return iteratorX().toConsList();
    }

    /**
     * Return the
     * @param predicate
     * @param mapper
     * @param <V>
     * @return
     */
    public default <V> V returnFirstMismatch( BooleanFunction1<T> predicate, Function1<T,V> mapper ) {
        Iterator<T> it = this.iterator();

        while ( it.hasNext() ) {
            T next = it.next();

            if ( !predicate.invoke(next) ) {
                return mapper.invoke( next );
            }
        }

        return null;
    }

    public default <K> Map<K,T> toJdkMap( Function1<T,K> toKeyFunc ) {
        Map<K,T> map = new HashMap<>();

        for ( T v : this ) {
            K key = toKeyFunc.invoke( v );

            map.put( key, v );
        }

        return map;
    }

    public default <K,V> Map<K,V> toJdkMap( Function1<T,K> toKeyFunc, Function1<T,V> toValueFunc ) {
        Map<K,V> map = new HashMap<>();

        for ( T v : this ) {
            K key   = toKeyFunc.invoke( v );
            V value = toValueFunc.invoke( v );

            map.put( key, value );
        }

        return map;
    }

    static abstract class BaseIterable<T> implements FPIterable<T> {
        public int hashCode() {
            return ListUtils.deepHashCode( this );
        }

        public boolean equals( Object o ) {
            return ListUtils.iterableEquals( this, o );
        }

        public String toString() {
            StringBuilder buf = new StringBuilder();
            buf.append('[');

            boolean isFirst = true;
            for ( T t : this ) {
                if ( isFirst ) {
                    isFirst = false;
                } else {
                    buf.append( ", " );
                }

                buf.append( t );
            }

            buf.append(']');

            return buf.toString();
        }
    }

    static abstract class BaseIntIterable implements IntIterable {
        public int hashCode() {
            int hashCodeSoFar = 17;

            IntIterator it = iterator();
            while ( it.hasNext() ) {
                hashCodeSoFar = 31*hashCodeSoFar + it.next();
            }

            return hashCodeSoFar;
        }

        public boolean equals( Object o ) {
            if ( !(o instanceof IntIterable) ) {
                return false;
            } else if ( this == o ) {
                return true;
            }

            IntIterable other = (IntIterable) o;
            IntIterator it1 = this.iterator();
            IntIterator it2 = other.iterator();

            while ( it1.hasNext() ) {
                if ( !it2.hasNext() ) {
                    return false;
                }

                int a = it1.next();
                int b = it2.next();

                if ( a != b ) {
                    return false;
                }
            }

            return !it2.hasNext();
        }

        public String toString() {
            StringBuilder buf = new StringBuilder();
            buf.append('[');

            boolean isFirst = true;
            IntIterator it = iterator();
            while ( it.hasNext() ) {
                if ( isFirst ) {
                    isFirst = false;
                } else {
                    buf.append( ", " );
                }

                buf.append( it.next() );
            }

            buf.append(']');

            return buf.toString();
        }
    }

    static abstract class BaseLongIterable implements LongIterable {
        public String toString() {
            StringBuilder buf = new StringBuilder();
            buf.append('[');

            boolean isFirst = true;
            LongIterator it = iterator();
            while ( it.hasNext() ) {
                if ( isFirst ) {
                    isFirst = false;
                } else {
                    buf.append( ", " );
                }

                buf.append( it.next() );
            }

            buf.append(']');

            return buf.toString();
        }
    }

    static abstract class BaseDoubleIterable implements DoubleIterable {
        public String toString() {
            StringBuilder buf = new StringBuilder();
            buf.append('[');

            boolean isFirst = true;
            DoubleIterator it = iterator();
            while ( it.hasNext() ) {
                if ( isFirst ) {
                    isFirst = false;
                } else {
                    buf.append( ", " );
                }

                buf.append( it.next() );
            }

            buf.append(']');

            return buf.toString();
        }
    }
}
