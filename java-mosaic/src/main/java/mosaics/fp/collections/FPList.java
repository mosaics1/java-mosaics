package mosaics.fp.collections;

import mosaics.lang.functions.VoidFunction0;
import mosaics.lang.BigIntegerX;
import mosaics.utils.ListUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Objects;


@SuppressWarnings( "unchecked" )
public interface FPList<T> extends FPIterable<T>, Cloneable {

    @SafeVarargs
    public static <T> FPList<T> wrapArray( T...array ) {
        return wrapList( Arrays.asList(array) );
    }

    public static <T> FPList<T> wrapCollection( Collection<T> collection ) {
        return wrapList( new ArrayList<>(collection) );
    }

    public static <T> FPList<T> wrapList( java.util.List<T> list ) {
        return new FPList<T>() {
            public FPIterator<T> iterator() {
                return FPIterator.wrap(list.iterator());
            }

            public T get( int i ) {
                return list.get(i);
            }

            public int size() {
                return list.size();
            }

            // todo cut this mutable crap out
            public void add( T t ) {
                list.add( t );
            }

            public void set( int i, T v ) {
                list.set( i, v );
            }

            public void clear() {
                list.clear();
            }

            public void remove( Object o ) {
                list.remove( o );
            }

            public String toString() {
                return mkString();
            }

            public FPList<T> clone() {
                return wrapList( new ArrayList<>(list) );
            }


            public int hashCode() {
                return ListUtils.deepHashCode( iterator() );
            }

            public boolean equals( Object o ) {
                return ListUtils.iterableEquals( this, o );
            }
        };
    }

    static <T> FPList<T> newArrayList() {
        return wrapList( new ArrayList<T>() );
    }

    static <T> FPList<T> newArrayList( int len ) {
        return wrapList( new ArrayList<T>(len) );
    }

    static <T> FPList<T> newLinkedList() {
        return wrapList( new LinkedList<T>() );
    }

    public T get( int i );
    public int size();
    public void add( T t );

    public void clear();
    public void remove( Object o );


    public FPList<T> clone();

    public default void addAll( T...array ) {
        for ( T e : array ) {
            this.add(e);
        }
    }

    /**
     * Append the specified range from this list into the specified destination list.
     */
    public default void copyInto( FPList<T> dest, int from, int toExc ) {
        for ( int i=from; i<toExc; i++ ) {
            dest.add( get(i) );
        }
    }

    public default FPOption<T> last() {
        if ( this.size() == 0 ) {
            return FPOption.none();
        } else {
            return FPOption.of( this.get(this.size()-1) );
        }
    }

    /**
     * Append the specified range from this list into the specified destination list.
     */
    public default void copyInto( FPList<T> dest, int from ) {
        copyInto( dest, from, size() );
    }

    public default FPIterator<FPList<T>> permutations() {
        // Algorithm inspired by https://en.wikipedia.org/wiki/Steinhaus%E2%80%93Johnson%E2%80%93Trotter_algorithm
        //
        // The key observation is that every item within the permutation can be calculated one swap at a time.
        // The secret is to order the permutations such that the next item is always one swap away.  This may be
        // done by following a re-occurring sequence of swaps similar to rocking a baby and saying 'weeee'
        // at the top of each swing on either side.
        //
        // An example will make this clear:
        //    - we start with the following sequence
        //           1, 2, 3, 4
        //
        //    - move the first value (1) to the rhs one swap at a time   (in our metaphor this is a single swing of the baby in ones arms from
        //      the left hand side of our body to the right hand side)
        //      eg
        //           (2, 1), 3,  4     // NB brackets show where we did the swap
        //           2, (3,  1), 4
        //           2,  3, (4,  1)
        //
        //    - now we swap the values at the far left hand side    (in our metaphor we say 'wee' at the top of the swing)
        //           (3, 2), 4,  1
        //
        //    - now we move the one back to the lhs one swap at a time   (we rock the baby in the opposite direction)
        //           3,  2, (1,  4)
        //           3, (1,  2), 4
        //           (1, 3), 2,  4
        //
        //    - and now we swap the two on the far rhs    ( we say 'weee' again)
        //           1,  3, (4,  2)
        //
        //   -  and now the algorithm repeats, sliding the one to the other side. swap the opposite side and then
        //      slide back.   The slide operation happens exactly (n-1)! times.  That is 6 times in this example
        //      because we have 4 elements in the list (n=4).

        return new FPIterator<FPList<T>>() {
            private final FPList<T> workingList   = FPList.this.clone();

            // NB implemented as a functional state machine.  The nextOp is a function pointer to a method that
            //    will perform the next swap and then selects which function will be called to do the next swap after
            //    that.  We have one function for each flow of the algorithm, sliding the value to the rhs, swapping the
            //    lhs, sliding to the lhs and swapping the rhs.  The algorithm transitions in a loop between each of those
            //    methods in turn until permCountDown reaches zero.

            private BigIntegerX permCountDown = new BigIntegerX(workingList.size()).factorial();
            private VoidFunction0 nextOp        = this::initialValueUnchanged;
            private int           i             = 0;

            protected boolean _hasNext() {
                return permCountDown.isGTZero();
            }

            protected FPList<T> _next() {
                nextOp.invoke();

                return workingList.clone();
            }

            private void initialValueUnchanged() {
                this.nextOp = this::slideRight;

                this.permCountDown = permCountDown.subtract(1);
            }

            private void slideRight() {
                swap(i, i + 1);

                i += 1;

                if ( i == workingList.size() - 1 ) {
                    this.permCountDown = permCountDown.subtract(workingList.size()-1);

                    this.nextOp = this::swapLHS;
                }
            }

            private void swapLHS() {
                swap( 0, 1 );

                this.permCountDown = permCountDown.subtract(1);

                this.nextOp = this::slideLeft;
            }

            private void slideLeft() {
                swap(i - 1, i);

                i -= 1;

                if ( i == 0 ) {
                    this.permCountDown = permCountDown.subtract(workingList.size()-1);

                    this.nextOp = this::swapRHS;
                }
            }

            private void swapRHS() {
                int n = workingList.size();

                swap(n - 2, n - 1);

                this.permCountDown = permCountDown.subtract(1);

                this.nextOp = this::slideRight;
            }

            private void swap( int i, int j ) {
                T v = workingList.get(i);

                workingList.set( i, workingList.get(j) );
                workingList.set( j, v );
            }
        };
    }

    public void set( int i, T v );

    public default int indexOf( T v ) {
        int i = 0;

        for ( T c : this ) {
            if ( Objects.equals(v,c) ) {
                return i;
            }

            i++;
        }

        return -1;
    }

    public default boolean replace( T currentValue, T newValue ) {
        int i = indexOf( currentValue );

        if ( i < 0 ) {
            return false;
        } else {
            set( i, newValue );

            return true;
        }
    }
}
