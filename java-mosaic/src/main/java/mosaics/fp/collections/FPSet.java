package mosaics.fp.collections;

import mosaics.utils.ListUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@SuppressWarnings( "unchecked" )
public interface FPSet<T> extends FPIterable<T>, Cloneable {

    @SafeVarargs
    public static <T> FPSet<T> wrapArray( T...array ) {
        return wrapList( Arrays.asList(array) );
    }

    public static <T> FPSet<T> wrapCollection( Collection<T> collection ) {
        return wrapList( new ArrayList<>(collection) );
    }

    public static <T> FPSet<T> wrapList( List<T> list ) {
        Set<T> set = new HashSet<>();
        set.addAll( list );

        return wrapSet( set );
    }

    public static <T> FPSet<T> wrapSet( Set<T> set ) {
        return new FPSet<T>() {
            public FPIterator<T> iterator() {
                return FPIterator.wrap(set.iterator());
            }

            public boolean contains( T v ) {
                return set.contains(v);
            }

            public int size() {
                return set.size();
            }

            public FPSet<T> add( T t ) {
                Set<T> clone = new HashSet<>(set);

                clone.add(t);

                return FPSet.wrapSet(clone);
            }

            public FPSet<T> remove( T t ) {
                Set<T> clone = new HashSet<>(set);

                clone.remove(t);

                return FPSet.wrapSet(clone);
            }

            public void clear() {
                set.clear();
            }

            public String toString() {
                return mkString();
            }

            public FPSet<T> clone() {
                return wrapSet( new HashSet<>(set) );
            }


            public int hashCode() {
                return ListUtils.deepHashCode( iterator() );
            }

            public boolean equals( Object o ) {
                return ListUtils.iterableEquals( this, o );
            }
        };
    }

    static <T> FPSet<T> newSet() {
        return wrapList( new ArrayList<T>() );
    }


    public boolean contains( T v );
    public int size();
    public FPSet<T> add( T t );
    public FPSet<T> remove( T t );

    public void clear();


    public FPSet<T> clone();

    public default FPSet<T> addAll( T...array ) {
        return addAll(Arrays.asList(array));
    }

    public default FPSet<T> addAll( Iterable<T> iterable ) {
        FPSet<T> result = this;

        for ( T e : iterable ) {
            result = result.add(e);
        }

        return result;
    }

    public default FPSet<T> removeAll( T...array ) {
        return removeAll(Arrays.asList(array));
    }

    public default FPSet<T> removeAll( Iterable<T> iterable ) {
        FPSet<T> result = this;

        for ( T e : iterable ) {
            result = result.remove(e);
        }

        return result;
    }

}
