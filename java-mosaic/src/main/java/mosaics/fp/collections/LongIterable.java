package mosaics.fp.collections;

import mosaics.lang.functions.LongFunction0;

import java.util.List;
import java.util.function.LongBinaryOperator;
import java.util.function.LongUnaryOperator;


public interface LongIterable {

    public static LongIterable wrap( long[] array ) {
        return new LongIterable() {
            @Override
            public LongIterator iterator() {
                return LongIterator.wrap( array );
            }
        };
    }

    /**
     * Generate an infinite number series by wrapping a function that maps an index (0..MaxLong)
     * to another long.
     */
    public static LongIterable wrap( LongUnaryOperator supplier ) {
        return new LongIterable() {
            public LongIterator iterator() {
                return new LongIterator() {
                    private long i = 0;

                    public boolean hasNext() {
                        return i < Long.MAX_VALUE;
                    }

                    public long next() {
                        long nextIndex = i;

                        i += 1;

                        return supplier.applyAsLong( nextIndex );
                    }
                };
            }
        };
    }

    /**
     * Create an iterator that combines the zipped values from two separate iterators.
     */
    static LongIterator combine( LongIterator lhs, LongIterator rhs, LongBinaryOperator aggregator ) {
        return new LongIterator() {
            public boolean hasNext() {
                return lhs.hasNext() && rhs.hasNext();
            }

            public long next() {
                long a = lhs.next();
                long b = rhs.next();

                return aggregator.applyAsLong(a, b);
            }
        };
    }


    public LongIterator iterator();


    public default long sum() {
        return fold(( a, b ) -> a + b);
    }

    /**
     * Combine all of the values using the supplied aggregation function.
     *
     * @param aggregator (soFar,newValue)
     * @return
     */
    public default long fold( LongBinaryOperator aggregator ) {
        return iterator().fold(aggregator);
    }

    public default LongIterable map( LongUnaryOperator mappingFunction ) {
        return new LongIterable() {
            public LongIterator iterator() {
                LongIterator it = LongIterable.this.iterator();

                return new LongIterator() {
                    public boolean hasNext() {
                        return it.hasNext();
                    }

                    public long next() {
                        long v = it.next();

                        return mappingFunction.applyAsLong( v );
                    }
                };
            }
        };
    }

    /**
     * For each long v in the series, returns Math.max(v, maxValue).
     */
    public default LongIterable max( long maxValue ) {
        return this.map( v -> Math.max(v, maxValue) );
    }

    /**
     * For each long v in the series, returns Math.min(v, minValue).
     */
    public default LongIterable min( long minValue ) {
        return this.map(v -> Math.min(v, minValue));
    }

    /**
     * For each long v in the series, returns Math.max(v, maxValue).
     */
    public default LongIterable max( LongFunction0 maxValueFetcher ) {
        return max(maxValueFetcher.invoke());
    }

    /**
     * For each long v in the series, returns Math.min(v, minValue).
     */
    public default LongIterable min( LongFunction0 minValueFetcher ) {
        return min(minValueFetcher.invoke());
    }

    public default List<Long> toList() {
        return iterator().toList();
    }
}
