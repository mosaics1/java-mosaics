package mosaics.fp.collections;

import mosaics.lang.Backdoor;
import mosaics.lang.QA;
import mosaics.lang.TryUtils;
import mosaics.lang.functions.Function0;
import mosaics.lang.functions.Function0WithThrows;
import mosaics.lang.functions.Function1WithThrows;
import mosaics.lang.functions.VoidFunction0WithThrows;
import mosaics.lang.functions.VoidFunction1;
import mosaics.lang.functions.VoidFunction1WithThrows;

import java.util.Collections;
import java.util.List;
import java.util.Objects;


/**
 *
 */
public class Some<T> extends FPOption<T> {

    private final T v;

    public Some( T v ) {
        QA.argNotNull( v, "v" );

        this.v = v;
    }

    public T get() {
        return v;
    }

    public boolean hasValue() {
        return true;
    }

    public boolean isEmpty() {
        return false;
    }

    public boolean isBlank() {
        return v instanceof String && ((String) v).trim().length() == 0;
    }

    public <B> FPOption<B> map( Function1WithThrows<T, ? extends B> mappingFunction ) {
        B mappedValue = TryUtils.invokeAndRethrowException( () -> mappingFunction.invoke( v ) );

        return mappedValue == v ? Backdoor.cast(this) : new Some<>( mappedValue );
    }

    public <B> FPOption<B> flatMap( Function1WithThrows<T, FPIterable<B>> mappingFunction ) {
        return TryUtils.invokeAndRethrowException(() -> mappingFunction.invoke(v)).toFPOption().cast();
    }

    public T orElse( T value ) {
        return v;
    }

    public T orElse( Function0WithThrows<T> fetcher ) {
        return v;
    }

    public FPOption<T> or( FPOption<T> alt ) {
        return this;
    }

    public FPOption<T> or( Function0<FPOption<T>> fetcher ) {
        return this;
    }

    public <X extends Throwable> FPOption<T> orThrow( Function0<X> exceptionFactory ) {
        return this;
    }

    public FPIterable<T> toIterable() {
        return FPIterable.wrapSingleton( v );
    }

    public List<T> toList() {
        return Collections.singletonList( v );
    }

    public <X extends Throwable> T orElseThrow( Function0<X> exceptionFactory ) {
        return v;
    }

    public void ifPresent( VoidFunction1WithThrows<T> op ) {
        TryUtils.invokeAndRethrowException( () -> op.invoke(v) );
    }

    public void ifEmpty( VoidFunction0WithThrows op ) {}

    public <B> FPOption<B> ifElse( Function1WithThrows<T,B> ifPresentFunc, Function0WithThrows<B> ifEmptyFunc ) {
        return of( ifPresentFunc.invokeSilently(v) );
    }

    public T orNull() {
        return v;
    }

    public FPIterable<T> and( FPOption<T> other ) {
        if ( other.isEmpty() ) {
            return this.toIterable();
        } else {
            return FPIterable.wrapAll( v, other.get() );
        }
    }

    public FPIterator<T> iterator() {
        return toIterable().iteratorX();
    }

    public void forEach( VoidFunction1<T> f ) {
        f.invoke( v );
    }

    public String toString() {
        return "Some("+v.toString()+")";
    }

    public int hashCode() {
        return v.hashCode();
    }

    public boolean equals( Object o ) {
        if ( o == this ) {
            return true;
        } else if ( !(o instanceof Some) ) {
            return false;
        }

        Some other = (Some) o;
        return Objects.equals( this.v, other.v );
    }

}
