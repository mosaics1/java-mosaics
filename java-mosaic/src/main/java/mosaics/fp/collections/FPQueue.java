package mosaics.fp.collections;

import mosaics.fp.FP;
import mosaics.lang.QA;

import java.util.LinkedList;


/**
 * A FIFO queue.
 */
public class FPQueue<T> implements FPIterable<T> {
    private LinkedList<T> queue = new LinkedList<>();

    public FPQueue() {}

    public FPQueue( T[] args ) {
        pushAll( args );
    }



    public void push( T v ) {
        QA.argNotNull(v, "v");

        queue.addFirst( v );
    }

    public T pop() {
        return queue.removeLast();
    }

    public T peek() {
        return queue.getLast();
    }

    public boolean isEmpty() {
        return queue.isEmpty();
    }

    public boolean hasContents() {
        return !isEmpty();
    }

    public FPIterator<T> iterator() {
        return FP.wrap(queue.descendingIterator());
    }

    public void pushAll( T[] elements ) {
        if ( elements == null ) {
            return;
        }

        for ( T e : elements ) {
            push(e);
        }
    }
}
