package mosaics.fp.collections;

import java.util.ArrayList;


public interface IntIterator {
    public boolean hasNext();
    public int next();

    public default int[] toIntArray() {
        ArrayList<Integer> arrayList = new ArrayList<>();

        while ( hasNext() ) {
            arrayList.add( next() );
        }

        int[] result = new int[arrayList.size()];
        for ( int i=0; i<arrayList.size(); i++ ) {
            result[i] = arrayList.get( i );
        }

        return result;
    }
}
