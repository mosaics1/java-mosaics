package mosaics.fp;

import mosaics.lang.functions.Function1;
import mosaics.lang.functions.VoidFunction1;
import mosaics.lang.functions.Function1;
import mosaics.lang.functions.VoidFunction1;


/**
 * A tryable is a unit of work that may or may not succeed.
 */
public interface Tryable<T> {

    /**
     * Returns true if the tryable has a result or been marked as failed.  False means that
     * the calculation is still in progress.
     */
    public boolean isComplete();

    /**
     * Returns true if the try contains the successful result.  False means
     * that the try contains a failure of some kind.
     */
    public boolean hasResult();


    /**
     * Returns true if the try contains a failure.  A failure represents
     * that something went wrong, which may or may not be exceptional.
     */
    public boolean hasFailure();

    /**
     * Returns the result of the job if available.  This method will return null
     * when the try contains a failure or a null result. Thus confirming the state of the
     * future before calling this method will avoid ambiguity of what null means.
     *
     * @throws IllegalStateException if the tryble failed
     */
    public T getResult();

    /**
     * Returns the description of why the job failed.  This method will return null
     * when the job has not failed.
     *
     * @throws IllegalStateException if the tryable has not failed
     */
    public Failure getFailure();

    /**
     * Creates a new Try that will contain the mapped result of this try.  If this
     * try has failed then this method will return this try unmodified.
     */
    public <B> Tryable<B> mapResult( final Function1<T, B> mappingFunction );

    /**
     * Creates a new Try that will contain the mapped result of this try.  If this
     * try has failed then this method will return this try unmodified.<p/>
     * <p>
     * This method differs from mapResult in that the mapping function returns
     * another Try.  This method will return that Try directly and will not
     * wrap it with another Try the way that mapResult would.
     */
    public <B> Tryable<B> flatMapResult( final Function1<T, Tryable<B>> mappingFunction );


    /**
     * Offers the opportunity to recover from a failure. If this Try contains
     * a result then this instance will be returned unmodified.  Otherwise
     * on a failure the recovery function will be invoked and a new Try
     * created containing its result will be returned.
     */
    public Tryable<T> recover( final Function1<Failure, T> recoveryFunction );


    /**
     * Offers the opportunity to recover from a failure. If this Try contains
     * a result then this instance will be returned unmodified.  Otherwise
     * on a failure the recovery function will be invoked and its result returned
     * unmodified.
     */
    public Tryable<T> flatRecover( final Function1<Failure, Tryable<T>> recoveryFunction );


    /**
     * Offers the opportunity to modify the description of a failed task. If this
     * Try contains a result, then this instance will be returned unmodified.
     * On the other hand if the Try contains a failure, then the mappingFunction
     * will be invoked and its result will be wrapped in a new Try and returned.
     */
    public Tryable<T> mapFailure( final Function1<Failure, Failure> mappingFunction );


    /**
     * Offers the opportunity to modify the description of a failed task. If this
     * Try contains a result, then this instance will be returned unmodified.
     * On the other hand if the Try contains a failure, then the mappingFunction
     * will be invoked and its result will be returned unmodified.
     */
    public Tryable<T> flatMapFailure( final Function1<Failure, Tryable<T>> mappingFunction );


    /**
     * Registers a callback to be called when this future is marked with a value of T.<p/>
     * <p>
     * If registered before this future is completed, then the callback will be
     * called from the same thread that completed the future.  If the future is
     * already completed then the callback will be invoked immediately from the calling
     * thread before onResult returns.
     */
    public Tryable<T> onSuccess( VoidFunction1<T> callback );

    /**
     * Registers a callback to be called when this future is marked with a Failure.<p/>
     * <p>
     * If registered before this future is completed, then the callback will be
     * called from the same thread that completed the future.  If the future is
     * already completed then the callback will be invoked immediately from the calling
     * thread before onResult returns.
     */
    public Tryable<T> onFailure( VoidFunction1<Failure> callback );

    /**
     * Throws an exception if the Tryable has failed else returns itself so that it can be chained.
     */
    public Tryable<T> throwIffFailure();


    public default String getFailureMessage() {
        return getFailure().toString();
    }
}
