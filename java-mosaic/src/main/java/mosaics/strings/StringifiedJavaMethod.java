package mosaics.strings;

import mosaics.fp.FP;
import mosaics.fp.collections.FPIterable;
import mosaics.fp.collections.FPOption;
import mosaics.lang.QA;
import mosaics.lang.functions.Function1;
import mosaics.lang.reflection.JavaClass;
import mosaics.lang.reflection.JavaMethod;
import mosaics.lang.reflection.JavaParameter;
import mosaics.lang.reflection.ReflectionException;

import java.lang.annotation.Annotation;
import java.lang.reflect.Executable;
import java.lang.reflect.Method;


public class StringifiedJavaMethod implements JavaMethod {
    public static JavaMethod stringify( JavaMethod originalMethod, Function1<JavaClass,FPOption<StringCodec>> codecFactory ) {
        if ( isAlreadyStringified(originalMethod) ) {
            return originalMethod;
        }

        return new StringifiedJavaMethod( originalMethod, codecFactory );
    }

    private static boolean isAlreadyStringified( JavaMethod m ) {
        return m.getParameters().isEvery( StringifiedJavaMethod::isAlreadyStringified );
    }

    private static boolean isAlreadyStringified( JavaParameter p ) {
        return p.getType().isString();
    }


    private JavaMethod              originalMethod;
    private int                     parameterCount;
    private StringCodec[]           codecs;
    private FPOption<VarArgHandler> varArgHandler;

    private StringifiedJavaMethod( JavaMethod originalMethod, Function1<JavaClass, FPOption<StringCodec>> codecFactory ) {
        VarArgSupport.validateVarArgParameters( originalMethod );

        this.originalMethod = originalMethod;
        this.parameterCount = originalMethod.getParameterCount();
        this.codecs         = VarArgSupport.selectStringCodecsForNonVarArgParameters(codecFactory, originalMethod);
        this.varArgHandler  = VarArgSupport.createVarArgHandlerFor(codecFactory, originalMethod);
    }

    public String getName() {
        return originalMethod.getName();
    }

    public FPIterable<JavaParameter> getParameters() {
        return FP.wrap( originalMethod.getParameters() )
            .map(
                p -> p.withType(JavaClass.STRING)
                      .withSource(this)
            );
    }

    public int getModifiers() {
        return originalMethod.getModifiers();
    }

    public FPOption<Executable> getJdkExecutable() {
        return FP.emptyOption();
    }

    public FPIterable<Annotation> getAnnotations() {
        return originalMethod.getAnnotations();
    }

    public JavaClass getReturnType() {
        return originalMethod.getReturnType();
    }

    public FPOption<Method> getJdkMethod() {
        return FP.emptyOption();
    }

    public <T extends Annotation> FPOption<T> getAnnotation( Class<T> annotationType ) {
        return originalMethod.getAnnotation( annotationType );
    }

    public Object invokeAgainst( Object object, Object... args ) {
        Object[] collectedArgs = convertArgs( args );

        return originalMethod.invokeAgainst( object, collectedArgs );
    }

    public Object invokeStaticAgainst( Object...args ) {
        Object[] collectedArgs = convertArgs( args );

        return originalMethod.invokeStaticAgainst( collectedArgs );
    }

    @Override
    public JavaClass getDeclaringClass() {
        return originalMethod.getDeclaringClass();
    }

    private Object[] convertArgs( Object[] args ) {
        if ( varArgHandler.isEmpty() ) {
            QA.argIsEqualTo( args.length, parameterCount, "args.length", "codecs.length" );
        } else {
            QA.argIsGTE( args.length, parameterCount-1, "args.length", "codecs.length" );
        }

        Object[] collectedArgs = new Object[parameterCount];
        for ( int i=0; i<codecs.length; i++ ) {
            try {
                collectedArgs[i] = codecs[i].decode( (String) args[i] ).getResult();
            } catch ( IllegalArgumentException | IllegalStateException ex ) {
                String argName = getParameters().toList().get(i).getName();

                throw ReflectionException.recast("Malformed value '"+args[i]+"' for arg '"+argName+"'", ex);
            }
        }

        if ( varArgHandler.hasValue() ) {
            collectedArgs[parameterCount-1] = varArgHandler.get().aggregate( args, parameterCount-1 );
        }

        return collectedArgs;
    }

}
