package mosaics.strings;

import mosaics.fp.Tryable;
import mosaics.lang.reflection.JavaClass;


/**
 * A StringCodec can convert an object into a string and back again.
 */
public interface StringCodec<T> {
    public String encode( T obj );
    public Tryable<T> decode( String text );

    public boolean supportsNull();
    public JavaClass getType();
}
