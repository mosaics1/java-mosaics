package mosaics.strings;

import mosaics.fp.FP;
import mosaics.fp.Failure;
import mosaics.fp.collections.FPOption;
import lombok.AllArgsConstructor;
import lombok.Value;


@Value
@AllArgsConstructor
public class MalformedValueFailure implements Failure {

    private String           value;
    private FPOption<String> validValues;

    public MalformedValueFailure( String value ) {
        this( value, FP.emptyOption() );
    }

    public MalformedValueFailure( String value, String validValues ) {
        this( value, FP.option(validValues) );
    }


    @Override
    public Throwable toException() {
        String msg = "'"+value+"' is malformed.";

        if ( validValues.hasValue() ) {
            msg += " Valid values: " + validValues.get();
        }

        return new IllegalArgumentException( msg );
    }

}
