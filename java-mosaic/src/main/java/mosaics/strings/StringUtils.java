package mosaics.strings;

import mosaics.fp.FP;
import mosaics.fp.collections.FPOption;
import mosaics.strings.parser.CharacterIterator;
import mosaics.strings.parser.CharacterMatcher;
import mosaics.strings.parser.PullParser;
import mosaics.fp.collections.FPIterator;
import mosaics.lang.functions.BooleanFunctionC;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Base64;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class StringUtils {

    /**
     * Returns the string that was matched by the regexp.
     */
    public static String extractFrom( String s, String regexp ) {
        Pattern p = Pattern.compile( regexp );
        Matcher m = p.matcher(s);

        return m.find() ? m.group(0) : null;
    }

    /**
     *
     * @return will return an empty string if the matching character is at the start of the input string
     */
    public static FPOption<String> extractUpToFirstMatch( String str, char delimitor ) {
        int i = str.indexOf( delimitor );

        return i < 0 ? FP.emptyOption() : FP.option( str.substring(0,i) );
    }

    /**
     *
     * @return will return an empty string if the matching character is at the start of the input string
     */
    public static FPOption<String> extractUpToLastMatch( String str, char delimitor ) {
        int i = str.lastIndexOf( delimitor );

        return i < 0 ? FP.emptyOption() : FP.option( str.substring(0,i) );
    }

    /**
     *
     * @return Will return an empty string if the matching character is at the end of the input string
     */
    public static FPOption<String> extractAfterFirstMatch( String str, char delimitor ) {
        int i = str.indexOf( delimitor );

        if ( i == str.length()-1 ) {
            return FP.option( "" );
        } else {
            return i < 0 ? FP.emptyOption() : FP.option( str.substring( i + 1 ) );
        }
    }

    /**
     *
     * @return Will return an empty string if the matching character is at the end of the input string
     */
    public static FPOption<String> extractAfterLastMatch( String str, char delimitor ) {
        int i = str.lastIndexOf( delimitor );

        if ( i == str.length()-1 ) {
            return FP.option( "" );
        } else {
            return i < 0  ? FP.emptyOption() : FP.option( str.substring( i + 1 ) );
        }
    }

    public static String toString( Throwable ex ) {
        StringWriter stringWriter = new StringWriter(  );

        ex.printStackTrace( new PrintWriter(stringWriter) );

        return stringWriter.toString();
    }

    public static String capitalizeFirstCharacter( String s ) {
        if ( s == null || s.length() == 0 ) {
            return s;
        }

        char c = s.charAt( 0 );
        if ( !Character.isLowerCase(c) ) {
            return s;
        }

        return Character.toUpperCase(c) + s.substring(1);
    }

    public static String concat( Object[] elements, String separator) {
        return concat( "", elements, separator, "");
    }

    public static String concat( String prefix, String postfix, String separator) {
        if ( isBlank(prefix) ) {
            return postfix;
        }

        return prefix + separator + postfix;
    }

    public static String concat( String prefix, Object[] elements, String separator, String postfix ) {
        StringBuilder buf = new StringBuilder(100);

        buf.append( prefix );

        for ( int i=0; i<elements.length; i++ ) {
            if ( i != 0 ) {
                buf.append( separator );
            }
            buf.append( elements[i] );
        }

        buf.append( postfix );

        return buf.toString();
    }

    public static String concat( List elements, String prefix, String separator, String postfix ) {
        StringBuilder buf = new StringBuilder(100);

        buf.append( prefix );

        for ( int i=0; i<elements.size(); i++ ) {
            if ( i != 0 ) {
                buf.append( separator );
            }
            buf.append( elements.get(i) );
        }

        buf.append( postfix );

        return buf.toString();
    }

    public static boolean isEmpty( String chars ) {
        return chars == null || chars.length() == 0;
    }

    public static boolean isBlank( String chars ) {
        return chars == null || chars.trim().length() == 0;
    }

    public static boolean isBlank( String[] lines ) {
        return lines == null || lines.length == 0 || (lines.length == 1 && isBlank(lines[0]));
    }

    public static boolean isBlank( Iterable<String> lines ) {
        return lines == null || FP.wrap(lines).filterNot( StringUtils::isBlank ).count() == 0;
    }

    public static boolean isNotBlank( String str ) {
        return !isBlank(str);
    }

    public static String join( String...elements ) {
        StringBuilder buf = new StringBuilder(100);

        for ( String s: elements ) {
            buf.append( s );
        }

        return buf.toString();
    }

    public static String repeat( int numTimes, char c ) {
        StringBuilder buf = new StringBuilder( numTimes );

        repeat( buf, numTimes, c );

        return buf.toString();
    }

    public static void repeat( StringBuilder buf, int numTimes, char c ) {
        for ( int i=0; i<numTimes; i++ ) {
            buf.append(c);
        }
    }

    /**
     * Given two strings, compares them in order and identifies the index of
     * the first char that does not match between them.  For example
     * given 'abc' and 'abd' it will return 2.  'ab' and 'ba' will return
     * 0.
     */
    public static int endIndexExcOfCommonPrefix(String a, String b) {
        int max = Math.min( a.length(), b.length() );
        for ( int i=0; i<max; i++ ) {
            if ( a.charAt(i) != b.charAt(i) )  {
                return i;
            }
        }

        return max;
    }


    public static String toLowerCase( String string ) {
        return string == null ? string : string.toLowerCase();
    }

    public static String trim( String string ) {
        return string == null ? string : string.trim();
    }

    public static String trimLeft( String string ) {
        int len = string.length();
        int i   = 0;

        while ( i < len ) {
            char c = string.charAt(i);

            if ( c == ' ' || c == '\t' ) {
                i += 1;
            } else {
                return string.substring(i);
            }
        }

        return "";
    }

    public static String trimRight( String string ) {
        int len = string.length();
        int i   = len;

        while ( i > 0 ) {
            char c = string.charAt(i-1);

            if ( c == ' ' || c == '\t' ) {
                i--;
            } else if ( i == len ) {
                return string;
            } else {
                return string.substring(0,i);
            }
        }

        return "";
    }

    public static String removePostFix( String string, String postfix ) {
        return string != null && string.endsWith(postfix) ? string.substring(0,string.length()-postfix.length()) : string;
    }

    /**
     * Return the string up to and excluding the specified character.
     */
    public static String upto( String str, char c ) {
        if ( str == null ) {
            return null;
        }

        int i = str.indexOf(c);

        return i >= 0 ? str.substring( 0, i ) : str;
    }

    /**
     * Return the string from and excluding the specified character.  If the character is not found
     * then it returns the empty string.
     */
    public static String after( String str, char c ) {
        if ( str == null ) {
            return null;
        }

        int i = str.indexOf(c);

        return i >= 0 ? str.substring( i+1 ) : "";
    }

    /**
     * Return the string from and excluding the specified character.  If the character is not found
     * then it returns the empty string.
     */
    public static String after( String str, String substr ) {
        if ( str == null ) {
            return null;
        }

        int i = str.indexOf(substr);

        return i >= 0 ? str.substring( i+substr.length() ) : "";
    }

    /**
     * Return the string before and excluding the specified character.  If the character is not found
     * then it returns the original string.
     */
    public static String before( String str, char c ) {
        if ( str == null ) {
            return null;
        }

        int i = str.indexOf(c);

        return i >= 0 ? str.substring(0,i) : str;
    }

    public static String dropWhile( String str, BooleanFunctionC predicate ) {
        if ( str == null ) {
            return null;
        }

        int i   = 0;
        int max = str.length();

        while ( i < max && predicate.invoke(str.charAt(i)) ) {
            i++;
        }

        return str.substring(i);
    }

    public static String lowercaseFirstLetter( String str ) {
        if ( isBlank(str) || Character.isLowerCase(str.charAt(0)) ) {
            return str;
        }

        return Character.toLowerCase(str.charAt(0)) + str.substring(1);
    }


    public static String uppercaseFirstLetter( String str ) {
        if ( isBlank(str) || Character.isUpperCase(str.charAt(0)) ) {
            return str;
        }

        return Character.toUpperCase(str.charAt(0)) + str.substring(1);
    }






    /**
     * An array of max values for n digit numbers (base 10). Index 0 is for single
     * digit numbers, index 1 is for two digit, 2 for three digits and so on (i+1).
     */
    private static long[] MAX_NUMBER_BOUNDARIES = generateMaxValuesForBase10();

    /**
     * For each boundary in MAX_NUMBER_BOUNDARIES, stores how many characters a string
     * representation of that number would require.
     */
    private static int[] NUM_DIGITS_FOR_NUMBER_BOUNDARIES = generateDigitLengthsMatchingNumberBoundaries();


    private static long[] generateMaxValuesForBase10() {
        long[] buf = new long[19*2];

        long soFar = -1000000000000000000L;

        for ( int i=0; i<buf.length; i++ ) {
            buf[i] = i > 20 && soFar < 0 ? Long.MAX_VALUE : soFar;

            if ( soFar < 0 ) {
                soFar = soFar/10;

                if ( soFar == 0 ) {
                    soFar = 9;
                }
            } else if ( soFar >= 0 ) {
                soFar = soFar*10 + 9;
            }
        }

        return buf;
    }

    private static int[] generateDigitLengthsMatchingNumberBoundaries() {
        int[] buf = new int[19*2];

        int soFar = 20;
        for ( int i=0; i<buf.length; i++ ) {
            buf[i] = soFar;

            if ( i < 19 ) {
                soFar -= 1;
            } else {
                soFar += 1;
            }
        }

        return buf;
    }

    private static final byte[] byteBoundaries        = new byte[] {-100,-10,-1,9,99,Byte.MAX_VALUE};
    private static final byte[] byteBoundariesLengths = new byte[] {  4,   3, 2,1, 2, 3};

    public static int countNumberOfCharactersNeededToRepresentByte( byte v ) {
        for ( int i=byteBoundaries.length-2; i>=0; i-- ) {   // array is too small to make a special case out of neg numbers
            if ( v > byteBoundaries[i] ) {
                return byteBoundariesLengths[i+1];
            }
        }

        return byteBoundariesLengths[0];
    }

    public static int countNumberOfCharactersNeededToRepresentLong( long v ) {
        if ( v >= 0 ) {
            for ( int i=19; i<MAX_NUMBER_BOUNDARIES.length; i++ ) {
                if ( v <= MAX_NUMBER_BOUNDARIES[i] ) {
                    return NUM_DIGITS_FOR_NUMBER_BOUNDARIES[i];
                }
            }
        } else {
            for ( int i=17; i>=0; i-- ) {
                if ( v > MAX_NUMBER_BOUNDARIES[i] ) {
                    return NUM_DIGITS_FOR_NUMBER_BOUNDARIES[i+1];
                }
            }

            return NUM_DIGITS_FOR_NUMBER_BOUNDARIES[0];
        }

        throw new IllegalStateException( "MAX_NUMBER_BOUNDARIES is too small for " + v );
    }

    public static String base64( String s ) {
        return Base64.getEncoder().encodeToString( s.getBytes() );
    }

    public static FPIterator<String> splitCamelCase( String s ) {
        return new FPIterator<String>() {
            private PullParser p = new PullParser( s, s );

            @Override
            protected boolean _hasNext() {
                return p.hasMore();
            }

            @Override
            protected String _next() {
                return p.pullMandatory(CamelCaseFragment.INSTANCE).toString();
            }
        };
    }

    /**
     * Returns A iff A is not blank else B.
     */
    public static String nvl( String a, String b ) {
        return isBlank(a) ? b : a;
    }

//    public static boolean toBoolean( String v, boolean defaultValue ) {
//        return BooleanCodec.BOOLEAN_PRIMITIVE_CODEC.decode( v, defaultValue );
//    }

    public static String firstLine( String s ) {
        if ( s == null ) {
            return null;
        }

        int i = s.indexOf('\n');

        return i >= 0 ? s.substring(0,i) : s;
    }


    private static class CamelCaseFragment implements CharacterMatcher {
        public static final CharacterMatcher INSTANCE = new CamelCaseFragment();

        @Override
        public int matchCount( CharacterIterator it ) {
            if ( isAcronym(it) ) {
                return consumeAcronym(it);
            } else {
                return consumeUpToNextUpperCase(it);
            }
        }

        private boolean isAcronym( CharacterIterator it ) {
            return it.peek( () -> {
                char a = it.next();

                if ( !it.hasNext() ) {
                    return false;
                }

                char b = it.next();

                return Character.isUpperCase(a) && Character.isUpperCase(b);
            } );
        }

        private int consumeAcronym( CharacterIterator it ) {
            int count = 0;

            while ( it.hasNext() ) {
                char c = it.next();

                if ( Character.isUpperCase(c) && (!it.hasNext() || Character.isUpperCase(it.peek())) ) {
                    count++;
                } else {
                    return count;
                }
            }

            return count;
        }

        private int consumeUpToNextUpperCase( CharacterIterator it ) {
            it.next();

            return 1 + it.countWhile( c -> Character.isLowerCase(c) || Character.isDigit(c) );
        }
    }
}
