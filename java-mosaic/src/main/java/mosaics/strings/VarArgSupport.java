package mosaics.strings;


import mosaics.fp.FP;
import mosaics.fp.collections.FPList;
import mosaics.fp.collections.FPOption;
import mosaics.fp.collections.FPSet;
import mosaics.fp.collections.RRBVector;
import mosaics.lang.functions.Function1;
import mosaics.lang.functions.Function1WithThrows;
import mosaics.lang.reflection.JavaClass;
import mosaics.lang.reflection.JavaMethod;
import mosaics.lang.reflection.JavaParameter;


/**
 * Internal support for StringifiedJavaMethod.
 */
class VarArgSupport {

    public static void validateVarArgParameters( JavaMethod m ) {
        JavaParameter[] parameters = m.getParameters().toArray( JavaParameter.class );

        for ( int i=0; i<parameters.length-1; i++ ) {
            JavaParameter p = parameters[i];

            if ( isVarArgType(p.getType()) ) {
                throw new IllegalArgumentException( "var arg parameter must appear at the end of the method" );
            }
        }
    }

    public static FPOption<VarArgHandler> createVarArgHandlerFor( Function1<JavaClass, FPOption<StringCodec>> codecFactory, JavaMethod originalMethod ) {
        return originalMethod.getParameters()
            .last()
            .flatMap( p -> selectVarArgHandlerFor(codecFactory,p.getType()) );
    }

    public static StringCodec[] selectStringCodecsForNonVarArgParameters( Function1<JavaClass, FPOption<StringCodec>> codecFactory, JavaMethod originalMethod ) {
        return originalMethod.getParameters()
            .filterNot(t -> isVarArgType(t.getType()))
            .map(t -> toStringCodec(codecFactory, t))
            .toArray(StringCodec.class);
    }

    public static boolean isVarArgType( JavaClass type ) {
        return type.isArray() || type.isJdkList() || type.isJdkSet() || type.isInstanceOf(FPList.class)
            || type.isInstanceOf(FPSet.class) || type.isInstanceOf(RRBVector.class);
    }

    private static StringCodec toStringCodec( Function1<JavaClass, FPOption<StringCodec>> codecFactory, JavaParameter parameterMeta ) {
        JavaClass             type        = parameterMeta.getType();
        FPOption<StringCodec> codecOption = codecFactory.invoke(type);

        return codecOption.orElseThrow( () -> new CodecNotFoundException(type.getFullName()) );
    }

    public static FPOption<VarArgHandler> selectVarArgHandlerFor( StringCodecs codecs, JavaClass type ) {
        return selectVarArgHandlerFor( codecs::getCodecFor, type );
    }

    public static FPOption<VarArgHandler> selectVarArgHandlerFor( Function1<JavaClass, FPOption<StringCodec>> codecFactory, JavaClass type ) {
        if ( type.isArray() ) {
            JavaClass componentTypeMeta = type.getArrayComponentType().get();
            Class     componentType     = componentTypeMeta.getJdkClass();

            return codecFactory.invoke(componentTypeMeta).map( codec -> new ArrayVarArgHandler(componentType, codec) );
        } else if ( type.isJdkList() ) {
            return createVarArgHandlerFor( codecFactory, type, ListVarArgHandler::new );
        } else if ( type.isInstanceOf( FPList.class) ) {
            return createVarArgHandlerFor( codecFactory, type, FPListVarArgHandler::new );
        } else if ( type.isJdkSet() ) {
            return createVarArgHandlerFor( codecFactory, type, SetVarArgHandler::new );
        } else if ( type.isInstanceOf( FPSet.class) ) {
            return createVarArgHandlerFor( codecFactory, type, FPSetVarArgHandler::new );
        } else if ( type.isInstanceOf( RRBVector.class) ) {
            return createVarArgHandlerFor( codecFactory, type, RRBVectorVarArgHandler::new );
        } else {
            return FP.emptyOption();
        }
    }

    private static FPOption<VarArgHandler> createVarArgHandlerFor( Function1<JavaClass, FPOption<StringCodec>> codecFactory, JavaClass type, Function1WithThrows<StringCodec,VarArgHandler> factory ) {
        if ( !StringCodecs.isClassWithSingleReifiedClassTypeParameter(type) ) {
            return FP.emptyOption();
        }

        JavaClass elementType = type.getClassGenerics().firstOrNull();

        return codecFactory.invoke( elementType ).map( factory );
    }
}
