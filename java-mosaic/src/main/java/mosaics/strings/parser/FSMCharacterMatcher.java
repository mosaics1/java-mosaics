package mosaics.strings.parser;

import mosaics.lang.functions.FunctionC;


/**
 * A character matcher that is built wrapping a Finite State Machine.  It can be used to implement
 * RegExp style automata.   Each node of the state machine is represented by a private method
 * of the form char -> FSMStep.  The method takes a character and returns a transition to the
 * next state.
 */
abstract class FSMCharacterMatcher implements CharacterMatcher {
    protected static final FSMStep NO_MATCH     = new NoMatch();
    protected static final FSMStep FAILED_MATCH = new HasFailed();


    private final FSMStep initialState;

    protected FSMCharacterMatcher( FunctionC<FSMStep> initialStateHandler ) {
        this.initialState = new NotMatchedYet(initialStateHandler);
    }

    public int matchCount( CharacterIterator it ) {
        return it.pull( () -> {
            FSMStep nextState = initialState;
            FSMStep prevState;


            int count = 0;

            while ( it.hasNext() && !nextState.endOfMatch() ) {
                char c = it.peek();

                prevState = nextState;
                nextState = nextState.invokeNext(c);

                if ( nextState.hasFailed() ) {
                    return 0;
                } else if ( nextState.endOfMatch() ) {
                    return prevState.isCandidateMatch() ? count : 0;
                }

                count++;
                it.next();
            }

            return nextState.isCandidateMatch() ? count : 0;
        });
    }



    public static interface FSMStep {
        public boolean hasFailed();
        public boolean endOfMatch();
        public boolean isCandidateMatch();

        public FSMStep invokeNext( char c );
    }


    public static class HasFailed implements FSMStep {
        public boolean hasFailed() {
            return true;
        }

        public boolean endOfMatch() {
            return true;
        }

        public boolean isCandidateMatch() {
            return false;
        }

        public FSMStep invokeNext( char c ) {
            throw new IllegalStateException();
        }
    }

    public static class NoMatch implements FSMStep {
        public boolean hasFailed() {
            return false;
        }

        public boolean endOfMatch() {
            return true;
        }

        public boolean isCandidateMatch() {
            return false;
        }

        public FSMStep invokeNext( char c ) {
            throw new IllegalStateException();
        }
    }

    public static class NotMatchedYet implements FSMStep {
        private FunctionC<FSMStep> nextStepHandler;

        public NotMatchedYet( FunctionC<FSMStep> nextStepHandler ) {
            this.nextStepHandler = nextStepHandler;
        }

        public boolean hasFailed() {
            return false;
        }

        public boolean endOfMatch() {
            return false;
        }

        public boolean isCandidateMatch() {
            return false;
        }

        public FSMStep invokeNext( char c ) {
            return nextStepHandler.invoke( c );
        }
    }

    public static class CandidateMatch implements FSMStep {
        private FunctionC<FSMStep> nextStepHandler;

        public CandidateMatch( FunctionC<FSMStep> nextStepHandler ) {
            this.nextStepHandler = nextStepHandler;
        }

        public boolean hasFailed() {
            return false;
        }

        public boolean endOfMatch() {
            return false;
        }

        public boolean isCandidateMatch() {
            return true;
        }

        public FSMStep invokeNext( char c ) {
            return nextStepHandler.invoke( c );
        }
    }
}

