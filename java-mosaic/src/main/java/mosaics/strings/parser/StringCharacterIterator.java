package mosaics.strings.parser;

import mosaics.lang.QA;

import java.util.Stack;


/**
 * Converts a java.lang.CharSequence into a CharacterIterator.
 */
public class StringCharacterIterator implements CharacterIterator {

    private final CharSequence   text;
    private       int            pos;
    private final Stack<Integer> markedPositions = new Stack<>();


    public StringCharacterIterator( CharSequence text ) {
        QA.argNotNull( text, "text" );

        this.text = text;
    }

    public boolean hasNext() {
        return pos < text.length();
    }

    public boolean hasNext( int numChars ) {
        return pos+numChars <= text.length();
    }

    public char next() {
        try {
            return text.charAt( pos++ );
        } catch ( StringIndexOutOfBoundsException ex ) {
            throw new IllegalStateException( "next() called at EOF", ex );
        }
    }

    public char peek() {
        try {
            return text.charAt( pos );
        } catch ( StringIndexOutOfBoundsException ex ) {
            throw new IllegalStateException( "peek() called at EOF", ex );
        }
    }

    public String pull( int numChars ) {
        QA.argIsGTEZero( numChars, "numChars" );

        try {
            String substr = text.subSequence( pos, pos + numChars ).toString();

            pos += numChars;

            return substr;
        } catch ( StringIndexOutOfBoundsException ex ) {
            throw new IllegalStateException( "pull("+numChars+") crosses EOF", ex );
        }
    }

    public void skip( int numChars ) {
        QA.argIsGTEZero( numChars, "numChars" );

        if ( numChars == 0 ) {
            return;
        } else if ( pos+numChars > text.length() ) {
            throw new IllegalStateException( "skip("+numChars+") crosses EOF" );
        }

        pos += numChars;
    }

    public void markPosition() {
        markedPositions.push( pos );
    }

    public void discardLastMark() {
        markedPositions.pop();
    }

    public void returnToLastMark() {
        this.pos = markedPositions.pop();
    }

    public int markedPositionCount() {
        return markedPositions.size();
    }

    public String pullRemaining() {
        String remainingText = text.subSequence( pos, text.length() ).toString();

        pos = text.length();

        return remainingText;
    }

    public String pullUpTo( int maxNumChars ) {
        String matchedText = text.subSequence( pos, Math.min(text.length(),pos+maxNumChars) ).toString();

        pos += matchedText.length();

        return matchedText;
    }

    public String toString() {
        return text.subSequence( pos, text.length() ).toString();
    }

}
