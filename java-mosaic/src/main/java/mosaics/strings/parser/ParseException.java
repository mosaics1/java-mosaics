package mosaics.strings.parser;

import mosaics.lang.QA;


public class ParseException extends RuntimeException {

    private final CharacterPosition pos;


    public ParseException( CharacterPosition pos, String msg ) {
        super( pos + ": " + msg );

        QA.argNotNull(pos, "pos");

        this.pos          = pos;
    }

    public CharacterPosition getPosition() {
        return pos;
    }

}
