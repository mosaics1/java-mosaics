package mosaics.strings.parser;

import mosaics.fp.collections.ConsList;
import mosaics.fp.collections.FPOption;
import mosaics.fp.collections.LazyLinkedList;
import mosaics.lang.Backdoor;
import mosaics.lang.QA;
import mosaics.lang.TryUtils;
import mosaics.lang.functions.Function0;

import java.io.Reader;
import java.util.Stack;


/**
 * Converts a java.io.Reader into a CharacterIterator.
 */
public class ReaderCharacterIterator implements CharacterIterator {

    // kept reasonably small to ensure fit within the TLAB
    private static final int DEFAULT_FRAME_SIZE = 256;

    private final Stack<RovingReader> markedPositionsStack = new Stack<>();

    private RovingReader rovingReader;



    public ReaderCharacterIterator( Reader in ) {
        this( in, DEFAULT_FRAME_SIZE );
    }

    public ReaderCharacterIterator( Reader in, int bufSize ) {
        this.rovingReader = new RovingReader(in, bufSize);
    }


    public boolean hasNext() {
        return rovingReader.hasNext();
    }

    public boolean hasNext( int numChars ) {
        return rovingReader.canRead( numChars );
    }

    public char next() {
        if ( !hasNext() ) {
            throw new IllegalStateException( "next() called at EOF" );
        }

        return rovingReader.next();
    }

    public char peek() {
        if ( !hasNext() ) {
            throw new IllegalStateException( "peek() called at EOF" );
        }

        return rovingReader.peek();
    }

    public String pull( int numChars ) {
        QA.argIsGTEZero( numChars, "numChars" );

        if ( numChars == 0 ) {
            return "";
        } else if ( !rovingReader.canRead(numChars) ) {
            throw new IllegalStateException( "pull("+numChars+") crosses EOF" );
        }

        StringBuilder buf = new StringBuilder(numChars);
        rovingReader.pull( buf, numChars );

        return buf.toString();
    }

    public void skip( int numChars ) {
        QA.argIsGTEZero( numChars, "numChars" );

        if ( numChars == 0 ) {
            return;
        } else if ( !rovingReader.canRead(numChars) ) {
            throw new IllegalStateException( "skip("+numChars+") crosses EOF" );
        }

        rovingReader.advance( numChars );
    }

    public void markPosition() {
        markedPositionsStack.push( rovingReader.clone() );
    }

    public void discardLastMark() {
        markedPositionsStack.pop();
    }

    public void returnToLastMark() {
        this.rovingReader = markedPositionsStack.pop();
    }

    public int markedPositionCount() {
        return markedPositionsStack.size();
    }



    public String pullRemaining() {
        StringBuilder buf = new StringBuilder();

        while ( this.hasNext() ) {
            buf.append( this.next() );
        }

        return buf.toString();
    }

    public String pullUpTo( int maxNumChars ) {
        StringBuilder buf = new StringBuilder();

        int count = 0;
        while ( this.hasNext() && count < maxNumChars ) {
            buf.append( this.next() );
            count++;
        }

        return buf.toString();
    }

    public String toString() {
        markPosition();

        try {
            return pullRemaining();
        } finally {
            returnToLastMark();
        }
    }

    private static class RovingReader implements Cloneable {
        private ConsList<CharacterChunk> characterChunks;
        private long                     currentIndex;


        public RovingReader( Reader in, int bufSize ) {
            this.characterChunks = new LazyLinkedList<>( new Function0<FPOption<CharacterChunk>>() {
                private long nextChunkStartsAt = 0L;

                public FPOption<CharacterChunk> invoke() {
                    char[] buf          = new char[bufSize];
                    int    numCharsRead = TryUtils.invokeAndRethrowException( () -> in.read(buf) );

                    if ( numCharsRead <= 0 ) {
                        return FPOption.none();
                    } else {
                        CharacterChunk chunk = new CharacterChunk( buf, numCharsRead, nextChunkStartsAt );
                        nextChunkStartsAt += numCharsRead;

                        return FPOption.of( chunk );
                    }
                }
            } );
        }

        public boolean hasNext() {
            return !characterChunks.isEOL() && characterChunks.getHead().hasCharAt( currentIndex );
        }

        public char next() {
            char c = characterChunks.getHead().charAt( currentIndex );

            advance(1);

            return c;
        }

        public char peek() {
            return characterChunks.getHead().charAt( currentIndex );
        }

        public boolean canRead( int numChars ) {
            return !scanForwardTo( currentIndex+numChars-1 ).isEOL();
        }



        public void pull( StringBuilder buf, int numChars ) {
            long maxInc  = currentIndex + numChars;

            while ( !characterChunks.isEOL() && currentIndex < maxInc ) {
                int numCharsRead = characterChunks.getHead().readInto( buf, currentIndex, maxInc-currentIndex );

                advance( numCharsRead );
            }
        }

        public RovingReader clone()  {
            try {
                return (RovingReader) super.clone();
            } catch ( CloneNotSupportedException ex ) {
                throw Backdoor.throwException( ex );
            }
        }

        private void advance( int numChars ) {
            currentIndex += numChars;

            characterChunks = scanForwardTo( currentIndex );
        }

        private ConsList<CharacterChunk> scanForwardTo( long targetPosition ) {
            return characterChunks.skipForwardUntil( chunk -> chunk.hasCharAt(targetPosition) );
        }
    }

    private static class CharacterChunk {
        private final char[] buf;
        private final int    chunkLength;
        private final long   chunkOffset;

        private CharacterChunk( char[] buf, int chunkLength, long initialOffset ) {
            this.buf         = buf;
            this.chunkLength = chunkLength;
            this.chunkOffset = initialOffset;
        }

        public boolean hasCharAt( long globalOffset ) {
            return globalOffset >= this.chunkOffset && globalOffset < this.chunkOffset+chunkLength;
        }

        public char charAt( long globalOffset ) {
            long off = globalOffset-chunkOffset;

            return buf[Backdoor.toInt(off)];
        }

        public boolean isEmpty() {
            return chunkLength <= 0;
        }

        /**
         *
         * @return  num chars read
         */
        public int readInto( StringBuilder buf, long fromInc, long numChars ) {
            int f = Backdoor.toInt(  fromInc - chunkOffset );
            int e = Backdoor.toInt( f + numChars );
            int max = Math.min(e, chunkLength);
            int count = max - f;

            buf.append( this.buf, f, count );

            return count;
        }
    }

}
