package mosaics.strings.parser;

import mosaics.fp.collections.FPOption;
import mosaics.lang.ArrayUtils;
import mosaics.lang.Backdoor;
import mosaics.lang.functions.BooleanFunction0;
import mosaics.lang.functions.BooleanFunctionC;
import mosaics.lang.functions.Function0;
import mosaics.lang.functions.IntFunction0;


/**
 * An incremental reader for a stream of characters.
 */
public interface CharacterIterator {

    public static CharacterIterator stringIterator( CharSequence txt ) {
        return new StringCharacterIterator(txt);
    }

    public static CharacterIterator linesIterator( CharSequence...txt ) {
        return new StringCharacterIterator( ArrayUtils.toString(txt,Backdoor.NEWLINE) );
    }


    public boolean hasNext();
    public boolean hasNext( int numChars );
    public char next();
    public char peek();

    public default boolean isEmpty() {
        return !hasNext();
    }

    public String pull( int numChars );
    public void skip( int numChars );

    public void markPosition();
    public void discardLastMark();
    public void returnToLastMark();

    public int markedPositionCount();

    /**
     * Consume and return the rest of the remaining characters.
     */
    public String pullRemaining();

    /**
     *
     * @return returns the remaining characters up to the max specified length
     */
    public String pullUpTo( int maxNumChars );


    public default int pull( IntFunction0 f ) {
        markPosition();

        int count = f.invoke();

        if ( count == 0 ) {
            returnToLastMark();
        } else {
            discardLastMark();
        }

        return count;
    }

    public default int peek( IntFunction0 f ) {
        markPosition();

        try {
            return f.invoke();
        } finally {
            returnToLastMark();
        }
    }

    public default <T> T peek( Function0<T> f ) {
        markPosition();

        try {
            return f.invoke();
        } finally {
            returnToLastMark();
        }
    }

    public default int countWhile( BooleanFunctionC predicate ) {
        return this.pull( () -> {
            int count = 0;
            while ( this.hasNext() ) {
                char c = this.peek();

                if ( predicate.invoke(c) ) {
                    count++;
                    this.next();
                } else {
                    return count;
                }
            }

            return count;
        } );
    }

    public default int countWhile( BooleanFunction0 predicate ) {
        return this.pull( () -> {
            int count = 0;
            while ( this.hasNext() ) {
                if ( predicate.invoke() ) {
                    count++;

                    next();
                } else {
                    return count;
                }
            }

            return count;
        } );
    }

    public default FPOption<String> pullWhile( BooleanFunctionC predicate ) {
        StringBuilder buf = new StringBuilder();

        while ( this.hasNext() ) {
            char c = this.peek();

            if ( predicate.invoke(c) ) {
                buf.append(c);
                this.next();
            } else {
                break;
            }
        }

        return buf.length() == 0 ? FPOption.none() : FPOption.of(buf.toString());
    }

    public default int pullCount( BooleanFunctionC predicate ) {
        int count = 0;

        while ( this.hasNext() ) {
            char c = this.peek();

            if ( predicate.invoke(c) ) {
                this.next();

                count += 1;
            } else {
                break;
            }
        }

        return count;
    }

    public default int conditionalNext( char targetChar ) {
        if ( this.hasNext() && this.peek() == targetChar ) {
            this.next();

            return 1;
        } else {
            return 0;
        }

    }
}
