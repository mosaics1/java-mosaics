package mosaics.strings.parser;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;


/**
 *
 */
public class CharacterIterators {

    public static CharacterIterator streamIterator( InputStream in, String charset ) throws UnsupportedEncodingException {
        return readerIterator( new InputStreamReader(in, charset) );
    }

    public static CharacterIterator streamIterator( InputStream in, Charset cs ) {
        return readerIterator( new InputStreamReader(in, cs) );
    }

    public static CharacterIterator readerIterator( Reader in ) {
        return new ReaderCharacterIterator( in );
    }

    public static CharacterIterator stringIterator( CharSequence in ) {
        return new StringCharacterIterator( in );
    }

}
