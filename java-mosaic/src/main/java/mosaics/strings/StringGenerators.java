package mosaics.strings;

import mosaics.fp.collections.FPIterator;


/**
 *
 */
public class StringGenerators {

    public static FPIterator<String> incrementingId( String prefix ) {
        return new FPIterator<String>() {
            private long nextId = 1;

            protected boolean _hasNext() {
                return true;
            }

            protected String _next() {
                return prefix + (nextId++);
            }
        };
    }

}
