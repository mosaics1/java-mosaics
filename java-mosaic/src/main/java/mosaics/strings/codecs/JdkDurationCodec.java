package mosaics.strings.codecs;

import mosaics.fp.Tryable;
import mosaics.lang.reflection.JavaClass;
import mosaics.strings.StringCodec;
import lombok.Value;

import java.time.Duration;


@Value
public class JdkDurationCodec implements StringCodec<Duration> {
    public static final StringCodec<Duration> INSTANCE = new JdkDurationCodec();

    public boolean supportsNull() {
        return DurationLongCodec.INSTANCE.supportsNull();
    }

    public String encode( Duration v ) {
        return v == null ? "" : DurationLongCodec.INSTANCE.encode(v.toMillis());
    }

    public Tryable<Duration> decode( String v ) {
        return DurationLongCodec.INSTANCE.decode(v).mapResult(Duration::ofMillis);
    }

    public JavaClass getType() {
        return JavaClass.DURATION;
    }

    public String toString() {
        return "JdkDurationCodec";
    }
}
