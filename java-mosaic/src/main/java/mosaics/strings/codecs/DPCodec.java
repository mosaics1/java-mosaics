package mosaics.strings.codecs;

import mosaics.fp.Try;
import mosaics.fp.Tryable;
import mosaics.lang.reflection.JavaClass;
import mosaics.strings.StringCodec;
import mosaics.strings.parser.CharacterMatcher;
import mosaics.strings.parser.CharacterMatchers;
import mosaics.strings.parser.PullParser;
import lombok.Value;


@Value
public class DPCodec implements StringCodec<Long> {

    public static final StringCodec<Long> DP1_CODEC = new DPCodec(1);
    public static final StringCodec<Long> DP3_CODEC = new DPCodec(3);


    private static final CharacterMatcher MAJOR   = CharacterMatchers.integer(true);
    private static final CharacterMatcher MINOR   = CharacterMatchers.digits();
    private static final CharacterMatcher DECIMAL = CharacterMatchers.constant('.');

    private final long multiplier;
    private final int  numDP;


    public DPCodec( int numDP ) {
        this.multiplier = (long) Math.pow(10, numDP);
        this.numDP      = numDP;
    }

    public String encode( Long v ) {
        if ( v == null ) {
            return "null";
        }

        StringBuilder buf = new StringBuilder();

        if ( v < 0 ) {
            buf.append('-');
        }

        buf.append( Long.toString(Math.abs(v)/multiplier) );
        buf.append( '.' );

        String minorStr = Long.toString(Math.abs(v % multiplier));
        int i = minorStr.length();
        while ( i < numDP ) {
            buf.append('0');
            i++;
        }

        buf.append(minorStr);

        return buf.toString();
    }

    public Tryable<Long> decode( String v ) {
        PullParser p = new PullParser(v, v);

        String majorStr = p.pullMandatory(MAJOR).toString();


        long minor = 0;
        if ( p.hasMore() && p.peek(DECIMAL) ) {
            p.skip(DECIMAL);

            String minorStr = p.pullMandatory(MINOR).toString();
            int minorStrLength = minorStr.length();

            if ( minorStrLength == 0 ) {

            } else if ( minorStrLength <= numDP ) {
                minor = Long.parseLong(minorStr);

                int i = minorStr.length();
                while ( i < numDP ) {
                    minor *= 10;
                    i++;
                }

            } else if ( minorStrLength == numDP+1 ) {
                minor = (Long.parseLong(minorStr) + 5)/10;
            } else {
                minorStr = minorStr.substring(0,numDP+1);
                minor    = (Long.parseLong(minorStr) + 5)/10;
            }
        }

        if ( p.hasMore() ) {
            return Try.failed(new NumberFormatException("For input string: \"" + v + "\""));
        }

        long sign   = majorStr.startsWith("-") ? -1 : 1;
        long major  = Long.parseLong(majorStr);  // nb major could be negative here; which is why the sign is only applied to minor below

        return Try.succeeded(major * multiplier + minor * sign);
    }

    public JavaClass getType() {
        return JavaClass.LONG_PRIMITIVE;
    }

    public String toString() {
        return numDP+"DPCodec";
    }

    @Override
    public boolean supportsNull() {
        return false;
    }
}
