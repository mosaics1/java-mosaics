package mosaics.strings.codecs;

import lombok.Value;
import mosaics.fp.Tryable;
import mosaics.lang.Backdoor;
import mosaics.lang.reflection.JavaClass;
import mosaics.lang.time.DTMUtils;
import mosaics.lang.time.TZ;
import mosaics.strings.StringCodec;

import java.text.SimpleDateFormat;
import java.util.Date;


@Value
public class DTMCodecLong implements StringCodec<Long> {

    public static final StringCodec<Long> DEFAULT_CODEC     = new DTMCodecLong( DTMUtils.DEFAULT_FORMATTER );
    public static final StringCodec<Long> DTM_ISO8601_CODEC = new DTMCodecLong( DTMUtils.DTM_ISO8601_FORMATTER );


    private final SimpleDateFormat dtmFormat;


    public DTMCodecLong( String dtmFormat ) {
        this( dtmFormat, TZ.UTC );
    }

    public DTMCodecLong( String dtmFormat, TZ tz ) {
        this( DTMUtils.createFormatter(dtmFormat, tz) );
    }

    public DTMCodecLong( SimpleDateFormat dtmFormat ) {
        this.dtmFormat = dtmFormat;
    }


    public boolean supportsNull() {
        return false;
    }

    public String encode( Long v ) {
        return Backdoor.invokeSynchronized( dtmFormat, () -> v == null ? "null" : dtmFormat.format(new Date(v)) );
    }

    public Tryable<Long> decode( String v ) {
        return Backdoor.invokeSynchronizedTry( dtmFormat, () -> dtmFormat.parse(v).getTime() );
    }

    public JavaClass getType() {
        return JavaClass.LONG_PRIMITIVE;
    }

    public String toString() {
        return "DTMCodecLong("+dtmFormat.toPattern()+")";
    }
}
