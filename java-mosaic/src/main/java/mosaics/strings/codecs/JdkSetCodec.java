package mosaics.strings.codecs;

import lombok.Value;
import mosaics.fp.FP;
import mosaics.fp.Tryable;
import mosaics.lang.reflection.JavaClass;
import mosaics.strings.StringCodec;
import mosaics.utils.ComparatorUtils;

import java.util.Set;


@Value
public class JdkSetCodec<T> implements StringCodec<Set<T>> {
    private JavaClass      type;
    private StringCodec<T> elementCodec;

    public String encode( Set<T> obj ) {
        if ( obj == null ) {
            return "";
        }

        StringBuilder buf = new StringBuilder();

        buf.append( '[' );

        String separator = "";
        for ( T v : FP.wrap(obj).sort(ComparatorUtils.comparing(v -> v)) ) {
            buf.append( separator );
            buf.append( elementCodec.encode(v) );

            separator = ",";
        }

        buf.append( ']' );

        return buf.toString();
    }

    public Tryable<Set<T>> decode( String text ) {
        //todo
        return null;
    }

    public boolean supportsNull() {
        return true;
    }

    public JavaClass getType() {
        return type;
    }
}
