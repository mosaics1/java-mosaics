package mosaics.strings.codecs;

import lombok.AllArgsConstructor;
import lombok.Value;
import mosaics.fp.FP;
import mosaics.fp.Try;
import mosaics.fp.Tryable;
import mosaics.fp.collections.FPOption;
import mosaics.lang.reflection.JavaClass;
import mosaics.strings.StringCodec;


@Value
@AllArgsConstructor
public class FPOptionCodec<T> implements StringCodec<FPOption<T>> {

    private StringCodec<T> innerCodec;
    private JavaClass      type;


    public FPOptionCodec( StringCodec<T> innerCodec ) {
        this( innerCodec, innerCodec.getType() );
    }


    public boolean supportsNull() {
        return true;
    }

    public String encode( FPOption<T> obj ) {
        if ( obj == null || obj.isEmpty() ) {
            return null;
        } else {
            return innerCodec.encode( obj.get() );
        }
    }

    public Tryable<FPOption<T>> decode( String text ) {
        if ( text == null || text.trim().length() == 0 ) {
            return Try.succeeded( FP.emptyOption() );
        } else {
            return innerCodec.decode(text).mapResult( FP::option );
        }
    }

    public JavaClass getType() {
        return type;
    }

}
