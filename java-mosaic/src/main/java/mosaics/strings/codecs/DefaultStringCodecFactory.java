package mosaics.strings.codecs;

import mosaics.fp.FP;
import mosaics.fp.Try;
import mosaics.fp.Tryable;
import mosaics.fp.collections.FPOption;
import mosaics.lang.reflection.JavaClass;
import mosaics.lang.reflection.JavaConstructor;
import mosaics.lang.reflection.JavaMethod;
import mosaics.strings.StringCodec;

import static mosaics.lang.Backdoor.cast;


/**
 * Creates a StringCodec from a classes single String arg constructor and no-arg toString methods.
 */
public class DefaultStringCodecFactory {

    @SuppressWarnings( "unchecked" )
    public static <T> StringCodec<T> createCodecFor( Class<T> c ) {
        if ( c.isEnum() ) {
            return createEnumCodecFor( (Class) c );
        } else {
            return createConstructorCodec(c);
        }
    }

    public static <T> StringCodec<T> createCodecFor( Class<T> c, T defaultValue ) {
        return createCodecFor(JavaClass.of(c), JavaClass.of(c), defaultValue);
    }

    public static <T> StringCodec<T> createCodecFor( JavaClass factoryClass, JavaClass type, T defaultValue ) {
            return createConstructorCodec( factoryClass, type, defaultValue );
    }


    private static <T extends Enum<T>> StringCodec<T> createEnumCodecFor( Class<T> c ) {
        return new EnumCodec<>(c);
    }

    private static <T> StringCodec<T> createConstructorCodec( Class<T> c ) {
        return createConstructorCodec( JavaClass.of(c) );
    }

    private static <T> StringCodec<T> createConstructorCodec( JavaClass c ) {
        return createConstructorCodec( c, c, FP.emptyOption() );
    }

    private static <T> StringCodec<T> createConstructorCodec( JavaClass factoryClass, JavaClass type, T defaultValue ) {
        return createConstructorCodec( factoryClass, type, FP.option(defaultValue) );
    }

    /**
     *
     * @param factoryClass  the class to scan for the constructor.. usually the same as type, but not when
     *                      dealing with primitives
     * @param type
     * @param defaultValue
     * @param <T>
     * @return
     */
    private static <T> StringCodec<T> createConstructorCodec( JavaClass factoryClass, JavaClass type, FPOption<T> defaultValue ) {
        JavaConstructor constructor    = factoryClass.getMandatoryPublicConstructor(String.class);
        JavaMethod      toStringMethod = factoryClass.findPublicMethod( String.class, "toString" );

        return new StringCodec<T>() {
            public boolean supportsNull() {
                return false;
            }

            public String encode( T v ) {
                if ( v == null ) {
                    return null;
                }

                Object o = toStringMethod.invokeAgainst( v );

                return (String) o;
            }

            public Tryable<T> decode( String v ) {
                if ( v == null && defaultValue.hasValue()) {
                    return Try.succeeded( defaultValue.get() );
                }

                return Try.invoke( () -> cast(constructor.newInstance(v)) );
            }

            public JavaClass getType() {
                return type;
            }

            public String toString() {
                return type.getShortName() + "Codec";
            }
        };
    }
}
