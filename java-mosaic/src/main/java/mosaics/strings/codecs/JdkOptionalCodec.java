package mosaics.strings.codecs;

import lombok.EqualsAndHashCode;
import mosaics.fp.Try;
import mosaics.fp.Tryable;
import mosaics.lang.reflection.JavaClass;
import mosaics.strings.StringCodec;

import java.util.Optional;


@EqualsAndHashCode
public class JdkOptionalCodec<T> implements StringCodec<Optional<T>> {

    private StringCodec<T> innerCodec;
    private JavaClass      type;


    public JdkOptionalCodec( StringCodec<T> innerCodec ) {
        this( innerCodec.getType(), innerCodec );
    }

    public JdkOptionalCodec( JavaClass type, StringCodec<T> innerCodec ) {
        this.innerCodec = innerCodec;
        this.type       = type;
    }

    public boolean supportsNull() {
        return true;
    }

    public String encode( Optional<T> obj ) {
        if ( obj == null || !obj.isPresent() ) {
            return null;
        } else {
            return innerCodec.encode( obj.get() );
        }
    }

    public Tryable<Optional<T>> decode( String text ) {
        if ( text == null || text.trim().length() == 0 ) {
            return Try.succeeded( Optional.empty() );
        } else {
            return innerCodec.decode(text).mapResult( Optional::ofNullable );
        }
    }

    public JavaClass getType() {
        return type;
    }

    public String toString() {
        return "JdkOptionalCodec("+innerCodec+")";
    }

}

