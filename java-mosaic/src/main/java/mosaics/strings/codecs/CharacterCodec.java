package mosaics.strings.codecs;

import lombok.Value;
import mosaics.fp.Try;
import mosaics.fp.Tryable;
import mosaics.lang.reflection.JavaClass;
import mosaics.strings.StringCodec;


@Value
public class CharacterCodec implements StringCodec<Character> {
    public static final StringCodec<Character> CHARACTER_PRIMITIVE_CODEC = new CharacterCodec(JavaClass.CHARACTER_PRIMITIVE);
    public static final StringCodec<Character> CHARACTER_OBJECT_CODEC = new CharacterCodec(JavaClass.CHARACTER_OBJECT);

    private final JavaClass type;

    private CharacterCodec( JavaClass type ) {
        this.type = type;
    }


    public boolean supportsNull() {
        return false;
    }

    public String encode( Character v ) {
        return v == null ? "" : v.toString();
    }

    public Tryable<Character> decode( String v ) {
        if ( v == null || v.length() > 1 ) {
            return Try.failed( new IllegalArgumentException("Invalid character: " + v) );
        }

        return Try.succeeded( v.charAt(0) );
    }

    public JavaClass getType() {
        return type;
    }

    public String toString() {
        return "CharacterCodec";
    }
}
