package mosaics.strings.codecs;

import lombok.Value;
import mosaics.fp.Tryable;
import mosaics.lang.reflection.JavaClass;
import mosaics.strings.StringCodec;
import mosaics.strings.StringCodecs;


/**
 * Look up a new StringCodec every time.
 */
@Value
@SuppressWarnings({"rawtypes", "unchecked"})
public class DynamicStringCodec implements StringCodec {
    private JavaClass    type;
    private StringCodecs stringCodecs;


    public String encode( Object obj ) {
        StringCodec codec = stringCodecs.getCodecFor( obj.getClass() ).get();

        return codec.encode( obj );
    }

    public Tryable decode( String text ) {
        throw new UnsupportedOperationException();
    }

    public boolean supportsNull() {
        return true;
    }
}
