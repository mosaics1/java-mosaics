package mosaics.strings.codecs;

import mosaics.fp.Try;
import mosaics.fp.Tryable;
import mosaics.lang.reflection.JavaClass;
import mosaics.strings.StringCodec;
import lombok.Value;


@Value
public class MillisCodec implements StringCodec<Long> {
    public static final StringCodec<Long> INSTANCE = new MillisCodec();


    public boolean supportsNull() {
        return false;
    }

    public String encode( Long v ) {
        if ( v == null ) {
            return "";
        }

        StringBuilder buf = new StringBuilder();
        buf.append(v.longValue());
        buf.append(" milli");

        if ( v != 1 && v != -1 ) {
            buf.append('s');
        }

        return buf.toString();
    }

    public Tryable<Long> decode( String v ) {
        if ( v == null || !(v.endsWith(" milli") || v.endsWith(" millis")) ) {
            return Try.failed( new NumberFormatException("For input string: \"" + v + "\"") );
        }

        int i = v.indexOf(' '); // existence of ' ' already validated above
        String numStr = v.substring(0, i);

        return Try.invoke(() -> Long.parseLong(numStr));
    }

    public JavaClass getType() {
        return JavaClass.LONG_PRIMITIVE;
    }

    public String toString() {
        return "MillisCodec";
    }
}
