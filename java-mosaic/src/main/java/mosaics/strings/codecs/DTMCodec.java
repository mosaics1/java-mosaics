package mosaics.strings.codecs;

import lombok.Value;
import mosaics.fp.Tryable;
import mosaics.lang.Backdoor;
import mosaics.lang.reflection.JavaClass;
import mosaics.lang.time.DTM;
import mosaics.lang.time.DTMUtils;
import mosaics.lang.time.TZ;
import mosaics.strings.StringCodec;

import java.text.SimpleDateFormat;


@Value
public class DTMCodec implements StringCodec<DTM> {

    public static final StringCodec<DTM> DEFAULT_CODEC     = new DTMCodec( DTMUtils.DEFAULT_FORMATTER );
    public static final StringCodec<DTM> DTM_ISO8601_CODEC = new DTMCodec( DTMUtils.DTM_ISO8601_FORMATTER );


    private final SimpleDateFormat dtmFormat;


    public DTMCodec( String dtmFormat ) {
        this( dtmFormat, TZ.UTC );
    }

    public DTMCodec( String dtmFormat, TZ tz ) {
        this( DTMUtils.createFormatter(dtmFormat, tz) );
    }

    public DTMCodec( SimpleDateFormat dtmFormat ) {
        this.dtmFormat = dtmFormat;
    }


    public boolean supportsNull() {
        return false;
    }

    public String encode( DTM v ) {
        return Backdoor.invokeSynchronized( dtmFormat, () -> v == null ? "null" : dtmFormat.format(v.toJDKDate()) );
    }

    public Tryable<DTM> decode( String v ) {
        return Backdoor.invokeSynchronizedTry( dtmFormat, () -> new DTM(dtmFormat.parse(v)) );
    }

    public JavaClass getType() {
        return JavaClass.LONG_PRIMITIVE;
    }

    public String toString() {
        return "DTMCodec("+dtmFormat.toPattern()+")";
    }
}
