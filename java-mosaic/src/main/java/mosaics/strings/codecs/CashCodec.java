package mosaics.strings.codecs;

import mosaics.strings.parser.CharacterMatcher;
import mosaics.strings.parser.CharacterMatchers;
import mosaics.strings.parser.PullParser;
import lombok.Value;
import mosaics.fp.Try;
import mosaics.fp.Tryable;
import mosaics.lang.reflection.JavaClass;
import mosaics.strings.StringCodec;


@Value
public class CashCodec implements StringCodec<Long> {
    public static final StringCodec<Long> INSTANCE = new CashCodec();

    private static final CharacterMatcher MAJOR      = CharacterMatchers.integer(true);
    private static final CharacterMatcher MINOR      = CharacterMatchers.digits();
    private static final CharacterMatcher DECIMAL    = CharacterMatchers.constant('.');


    /**
     * longs are stored in tenths of the minor currency.. which for now we only support GBP.
     * Thus 1 is 0.001, which when rounded down is &pound;0.00.  And 101 is &pound;0.10 and 1909 is &pound;1.90.
     */
    public String encode( Long v ) {
        if ( v == null ) {
            return "null";
        }

        StringBuilder buf = new StringBuilder();

        if ( v < 0 ) {
            buf.append('-');
        }

        buf.append( "£" );
        buf.append( Long.toString(Math.abs(v)/1000) );
        buf.append( '.' );

        String minorStr = Long.toString(Math.abs(v % 1000) / 10);
        int i = minorStr.length();
        while ( i < 2 ) {
            buf.append('0');
            i++;
        }

        buf.append(minorStr);

        return buf.toString();
    }

    public Tryable<Long> decode( String v ) {
        PullParser p = new PullParser(v, v);

        long sign;
        if ( p.peek('-') ) {
            sign = -1;
            p.skip(1);
        } else {
            sign = 1;
        }

        if ( p.peek("£") ) {
            p.skip(1);
        }


        String majorStr = p.pullMandatory(MAJOR).toString();

        int  numDP = 3;
        long minor = 0;
        if ( p.hasMore() && p.peek(DECIMAL) ) {
            p.skip(DECIMAL);

            String minorStr = p.pullMandatory(MINOR).toString();
            int minorStrLength = minorStr.length();

            if ( minorStrLength == 0 ) {

            } else if ( minorStrLength <= numDP ) {
                minor = Long.parseLong(minorStr);

                int i = minorStr.length();
                while ( i < numDP ) {
                    minor *= 10;
                    i++;
                }

            } else if ( minorStrLength == numDP+1 ) {
                minor = (Long.parseLong(minorStr) + 5)/10;
            } else {
                minorStr = minorStr.substring(0,numDP+1);
                minor    = (Long.parseLong(minorStr) + 5)/10;
            }
        }

        if ( p.hasMore() ) {
            return Try.failed(new NumberFormatException("For input string: \"" + v + "\""));
        }


        long major  = Long.parseLong(majorStr);

        return Try.succeeded((major * 1000 + minor) * sign);
    }

    public boolean supportsNull() {
        return false;
    }

    public JavaClass getType() {
        return JavaClass.LONG_PRIMITIVE;
    }

    public String toString() {
        return "CashCodec";
    }
}
