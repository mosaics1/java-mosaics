package mosaics.strings;

import mosaics.fp.collections.FPOption;
import mosaics.lang.Backdoor;
import mosaics.strings.codecs.DTMCodecLong;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;


public class StringFormatters {

//    public static final StringFormatter<?> NOOP_FORMATTER = obj -> obj == null ? "null" : obj.toString();

    public static <T> StringFormatter<T> wrapStringCodec( StringCodec<T> codec ) {
        return codec::encode;
    }

    public static final StringFormatters DEFAULT = new StringFormatters();



    private final Map<String,StringFormatter> formatterCache = new HashMap<>();
    private final StringCodecs                wrappedCodecs;



    public StringFormatters() {
        this( StringCodecs.DEFAULT_CODECS );
    }

    public StringFormatters( StringCodecs codecs ) {
        this.wrappedCodecs = codecs;
    }



    public <T> StringFormatter<T> getFormatterFor( Class<?> jc ) {
        return getFormatterFor( jc, Optional.empty() );
    }

    @SuppressWarnings( "unchecked" )
    public <T> StringFormatter<T> getFormatterFor( Class<?> jc, Optional<String> alias ) {
        StringFormatter<T> f;
        if ( alias.isPresent() ) {
            f = getFormatterByName( alias.get() );
        } else {
            FPOption<StringCodec<T>> codec = Backdoor.cast(wrappedCodecs.getCodecFor(jc));

            if ( codec.isEmpty() ) {
                f = Object::toString;
            } else {
                f = codec.get()::encode;
            }
        }

        return f;
    }


    ////////////////////////
    @SuppressWarnings("unchecked")
    public <T> StringFormatter<T> getFormatterByName( String name ) {
        StringFormatter<T> f = (StringFormatter<T>) formatterCache.get(name);

        if ( f == null ) {
            f = createFormatter(name);

            formatterCache.put( name, f );
        }

        return f;
    }

    public <T> void registerFormatter( String name, StringFormatter<T> f ) {
        formatterCache.put( name, f );
    }


    @SuppressWarnings("unchecked")
    protected <T> StringFormatter<T> createFormatter( String name ) {
        StringCodec<T> codec = wrappedCodecs.getCodecByAlias(name);
        if ( codec != null ) {
            return codec::encode;
        }

        if ( name.startsWith("DTM(") && name.endsWith(")") ) {
            String dtmFormatTxt = name.substring("DTM(".length(), name.length() - 1);

            StringFormatter formatter = StringFormatters.wrapStringCodec(new DTMCodecLong(dtmFormatTxt));

            return formatter;
        } else if ( name.startsWith("pad") ) {
            if ( name.startsWith("padr") ) {
                int targetLength = Integer.parseInt(name.substring(4));

                return (StringFormatter<T>) new PadRightFormatter(targetLength, ' ');
            } else {
                int targetLength = Integer.parseInt(name.substring(3));

                return (StringFormatter<T>) new PadLeftFormatter(targetLength, ' ');
            }
        } else if ( name.startsWith("zpad") ) {
            if ( name.startsWith("zpadr") ) {
                int targetLength = Integer.parseInt(name.substring(5));

                return (StringFormatter<T>) new PadRightFormatter(targetLength, '0');
            } else {
                int targetLength = Integer.parseInt(name.substring(4));

                return (StringFormatter<T>) new PadLeftFormatter(targetLength, '0');
            }
        }

        return null;
    }



    private static final class PadLeftFormatter implements StringFormatter {
        private int  targetLength;
        private char padChar;

        public PadLeftFormatter( int targetLength, char padChar ) {
            this.targetLength = targetLength;
            this.padChar      = padChar;
        }

        public String format( Object o ) {
            String v = o == null ? "" : o.toString();

            if ( v.length() >= targetLength ) {
                return v;
            }

            StringBuilder buf = new StringBuilder(targetLength);

            buf.append(v);

            while ( buf.length() < targetLength ) {
                buf.append( padChar );
            }

            return buf.toString();
        }
    }

    private static final class PadRightFormatter implements StringFormatter {
        private int  targetLength;
        private char padChar;

        public PadRightFormatter( int targetLength, char padChar ) {
            this.targetLength = targetLength;
            this.padChar      = padChar;
        }

        public String format( Object o ) {
            String v = o == null ? "" : o.toString();

            if ( v.length() >= targetLength ) {
                return v;
            }

            StringBuilder buf = new StringBuilder(targetLength);

            while ( buf.length()+v.length() < targetLength ) {
                buf.append( padChar );
            }

            buf.append(v);

            return buf.toString();
        }
    }
}
