package mosaics.strings.templates;

import lombok.EqualsAndHashCode;
import lombok.Value;
import mosaics.fp.collections.FPOption;
import mosaics.lang.Backdoor;
import mosaics.lang.TryUtils;
import mosaics.lang.functions.Function1;
import mosaics.lang.reflection.ReflectionUtils;
import mosaics.strings.StringFormatter;
import mosaics.strings.StringFormatters;
import mosaics.strings.StringUtils;
import mosaics.strings.parser.CharacterPosition;
import mosaics.strings.templates.ast.ASTExpressionEvaluator;
import mosaics.strings.templates.ast.TemplateAST;
import mosaics.strings.templates.ast.TemplateASTWalker;
import mosaics.strings.templates.ast.TemplateExecuteASTWalker;
import mosaics.utils.ListUtils;
import mosaics.utils.MapUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * A text template engine.  Given a textual description of an output this class is capable of
 * generating the output text from a series of operations that construct the output.
 *
 * ||example||description||
 *  | hello world                | static text - comes out unchanged |
 *  | hello $name                | $name will be replaced with a value that is past in when the template is evaluated.
 *                               If the value 'name' cannot be found then MissingParameterException will be thrown. |
 *  | hello \\$name              | \\ before a $ escapes the $ so $ will be printed and no parameter name look up will occur |
 *  | hello ${name}              | as hello $name but the name is delimited by curly braces |
 *  | hello ${name:String}       | specifies the StringCodec to use to covert the value into a string. |
 *  | hello ${name:String+lc}    | +lc is a modifier that runs after the initial string has been rendered,
 *                                 lc is short for lowercase. |
 *  | hello ${name:String+lc+uc} | multiple modifiers may be specified, they are invoked in order from left to right |
 *  | ${a.b.c}                   | property chaining - retrieve the key a and then read property b on a and then c from that.
 *                                 a property may be a method, getter method or field |
 *  | ${a.b()}                   | brackets may be optionally specified to make it clear that we expect the method b()
 *                                 to be called |
 *  | ${name=Bob}                | As $name except if 'name' is not found then the value 'Bob' will be used |
 *  | [Hello $name]              | A one line if statement.  If 'name' is found then (eg JJ) then 'Hello JJ' will be displayed
 *                                 else nothing will be displayed.  The square brackets mark the text that will be skipped if
 *                                 'name' is not found. |
 *  | ${name ? a : b}            | A simple if/else block.  If 'name' is found then 'a' will be displayed, else 'b'.
 *                                 NB name can be any type of value that can be converted to a boolean.  Most values where
 *                                 the conversion to a boolean is not obvious then just its presence is used to imply true
 *                                 and its absence false. |
 *  | #if a
 *    text 1
 *    #else if b.c.d()
 *    text 2
 *    #else
 *    text 3
 *    #endif                     | An if/elseif/else block.  The condition must be a single variable fetcher.  No equations
 *                                 are composites are supported at this time. |
 *  | #for a in b
 *    $a
 *    #endfor                    | Iterate over a collection. |
 *
 *  | #static ... #endstatic     | output the text between the directives without interpreting them as commands |
 *  | #include-static path       | loads path from the classpath and inserts it with no interpretation |
 */
public class TextTemplate {

    private final TemplateAST                          ast;
    private final StringFormatters                     formatters;
    private final Map<String,Function1<String,String>> stringModifiers = new HashMap<>();


    TextTemplate( TemplateAST ast, StringFormatters formatters ) {
        this.ast        = ast;
        this.formatters = formatters;

        withStringModifier( "uc", String::toUpperCase );
        withStringModifier( "lc", String::toLowerCase );
        withStringModifier( "CapitaliseFirstWord", StringUtils::uppercaseFirstLetter );
    }


    public TextTemplate withStringModifier( String modName, Function1<String,String> mod ) {
        stringModifiers.put( modName, mod );

        return this;
    }

    public void invoke( Appendable out, Object...mapParts ) {
        invoke( out, MapUtils.asMap(mapParts));
    }

    public void invoke( Appendable out, Map<String,Object> properties ) {
        invoke( out, key -> FPOption.of(properties.get(key)) );
    }

    public <T> void invoke( File f, T dto ) {
        Writer out = TryUtils.invokeAndRethrowException( () -> new OutputStreamWriter(new FileOutputStream(f, false), Backdoor.UTF8) );

        try {
            this.invoke( out, dto );
        } catch ( Throwable ex ) {
            throw Backdoor.throwException( ex );
        } finally {
            TryUtils.invokeAndRethrowException( out::flush );
            TryUtils.invokeAndRethrowException( out::close );
        }
    }

    public <T> void invoke( Appendable out, T dto ) {
        invoke( out, key -> {
            Object value = ReflectionUtils.getPrivateFieldOrNoArgMethod(dto, key);

            if ( value instanceof Throwable ) {
                return FPOption.of( StringUtils.toString((Throwable) value));
            } else {
                return FPOption.of( value );
            }
        });
    }

    public String invoke( Object...mapParts ) {
        StringWriter out = new StringWriter();

        invoke( out, mapParts );

        return out.toString();
    }

    public String invoke( Map<String,Object> properties ) {
        StringWriter out = new StringWriter();

        invoke( out, properties );

        return out.toString();
    }

    public <T> String invoke( T dto ) {
        StringWriter out = new StringWriter();

        invoke( out, dto );

        return out.toString();
    }

    /**
     * Returns a set of property names that are referenced by the template.
     */
    public Set<String> getTemplatePropertyNames() {
        ParameterCollector collector = new ParameterCollector();

        ast.walkAST( collector );

        return collector.getParameterNames();
    }

    public void invoke( Appendable out, Function1<String,FPOption<Object>> dataFetcher ) {
        TemplateWriter templateState = new TemplateWriter( out, formatters, dataFetcher, stringModifiers );

        TryUtils.invokeAndRethrowException( () -> ast.walkAST(templateState) );
    }


    @Value
    @EqualsAndHashCode(callSuper = false)
    private static class TemplateWriter extends TemplateExecuteASTWalker {
        private final Appendable                           out;

        private final StringFormatters                     formatters;
        private final Map<String,Function1<String,String>> stringModifiers;


        public TemplateWriter( Appendable out, StringFormatters formatters, Function1<String, FPOption<Object>> dataFetcher, Map<String,Function1<String,String>> stringModifiers ) {
            super( dataFetcher );

            this.out             = out;
            this.formatters      = formatters;
            this.stringModifiers = stringModifiers;
        }


        public void walkTextAST( CharacterPosition pos, String text ) {
            appendToTemplateOut( text );
        }

        public void walkPropertyAST( CharacterPosition pos, String parameterName, FPOption<String> parameterType, List<String> chainedProperties, List<String> applyModifiers, FPOption<String> defaultValue ) {
            Object parameterValue = ASTExpressionEvaluator.fetchParameter( getDataFetcher(), pos, parameterName, chainedProperties, defaultValue.isEmpty() )
                .orElse( defaultValue::get );

            String formattedValue = formatValue( pos, parameterValue, parameterType, applyModifiers );

            appendToTemplateOut( formattedValue );
        }

        public void walkAndAST( CharacterPosition pos, List<TemplateAST> predicates ) {
            throw new UnsupportedOperationException();
        }

        private void appendToTemplateOut( String txt ) {
            try {
                out.append( txt );
            } catch ( IOException ex ) {
                throw Backdoor.throwException( ex );
            }
        }

        private String formatValue( CharacterPosition pos, Object parameterValue, FPOption<String> parameterType, List<String> applyModifiers ) {
            StringFormatter<Object> f;

            if ( parameterType.hasValue() ) {
                f = formatters.getFormatterByName(parameterType.get());

                if ( f == null ) {
                    throw new UnknownTypeException( pos, parameterType.get() );
                }
            } else {
                f = formatters.getFormatterFor( parameterValue.getClass() );
            }


            return ListUtils.fold(applyModifiers, f.format(parameterValue), ( txt, mod) -> this.applyModifier(pos,mod,txt));
        }


        private String applyModifier( CharacterPosition pos, String mod, String v ) {
            Function1<String,String> modifier = stringModifiers.get(mod);
            if ( modifier == null ) {
                throw new MissingModifierException( pos, mod );
            }

            return modifier.invoke(v);
        }
    }


    private static class ParameterCollector implements TemplateASTWalker {
        private final Set<String> parameterNames = new HashSet<>();

        public Set<String> getParameterNames() {
            return parameterNames;
        }

        public void walkTextAST( CharacterPosition pos, String text ) {}

        public void walkPropertyAST( CharacterPosition pos, String parameterName, FPOption<String> parameterType, List<String> chainedProperties, List<String> applyModifiers, FPOption<String> defaultValue ) {
            parameterNames.add( parameterName );
        }

        public void walkIfBlockAST( CharacterPosition pos, TemplateAST predicate, FPOption<TemplateAST> ifTrueBlock, FPOption<TemplateAST> elseBlock ) {
            predicate.walkAST( this );
            ifTrueBlock.ifPresent( block -> block.walkAST(this) );
            elseBlock.ifPresent( block -> block.walkAST(this) );
        }

        public void walkForEachAST( CharacterPosition pos, String variableName, TemplateAST expression, TemplateAST block ) {
            boolean isVariableNameAlreadyReferenced = parameterNames.contains( variableName );

            expression.walkAST(this);
            block.walkAST(this);

            if ( !isVariableNameAlreadyReferenced ) {
                parameterNames.remove( variableName );
            }
        }

        public void walkAndAST( CharacterPosition pos, List<TemplateAST> predicates ) {
            predicates.forEach( p -> p.walkAST(this) );
        }
    }
}
