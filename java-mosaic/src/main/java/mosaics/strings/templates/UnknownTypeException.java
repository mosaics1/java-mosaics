package mosaics.strings.templates;

import mosaics.strings.parser.CharacterPosition;
import mosaics.strings.parser.ParseException;


/**
 *
 */
public class UnknownTypeException extends ParseException {

    public UnknownTypeException( CharacterPosition pos, String type ) {
        super( pos, "Unknown parameter type '" + type + "'" );
    }

}
