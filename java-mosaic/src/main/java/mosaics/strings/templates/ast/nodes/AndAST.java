package mosaics.strings.templates.ast.nodes;

import mosaics.fp.FP;
import mosaics.fp.collections.FPIterable;
import mosaics.fp.collections.FPOption;
import mosaics.strings.parser.CharacterPosition;
import mosaics.strings.templates.ast.TemplateAST;
import mosaics.strings.templates.ast.TemplateASTWalker;
import mosaics.strings.templates.ast.TemplateContext;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Value;

import java.util.Arrays;
import java.util.List;

import static mosaics.lang.Backdoor.cast;


@Value
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class AndAST extends TemplateAST {
    private CharacterPosition pos;
    private List<TemplateAST> predicates;


    public AndAST( CharacterPosition pos, TemplateAST...children ) {
        this( pos, Arrays.asList(children) );
    }


    public void walkAST( TemplateASTWalker walker ) {
        walker.walkAndAST( pos, predicates );
    }

    public FPOption<TemplateAST> optimiseSelf( TemplateContext ctx ) {
        List<TemplateAST> optimisedPredicates = FP.wrap(predicates).flatMap(ast -> ast.optimiseSelf(ctx)).toList();

        if ( optimisedPredicates.isEmpty() ) {
            return FP.emptyOption();
        } else if ( optimisedPredicates.size() == 1 ) {
            return FP.option( optimisedPredicates.get(0) );
        }

        return internOptimisedVersion( new AndAST(pos,optimisedPredicates) );
    }

    public <T extends TemplateAST> FPIterable<TemplateAST> collectChildrenOfType( Class<T> type ) {
        if ( type.isAssignableFrom(this.getClass()) ) {
            return FP.wrapSingleton( this );
        }

        return cast(FP.wrap(predicates).filter(type));
    }
}
