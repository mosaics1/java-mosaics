package mosaics.strings.templates.ast;

import mosaics.strings.parser.CharacterPosition;
import mosaics.fp.collections.FPOption;

import java.util.List;


/**
 * Set of callbacks received by walking the structure of the TemplateAST.
 */
public interface TemplateASTWalker {

    public void walkTextAST( CharacterPosition pos, String text );

    public void walkPropertyAST( CharacterPosition pos, String parameterName, FPOption<String> parameterType, List<String> chainedProperties, List<String> applyModifiers, FPOption<String> defaultValue );
    public void walkIfBlockAST( CharacterPosition pos, TemplateAST predicate, FPOption<TemplateAST> ifTrueBlock, FPOption<TemplateAST> elseBlock );
    public void walkForEachAST( CharacterPosition pos, String variableName, TemplateAST expression, TemplateAST block );

    public void walkAndAST( CharacterPosition pos, List<TemplateAST> predicates );


    public default void walkBlockAST( CharacterPosition pos, List<TemplateAST> statements ) {
        statements.forEach( ast -> ast.walkAST(this) );
    }

}
