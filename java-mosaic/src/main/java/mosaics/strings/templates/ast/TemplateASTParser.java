package mosaics.strings.templates.ast;

import mosaics.io.IOUtils;
import mosaics.strings.parser.CharacterIterator;
import mosaics.strings.parser.CharacterMatcher;
import mosaics.strings.parser.CharacterMatchers;
import mosaics.strings.parser.CharacterPosition;
import mosaics.strings.parser.ParseException;
import mosaics.strings.parser.PullParser;
import mosaics.strings.templates.ast.nodes.AndAST;
import mosaics.strings.templates.ast.nodes.BlockAST;
import mosaics.strings.templates.ast.nodes.ForEachAST;
import mosaics.strings.templates.ast.nodes.IfBlockAST;
import mosaics.strings.templates.ast.nodes.ParameterAST;
import mosaics.strings.templates.ast.nodes.TextAST;
import lombok.AllArgsConstructor;
import lombok.Value;
import mosaics.fp.FP;
import mosaics.fp.collections.ConsList;
import mosaics.fp.collections.FPOption;
import mosaics.lang.Backdoor;
import mosaics.lang.QA;
import mosaics.strings.StringUtils;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static mosaics.strings.templates.ast.TemplateParserFeature.FOREACH_SUPPORT;


@Value
@AllArgsConstructor
public class TemplateASTParser {

    private static final CharacterMatcher TEMPLATE_TEXT          = CharacterMatchers.everythingExcept("$", "[", "\\", "#if", "#else", "#endif", "#for", "#endfor", "\r", "\n" );
    private static final CharacterMatcher PARAMETER_NAME         = CharacterMatchers.javaVariableName();
    private static final CharacterMatcher PARAMETER_TYPEALIAS    = CharacterMatchers.regexp("[a-zA-Z0-9]+(\\([^)]*\\))?");
    private static final CharacterMatcher PARAMETER_MODIFIER     = CharacterMatchers.regexp("[a-zA-Z0-9]+");
    private static final CharacterMatcher JAVA_PROPERTY_CALL     = CharacterMatchers.regexp("[a-zA-Z][a-zA-Z0-9$_]*");
    private static final CharacterMatcher DEFAULT_PROPERTY_VALUE = CharacterMatchers.regexp( "[^?:}]*" );
    private static final CharacterMatcher PARAMETER_VALUE        = CharacterMatchers.regexp( "[^?:}]*" );
    private static final CharacterMatcher DIRECTIVE_LABEL        = CharacterMatchers.regexp( "#[a-zA-Z0-9]+" );
    private static final CharacterMatcher ENDDIRECTIVE_LABEL     = CharacterMatchers.regexp( "[ \t]*#end[a-zA-Z0-9]*" );
    private static final CharacterMatcher IS_DIRECTIVE_PRESENT   = CharacterMatchers.regexp( "[ \t]*#[a-zA-Z0-9]+" );
    private static final CharacterMatcher INCLUDESTATIC_VALUE    = CharacterMatchers.regexp( "[ \t]*#include-static" );
    private static final CharacterMatcher STATIC_VALUE           = CharacterMatchers.regexp( "[ \t]*#static" );
    private static final CharacterMatcher STATICBLOCK_TEXT       = CharacterMatchers.everythingExcept( "#endstatic" );
    private static final CharacterMatcher ENDSTATIC_VALUE        = CharacterMatchers.regexp( "[ \t]*#endstatic" );
    private static final CharacterMatcher IFCMD_VALUE            = CharacterMatchers.regexp( "[ \t]*#if" );
    private static final CharacterMatcher ELSEIFCMD_VALUE        = CharacterMatchers.regexp( "[ \t]*#else if" );
    private static final CharacterMatcher ELSECMD_VALUE          = CharacterMatchers.regexp( "[ \t]*#else" );
    private static final CharacterMatcher ENDIFCMD_VALUE         = CharacterMatchers.regexp( "[ \t]*#endif" );
    private static final CharacterMatcher FORCMD_VALUE           = CharacterMatchers.regexp( "[ \t]*#for" );
    private static final CharacterMatcher ENDFORCMD_VALUE        = CharacterMatchers.regexp( "[ \t]*#endfor" );
    private static final CharacterMatcher IN_KEYWORD             = CharacterMatchers.constant( "in" );

    private static final CharacterMatcher CONSTANT_TEXT_WITHIN_ONELINEIF = CharacterMatchers.everythingExcept( "$", "]", "\\" );


    private static final FPOption<String> BLANK_STRING_OPTION = FP.option("");


    private Set<TemplateParserFeature> disabledFeatures;


    public TemplateASTParser() {
        this( Collections.emptySet() );
    }

    public TemplateASTParser withDisableFeatures( TemplateParserFeature...featuresToDisable ) {
        Set<TemplateParserFeature> newDisabledSet = new HashSet<>(disabledFeatures);
        newDisabledSet.addAll( Arrays.asList(featuresToDisable) );

        return new TemplateASTParser( newDisabledSet );
    }

    public FPOption<TemplateAST> parse( String sourceName, String...lines ) {
        return parse( sourceName, CharacterIterator.linesIterator(lines) );
    }

    public FPOption<TemplateAST> parse( String sourceName, CharacterIterator in ) {
                QA.argNotNull( "templateName", sourceName );

        PullParser p = new PullParser( sourceName, in );

        TemplateContext ctx = new TemplateContext();

        TemplateAST templateAST2 = parseNextTemplateBlock( p );
        return templateAST2.optimiseSelf(ctx);
    }

    private TemplateAST parseNextTemplateBlock( PullParser p ) {
        BlockAST rootAST = new BlockAST(p.getPosition(), Collections.emptyList() );
        while ( p.hasMore() && !p.peek(ENDDIRECTIVE_LABEL) && !p.peek(ELSECMD_VALUE) ) {
            FPOption<TemplateAST> ast = parseNextTemplatePart( p );

            rootAST = rootAST.conditionallyAppend( ast );
        }

        return rootAST;
    }

    private FPOption<TemplateAST> parseNextTemplatePart( PullParser p ) {
        char c = p.peek();

        if ( c == '$' ) {
            return parseParameterPart(p);
        } else if ( c == '[' ) {
            return parseOneLineIfPart( p );
        } else if ( p.peek(IS_DIRECTIVE_PRESENT) ) {
            p.skipWhitespace();
            CharacterPosition pos = p.getPosition();

            if ( p.peek(IFCMD_VALUE) ) {
                return parseIfBlock(p);
            } else if ( p.peek(FORCMD_VALUE) && !disabledFeatures.contains(FOREACH_SUPPORT) ) {
                return parseForBlock( p );
            } else if ( p.peek(INCLUDESTATIC_VALUE) ) {
                return parseIncludeStaticDirective( p );
            } else if ( p.peek(STATIC_VALUE) ) {
                return parseStaticBlock( p );
            } else {
                String directiveLabel = p.pullMandatory( DIRECTIVE_LABEL );

                throw new ParseException( pos, "Unknown directive: " + directiveLabel );
            }
        } else {
            return parseConstantPart(p);
        }
    }

    private FPOption<TemplateAST> parseConstantPart( PullParser p ) {
        return parseConstantPart( p, TEMPLATE_TEXT );
    }

    private FPOption<TemplateAST> parseConstantPart( PullParser p, CharacterMatcher textMatcher ) {
        CharacterPosition pos  = p.getPosition();
        FPOption<String>  text = p.pullOptional( textMatcher );

        if ( text.hasValue() ) {
            return text.map( constantPart -> new TextAST( pos, constantPart ) );
        } else if ( p.peekAndPull('\\') ) {   // handle escaped characters such as '\$name'
            if ( p.hasMore() ) {
                char c = p.next();

                return FPOption.of( new TextAST(pos,Character.toString(c)) );
            }

            return FPOption.none();
        } else {
            p.peekAndPull( '\r' );
            p.peekAndPull( '\n' );

            return FPOption.of( new TextAST(pos,Backdoor.NEWLINE) );
        }
    }

    // $foo
    // ${foo}
    // ${foo.a.b.getFoo()}
    // ${foo:type}
    // ${foo:type+mod1}
    // ${foo:type+mod1+mod2}
    // ${foo:+mod1+mod2}
    private FPOption<TemplateAST> parseParameterPart( PullParser p ) {
        CharacterPosition parameterPosition = p.getPosition();

        p.pullMandatory( '$' );

        if (p.isEmpty()) {
            throw new ParseException( parameterPosition, "missing parameter name after $ at end of template" );
        }

        if ( p.peekAndPull('{') ) {
            FPOption<String>            parameterType     = FP.emptyOption();
            List<String>                chainedProperties = new ArrayList<>();
            List<String>                modifiers         = new ArrayList<>();
            FPOption<String>            defaultValue      = FP.emptyOption();
            FPOption<CharacterPosition> defaultValuePos   = FP.emptyOption();

            String                      parameterName     = p.pullMandatory( PARAMETER_NAME );

            while ( p.peekAndPull('.') ) {
                String propertyName = p.pullMandatory( JAVA_PROPERTY_CALL );

                chainedProperties.add( propertyName );

                if ( p.peekAndPull('(') ) {
                    p.pullMandatory( ')' );
                }
            }

            if ( p.peekAndPull( '=' ) ) {
                defaultValuePos = FP.option( p.getPosition() );

                defaultValue = p.pullOptional( DEFAULT_PROPERTY_VALUE )
                    .or( BLANK_STRING_OPTION );
            }

            p.skipWhitespace();


            TemplateAST parameterAST;
            if ( p.peekAndPull(':') ) {
                p.skipWhitespace();

                CharacterPosition typePos = p.getPosition();

                parameterType = p.pullOptional(PARAMETER_TYPEALIAS);


                while ( p.peekAndPull('+') ) {
                    String modifier = p.pullMandatory( PARAMETER_MODIFIER );

                    modifiers.add( modifier );
                }

                p.skipWhitespace();

                if ( p.peek( '?' ) ) {
                    throw new ParseException( typePos, "explicit types are not supported with the `? :` operators" );
                }

                parameterAST = new ParameterAST(parameterPosition, parameterName, parameterType, chainedProperties, modifiers, defaultValue, FP.emptyOption());
            } else {
                if ( p.peekAndPull( '?' ) ) {
                    if ( defaultValue.hasValue() ) {
                        throw new ParseException( defaultValuePos.get(), "default values are not supported with the `? :` operators" );
                    }

                    p.skipWhitespace();

                    CharacterPosition ifTruePos = p.getPosition();
                    String trueValue = p.pullMandatory( PARAMETER_VALUE );

                    p.skipWhitespace();

                    p.pullMandatory( ':' );

                    p.skipWhitespace();

                    CharacterPosition ifFalsePos = p.getPosition();
                    String falseValue = p.pullMandatory( PARAMETER_VALUE );

                    parameterAST = new IfBlockAST(
                        parameterPosition,
                        new ParameterAST(parameterPosition, parameterName, parameterType, chainedProperties, modifiers, defaultValue, FP.emptyOption())
                            .withParameterType( "boolean" ),
                        FP.option( new TextAST(ifTruePos, trueValue.trim()) ),
                        FP.option( new TextAST(ifFalsePos, falseValue.trim()) )
                    );
                } else {
                    parameterAST = new ParameterAST(parameterPosition, parameterName, parameterType, chainedProperties, modifiers, defaultValue, FP.emptyOption());
                }
            }

            p.skipWhitespace();
            p.pullMandatory( '}' );

            return FP.option( parameterAST );
        } else {
            String parameterName = p.pullMandatory( PARAMETER_NAME );

            return FP.option( new ParameterAST(parameterPosition, parameterName) );
        }
    }

    private FPOption<TemplateAST> parseStaticBlock( PullParser p ) {
        p.skipWhitespace();

        CharacterPosition directivePos = p.getPosition();

        if ( p.peekAndPull(STATIC_VALUE) ) {
            p.skipToEndOfLine();

            CharacterPosition textPos = p.getPosition();
            String            text    = p.pullMandatory( STATICBLOCK_TEXT );

            p.pullMandatory( ENDSTATIC_VALUE );

            return FP.option( new TextAST(textPos, text) );
        }

        throw new IllegalStateException( "Unknown directive at " + directivePos );
    }

    /*
    #include-static /a/b/c/foo.html
     */
    private FPOption<TemplateAST> parseIncludeStaticDirective( PullParser p ) {
        p.skipWhitespace();

        CharacterPosition directivePos = p.getPosition();

        if ( p.peekAndPull(INCLUDESTATIC_VALUE) ) {
            p.skipWhitespace();

            String path = p.pullToEndOfLine().trim();
            if ( StringUtils.isBlank(path) ) {
                throw new ParseException( directivePos, "#include-static requires a classpath resource path" );
            }

            String text = loadResource(directivePos, path);

            return FP.option( new TextAST(directivePos, text) );
        }

        throw new IllegalStateException( "Unknown directive at " + directivePos );
    }

    private String loadResource( CharacterPosition pos, String path ) {
        InputStream in = this.getClass().getResourceAsStream(path);
        if ( in == null ) {
            throw new ParseException( pos, "Unable to find resource '"+path+"' on the classpath." );
        }

        return IOUtils.toString( new InputStreamReader(in,IOUtils.UTF8) );
    }

    /*
         #if expr
         .....
         #else if expr2
         .....
         #else
         .....
         #endif
     */
    private FPOption<TemplateAST> parseIfBlock( PullParser p ) {
        p.skipWhitespace();

        CharacterPosition pos = p.getPosition();

        if ( p.peekAndPull(IFCMD_VALUE) ) {
            p.skipWhitespace();

            TemplateAST predicate = parseBooleanExpression(p);

            p.skipToEndOfLine();

            TemplateAST onTrueBlock = parseNextTemplateBlock(p);

            p.skipWhitespace();

            // NB store the elseif blocks in reverse order (as we are going to join them together
            // starting at the bottom and working upwards)
            ConsList<IfBlockAST> ifBlocks = ConsList.singleton(
                new IfBlockAST(pos, predicate, FP.option(onTrueBlock), FP.emptyOption())
            );

            while ( p.peek(ELSEIFCMD_VALUE) ) {
                CharacterPosition elseIfPos = p.getPosition();

                p.pullMandatory( ELSEIFCMD_VALUE );
                p.skipWhitespace();

                TemplateAST elseIfExpr = parseBooleanExpression(p);

                p.skipToEndOfLine();

                TemplateAST elseIfBlock = parseNextTemplateBlock(p);

                ifBlocks = ifBlocks.append(
                    new IfBlockAST( elseIfPos, elseIfExpr, elseIfBlock )
                );

                p.skipWhitespace();
            }

            if ( p.peekAndPull(ELSECMD_VALUE) ) {
                p.skipToEndOfLine();

                TemplateAST elseBlock = parseNextTemplateBlock(p);

                // We add the else as the last ifBlock in the chain..  This is because our ConsList
                // only takes IfBlocks.  So we give it an empty predicate knowing that the optimiser
                // will remove it.  This oddity lets us treat elseif and else blocks in the same way.
                ifBlocks = ifBlocks.append(
                    new IfBlockAST( elseBlock.getPos(), new AndAST(elseBlock.getPos()), FP.emptyOption(), FP.option(elseBlock) )
                );

                p.skipWhitespace();
            }

            p.pullMandatory( ENDIFCMD_VALUE );
            p.skipToEndOfLine();

            IfBlockAST rootIfBlock = ifBlocks.tail().fold(
                ifBlocks.getHead(),
                (soFar,next) -> next.withElseBlock(soFar)
            );

            return FPOption.of( rootIfBlock );
        }

        throw new IllegalStateException( "Unknown directive at " + pos );
    }

    private FPOption<TemplateAST> parseOneLineIfPart( PullParser p ) {
        CharacterPosition parameterPosition = p.getPosition();

        BlockAST onTrueBlock = new BlockAST( parameterPosition );

        p.pullMandatory( '[' );

        while ( p.hasMore() && !p.peek(']') ) {
            FPOption<TemplateAST> fragment = parseOneLineIfFragment( p );

            onTrueBlock = onTrueBlock.conditionallyAppend( fragment );
        }

        p.pullMandatory( ']' );


        List<TemplateAST> requiredParameters = FP.wrap(onTrueBlock.getChildren())
            .flatMap( child -> child.collectChildrenOfType(ParameterAST.class) )
            .filter(ParameterAST.class)
            .map( child -> child.withParameterType("boolean") )
            .filter(TemplateAST.class)
            .toList();

        TemplateAST predicate = new AndAST( parameterPosition, requiredParameters );

        return FP.option(
            new IfBlockAST( parameterPosition, predicate, FP.option(onTrueBlock), FP.emptyOption() )
        );
    }

    private FPOption<TemplateAST> parseOneLineIfFragment( PullParser p ) {
        if ( p.peek() == '$' ) {
            return parseParameterPart( p );
        } else if ( p.peek() == '\\' ) {
            CharacterPosition pos = p.getPosition();

            p.pullMandatory( '\\' );

            if ( p.hasMore() ) {
                char c = p.next();

                return FPOption.of( new TextAST(pos,Character.toString(c)) );
            } else {
                return FPOption.none();
            }
        } else {
            return parseConstantPart( p, CONSTANT_TEXT_WITHIN_ONELINEIF );
        }
    }



    /*
    #for x in y
    ....
    #endfor
     */
    private FPOption<TemplateAST> parseForBlock( PullParser p ) {
        p.skipWhitespace();

        CharacterPosition pos = p.getPosition();

        if ( p.peekAndPull(FORCMD_VALUE) ) {
            p.skipWhitespace();

            String parameterName = p.pullMandatory(PARAMETER_NAME);

            p.skipWhitespace();
            p.pullMandatory( IN_KEYWORD );
            p.skipWhitespace();

            TemplateAST collectionFetcher = parseExpression(p);


            p.skipToEndOfLine();

            TemplateAST forBlock = parseNextTemplateBlock(p);

            p.pullMandatory( ENDFORCMD_VALUE );

            TemplateAST forEachAST = new ForEachAST( pos, parameterName, collectionFetcher, forBlock );

            return FPOption.of(forEachAST);
        }

        throw new IllegalStateException( "Unknown directive at " + pos );
    }

    private TemplateAST parseBooleanExpression( PullParser p ) {
        return parseExpression(p).withParameterType( "boolean" );
    }

    private ParameterAST parseExpression( PullParser p ) {
        CharacterPosition paramNamePos      = p.getPosition();
        String            parameterName     = p.pullMandatory( PARAMETER_NAME );
        List<String>      chainedProperties = new ArrayList<>();

        while ( p.peekAndPull('.') ) {
            String propertyName = p.pullMandatory( JAVA_PROPERTY_CALL );

            chainedProperties.add( propertyName );

            if ( p.peekAndPull('(') ) {
                p.pullMandatory( ')' );
            }
        }

        return new ParameterAST( paramNamePos, parameterName )
            .withChainedProperties( chainedProperties );
    }

}


