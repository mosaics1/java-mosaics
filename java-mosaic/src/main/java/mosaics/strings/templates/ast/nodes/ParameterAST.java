package mosaics.strings.templates.ast.nodes;

import mosaics.strings.parser.CharacterPosition;
import mosaics.strings.templates.ast.TemplateAST;
import mosaics.strings.templates.ast.TemplateASTWalker;
import mosaics.strings.templates.ast.TemplateContext;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Value;
import mosaics.fp.FP;
import mosaics.fp.collections.FPOption;
import mosaics.lang.reflection.fetcher.ValueFetcher;
import mosaics.lang.reflection.fetcher.ValueFetcherBuilder;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;


@Value
@AllArgsConstructor
@EqualsAndHashCode(callSuper=false, exclude="valueFetchers")
public class ParameterAST extends TemplateAST {
    private CharacterPosition      pos;
    private String                 parameterName;
    private FPOption<String>       parameterType;
    private List<String>           chainedProperties;
    private List<String>           modifiers;
    private FPOption<String>       defaultValue;

    private FPOption<ValueFetcher> valueFetchers;


    public ParameterAST( CharacterPosition pos, String parameterName ) {
        this( pos, parameterName, FPOption.none(), Collections.emptyList(), Collections.emptyList(), FP.emptyOption(), FP.emptyOption() );
    }


    public void walkAST( TemplateASTWalker walker ) {
        walker.walkPropertyAST( pos, parameterName, parameterType, chainedProperties, modifiers, defaultValue );
    }


    public FPOption<TemplateAST> optimiseSelf( TemplateContext ctx ) {
        ValueFetcherBuilder builder = ValueFetcherBuilder.newValueFetcherBuilder( parameterName );

        for ( String propertyName : chainedProperties ) {
            builder = builder.fetchProperty( propertyName );
        }

        if ( parameterType.hasValue() ) {
//            TypeConverter converter = ctx.getTypeConverterFor( parameterType.get() )
//                .orElseThrow( () -> new UnknownTypeException(pos, typeLabel) );
//
//            builder = builder.convertTypeUsing( converter );

            //////////
//            String typeLabel = parameterType.get().getType();
//
//            JavaClass resolvedType = ctx.getTypeByLabel( typeLabel )
//                .orElseThrow( () -> new UnknownTypeException(pos, typeLabel) );
//
//            builder = builder.asType( resolvedType );
        }

        if ( defaultValue.hasValue() ) {
//            builder = builder.withDefaultValue( defaultValue.get() );
        }

        ValueFetcher fetcher = builder.build();

        return FP.option( new ParameterAST(pos, parameterName, parameterType, chainedProperties, modifiers, defaultValue, FP.option(fetcher)) );
    }

    public ParameterAST withParameterType( String type ) {
        return new ParameterAST( pos, parameterName, FP.option(type), chainedProperties, modifiers, defaultValue, FP.emptyOption() );
    }

    public ParameterAST withChainedProperties(String...propertyLabels) {
        return new ParameterAST( pos, parameterName, parameterType, Arrays.asList(propertyLabels), modifiers, defaultValue, FP.emptyOption() );
    }

    public ParameterAST withChainedProperties(List<String> propertyLabels) {
        return new ParameterAST( pos, parameterName, parameterType, propertyLabels, modifiers, defaultValue, FP.emptyOption() );
    }

    public ParameterAST withModifiers(String...modifiers) {
        return new ParameterAST( pos, parameterName, parameterType, chainedProperties, Arrays.asList(modifiers), defaultValue, FP.emptyOption() );
    }

    public ParameterAST withDefaultValue(String newDefaultValue) {
        return new ParameterAST( pos, parameterName, parameterType, chainedProperties, modifiers, FP.option(newDefaultValue), FP.emptyOption() );
    }

}
