package mosaics.strings.templates.ast.nodes;

import mosaics.fp.FP;
import mosaics.fp.collections.FPIterable;
import mosaics.fp.collections.FPOption;
import mosaics.strings.parser.CharacterPosition;
import mosaics.strings.templates.ast.TemplateAST;
import mosaics.strings.templates.ast.TemplateASTWalker;
import mosaics.strings.templates.ast.TemplateContext;
import lombok.EqualsAndHashCode;
import lombok.Value;


@Value
@EqualsAndHashCode(callSuper = false)
public class ForEachAST extends TemplateAST {
    private CharacterPosition pos;
    private String            variableName;
    private TemplateAST expression;
    private TemplateAST block;

    public void walkAST( TemplateASTWalker walker ) {
        walker.walkForEachAST( pos, variableName, expression, block );
    }

    public FPOption<TemplateAST> optimiseSelf( TemplateContext ctx ) {
        FPOption<TemplateAST> optimisedExpression = expression.optimiseSelf(ctx);
        FPOption<TemplateAST> optimisedIfBlock    = block.optimiseSelf(ctx);

        if ( optimisedExpression.isEmpty() || optimisedIfBlock.isEmpty() ) {
            return FP.emptyOption();
        }

        TemplateAST alt = new ForEachAST(
            pos,
            variableName,
            optimisedExpression.get(),
            optimisedIfBlock.get()
        );

        return internOptimisedVersion( alt );
    }

    public <T extends TemplateAST> FPIterable<TemplateAST> collectChildrenOfType( Class<T> type ) {
        if ( type.isAssignableFrom(this.getClass()) ) {
            return FP.wrapSingleton( this );
        }

        return expression.collectChildrenOfType(type).and( block.collectChildrenOfType(type) );
    }
}
