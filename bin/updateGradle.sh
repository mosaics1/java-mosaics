#!/bin/bash

if [ -z "$1" ]
  then
    echo "Usage: $0 <NEW_GRADLE_VERSION>"
    exit 1
fi

set -eux

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." >/dev/null 2>&1 && pwd )"
newGradleVersion=$1

$DIR/gradlew wrapper --gradle-version $newGradleVersion
