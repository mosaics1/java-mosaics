package mosaics.logging;

import lombok.Value;
import mosaics.fp.FP;
import mosaics.lang.Secret;
import mosaics.lang.reflection.MissingPropertyException;
import mosaics.logging.loglevels.AuditError;
import mosaics.logging.loglevels.AuditFatal;
import mosaics.logging.loglevels.AuditInfo;
import mosaics.logging.loglevels.AuditWarn;
import mosaics.logging.loglevels.DevError;
import mosaics.logging.loglevels.DevFatal;
import mosaics.logging.loglevels.DevInfo;
import mosaics.logging.loglevels.DevWarn;
import mosaics.logging.loglevels.OpsError;
import mosaics.logging.loglevels.OpsFatal;
import mosaics.logging.loglevels.OpsInfo;
import mosaics.logging.loglevels.OpsWarn;
import mosaics.logging.loglevels.UserError;
import mosaics.logging.loglevels.UserFatal;
import mosaics.logging.loglevels.UserInfo;
import mosaics.logging.loglevels.UserWarn;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static mosaics.junit.JMAssertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;


@SuppressWarnings("UnnecessaryInterfaceModifier")
public class LogMessageFactoryTest {
    private final LogMessageProxyFactory logFactory = new LogMessageProxyFactory();

    @Nested
    public class MalformeLogMessageFactoryTestCases {
        @Test
        public void givenNulLogMessageFactoryClass_callcreateLogMessageFactory_expectException() {
            assertThrows( IllegalArgumentException.class, () -> logFactory.createLogMessageFactory(null) );
        }

        @Test
        public void givenNoAnnotationOLogMessageFactoryInterface_callcreateLogMessageFactory_expectException() {
            try {
                logFactory.createLogMessageFactory( MissingAnnotationLogMessageFactory.class );
                fail( "expected exception" );
            } catch ( IllegalArgumentException ex ) {
                assertEquals( "mosaics.logging.LogMessageFactoryTest$MissingAnnotationLogMessageFactory#a() is missing the required log level annotation", ex.getMessage() );
            }
        }

        @Test
        public void giveLogMessageFactoryWithVoidReturnType_expectException() {
            try {
                logFactory.createLogMessageFactory( VoidReturnTypLogMessageFactory.class );
                fail( "expected exception" );
            } catch ( IllegalArgumentException ex ) {
                assertEquals( "mosaics.logging.LogMessageFactoryTest$VoidReturnTypLogMessageFactory#a() must return LogMessage (it returns void)", ex.getMessage() );
            }
        }

        @Test
        public void hasDefaultMethod_expectException() {
            try {
                logFactory.createLogMessageFactory( UseDefaultMethoLogMessageFactory.class );
                fail( "expected exception" );
            } catch ( IllegalArgumentException ex ) {
                assertEquals( "mosaics.logging.LogMessageFactoryTest$UseDefaultMethoLogMessageFactory#a() is declared default (default methods are not supported)", ex.getMessage() );
            }
        }

        @Test
        public void givenMalformedTemplate_expectError() {
            assertThrows( MalformedTemplateException.class, () -> logFactory.createLogMessageFactory( MalformedTemplateLogMessageFactory.class) );
        }

        @Test
        public void givenTemplateThatReferencesMissingVariableName_callcreateLogMessageFactory_expectException() {
            assertThrows(
                MissingPropertyException.class,
                "mosaics.logging.LogMessageFactoryTest$TemplateReferencesMissingParameterByNamLogMessageFactory#malformedTemplate(boolean flag, java.lang.String name) is missing property 'foo'",
                () -> logFactory.createLogMessageFactory( TemplateReferencesMissingParameterByNamLogMessageFactory.class)
            );
        }
    }

    @Nested
    public class NoArgLogMethods {
        @Test
        public void givenOneAuditFatalNoArgMethod_callTheLogMethod_expectLogWritterToBeCalled() {
            NoArLogMessageFactory logger = logFactory.createLogMessageFactory( NoArLogMessageFactory.class );

            LogMessage expectedLogMessage = new LogMessage(
                TargetAudience.AUDIT,
                LogSeverity.FATAL,
                FP.option( NoArLogMessageFactory.class.getName()+"#auditFatal()"),
                "auditFatal() was called",
                FP.emptyMap()
            );

            assertEquals( expectedLogMessage, logger.auditFatal() );
        }

        @Test
        public void givenOneAuditErrorNoArgMethod_callTheLogMethod_expectLogWritterToBeCalled() {
            NoArLogMessageFactory logger = logFactory.createLogMessageFactory( NoArLogMessageFactory.class );


            LogMessage expectedLogMessage = new LogMessage(
                TargetAudience.AUDIT,
                LogSeverity.ERROR,
                FP.option( NoArLogMessageFactory.class.getName()+"#auditError()"),
                "auditError() was called",
                FP.emptyMap()
            );

            assertEquals( expectedLogMessage, logger.auditError() );
        }

        @Test
        public void givenOneAuditWarnNoArgMethod_callTheLogMethod_expectLogWritterToBeCalled() {
            NoArLogMessageFactory logger = logFactory.createLogMessageFactory( NoArLogMessageFactory.class );


            LogMessage expectedLogMessage = new LogMessage(
                TargetAudience.AUDIT,
                LogSeverity.WARN,
                FP.option( NoArLogMessageFactory.class.getName()+"#auditWarn()"),
                "auditWarn() was called",
                FP.emptyMap()
            );

            assertEquals( expectedLogMessage, logger.auditWarn() );
        }

        @Test
        public void givenOneAuditInfoNoArgMethod_callTheLogMethod_expectLogWritterToBeCalled() {
            NoArLogMessageFactory logger = logFactory.createLogMessageFactory( NoArLogMessageFactory.class );
            logger.auditInfo();


            LogMessage expectedLogMessage = new LogMessage(
                TargetAudience.AUDIT,
                LogSeverity.INFO,
                FP.option( NoArLogMessageFactory.class.getName()+"#auditInfo()"),
                "auditInfo() was called",
                FP.emptyMap()
            );

            assertEquals( expectedLogMessage, logger.auditInfo() );
        }

        @Test
        public void givenOneOpsFatalNoArgMethod_callTheLogMethod_expectLogWritterToBeCalled() {
            NoArLogMessageFactory logger = logFactory.createLogMessageFactory( NoArLogMessageFactory.class );


            LogMessage expectedLogMessage = new LogMessage(
                TargetAudience.OPS,
                LogSeverity.FATAL,
                FP.option( NoArLogMessageFactory.class.getName()+"#opsFatal()"),
                "opsFatal() was called",
                FP.emptyMap()
            );

            assertEquals( expectedLogMessage, logger.opsFatal() );
        }

        @Test
        public void givenOneOpsErrorNoArgMethod_callTheLogMethod_expectLogWritterToBeCalled() {
            NoArLogMessageFactory logger = logFactory.createLogMessageFactory( NoArLogMessageFactory.class );


            LogMessage expectedLogMessage = new LogMessage(
                TargetAudience.OPS,
                LogSeverity.ERROR,
                FP.option( NoArLogMessageFactory.class.getName()+"#opsError()"),
                "opsError() was called",
                FP.emptyMap()
            );

            assertEquals( expectedLogMessage, logger.opsError() );
        }

        @Test
        public void givenOneOpsWarnNoArgMethod_callTheLogMethod_expectLogWritterToBeCalled() {
            NoArLogMessageFactory logger = logFactory.createLogMessageFactory( NoArLogMessageFactory.class );


            LogMessage expectedLogMessage = new LogMessage(
                TargetAudience.OPS,
                LogSeverity.WARN,
                FP.option( NoArLogMessageFactory.class.getName()+"#opsWarn()"),
                "opsWarn() was called",
                FP.emptyMap()
            );

            assertEquals( expectedLogMessage, logger.opsWarn() );
        }

        @Test
        public void givenOneOpsInfoNoArgMethod_callTheLogMethod_expectLogWritterToBeCalled() {
            NoArLogMessageFactory logger = logFactory.createLogMessageFactory( NoArLogMessageFactory.class );


            LogMessage expectedLogMessage = new LogMessage(
                TargetAudience.OPS,
                LogSeverity.INFO,
                FP.option( NoArLogMessageFactory.class.getName()+"#opsInfo()"),
                "opsInfo() was called",
                FP.emptyMap()
            );

            assertEquals( expectedLogMessage, logger.opsInfo() );
        }


        @Test
        public void givenOneDevFatalNoArgMethod_callTheLogMethod_expectLogWritterToBeCalled() {
            NoArLogMessageFactory logger = logFactory.createLogMessageFactory( NoArLogMessageFactory.class );


            LogMessage expectedLogMessage = new LogMessage(
                TargetAudience.DEV,
                LogSeverity.FATAL,
                FP.option( NoArLogMessageFactory.class.getName()+"#devFatal()"),
                "devFatal() was called",
                FP.emptyMap()
            );

            assertEquals( expectedLogMessage, logger.devFatal() );
        }

        @Test
        public void givenOneDevErrorNoArgMethod_callTheLogMethod_expectLogWritterToBeCalled() {
            NoArLogMessageFactory logger = logFactory.createLogMessageFactory( NoArLogMessageFactory.class );


            LogMessage expectedLogMessage = new LogMessage(
                TargetAudience.DEV,
                LogSeverity.ERROR,
                FP.option( NoArLogMessageFactory.class.getName()+"#devError()"),
                "devError() was called",
                FP.emptyMap()
            );

            assertEquals( expectedLogMessage, logger.devError() );
        }

        @Test
        public void givenOneDevWarnNoArgMethod_callTheLogMethod_expectLogWritterToBeCalled() {
            NoArLogMessageFactory logger = logFactory.createLogMessageFactory( NoArLogMessageFactory.class );


            LogMessage expectedLogMessage = new LogMessage(
                TargetAudience.DEV,
                LogSeverity.WARN,
                FP.option( NoArLogMessageFactory.class.getName()+"#devWarn()"),
                "devWarn() was called",
                FP.emptyMap()
            );

            assertEquals( expectedLogMessage, logger.devWarn() );
        }

        @Test
        public void givenOneDevInfoNoArgMethod_callTheLogMethod_expectLogWritterToBeCalled() {
            NoArLogMessageFactory logger = logFactory.createLogMessageFactory( NoArLogMessageFactory.class );


            LogMessage expectedLogMessage = new LogMessage(
                TargetAudience.DEV,
                LogSeverity.INFO,
                FP.option( NoArLogMessageFactory.class.getName()+"#devInfo()"),
                "devInfo() was called",
                FP.emptyMap()
            );

            assertEquals( expectedLogMessage, logger.devInfo() );
        }


        @Test
        public void givenOneUserFatalNoArgMethod_callTheLogMethod_expectLogWritterToBeCalled() {
            NoArLogMessageFactory logger = logFactory.createLogMessageFactory( NoArLogMessageFactory.class );


            LogMessage expectedLogMessage = new LogMessage(
                TargetAudience.USER,
                LogSeverity.FATAL,
                FP.option( NoArLogMessageFactory.class.getName()+"#userFatal()"),
                "userFatal() was called",
                FP.emptyMap()
            );

            assertEquals( expectedLogMessage, logger.userFatal() );
        }

        @Test
        public void givenOneUserErrorNoArgMethod_callTheLogMethod_expectLogWritterToBeCalled() {
            NoArLogMessageFactory logger = logFactory.createLogMessageFactory( NoArLogMessageFactory.class );


            LogMessage expectedLogMessage = new LogMessage(
                TargetAudience.USER,
                LogSeverity.ERROR,
                FP.option( NoArLogMessageFactory.class.getName()+"#userError()"),
                "userError() was called",
                FP.emptyMap()
            );

            assertEquals( expectedLogMessage, logger.userError() );
        }

        @Test
        public void givenOneUserWarnNoArgMethod_callTheLogMethod_expectLogWritterToBeCalled() {
            NoArLogMessageFactory logger = logFactory.createLogMessageFactory( NoArLogMessageFactory.class );


            LogMessage expectedLogMessage = new LogMessage(
                TargetAudience.USER,
                LogSeverity.WARN,
                FP.option( NoArLogMessageFactory.class.getName()+"#userWarn()"),
                "userWarn() was called",
                FP.emptyMap()
            );

            assertEquals( expectedLogMessage, logger.userWarn() );
        }

        @Test
        public void givenOneUserInfoNoArgMethod_callTheLogMethod_expectLogWritterToBeCalled() {
            NoArLogMessageFactory logger = logFactory.createLogMessageFactory( NoArLogMessageFactory.class );


            LogMessage expectedLogMessage = new LogMessage(
                TargetAudience.USER,
                LogSeverity.INFO,
                FP.option( NoArLogMessageFactory.class.getName()+"#userInfo()"),
                "userInfo() was called",
                FP.emptyMap()
            );

            assertEquals( expectedLogMessage, logger.userInfo() );
        }
    }

    @Nested
    public class TemplatedArgTypeTests {
        @Test
        public void booleanParameter() {
            SingleArLogMessageFactory logger = logFactory.createLogMessageFactory( SingleArLogMessageFactory.class );
            logger.booleanArg(true);


            LogMessage expectedLogMessage = new LogMessage(
                TargetAudience.DEV,
                LogSeverity.INFO,
                FP.option( SingleArLogMessageFactory.class.getName()+"#booleanArg(boolean v)"),
                "hello $v",
                FP.toMap("v", true)
            );

            assertEquals( expectedLogMessage, logger.booleanArg(true) );
        }

        @Test
        public void charParameter() {
            SingleArLogMessageFactory logger = logFactory.createLogMessageFactory( SingleArLogMessageFactory.class );
            logger.charArg('a');


            LogMessage expectedLogMessage = new LogMessage(
                TargetAudience.DEV,
                LogSeverity.INFO,
                FP.option( SingleArLogMessageFactory.class.getName()+"#charArg(char v)"),
                "hello $v",
                FP.toMap("v", 'a')
            );

            assertEquals( expectedLogMessage, logger.charArg('a') );
        }

        @Test
        public void booleanObjParameter() {
            SingleArLogMessageFactory logger = logFactory.createLogMessageFactory( SingleArLogMessageFactory.class );


            LogMessage expectedLogMessage = new LogMessage(
                TargetAudience.DEV,
                LogSeverity.INFO,
                FP.option( SingleArLogMessageFactory.class.getName()+"#booleanObjArg(java.lang.Boolean v)"),
                "hello $v",
                FP.toMap("v", true)
            );

            assertEquals( expectedLogMessage, logger.booleanObjArg(true) );
        }

        @Test
        public void nullBooleanObjParameter() {
            SingleArLogMessageFactory logger = logFactory.createLogMessageFactory( SingleArLogMessageFactory.class );


            LogMessage expectedLogMessage = new LogMessage(
                TargetAudience.DEV,
                LogSeverity.INFO,
                FP.option( SingleArLogMessageFactory.class.getName()+"#booleanObjArg(java.lang.Boolean v)"),
                "hello $v",
                FP.toMap("v", null)
            );

            assertEquals( expectedLogMessage, logger.booleanObjArg(null) );
        }

        @Test
        public void multiArgMethod() {
            MultiArLogMessageFactory logger = logFactory.createLogMessageFactory( MultiArLogMessageFactory.class );
            logger.multiArgEvent(false, "James");


            LogMessage expectedLogMessage = new LogMessage(
                TargetAudience.DEV,
                LogSeverity.INFO,
                FP.option( MultiArLogMessageFactory.class.getName()+"#multiArgEvent(boolean flag, java.lang.String name)"),
                "hello $name ($flag)",
                FP.toMap(
                    "name", "James",
                    "flag", false
                )
            );

            assertEquals( expectedLogMessage, logger.multiArgEvent(false,"James") );
        }

        @Test
        public void ensureThatSecretsAreNotCapturedByLogging() {
            MultiArLogMessageFactory logger = logFactory.createLogMessageFactory( MultiArLogMessageFactory.class );


            LogMessage expectedLogMessage = new LogMessage(
                TargetAudience.DEV,
                LogSeverity.INFO,
                FP.option( MultiArLogMessageFactory.class.getName()+"#login(java.lang.String name, mosaics.lang.Secret password)"),
                "hello $name, your password is $password",
                FP.toMap(
                    "name", "James",
                    "password", "********" // notice the change in type and value here
                )
            );

            assertEquals( expectedLogMessage, logger.login("James", new Secret("12345")) );
        }
    }



    public static interface MissingAnnotationLogMessageFactory extends LogMessageFactory {
        @SuppressWarnings("unused")
        public LogMessage a();
    }

    public static interface VoidReturnTypLogMessageFactory extends LogMessageFactory {
        @SuppressWarnings("unused")
        @AuditError("auditError() was called")
        public void a();
    }

    public static interface UseDefaultMethoLogMessageFactory extends LogMessageFactory {
        @SuppressWarnings("unused")
        public default LogMessage a() {
            return b("Jim");
        }

        @AuditError("hello $name")
        public LogMessage b(String name);
    }

    public static interface NoArLogMessageFactory extends LogMessageFactory {
        @AuditError("auditError() was called")
        public LogMessage auditError();

        @AuditFatal("auditFatal() was called")
        public LogMessage auditFatal();

        @AuditWarn("auditWarn() was called")
        public LogMessage auditWarn();

        @AuditInfo("auditInfo() was called")
        public LogMessage auditInfo();


        @OpsError("opsError() was called")
        public LogMessage opsError();

        @OpsFatal("opsFatal() was called")
        public LogMessage opsFatal();

        @OpsWarn("opsWarn() was called")
        public LogMessage opsWarn();

        @OpsInfo("opsInfo() was called")
        public LogMessage opsInfo();


        @DevError("devError() was called")
        public LogMessage devError();

        @DevFatal("devFatal() was called")
        public LogMessage devFatal();

        @DevWarn("devWarn() was called")
        public LogMessage devWarn();

        @DevInfo("devInfo() was called")
        public LogMessage devInfo();


        @UserError("userError() was called")
        public LogMessage userError();

        @UserFatal("userFatal() was called")
        public LogMessage userFatal();

        @UserWarn("userWarn() was called")
        public LogMessage userWarn();

        @UserInfo("userInfo() was called")
        public LogMessage userInfo();
    }

    public static interface SingleArLogMessageFactory extends LogMessageFactory {
        @DevInfo("hello $v")
        public LogMessage booleanArg(boolean v);

        @DevInfo("hello $v")
        public LogMessage charArg(char v);

        @DevInfo("hello $v")
        public LogMessage booleanObjArg(Boolean v);
    }

    public static interface MultiArLogMessageFactory extends LogMessageFactory {
        @DevInfo("hello $name ($flag)")
        public LogMessage multiArgEvent(boolean flag, String name);

        @DevInfo("hello $name, your password is $password")
        public LogMessage login( String name, Secret password );
    }


    public static interface MalformedTemplateLogMessageFactory extends LogMessageFactory {
        @DevInfo("hello ${name")
        @SuppressWarnings("unused")
        public LogMessage malformedTemplate(boolean flag, String name);
    }

    public static interface TemplateReferencesMissingParameterByNamLogMessageFactory extends LogMessageFactory {
        @DevInfo("hello ${foo}")
        @SuppressWarnings("unused")
        public LogMessage malformedTemplate(boolean flag, String name);
    }


    @Value
    public static class RequestId {
        @SuppressWarnings("RedundantModifiersValueLombok")
        private String id;
    }
}
