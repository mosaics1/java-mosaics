package mosaics.logging.loglevels;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * Mark an @Events method as being targeted as a 'major incident' that requires attention by people
 * who are responsible for the 'business process/operation' of the system;  possibly for security
 * or regulatory purposes.   For example a confirmed breach of protocol has been detected that
 * risks the companies relationship of a customer or customers has been found and requires
 * immediate attention.<p/>
 *
 * Audit events differ in severity based on the impact of the event.  Business as usual events
 * are recorded as 'informative', potential breaches or suspicious usage which should be
 * marked for later investigation are marked as warnings.  Errors suggest that a usage situation
 * has occurred that was previously considered as not possible or a problem that would need
 * to be addressed has occurred and the fatal severity is used to mark serious breaches that risk
 * the companies relationship with a customer or customers and require immediate attention.<p/>
 *
 * Audit events target people who are concerned with the "business usage" of a system, where as
 * Op events target people who are responsible for the infrastructure that is running the system.
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface AuditFatal {
    /**
     * An optional template used to generate descriptions of the event.  The template follows the
     * format rules of TextTemplate;  which uses $parameterName to represent placeholders.
     *
     * For example, an event that has a parameter called 'userName' could have the following
     * template:
     *
     * <pre>
     *     "'$userName' logged in'
     * </pre>
     *
     * @See com.softwaremosaic.strings.templates.TextTemplate
     */
    public String value() default "";
}
