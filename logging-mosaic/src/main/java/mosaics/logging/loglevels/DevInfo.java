package mosaics.logging.loglevels;


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * Mark an @Events method as being targeted as 'informative' for 'developers'.  These messages
 * provide extra context useful for developers to diagnose issues with a running system.  Usually
 * these events will be disabled as their inclusion would harm the operation of the system in some
 * way, such as being too resource intensive;  however their inclusion can be requested dynamically
 * at runtime as part of an investigation.
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface DevInfo {
    /**
     * An optional template used to generate descriptions of the event.  The template follows the
     * format rules of TextTemplate;  which uses $parameterName to represent placeholders.
     *
     * For example, an event that has a parameter called 'userName' could have the following
     * template:
     *
     * <pre>
     *     "'$userName' logged in'
     * </pre>
     *
     * @See com.softwaremosaic.strings.templates.TextTemplate
     */
    public String value() default "";
}
