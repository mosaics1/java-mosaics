package mosaics.logging.loglevels;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * Mark an @Events method as reporting an unrecoverable error for the attention of a 'user'
 * of the system.  An event with the severity level of 'fatal' differs from one with the level of
 * 'error' because it is expected that a user will not be able to take a corrective action of
 * some kind.
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface UserFatal {
    /**
     * An optional template used to generate descriptions of the event.  The template follows the
     * format rules of TextTemplate;  which uses $parameterName to represent placeholders.
     *
     * For example, an event that has a parameter called 'userName' could have the following
     * template:
     *
     * <pre>
     *     "'$userName' logged in'
     * </pre>
     *
     * @See com.softwaremosaic.strings.templates.TextTemplate
     */
    public String value() default "";
}
