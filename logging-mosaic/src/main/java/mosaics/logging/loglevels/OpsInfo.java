package mosaics.logging.loglevels;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * Mark an @Events method as being targeted as 'informative' for 'staff involved with the day to
 * day running of the system infrastructure'.  For example, information that confirms that the system is healthy
 * and what it is currently doing at a hardware/network resource level such as network requests and
 * usage metrics.<p/>
 *
 * Audit events target people who are concerned with the "business usage" of a system, where as
 * Op events target people who are responsible for the infrastructure that is running the system.
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface OpsInfo {
    /**
     * An optional template used to generate descriptions of the event.  The template follows the
     * format rules of TextTemplate;  which uses $parameterName to represent placeholders.
     *
     * For example, an event that has a parameter called 'userName' could have the following
     * template:
     *
     * <pre>
     *     "'$userName' logged in'
     * </pre>
     *
     * @See com.softwaremosaic.strings.templates.TextTemplate
     */
    public String value();
}
