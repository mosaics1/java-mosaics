package mosaics.logging.loglevels;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * Mark an @Events method as being targeted as 'problem' that requires attention asap but if it
 * occurs out of normal work hours then it is not considered severe enough to wake people up for.
 * For example, one of three machines has failed and the other two are covering the first failure.
 * Because there is sufficient redundancy in the system then users have not been impacted yet but
 * operational intervention will be required soon to recover the failed node and bring the system
 * back into a fully operational state.<p/>
 *
 * OpWarn, OpError and OpFatal differ in how severe the impact of the issue is right now and who
 * needs to take action and when.  Warnings are not time critical (yet) and can usually wait a bit, but
 * they do have the potential to grow into major incidents if they are not "nipped in the bud".
 * Errors are events that are impacting parts of the system now but the system is self managing them
 * and so long as the incidents get prioritised soon then there is no need to wake support staff at
 * 2am in the morning.  Fatal events are major fires that are in progress now, and require first
 * responders to stop what ever they are doing and prioritise this incident now;  even if they
 * are asleep and even if it is 2am on Christmas day and it is snowing outside.<p/>
 *
 * Audit events target people who are concerned with the "business usage" of a system, where as
 * Op events target people who are responsible for the infrastructure that is running the system.
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface OpsError {
    /**
     * An optional template used to generate descriptions of the event.  The template follows the
     * format rules of TextTemplate;  which uses $parameterName to represent placeholders.
     *
     * For example, an event that has a parameter called 'userName' could have the following
     * template:
     *
     * <pre>
     *     "'$userName' logged in'
     * </pre>
     *
     * @See com.softwaremosaic.strings.templates.TextTemplate
     */
    public String value() default "";
}
