package mosaics.logging.loglevels;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * Mark an @Events method as being targeted as a 'potential problem' to be picked up by 'developers'.
 *
 *
 * Typically this will be a soft constraint that the developers did not expect to be violated
 * or have put off extra development of this part of the system while they focused on another
 * part of the solution until such time as the use of the system has grown to a sufficient point
 * where they need to reallocate their priorities.  For example, a system designed to cope with up
 * to 100 msg per second is unlikely to die when it hits 102 messages per second;  however by marking
 * such an event as a developer warning then it flags to developers that they should review this part of the
 * system and plan any future actions accordingly.
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface DevWarn {
    /**
     * An optional template used to generate descriptions of the event.  The template follows the
     * format rules of TextTemplate;  which uses $parameterName to represent placeholders.
     *
     * For example, an event that has a parameter called 'userName' could have the following
     * template:
     *
     * <pre>
     *     "'$userName' logged in'
     * </pre>
     *
     * @See com.softwaremosaic.strings.templates.TextTemplate
     */
    public String value() default "";
}
