package mosaics.logging.loglevels;

import lombok.Value;
import mosaics.fp.FP;
import mosaics.fp.collections.FPIterable;
import mosaics.fp.collections.maps.FPMap;
import mosaics.fp.collections.FPOption;
import mosaics.lang.Backdoor;
import mosaics.logging.LogSeverity;
import mosaics.logging.TargetAudience;

import java.lang.annotation.Annotation;
import java.util.function.Function;

import static mosaics.logging.LogSeverity.ERROR;
import static mosaics.logging.LogSeverity.FATAL;
import static mosaics.logging.LogSeverity.INFO;
import static mosaics.logging.LogSeverity.WARN;
import static mosaics.logging.TargetAudience.AUDIT;
import static mosaics.logging.TargetAudience.DEV;
import static mosaics.logging.TargetAudience.OPS;
import static mosaics.logging.TargetAudience.USER;


/**
 * Log annotation handling.  The Java annotation type system does not support interfaces or inheritance,
 * so this class is standing in as a place to work with the log annotations.
 */
public class LogAnnotationUtils {
    private static final FPMap<Class<? extends Annotation>, LogAnnotation> LOG_ANNOTATIONS = FP.toMap(
        AuditFatal.class, new LogAnnotationHandler<>(AUDIT, FATAL, AuditFatal::value),
        AuditError.class, new LogAnnotationHandler<>(AUDIT, ERROR, AuditError::value),
        AuditWarn.class,  new LogAnnotationHandler<>(AUDIT, WARN,  AuditWarn::value),
        AuditInfo.class,  new LogAnnotationHandler<>(AUDIT, INFO,  AuditInfo::value),
        DevFatal.class,   new LogAnnotationHandler<>(DEV,   FATAL, DevFatal::value),
        DevError.class,   new LogAnnotationHandler<>(DEV,   ERROR, DevError::value),
        DevWarn.class,    new LogAnnotationHandler<>(DEV,   WARN,  DevWarn::value),
        DevInfo.class,    new LogAnnotationHandler<>(DEV,   INFO,  DevInfo::value),
        OpsFatal.class,   new LogAnnotationHandler<>(OPS,   FATAL, OpsFatal::value),
        OpsError.class,   new LogAnnotationHandler<>(OPS,   ERROR, OpsError::value),
        OpsWarn.class,    new LogAnnotationHandler<>(OPS,   WARN,  OpsWarn::value),
        OpsInfo.class,    new LogAnnotationHandler<>(OPS,   INFO,  OpsInfo::value),
        UserFatal.class,  new LogAnnotationHandler<>(USER,  FATAL, UserFatal::value),
        UserError.class,  new LogAnnotationHandler<>(USER,  ERROR, UserError::value),
        UserWarn.class,   new LogAnnotationHandler<>(USER,  WARN,  UserWarn::value),
        UserInfo.class,   new LogAnnotationHandler<>(USER,  INFO,  UserInfo::value)
    ) ;

    public static <A extends Annotation> boolean isLogAnnotation( A annotation ) {
        return LOG_ANNOTATIONS.containsKey( annotation.annotationType() );
    }

    public static <A extends Annotation> LogAnnotation toLogAnnotation( A annotation ) {
        LogAnnotationHandler<A> h = Backdoor.cast(LOG_ANNOTATIONS.get(annotation.annotationType()).get());
        
        return new LogAnnotation( h.getTargetAudience(), h.getLogSeverity(), h.getTemplateExtractor().apply(annotation) );
    }

    public static FPOption<LogAnnotation> findLogAnnotationFrom( FPIterable<Annotation> annotations ) {
        return annotations
            .first(LogAnnotationUtils::isLogAnnotation)
            .map(LogAnnotationUtils::toLogAnnotation);
    }


    @Value
    private static class LogAnnotationHandler<T extends Annotation> {
        private TargetAudience      targetAudience;
        private LogSeverity         logSeverity;
        private Function<T, String> templateExtractor;
    }
}
