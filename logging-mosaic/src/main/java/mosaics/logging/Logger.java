package mosaics.logging;

import java.util.Arrays;


public interface Logger {
    public void log( LogMessage msg );


    public default void logAll( LogMessage...msgs ) {
        Arrays.stream( msgs ).forEach( this::log );
    }

    public default void logAll( Iterable<LogMessage> msgs ) {
        msgs.forEach( this::log );
    }

    
// DEV    
    public default void logToDevInfo( String msg ) {
        log( new LogMessage(TargetAudience.DEV, LogSeverity.INFO, msg) );
    }

    public default void logToDevWarn( String msg ) {
        log( new LogMessage(TargetAudience.DEV, LogSeverity.WARN, msg) );
    }

    public default void logToDevError( String msg ) {
        log( new LogMessage(TargetAudience.DEV, LogSeverity.ERROR, msg) );
    }

    public default void logToDevFatal( String msg ) {
        log( new LogMessage(TargetAudience.DEV, LogSeverity.FATAL, msg) );
    }
    
// OPS    
    public default void logToOpsInfo( String msg ) {
        log( new LogMessage(TargetAudience.OPS, LogSeverity.INFO, msg) );
    }

    public default void logToOpsWarn( String msg ) {
        log( new LogMessage(TargetAudience.OPS, LogSeverity.WARN, msg) );
    }

    public default void logToOpsError( String msg ) {
        log( new LogMessage(TargetAudience.OPS, LogSeverity.ERROR, msg) );
    }

    public default void logToOpsFatal( String msg ) {
        log( new LogMessage(TargetAudience.OPS, LogSeverity.FATAL, msg) );
    }
    
// USER    
    public default void logToUserInfo( String msg ) {
        log( new LogMessage(TargetAudience.USER, LogSeverity.INFO, msg) );
    }

    public default void logToUserWarn( String msg ) {
        log( new LogMessage(TargetAudience.USER, LogSeverity.WARN, msg) );
    }

    public default void logToUserError( String msg ) {
        log( new LogMessage(TargetAudience.USER, LogSeverity.ERROR, msg) );
    }

    public default void logToUserFatal( String msg ) {
        log( new LogMessage(TargetAudience.USER, LogSeverity.FATAL, msg) );
    }
    
// AUDIT    
    public default void logToAuditInfo( String msg ) {
        log( new LogMessage(TargetAudience.AUDIT, LogSeverity.INFO, msg) );
    }

    public default void logToAuditWarn( String msg ) {
        log( new LogMessage(TargetAudience.AUDIT, LogSeverity.WARN, msg) );
    }

    public default void logToAuditError( String msg ) {
        log( new LogMessage(TargetAudience.AUDIT, LogSeverity.ERROR, msg) );
    }

    public default void logToAuditFatal( String msg ) {
        log( new LogMessage(TargetAudience.AUDIT, LogSeverity.FATAL, msg) );
    }
}
