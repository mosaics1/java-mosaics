package mosaics.logging;

/**
 * How critical is the messages call to action right now?  Ranging from INFO (no action required)
 * to FATAL (now now now!).
 */
public enum LogSeverity {
    /**
     * INFO messages are typically looked at on demand when somebody has a query for what is going on.
     */
    INFO,

    /**
     * WARN messages ask the target audience to be alert/consider an issue that might grow into an
     * incident if ignored.
     */
    WARN,

    /**
     * ERROR means that an incident has occurred and is in progress.  However the incident is
     * localised, recoverable without intervention, has an acceptable impact and/or can be
     * left overnight if it occurred out of business hours.
     */
    ERROR,

    /**
     * FATAL errors mean that the system cannot perform its function for a significant portion of
     * the user base and needs urgent attention right now.  Get out of bed dudes, the fire is
     * on your door step right now.
     */
    FATAL;

    public boolean isInfo() {
        return this == INFO;
    }

    public boolean isWarn() {
        return this == WARN;
    }

    public boolean isError() {
        return this == ERROR;
    }

    public boolean isFatal() {
        return this == FATAL;
    }


    public boolean isGTE( LogSeverity watermark ) {
        return this.ordinal() >= watermark.ordinal();
    }
}
