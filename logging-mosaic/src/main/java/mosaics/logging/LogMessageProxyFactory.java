package mosaics.logging;

import mosaics.fp.FP;
import mosaics.fp.collections.FPIterable;
import mosaics.fp.collections.FPList;
import mosaics.fp.collections.FPOption;
import mosaics.fp.collections.FPSet;
import mosaics.lang.Backdoor;
import mosaics.lang.QA;
import mosaics.lang.Secret;
import mosaics.lang.functions.Function1;
import mosaics.lang.reflection.JavaClass;
import mosaics.lang.reflection.JavaMethod;
import mosaics.lang.reflection.JavaParameter;
import mosaics.lang.reflection.MissingPropertyException;
import mosaics.logging.loglevels.LogAnnotation;
import mosaics.logging.loglevels.LogAnnotationUtils;
import mosaics.strings.parser.ParseException;
import mosaics.strings.templates.TextTemplate;
import mosaics.strings.templates.TextTemplateParser;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * Creates LogMessage factory methods from interfaces.
 *
 * @see LogMessageFactory
 */
public class LogMessageProxyFactory {
    public static final LogMessageProxyFactory INSTANCE = new LogMessageProxyFactory();

    private final Map<Class, LogMessageFactory> cachedLoggers = new ConcurrentHashMap<>();


    @SuppressWarnings("unchecked")
    public <L extends LogMessageFactory> L createLogMessageFactory(Class<L> loggerInterface) {
        QA.argNotNull( loggerInterface, "loggerInterface" );

        return Backdoor.cast( cachedLoggers.computeIfAbsent(loggerInterface,this::createReflectiveLoggerFor) );
    }


    private <T extends LogMessageFactory> T createReflectiveLoggerFor( Class<T> loggerClass ) {
        LoggerInvocationHandler handler = new LoggerInvocationHandler(loggerClass);
        Object                  proxy   = Proxy.newProxyInstance( loggerClass.getClassLoader(), new Class[]{loggerClass}, handler );

        return Backdoor.cast( proxy );
    }


    private static class LoggerInvocationHandler implements InvocationHandler {
        private final Map<String, Function1<Object[],LogMessage>> methodHandlers;

        public LoggerInvocationHandler( Class<? extends LogMessageFactory> loggerClass ) {
            methodHandlers = createMethodHandlers( JavaClass.of(loggerClass) );
        }

        public Object invoke( Object proxy, Method method, Object[] args ) {
            Function1<Object[],LogMessage> logger = methodHandlers.get(JavaMethod.of(method).getShortSignature());

            if ( logger != null ) {
                return logger.invoke( args );
            }

            return null;
        }


        private static Map<String, Function1<Object[],LogMessage>> createMethodHandlers( JavaClass loggerClass ) {
            return loggerClass.getAllMethods()
//                .filter( LoggerInvocationHandler::hasLogAnnotation )
                .toJdkMap( JavaMethod::getShortSignature, LoggerInvocationHandler::createMethodHandlerFor );
        }

        private static boolean hasLogAnnotation( JavaMethod javaMethod ) {
            return javaMethod.getAnnotations().hasFirst( LogAnnotationUtils::isLogAnnotation);
        }

        private static final JavaClass LOGMESSAGE_JC = JavaClass.of(LogMessage.class);

        private static Function1<Object[],LogMessage> createMethodHandlerFor( JavaMethod method ) {
            if ( !method.getReturnType().equals(LOGMESSAGE_JC) ) {
                throw new IllegalArgumentException(method.getFullSignature()+" must return LogMessage (it returns void)");
            } else if ( method.isDefault() ) {
                throw new IllegalArgumentException(method.getFullSignature()+" is declared default (default methods are not supported)");
            }

            LogAnnotation    logAnnotation  = getMandatoryLogAnnotationFor( method );
            FPOption<String> messageType    = FP.option(method.getFullSignature());
            FPList<String>   parameterNames = method.getParameters().map(JavaParameter::getName).toFPList();
            String           template       = logAnnotation.getTemplate();

            verifyTemplate(messageType.get(), template, method.getParameters());

            return args -> {
                return new LogMessage(
                    logAnnotation.getTargetAudience(),
                    logAnnotation.getSeverity(),
                    messageType,
                    template,
                    parameterNames.zip(
                        FP.wrapArray(args).map( LoggerInvocationHandler::convertArgsForLogging)
                    ).toFPMap()
                );
            };
        }

        private static LogAnnotation getMandatoryLogAnnotationFor( JavaMethod method ) {
            return LogAnnotationUtils.findLogAnnotationFrom( method.getAnnotations() ).orElseThrow(
                () -> new IllegalArgumentException(method.getFullSignature()+" is missing the required log level annotation")
            );
        }

        private static Object convertArgsForLogging( Object o ) {
            if ( o instanceof Secret ) {
                return "********";
            } else {
                return o;
            }
        }

        private static void verifyTemplate( String templateName, String templateText, FPIterable<JavaParameter> parameters ) {
            TextTemplateParser parser = new TextTemplateParser();

            try {
                TextTemplate template = parser.parse( templateName, templateText );

                verifyTemplateProperties( templateName, template, parameters );
            } catch ( IllegalStateException | ParseException ex ) {
                throw new MalformedTemplateException(templateName, templateText, ex);
            }
        }

        private static void verifyTemplateProperties( String templateName, TextTemplate template, FPIterable<JavaParameter> parameters ) {
            FPSet<String> templatePropertyNames           = FPSet.wrapSet(template.getTemplatePropertyNames());
            FPSet<String> providedPropertyNames           = parameters.map(JavaParameter::getName).toSetFP();

            FPSet<String> requiredButMissingPropertyNames = templatePropertyNames.removeAll(providedPropertyNames);

            if ( requiredButMissingPropertyNames.hasContents() ) {
                throw new MissingPropertyException( templateName, requiredButMissingPropertyNames.mkString(", ") );
            }
        }
    }
}
