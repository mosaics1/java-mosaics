package mosaics.logging;

/**
 * Who is the log entry intended for?
 */
public enum TargetAudience {
    /**
     * Developer messages are intended for developers to use during development.  They are usually
     * disabled as standard.
     */
    DEV,

    /**
     * The OPS messages are used to inform the people who look after the day to day running
     * of the system.  The messages are used to monitor the health of the system.  INFO messages
     * confirm that the system is healthy, confirm how it is configured and what the system is
     * doing.  WARN messages are low priority, non time critical alerts that should be addressed
     * before they become user visible problems, ERROR messages require attention soon but do not
     * require waking support staff in the early hours of the morning to address.  FATAL messages
     * mean that the system is broken for a significant portion of the user base, and acts as a
     * call to action for support staff to drop what ever they are doing and to get on to this
     * problem now.
     */
    OPS,

    /**
     * USER level messages are intended to be seen by the users of the system.  USER messages are
     * usually used in command line apps.  In which case INFO is sent to
     * stdout and WARN/ERROR/FATAL is sent to stderr.  Server applications typically have little
     * use for this channel.
     */
    USER,

    /**
     * An audit of what the system and or users have been doing.  Collected to help understand
     * the system and are stored possibly for security, and regulatory purposes.  They track
     * input/output, requests, remote calls etc.
     */
    AUDIT;


    public boolean isDev() {
        return this == DEV;
    }

    public boolean isOps() {
        return this == OPS;
    }

    public boolean isUser() {
        return this == USER;
    }

    public boolean isAudit() {
        return this == AUDIT;
    }


    public boolean isGTE( TargetAudience watermark ) {
        return this.ordinal() >= watermark.ordinal();
    }
}
