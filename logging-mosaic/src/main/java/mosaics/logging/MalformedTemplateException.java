package mosaics.logging;

public class MalformedTemplateException extends RuntimeException {
    public MalformedTemplateException( String templateName, String templateText, Throwable ex ) {
        super("Failed to parse template for "+templateName+": '" + templateText+"'", ex);
    }
}
