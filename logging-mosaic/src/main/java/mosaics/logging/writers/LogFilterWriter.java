package mosaics.logging.writers;

import mosaics.io.resources.MimeType;
import mosaics.logging.LogFilter;
import mosaics.logging.LogMessage;


public class LogFilterWriter implements LogWriter {
    private final LogFilter logFilter;
    private final LogWriter wrappedLogWriter;

    public LogFilterWriter( LogFilter logFilter, LogWriter wrappedLogWriter ) {
        this.logFilter        = logFilter;
        this.wrappedLogWriter = wrappedLogWriter;
    }

    public MimeType getMimeType() {
        return wrappedLogWriter.getMimeType();
    }

    public void write( LogMessage msg ) {
        if ( !logFilter.accept(msg) ) {
            return;
        }

        wrappedLogWriter.write( msg );
    }
}
