package mosaics.logging.writers;

import mosaics.logging.LogFilter;


public class LogWriters {

    public static LogWriter logFilter( LogFilter logFilter, LogWriter wrappedWriter ) {
        return new LogFilterWriter( logFilter, wrappedWriter );
    }

    public static LogWriter textLogWriter( Appendable out, boolean incStackTraces ) {
        return new TextLogWriter( out, incStackTraces );
    }

    public static LogWriter jsonLogWriter( Appendable out ) {
        return new JsonLogWriter( out );
    }

    public static LogWriter nullLogWriter() {
        return NullLogWriter.INSTANCE;
    }

    public static CapturingLogWriter capturingLogWriter() {
        return new CapturingLogWriter();
    }

}
