package mosaics.logging.writers;

import mosaics.io.resources.MimeType;
import mosaics.logging.LogMessage;


public class NullLogWriter implements LogWriter {
    public static final LogWriter INSTANCE = new NullLogWriter();

    public MimeType getMimeType() {
        return MimeType.TEXT;
    }

    public void write( LogMessage msg ) {}
}
