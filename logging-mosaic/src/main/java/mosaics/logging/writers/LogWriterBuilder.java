package mosaics.logging.writers;

import mosaics.io.resources.MimeType;
import mosaics.logging.LogFilter;


public class LogWriterBuilder {
    private MimeType  mimeType  = MimeType.TEXT;
    private LogFilter logFilter = LogFilter.NORMAL;


    public LogWriterBuilder withMimeType( MimeType mimeType ) {
        this.mimeType = mimeType;

        return this;
    }

    public LogWriterBuilder withLogFilter( LogFilter logFilter ) {
        this.logFilter = logFilter;

        return this;
    }

    public LogWriter create( Appendable out ) {
        LogWriter logWriter = createBaseLogWriter( out );

        return wrapWithOptionalLogFilter(logWriter);
    }

    private LogWriter wrapWithOptionalLogFilter( LogWriter baseLogWriter ) {
        return logFilter == LogFilter.DEBUG ? baseLogWriter : new LogFilterWriter(logFilter,baseLogWriter);
    }

    private LogWriter createBaseLogWriter( Appendable out ) {
        if ( MimeType.TEXT.equals(mimeType) ) {
            return LogWriters.textLogWriter( out, true );
        } else if ( MimeType.JSON.equals(mimeType) ) {
            return LogWriters.jsonLogWriter( out );
        }

        throw new IllegalArgumentException( "Unsupported mimetype: " + mimeType );
    }
}
