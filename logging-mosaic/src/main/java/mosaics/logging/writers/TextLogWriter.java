package mosaics.logging.writers;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.With;
import mosaics.cache.PermCache;
import mosaics.fp.FP;
import mosaics.fp.collections.maps.FPMap;
import mosaics.io.writers.AppendableWriterAdapter;
import mosaics.io.resources.MimeType;
import mosaics.logging.LogMessage;
import mosaics.strings.templates.TextTemplate;
import mosaics.strings.templates.TextTemplateParser;

import java.io.PrintWriter;


/**
 * Writes output as:
 *
 * <pre>
 *     [2020-03-10 10:30:11 DEVINFO]: hello world
 * </pre>
 */
@AllArgsConstructor
public class TextLogWriter implements LogWriter {
    public static String toString( LogMessage logMessage ) {
        StringBuilder buf           = new StringBuilder();
        TextLogWriter textLogWriter = new TextLogWriter( buf ).withMessageTemplatePrefix( "" );

        textLogWriter.write( logMessage );

        return buf.toString().trim();
    }

    public static final String DEFAULT_MESSAGE_TEMPLATE_PREFIX = "\\[${when} ${targetAudience}${severity} ${requestId=-}]: ";

    @With private final String             messageTemplatePrefix;
          private final TextTemplateParser templateParser = new TextTemplateParser().withCache( new PermCache<>() );
          private final Appendable         out;
    @With private final boolean            incStackTraces;

    public TextLogWriter( Appendable out ) {
        this( out, false );
    }

    public TextLogWriter( Appendable out, boolean incStackTraces ) {
        this( DEFAULT_MESSAGE_TEMPLATE_PREFIX, out, incStackTraces );
    }

    public MimeType getMimeType() {
        return MimeType.TEXT;
    }

    @SneakyThrows
    public void write( LogMessage msg ) {
        String               templateText = msg.getMessageTemplate();
        String               messageKey   = msg.getMessageType().orElse( templateText );
        FPMap<String,Object> properties   = msg.getProperties();
        TextTemplate         template     = templateParser.parse( messageKey, messageTemplatePrefix+templateText );

        template.invoke( out, key -> {
            return switch (key) {
                case "when"           -> FP.option(msg.getWhen());
                case "messageType"    -> FP.option(msg.getMessageType());
                case "requestId"      -> FP.option(msg.getRequestId());
                case "severity"       -> FP.option(msg.getSeverity());
                case "targetAudience" -> FP.option(msg.getTargetAudience());
                default               -> properties.get( key );
            };
        } );

        out.append( "\n" );

        writeProperties( msg.getProperties() );
    }

    private void writeProperties( FPMap<String, Object> properties ) {
        properties.forEach( this::printProperty );
    }

    private void printProperty( String key, Object value ) {
        if ( value instanceof Throwable ex ) {
            if ( incStackTraces ) {
                ex.printStackTrace( new PrintWriter( new AppendableWriterAdapter( out ) ) );
            }
        }
    }
}
