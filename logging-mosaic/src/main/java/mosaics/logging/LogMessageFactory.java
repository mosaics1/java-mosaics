package mosaics.logging;

/**
 * A marker interface for logs.
 *
 * Example:
 * <pre>
 * public interface ApplicationLogFactory extends LogMessageFactory {
 *     private static final ApplicationLogFactory APPLICATION_LOGFACTORY = LogMessageProxyFactory.INSTANCE.createLogMessageFactory(ApplicationLogFactory.class);
 *
 *     @OpsInfo("Application started");
 *     public LogMessage appStarted();
 *
 *     @AuditInfo("Request received: $method $endpoint")
 *     public LogMessage requestMade(String method, String endpoint);
 * }
 * </pre>
 *
 * Now, when ever you invoke methods on ApplicationLogFactory.INSTANCE, they will return instances
 * of LogMessage that are ready to be written to instances of LogWriter.
 */
public interface LogMessageFactory extends Cloneable {

}
