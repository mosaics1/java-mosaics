plugins {
    id("myproject.java-conventions")
}

dependencies {
    api(project(":logging-mosaic"))

    api("io.reactivex.rxjava2:rxjava:2.2.21")

    testImplementation("org.mock-server:mockserver-netty:5.11.2")
}

