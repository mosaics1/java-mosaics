package mosaics.cli.runner;

import mosaics.fp.FP;
import mosaics.fp.collections.FPIterable;
import mosaics.fp.collections.FPQueue;

import java.util.ArrayList;
import java.util.List;


/**
 * Parses the values passed into a main() method into KV objects.
 *
 * The following forms are supported:
 *
 *  -f            ShortNameFlag
 *  --long        LongNameFlag
 *  --long=abc    LongNameKV
 *  abc           ValueOnly
 *  -lsh          ShortNamesConcatenatedTogether
 */
public class MainArgsParser {

    public static FPIterable<KVPair> parse( String...args ) {
        return parse( new FPQueue<>(args) );
    }

    private static FPIterable<KVPair> parse( FPQueue<String> queue ) {
        List<KVPair> pairs = new ArrayList<>();

        while ( queue.hasContents() ) {
            KVPair kv = parseNextKVFrom( queue );

            pairs.add( kv );
        }

        return FP.wrap(pairs);
    }

    private static KVPair parseNextKVFrom( FPQueue<String> queue ) {
        String first = queue.pop();

        String key = extractKeyFrom(first);
        String value;

        if ( key == null ) {
            value = first;
        } else if ( key.contains("=") ) {
            String kv = key;
            int    i  = kv.indexOf('=');

            key   = kv.substring( 0, i );
            value = kv.substring( i+1 );
        } else if ( !first.startsWith("--") && first.startsWith("-") && key.length() > 1 ) { // multiple short names in one
            for ( char letter : key.toCharArray() ) {
                queue.push( "-" + letter );
            }

            return parseNextKVFrom( queue );
        } else {
            value = "true";
        }

        return new KVPair( key, value );
    }

    private static String extractKeyFrom( String str ) {
        if ( str.startsWith("--") ) {
            return str.substring(2);
        } else if ( str.startsWith("-") ) {
            return str.substring(1);
        } else {
            return null;
        }
    }

}
