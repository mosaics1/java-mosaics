package mosaics.cli.runner;

import lombok.Value;


@Value
public class AppResult {
    public static final int SUCCESS              = 0;
    public static final int USER_ERROR           = 1;
    public static final int MISSING_CONFIG_ERROR = 2;

    public static final int EXCEPTION_THROWN     = 10;
    public static final int NO_RUN_METHOD_FOUND  = 11;


    public static AppResult noValidRunMethods() {
        return create( NO_RUN_METHOD_FOUND );
    }


    public static AppResult create( int exitCode ) {
        return new AppResult( exitCode );
    }

    public static AppResult success() {
        return create( SUCCESS );
    }


    private int exitCode;

    public boolean isSuccess() {
        return exitCode == SUCCESS;
    }
}
