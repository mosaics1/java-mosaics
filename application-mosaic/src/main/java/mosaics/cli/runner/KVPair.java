package mosaics.cli.runner;

import lombok.Value;

import java.util.Map;


@Value
public class KVPair {

    private String key;
    private String value;


    public KVPair( Map.Entry<String,String> entry ) {
        this( entry.getKey(), entry.getValue() );
    }

    public KVPair( String key ) {
        this( key, null );
    }

    public KVPair( String key, String value ) {
        this.key   = key;
        this.value = value;
    }


    public boolean hasKey() {
        return key != null;
    }

}
