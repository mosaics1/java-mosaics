package mosaics.cli.runner;

import mosaics.fp.Failure;
import mosaics.logging.LogMessage;
import mosaics.logging.LogMessageFactory;
import mosaics.logging.LogMessageProxyFactory;
import mosaics.logging.loglevels.DevInfo;
import mosaics.logging.loglevels.DevWarn;
import mosaics.logging.loglevels.OpsError;
import mosaics.logging.loglevels.OpsFatal;
import mosaics.logging.loglevels.OpsInfo;
import mosaics.logging.loglevels.OpsWarn;
import mosaics.logging.loglevels.UserError;

import java.util.Map;


public interface AppLogMessageFactory extends LogMessageFactory {
    public static final AppLogMessageFactory INSTANCE = LogMessageProxyFactory.INSTANCE.createLogMessageFactory( AppLogMessageFactory.class );


    @OpsInfo("Request Started")
    public LogMessage requestStarted();

    @OpsWarn("Request '$requestDescription' ($requestId) took longer than expected: ${durationMillis:duration}")
    public LogMessage slowRequest(String requestId, String requestDescription, long durationMillis );

    @DevWarn("Remote call count exceeded warning threshold: $remoteCallCount")
    public LogMessage exceededRemoteCallCountWarningThreshold( long remoteCallCount );

    @DevWarn("Remote row count exceeded warning threshold: $remoteRowCount")
    public LogMessage exceededRemoteRowCountWarningThreshold( long remoteRowCount );

    @OpsInfo("Request succeeded with result: $result")
    public LogMessage requestSucceeded( Object result );

    @OpsInfo("Request errored after ${erroredAfterMillis:duration}: $failure")
    public LogMessage requestErrored( long erroredAfterMillis, Failure failure );

    @OpsInfo("Supplied config: $config")
    public LogMessage configLogMessage( Map<String, String> config );

    @OpsFatal("Missing config: $key")
    public LogMessage missingConfig( String key );

    @DevInfo("$text")
    public LogMessage stdout( String text );

    @UserError("$msg")
    public LogMessage userErrorMessage( String msg );

    @OpsError("$msg: $ex")
    public LogMessage opsErrorMessage( String msg, Throwable ex );
}
