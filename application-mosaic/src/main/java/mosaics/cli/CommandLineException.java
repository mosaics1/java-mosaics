package mosaics.cli;


import mosaics.cli.runner.AppResult;


/**
 * Throw this exception when the command line app is to be terminated and the error message
 * from this exception is to be printed to stderr.
 */
public class CommandLineException extends RuntimeException {
    private final int exitStatus;

    public CommandLineException( String errorMessage ) {
        this( errorMessage, AppResult.EXCEPTION_THROWN );
    }

    public CommandLineException( String errorMessage, int exitStatus ) {
        super( errorMessage );

        this.exitStatus = exitStatus;
    }

    public int getExitStatus() {
        return exitStatus;
    }
}
