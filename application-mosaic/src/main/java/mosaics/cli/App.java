package mosaics.cli;


import mosaics.cli.runner.AppResult;
import mosaics.cli.runner.AppRunner;
import mosaics.fp.FP;
import mosaics.fp.collections.FPOption;
import mosaics.lang.QA;
import mosaics.lang.lifecycle.StartStoppable;
import mosaics.lang.reflection.JavaClass;
import mosaics.strings.StringUtils;


/**
 * Extend this class to create a command line application.
 *
 * Example:
 * <code>
 *
 * </code>
 *
 * @param <C> The class type of the configuration dto
 */
@SuppressWarnings( "unchecked" )
public class App<C> extends StartStoppable<App<C>> {

    public static void main( String[] args ) {
        JavaClass             mainClass      = identifyMainClass();
        FPOption<JavaClass>   configDTOClass = getConfigDTOClass( mainClass );
        App                   app            = mainClass.newInstance();
        Env                   env            = Env.createLiveEnv( app.getName()+"Env" );
        AppRunner<App>        appRunner      = new AppRunner<>( mainClass, env, configDTOClass );

        AppResult             result         = appRunner.run( app, args );

        System.exit( result.getExitCode() );
    }


    /**
     * Identifies the class specified to the JVM to invoke main() on.  Relies on the system property
     * sun.java.command, which may not be supported by all JVMs.
     */
    private static JavaClass identifyMainClass() {
        String property = System.getProperty("sun.java.command");
        if ( StringUtils.isBlank(property) ) {
            QA.fail( "The JVM property 'sun.java.command' is blank.  If your JVM does not provide this setting then please set it manually to the name of the class that you are invoking." );
        }

        String[] cliCommand = property.split(" ");
        String   mainClass  = cliCommand[0].equals("com.intellij.rt.execution.application.AppMain" ) ? cliCommand[1] : cliCommand[0];

        return JavaClass.forName(mainClass);
    }

    private static FPOption<JavaClass> getConfigDTOClass( JavaClass mainClass ) {
        JavaClass appClass  = mainClass
            .getParentClass(App.class)
            .orElseThrow( () -> new IllegalStateException("The main class '"+mainClass.getFullName()+"' does not extend App") );

        JavaClass configDTOClass = appClass.getClassGenerics()
            .first()
            .orElseThrow( () -> new IllegalStateException(
                "The main class '" + mainClass + "' does not have any config dto "
                    + "declared.  To declare no config class, extend App<Void>."
            ) );

        return configDTOClass.getJdkClass().equals( Void.class ) ? FP.emptyOption() : FP.option( configDTOClass );
    }


    public App() {
        super( identifyMainClass().getShortName() );
    }

    public App( String appName ) {
        super( appName );
    }


//    public App() {
//        super( null, "" );
//    }

//    protected App( RequestFactory requestFactory, String serviceName ) {
//        super( requestFactory, serviceName );
//    }


//    protected DI createDI() {
//        return new DI( requestFactory );
//    }

//    private boolean      requiresLockFile;
//    private Option<File> lockFileOption = Option.none();
//
//    protected void setUseLockFileFlag( Env env, String lockFileName ) {
//        File lockFile = env.getFileSystem().getCurrentWorkingDirectory().getOrCreateFile(lockFileName);
//
//        setUseLockFileFlag( lockFile );
//    }

//    protected void setUseLockFileFlag( File lockFile ) {
//        this.requiresLockFile = true;
//        this.lockFileOption   = Option.of(lockFile);
//    }

//    public boolean requiresLockFile() {
//        return requiresLockFile;
//    }
//
//    public Option<File> lockFile() {
//        return lockFileOption;
//    }


//    /**
//     * Invoked when restarting an application that did not shutdown cleanly previously.  Override
//     * to recover from crashes, as detected by a stale LOCK file.  By default this method will
//     * throw an exception, which will abort the application.
//     *
//     * @see #setUseLockFileFlag
//     */
//    protected void recoverFromCrash( Env env ) {
//        throw new IllegalStateException( "A previous run of the app did not clean up after itself, manual recovery required. Aborting.." );
//    }
}
