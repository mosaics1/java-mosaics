package mosaics.cli.requests;

import lombok.AllArgsConstructor;
import lombok.Value;
import lombok.With;
import mosaics.i18n.I18nContext;
import mosaics.lang.Copyable;


@With
@Value
@AllArgsConstructor
public class RequestOptions implements Copyable<RequestOptions> {
    /**
     * The prefered internationalistion options to use when rendering to the caller.
     */
    private I18nContext i18nContext;

    /**
     * A tracer round request is a special request that will override the logging settings used
     * during this request.  It is used to illuminate the detail of what a request is doing by
     * turning full debug on for this request only.  Used to debug live systems without having
     * to turn full debugging on for all servers that the request might pass through.
     */
    private boolean isTracerRound;

    public RequestOptions() {
        this( I18nContext.UTC, false );
    }

    public RequestOptions(RequestOptions other) {
        this( other.i18nContext, other.isTracerRound );
    }
}
