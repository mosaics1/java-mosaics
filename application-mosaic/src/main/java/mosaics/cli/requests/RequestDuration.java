package mosaics.cli.requests;

import lombok.Value;
import lombok.With;
import mosaics.fp.collections.FPOption;


@Value
public class RequestDuration {

    @With private long startedAtMillis;

    @With private FPOption<Long> finishedAtMillis;

    /**
     * This request has been scheduled to timeout.
     */
    @With private FPOption<Long> willTimeoutAtMillis;


}
