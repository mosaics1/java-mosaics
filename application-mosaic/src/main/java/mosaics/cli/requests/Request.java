package mosaics.cli.requests;


import mosaics.cli.Env;
import mosaics.cli.runner.AppLogMessageFactory;
import mosaics.fp.FP;
import mosaics.fp.collections.ConsList;
import mosaics.fp.collections.FPOption;
import mosaics.fp.collections.RRBVector;
import mosaics.lang.QA;
import mosaics.lang.TryUtils;
import mosaics.lang.functions.Function0;
import mosaics.lang.functions.Function1WithThrows;
import mosaics.lang.functions.VoidFunction0WithThrows;
import mosaics.lang.reflection.ReflectionUtils;
import mosaics.lang.time.DTM;
import mosaics.logging.LogMessage;
import mosaics.logging.Logger;

import java.time.Duration;
import java.util.concurrent.atomic.AtomicReference;

// todo test json codec on Request
// todo test Request
public class Request implements Logger{
    private transient final Env                                   env;
    private           final RequestIdentity                       requestIdentity;
    private           final AtomicReference<CallerDetails>        callerDetailsRef        = new AtomicReference<>();
    private           final AtomicReference<RequestTiming>        requestTimingRef        = new AtomicReference<>();
    private           final AtomicReference<RequestMetrics>       requestMetricsRef       = new AtomicReference<>();
    private           final AtomicReference<RequestOptions>       requestOptionsRef       = new AtomicReference<>();
    private           final AtomicReference<RemoteCallThresholds> remoteCallThresholdsRef = new AtomicReference<>(RemoteCallThresholds.DEFAULT);


    private           final FPOption<Request>                     parentRequest;
    private           final AtomicReference<RRBVector<Request>>   childRequests           = new AtomicReference<>(RRBVector.emptyVector());


    public Request( Env env ) {
        this( env, ReflectionUtils.getCallersMethod().getShortClassNameAndMethodName() );
    }

    public Request( Env env, String requestDescription ) {
        this( env, env.nextUUID(), requestDescription );
    }

    public Request( Env env, String requestId, String requestDescription ) {
        DTM requestStartDTM = env.currentDTM();

        this.env             = env;
        this.requestIdentity = new RequestIdentity( ConsList.consList(requestId), requestDescription );
        this.parentRequest   = FP.emptyOption();

        callerDetailsRef.set( new CallerDetails() );
        requestTimingRef.set( new RequestTiming(requestStartDTM) );
        requestMetricsRef.set( new RequestMetrics() );
        requestOptionsRef.set( new RequestOptions() );
    }

    private Request( Request parent, String childsRequestDescription ) {
        Env              env               = parent.getEnv();
        DTM              requestStartDTM   = env.currentDTM();
        String           newRequestId      = env.nextUUID();
        RequestOptions   newRequestOptions = new RequestOptions( parent.requestOptionsRef.get() );
        ConsList<String> childRequestIds   = parent.requestIdentity.getRequestIds().append(newRequestId);

        this.env             = env;
        this.requestIdentity = new RequestIdentity( childRequestIds, childsRequestDescription );
        this.parentRequest   = FP.option( parent );

        callerDetailsRef.set( parent.callerDetailsRef.get() );
        requestTimingRef.set( new RequestTiming(requestStartDTM) );
        requestMetricsRef.set( new RequestMetrics() );
        requestOptionsRef.set( newRequestOptions );
    }


// GETTERS
    public Env getEnv() {
        return env;
    }


// REQUEST STATUS
    public String getRequestId() {
        return requestIdentity.getRequestId();
    }

    public boolean isRequestRunning() {
        return requestTimingRef.get().getRequestEndDTM().isEmpty();
    }

    public boolean hasRequestFinished() {
        return requestTimingRef.get().hasRequestFinished();
    }

    public String getRequestDescription() {
        return requestIdentity.getRequestDescription();
    }

    public DTM getRequestStartDTM() {
        return requestTimingRef.get().getRequestStartDTM();
    }

    public long getRemoteRowCount() {
        return requestMetricsRef.get().getRemoteRowCount();
    }

    public long getRemoteCallCount() {
        return requestMetricsRef.get().getRemoteCallCount();
    }

    public long getRemoteCallCountThreshold() {
        return remoteCallThresholdsRef.get().getRemoteCallCountThreshold();
    }

    public long getSlowRequestThreshold() {
        return remoteCallThresholdsRef.get().getSlowRequestThreshold();
    }

// LOGGING

    public boolean isTracerRequest() {
        return requestOptionsRef.get().isTracerRound();
    }

    public Request withIsTracerRound( boolean isTracerRound ) {
        requestOptionsRef.updateAndGet( options -> options.withTracerRound(isTracerRound) );

        return this;
    }

    /**
     * Write the specified LogMessage to the environments LogWriter.  So you decided to create
     * your own LogMessages directly rather than use the Logger interfaces.
     */
    public void log( LogMessage logMessage ) {
        env.log(
            logMessage
                .withRequestId( FP.option(getRequestId()) )
                .withTracerMessage( isTracerRequest() )
                .withRequestDescription( FPOption.ofBlankableString(getRequestDescription()) )
                .withUserId( getUserId() )
        );
    }


// CLOCK

    public long currentTimeMillis() {
        return env.currentTimeMillis();
    }
    public long currentTimeNanos() { return env.currentTimeNanos(); }

    public double elapsedMillisSinceNanos( long startNanos ) {
        return (currentTimeNanos() - startNanos) / 1_000_000.0;
    }

    public DTM currentDTM() {
        return env.currentDTM();
    }

    public Duration duration( long startMillis ) {
        return Duration.ofMillis( currentTimeMillis() - startMillis );
    }

    public double timeMillis( VoidFunction0WithThrows f ) {
        return env.timeMillis( f );
    }

    public Request markCompleted() {
        return markCompleted( currentTimeMillis() );
    }

    public Request markCompleted( long endedAtAtMillis ) {
        return markCompleted( new DTM(endedAtAtMillis) );
    }

    @SuppressWarnings("unchecked")
    public Request markCompleted( DTM endedAt ) {
        return markCompleted(
            RequestResult.success(this,getRequestStartDTM().getMillis(), endedAt.getMillis())
        );
    }

    public <T> Request markCompleted( RequestResult<T> result ) {
        DTM now = env.currentDTM();

        return markCompleted( now, result );
    }

    private synchronized <T> Request markCompleted( DTM now, RequestResult<T> result ) {
        childRequests.get().forEach( child -> child.markCompleted(now,result) );
        childRequests.updateAndGet(list -> RRBVector.emptyVector());

        requestTimingRef.updateAndGet( rd -> rd.markCompleted(now) );
        parentRequest.ifPresent( parent -> parent.childCompleted(this) );

        //        endOfRequestCallbacks.invokeAllCallbacks( this );

//        if ( result.hasResult() ) {
//            requestScope.requestSucceeded( this );
//        } else {
//            requestScope.requestFailed( this, result.getFailure() );
//        }

        return this;
    }

    private void childCompleted( Request child ) {
//        this.incRemoteCallCount( child.getRemoteCallCount() );
//        this.incRemoteRowCount( child.getRemoteRowCount() );

        this.childRequests.updateAndGet( list -> list.remove(child) );
    }

    public void throwIfRequestHasAlreadyEnded() {
        requestTimingRef.get().throwIfRequestHasAlreadyFinished();
    }


//    public static final int MAX_REQUEST_ID_CHARACTER_LENGTH = 40;
//
//
//
//    public RequestScope getRequestScope();
//
//    public long getSlowRequestThreshold();
//    public long getRemoteCallCountThreshold();
//    public long getRemoteRowCountThreshold();
//
//    public RequestDetails getRequestDetails();
//
//
//    public Executor getNonBlockingExecutor();
//
//    public Executor getBlockingExecutor();
//
//
//    public void log( Event logMessage );
//
//    /**
//     * Disables the logger.
//     */
//    public void silenceLog();
//
//    public void withExtraLogHandler( LogHandler extraLogHandler );
//
//    /**
//     * A developer debug message.  This is a convenience method so that developers do not have to
//     * create a DTO for every throw away println that they write during development.  Other messages
//     * that are intended for Ops, Users and Audits on the other hand are worth the extra effort.
//     */
//    public void dev( String msg );
//

    /**
     * Invokes the specified function within this request.
     */
    public <T> RequestResult<T> invoke( Function1WithThrows<Request, T> workerFunction ) {
        QA.isFalseState( hasRequestFinished(), "cannot invoke request, as it has already been marked as completed" );

        log( AppLogMessageFactory.INSTANCE.requestStarted() );

        RequestResult<T> requestResult = invokeSilently( workerFunction );

        markCompleted( requestResult );
        logResult( requestResult );

        return requestResult;
    }

    /**
     * Invokes the specified function in a child request context.
     */
    public <T> T invokeChild( String childDescription, Function1WithThrows<Request, T> workerFunction ) {
        Request childContext = this.createChild( childDescription );

        return childContext.invoke( workerFunction ).getResult();
    }

        /**
         * Create a new request that is a child of this request.  The new request will start from now
         * and will have all of its counters reset to zero.  When it completes it will roll its counters
         * up onto its parents totals.
         *
         * Completing the child request will not complete the parent request, but completing the parent
         * request will complete the children.
         */
    public Request createChild( String description ) {
        Request newChild = new Request(this, description );

        childRequests.getAndUpdate(list -> list.add(newChild));

        return newChild;
    }

//
//    /**
//     * Creates a child request and invokes the specified worker function.
//     */
//    public <T> RequestResult<T> invokeNested( String jobDescription, Function1WithThrows<Request, T> workerFunction );
//
//    /**
//     * Creates a child request and invokes the specified worker function.
//     */
//    public <T> RequestResult<T> invokeRemote( String remote, String jobDescription, Function1WithThrows<Request, T> workerFunction );
//
//    public FPList<Request> getChildren();
//
//    public long getStartedAtMillis();
//
//    public FPOption<Long> getFinishedAtMillis();
//
//    public FPOption<Long> getWillTimeoutAtMillis();
//
//
//    public String getRequestId();
//
//    public String getRequestDescription();
//
//    public FPOption<String> getUserId();
//
//    public FPOption<String> getCallerIp();
//
    public FPOption<String> getUserId() {
        return callerDetailsRef.get().getUserId();
    }
//
//    public FPOption<String> getProxyProtocolForwards();
//
//    public FPOption<String> getProxyIpForwards();
//
//    public boolean isTracerRound();
//
//
//    public Request withStartedAtMillis( long startedAtMillis );
//
//    public Request withWillTimeoutAtMillis( FPOption<Long> willTimeoutAtMillis );
//
//    public long getMillisUntilTimeout();
//
//    public Request withClock( Clock clock );
//
//    public Request withRequestId( String requestId );
//
//    public Request withRequestDescription( String requestDescription );
//
//    public Request withUserId( FPOption<String> userId );
//
//    public Request withCallerIp( FPOption<String> callerIp );
//
//    public Request withCallerJvmInstanceId( FPOption<String> callerJvmInstanceId );
//
//    public Request withProxyProtocolForwards( FPOption<String> proxyProtocolForwards );
//
//    public Request withProxyIpForwards( FPOption<String> proxyIpForwards );
//
//
//    public Subscription registerEndOfRequestCallback( VoidFunction1<Request> callback );
//
//
//    public <T> Request markCompleted( RequestResult<T> result );
//
//    public Locale getLocale();
//
//    public TimeZone getTimeZone();
//
//    public long currentTimeMillis();
//
//    public String generateNewGuid();
//
//    public Request incRemoteRowCount( long delta );
//
//    public Request incRemoteCallCount( long delta );
//
//    public long getRemoteCallCount();
//    public long getRemoteRowCount();
//    public long getWarningCount();
//    public long getErrorCount();
//    public long getFatalErrorCount();
//
//    public <K,V> void putRequestScope( K key, V value );
//
//    public <K> void removeFromRequestScope( K key );
//
//    public long currentTimeNanos();
//
//
//
//

//
//    public default long elapsedMillisSinceNanos( long startNanos ) {
//        return (currentTimeNanos() - startNanos + 200_000) / NUMBER_OF_NANOSECODS_PER_MILLISECOND;
//    }

    public void sleep( long millis, String reason ) {
        log( RequestLogMessageFactory.INSTANCE.threadGoingToSleep(millis,reason) );

        long expectedWakeupMillis = env.currentTimeMillis()+millis;

        try {
            env.sleep( millis );

            log( RequestLogMessageFactory.INSTANCE.threadSleepComplete(millis) );
            TryUtils.fakeExceptionThrow(InterruptedException.class);
        } catch ( InterruptedException ex ) {
            long earlyMillis = expectedWakeupMillis - env.currentTimeMillis();

            log( RequestLogMessageFactory.INSTANCE.threadSleepInterrupted(earlyMillis,ex) );
        }
    }

    /**
     * Returns how long it took to run the specified task.
     */
    public long time( Runnable task ) {
        long startMillis = currentTimeMillis();

        task.run();

        return currentTimeMillis() - startMillis;
    }

    /**
     * Returns how long it took to run the specified task and the result.
     */
    public <T> TimedCallResult<T> time( Function0<T> task ) {
        long startMillis = currentTimeMillis();

        T result = task.invoke();

        return new TimedCallResult<>(currentTimeMillis() - startMillis, result);
    }

    private <T> RequestResult<T> invokeSilently( Function1WithThrows<Request, T> workerFunction ) {
        long startedAtMillis = currentTimeMillis();

        try {
            T result = workerFunction.invoke( this );

            return RequestResult.success(this, result, startedAtMillis, currentTimeMillis() );
        } catch ( Throwable ex ) {
            return RequestResult.failure( this, ex, startedAtMillis, currentTimeMillis() );
        }
    }

    private <T> void logResult( RequestResult<T> requestResult ) {
        if ( requestResult.getDurationMillis() > getSlowRequestThreshold() ) {
            log(AppLogMessageFactory.INSTANCE.slowRequest(getRequestId(), getRequestDescription(), requestResult.getDurationMillis()));
        } else if ( requestResult.getRemoteCallCount() > getRemoteCallCountThreshold() ) {
            log(AppLogMessageFactory.INSTANCE.exceededRemoteCallCountWarningThreshold(getRemoteCallCount()));
        } else if ( requestResult.getRemoteRowCount() > getRemoteCallCountThreshold() ) {
            log(AppLogMessageFactory.INSTANCE.exceededRemoteRowCountWarningThreshold(getRemoteRowCount()));
        }

        if ( requestResult.hasResult() ) {
            log(AppLogMessageFactory.INSTANCE.requestSucceeded(requestResult.getResult()) );
        } else {
            log(AppLogMessageFactory.INSTANCE.requestErrored(requestResult.getDurationMillis(), requestResult.getFailure()));
        }
    }
}

