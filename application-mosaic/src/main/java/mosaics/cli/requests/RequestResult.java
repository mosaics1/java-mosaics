package mosaics.cli.requests;

import lombok.Getter;
import mosaics.fp.ExceptionFailure;
import mosaics.fp.Failure;
import mosaics.fp.Tryable;
import mosaics.fp.collections.FPIterable;
import mosaics.lang.Backdoor;
import mosaics.lang.functions.Function1;


@SuppressWarnings( "unchecked" )
public abstract class RequestResult<T> implements FPIterable<T> {

    @SuppressWarnings("unchecked")
    public static RequestResult success( Request req, long startedAtMillis, long finishedAtMillis ) {
        return new RequestResultSuccess( req , null, startedAtMillis, finishedAtMillis );
    }

    public static <T> RequestResult<T> success( Request req, T v, long startedAtMillis, long finishedAtMillis) {
        return new RequestResultSuccess<>( req , v, startedAtMillis, finishedAtMillis );
    }

    public static <T> RequestResult<T> failure( Request req, Throwable ex, long startedAtMillis, long finishedAtMillis) {
        return failure( req, new ExceptionFailure(ex), startedAtMillis, finishedAtMillis );
    }

    public static <T> RequestResult<T> failure( Request req, Failure f, long startedAtMillis, long finishedAtMillis ) {
        return new RequestResultFailure<>(req, f, startedAtMillis, finishedAtMillis);
    }


    @Getter private Request request;
    @Getter private long    startedAtMillis;
    @Getter private long    finishedAtMillis;
    @Getter private long    remoteRowCount;
    @Getter private long    remoteCallCount;



    protected RequestResult( Request req, long startedAtMillis, long finishedAtMillis ) {
        this.request          = req;
        this.startedAtMillis  = startedAtMillis;
        this.finishedAtMillis = finishedAtMillis;
// todo
//        this.remoteRowCount   = req.getRemoteRowCount();
//        this.remoteCallCount  = req.getRemoteCallCount();
    }




    public abstract T getResult();
    public abstract boolean hasResult();
    public abstract boolean hasFailure();
    public abstract Failure getFailure();


    public abstract <B> RequestResult<B> flatMapResult( Function1<T, Tryable<B>> mappingFunction );

    public abstract <E extends Throwable> RequestResult<T> recover( Class<E> exType, Function1<RequestResultFailure<T>,T> recoverFunc );

    public long getDurationMillis() {
        return finishedAtMillis - startedAtMillis;
    }

    //todo
//    public RequestDetails getRequestDetails() {
//        return request.getRequestDetails();
//    }

    public void throwIfFailure() {
        if ( hasFailure() ) {
            throw Backdoor.throwException(getFailure().toException());
        }
    }
}
