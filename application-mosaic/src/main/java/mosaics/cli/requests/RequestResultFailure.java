package mosaics.cli.requests;

import lombok.EqualsAndHashCode;
import lombok.Value;
import mosaics.fp.FP;
import mosaics.fp.Failure;
import mosaics.fp.Tryable;
import mosaics.fp.collections.FPIterator;
import mosaics.fp.collections.FPOption;
import mosaics.lang.Backdoor;
import mosaics.lang.functions.Function1;


@Value
@EqualsAndHashCode(callSuper = false)
public class RequestResultFailure<T> extends RequestResult<T> {

    private Failure failure;

    protected RequestResultFailure( Request request, Failure failure, long startedAtMillis, long finishedAtMillis ) {
        super( request, startedAtMillis, finishedAtMillis );

        this.failure = failure;
    }

    public T getResult() {
        throw Backdoor.throwException( failure.toException() );
    }

    public boolean hasResult() {
        return false;
    }

    public boolean hasFailure() {
        return true;
    }

    public Failure getFailure() {
        return failure;
    }

    public FPIterator<T> iterator() {
        return FP.emptyIterator();
    }

    public <B> RequestResult<B> flatMapResult( Function1<T, Tryable<B>> mappingFunction ) {
        return Backdoor.cast(this);
    }

    public <E extends Throwable> RequestResult<T> recover( Class<E> exType, Function1<RequestResultFailure<T>,T> recoverFunc ) {
        if ( getException(exType).hasValue() ) {
            return RequestResult.success( getRequest(), recoverFunc.invoke(this), getStartedAtMillis(), getRequest().currentTimeMillis() );
        }

        return this;
    }

    public <T extends Throwable> FPOption<T> getException( Class<T> type ) {
        return failure.toException(type);
    }

}
