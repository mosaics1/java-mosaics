package mosaics.cli.requests;

import mosaics.fp.FP;
import mosaics.fp.Failure;
import mosaics.fp.Tryable;
import mosaics.fp.collections.FPIterator;
import mosaics.lang.Backdoor;
import mosaics.lang.functions.Function1;


public class RequestResultSuccess<T> extends RequestResult<T> {
    private T                 result;

    RequestResultSuccess( Request request, T result, long startedAtMillis, long finishedAtMillis ) {
        super( request, startedAtMillis, finishedAtMillis );

        this.result = result;
    }

    public T getResult() {
        return result;
    }

    public boolean hasResult() {
        return true;
    }

    public boolean hasFailure() {
        return false;
    }

    public Failure getFailure() {
        throw new UnsupportedOperationException();
    }

    public FPIterator<T> iterator() {
        return FP.wrapSingleton(result).iterator();
    }

    public <B> RequestResult<B> flatMapResult( Function1<T, Tryable<B>> mappingFunction ) {
        Tryable<B> mappedResult = mappingFunction.invoke( result );

        if ( mappedResult.hasResult() ) {
            return new RequestResultSuccess<>( this.getRequest(), mappedResult.getResult(), this.getStartedAtMillis(), this.getFinishedAtMillis() );
        } else {
            return new RequestResultFailure<>( this.getRequest(), mappedResult.getFailure(), this.getStartedAtMillis(), this.getFinishedAtMillis() );
        }
    }

    public <E extends Throwable> RequestResult<T> recover( Class<E> exType, Function1<RequestResultFailure<T>,T> recoverFunc ) {
        return Backdoor.cast(this);
    }

}
