package mosaics.cli.requests;

import mosaics.logging.LogMessage;
import mosaics.logging.LogMessageFactory;
import mosaics.logging.LogMessageProxyFactory;
import mosaics.logging.loglevels.DevInfo;


public interface RequestLogMessageFactory extends LogMessageFactory {
    public static final RequestLogMessageFactory INSTANCE = LogMessageProxyFactory.INSTANCE.createLogMessageFactory( RequestLogMessageFactory.class );

    @DevInfo("Putting thread to sleep for ${millis:duration}: $reason")
    public LogMessage threadGoingToSleep(long millis, String reason);

    @DevInfo("Thread woke up after ${millis:duration} sleep")
    public LogMessage threadSleepComplete(long millis);

    @DevInfo("Thread sleep interrupted ${millisEarly:duration}: $ex")
    public LogMessage threadSleepInterrupted(long millisEarly, InterruptedException ex);
}
