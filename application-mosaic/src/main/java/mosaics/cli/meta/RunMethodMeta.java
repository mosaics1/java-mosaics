package mosaics.cli.meta;

import lombok.Value;
import mosaics.fp.FP;
import mosaics.fp.collections.FPIterable;
import mosaics.fp.collections.FPOption;
import mosaics.lang.QA;
import mosaics.lang.reflection.DI;
import mosaics.lang.reflection.JavaClass;
import mosaics.lang.reflection.JavaMethod;


@Value
public class RunMethodMeta {
    public static RunMethodMeta of( JavaMethod m ) {
        RunMethodMeta runMethodMeta = new RunMethodMeta(
            m.getReturnType(),
            m.getName(),
            m.getParameters().map( ArgMeta::of ).toFPList()
        );

        throwIfInvalid( runMethodMeta, m.getDeclaringClass().getShortName() );

        return runMethodMeta;
    }


    private JavaClass           returnType;
    private String              methodName;
    private FPIterable<ArgMeta> args;


    public boolean acceptsArgs( DI di, String[] args ) {
        return args.length >= getMinArgCount(di) && args.length <= getMaxArgCountInc(di);
    }

    private long getMinArgCount( DI di ) {
        if ( hasVarArgs(this) ) {
            return getArgs().count() - 1;
        } else {
            return (int) getArgs()
                .filterNot( ArgMeta::isOptional )
//                .filterNot( argMeta -> argMeta.getParameterMeta().getType().isInstanceOf( Request.class ) )
                .filterNot( argMeta -> argMeta.accepts(di) )
                .count();
        }
    }

    private long getMaxArgCountInc( DI di ) {
        if ( hasVarArgs(this) ) {
            return Integer.MAX_VALUE;
        } else {
            return getArgs()
//                .filterNot( argMeta -> argMeta.getParameterMeta().getType().isInstanceOf( Request.class ) )
                .filterNot( argMeta -> argMeta.accepts(di) )
                .count();
        }
    }

    private static boolean hasVarArgs( RunMethodMeta runMethodMeta ) {
        FPOption<ArgMeta> lastParam = runMethodMeta.getArgs().last();

        return lastParam.hasFirst( ArgMeta::isVarArg );
    }

    private static boolean hasValidArgs( RunMethodMeta runMethodMeta ) {
        if ( hasVarArgs(runMethodMeta) ) {
            ArgMeta[] args = runMethodMeta.getArgs().toArray( ArgMeta.class);
            for ( int i=0; i<args.length-1; i++ ) {
                if ( args[i].isOptional() || args[i].isVarArg() ) {
                    return false;
                }
            }
        } else {
            return !runMethodMeta.getArgs().any( ArgMeta::isVarArg) && firstMandatoryArgAfterOptionalArg(runMethodMeta).isEmpty();
        }

        return true;
    }

    private static void throwIfInvalid( RunMethodMeta runMethodMeta, String appShortName ) {
        if ( hasValidArgs(runMethodMeta) ) {
            return;
        }

        firstOptionalArgBeforeVarArg(runMethodMeta).ifPresent(
            noneOptionalArg ->
                QA.fail(
                    "%s is not valid.  The optional run method arg %s is now allowed before " +
                        "any var args.",
                    appShortName,
                    noneOptionalArg.getName()
                )
        );

        firstMandatoryArgAfterOptionalArg(runMethodMeta).ifPresent(
            noneOptionalArg ->
                QA.fail(
                    "%s is not valid.  The run method has mandatory arg %s after an optional arg.  " +
                        "All optional args must be declared last, order matters here.",
                    appShortName,
                    noneOptionalArg.getName()
                )
        );

        findVarArgNotAtEndOfParameters(runMethodMeta).ifPresent(
            noneOptionalArg ->
                QA.fail(
                    "%s is not valid.  The var arg run method arg %s must be the last parameter " +
                        "of the method.",
                    appShortName,
                    noneOptionalArg.getName()
                )
        );
    }

    private static FPOption<ArgMeta> firstOptionalArgBeforeVarArg(RunMethodMeta runMethodMeta) {
        if ( hasVarArgs(runMethodMeta) ) {
            return runMethodMeta.getArgs().first( ArgMeta::isOptional);
        } else {
            return FP.emptyOption();
        }
    }

    private static FPOption<ArgMeta> firstMandatoryArgAfterOptionalArg( RunMethodMeta runMethodMeta ) {
        FPIterable<ArgMeta> optionalArgs = runMethodMeta.getArgs().dropWhile( arg -> !arg.isOptional() );

        return optionalArgs.dropWhile( ArgMeta::isOptional).first();
    }

    private static FPOption<ArgMeta> findVarArgNotAtEndOfParameters( RunMethodMeta runMethodMeta ) {
        if ( hasVarArgs(runMethodMeta) ) {
            return FP.emptyOption();
        } else {
            return runMethodMeta.getArgs().first( ArgMeta::isVarArg);
        }
    }
}
