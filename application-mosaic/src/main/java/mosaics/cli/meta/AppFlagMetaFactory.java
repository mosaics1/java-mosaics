package mosaics.cli.meta;

import mosaics.cli.CommandLineArg;
import mosaics.fp.collections.FPOption;
import mosaics.lang.reflection.DTOBuilder;
import mosaics.lang.reflection.JavaClass;
import mosaics.lang.reflection.JavaDoc;
import mosaics.lang.reflection.JavaProperty;


public class AppFlagMetaFactory {
    public <A,B> FPOption<AppFlagMeta> createFlagFor( JavaProperty<A,B> javaProperty ) {
        FPOption<Character> shortName     = getShortNameFor( javaProperty );
        String              longName      = getLongNameFor( javaProperty );

        String              propertyPath  = javaProperty.toString();
        JavaClass           valueType     = javaProperty.getValueType();
        FPOption<JavaDoc>   documentation = javaProperty.getJavaDoc();
        FPOption<?>         defaultValue  = getDefaultValueOf(javaProperty);

        return FPOption.of(
            new AppFlagMeta(shortName, longName, propertyPath, valueType, documentation, defaultValue)
        );
    }

    private <A, B> FPOption<?> getDefaultValueOf( JavaProperty<A, B> javaProperty ) {
        A defaultSource = DTOBuilder.<A>createDTOBuilderFor(javaProperty.getSourceType()).build();

        return javaProperty.getValueFrom( defaultSource );
    }

    private <A, B> FPOption<Character> getShortNameFor( JavaProperty<A, B> javaProperty ) {
        FPOption<CommandLineArg> annotation = javaProperty.getAnnotation( CommandLineArg.class );

        return annotation.map( CommandLineArg::shortName );
    }

    private String getLongNameFor( JavaProperty<?,?> javaProperty ) {
        return javaProperty.getAnnotation( CommandLineArg.class )
            .flatMap( arg -> FPOption.ofBlankableString(arg.longName()) )
            .orElse( javaProperty::getName );
    }
}
