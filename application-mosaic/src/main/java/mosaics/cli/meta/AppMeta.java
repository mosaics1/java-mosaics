package mosaics.cli.meta;

import lombok.Value;
import mosaics.fp.collections.FPIterable;
import mosaics.fp.collections.FPOption;
import mosaics.lang.reflection.JavaClass;
import mosaics.lang.reflection.JavaDoc;


@Value
public class AppMeta {
    private JavaClass                 appMainClass;
    private String                    appName;
    private FPOption<JavaClass>       appConfigClass;

    private FPOption<JavaDoc>         appDocs;
    private FPIterable<AppFlagMeta>   flags;
    private FPIterable<RunMethodMeta> runMethods;


    public FPOption<AppFlagMeta> getKVByName( String name ) {
        return getFlags().first( arg -> arg.matchesName(name) );
    }

    public String toString() {
        return appMainClass.getShortName();
    }

    public boolean isConfigDTOClass( JavaClass candidate ) {
        return appConfigClass.map(candidate::equals).orElse( false );
    }

    public FPOption<AppFlagMeta> getFlagByLabel( String label ) {
        return getFlags().first( candidateFlag -> candidateFlag.matchesName(label) );
    }
}
