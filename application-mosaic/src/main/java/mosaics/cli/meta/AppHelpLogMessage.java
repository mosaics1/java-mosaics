package mosaics.cli.meta;

import lombok.Value;
import mosaics.fp.FP;
import mosaics.fp.collections.FPOption;
import mosaics.io.writers.IndentWriter;
import mosaics.lang.reflection.JavaClass;
import mosaics.lang.reflection.JavaDoc;
import mosaics.lang.reflection.ReflectionUtils;
import mosaics.lang.time.DTM;
import mosaics.logging.LogMessage;
import mosaics.logging.LogSeverity;
import mosaics.logging.TargetAudience;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


/**
 * Inform the user on how to use to command line application.
 */
@Value
@SuppressWarnings("unchecked")
public class AppHelpLogMessage {
    public static LogMessage createHelpMessage( DTM when, AppMeta appMeta ) {
        return new LogMessage(
            when,
            TargetAudience.USER,
            LogSeverity.INFO,
            FP.emptyOption(),
            FP.option( AppHelpLogMessage.class.getName() ),
            getTemplateFor(appMeta),
            FP.emptyOption(),
            FP.emptyOption(),
            FP.emptyMap(),
            false
        );
    }

    private static String getTemplateFor( AppMeta appMeta ) {
        StringBuilder buf = new StringBuilder();

        buf.append("#static\n");
        writeTo( appMeta, buf );
        buf.append("#endstatic");

        return buf.toString();
    }


    private static void writeTo( AppMeta appMeta, Appendable appendable ) {
        IndentWriter out = new IndentWriter( appendable, "    " );

        printParagraph( out, appMeta.getAppDocs(), true );

        Map<String, ArgMeta> args = new LinkedHashMap<>();

        JavaClass appMainClass = appMeta.getAppMainClass();
        for ( RunMethodMeta m : appMeta.getRunMethods() ) {
            out.print( "Usage: " );
            out.print( appMainClass.getFullName());

            for ( ArgMeta arg : m.getArgs() ) {
                out.print( " " );

                if ( arg.isOptional() ) {
                    out.print( '[' );
                }

                out.print( arg.getName() );

                if ( arg.isOptional() ) {
                    out.print( ']' );
                }

                args.put( arg.getName(), arg );
            }

            out.newLine();
        }

        out.newLine();


        out.indent( () -> {
            for ( ArgMeta arg : args.values() ) {
                if ( !arg.getDocumentation().isEmpty() ) {
                    out.print( arg.getName() );

                    if ( arg.isEnum() ) {
                        out.print( " (" );
                        out.printEach( arg.getEnumValues(), "|" );
                        out.print( ')' );
                    }

                    out.newLine();
                    out.indent( () -> out.printLines( arg.getDocumentation() ) );
                    out.newLine();
                }
            }

            for ( AppFlagMeta flagMeta : appMeta.getFlags().sort() ) {
                out.print("--");
                out.print(flagMeta.getLongName());

                if ( flagMeta.getShortName().hasValue() ) {
                    out.print(", -");
                    out.print(flagMeta.getShortName().get());
                }

                if ( flagMeta.isEnum() ) {
                    out.print(" = ");
                    out.printEach( FP.wrap(flagMeta.getEnumValues()).map(Object::toString).toList(), "|" );
                }

                FPOption<?> defaultAppInstance = createDefaultInstanceOf(appMainClass);

                defaultAppInstance.ifPresent( app -> {
                   flagMeta.getValueFrom(app).ifPresent( value -> {
                       if ( shouldDisplayNonDefaultValue(value) ) {
                           out.print( "  (default=" );
                           out.print( value.toString() );
                           out.print( ")" );
                       }
                   } );
                });

                out.newLine();
                out.indent( () -> printParagraph( out, flagMeta.getDocumentation(), false ) );
            }
        });


        out.newLine();
        out.println("    --help, -?");
        out.println("        Display this help summary.");
        out.println("    --debug, -d");
        out.println("        Enable debug logging, which will display all fine grained logging suitable for developers.");
        out.println("    --verbose, -v");
        out.println("        Enable verbose logging, which will display important usage events suitable for operational monitoring.");
        out.println("    --quiet, -q");
        out.println("        Only errors will be logged.");
        out.println("    --silent, -s");
        out.println("        Disable all logging.");
        out.println("    --logFormat (default=TEXT)");
        out.println("        Format the logged events either TEXT or JSON.");
        out.println("    --config=<url>");
        out.println("        Specify where to load the configuration file from.  The URL supports file://<absolute path>, and");
        out.println("        classpath:<absolute path>.  The contents of the config file matches the same arguments described");
        out.println("        above.  The config file can hold an argument that may be specified on the command line, stored.");
        out.println("        as a Java properties file.  For example to specify the verbose flag within the config file");
        out.println("        add the following line into the properties file 'verbose=true'.");
    }

    private static FPOption<?> createDefaultInstanceOf( JavaClass appMainClass ) {
        return tryToCreatDefaultInstanceOfAppUsingRequestFactory(appMainClass)
            .or( () -> tryToCreatDefaultInstanceOfAppUsingZeroArgConstructor(appMainClass) );
    }

    private static FPOption<Object> tryToCreatDefaultInstanceOfAppUsingRequestFactory( JavaClass appMainClass ) {
//        return appMainClass.getPublicConstructor( RequestFactory.class )
//            .map( constructor -> constructor.newInstance(new RequestFactory()) );
        return FP.emptyOption();
    }

    private static FPOption<Object> tryToCreatDefaultInstanceOfAppUsingZeroArgConstructor( JavaClass appMainClass ) {
        return appMainClass.getPublicConstructor()
            .map( constructor -> constructor.newInstance() );
    }

    private static boolean shouldDisplayNonDefaultValue( Object v ) {
        return !ReflectionUtils.isZeroOrEmpty(v);
    }

    private static void printParagraph( IndentWriter out, FPOption<JavaDoc> documentation, boolean endWithBlankLine ) {
        documentation.ifPresent( docMeta -> printParagraph(out,docMeta.getText(),endWithBlankLine) );
    }

    private static void printParagraph( IndentWriter out, List<String> text, boolean endWithBlankLine ) {
        if ( text != null && text.size() > 0 ) {
            text.forEach( out::println );

            if ( endWithBlankLine ) {
                out.newLine();
            }
        }
    }

}
