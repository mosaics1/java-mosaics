package mosaics.cli.meta;


import lombok.Value;
import mosaics.fp.collections.FPList;
import mosaics.fp.collections.FPSet;
import mosaics.fp.collections.RRBVector;
import mosaics.lang.reflection.DI;
import mosaics.lang.reflection.JavaClass;
import mosaics.lang.reflection.JavaDoc;
import mosaics.lang.reflection.JavaParameter;
import mosaics.utils.ListUtils;

import java.util.Collections;
import java.util.List;


@SuppressWarnings("unchecked")
@Value
public class ArgMeta {

    public static ArgMeta of( JavaParameter p ) {
        return new ArgMeta(
            p.getType(),
            p.getName(),
            p.getJavaDoc().map(JavaDoc::getText).orElse( Collections.emptyList() )
        );
    }


    private JavaClass    valueType;
    private String       name;
    private List<String> documentation;


    public boolean isVarArg() {
        JavaClass type = getValueType();

        return type.isArray() || type.isJdkList() || type.isJdkSet() || type.isInstanceOf( FPList.class)
            || type.isInstanceOf( FPSet.class) || type.isInstanceOf( RRBVector.class);
    }

    public boolean isOptional() {
        return getValueType().isOptional();
    }

    public boolean isEnum() {
        return getValueType().isEnum();
    }

    public List<String> getEnumValues() {
        return ListUtils.<Enum,String>mapIterable(getValueType().getEnumValues(), Enum::name);
    }

    public boolean accepts( DI di ) {
        return di.accepts( getValueType().getJdkClass(), getName() );
    }
}
