package mosaics.concurrency;

import mosaics.cli.requests.Request;
import mosaics.lang.clock.Clock;
import mosaics.lang.clock.RealTimeClock;
import mosaics.lang.functions.Function0;

import java.util.concurrent.atomic.AtomicBoolean;


/**
 * Use instead of the equivalent methods on Object to support unit testing.
 */
public interface ObjectMonitor {
    public static ObjectMonitor createObjectMonitorFor( Clock clock ) {
        if ( clock instanceof RealTimeClock ) {
            return createObjectMonitorFor( (Object) clock );
        }

        return new ObjectMonitor() {
            private AtomicBoolean notifyFlag = new AtomicBoolean();

            public void sleep( Request req, long millis ) {
                req.log( ObjectMonitorLogMessageFactory.INSTANCE.threadGoingToSleep(Thread.currentThread().getName(),millis) );

                long targetMillis = clock.currentTimeMillis() + millis;

                while ( clock.currentTimeMillis() < targetMillis && !Thread.interrupted() ) {
                    Thread.yield();
                }
            }

            public void wait( Request req, long millis ) {
                req.log( ObjectMonitorLogMessageFactory.INSTANCE.threadWaitingAgainstMonitor(Thread.currentThread().getName(),millis,this.toString()) );

                long targetMillis = clock.currentTimeMillis() + millis;

                while ( clock.currentTimeMillis() < targetMillis && !notifyFlag.get() && !Thread.interrupted() ) {
                    Thread.yield();
                }

                notifyFlag.set( false );
            }

            public void notify( Request req ) {
                req.log( ObjectMonitorLogMessageFactory.INSTANCE.threadCalledNotify(Thread.currentThread().getName(),this.toString()) );

                notifyFlag.set( true );
            }

            public void notifyAll( Request req ) {
                req.log( ObjectMonitorLogMessageFactory.INSTANCE.threadCalledNotifyAll(Thread.currentThread().getName(),this.toString()) );

                notifyFlag.set( true );
            }

            public <R> R withinSynchronizedBlock( Function0<R> exclusiveBlock ) {
                synchronized (this) {
                    return exclusiveBlock.invoke();
                }
            }
        };
    }

    public static ObjectMonitor createObjectMonitor() {
        return createObjectMonitorFor( new Object() );
    }

    public static ObjectMonitor createObjectMonitorFor( Object obj ) {
        return new ObjectMonitor() {
            public void sleep( Request req, long millis ) throws InterruptedException {
                req.log( ObjectMonitorLogMessageFactory.INSTANCE.threadGoingToSleep(Thread.currentThread().getName(),millis) );

                Thread.sleep( millis );
            }

            public void wait( Request req, long millis ) throws InterruptedException {
                synchronized (obj) {
                    req.log( ObjectMonitorLogMessageFactory.INSTANCE.threadWaitingAgainstMonitor(Thread.currentThread().getName(),millis,this.toString()) );

                    obj.wait( millis ); // NB java will now release the lock that it holds on obj, even if the lock is held more than once
                                        // it will be reacquired before this thread wakes up and continues
                }
            }

            public void notify( Request req ) {
                synchronized (obj) {
                    req.log( ObjectMonitorLogMessageFactory.INSTANCE.threadCalledNotify(Thread.currentThread().getName(),this.toString()) );

                    obj.notify();
                }
            }

            public void notifyAll( Request req ) {
                synchronized (obj) {
                    req.log( ObjectMonitorLogMessageFactory.INSTANCE.threadCalledNotifyAll(Thread.currentThread().getName(),this.toString()) );

                    obj.notifyAll();
                }
            }

            public <R> R withinSynchronizedBlock( Function0<R> exclusiveBlock ) {
                synchronized (obj) {
                    return exclusiveBlock.invoke();
                }
            }
        };
    }


    public void sleep( Request req, long millis ) throws InterruptedException;
    public void wait( Request req, long millis ) throws InterruptedException;
    public void notify( Request req );
    public void notifyAll( Request req );

    public <R> R withinSynchronizedBlock( Function0<R> exclusiveBlock );
}
