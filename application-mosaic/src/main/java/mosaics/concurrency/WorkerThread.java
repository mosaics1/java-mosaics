package mosaics.concurrency;

import mosaics.cli.Env;
import mosaics.cli.requests.Request;
import mosaics.lang.functions.LongFunction0;
import mosaics.lang.lifecycle.StartStoppable;


/**
 * Starts a thread which polls a service method at regular intervals.
 */
public class WorkerThread extends StartStoppable<WorkerThread> {

    public static final long EXIT = Long.MIN_VALUE;


    private Env           env;
    private Thread        thread;
    private ThreadType    threadType = ThreadType.NON_DAEMON;
    private SleepGuard    sleepGuard;
    private LongFunction0 jobFunction;

    /**
     *
     * @param jobFunction perform this threads work and return how long in milliseconds the thread is to sleep
     *                    between invocations of this method.. 0 means no wait and returning the magic value of EXIT
     *                    signals that the thread is to finish
     */
    public WorkerThread( Env env, String serviceName, LongFunction0 jobFunction ) {
        this( env, serviceName, jobFunction, ObjectMonitor.createObjectMonitor() );
    }

    public WorkerThread( Env env, String serviceName, LongFunction0 jobFunction, ObjectMonitor monitor ) {
        super( serviceName );

        this.env         = env;
        this.jobFunction = jobFunction;
        this.sleepGuard  = new SleepGuard( monitor, serviceName, 1_000 );
    }


    protected WorkerThread asDaemon() {
        throwIfRunning();

        this.threadType = ThreadType.DAEMON;

        return this;
    }

    protected WorkerThread withMaxIntervalBetweenPolls( long maxSleepMillis ) {
        this.sleepGuard.setMaxSleepMillis( maxSleepMillis );

        return this;
    }

    /**
     * If this thread is asleep, wake it up so that it can call the jobFunction.  It is likely that
     * there is now work there for it to perform.
     */
    protected void notifyWorkerThread( Request req ) {
        this.sleepGuard.wakeup( req );
    }


    protected void doStart() {
        this.thread = new Thread( getName() ) {
            public void run() {
                Request req = env.createRequest( this.getName() );

                long sleepMillis = 0;
                while ( isRunning() && sleepMillis != EXIT ) {
                    try {
                        sleepMillis = jobFunction.invoke();
                    } catch ( Throwable ex ) {
                        req.log( ThreadLogMessageFactory.INSTANCE.threadErrored(getName(),ex) );

                        sleepMillis = 5_000;
                    }

                    if ( sleepMillis > 0 ) {
                        sleepGuard.sleep( req, sleepMillis );
                    }
                }
            }
        };

        this.thread.setDaemon( threadType.isDaemon() );

        this.thread.start();
    }

    protected void doStop() throws Exception {
        if ( this.thread != null ) {
            this.thread.interrupt();
            this.thread.join(10_000);

            this.thread = null;
        }
    }

}
