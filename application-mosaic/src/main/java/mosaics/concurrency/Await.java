package mosaics.concurrency;

import mosaics.concurrency.futures.Future;
import mosaics.concurrency.futures.UnexpectedResultException;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.Value;
import lombok.With;
import mosaics.cli.Env;
import mosaics.cli.requests.Request;
import mosaics.fp.FP;
import mosaics.fp.Failure;
import mosaics.fp.Try;
import mosaics.fp.Tryable;
import mosaics.fp.collections.FPOption;
import mosaics.lang.Backdoor;
import mosaics.lang.threads.STWDetector;

import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicReference;


@Value
@With
@AllArgsConstructor
@ToString(exclude = {"scheduler"})
@EqualsAndHashCode
public class Await {

    private long      maxWaitMillis;
    private long      pollIntervalMillis;
    private Scheduler scheduler;


    public Await( Env env ) {
        this( new SystemScheduler(env, "AwaitScheduler") );
    }

    public Await( Scheduler scheduler ) {
        this( 5_000, 1, scheduler );
    }


    /**
     *
     * @throws TimeoutException future did not complete in time
     */
    public <T> T result( Request req, Future<T> f ) {
        AtomicReference<FPOption<Tryable<T>>> result = new AtomicReference<>(FP.emptyOption());

        f.onSuccess( (r,v) -> result.set(FP.option(Try.succeeded(v))) )
         .onFailure( (r,failure) -> result.set(FP.option(Try.failed(failure))) );

        spinUntilResultReceivedOrTimeoutReached( req, f, result );

        return result.get().get().getResult();
    }

    /**
     *
     * @throws TimeoutException future did not complete in time
     * @throws UnexpectedResultException future completed successfully with a result
     */
    public <T> Failure failure( Request req, Future<T> f ) {
        AtomicReference<FPOption<Tryable<Failure>>> result = new AtomicReference<>(FP.emptyOption());

        f.onSuccess( (r,v) -> result.set(FP.option(Try.failed(new UnexpectedResultException(v)))) )
         .onFailure( (r,failure) -> result.set(FP.option(Try.succeeded(failure))) );

        spinUntilResultReceivedOrTimeoutReached( req, f, result );

        return result.get().get().getResult();
    }


    private <T,B> void spinUntilResultReceivedOrTimeoutReached( Request req, Future<T> f, AtomicReference<FPOption<Tryable<B>>> result ) {
        long startMillis   = scheduler.currentTimeMillis();
        long abortAtMillis = startMillis + maxWaitMillis;
        STWDetector stwDetector = STWDetector.global();
        long delaySoFar = stwDetector.getTotalDelaySoFarMillis();

        while ( result.get().isEmpty() ) {
            long delayDelta = stwDetector.getTotalDelaySoFarMillis() - delaySoFar;
            if ( abortAtMillis+delayDelta <= scheduler.currentTimeMillis() ) {
                throw Backdoor.throwException( new TimeoutException("Timedout waiting for " + f) );
            }

            scheduler.sleep( req, pollIntervalMillis );
        }
    }
}
