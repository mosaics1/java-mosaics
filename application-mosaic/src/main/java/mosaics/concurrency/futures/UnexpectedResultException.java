package mosaics.concurrency.futures;

public class UnexpectedResultException extends RuntimeException {
    private Object result;

    public UnexpectedResultException( Object result ) {
        super( "Unexpected result: " + result );

        this.result = result;
    }
}
