package mosaics.concurrency.futures;

import lombok.AllArgsConstructor;
import mosaics.cli.requests.Request;
import mosaics.fp.ExceptionFailure;
import mosaics.fp.Failure;
import mosaics.fp.Tryable;
import mosaics.fp.collections.ConsList;
import mosaics.lang.functions.Function1;
import mosaics.lang.functions.Function2;
import mosaics.lang.functions.VoidFunction2;

import java.util.Objects;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicReference;

import static mosaics.fp.collections.ConsList.nil;
import static mosaics.lang.Backdoor.cast;


public class Promise<T> implements Future<T> {

    private final AtomicReference<PromiseHandler<T>> currentState;


    public Promise( Request req ) {
        this.currentState = new AtomicReference<>( new WaitingForResultHandler<>(req,this, null) );
    }

    public Promise( Request req, String description ) {
        this.currentState = new AtomicReference<>( new WaitingForResultHandler<>(req,this, description) );
    }

    public Promise( Executor executor, Request req ) {
        this.currentState = new AtomicReference<>( new WaitingForResultHandler<>(req, executor, this, null) );
    }

    public Promise( Executor executor, Request req, String description ) {
        this.currentState = new AtomicReference<>( new WaitingForResultHandler<>(req, executor, this, description) );
    }

    public Promise( Executor executor, Request req, Future sourceFuture, String description ) {
        this.currentState = new AtomicReference<>( new WaitingForResultHandler<>(req, executor, sourceFuture, description) );
    }


    public void setResult( T v ) {
        transition( h -> h.setResult( v ) );
    }

    public void setFailure( Failure f ) {
        transition( h -> h.setFailure( f ) );
    }

    public void complete( Tryable<T> tryable ) {
        tryable.onSuccess( this::setResult )
            .onFailure( this::setFailure );
    }


    public boolean isComplete() {
        return currentState.get().isComplete();
    }

    public boolean hasResult() {
        return currentState.get().hasResult();
    }

    public boolean hasFailure() {
        return currentState.get().hasFailure();
    }

    public Future<T> withExecutor( Executor executor ) {
        return transition( h -> h.withExecutor(executor) );
    }

    public Future<T> withDescription( String description ) {
        return transition( h -> h.withDescription(description) );
    }

    public <B> Future<B> mapResult( Function2<Request,T, B> mappingFunction ) {
        return transition( h -> h.mapResult(mappingFunction) );
    }

    public <B> Future<B> mapResultToTryable( Function2<Request,T, Tryable<B>> mappingFunction ) {
        return transition( h -> h.mapResultToTryable(mappingFunction) );
    }

    public <B> Future<B> flatMapResult( Function2<Request,T, Future<B>> mappingFunction ) {
        return transition( h -> h.flatMapResult(mappingFunction) );
    }

    public Future<T> recover( Function2<Request,Failure, T> recoveryFunction ) {
        return transition( h -> h.recover(recoveryFunction) );
    }

    public Future<T> flatRecover( Function2<Request,Failure, Future<T>> recoveryFunction ) {
        return transition( h -> h.flatRecover(recoveryFunction) );
    }

    public Future<T> mapFailure( Function2<Request,Failure, Failure> mappingFunction ) {
        return transition( h -> h.mapFailure(mappingFunction) );
    }

    public Future<T> flatMapFailure( Function2<Request,Failure, Future<Failure>> mappingFunction ) {
        return transition( h -> h.flatMapFailure(mappingFunction) );
    }

    public Future<T> onSuccess( VoidFunction2<Request,T> callback ) {
        return transition( h -> h.onSuccess(callback) );
    }

    public Future<T> onFailure( VoidFunction2<Request,Failure> callback ) {
        return transition( h -> h.onFailure(callback) );
    }

    public String toString() {
        return currentState.get().toString();
    }

    private <B> Future<B> transition( Function1<PromiseHandler<T>,PromiseHandler<T>> calcNextState ) {
        PromiseHandler<T> prev, next;

        do {
            prev = currentState.get();
            next = calcNextState.invoke(prev);
        } while (!currentState.compareAndSet(prev, next));

        if ( prev != next ) {
            next.enterState();
        }

        Future<B> nextFuture = next.nextFuture();
        return nextFuture == null ? cast(this) : nextFuture;
    }

}


interface PromiseHandler<T> {
    public PromiseHandler<T> setResult( T v );
    public PromiseHandler<T> setFailure( Failure f );

    public void enterState();

    public <B> Future<B> nextFuture();

    public boolean isComplete();
    public boolean hasResult();
    public boolean hasFailure();

    public PromiseHandler<T> withExecutor( Executor executor );
    public PromiseHandler<T> withDescription( String description );
    public <B> PromiseHandler<T> mapResult( Function2<Request,T, B> mappingFunction );
    public <B> PromiseHandler<T> mapResultToTryable( Function2<Request,T, Tryable<B>> mappingFunction );
    public <B> PromiseHandler<T> flatMapResult( Function2<Request,T, Future<B>> mappingFunction );
    public PromiseHandler<T> recover( Function2<Request,Failure, T> recoveryFunction );
    public PromiseHandler<T> flatRecover( Function2<Request,Failure, Future<T>> recoveryFunction );
    public PromiseHandler<T> mapFailure( Function2<Request,Failure, Failure> mappingFunction );
    public PromiseHandler<T> flatMapFailure( Function2<Request,Failure, Future<Failure>> mappingFunction );
    public PromiseHandler<T> onSuccess( VoidFunction2<Request,T> callback );
    public PromiseHandler<T> onFailure( VoidFunction2<Request,Failure> callback );
}

@AllArgsConstructor
class WaitingForResultHandler<T> implements PromiseHandler<T> {

    private Request                                  request;
    private ConsList<VoidFunction2<Request,T>>       onSuccessCallbacks;
    private ConsList<VoidFunction2<Request,Failure>> onFailureCallbacks;
    private Future                                   sourceFuture;
    private Future                                   nextFuture;
    private Executor                                 executor;
    private String                                   description;

    public WaitingForResultHandler( Request req, Future sourceFuture, String description ) {
        this( req, nil(), nil(), sourceFuture, null, UseCallingThreadExecutor.INSTANCE, description );
    }

    public WaitingForResultHandler( Request req, Executor executor, Future sourceFuture, String description ) {
        this( req, nil(), nil(), sourceFuture, null, executor, description );
    }


    public PromiseHandler<T> setResult( T v ) {
        return new HasResultHandler<>( request, onSuccessCallbacks, v, UseCallingThreadExecutor.INSTANCE, sourceFuture, null, description );
    }

    public PromiseHandler<T> setFailure( Failure f ) {
        return new HasFailureHandler<>( request, onFailureCallbacks, f, UseCallingThreadExecutor.INSTANCE, sourceFuture, null, description );
    }


    public void enterState() {}

    public <B> Future<B> nextFuture() {
        return cast(nextFuture);
    }


    public boolean isComplete() {
        return false;
    }

    public boolean hasResult() {
        return false;
    }

    public boolean hasFailure() {
        return false;
    }


    public PromiseHandler<T> withExecutor( Executor newExecutor ) {
        return newExecutor == this.executor && nextFuture == null ? this : new WaitingForResultHandler<>( request, onSuccessCallbacks, onFailureCallbacks, sourceFuture, null, newExecutor, description );
    }

    public PromiseHandler<T> withDescription( String newDescription ) {
        return Objects.equals(this.description,newDescription) ? this : new WaitingForResultHandler<>( request, onSuccessCallbacks, onFailureCallbacks, sourceFuture, null, executor, newDescription );
    }

    public <B> PromiseHandler<T> mapResult( Function2<Request,T, B> mappingFunction ) {
        return this.<B>registerResultHandler( (promise,v) -> promise.setResult(mappingFunction.invoke(request,v)) );
    }

    public <B> PromiseHandler<T> mapResultToTryable( Function2<Request,T, Tryable<B>> mappingFunction ) {
        return this.<B>registerResultHandler( (promise,v) -> promise.complete(mappingFunction.invoke(request,v)) );
    }

    public <B> PromiseHandler<T> flatMapResult( Function2<Request,T, Future<B>> mappingFunction ) {
        return this.<B>registerResultHandler( (promise,v) -> mappingFunction.invoke(request,v).chainToPromise(promise) );
    }

    public PromiseHandler<T> recover( Function2<Request,Failure, T> recoveryFunction ) {
        return this.<T>registerFailureHandler( (promise,f) -> promise.setResult(recoveryFunction.invoke(request,f)) );
    }

    public PromiseHandler<T> flatRecover( Function2<Request,Failure, Future<T>> recoveryFunction ) {
        return this.<T>registerFailureHandler( (promise,f) -> recoveryFunction.invoke(request,f).chainToPromise(promise) );
    }

    public PromiseHandler<T> mapFailure( Function2<Request,Failure, Failure> mappingFunction ) {
        return this.<T>registerFailureHandler( (promise,f) -> promise.setFailure(mappingFunction.invoke(request,f)) );
    }

    public PromiseHandler<T> flatMapFailure( Function2<Request,Failure, Future<Failure>> mappingFunction ) {
        return this.<T>registerFailureHandler( (promise,f) -> mappingFunction.invoke(request,f).chainToPromise(cast(promise)) );
    }

    public PromiseHandler<T> onSuccess( VoidFunction2<Request,T> f ) {
        return this.withOnSuccessCallback( (r,v) ->
            executor.execute( () -> {
                try {
                    f.invoke( request, v );
                } catch ( Throwable ex ) {
                    request.log( PromiseLogMessageFactory.INSTANCE.futureSideEffectErrored(ex) );
                }
            } )
        );
    }

    public PromiseHandler<T> onFailure( VoidFunction2<Request,Failure> f ) {
        return this.withOnFailureCallback( (r,failure) ->
            executor.execute( () -> {
                try {
                    f.invoke( request, failure );
                } catch ( Throwable ex ) {
                    request.log( PromiseLogMessageFactory.INSTANCE.futureSideEffectErrored(ex) );
                }
            } )
        );
    }

    public String toString() {
        return description == null ? "WaitingPromise()" : description;
    }

    private WaitingForResultHandler<T> withOnSuccessCallback( VoidFunction2<Request,T> f ) {
        return withOnSuccessCallback( f, null );
    }

    private WaitingForResultHandler<T> withOnSuccessCallback( VoidFunction2<Request,T> f, Promise nextFuture ) {
        ConsList<VoidFunction2<Request,Failure>> failureCallbacks = onFailureCallbacks;

        if ( nextFuture != null ) {
            failureCallbacks = failureCallbacks.append( (r,failure) -> nextFuture.setFailure(failure) );
        }

        return new WaitingForResultHandler<>(
            request,
            onSuccessCallbacks.append(f),
            failureCallbacks,
            sourceFuture,
            nextFuture,
            executor,
            description
        );
    }

    private WaitingForResultHandler<T> withOnFailureCallback( VoidFunction2<Request,Failure> f ) {
        return withOnFailureCallback( f, null );
    }

    private <B> WaitingForResultHandler<T> withOnFailureCallback( VoidFunction2<Request,Failure> f, Promise<B> nextFuture ) {
        ConsList<VoidFunction2<Request,T>> successCallbacks = onSuccessCallbacks;

        if ( nextFuture != null ) {
            successCallbacks = successCallbacks.append( (r,v) -> nextFuture.setResult(cast(v)) );
        }

        return new WaitingForResultHandler<>( request, successCallbacks, onFailureCallbacks.append(f), sourceFuture, nextFuture, executor, description );
    }



    private <B> PromiseHandler<T> registerResultHandler( VoidFunction2<Promise<B>,T> f ) {
        if ( request.hasRequestFinished() ) {
            return new TimedoutHandler<>( request, sourceFuture, (Failure) null, description );
        }

        Promise<B> promise = new Promise<>(UseCallingThreadExecutor.INSTANCE, request);

        return this.withOnSuccessCallback( (r,v) -> executor.execute( () -> {
            if ( request.hasRequestFinished() ) {
                promise.setFailure( new TimeoutFailure(request,promise) );

                return;
            }

            try {
                f.invoke( promise, v );
            } catch ( Throwable ex ) {
                promise.setFailure( new ExceptionFailure( ex ) );
            }
        } ),
            promise
        );
    }

    private <B> PromiseHandler<T> registerFailureHandler( VoidFunction2<Promise<B>,Failure> f ) {
        if ( request.hasRequestFinished() ) {
            return new TimedoutHandler<>( request, sourceFuture, (Failure) null, description );
        }

        Promise<B> promise = new Promise<>(UseCallingThreadExecutor.INSTANCE, request);

        return this.withOnFailureCallback( (r,failure) -> executor.execute( () -> {
            if ( request.hasRequestFinished() ) {
                promise.setFailure( new TimeoutFailure(request,promise) );

                return;
            }

            try {
                f.invoke( promise, failure );
            } catch ( Throwable ex ) {
                promise.setFailure( new ExceptionFailure(ex,failure) );
            }
        } ),
            promise
        );
    }

}


@AllArgsConstructor
class HasResultHandler<T> implements PromiseHandler<T> {
    private Request                            request;
    private ConsList<VoidFunction2<Request,T>> onSuccessCallbacks;
    private T                                  result;
    private Executor                           executor;
    private Future                             sourceFuture;
    private Future                             nextFuture;
    private String                             description;


    public PromiseHandler<T> setResult( T v ) {
        request.log( PromiseLogMessageFactory.INSTANCE.promiseAlreadyCompleted() );

        throw new UnsupportedOperationException();
    }

    public PromiseHandler<T> setFailure( Failure f ) {
        request.log( PromiseLogMessageFactory.INSTANCE.promiseAlreadyCompleted() );

        throw new UnsupportedOperationException();
    }

    public void enterState() {
        onSuccessCallbacks.forEach( callback -> {
            try {
                callback.invoke(request,result);
            } catch ( Throwable ex ) {
                request.log( PromiseLogMessageFactory.INSTANCE.futureSideEffectErrored(ex) );
            }
        });

        onSuccessCallbacks = nil();
    }


    public <B> Future<B> nextFuture() {
        return cast(nextFuture);
    }

    public boolean isComplete() {
        return true;
    }

    public boolean hasResult() {
        return true;
    }

    public boolean hasFailure() {
        return false;
    }

    public PromiseHandler<T> withExecutor( Executor newExecutor ) {
        return cloneThisWithNoCallbacks( newExecutor, description );
    }

    public PromiseHandler<T> withDescription( String newDescription ) {
        return cloneThisWithNoCallbacks( executor, newDescription );
    }


    public <B> PromiseHandler<T> mapResult( Function2<Request,T, B> mappingFunction ) {
        return this.<B>scheduleDelayedCallback( (promise,result) -> promise.setResult(mappingFunction.invoke(request,result)) );
    }

    public <B> PromiseHandler<T> mapResultToTryable( Function2<Request,T, Tryable<B>> mappingFunction ) {
        return this.<B>scheduleDelayedCallback( (promise,result) -> promise.complete(mappingFunction.invoke(request,result)) );
    }

    public <B> PromiseHandler<T> flatMapResult( Function2<Request,T, Future<B>> mappingFunction ) {
        return this.<B>scheduleDelayedCallback( (promise,result) -> mappingFunction.invoke(request,result).chainToPromise(promise) );
    }

    public PromiseHandler<T> recover( Function2<Request,Failure, T> recoveryFunction ) {
        return cloneThisWithNoCallbacks();
    }

    public PromiseHandler<T> flatRecover( Function2<Request,Failure, Future<T>> recoveryFunction ) {
        return cloneThisWithNoCallbacks();
    }

    public PromiseHandler<T> mapFailure( Function2<Request,Failure, Failure> mappingFunction ) {
        return cloneThisWithNoCallbacks();
    }

    public PromiseHandler<T> flatMapFailure( Function2<Request,Failure, Future<Failure>> mappingFunction ) {
        return cloneThisWithNoCallbacks();
    }

    public PromiseHandler<T> onSuccess( VoidFunction2<Request,T> callback ) {
        return new HasResultHandler<>( request, ConsList.singleton(callback), result, executor, sourceFuture,null, description );
    }

    public PromiseHandler<T> onFailure( VoidFunction2<Request,Failure> callback ) {
        return cloneThisWithNoCallbacks();
    }

    public String toString() {
        return description == null ? "Future("+result+")" : description;
    }

    private HasResultHandler<T> cloneThisWithNoCallbacks() {
        return cloneThisWithNoCallbacks( executor, description );
    }

    private HasResultHandler<T> cloneThisWithNoCallbacks( Executor newExecutor, String description ) {
        if ( this.executor == newExecutor && this.onSuccessCallbacks.isEmpty() && nextFuture == null && Objects.equals(this.description,description)) {
            return this;
        }

        return new HasResultHandler<>( request, ConsList.nil(), result, newExecutor, sourceFuture,null, description );
    }

    private <B> PromiseHandler<T> scheduleDelayedCallback( VoidFunction2<Promise<B>,T> f ) {
        if ( request.hasRequestFinished() ) {
            return new TimedoutHandler<>( request, sourceFuture, (Failure) null, description );
        }

        Promise<B> promise = new Promise<>(UseCallingThreadExecutor.INSTANCE, request, sourceFuture, description);

        VoidFunction2<Request,T> onSuccessCallback = (r,v) -> executor.execute( () -> {
            if ( r.hasRequestFinished() ) {
                promise.setFailure( new TimeoutFailure(r,promise) );
            } else {
                try {
                    f.invoke( promise, v );
                } catch ( Throwable ex ) {
                    promise.setFailure( new ExceptionFailure( ex ) );
                }
            }
        } );

        return new HasResultHandler<>( request, ConsList.singleton(onSuccessCallback), result, executor, sourceFuture, promise, description );
    }

}

@AllArgsConstructor
class HasFailureHandler<T> implements PromiseHandler<T> {
    private Request                                  request;
    private ConsList<VoidFunction2<Request,Failure>> onFailureCallbacks;
    private Failure                                  failure;
    private Executor                                 executor;
    private Future                                   sourceFuture;
    private Future                                   nextFuture;
    private String                                   description;

    public PromiseHandler<T> setResult( T v ) {
        request.log( PromiseLogMessageFactory.INSTANCE.promiseAlreadyCompleted() );

        throw new UnsupportedOperationException();
    }

    public PromiseHandler<T> setFailure( Failure f ) {
        if ( !(failure instanceof TimeoutFailure) ) {
            request.log( PromiseLogMessageFactory.INSTANCE.promiseAlreadyCompleted() );

            throw new UnsupportedOperationException();
        } else {
            return this;
        }
    }

    public void enterState() {
        onFailureCallbacks.forEach( callback -> {
            try {
                callback.invoke(request, failure);
            } catch ( Throwable ex ) {
                request.log( PromiseLogMessageFactory.INSTANCE.futureSideEffectErrored(ex) );
            }
        } );

        onFailureCallbacks = nil();
    }

    public <B> Future<B> nextFuture() {
        return cast(nextFuture);
    }

    public boolean isComplete() {
        return true;
    }

    public boolean hasResult() {
        return false;
    }

    public boolean hasFailure() {
        return true;
    }

    public PromiseHandler<T> withExecutor( Executor executor ) {
        return cloneThisWithNoCallbacks( executor, description );
    }

    public PromiseHandler<T> withDescription( String description ) {
        return cloneThisWithNoCallbacks( executor, description );
    }

    public <B> PromiseHandler<T> mapResult( Function2<Request,T, B> mappingFunction ) {
        return cloneThisWithNoCallbacks();
    }

    public <B> PromiseHandler<T> mapResultToTryable( Function2<Request,T, Tryable<B>> mappingFunction ) {
        return cloneThisWithNoCallbacks();
    }

    public <B> PromiseHandler<T> flatMapResult( Function2<Request,T, Future<B>> mappingFunction ) {
        return cloneThisWithNoCallbacks();
    }

    public PromiseHandler<T> recover( Function2<Request,Failure, T> recoveryFunction ) {
        return this.<T>scheduleDelayedCallback( (promise,failure) -> promise.setResult(recoveryFunction.invoke(request,failure)) );
    }

    public PromiseHandler<T> flatRecover( Function2<Request,Failure, Future<T>> recoveryFunction ) {
        return this.<T>scheduleDelayedCallback( (promise,failure) -> recoveryFunction.invoke(request,failure).chainToPromise(promise) );
    }

    public PromiseHandler<T> mapFailure( Function2<Request,Failure, Failure> mappingFunction ) {
        return this.<T>scheduleDelayedCallback( (promise,failure) -> promise.setFailure(mappingFunction.invoke(request,failure)) );
    }

    public PromiseHandler<T> flatMapFailure( Function2<Request,Failure, Future<Failure>> mappingFunction ) {
        return this.<T>scheduleDelayedCallback( (promise,failure) -> mappingFunction.invoke(request,failure).chainToPromise(cast(promise)) );
    }

    public PromiseHandler<T> onSuccess( VoidFunction2<Request,T> callback ) {
        return cloneThisWithNoCallbacks();
    }

    public PromiseHandler<T> onFailure( VoidFunction2<Request,Failure> callback ) {
        return new HasFailureHandler<>( request, ConsList.singleton(callback), failure, executor, sourceFuture,null, description );
    }

    public String toString() {
        return description == null ? "FailedFuture("+failure+")" : description;
    }

    private HasFailureHandler<T> cloneThisWithNoCallbacks() {
        return cloneThisWithNoCallbacks( executor, description );
    }

    private HasFailureHandler<T> cloneThisWithNoCallbacks( Executor newExecutor, String description ) {
        if ( this.executor == newExecutor && this.onFailureCallbacks.isEmpty() && nextFuture == null && Objects.equals(this.description,description) ) {
            return this;
        }

        return new HasFailureHandler<>( request, ConsList.nil(), failure, newExecutor, sourceFuture,null, description );
    }

    private <B> PromiseHandler<T> scheduleDelayedCallback( VoidFunction2<Promise<B>,Failure> f ) {
        if ( request.hasRequestFinished() ) {
            return new TimedoutHandler<>( request, sourceFuture, failure, description );
        }

        Promise<B> promise = new Promise<>(UseCallingThreadExecutor.INSTANCE, request, sourceFuture, description);

        VoidFunction2<Request,Failure> onFailureCallback = (r,failure) -> executor.execute( () -> {
            if ( r.hasRequestFinished() ) {
                promise.setFailure( new TimeoutFailure(r, sourceFuture, failure) );
            } else {
                try {
                    f.invoke( promise, failure );
                } catch ( Throwable ex ) {
                    promise.setFailure( new ExceptionFailure( ex, failure ) );
                }
            }
        } );

        return new HasFailureHandler<>( request, ConsList.singleton(onFailureCallback), failure, executor, sourceFuture, promise, description );
    }

}


@AllArgsConstructor
class TimedoutHandler<T> implements PromiseHandler<T> {

    private Request   request;
    private Future    sourceFuture;
    private Future<T> timedoutFuture;
    private String    description;

    TimedoutHandler( Request request, Future sourceFuture, Failure failure, String description ) {
        this.request        = request;
        this.sourceFuture   = sourceFuture;
        this.timedoutFuture = Future.failure( request, new TimeoutFailure(request, sourceFuture, failure), description );
        this.description    = description;
    }

    public PromiseHandler<T> setResult( T v ) {
        request.log( PromiseLogMessageFactory.INSTANCE.promiseResultDiscardedDueToTimeout(v) );

        return this;
    }

    public PromiseHandler<T> setFailure( Failure f ) {
        request.log( PromiseLogMessageFactory.INSTANCE.promiseFailureDiscardedDueToTimeout(f) );

        return this;
    }

    public void enterState() {}

    public <B> Future<B> nextFuture() {
        return cast(timedoutFuture);
    }

    public boolean isComplete() {
        return true;
    }

    public boolean hasResult() {
        return false;
    }

    public boolean hasFailure() {
        return true;
    }

    public PromiseHandler<T> withExecutor( Executor executor ) {
        return this;
    }

    public PromiseHandler<T> withDescription( String description ) {
        if ( Objects.equals(this.description,description) ) {
            return this;
        } else {
            return new TimedoutHandler<>( request, sourceFuture, timedoutFuture, description );
        }
    }

    public <B> PromiseHandler<T> mapResult( Function2<Request, T, B> mappingFunction ) {
        return this;
    }

    public <B> PromiseHandler<T> mapResultToTryable( Function2<Request, T, Tryable<B>> mappingFunction ) {
        return this;
    }

    public <B> PromiseHandler<T> flatMapResult( Function2<Request, T, Future<B>> mappingFunction ) {
        return this;
    }

    public PromiseHandler<T> recover( Function2<Request, Failure, T> recoveryFunction ) {
        return this;
    }

    public PromiseHandler<T> flatRecover( Function2<Request, Failure, Future<T>> recoveryFunction ) {
        return this;
    }

    public PromiseHandler<T> mapFailure( Function2<Request, Failure, Failure> mappingFunction ) {
        return this;
    }

    public PromiseHandler<T> flatMapFailure( Function2<Request, Failure, Future<Failure>> mappingFunction ) {
        return this;
    }

    public PromiseHandler<T> onSuccess( VoidFunction2<Request, T> callback ) {
        return this;
    }

    public PromiseHandler<T> onFailure( VoidFunction2<Request, Failure> callback ) {
        return this;
    }

    public String toString() {
        return description == null ? "TimedOutFuture()" : description;
    }
}
