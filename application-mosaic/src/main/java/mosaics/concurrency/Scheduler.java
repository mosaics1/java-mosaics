package mosaics.concurrency;


import mosaics.cli.requests.Request;
import mosaics.lang.functions.VoidFunction0WithThrows;
import mosaics.lang.lifecycle.Subscription;

import java.time.Duration;


/**
 * A scheduler manages when a piece of work is to occur.
 */
public interface Scheduler {

    public long currentTimeMillis();
    public void sleep( Request req, long millis );

//    public <T> CompletionStage<T> sleepAsync( long millis, CompletionStage<T> job );


    public Subscription scheduleOnce( Request req, long delayMillis, VoidFunction0WithThrows callback );

    public default Subscription scheduleOnce( Request req, Duration delay, VoidFunction0WithThrows callback ) {
        return scheduleOnce( req, delay.toMillis(), callback );
    }

}
