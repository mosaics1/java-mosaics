package mosaics.concurrency;

import mosaics.logging.LogMessage;
import mosaics.logging.LogMessageFactory;
import mosaics.logging.LogMessageProxyFactory;
import mosaics.logging.loglevels.DevInfo;


public interface ObjectMonitorLogMessageFactory extends LogMessageFactory {
    public static final ObjectMonitorLogMessageFactory INSTANCE = LogMessageProxyFactory.INSTANCE.createLogMessageFactory( ObjectMonitorLogMessageFactory.class );

    @DevInfo("Thread $threadName going to sleep for ${millis:duration}.")
    public LogMessage threadGoingToSleep( String threadName, long millis );

    @DevInfo("Thread $threadName waiting for ${millis:duration} on $objectDescription.")
    public LogMessage threadWaitingAgainstMonitor( String threadName, long millis, String objectDescription );

    @DevInfo("Monitor $objectDescription notified by $threadName.")
    public LogMessage threadCalledNotify( String threadName, String objectDescription );

    @DevInfo("Thread $threadName notified all on $objectDescription.")
    public LogMessage threadCalledNotifyAll( String threadName, String objectDescription );
}
