package mosaics.concurrency;

import mosaics.logging.LogMessage;
import mosaics.logging.LogMessageFactory;
import mosaics.logging.LogMessageProxyFactory;
import mosaics.logging.loglevels.DevInfo;
import mosaics.logging.loglevels.OpsError;
import mosaics.logging.loglevels.OpsInfo;


public interface SystemSchedulerLogMessageFactory extends LogMessageFactory {
    public static final SystemSchedulerLogMessageFactory INSTANCE = LogMessageProxyFactory.INSTANCE.createLogMessageFactory( SystemSchedulerLogMessageFactory.class );

    @DevInfo("Job $jobDesc scheduled to run at ${scheduleAt:dtm}")
    public LogMessage jobScheduled( String jobDesc, long scheduleAt );

    @DevInfo("Job $jobDesc cancelled ${millisEarly:duration} early")
    public LogMessage jobCancelled( String jobDesc, long millisEarly );

    @DevInfo(
        "Scheduler $schedulerName processed $processedCount jobs in " +
            "${durationMillis:duration}; of which ${stwMillis:duration} was JVM/OS stalls"
    )
    public LogMessage schedulerRan( String schedulerName, int processedCount, long durationMillis, long stwMillis );


    @OpsInfo(
        "Job $jobDesc invoked successfully.  It started ${millisLate:duration} late " +
            "and took ${totalDurationMillis:duration} to complete; of which ${stwMillis:duration} was due " +
            "to full JVM/OS freezes."
    )
    public LogMessage jobInvokedSuccessfully( String jobDesc, long totalDurationMillis, long millisLate, long stwMillis );

    @OpsError("Job $jobDesc failed: $ex")
    public LogMessage jobFailed( String jobDescription, Throwable ex );
}
