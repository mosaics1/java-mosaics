package mosaics.concurrency;

import mosaics.logging.LogMessage;
import mosaics.logging.LogMessageFactory;
import mosaics.logging.LogMessageProxyFactory;
import mosaics.logging.loglevels.DevInfo;
import mosaics.logging.loglevels.OpsError;
import mosaics.logging.loglevels.OpsFatal;


public interface ThreadLogMessageFactory extends LogMessageFactory {
    public static final ThreadLogMessageFactory INSTANCE = LogMessageProxyFactory.INSTANCE.createLogMessageFactory( ThreadLogMessageFactory.class );

    @OpsError("$threadName threw an unexpected exception: $exception")
    public LogMessage threadErrored( String threadName, Throwable exception );

    @OpsFatal("$threadName threw an unexpected exception that cannot be recovered from: $exception")
    public LogMessage threadFatalError( String threadName, Throwable exception );

    @DevInfo("$threadName received a Thread interruption")
    public LogMessage threadInterrupted( String threadName );
}
