package mosaics.concurrency;

import lombok.Setter;
import mosaics.cli.requests.Request;


/**
 * A concurrency guard that manages putting threads to sleep and waking them up again based
 * on either time or an external event.
 *
 * Effectively this class is a wrapper around wait/notify;  the reason that it exists is that it
 * places a limit on how long wait may be called and it logs InterruptedException.
 */
public class SleepGuard {

            private String        name;
    @Setter private long          maxSleepMillis;
            private ObjectMonitor monitor;


    public SleepGuard( String name ) {
        this( ObjectMonitor.createObjectMonitor(), name, 1_000 );
    }

    public SleepGuard( String name, long maxSleepMillis ) {
        this( ObjectMonitor.createObjectMonitor(), name, maxSleepMillis );
    }

    public SleepGuard( ObjectMonitor objectMonitor, String name, long maxSleepMillis ) {
        this.monitor        = objectMonitor;
        this.name           = name;
        this.maxSleepMillis = maxSleepMillis;
    }

    /**
     * Sleep for the specified period of time.
     */
    public void sleep( Request req, long sleepMillis ) {
        if ( sleepMillis > 0 ) {
            synchronized (this) {
                try {
                    monitor.wait( req, Math.min(maxSleepMillis,sleepMillis) );
                } catch ( InterruptedException e ) {
                    req.log( ThreadLogMessageFactory.INSTANCE.threadInterrupted(name) );
                }
            }
        }
    }

    /**
     * Wakeup any threads that are currently asleep.
     */
    public void wakeup( Request req ) {
        monitor.notifyAll( req );
    }

}

