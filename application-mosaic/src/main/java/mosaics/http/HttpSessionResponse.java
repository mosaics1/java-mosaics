package mosaics.http;

import lombok.Value;


/**
 * A server can request changes to the HttpSession in a response (eg via the Set-Cookie http
 * response header).
 * <p>
 * Thus this DTO contains both the http response and the updated http session.  Because
 * the HttpSession object is immutable, a client can then choose to accept the
 * updated session or ignore it by virtual of whether they use the updated session or not.
 * <p>
 * This also means, that when making concurrent http calls as part of the same session that
 * the changes to the session will only propagate if the sessions are all merged together.
 */
@Value
public class HttpSessionResponse {
    private final HttpSession updatedSession;
    private final HttpResponse response;
}
