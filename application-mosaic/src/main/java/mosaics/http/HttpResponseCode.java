package mosaics.http;

import lombok.Getter;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * @see <a href="https://en.wikipedia.org/wiki/List_of_HTTP_status_codes">Wikipedia documentation</a>
 */
public final class HttpResponseCode {
    private static final Map<Integer,HttpResponseCode> cache = new ConcurrentHashMap<>();

    // 1xx INFORMATIONAL

    /**
     * Server telling client to send payload.  When a payload is large, and there
     * is a risk of the server rejecting it.   The request is split into two calls.
     * One with the headers containing a Expect: 100-continue header (which if
     * okay receives a CONTINUE response) and the second containing the large payload.
     * If the first request returns an error, then the large payload is not sent.
     */
    public static final HttpResponseCode CONTINUE = of(100, "CONTINUE");

    /**
     * The requester has asked the server to switch protocols and the server has
     * agreed to do so.
     */
    public static final HttpResponseCode SWITCHING_PROTOCOLS = of(101, "SWITCHING_PROTOCOLS");

    /**
     *
     */
    public static final HttpResponseCode PROCESSING = of(102, "PROCESSING");
    public static final HttpResponseCode EARLY_HINTS = of(103, "EARLY_HINTS");


    // 2xx SUCCESS

    /**
     * Standard response for successful HTTP requests. The actual response
     * will depend on the request method used. In a GET request, the response
     * will contain an entity corresponding to the requested resource. In a POST
     * request, the response will contain an entity describing or containing the
     * result of the action.
     */
    public static final HttpResponseCode OK = of(200, "OK");

    /**
     * The request has been fulfilled, resulting in the creation of a new resource.
     */
    public static final HttpResponseCode CREATED = of(201, "CREATED");

    /**
     * The request has been accepted for processing, but the processing has not
     * been completed. The request might or might not be eventually acted upon,
     * and may be disallowed when processing occurs.
     */
    public static final HttpResponseCode ACCEPTED = of(202, "ACCEPTED");

    /**
     * The server successfully processed the request, and is not returning any
     * content.
     */
    public static final HttpResponseCode NO_CONTENT = of(204, "NO_CONTENT");

    /**
     * The server is delivering only part of the resource (byte serving) due to a
     * range header sent by the client. The range header is used by HTTP clients to
     * enable resuming of interrupted downloads, or split a download into multiple
     * simultaneous streams.
     */
    public static final HttpResponseCode PARTIAL_CONTENT = of(206, "PARTIAL_CONTENT");

    // 4xx client error

    /**
     * The server cannot or will not process the request due to an apparent
     * client error.
     */
    public static final HttpResponseCode BAD_REQUEST = of(400, "BAD_REQUEST");

    /**
     * User must login or has failed.
     */
    public static final HttpResponseCode UNAUTHORIZED = of(401, "UNAUTHORIZED");

    public static final HttpResponseCode PAYMENT_REQUIRED = of(402, "PAYMENT_REQUIRED");

    /**
     * User lacks permissions.
     */
    public static final HttpResponseCode FORBIDDEN = of(403, "FORBIDDEN");

    /**
     * The requested URL does not exist.
     */
    public static final HttpResponseCode NOT_FOUND = of(404, "NOT_FOUND");

    public static final HttpResponseCode METHOD_NOT_ALLOWED = of(405, "METHOD_NOT_ALLOWED");

    public static final HttpResponseCode NOT_ACCEPTABLE = of(406, "NOT_ACCEPTABLE");

    public static final HttpResponseCode REQUEST_TIMEOUT = of(408, "REQUEST_TIMEOUT");

    public static final HttpResponseCode CONFLICT = of(409, "CONFLICT");

    public static final HttpResponseCode GONE = of(410, "GONE");

    public static final HttpResponseCode LENGTH_REQUIRED = of(411, "LENGTH_REQUIRED");

    public static final HttpResponseCode PRECONDITION_FAILED = of(412, "PRECONDITION_FAILED");

    public static final HttpResponseCode PAYLOAD_TOO_LARGE = of(413, "PAYLOAD_TOO_LARGE");

    public static final HttpResponseCode UNSUPPORTED_MEDIA_TYPE = of(415, "UNSUPPORTED_MEDIA_TYPE");

    public static final HttpResponseCode RANGE_NOT_SATISFIABLE = of(416, "RANGE_NOT_SATISFIABLE");

    /**
     * The server cannot meet the requirements of the Expect request-header field.
     */
    public static final HttpResponseCode EXPECTATION_FAILED = of(417, "EXPECTATION_FAILED");

    public static final HttpResponseCode LOCKED = of(423, "LOCKED");
    public static final HttpResponseCode FAILED_DEPENDENCY = of(424, "FAILED_DEPENDENCY");
    public static final HttpResponseCode TOO_EARLY = of(425, "TOO_EARLY");
    public static final HttpResponseCode UPGRADE_REQUIRED = of(426, "UPGRADE_REQUIRED");
    public static final HttpResponseCode PRECONDITION_REQUIRED = of(428, "PRECONDITION_REQUIRED");
    public static final HttpResponseCode TOO_MANY_REQUESTS = of(429, "TOO_MANY_REQUESTS");
    public static final HttpResponseCode REQUEST_HEADER_FIELDS_TOO_LARGE = of(431, "REQUEST_HEADER_FIELDS_TOO_LARGE");
    public static final HttpResponseCode UNAVAILABLE_FOR_LEGAL_REASONS = of(451, "UNAVAILABLE_FOR_LEGAL_REASONS");


    // 5xx server errors
    public static final HttpResponseCode INTERNAL_SERVER_ERROR = of(500, "INTERNAL_SERVER_ERROR");
    public static final HttpResponseCode NOT_IMPLEMENTED = of(501, "NOT_IMPLEMENTED");
    public static final HttpResponseCode BAD_GATEWAY = of(502, "BAD_GATEWAY");
    public static final HttpResponseCode SERVICE_UNAVAILABLE = of(503, "SERVICE_UNAVAILABLE");
    public static final HttpResponseCode GATEWAY_TIMEOUT = of(504, "GATEWAY_TIMEOUT");
    public static final HttpResponseCode HTTP_VERSION_NOT_SUPPORTED = of(505, "HTTP_VERSION_NOT_SUPPORTED");
    public static final HttpResponseCode INSUFFICIENT_STORAGE = of(507, "INSUFFICIENT_STORAGE");
    public static final HttpResponseCode LOOP_DETECTED = of(508, "LOOP_DETECTED");
    public static final HttpResponseCode NOT_EXTENDED = of(510, "NOT_EXTENDED");
    public static final HttpResponseCode NETWORK_AUTHENTICATION_REQUIRED = of(511, "NETWORK_AUTHENTICATION_REQUIRED");


    public static HttpResponseCode of(int code) {
        return cache.computeIfAbsent( code, key -> new HttpResponseCode(code) );
    }

    public static HttpResponseCode of(int code, String label) {
        return cache.computeIfAbsent( code, key -> new HttpResponseCode(code,label) );
    }


    @Getter private final int    code;
    @Getter private final String label;

    private HttpResponseCode( int code ) {
        this( code, null );
    }

    private HttpResponseCode( int code, String label ) {
        this.code = code;
        this.label = label;
    }

    public int toInt() {
        return code;
    }

    public int hashCode() {
        return code;
    }

    public String toString() {
        return label == null ? Integer.toString(code) : code + " " + label;
    }

    public boolean is404() {
        return code == 404;
    }
}
