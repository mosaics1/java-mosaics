package mosaics.http.jdkclient;

import lombok.SneakyThrows;
import mosaics.cli.requests.Request;
import mosaics.fp.collections.FPOption;
import mosaics.http.HttpCookies;
import mosaics.http.HttpHeaders;
import mosaics.http.HttpPayload;
import mosaics.http.HttpRequest;
import mosaics.http.HttpResponseCode;
import mosaics.http.HttpResponse;
import mosaics.http.HttpClient;
import mosaics.io.resources.MimeType;
import mosaics.strings.StringUtils;

import java.io.IOException;
import java.net.URI;
import java.time.Duration;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class JdkHttpClient extends HttpClient {
    private       ExecutorService    executor;
    private java.net.http.HttpClient httpClient;
    private final JdkResponseHandler jdkResponseHandler = new JdkResponseHandler();

    public JdkHttpClient( String serviceName ) {
        super( serviceName );
    }

    @Override
    public HttpResponse invoke( Request req, HttpRequest requestDetails ) throws IOException {
        String callShortDescription = requestDetails.getShortDescription();

        return req.invokeChild( callShortDescription, c -> makeHttpCall(c,requestDetails) );
    }


    protected void doStart() {
        this.executor = Executors.newSingleThreadExecutor();

        this.httpClient = java.net.http.HttpClient.newBuilder()
            .connectTimeout( Duration.ofSeconds(30) )
            .followRedirects( java.net.http.HttpClient.Redirect.ALWAYS )
            .version( java.net.http.HttpClient.Version.HTTP_2 )
            .executor( executor )
            .build();
    }

    protected void doStop() {
        executor.shutdownNow();

        executor   = null;
        httpClient = null;
    }


    @SneakyThrows
    private HttpResponse makeHttpCall( Request req, HttpRequest requestDetails ) throws IOException {
        // todo req.inc
        // todo increase byte counters sent
        // todo increase byte counters received
        long                       nowMillis   = req.currentTimeMillis();
        java.net.http.HttpRequest  jdkRequest  = createJdkRequest( nowMillis, requestDetails );
        java.net.http.HttpResponse jdkResponse = httpClient.send( jdkRequest, java.net.http.HttpResponse.BodyHandlers.ofString() );

        return toHttpResponse( jdkResponse );
    }

    private java.net.http.HttpRequest createJdkRequest( long nowMillis, HttpRequest httpRequest ) {
        URI         uri         = httpRequest.toURI();
        String      method      = httpRequest.getRequestMethod().name();
        HttpPayload httpPayload = httpRequest.getBody();


        java.net.http.HttpRequest.Builder httpRequestBuilder = java.net.http.HttpRequest.newBuilder()
            .uri( uri )
            .timeout( Duration.ofSeconds( 30 ) );

        httpRequestBuilder = setupRequestHeadersIn( httpRequestBuilder, httpRequest );
        httpRequestBuilder = setupRequestCookiesIn( nowMillis, httpRequestBuilder, httpRequest );

        return setupRequestPayloadIn( httpRequestBuilder, method, httpPayload ).build();
    }

    private java.net.http.HttpRequest.Builder setupRequestHeadersIn( java.net.http.HttpRequest.Builder httpRequestBuilder, HttpRequest httpRequest ) {
        return httpRequest.getHeaders().iterator().fold(
            httpRequestBuilder,
            (agg,kv) -> agg.header( kv.getFirst(), kv.getSecond() )
        );
    }

    private java.net.http.HttpRequest.Builder setupRequestCookiesIn( long nowMillis, java.net.http.HttpRequest.Builder jdkHttpRequestBuilder, HttpRequest httpRequest ) {
        HttpCookies httpCookies = httpRequest.getCookies();

        String cookieHeader = httpCookies.iterator()
            .filter( c -> c.isAliveAt(nowMillis) )
            .filter( c -> c.accepts(httpRequest) )
            .map( c -> c.getName() + "=" + c.getValue() )
            .mkString( "; " );

        return StringUtils.isBlank(cookieHeader) ? jdkHttpRequestBuilder : jdkHttpRequestBuilder.header( "Cookie", cookieHeader );
    }

    private java.net.http.HttpRequest.Builder setupRequestPayloadIn( java.net.http.HttpRequest.Builder httpRequestBuilder, String method, HttpPayload httpPayload ) {
        if ( httpPayload.isEmpty() ) {
            java.net.http.HttpRequest.BodyPublisher bodyPublisher = java.net.http.HttpRequest.BodyPublishers.noBody();

            httpRequestBuilder = httpRequestBuilder.method( method, bodyPublisher );
        } else {
            java.net.http.HttpRequest.BodyPublisher bodyPublisher = java.net.http.HttpRequest.BodyPublishers.ofString( httpPayload.asString() );

            httpRequestBuilder = httpRequestBuilder.method( method, bodyPublisher );
            httpRequestBuilder = httpRequestBuilder.header( "content-type", httpPayload.getMimeType().orElse(MimeType.TEXT).getMimeType() );
        }

        return httpRequestBuilder;
    }

    private HttpResponse toHttpResponse( java.net.http.HttpResponse jdkResponse ) {
        HttpResponseCode status  = HttpResponseCode.of( jdkResponse.statusCode() );
        HttpHeaders      headers = toHeaders( jdkResponse.headers() );
        FPOption<String> body    = FPOption.ofBlankableString( Objects.toString(jdkResponse.body()) );
        HttpPayload      payload = jdkResponseHandler.toPayload( headers, body );

        return HttpResponse.builder()
            .responseCode( status )
            .headers( headers )
            .body( payload )
            .build();
    }

    private HttpHeaders toHeaders( java.net.http.HttpHeaders jdkHeaders ) {
        return HttpHeaders.wrapJdkMapOfLists( jdkHeaders.map() );
    }
}
