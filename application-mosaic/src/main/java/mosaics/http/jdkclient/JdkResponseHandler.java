package mosaics.http.jdkclient;

import mosaics.fp.collections.FPOption;
import mosaics.http.HttpHeaders;
import mosaics.http.HttpPayload;
import mosaics.io.resources.MimeType;

import java.net.http.HttpResponse;


public class JdkResponseHandler implements HttpResponse.BodyHandler<Object> {
    public HttpResponse.BodySubscriber<Object> apply( HttpResponse.ResponseInfo responseInfo ) {
        return null;
    }

    public HttpPayload toPayload( HttpHeaders headers, FPOption<String> body ) {
        if (body.isEmpty()) {
            return HttpPayload.empty();
        }

        MimeType mimeType = headers.getSingleHeader( "content-type" )
            .flatMap( MimeType::getMimeTypeFrom )
            .orElse( () -> MimeType.TEXT );

        return HttpPayload.of(mimeType, body.get());
    }
}
