package mosaics.http;

import lombok.Value;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


@Value
public class HttpProtocol {
    private static Map<String,HttpProtocol> CACHE = new ConcurrentHashMap<>();

    public static final HttpProtocol HTTP  = of("http");
    public static final HttpProtocol HTTPS = of("https", true);

    public static HttpProtocol of(String protocol) {
        return of(protocol, false);
    }

    public static HttpProtocol of(String protocol, boolean isSecure) {
        return CACHE.computeIfAbsent(protocol.toLowerCase(), key -> new HttpProtocol(key,isSecure));
    }

    private final String  name;
    private final boolean isSecure;
}
