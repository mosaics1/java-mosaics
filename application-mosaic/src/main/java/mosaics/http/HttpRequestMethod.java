package mosaics.http;

public enum HttpRequestMethod {
    GET, POST, PUT, DELETE, HEAD, PATCH;

    public boolean isGET() {
        return this == GET;
    }

    public boolean isPOST() {
        return this == POST;
    }

    public boolean isPUT() {
        return this == PUT;
    }

    public boolean isDELETE() {
        return this == DELETE;
    }

    public boolean isHEAD() {
        return this == HEAD;
    }

    public boolean isPATCH() {
        return this == PATCH;
    }

    public void toString( StringBuilder buf ) {
        buf.append(this.toString());
    }
}
