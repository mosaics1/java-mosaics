package mosaics.http;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;
import lombok.With;
import mosaics.cli.requests.Request;
import mosaics.fp.FP;
import mosaics.fp.collections.FPIterable;

import java.io.IOException;


/**
 * HttpSessions offer the following services:
 *
 * 1. apply a set of headers to every request that is sent out by the session
 * 2. accept the Set-Cookie header from responses and store them
 * 3. select matching cookies from the session store and add them to out going requests
 */
@Value
@Builder
@With
@AllArgsConstructor
public class HttpSession {
                     private HttpClient httpClient;
    @Builder.Default private HttpHeaders sessionHeaders = HttpHeaders.empty();
    @Builder.Default private HttpCookies sessionCookies = HttpCookies.empty();

    public HttpSession( HttpClient httpClient ) {
        this( httpClient, HttpHeaders.empty(), HttpCookies.empty() );
    }

    public HttpSession appendHeader( String header, String value ) {
        return withSessionHeaders( this.sessionHeaders.appendHeader(header,value) );
    }

    public HttpSession appendCookie( String name, String value ) {
        return appendCookie(HttpCookie.of(name,value));
    }

    public HttpSession appendCookies( HttpCookie...newCookies ) {
        return appendCookies( FP.wrapArray(newCookies) );
    }

    public HttpSession appendCookies( FPIterable<HttpCookie> newCookies ) {
        return newCookies.fold(this, HttpSession::appendCookie );
    }

    private HttpSession appendCookie( HttpCookie cookie ) {
        return withSessionCookies( sessionCookies.appendCookie(cookie) );
    }

    public HttpSessionResponse invoke( Request ctx, HttpRequest httpRequest ) throws IOException {
        HttpRequest updatedHttpRequest = httpRequest
            .appendHeaders( sessionHeaders )
            .appendCookies( sessionCookies );

        HttpResponse httpResponse   = httpClient.invoke( ctx, updatedHttpRequest );
        HttpSession  updatedSession = this.appendCookies( httpResponse.getSetCookies() );

        return new HttpSessionResponse( updatedSession, httpResponse );
    }
}

