package mosaics.http;

import lombok.Builder;
import lombok.Value;
import lombok.With;
import mosaics.fp.FP;
import mosaics.fp.collections.FPOption;

import java.util.Objects;


@With
@Value
@Builder
public class HttpCookie {
    /**
     * Format:
     *
     * <code>
     *     Set-Cookie: &lt;cookie-name>=&lt;cookie-value>; Expires=&lt;date>; Secure; HttpOnly;
     *                    ; Max-Age=&lt;non-zero-digit>; Domain=&lt;domain-value>
     *                    ; Path=&lt;path-value>; SameSite=Strict|Lax|none
     * </code>
     *
     * Date Format: &lt;day-name>, &lt;day> &lt;month> &lt;year> &lt;hour>:&lt;minute>:&lt;second> GMT<p/>
     * Example: Wed, 21 Oct 2015 07:28:00 GMT
     *
     * @see <a href="https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Set-Cookie">http Set-Cookie spec</a>
     */
    public static HttpCookie parseSetCookieHeader(String str) {
        return HttpParserUtils.parseEncodedSetCookieString( str );
    }



    public static HttpCookie of( String name, String value ) {
        return HttpCookie.builder()
            .name(name)
            .value(value)
            .build();
    }

    public static enum SameSiteEnum {STRICT,LAX,NONE}

                     private final String name;
                     private final String value;

    @Builder.Default private final long             expiresAtMillis = -1;
    @Builder.Default private final boolean          isSecure        = false;
    @Builder.Default private final boolean          isHttpOnly      = false;
    @Builder.Default private final int              maxAgeSeconds   = -1;
    @Builder.Default private final FPOption<String> domain          = FP.emptyOption();
    @Builder.Default private final HttpPath         path            = HttpPath.empty();
    @Builder.Default private final SameSiteEnum     sameSite        = SameSiteEnum.NONE;

    //
    // send Cookie: name=value; name=value; name=value

    public HttpCookie withPath( String newPath ) {
        return withPath( HttpPath.of(newPath) );
    }

    public HttpCookie withPath( HttpPath newPath ) {
        if ( this.path.equals(newPath) ) {
            return this;
        }

        return new HttpCookie( name, value, expiresAtMillis, isSecure, isHttpOnly, maxAgeSeconds, domain, newPath, sameSite );
    }

    /**
     * Given the current time, update the expireAt value for this cookie.  NB if the cookie
     * has already had its expireAt set, then the max age will be ignored as per the cookie
     * specification.
     *
     * @see <a href="https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Set-Cookie#Max-Age">Max-Age spec</a>
     */
    public HttpCookie updateExpiresBasedOnMaxAge( long nowMillis ) {
        if ( expiresAtMillis >= 0 || maxAgeSeconds < 0 ) {
            return this;
        }

        return this.withExpiresAtMillis( nowMillis + maxAgeSeconds*1000 )
                   .withMaxAgeSeconds( -1 );
    }

    public boolean isAliveAt( long nowMillis ) {
        if ( expiresAtMillis < 0 && maxAgeSeconds >= 0 ) {
            throw new IllegalStateException(
              "Cookie '"+name+"' cannot check whether it is alive or not because its Max-Age " +
                  "has not been converted to an Expires.  Invoke " +
                  "cookie.updateExpiresBasedOnMaxAge() first."
            );
        }

        return expiresAtMillis < 0 || expiresAtMillis > nowMillis;
    }

    public boolean hasExpiredAt( long nowMillis ) {
        return !isAliveAt( nowMillis );
    }

    public boolean accepts( HttpRequest httpRequest ) {
        if (isSecure && !httpRequest.isSecure()) {
            return false;
        }

        if ( this.getDomain().hasValue() && !Objects.equals(this.domain.get(), httpRequest.getHost())) {
            return false;
        }

        if ( !httpRequest.getPath().startsWith(this.path) ) {
            return false;
        }

        return true;
    }



    // this line is here to prevent a JavaDoc error in HttpParserUtils (which references this class)
    // the specifics of this builder are still built by Lombok.
    public static class HttpCookieBuilder {}
}
