package mosaics.http;


import lombok.EqualsAndHashCode;
import lombok.Value;
import mosaics.fp.FP;
import mosaics.fp.collections.FPOption;
import mosaics.io.resources.MimeType;
import mosaics.json.JsonCodec;
import mosaics.json.JsonCodecBuilder;
import mosaics.strings.StringUtils;

import static mosaics.lang.Backdoor.cast;


public abstract class HttpPayload {
    private static final JsonCodec DEFAULT_JSON_CODEC = new JsonCodecBuilder().withPrettyPrinting().create();
    private static final HttpPayload EMPTY = new EmptyPayload();

    public static HttpPayload empty() {
        return EMPTY;
    }

    public static HttpPayload of( MimeType mimeType, String text ) {
        if ( StringUtils.isEmpty(text) ) {
            return empty();
        } else if ( mimeType.equals(MimeType.JSON) ) {
            return new JsonStringPayload( text );
        } else {
            return new StringPayload( FP.option(mimeType), text );
        }
    }

    public static HttpPayload plainText( String txt ) {
        return of(MimeType.TEXT,txt);
    }

    public static HttpPayload json( String txt ) {
        return new JsonStringPayload( txt );
    }

    public static <T> HttpPayload json( T pojo ) {
        return new JsonDTOPayload<>(pojo);
    }

    public abstract boolean isEmpty();
    public abstract FPOption<MimeType> getMimeType();
    public abstract String asString();

    public abstract <T> boolean supports(Class<T> targetType);
    public abstract <T> FPOption<T> asDTO(Class<T> targetType);

    @Value
    @EqualsAndHashCode(callSuper=false)
    private static class EmptyPayload extends HttpPayload {
        public boolean isEmpty() {
            return true;
        }

        public FPOption<MimeType> getMimeType() {
            return FP.emptyOption();
        }

        public String asString() {
            return "";
        }

        public <T> boolean supports( Class<T> targetType ) {
            return false;
        }

        public <T> FPOption<T> asDTO(Class<T> targetType) {
            throw new UnsupportedOperationException();
        }
    }

    @Value
    @EqualsAndHashCode(callSuper=false)
    private static class StringPayload extends HttpPayload {
        private final FPOption<MimeType> mimeType;
        private final String             payload;

        public boolean isEmpty() {
            return payload == null || payload.equals("");
        }

        public String asString() {
            return payload;
        }

        public <T> boolean supports( Class<T> targetType ) {
            return true;
        }

        public <T> FPOption<T> asDTO(Class<T> targetType) {
            return FP.option( DEFAULT_JSON_CODEC.fromJson(payload,targetType) );
        }
    }

    @Value
    @EqualsAndHashCode(callSuper=false)
    private static class JsonStringPayload extends HttpPayload {
        private final String payload;

        public FPOption<MimeType> getMimeType() {
            return FP.option(MimeType.JSON);
        }

        public boolean isEmpty() {
            return payload == null || payload.equals("");
        }

        public String asString() {
            return payload;
        }

        @Override
        public <T> boolean supports( Class<T> targetType ) {
            return true;
        }

        public <T> FPOption<T> asDTO(Class<T> targetType) {
            return FP.option( DEFAULT_JSON_CODEC.fromJson(payload,targetType) );
        }
    }

    @Value
    @EqualsAndHashCode(callSuper=false)
    private static class JsonDTOPayload<T> extends HttpPayload {
        private final T payload;

        public FPOption<MimeType> getMimeType() {
            return FP.option(MimeType.JSON);
        }

        public boolean isEmpty() {
            return payload == null;
        }

        public String asString() {
            return DEFAULT_JSON_CODEC.toJson( payload );
        }

        @Override
        public <T> boolean supports( Class<T> targetType ) {
            return payload == null ? true : payload.getClass() == targetType;
        }

        public <T> FPOption<T> asDTO(Class<T> targetType) {
            return FP.option(cast(payload));
        }
    }
}
