package mosaics.http;

import mosaics.cli.requests.Request;
import mosaics.lang.lifecycle.StartStoppable;

import java.io.IOException;


public abstract class HttpClient extends StartStoppable<HttpClient> {
    public HttpClient( String serviceName ) {
        super( serviceName );
    }

    public abstract HttpResponse invoke( Request ctx, HttpRequest requestDetails ) throws IOException;

    public HttpSession createSession() {
        return new HttpSession(this);
    }
}
