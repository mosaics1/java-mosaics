package mosaics.http.exceptions;

import mosaics.http.HttpResponse;


public class HttpBadRequestException extends HttpClientException {
    public HttpBadRequestException( HttpResponse resp ) {
        super( resp );
    }
}
