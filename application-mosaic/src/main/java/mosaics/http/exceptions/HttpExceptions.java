package mosaics.http.exceptions;

import mosaics.http.HttpResponseCode;
import mosaics.http.HttpResponse;
import mosaics.lang.functions.Function1;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static mosaics.http.HttpResponseCode.*;


public class HttpExceptions {
    private static final Map<Integer, Function1<HttpResponse,HttpException>> mappers = new ConcurrentHashMap<>();

    {
        registerMapper( BAD_REQUEST, HttpBadRequestException::new);
//        public static final HttpResponseCode BAD_REQUEST = of(400, "BAD_REQUEST");
//   public static final HttpResponseCode UNAUTHORIZED = of(401, "UNAUTHORIZED");
//        public static final HttpResponseCode PAYMENT_REQUIRED = of(402, "PAYMENT_REQUIRED");
//        public static final HttpResponseCode FORBIDDEN = of(403, "FORBIDDEN");
//        public static final HttpResponseCode NOT_FOUND = of(404, "NOT_FOUND");
//        public static final HttpResponseCode METHOD_NOT_ALLOWED = of(405, "METHOD_NOT_ALLOWED");
//        public static final HttpResponseCode NOT_ACCEPTABLE = of(406, "NOT_ACCEPTABLE");
//        public static final HttpResponseCode REQUEST_TIMEOUT = of(408, "REQUEST_TIMEOUT");
//        public static final HttpResponseCode PRECONDITION_FAILED = of(412, "PRECONDITION_FAILED");
//        public static final HttpResponseCode PAYLOAD_TOO_LARGE = of(413, "PAYLOAD_TOO_LARGE");
//        public static final HttpResponseCode UNSUPPORTED_MEDIA_TYPE = of(415, "UNSUPPORTED_MEDIA_TYPE");
//        public static final HttpResponseCode RANGE_NOT_SATISFIABLE = of(416, "RANGE_NOT_SATISFIABLE");
//        public static final HttpResponseCode EXPECTATION_FAILED = of(417, "EXPECTATION_FAILED");
//        public static final HttpResponseCode LOCKED = of(423, "LOCKED");
//        public static final HttpResponseCode FAILED_DEPENDENCY = of(424, "FAILED_DEPENDENCY");
//        public static final HttpResponseCode TOO_MANY_REQUESTS = of(429, "TOO_MANY_REQUESTS");
//        public static final HttpResponseCode INTERNAL_SERVER_ERROR = of(500, "INTERNAL_SERVER_ERROR");
//        public static final HttpResponseCode NOT_IMPLEMENTED = of(501, "NOT_IMPLEMENTED");
//        public static final HttpResponseCode BAD_GATEWAY = of(502, "BAD_GATEWAY");
//        public static final HttpResponseCode SERVICE_UNAVAILABLE = of(503, "SERVICE_UNAVAILABLE");
//        public static final HttpResponseCode GATEWAY_TIMEOUT = of(504, "GATEWAY_TIMEOUT");
//        public static final HttpResponseCode INSUFFICIENT_STORAGE = of(507, "INSUFFICIENT_STORAGE");

    }


    public static void registerMapper( HttpResponseCode code, Function1<HttpResponse,HttpException> mapper ) {
        registerMapper( code.toInt(), mapper );
    }

    public static void registerMapper( int code, Function1<HttpResponse,HttpException> mapper ) {
        mappers.put( code, mapper );
    }

    public static HttpException toException( HttpResponse response ) {
        Function1<HttpResponse,HttpException> mapper = mappers.get(response.getResponseCode().toInt());

        if ( mapper != null ) {
            return mapper.invoke( response );
        } else if ( response.isClientError() ) {
            return new HttpClientException( response );
        } else if ( response.isServerError() ) {
            return new HttpServerException( response );
        } else {
            return new HttpException( response );
        }
    }
}
