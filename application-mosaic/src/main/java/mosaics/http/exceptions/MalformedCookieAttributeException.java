package mosaics.http.exceptions;

public class MalformedCookieAttributeException extends RuntimeException {
    public MalformedCookieAttributeException(String key, String value) {
        super("Malformed cookie attribute "+key+"="+value);
    }

    public MalformedCookieAttributeException(String key, String value, Throwable ex) {
        super("Malformed cookie attribute "+key+"="+value, ex);
    }
}
