package mosaics.http;

import lombok.Value;
import lombok.val;
import mosaics.collections.immutable.trees.FPTreeMap;
import mosaics.fp.FP;
import mosaics.fp.collections.FPIterable;
import mosaics.fp.collections.FPIterator;


@Value
public class HttpCookies implements FPIterable<HttpCookie> {
    private static final HttpCookies EMPTY = new HttpCookies( FP.emptyTree() );

    public static HttpCookies empty() {
        return EMPTY;
    }

    public static HttpCookies of( HttpCookie...cookies ) {
        return empty().appendCookies( FP.wrapArray(cookies) );
    }


    private final FPTreeMap<String,HttpCookie> cookiesByName;

    public HttpCookies appendCookie( HttpCookie newCookie ) {
//        val updatedCookie  = newCookie.updateExpiresBasedOnMaxAge( nowMillis );
        val updatedCookies = cookiesByName.put( newCookie.getName(), newCookie );

        return wrap(updatedCookies);
    }

    public HttpCookies appendCookies( FPIterable<HttpCookie> newCookies ) {
        return newCookies.fold( this, HttpCookies::appendCookie );
    }

//    public HttpCookies expireOldCookies( long nowMillis ) {
//        var updatedCookies = cookiesByName.filterValues( cookie -> cookie.isAliveAt(nowMillis) );
//
//        return wrap(updatedCookies);
//    }

//    public HttpRequest applyCookiesTo( HttpRequest initialRequest ) {
//        cookiesByName.values().filter( c -> c.accepts(initialRequest) );
////            .map( c -> c.getName()+"="+c.getValue() )
////            .mkString("; ");
//
//        return initialRequest;
//    }
    public FPIterator<HttpCookie> iterator() {
        return cookiesByName.values();
    }

    private HttpCookies wrap( FPTreeMap<String,HttpCookie> updatedCookies ) {
        return updatedCookies == this.cookiesByName ? this : new HttpCookies(updatedCookies);
    }
}
