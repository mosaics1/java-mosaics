package mosaics.http;

import lombok.SneakyThrows;
import mosaics.fp.FP;
import mosaics.fp.collections.FPOption;
import mosaics.fp.collections.Tuple2;
import mosaics.fp.collections.maps.FPMap;
import mosaics.http.exceptions.MalformedCookieAttributeException;
import mosaics.lang.functions.Function3;
import mosaics.strings.StringUtils;

import java.net.URLDecoder;
import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Set;
import java.util.regex.Pattern;


class HttpParserUtils {
    private static final Set<String> VALID_BOOLEANS                     = Set.of("t", "true");
    private static final Pattern     COOKIE_PARAMETER_SEPARATER_PATTERN = Pattern.compile( ";" );
    private static final Pattern     QUERY_PARAMETER_SEPARATER_PATTERN  = Pattern.compile( "&" );

    public static HttpCookie parseEncodedSetCookieString(String str) {
        HttpCookie.HttpCookieBuilder cookieBuilder = parseDelimitedString(
            COOKIE_PARAMETER_SEPARATER_PATTERN,
            HttpCookie.builder(),
            HttpParserUtils::configureCookie,
            str
        );

        return cookieBuilder.build();
    }

    public static HttpQueryParameters parseQueryParametersString(String str) {
        FPMap<String, String> decodedParams = parseDelimitedString(
            QUERY_PARAMETER_SEPARATER_PATTERN,
            FP.emptyMap(),
            FPMap::put,
            str
        );

        return new HttpQueryParameters( decodedParams );
    }


    private static <T> T parseDelimitedString( Pattern p, T initial, Function3<T,String,String,T> aggregator, String txt ) {
        return FP.wrapArray( p.split( txt ) )
            .map( HttpParserUtils::splitKVPair )
            .fold( initial, (soFar,kv) -> aggregator.invoke(soFar,decode(kv.getFirst()),decode(kv.getSecond())) );
    }


    /**
     * Splits 'a=b'.  Supports 'a', as it maps it to 'a=true'.
     */
    private static Tuple2<String,String> splitKVPair(String kvStr ) {
        int i = kvStr.indexOf( '=' );

        if ( i < 0 ) {
            return new Tuple2<>( kvStr.trim(), "true" );
        } else {
            return new Tuple2<>( kvStr.substring(0,i).trim(), kvStr.substring(i+1) );
        }
    }

    @SneakyThrows
    static String decode( String s1 ) {
        return URLDecoder.decode( s1,"UTF-8");
    }

    private static HttpCookie.HttpCookieBuilder configureCookie( HttpCookie.HttpCookieBuilder cookieBuilder, String key, String value ) {
        cookieBuilder = switch (key.toLowerCase()) {
            case "expires"  -> cookieBuilder.expiresAtMillis( parseExpiresToEpoch(key,value) );
            case "secure"   -> cookieBuilder.isSecure( parseBoolean(key,value) );
            case "httponly" -> cookieBuilder.isHttpOnly( parseBoolean(key,value) );
            case "max-age"  -> cookieBuilder.maxAgeSeconds( parseInteger(key,value) );
            case "domain"   -> cookieBuilder.domain( parseDomainString(key,value) );
            case "path"     -> cookieBuilder.path( HttpPath.of(value) );
            case "samesite" -> cookieBuilder.sameSite( parseEnum(HttpCookie.SameSiteEnum.class,key,value, HttpCookie.SameSiteEnum.NONE) );
            default         -> cookieBuilder.name( key ).value( parseValue(value) );
        };


        return cookieBuilder;
    }

    private static String parseValue( String value ) {
        if (value.startsWith("\"") && value.endsWith("\"")) {
            return value.substring(1,value.length()-1);
        } else {
            return value;
        }
    }

    private static FPOption<String> parseDomainString( String key, String value ) {
        if ( StringUtils.isBlank(value) ) {
            return FP.emptyOption();
        }

        if ( value.startsWith(".") ) {
            return FP.option( value.substring(1) );
        } else {
            return FP.option( value );
        }
    }

    private static <T extends Enum<T>> T parseEnum( Class<T> enumClass, String key, String value, T defaultValue ) {
        if ( StringUtils.isBlank(value) ) {
            return defaultValue;
        }

        for ( T candidate: enumClass.getEnumConstants() ) {
            if ( candidate.name().equalsIgnoreCase(value) ) {
                return candidate;
            }
        }

        throw new MalformedCookieAttributeException( key, value );
    }

    private static long parseExpiresToEpoch( String key, String value ) {
        try {
            return Instant.from( DateTimeFormatter.RFC_1123_DATE_TIME.parse( value ) ).toEpochMilli();
        } catch ( DateTimeParseException ex ) {
            throw new MalformedCookieAttributeException(key, value, ex);
        }
    }

    private static boolean parseBoolean( String key, String value ) {
        return VALID_BOOLEANS.contains(value.toLowerCase());
    }

    private static int parseInteger( String key, String value ) {
        try {
            return Integer.parseInt( value );
        } catch ( NumberFormatException ex ) {
            throw new MalformedCookieAttributeException( key, value, ex );
        }
    }
}
