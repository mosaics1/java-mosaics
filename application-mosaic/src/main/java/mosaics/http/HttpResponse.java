package mosaics.http;

import lombok.Builder;
import lombok.SneakyThrows;
import lombok.Value;
import lombok.With;
import mosaics.fp.collections.FPIterable;
import mosaics.fp.collections.FPOption;
import mosaics.io.writers.AppendableWriterAdapter;
import mosaics.io.writers.Writer;
import mosaics.lang.ToStringMixin;
import mosaics.strings.PrettyPrinter;

import java.io.IOException;


@With
@Value
@Builder
public class HttpResponse implements ToStringMixin {
    public static HttpResponse of( int statusCode ) {
        return new HttpResponse( HttpResponseCode.of(statusCode), HttpHeaders.empty(), HttpPayload.empty() );
    }

    public static final PrettyPrinter<HttpResponse> HTTP11_PRINTER = new Http11Printer();


                     private final HttpResponseCode   responseCode;
    @Builder.Default private final HttpHeaders        headers = HttpHeaders.empty();
    @Builder.Default private final HttpPayload        body    = HttpPayload.empty();

    public boolean isClientError() {
        return isResponseCodeBetween( 400, 500 );
    }

    public boolean isServerError() {
        return isResponseCodeBetween( 500, 600 );
    }

    public boolean isOK() {
        return isResponseCodeBetween( 200, 300 );
    }

    public boolean is404() {
        return responseCode.is404();
    }


    public HttpResponse appendHeader( String name, String value ) {
        return this.withHeaders( this.headers.appendHeader(name,value) );
    }

    public HttpResponse appendHeaders( String...kvPairs ) {
        return this.withHeaders(
            this.headers.appendHeaders(kvPairs)
        );
    }

    public String getHeaderMandatory( String name ) {
        return headers.getHeaderMandatory(name);
    }

    public <T> T getMandatoryPayloadAs( Class<T> targetType ) {
        return getPayloadAs( targetType ).get();
    }

    public <T> FPOption<T> getPayloadAs( Class<T> targetType ) {
        return this.body.asDTO( targetType );
    }

    public FPIterable<HttpCookie> getSetCookies() {
        return headers.getHeader( "set-cookie" ).map(HttpCookie::parseSetCookieHeader);
    }

    public void toString( Appendable out ) throws IOException {
        out.append( "HttpResponseDetails(" );
        out.append( Integer.toString(responseCode.getCode()) );
        out.append( ", " );
        out.append( headers.toString() );
        out.append( ", " );
        out.append( body.toString() );
        out.append( ")" );
    }

    public String toString() {
        return toStringWithStringBuilder();
    }

    private boolean isResponseCodeBetween(int lowerInc, int upperExc) {
        int code = responseCode.getCode();

        return code >= lowerInc && code < upperExc;
    }


    private static class Http11Printer implements PrettyPrinter<HttpResponse> {
        public void writeTo( HttpResponse r, Appendable out ) throws IOException {
            Writer o = new AppendableWriterAdapter(out);

            printHttpLine( o, r.getResponseCode() );
            printHeaders( o, r.getHeaders() );
            printPayload( o, r.getBody() );
            printEOM( o );
        }

        private void printHttpLine( Writer out, HttpResponseCode code ) throws IOException {
            out.append( "HTTP/1.1 " );
            out.append( Integer.toString(code.toInt()) );
            out.newLine();
        }

        private void printHeaders( Writer out, HttpHeaders headers ) {
            headers.forEach( (k,v) -> {
                out.append(k);
                out.append( ": " );
                out.append( v );
                out.newLine();
            } );

            out.newLine();
        }

        @SneakyThrows
        private void printPayload( Writer o, HttpPayload payload ) {
            o.append( payload.asString() );
            o.newLine();
        }

        private void printEOM( Writer out ) {
            out.newLine();
        }
    }
}
