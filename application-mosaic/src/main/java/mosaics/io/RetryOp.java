package mosaics.io;


import lombok.AllArgsConstructor;
import lombok.Value;
import lombok.With;
import mosaics.cli.Env;
import mosaics.cli.requests.Request;
import mosaics.concurrency.Scheduler;
import mosaics.concurrency.SystemScheduler;
import mosaics.fp.ExceptionFailure;
import mosaics.lang.Backdoor;
import mosaics.lang.functions.BooleanFunction1;
import mosaics.lang.functions.Function0WithThrows;
import mosaics.lang.functions.VoidFunction0WithThrows;
import mosaics.lang.threads.RetryDelay;

import java.io.IOException;
import java.sql.SQLException;
import java.time.Duration;
import java.util.function.IntToLongFunction;


/**
 * Decorate a function with retrying after an exception.  Supports a max number of retry, pluggable
 * delays between retries (defaults to an exponential backoff) and logs what it is doing and why.
 */
@Value
@AllArgsConstructor
public class RetryOp {
    private Scheduler scheduler;

    @With private int                         maxAttemptCount;
    @With private Duration                    maxDelay;
    @With private IntToLongFunction           retryDelayGenerator;
    @With private BooleanFunction1<Throwable> isRetryableClassifier;


    public RetryOp( Env env) {
        this( new SystemScheduler(env,"RetryOpScheduler") );
    }

    public RetryOp( Scheduler scheduler ) {
        this( scheduler, 3, Duration.ofSeconds(3), RetryOp::defaultRetryGenerator, RetryOp::defaultIsRetryable );
    }


    public void invoke( Request req, VoidFunction0WithThrows task ) {
        invoke( req, () -> {
            task.invoke();

            return null;
        });
    }

    public <T> T invoke( Request req, Function0WithThrows<T> task ) {
        int       attemptNumber = 0;
        Throwable lastException = null;

        while ( attemptNumber < maxAttemptCount ) {
            try {
                return task.invoke();
            } catch ( Throwable ex ) {
                if ( !isRetryableClassifier.invoke(ex) ) {
                    req.log(RetryOpLogMessageFactory.INSTANCE.opFailedWithNonRetryableException(task.toString(),new ExceptionFailure(ex)) );

                    throw Backdoor.throwException( ex );
                }

                attemptNumber += 1;

                lastException = ex;

                if ( attemptNumber < maxAttemptCount ) {
                    long sleepMillis = retryDelayGenerator.applyAsLong( attemptNumber );

                    sleepMillis = Math.max( 0, Math.min(maxDelay.toMillis(),sleepMillis) );

                    req.log(
                        RetryOpLogMessageFactory.INSTANCE.retryFailedWillRetryAgain(
                            task.toString(),
                            attemptNumber,
                            maxAttemptCount,
                            Duration.ofMillis(sleepMillis),
                            new ExceptionFailure(ex)
                        )
                    );

                    scheduler.sleep( req, sleepMillis );
                }
            }
        }

        req.log(RetryOpLogMessageFactory.INSTANCE.retryFailedForTheLastTimeActionAborted(task.toString(), maxAttemptCount, lastException));

        throw Backdoor.throwException( lastException );
    }

    static long defaultRetryGenerator( int attemptNumber ) {
        return RetryDelay.exponentialBackoffSeries(attemptNumber);
    }

    static boolean defaultIsRetryable( Throwable ex ) {
        return ex instanceof IOException || ex instanceof SQLException;
    }
}
