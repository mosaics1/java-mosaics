package mosaics.io.codecs;

import mosaics.fp.collections.FPOption;
import mosaics.fp.collections.RRBVector;
import mosaics.fp.collections.maps.FPMap;
import mosaics.io.resources.MimeType;
import mosaics.io.resources.UnknownMimeTypeException;

import java.util.Collection;


/**
 *
 */
public class ObjectCodecs {

    private final ObjectCodec                 defaultCodec;
    private final FPMap<MimeType,ObjectCodec> codecs;
    private final RRBVector<MimeType>         mimeTypesInPreferenceOrder;


    ObjectCodecs( FPMap<MimeType, ObjectCodec> codecs, ObjectCodec defaultCodec, RRBVector<MimeType> mimeTypesInPreferenceOrder ) {
        this.codecs                     = codecs;
        this.defaultCodec               = defaultCodec;
        this.mimeTypesInPreferenceOrder = mimeTypesInPreferenceOrder;
    }


    public Collection<ObjectCodec> getCodecs() {
        return codecs.getValues().toList();
    }

    /**
     * Performs content negotiation by an Http Request's Accept header.  An Accept header lists
     * n weighted mime types that the client supports.  The mime types may include wild cards.  This
     * method will sort the mime types by weight and then checks each in turn to find an ObjectCodec.
     * The first one that is matched will be returned.  If none match, then no codec will be returned
     * and content negotiation will have failed.
     */
    public FPOption<ObjectCodec> getCodecForHttpAcceptHeader( String acceptHeader ) {
        return MimeType.getMimeTypesFrom(acceptHeader).firstOpt( this::getCodecFor );
    }

    public FPOption<ObjectCodec> getCodecFor( MimeType mimeType ) {
        if ( mimeType.hasWildCard() ) {
            return mimeTypesInPreferenceOrder
                         .first( mimeType::matches )
                         .flatMap( codecs::get );
        } else {
            return codecs.get(mimeType);
        }
    }

    public ObjectCodec getDefaultCodec() {
        return defaultCodec;
    }

    public ObjectCodec getCodecOrThrowUnsupportedExceptionFor( MimeType mimeType ) throws UnknownMimeTypeException {
        return getCodecFor(mimeType).orElseThrow( () ->  new UnknownMimeTypeException(mimeType.getMimeType()) );
    }

}
