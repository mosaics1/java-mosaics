package mosaics.io.codecs.html;


import mosaics.io.xml.TagWriter;
import mosaics.lang.functions.VoidFunction2;


/**
 * Knows how to serialize a specific class of object into Html.
 */
public interface HtmlSerializer {
    public void serialize( Object obj, VoidFunction2<Object, TagWriter> nestedSerializer, TagWriter out );
}
