package mosaics.io.codecs.html;

import mosaics.fp.FP;
import mosaics.fp.Tryable;
import mosaics.fp.collections.FPList;
import mosaics.fp.collections.FPOption;
import mosaics.fp.collections.maps.FPMap;
import mosaics.io.codecs.ObjectCodec;
import mosaics.io.resources.MimeType;
import mosaics.io.xml.TagWriter;
import mosaics.lang.Backdoor;
import mosaics.lang.Secret;
import mosaics.lang.reflection.JavaClass;
import mosaics.lang.reflection.JavaProperty;
import mosaics.lang.reflection.ReflectionUtils;
import mosaics.strings.StringUtils;
import mosaics.xml.XmlTemplate;
import mosaics.xml.XmlTemplateFactory;

import java.io.Reader;
import java.io.Writer;
import java.lang.reflect.Array;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;


/**
 * Serialises objects to html.  Not yet capable of reading them back in yet.<p/>
 *
 * Serialisation takes the following steps:
 *
 * 1) Look for a @Html annotation.  If found, it uses the list of properties on the annotation
 *    to generate the HTML.
 * 2) If no annotation is found, then reflection is used to identify the properties from method
 *    names and field names.
 *
 * The default HTML format is as follows:
 *
 * <pre>
 *     <div class="jcpojo jcname_{className}">
 *         #foreach p in classesProperties
 *         <div class="jcprop jcprop_{propertyName}">
 *           <div class="jcpropkey jckey_{propertyName}">
 *               {propertyName}
 *           </div>
 *           <div class="jcpropvalue jcvalue_{propertyValue}">
 *               {propertyValue}
 *           </div>
 *         </div>
 *         #endfor
 *     </div>
 * </pre>
 */
@SuppressWarnings("unchecked")
public class HtmlObjectCodec implements ObjectCodec {

    public MimeType getCodecMimeType() {
        return MimeType.HTML;
    }

    public <T> boolean supports( Class<T> objectType ) {
        return supports( JavaClass.of(objectType) );
    }

    public <T> boolean supports( JavaClass objectType ) {
        return objectType.isPojo();
    }

    public void writeTo( Object o, Writer writer, Class objectType ) {
        TagWriter out = TagWriter.htmlWriter( writer );

        out.tag( "html" );
        out.tag( "body" );

        doSerialisation( o, out );

        out.closeTag( "body" );
        out.closeTag( "html" );
    }

    public <T> Tryable<T> fromReader( Reader in, Class<T> objectType ) {
        throw new UnsupportedOperationException();
    }




    private static final HtmlSerializer TOSTRING_SERIALIZER = ( obj, dispatcher, out) -> out.bodyText(obj.toString());

    private static final HtmlSerializer JDK_OPTION_SERIALIZER = (value,dispatcher,out) -> {
        Optional option = Backdoor.cast( value );

        option.ifPresent( v -> dispatcher.invoke( v, out ));
    };

    private static final HtmlSerializer FP_OPTION_SERIALIZER = (value,dispatcher,out) -> {
        FPOption option = Backdoor.cast( value );

        option.ifPresent( v -> dispatcher.invoke( v, out ));
    };

    private static final HtmlSerializer ARRAY_SERIALIZER = (array,dispatcher,out) -> {
        int length = Array.getLength(array);
        if ( length == 0 ) {
            return;
        }

        out.tag( "ol" );
        for ( int i=0; i<length; i++ ) {
            Object element = Array.get( array, i );

            out.tag("li");
            dispatcher.invoke( element, out );
            out.closeTag("li");
        }

        out.closeTag( "ol" );
    };

    private static final HtmlSerializer ITERABLE_SERIALIZER = (obj,dispatcher,out) -> {
        Iterable iterable = Backdoor.cast(obj);
        Iterator it = iterable.iterator();
        if ( !it.hasNext() ) {
            return;
        }

        out.tag( "ol" );
        while ( it.hasNext() ) {
            Object element = it.next();

            out.tag("li");
            dispatcher.invoke( element, out );
            out.closeTag("li");
        }

        out.closeTag( "ol" );
    };

    private static final HtmlSerializer MAP_SERIALIZER =
        (obj, dispatcher, out) -> {
            Map<Object, Object> map = Backdoor.cast(obj);
            List<Map.Entry<Object, Object>> kvPairs = map.entrySet().stream()
                .filter( kv -> ReflectionUtils.hasValue( kv.getValue() ) )
                .collect( Collectors.toList() );

            if (kvPairs.size() == 0) {
                return;
            }

            out.tag("dl");
            for (Map.Entry e : kvPairs ) {
                if (e.getValue() == null) {
                    continue;
                }

                {
                    out.tag("dt");
                    dispatcher.invoke(e.getKey(), out);
                    out.closeTag("dt");

                    out.tag("dd");
                    dispatcher.invoke(e.getValue(), out);
                    out.closeTag("dd");
                }
            }

            out.closeTag("dl");
        };

    private static final HtmlSerializer FPMAP_SERIALIZER =
        (obj, dispatcher, out) -> {
            FPMap<Object, Object> map = Backdoor.cast(obj);
            List<Map.Entry<Object, Object>> kvPairs = map.getEntries()
                .filter( kv -> ReflectionUtils.hasValue(kv.getValue()) )
                .toList();

            if (kvPairs.size() == 0) {
                return;
            }

            out.tag("dl");
            for (Map.Entry e : kvPairs ) {
                if (e.getValue() == null) {
                    continue;
                }

                {
                    out.tag("dt");
                    dispatcher.invoke(e.getKey(), out);
                    out.closeTag("dt");

                    out.tag("dd");
                    dispatcher.invoke(e.getValue(), out);
                    out.closeTag("dd");
                }
            }

            out.closeTag("dl");
        };

    private ConcurrentHashMap<Class<?>, HtmlSerializer> serializers = new ConcurrentHashMap<>();



    public HtmlObjectCodec registerSerializer( Class type, HtmlSerializer serializer ) {
        serializers.put( type, serializer );

        return this;
    }

    private void doSerialisation( Object object, TagWriter out ) {
        if ( object == null ) {
            return;
        }

        HtmlSerializer serializer = serializers.computeIfAbsent( object.getClass(), this::createSerializerFor );

        serializer.serialize( object, this::doSerialisation, out );
    }

    @SuppressWarnings("unchecked")
    private HtmlSerializer createSerializerFor( Class type ) {
        JavaClass jc = JavaClass.of( type );

        if ( hasTemplate(jc) ) {
            return createTemplatedSerializerFor(jc);
        }


        if ( jc.isFPOption() ) {
            return FP_OPTION_SERIALIZER;
        } else if ( jc.isJdkOptional() ) {
            return JDK_OPTION_SERIALIZER;
        } else if ( jc.isArray() ) {
            return ARRAY_SERIALIZER;
        } else if ( Map.class.isAssignableFrom(jc.getJdkClass()) ) {
            return MAP_SERIALIZER;
        } else if ( FPMap.class.isAssignableFrom(jc.getJdkClass()) ) {
            return FPMAP_SERIALIZER;
        } else if ( Iterable.class.isAssignableFrom(jc.getJdkClass()) ) {
            return ITERABLE_SERIALIZER;
        }

        FPList<JavaProperty<Object,Object>> properties = selectPropertiesFor(jc);

        if ( properties.isEmpty() ) {
            return TOSTRING_SERIALIZER;
        } else if ( jc.getJdkClass().equals( Secret.class) ) {
            return TOSTRING_SERIALIZER;
        }

        String shortName = jc.getShortName();
        //        <div class="jcpojo jcname_{className}">
        //            #foreach p in classesPropertiesreturn ( obj, nestedSerializer, out ) -> {
        //            <div class="jcprop jcprop_{className}_{propertyName}">    out.tag( "div" );
        //              <div class="jcpropkey jckey_{className}_{propertyName}">    out.attr( "class",
        // shortName );
        //                  {propertyName}
        //              </div>    properties.forEach( property -> {
        //              <div class="jcpropvalue jcvalue_{className}_{propertyValue}">
        // FPOption<Object> v = property.getValueFrom( obj );
        //                  {propertyValue}
        //              </div>        if ( v.hasValue() ) {
        //            </div>            out.tag( "div" );
        //            #endfor            out.attr( "class", shortName + " " + property );
        //        </div>            nestedSerializer.invoke( v.get(), out );
        return (obj, nestedSerializer, out) -> {
            out.tag("div");
            out.attr("class", "jcpojo jcname_" + shortName);

            properties.forEach(
                property -> {
                    FPOption<Object> v = property.getValueFrom(obj);

                    if (v.flatten().hasContents()) {
                        out.tag("div");
                        out.attr("class", "jcprop jcprop_" + shortName + "_" + property.getName());

                        {
                            out.tag("div");
                            out.attr("class", "jcpropkey jckey_" + shortName + "_" + property.getName());
                            out.bodyText(property.getName());
                            out.closeTag("div");

                            out.tag("div");
                            out.attr("class", "jcpropvalue jcvalue_" + shortName + "_" + property.getName());
                            nestedSerializer.invoke(v.get(), out);
                            out.closeTag("div");
                        }

                        out.closeTag("div");
                    }
                });

            out.closeTag("div");
        };
    }

    private boolean hasTemplate( JavaClass jc ) {
        FPOption<Html> htmlAnnotationOption = jc.getAnnotation( Html.class );

        return htmlAnnotationOption.hasValue() && !StringUtils.isBlank(getAnnotationTemplate(htmlAnnotationOption));
    }

    private String getAnnotationTemplate( FPOption<Html> annotation ) {
        return annotation.map(a -> StringUtils.concat(a.template(),"\n")).orElse("");
    }

    private HtmlSerializer createTemplatedSerializerFor( JavaClass jc ) {
        String      templateStr = getAnnotationTemplate( jc.getAnnotation(Html.class) );
        XmlTemplate template    = new XmlTemplateFactory().parse( jc.getFullName(), templateStr, jc );

        return template::invoke;
    }

    private FPList<JavaProperty<Object, Object>> selectPropertiesFor( JavaClass jc ) {
        FPOption<Html> annotationOption = jc.getAnnotation( Html.class );

        if (annotationOption.isEmpty()) {
            return jc.getProperties().filter(JavaProperty::isPublic).toFPList();
        } else {
            return FP.wrapArray(annotationOption.get().properties())
                .flatMap(jc::getProperty)
                .toFPList();
        }
    }
}
