package mosaics.io.codecs.html;

import mosaics.io.codecs.ObjectCodecBuilder;
import mosaics.io.resources.MimeType;

import java.util.Map;


/**
 *
 */
public class HtmlObjectCodecBuilder implements ObjectCodecBuilder<HtmlObjectCodec> {

    public MimeType getCodecMimeType() {
        return MimeType.HTML;
    }

    public HtmlObjectCodec create() {
        return new HtmlObjectCodec();
    }


    public HtmlObjectCodecBuilder identifyInheritanceTreeByPreRegisteredIdentifiers( Class baseType, String propertyName, Map mappings ) {
        return this;
    }

    public HtmlObjectCodecBuilder identifyInheritanceTreeByPreRegisteredIdentifiers( Class baseType, Map mappings ) {
        return this;
    }

    public HtmlObjectCodecBuilder identifyInheritanceTreeByClassName( Class baseType, String propertyName ) {
        return this;
    }

    public HtmlObjectCodecBuilder identifyInheritanceTreeByClassName( Class baseType ) {
        return this;
    }

}
