package mosaics.io.codecs;


import mosaics.fp.Tryable;
import mosaics.io.resources.MimeType;
import mosaics.lang.reflection.JavaClass;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;


/**
 *
 */
public interface SpecialisedObjectCodec<T> {

    public MimeType getCodecMimeType();

    public boolean supports( JavaClass objectType );

    public String toString( T o );
    public void writeTo( T o, Writer out );
    public void writeTo( T o, OutputStream out );

    public Tryable<T> fromString( String in );
    public Tryable<T> fromReader( Reader in );
    public Tryable<T> fromInputStream( InputStream in );

}
