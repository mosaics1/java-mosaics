package mosaics.io.codecs.text;

import mosaics.fp.FP;
import mosaics.fp.Try;
import mosaics.fp.Tryable;
import mosaics.fp.collections.maps.FPMutableMap;
import mosaics.fp.collections.FPOption;
import mosaics.io.codecs.ObjectCodec;
import mosaics.io.readers.MultiLineReader;
import mosaics.io.resources.MimeType;
import mosaics.lang.Backdoor;
import mosaics.lang.TryUtils;
import mosaics.lang.functions.Function1;
import mosaics.lang.reflection.JavaClass;
import mosaics.lang.reflection.JavaConstructor;
import mosaics.lang.reflection.JavaMethod;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;

import static mosaics.lang.Backdoor.cast;


/**
 * Encodes objects using toString and decodes them using the objects constructor or static factory
 * method 'parseString'.
 */
@SuppressWarnings({"rawtypes","unchecked"})
public class StringObjectCodec implements ObjectCodec{

    public static final StringObjectCodec INSTANCE = new StringObjectCodec();

    public MimeType getCodecMimeType() {
        return MimeType.TEXT;
    }

    public boolean supports( Class objectType ) {
        return true;
    }

    public boolean supports( JavaClass objectType ) {
        return true;
    }


    public String toString( Object o, JavaClass objectType ) {
        return String.valueOf(o).replaceAll( Backdoor.NEWLINE, "/"+Backdoor.NEWLINE );
    }

    public void writeTo( Object o, Writer out, JavaClass objectType ) {
        if ( o != null ) {
            TryUtils.invokeAndRethrowException( () -> out.write(o.toString().replaceAll( Backdoor.NEWLINE, "/"+Backdoor.NEWLINE )) );
        }
    }

    public void writeTo( Object o, OutputStream out, JavaClass objectType ) {
        if ( o != null ) {
            TryUtils.invokeAndRethrowException( () -> out.write(o.toString().replaceAll( Backdoor.NEWLINE, "/"+Backdoor.NEWLINE ).getBytes()) );
        }
    }

    private FPMutableMap<JavaClass,Function1> constructors = FPMutableMap.newMap();

    @SuppressWarnings("unchecked")
    public <T> Tryable<T> fromString( String in, JavaClass objectType ) {
        return Try.invoke( () -> {
            Function1<String, T> parser = constructors.computeIfAbsent( objectType, type -> {
                FPOption<Function1> deserialiserOpt = cast(this.toDeserialiserFunction(type));

                if ( deserialiserOpt.isEmpty() ) {
                    throw new IllegalArgumentException( "no single String arg constructor or static factory method 'parseString' found for " + objectType );
                }

                return deserialiserOpt.get();
            } );

            return parser.invoke( in );
        });
    }

    public <T> Tryable<T> fromReader( Reader in, JavaClass objectType ) {
        FPOption<Function1<String,T>> deserialiserOpt = toDeserialiserFunction( objectType );

        if ( deserialiserOpt.isEmpty() ) {
            return Try.failed( "no single String arg constructor or static factory method 'parseString' found for " + objectType );
        }

        return Try.invoke( () -> {
            String line = new MultiLineReader( in ).readLine();

            return deserialiserOpt.get().invoke( line );
        } );
    }

    public <T> Tryable<T> fromInputStream( InputStream in, JavaClass objectType ) {
        return fromReader( new InputStreamReader(in, Backdoor.UTF8), objectType );
    }

    public String toString( Object o, Class objectType ) {
        return String.valueOf( o ).replaceAll( Backdoor.NEWLINE, "/"+Backdoor.NEWLINE );
    }

    @SuppressWarnings("unchecked")
    public void writeTo( Object o, Writer out, Class objectType ) {
        writeTo( o, out, JavaClass.of(objectType) );
    }

    public void writeTo( Object o, OutputStream out, Class objectType ) {
        writeTo( o, out, JavaClass.of(objectType) );
    }

    public <T> Tryable<T> fromString( String in, Class<T> objectType ) {
        return fromString( in, JavaClass.of(objectType) );
    }

    public <T> Tryable<T> fromReader( Reader in, Class<T> objectType ) {
        return fromReader( in, JavaClass.of(objectType) );
    }

    public <T> Tryable<T> fromInputStream( InputStream in, Class<T> objectType ) {
        return fromInputStream( in, JavaClass.of(objectType) );
    }


    private <T> FPOption<Function1<String,T>> toDeserialiserFunction( JavaClass objectType ) {
        FPOption<JavaConstructor> constructor = objectType.getPublicConstructor(String.class);

        if ( constructor.hasValue() ) {
            return FP.option( str -> cast(constructor.get().newInstance( str.replaceAll( "/" + Backdoor.NEWLINE, Backdoor.NEWLINE ) )) );
        } else {
            FPOption<JavaMethod> method = objectType.getStaticMethod( objectType.getJdkClass(), "parseString", String.class );

            return method.map( m -> (str -> cast(m.invokeStaticAgainst(str.replaceAll("/"+Backdoor.NEWLINE, Backdoor.NEWLINE)))) );
        }
    }
}
