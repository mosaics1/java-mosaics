package mosaics.io.codecs;

import mosaics.io.resources.MimeType;

import java.util.Map;


/**
 *
 */
public interface ObjectCodecBuilder<T extends ObjectCodec> {

    public MimeType getCodecMimeType();

    public ObjectCodecBuilder<T> identifyInheritanceTreeByClassName( Class<?> baseType );
    public ObjectCodecBuilder<T> identifyInheritanceTreeByClassName( Class<?> baseType, String propertyName );

    public ObjectCodecBuilder<T> identifyInheritanceTreeByPreRegisteredIdentifiers( Class<?> baseType, Map<Class, String> mappings );
    public ObjectCodecBuilder<T> identifyInheritanceTreeByPreRegisteredIdentifiers( Class<?> baseType, String propertyName, Map<Class,String> mappings );

    public T create();

}
