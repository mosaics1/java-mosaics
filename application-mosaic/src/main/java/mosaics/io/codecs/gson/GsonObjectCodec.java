package mosaics.io.codecs.gson;

import mosaics.io.codecs.ObjectCodec;
import lombok.SneakyThrows;
import mosaics.fp.Try;
import mosaics.fp.Tryable;
import mosaics.io.IOUtils;
import mosaics.io.resources.MimeType;
import mosaics.json.JsonCodec;
import mosaics.lang.Backdoor;
import mosaics.lang.reflection.JavaClass;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.nio.charset.Charset;


/**
 * Marshals and un-marshals objects as UTF-8 encoded JSON strings.
 */
@SuppressWarnings("unchecked")
public class GsonObjectCodec implements ObjectCodec {
    private final JsonCodec jsonCodec;
    private final Charset defaultCharset;


    public GsonObjectCodec( JsonCodec jsonCodec ) {
        this( jsonCodec, Backdoor.UTF8 );
    }

    public GsonObjectCodec( JsonCodec jsonCodec, Charset defaultCharset ) {
        this.jsonCodec      = jsonCodec;
        this.defaultCharset = defaultCharset;
    }

    public MimeType getCodecMimeType() {
        return MimeType.JSON;
    }

    public <T> boolean supports( JavaClass objectType ) {
        return objectType.isPojo() || objectType.isString();
    }

    public <T> boolean supports( Class<T>  objectType ) {
        return supports( JavaClass.of(objectType) );
    }


    public String toString( Object o, JavaClass objectType ) {
        return toString( o, objectType.getJdkClass() );
    }

    public void writeTo( Object o, Writer out, JavaClass objectType ) {
        writeTo( o, out, objectType.getJdkClass() );
    }

    public void writeTo( Object o, OutputStream out, JavaClass objectType ) {
        writeTo( o, out, objectType.getJdkClass() );
    }

    public <T> Tryable<T> fromString( String in, JavaClass objectType ) {
        return fromString( in, objectType.getJdkClass() );
    }

    public <T> Tryable<T> fromReader( Reader in, JavaClass objectType ) {
        return fromReader( in, objectType.getJdkClass() );
    }

    public <T> Tryable<T> fromInputStream( InputStream in, JavaClass objectType ) {
        return fromInputStream( in, objectType.getJdkClass() );
    }


    public String toString( Object o, Class objectType ) {
        return jsonCodec.toJson( o, objectType );
    }

    @SneakyThrows
    public void writeTo( Object o, Writer out, Class objectType ) {
        out.append( jsonCodec.toJson(o,objectType) );
    }

    public void writeTo( Object o, OutputStream out, Class objectType ) {
        Writer writer = new OutputStreamWriter(out,defaultCharset);

        try {
            writeTo( o, writer, objectType );
        } finally {
            IOUtils.flush(writer);
        }
    }

    public <T> Tryable<T> fromString( String in, Class<T> objectType ) {
        return Try.invoke( () -> jsonCodec.fromJson(in, objectType) );
    }

    public <T> Tryable<T> fromReader( Reader in, Class<T> objectType ) {
        return Try.invoke( () -> jsonCodec.fromJson(in, objectType) );
    }

    public <T> Tryable<T> fromInputStream( InputStream in, Class<T> objectType ) {
        return Try.invoke( () -> jsonCodec.fromJson(new InputStreamReader(in, defaultCharset), objectType) );
    }
}






