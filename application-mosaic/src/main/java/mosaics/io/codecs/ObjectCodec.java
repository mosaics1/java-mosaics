package mosaics.io.codecs;

import mosaics.fp.Tryable;
import mosaics.io.IOUtils;
import mosaics.io.resources.MimeType;
import mosaics.lang.Backdoor;
import mosaics.lang.reflection.JavaClass;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;


/**
 * Capable of marshalling and unmarshalling Data Transfer Objects (DTOs).
 */
@SuppressWarnings({"rawtypes","unchecked"})
public interface ObjectCodec {

    public MimeType getCodecMimeType();


    public <T> boolean supports( Class<T> objectType );
    public void writeTo( Object o, Writer out, Class objectType );
    public <T> Tryable<T> fromReader( Reader in, Class<T> objectType );




    public default <T> boolean supports( JavaClass  objectType ) {
        return supports( objectType.getJdkClass() );
    }

    public default void writeTo( Object o, Writer out, JavaClass objectType ) {
        writeTo( o, out, objectType.getJdkClass() );
    }

    public default <T> Tryable<T> fromReader( Reader in, JavaClass objectType ) {
        return fromReader( in, objectType.getJdkClass() );
    }

    public default String toString( Object o, JavaClass objectType ) {
        StringWriter stringWriter = new StringWriter();
        writeTo( o, stringWriter, objectType );

        return stringWriter.toString();
    }

    public default void writeTo( Object o, OutputStream out, JavaClass objectType ) {
        writeTo( o, new OutputStreamWriter(out, Backdoor.UTF8), objectType );
    }

    public default <T> Tryable<T> fromString( String in, JavaClass objectType ) {
        return fromReader( new StringReader(in), objectType );
    }

    public default <T> Tryable<T> fromInputStream( InputStream in, JavaClass objectType ) {
        return fromReader( new InputStreamReader(in, Backdoor.UTF8), objectType );
    }

    public default String toString( Object o, Class objectType ) {
        StringWriter stringWriter = new StringWriter();
        writeTo( o, stringWriter, objectType );

        return stringWriter.toString();
    }

    public default void writeTo( Object o, OutputStream out, Class objectType ) {
        OutputStreamWriter osw = new OutputStreamWriter( out, Backdoor.UTF8 );

        writeTo( o, osw, objectType );

        IOUtils.flush( osw );
    }

    public default <T> Tryable<T> fromString( String in, Class<T> objectType ) {
        return fromReader( new StringReader(in), objectType );
    }

    public default <T> Tryable<T> fromInputStream( InputStream in, Class<T> objectType ) {
        return fromReader( new InputStreamReader(in, Backdoor.UTF8), objectType );
    }



    public default <T> SpecialisedObjectCodec<T> specializeCodec( Class<T> objectType ) {
        return specializeCodec( JavaClass.of(objectType) );
    }

    public default <T> SpecialisedObjectCodec<T> specializeCodec( JavaClass objectType ) {
        final ObjectCodec codec = this;

        return new SpecialisedObjectCodec<T>() {
            public MimeType getCodecMimeType() {
                return codec.getCodecMimeType();
            }

            public boolean supports( JavaClass candidateType ) {
                return candidateType.isAssignableTo( objectType );
            }

            public String toString( T o ) {
                return codec.toString( o, objectType );
            }

            public void writeTo( T o, Writer out ) {
                codec.writeTo( o, out, objectType );
            }

            public void writeTo( T o, OutputStream out ) {
                codec.writeTo( o, out, objectType );
            }

            public Tryable<T> fromString( String in ) {
                return ObjectCodec.this.fromString( in, objectType );
            }

            public Tryable<T> fromReader( Reader in ) {
                return ObjectCodec.this.fromReader( in, objectType );
            }

            public Tryable<T> fromInputStream( InputStream in ) {
                return ObjectCodec.this.fromInputStream( in, objectType );
            }
        };
    }

}



