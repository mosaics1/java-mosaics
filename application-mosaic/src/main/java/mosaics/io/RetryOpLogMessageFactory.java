package mosaics.io;

import mosaics.fp.Failure;
import mosaics.logging.LogMessage;
import mosaics.logging.LogMessageFactory;
import mosaics.logging.LogMessageProxyFactory;
import mosaics.logging.loglevels.OpsWarn;

import java.time.Duration;


public interface RetryOpLogMessageFactory extends LogMessageFactory {
    public static final RetryOpLogMessageFactory INSTANCE = LogMessageProxyFactory.INSTANCE.createLogMessageFactory( RetryOpLogMessageFactory.class );


    @OpsWarn(
        "$action failed (attempt=$attemptNum out of $maxAttemptCount).  Will retry in $delay.  The failure was:  $ex"
    )
    public LogMessage retryFailedWillRetryAgain( String action, long attemptNum, long maxAttemptCount, Duration delay, Failure ex );

    @OpsWarn( "$action failed (attempt=$attemptNum).  Max attempts exceeded, will not try again.  The failure was:  $failure" )
    public LogMessage retryFailedForTheLastTimeActionAborted( String action, long attemptNum, Throwable failure );

    @OpsWarn( "$action failed with non-retryable exception. The failure was:  $failure" )
    public LogMessage opFailedWithNonRetryableException( String action, Failure failure );
}
