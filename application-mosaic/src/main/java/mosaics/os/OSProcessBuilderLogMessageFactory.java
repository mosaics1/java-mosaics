package mosaics.os;

import mosaics.fp.Failure;
import mosaics.fp.collections.FPOption;
import mosaics.logging.LogMessage;
import mosaics.logging.LogMessageFactory;
import mosaics.logging.LogMessageProxyFactory;
import mosaics.logging.loglevels.OpsInfo;

import java.io.File;
import java.io.IOException;


public interface OSProcessBuilderLogMessageFactory extends LogMessageFactory {
    public static final OSProcessBuilderLogMessageFactory INSTANCE = LogMessageProxyFactory.INSTANCE.createLogMessageFactory( OSProcessBuilderLogMessageFactory.class );

    @OpsInfo("Starting new child process $targetWorkingDirectory $cmdWithArgs")
    public LogMessage startingNewChildProcess( File targetWorkingDirectory, String[] cmdWithArgs );

    @OpsInfo("New child process ($pid) started $targetWorkingDirectory $cmdWithArgs")
    public LogMessage newChildProcessStarted( FPOption<File> targetWorkingDirectory, String[] cmdWithArgs, long pid );

    @OpsInfo("Request to abort child process $pid $f")
    public LogMessage requestToAbortChildProcessReceived( long pid, Failure f );

    @OpsInfo("Issuing interrupt to child process $pid")
    public LogMessage issueingInterruptToChildProcess( long pid );

    @OpsInfo("Interrupt did not kill child process $pid in ${timeoutMillis:duration}")
    public LogMessage interruptDidNotKillChildProcess( long pid, long timeoutMillis );

    @OpsInfo("Issuing kill signal to child process $pid")
    public LogMessage issuingKillToChildProcess( long pid );

    @OpsInfo("Kill did not kill child process $pid in ${timeoutMillis:duration}")
    public LogMessage killDidNotKillChildProcess( long pid, long timeoutMillis );

    @OpsInfo("Child process $pid killed in ${durationMillis:duration}")
    public LogMessage childProcessKilled( long pid, double durationMillis );

    @OpsInfo("Interrupt of $pid not supported by OS:  $ex")
    public LogMessage interruptNotSupportedByOS( long pid, IOException ex );

    @OpsInfo("Killing child process $pid as JVM is exiting")
    public LogMessage killingChildProcessAsJVMIsExiting( long pid );
}
