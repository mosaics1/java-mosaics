package mosaics.os;

import io.reactivex.Flowable;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import mosaics.concurrency.futures.Promise;
import mosaics.fp.ExceptionFailure;
import mosaics.fp.LazyVal;
import mosaics.io.writers.NullPrintWriter;
import mosaics.lang.functions.BooleanFunction0;
import mosaics.lang.functions.Function0;

import java.io.PrintWriter;


@EqualsAndHashCode(of="pid")
public class OSProcess {
    @Getter private long                      pid;
            private Promise<Integer>          exitCodePromise;
            private LazyVal<Flowable<String>> stdoutObservable;
            private LazyVal<Flowable<String>> stderrObservable;
            private LazyVal<PrintWriter>      stdinWriter;
            private BooleanFunction0          isRunning;


    OSProcess( int pid, Promise<Integer> exitCodePromise ) {
        this( pid, exitCodePromise, Flowable::empty, Flowable::empty, NullPrintWriter::nullPrintWriter, () -> false );
    }

    OSProcess(
        long                        pid,
        Promise<Integer>            exitCodePromise,
        Function0<Flowable<String>> stdoutFetcher,
        Function0<Flowable<String>> stderrFetcher,
        Function0<PrintWriter>      stdinWriter,
        BooleanFunction0            isRunning
    ) {
        this.pid              = pid;
        this.exitCodePromise  = exitCodePromise;
        this.stdoutObservable = new LazyVal<>( stdoutFetcher );
        this.stderrObservable = new LazyVal<>( stderrFetcher );
        this.stdinWriter      = new LazyVal<>( stdinWriter );
        this.isRunning        = isRunning;
    }

    public Flowable<String> getStdout() {
        return stdoutObservable.fetch();
    }

    public Flowable<String> getStderr() {
        return stderrObservable.fetch();
    }

    public PrintWriter getStdin() {
        return stdinWriter.fetch();
    }

    public Promise<Integer> toPromise() {
        return exitCodePromise;
    }

    public boolean isRunning() {
        return isRunning.invoke();
    }


    public void killProcess() {
        killProcess( "" );
    }

    public void killProcess( String reason ) {
        if ( isRunning() ) {
            exitCodePromise.setFailure( new ExceptionFailure(reason) );
        }
    }

    /**
     * As getStdout but used with apps that output json copies of LogEvent to the console.  This method
     * returns a class that helps with scanning the json objects.
     */
    public StdOutEventSink getStdoutAsEventsSink() {
        return new StdOutEventSink( getStdout() );
    }
}
