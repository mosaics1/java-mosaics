package mosaics.os;

import mosaics.concurrency.futures.Promise;
import mosaics.fp.collections.Tuple2;

import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Timer;
import java.util.TimerTask;


/**
 * Schedules a background check of a child process to see if it has exited and if so what its status was.
 */
class ProcessMonitor {

    private static final List<Tuple2<Process, Promise<Integer>>> checkList   = new LinkedList<>();
    private static       Timer                                   timer       = null;
    private static       boolean                                 isScheduled = false;


    public static void push( Process p, Promise<Integer> exitCodePromise ) {
        synchronized (checkList) {
            checkList.add( new Tuple2<>(p,exitCodePromise) );

            if ( !isScheduled ) {
                scheduleCheck();
            }
        }
    }


    private static void scheduleCheck() {
        timer = new Timer("ProcessMonitorTimer");
        timer.schedule( new CheckActiveProcessesTask(), 300L );

        isScheduled = true;
    }

    private static class CheckActiveProcessesTask extends TimerTask {
        public void run() {
            synchronized (checkList) {
                ListIterator<Tuple2<Process, Promise<Integer>>> it = checkList.listIterator();

                while ( it.hasNext() ) {
                    Tuple2<Process, Promise<Integer>> tuple = it.next();

                    Process p = tuple.getFirst();
                    if ( !p.isAlive() ) {
                        if ( !tuple.getSecond().isComplete() ) {
                            tuple.getSecond().setResult( p.exitValue() );
                        }

                        it.remove();
                    }
                }

                if ( checkList.isEmpty() ) {
                    isScheduled = false;
                    timer.cancel();
                    timer = null;
                } else {
                    scheduleCheck();
                }
            }
        }
    }

}
