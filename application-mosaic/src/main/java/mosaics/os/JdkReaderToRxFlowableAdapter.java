package mosaics.os;

import io.reactivex.Flowable;
import lombok.AllArgsConstructor;
import mosaics.lang.functions.BooleanFunction0;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.concurrent.atomic.AtomicLong;


/**
 * Create a Flowable whose source is the specified Reader.  The Flowable will keep reading from
 * the reader until the reader is exhausted, closed
 */
@AllArgsConstructor
public class JdkReaderToRxFlowableAdapter extends Flowable<String> {

    private Reader           reader;
    private BooleanFunction0 isWriterAlive;


    public JdkReaderToRxFlowableAdapter( Reader in ) {
        this( in, () -> true );
    }

    public JdkReaderToRxFlowableAdapter( InputStream in ) {
        this( new InputStreamReader(in), () -> true );
    }

    public JdkReaderToRxFlowableAdapter( InputStream in, BooleanFunction0 isWriterAlive ) {
        this( new InputStreamReader(in), isWriterAlive );
    }

    protected void subscribeActual( Subscriber<? super String> subscriber ) {
        subscriber.onSubscribe( new Subscription() {
            private volatile boolean isRunning = true;
            private AtomicLong remainingCount = new AtomicLong();

            public synchronized void request( long n ) {
                if ( !isRunning ) {
                    return;
                }

                remainingCount.addAndGet( n );

                BufferedReader in = new BufferedReader( reader );

                try {
                    while ( isRunning && remainingCount.get() > 0  ) {
                        String nextLine = in.readLine();

                        if ( nextLine == null ) {
                            remainingCount.set( 0 );

                            subscriber.onComplete();
                        } else {
                            remainingCount.decrementAndGet();

                            subscriber.onNext( nextLine );
                        }
                    }

                    if ( !reader.ready() && !isWriterAlive.invoke() ) {
                        subscriber.onComplete();
                    }
                } catch ( IOException e ) {
                    remainingCount.set( 0 );

                    subscriber.onComplete();
                }
            }

            public void cancel() {
                isRunning = false;
                remainingCount.set( 0 );

                subscriber.onComplete();
            }
        } );
    }

}
