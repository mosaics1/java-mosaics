package mosaics.http.jdkclient;

import lombok.Value;
import mosaics.cli.Env;
import mosaics.cli.requests.Request;
import mosaics.cli.runner.AppLogMessageFactory;
import mosaics.fp.FP;
import mosaics.http.HttpCookie;
import mosaics.http.HttpPayload;
import mosaics.http.HttpRequest;
import mosaics.http.HttpResponse;
import mosaics.http.HttpURL;
import mosaics.http.HttpClient;
import mosaics.http.jdkclient.JdkHttpClient;
import mosaics.io.resources.MimeType;
import mosaics.lang.time.DTM;
import mosaics.logging.writers.CapturingLogWriter;
import mosaics.net.HostUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockserver.integration.ClientAndServer;
import org.mockserver.model.MediaType;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;


public class JdkHttpClientTest {
    private static final DTM T0 = new DTM( 2020,1,1, 10,30 );

    private CapturingLogWriter capturingLogWriter = new CapturingLogWriter();
    private Env                env                = Env.createTestEnv(T0, capturingLogWriter);
    private Request            req                = env.createRequest( "junit" );

    private ClientAndServer mockServer;
    private HttpClient httpClient;
    private HttpURL         baseUrl;


//    private ObjectCodecs codecs = new ObjectCodecsBuilder()
//        .supportsGson()
//        .supportsText()
//        .create();
//

    @BeforeEach
    public void setup() {
        this.mockServer = ClientAndServer.startClientAndServer( 0 );
        this.httpClient = new JdkHttpClient( "httpClient" );
        this.baseUrl    = HttpURL.of("http","localhost",mockServer.getPort());

        httpClient.start();

        HostUtils.spinUntilAvailable( 20_000, 100, "localhost", mockServer.getPort() );
    }

    @AfterEach
    public void tearDown() {
        this.httpClient.stop();
        this.mockServer.stop();
    }

    @Nested
    public class GETTestCases {
        @Test
        public void getRequest300NoBody() throws IOException {
            mockServer.when(
                request( "/foo" ).withMethod( "GET" )
            ).respond(
                response().withStatusCode( 300 )
            );

            HttpRequest httpRequest  = HttpRequest.GET( baseUrl+"/foo" );
            HttpResponse actual       = httpClient.invoke( req, httpRequest );

            HttpResponse expected = HttpResponse.of( 300 )
                .appendHeaders(
                    "content-length", "0",
                    "connection", "keep-alive"
                );

            assertEquals( expected, actual );
        }

        @Test
        public void getRequest_expectLog() throws IOException {
            mockServer.when(
                request( "/foo" ).withMethod( "GET" )
            ).respond(
                response().withStatusCode(300)
            );

            capturingLogWriter.clear();

            HttpRequest httpRequest = HttpRequest.GET( baseUrl+"/foo" );

            httpClient.invoke( req, httpRequest );

            HttpResponse expected = HttpResponse
                .of( 300 )
                .appendHeaders(
                    "content-length", "0",
                    "connection", "keep-alive"
                );

            capturingLogWriter.assertLoggedMessages(
                AppLogMessageFactory.INSTANCE.requestStarted(),
                AppLogMessageFactory.INSTANCE.requestSucceeded(expected)
            );
        }

        // todo incRemoteCount
        // todo incRemoteBytes

        @Test
        public void getRequest200BodyAsString() throws IOException {
            mockServer.when(
                request( "/foo" ).withMethod( "GET" )
            ).respond(
                response()
                    .withStatusCode( 200 )
                    .withHeader( "Content-Type", MediaType.TEXT_PLAIN.toString() )
                    .withBody( "ABC 123" )
            );

            HttpRequest httpRequest = HttpRequest.GET( baseUrl+"/foo" );
            HttpResponse actual      = httpClient.invoke( req, httpRequest );

            HttpResponse expected = HttpResponse
                .of( 200 )
                .appendHeaders(
                    "content-length", "7",
                    "connection", "keep-alive",
                    "content-type", "text/plain"
                )
                .withBody( HttpPayload.of(MimeType.TEXT,"ABC 123") );

            assertEquals( expected, actual );
        }

        @Test
        public void getRequest200BodyAsJson() throws IOException {
            mockServer.when(
                request( "/foo" ).withMethod( "GET" )
            ).respond(
                response()
                    .withStatusCode( 200 )
                    .withHeader( "Content-Type", "application/json; charset=UTF-8" )
                    .withBody( "{\"name\":\"Bob\"}" )
            );

            HttpRequest httpRequest = HttpRequest.GET( baseUrl+"/foo" );
            HttpResponse actual      = httpClient.invoke( req, httpRequest );

            HttpResponse expected = HttpResponse
                .of( 200 )
                .appendHeaders(
                    "content-length", "14",
                    "connection", "keep-alive",
                    "content-type", "application/json; charset=UTF-8"
                )
                .withBody( HttpPayload.of(MimeType.JSON,"{\"name\":\"Bob\"}") );

            assertEquals( expected, actual );
        }

        @Test
        public void getRequest404() throws IOException {
            mockServer.when(
                request( "/foo" ).withMethod( "GET" )
            ).respond(
                response()
                    .withStatusCode( 200 )
                    .withHeader( "Content-Type", "application/json; charset=UTF-8" )
                    .withBody( "{\"name\":\"Bob\"}" )
            );

            HttpRequest httpRequest = HttpRequest.GET( baseUrl+"/bar" );
            HttpResponse actual      = httpClient.invoke( req, httpRequest );

            HttpResponse expected = HttpResponse
                .of( 404 )
                .appendHeaders(
                    "content-length", "0",
                    "connection", "keep-alive"
                );

            assertEquals( expected, actual );
        }

        @Test
        public void getRequestWith1QueryParameter() throws IOException {
            mockServer.when(
                request( "/foo" )
                    .withMethod( "GET" )
                    .withQueryStringParameter( "a", "1" )
            ).respond(
                response()
                    .withStatusCode( 200 )
                    .withHeader( "Content-Type", "application/json; charset=UTF-8" )
                    .withBody( "{\"name\":\"Bob\"}" )
            );

            HttpRequest httpRequest = HttpRequest.GET( baseUrl+"/foo?a=1" );
            HttpResponse actual      = httpClient.invoke( req, httpRequest );

            HttpResponse expected = HttpResponse
                .of( 200 )
                .appendHeaders(
                    "content-length", "14",
                    "connection", "keep-alive",
                    "content-type", "application/json; charset=UTF-8"
                )
                .withBody( HttpPayload.of(MimeType.JSON,"{\"name\":\"Bob\"}") );

            assertEquals( expected, actual );
        }

        @Test
        public void getRequestWith2QueryParameter() throws IOException {
            mockServer.when(
                request( "/foo" )
                    .withMethod( "GET" )
                    .withQueryStringParameter( "a", "1" )
                    .withQueryStringParameter( "b", "2" )
            ).respond(
                response()
                    .withStatusCode( 200 )
                    .withHeader( "Content-Type", "application/json; charset=UTF-8" )
                    .withBody( "{\"name\":\"Bob\"}" )
            );

            HttpRequest httpRequest = HttpRequest.GET( baseUrl+"/foo?a=1&b=2" );
            HttpResponse actual      = httpClient.invoke( req, httpRequest );

            HttpResponse expected = HttpResponse
                .of( 200 )
                .appendHeaders(
                    "content-length", "14",
                    "connection", "keep-alive",
                    "content-type", "application/json; charset=UTF-8"
                )
                .withBody( HttpPayload.of(MimeType.JSON,"{\"name\":\"Bob\"}") );

            assertEquals( expected, actual );
        }

        @Test
        public void getRequestWithQueryParameterContainingSpecialCharacters() throws IOException {
            mockServer.when(
                request( "/foo" )
                    .withMethod( "GET" )
                    .withQueryStringParameter( "a", "<&>" )
            ).respond(
                response()
                    .withStatusCode( 200 )
                    .withHeader( "Content-Type", "application/json; charset=UTF-8" )
                    .withBody( "{\"name\":\"Bob\"}" )
            );

            HttpRequest httpRequest = HttpRequest.GET( baseUrl+"/foo" ).appendQueryParameter( "a", "<&>" );
            HttpResponse actual      = httpClient.invoke( req, httpRequest );

            HttpResponse expected = HttpResponse
                .of( 200 )
                .appendHeaders(
                    "content-length", "14",
                    "connection", "keep-alive",
                    "content-type", "application/json; charset=UTF-8"
                )
                .withBody( HttpPayload.of(MimeType.JSON,"{\"name\":\"Bob\"}") );

            assertEquals( expected, actual );
        }

        @Test
        public void getRequestWithCookie() throws IOException {
            mockServer.when(
                request( "/foo" )
                    .withMethod( "GET" )
                    .withHeader( "Cookie", "a=1" )
            ).respond(
                response()
                    .withStatusCode( 200 )
                    .withHeader( "Content-Type", "application/json; charset=UTF-8" )
                    .withBody( "{\"name\":\"Bob\"}" )
            );

            HttpRequest httpRequest = HttpRequest.GET( baseUrl+"/foo" ).appendCookie( HttpCookie.of("a","1") );
            HttpResponse actual = httpClient.invoke( req, httpRequest );

            HttpResponse expected = HttpResponse
                .of( 200 )
                .appendHeaders(
                    "content-length", "14",
                    "connection", "keep-alive",
                    "content-type", "application/json; charset=UTF-8"
                )
                .withBody( HttpPayload.of(MimeType.JSON,"{\"name\":\"Bob\"}") );

            assertEquals( expected, actual );
        }

        @Test
        public void getRequestWithCookies() throws IOException {
            mockServer.when(
                request( "/foo" )
                    .withMethod( "GET" )
                    .withHeader( "Cookie", "a=1; b=2" )
            ).respond(
                response()
                    .withStatusCode( 200 )
                    .withHeader( "Content-Type", "application/json; charset=UTF-8" )
                    .withBody( "{\"name\":\"Bob\"}" )
            );

            HttpRequest httpRequest = HttpRequest.GET( baseUrl+"/foo" )
                .appendCookie( HttpCookie.of("a","1") )
                .appendCookie( HttpCookie.of("b","2") );

            HttpResponse actual = httpClient.invoke( req, httpRequest );

            HttpResponse expected = HttpResponse
                .of( 200 )
                .appendHeaders(
                    "content-length", "14",
                    "connection", "keep-alive",
                    "content-type", "application/json; charset=UTF-8"
                )
                .withBody( HttpPayload.of(MimeType.JSON,"{\"name\":\"Bob\"}") );

            assertEquals( expected, actual );
        }

        @Test
        public void getRequestWithAnExpiredCookie_expectTheExpiredCookieToGetDropped() throws IOException {
            mockServer.when(
                request( "/foo" )
                    .withMethod( "GET" )
                    .withHeader( "Cookie", "b=2" )
            ).respond(
                response()
                    .withStatusCode( 200 )
                    .withHeader( "Content-Type", "application/json; charset=UTF-8" )
                    .withBody( "{\"name\":\"Bob\"}" )
            );

            HttpRequest httpRequest = HttpRequest.GET( baseUrl+"/foo" )
                .appendCookie( HttpCookie.of("a","1").withExpiresAtMillis(T0.getMillisSinceEpoch()) )
                .appendCookie( HttpCookie.of("b","2") );

            HttpResponse actual = httpClient.invoke( req, httpRequest );

            HttpResponse expected = HttpResponse
                .of( 200 )
                .appendHeaders(
                    "content-length", "14",
                    "connection", "keep-alive",
                    "content-type", "application/json; charset=UTF-8"
                )
                .withBody( HttpPayload.of(MimeType.JSON,"{\"name\":\"Bob\"}") );

            assertEquals( expected, actual );
        }

        @Test
        public void getRequestWithCookieThatHasMismatchedDomain_expectCookieToNotBeSent() throws IOException {
            mockServer.when(
                request( "/foo" )
                    .withMethod( "GET" )
                    .withHeader( "Cookie", "a=1" )
            ).respond(
                response()
                    .withStatusCode( 200 )
                    .withHeader( "Content-Type", "application/json; charset=UTF-8" )
                    .withBody( "{\"name\":\"Bob\"}" )
            );

            HttpRequest httpRequest = HttpRequest.GET( baseUrl+"/foo" )
                .appendCookie( HttpCookie.of("a","1") )
                .appendCookie( HttpCookie.of("b","2").withDomain(FP.option("a.com")) );

            HttpResponse actual = httpClient.invoke( req, httpRequest );

            HttpResponse expected = HttpResponse
                .of( 200 )
                .appendHeaders(
                    "content-length", "14",
                    "connection", "keep-alive",
                    "content-type", "application/json; charset=UTF-8"
                )
                .withBody( HttpPayload.of(MimeType.JSON,"{\"name\":\"Bob\"}") );

            assertEquals( expected, actual );
        }

        @Test
        public void getRequestWithCookieThatHasMatchedDomain_expectCookieToBeSent() throws IOException {
            mockServer.when(
                request( "/foo" )
                    .withMethod( "GET" )
                    .withHeader( "Cookie", "a=1; b=2" )
            ).respond(
                response()
                    .withStatusCode( 200 )
                    .withHeader( "Content-Type", "application/json; charset=UTF-8" )
                    .withBody( "{\"name\":\"Bob\"}" )
            );

            HttpRequest httpRequest = HttpRequest.GET( baseUrl+"/foo" )
                .appendCookie( HttpCookie.of("a","1") )
                .appendCookie( HttpCookie.of("b","2").withDomain(FP.option(baseUrl.getHost())) );

            HttpResponse actual = httpClient.invoke( req, httpRequest );

            HttpResponse expected = HttpResponse
                .of( 200 )
                .appendHeaders(
                    "content-length", "14",
                    "connection", "keep-alive",
                    "content-type", "application/json; charset=UTF-8"
                )
                .withBody( HttpPayload.of(MimeType.JSON,"{\"name\":\"Bob\"}") );

            assertEquals( expected, actual );
        }

        @Test
        public void getRequestWithCookieThatHasMismatchedPath_expectCookieToNotBeSent() throws IOException {
            mockServer.when(
                request( "/foo" )
                    .withMethod( "GET" )
                    .withHeader( "Cookie", "a=1" )
            ).respond(
                response()
                    .withStatusCode( 200 )
                    .withHeader( "Content-Type", "application/json; charset=UTF-8" )
                    .withBody( "{\"name\":\"Bob\"}" )
            );

            HttpRequest httpRequest = HttpRequest.GET( baseUrl+"/foo" )
                .appendCookie( HttpCookie.of("a","1") )
                .appendCookie( HttpCookie.of("b","2").withPath("/foo/bar") );

            HttpResponse actual = httpClient.invoke( req, httpRequest );

            HttpResponse expected = HttpResponse
                .of( 200 )
                .appendHeaders(
                    "content-length", "14",
                    "connection", "keep-alive",
                    "content-type", "application/json; charset=UTF-8"
                )
                .withBody( HttpPayload.of(MimeType.JSON,"{\"name\":\"Bob\"}") );

            assertEquals( expected, actual );
        }

        @Test
        public void getRequestWithCookieThatHasMatchedPath_expectCookieToBeSent() throws IOException {
            mockServer.when(
                request( "/foo" )
                    .withMethod( "GET" )
                    .withHeader( "Cookie", "a=1; b=2" )
            ).respond(
                response()
                    .withStatusCode( 200 )
                    .withHeader( "Content-Type", "application/json; charset=UTF-8" )
                    .withBody( "{\"name\":\"Bob\"}" )
            );

            HttpRequest httpRequest = HttpRequest.GET( baseUrl+"/foo" )
                .appendCookie( HttpCookie.of("a","1") )
                .appendCookie( HttpCookie.of("b","2").withPath("/foo") );

            HttpResponse actual = httpClient.invoke( req, httpRequest );

            HttpResponse expected = HttpResponse
                .of( 200 )
                .appendHeaders(
                    "content-length", "14",
                    "connection", "keep-alive",
                    "content-type", "application/json; charset=UTF-8"
                )
                .withBody( HttpPayload.of(MimeType.JSON,"{\"name\":\"Bob\"}") );

            assertEquals( expected, actual );
        }

        @Test
        public void getRequestWithHeaders() throws IOException {
            mockServer.when(
                request( "/foo" )
                    .withMethod( "GET" )
                    .withHeader( "H1", "v1" )
                    .withHeader( "H2", "v2" )
            ).respond(
                response()
                    .withStatusCode( 200 )
                    .withHeader( "Content-Type", "application/json; charset=UTF-8" )
                    .withBody( "{\"name\":\"Bob\"}" )
            );

            HttpRequest httpRequest = HttpRequest.GET( baseUrl+"/foo" )
                .appendHeader( "H1", "v1" )
                .appendHeader( "H2", "v2" );

            HttpResponse actual = httpClient.invoke( req, httpRequest );

            HttpResponse expected = HttpResponse
                .of( 200 )
                .appendHeaders(
                    "content-length", "14",
                    "connection", "keep-alive",
                    "content-type", "application/json; charset=UTF-8"
                )
                .withBody( HttpPayload.of(MimeType.JSON,"{\"name\":\"Bob\"}") );

            assertEquals( expected, actual );
        }
    }



    @Nested
    public class POSTTestCases {
        @Test
        public void postRequest300NoBody() throws IOException {
            mockServer.when(
                request( "/foo" ).withMethod( "POST" )
            ).respond(
                response().withStatusCode( 300 )
            );

            HttpRequest httpRequest = HttpRequest.POST( baseUrl+"/foo" ).appendQueryParameter( "a", "<&>" );
            HttpResponse actual      = httpClient.invoke( req, httpRequest );

            HttpResponse expected = HttpResponse
                .of( 300 )
                .appendHeaders(
                    "content-length", "0",
                    "connection", "keep-alive"
                );

            assertEquals( expected, actual );
        }

        @Test
        public void postRequest200BodyAsString() throws IOException {
            mockServer.when(
                request( "/foo" ).withMethod( "POST" )
            ).respond(
                response()
                    .withStatusCode( 200 )
                    .withBody( "Hello", MediaType.TEXT_PLAIN )
            );

            HttpRequest httpRequest = HttpRequest.POST( baseUrl+"/foo" );
            HttpResponse actual      = httpClient.invoke( req, httpRequest );

            HttpResponse expected = HttpResponse
                .of( 200 )
                .appendHeaders(
                    "content-length", "5",
                    "connection", "keep-alive",
                    "content-type", "text/plain"
                ).withBody( HttpPayload.plainText("Hello") );

            assertEquals( expected, actual );
        }

        @Test
        public void postRequest200BodyAsJson() throws IOException {
            mockServer.when(
                request( "/foo" ).withMethod( "POST" )
            ).respond(
                response()
                    .withStatusCode( 200 )
                    .withBody( "{}", MediaType.APPLICATION_JSON )
            );

            HttpRequest httpRequest = HttpRequest.POST( baseUrl+"/foo" );
            HttpResponse actual      = httpClient.invoke( req, httpRequest );

            HttpResponse expected = HttpResponse
                .of( 200 )
                .appendHeaders(
                    "content-length", "2",
                    "connection", "keep-alive",
                    "content-type", "application/json"
                ).withBody( HttpPayload.json("{}") );

            assertEquals( expected, actual );
        }

        @Test
        public void postRequest404() throws IOException {
            mockServer.when(
                request( "/foo" ).withMethod( "POST" )
            ).respond(
                response()
                    .withStatusCode( 200 )
                    .withBody( "{}", MediaType.APPLICATION_JSON )
            );

            HttpRequest httpRequest = HttpRequest.POST( baseUrl+"/bar" );
            HttpResponse actual      = httpClient.invoke( req, httpRequest );

            HttpResponse expected = HttpResponse
                .of( 404 )
                .appendHeaders(
                    "content-length", "0",
                    "connection", "keep-alive"
                );

            assertEquals( expected, actual );
        }

        @Test
        public void POSTRequestWithRequestHeader() throws IOException {
            mockServer.when(
                request( "/foo" )
                    .withMethod( "POST" )
                    .withHeader( "A", "1" )
            ).respond(
                response()
                    .withStatusCode( 200 )
                    .withBody( "{}", MediaType.APPLICATION_JSON )
            );

            HttpRequest httpRequest = HttpRequest.POST( baseUrl+"/bar" )
                .appendHeader( "A", "1" );
            HttpResponse actual      = httpClient.invoke( req, httpRequest );

            HttpResponse expected = HttpResponse
                .of( 404 )
                .appendHeaders(
                    "content-length", "0",
                    "connection", "keep-alive"
                );

            assertEquals( expected, actual );
        }

        @Test
        public void postRequestWithBody() throws IOException {
            mockServer.when(
                request( "/foo" )
                    .withContentType( MediaType.TEXT_PLAIN )
                    .withMethod( "POST" )
                    .withBody( "ABC" )
            ).respond(
                response()
                    .withStatusCode( 200 )
            );

            HttpRequest httpRequest = HttpRequest.POST( baseUrl+"/foo" )
                .withBody( HttpPayload.plainText("ABC") );
            HttpResponse actual      = httpClient.invoke( req, httpRequest );

            HttpResponse expected = HttpResponse
                .of( 200 )
                .appendHeaders(
                    "content-length", "0",
                    "connection", "keep-alive"
                );

            assertEquals( expected, actual );
        }
    }

    @Value
    public static class Name {
        private String name;
    }
}
