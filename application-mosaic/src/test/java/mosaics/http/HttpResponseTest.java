package mosaics.http;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class HttpResponseTest {
    @Test
    public void testWithHeaders() {
        HttpResponse orig = HttpResponse.of(200);

        assertSame( orig, orig.withHeaders(HttpHeaders.empty()) );
    }
}
