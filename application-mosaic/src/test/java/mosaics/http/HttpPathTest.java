package mosaics.http;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class HttpPathTest {
    @Test
    public void givenEmptyPath() {
        HttpPath path = HttpPath.empty();

        assertEquals( "", path.toString() );
    }

    @Test
    public void givenOnePathPart() {
        HttpPath path = HttpPath.of("a");

        assertEquals( "/a", path.toString() );
    }

    @Test
    public void givenTwoPathParts() {
        HttpPath path = HttpPath.of("a","b");

        assertEquals( "/a/b", path.toString() );
    }

    @Test
    public void givenOnePathPart_callAppend() {
        HttpPath path = HttpPath.of("a").append("b");

        assertEquals( "/a/b", path.toString() );
    }

    @Test
    public void testOfWithVarArgs() {
        assertEquals( "", HttpPath.of("").toString() );
        assertEquals( "/", HttpPath.of("/").toString() );
        assertEquals( "/a", HttpPath.of("a").toString() );
        assertEquals( "/a", HttpPath.of("/a").toString() );
        assertEquals( "/a/b", HttpPath.of("/a/b").toString() );
        assertEquals( "/a/b/", HttpPath.of("/a/b/").toString() );
    }

    @Test
    public void testStartsWith() {
        assertTrue(HttpPath.of("/a/b").startsWith(HttpPath.of("/a/b")));
        assertTrue(HttpPath.of("/a/b").startsWith(HttpPath.of("/a/")));
        assertTrue(HttpPath.of("/a/b").startsWith(HttpPath.of("/a")));
        assertTrue(HttpPath.of("/a/b").startsWith(HttpPath.of("/")));
        assertTrue(HttpPath.of("/a/b").startsWith(HttpPath.of("")));
        assertTrue(HttpPath.of("/a/b").startsWith(HttpPath.empty()));
    }
}
