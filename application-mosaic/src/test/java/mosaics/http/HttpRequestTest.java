package mosaics.http;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class HttpRequestTest {
    @Test
    public void testGetShortDescription() {
        assertEquals( "GET http://a.com/b/c", HttpRequest.GET("http://a.com/b/c").getShortDescription() );
        assertEquals( "GET http://a.com/b/c?q1=a&q2=b", HttpRequest.GET("http://a.com/b/c?q1=a&q2=b").getShortDescription() );
        assertEquals( "GET http://a.com/b/c?q1=a&q2=b", HttpRequest.GET("http://a.com/b/c?q1=a&q2=b").appendHeader("h1","v1").getShortDescription() );
        assertEquals( "POST http://a.com/b/c", HttpRequest.POST("http://a.com/b/c").appendHeader("h1","v1").getShortDescription() );
    }

//    @Test   TODO held off because this would be best implemented with a lombok @Validator -- which does not yet exist :(
//    public void givenGetWithBody_expectException() {
//        IllegalStateException ex = assertThrows(IllegalStateException.class, () -> HttpRequestDetails.GET("http://a.com/b/c").withJsonBody( "[]" ) );
//
//        assertEquals( "", ex.getMessage() );
//    }

    @Test
    public void testAppendCookie() {
        HttpRequest initialRequest = HttpRequest.GET("http://a.com/a/b");
        HttpCookie  cookie1        = HttpCookie.of("A", "1");
        HttpCookie  cookie2        = HttpCookie.of("B", "2");

        HttpCookies cookies1 = initialRequest.appendCookie( cookie1 ).getCookies();
        assertEquals(HttpCookies.of(cookie1), cookies1 );
        assertEquals( cookies1, cookies1.appendCookie(cookie1) );
        assertEquals(HttpCookies.of(cookie1,cookie2), cookies1.appendCookie(cookie2) );
    }
}
