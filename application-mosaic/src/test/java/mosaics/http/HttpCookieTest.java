package mosaics.http;

import lombok.SneakyThrows;
import mosaics.fp.FP;
import mosaics.http.exceptions.MalformedCookieAttributeException;
import mosaics.lang.time.DTMUtils;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.net.URLEncoder;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class HttpCookieTest {
    /*
    Set-Cookie: <cookie-name>=<cookie-value>; Expires=<date>; Secure; HttpOnly;
     *                    ; Max-Age=<non-zero-digit>; Domain=<domain-value>
     *                    ; Path=<path-value>; SameSite=Strict|Lax|none
     */

    @Nested
    public class ParseSetCookieHeaderTestCases {
        private static String RESTRICTED = "£ ( ) < > @ , ; : \\ \" / [ ] ? = { }";

        @Test
        public void parseNameValueOnly() {
            HttpCookie expected = HttpCookie.of("name", "value");

            assertEquals( expected, HttpCookie.parseSetCookieHeader("name=value") );
        }

        @Test
        public void parseQuotedValue() {
            HttpCookie expected = HttpCookie.of("name", "value");

            assertEquals( expected, HttpCookie.parseSetCookieHeader("name=\"value\"") );
        }

        @Test
        public void parseEscapedQuotesValue() {
            HttpCookie expected = HttpCookie.of("name", "value");

            assertEquals( expected, HttpCookie.parseSetCookieHeader("name=%22value%22") );
        }

        @Test
        public void parseNameValueWithRestrictedCharacters() {
            HttpCookie expected = HttpCookie.of("name"+RESTRICTED, "value"+RESTRICTED);

            assertEquals( expected, HttpCookie.parseSetCookieHeader("name"+encode(RESTRICTED)+"=value"+encode(RESTRICTED)) );
        }

        @Test
        public void parseExpires() {
            HttpCookie expected = HttpCookie.builder()
                .expiresAtMillis( DTMUtils.toEpoch(2021,11,9, 7,45) )
                .build();

            assertEquals( expected, HttpCookie.parseSetCookieHeader("Expires=Tue, 09 Nov 2021 07:45:00 GMT") );
            assertEquals( expected, HttpCookie.parseSetCookieHeader("expires=Tue, 09 Nov 2021 07:45:00 GMT") );
            assertEquals( expected, HttpCookie.parseSetCookieHeader("eXpires=Tue, 09 Nov 2021 07:45:00 GMT") );
        }

        @Test
        public void parseMalformedExpires() {
            MalformedCookieAttributeException ex = assertThrows( MalformedCookieAttributeException.class, () -> HttpCookie.parseSetCookieHeader("Expires=Tues, 09 Nov 2021 07:45:00 GMT") );
            assertEquals( "Malformed cookie attribute Expires=Tues, 09 Nov 2021 07:45:00 GMT", ex.getMessage() );
        }

        @Test
        public void parseSecure() {
            HttpCookie expected = HttpCookie.builder()
                .isSecure( true )
                .build();

            assertEquals( expected, HttpCookie.parseSetCookieHeader("Secure") );
            assertEquals( expected, HttpCookie.parseSetCookieHeader("secure") );
            assertEquals( expected, HttpCookie.parseSetCookieHeader("seCure") );
        }

        @Test
        public void parseHttpOnly() {
            HttpCookie expected = HttpCookie.builder()
                .isHttpOnly( true )
                .build();

            assertEquals( expected, HttpCookie.parseSetCookieHeader("HttpOnly") );
            assertEquals( expected, HttpCookie.parseSetCookieHeader("Httponly") );
            assertEquals( expected, HttpCookie.parseSetCookieHeader("httponly") );
        }

        @Test
        public void parseMaxAge() {
            HttpCookie.HttpCookieBuilder builder = HttpCookie.builder();

            assertEquals( builder.maxAgeSeconds(786).build(), HttpCookie.parseSetCookieHeader("Max-Age=786") );
            assertEquals( builder.maxAgeSeconds(-5).build(), HttpCookie.parseSetCookieHeader("max-age=-5") );
            assertEquals( builder.maxAgeSeconds(10).build(), HttpCookie.parseSetCookieHeader("MAX-AGE=10") );

            MalformedCookieAttributeException ex = assertThrows( MalformedCookieAttributeException.class, () -> HttpCookie.parseSetCookieHeader("MAX-AGE=abc") );
            assertEquals( "Malformed cookie attribute MAX-AGE=abc", ex.getMessage() );
        }

        @Test
        public void parseDomain() {
            HttpCookie.HttpCookieBuilder builder = HttpCookie.builder();

            assertEquals( builder.domain(FP.option("home.co.uk")).build(), HttpCookie.parseSetCookieHeader("Domain=home.co.uk") );
            assertEquals( builder.domain(FP.option("co.uk")).build(), HttpCookie.parseSetCookieHeader("domain=.co.uk") );
            assertEquals( builder.domain(FP.emptyOption()).build(), HttpCookie.parseSetCookieHeader("DOMAIN=") );
        }

        @Test
        public void parsePath() {
            HttpCookie.HttpCookieBuilder builder = HttpCookie.builder();

            assertEquals( builder.path(HttpPath.of("/a/b")).build(), HttpCookie.parseSetCookieHeader("Path=/a/b") );
            assertEquals( builder.path(HttpPath.of("/")).build(), HttpCookie.parseSetCookieHeader("path=/") );
            assertEquals( builder.build(), HttpCookie.parseSetCookieHeader("PATH=") );
        }

        @Test
        public void parseSameSite() {
            HttpCookie.HttpCookieBuilder builder = HttpCookie.builder();

            assertEquals( builder.sameSite(HttpCookie.SameSiteEnum.STRICT).build(), HttpCookie.parseSetCookieHeader("SameSite=STRICT") );
            assertEquals( builder.sameSite(HttpCookie.SameSiteEnum.STRICT).build(), HttpCookie.parseSetCookieHeader("samesite=strict") );
            assertEquals( builder.sameSite(HttpCookie.SameSiteEnum.LAX).build(),  HttpCookie.parseSetCookieHeader("samesite=LAX") );
            assertEquals( builder.sameSite(HttpCookie.SameSiteEnum.NONE).build(), HttpCookie.parseSetCookieHeader("samesite=") );
            assertEquals( builder.sameSite(HttpCookie.SameSiteEnum.NONE).build(), HttpCookie.parseSetCookieHeader("SAMESITE=NONE") );
            assertEquals( builder.sameSite(HttpCookie.SameSiteEnum.NONE).build(), HttpCookie.parseSetCookieHeader("SAMESITE=") );

            MalformedCookieAttributeException ex = assertThrows( MalformedCookieAttributeException.class, () -> HttpCookie.parseSetCookieHeader("SameSite=abc") );
            assertEquals( "Malformed cookie attribute SameSite=abc", ex.getMessage() );
        }

        @Test
        public void parseAll() {
            HttpCookie expected = HttpCookie.builder()
                .name( "show_pop" )
                .value( "true" )
                .expiresAtMillis( DTMUtils.toEpoch(2021,11,9, 7,45) )
                .sameSite( HttpCookie.SameSiteEnum.STRICT )
                .domain( FP.option("foo_test.com") )
                .maxAgeSeconds( 10 )
                .build();

            assertEquals( expected, HttpCookie.parseSetCookieHeader("show_pop=true; Expires=Tue, 09 Nov 2021 07:45:00 GMT; Max-Age=10; Domain=foo_test.com; SameSite=Strict") );
            assertEquals( expected, HttpCookie.parseSetCookieHeader("show_pop=true; Max-Age=10; Expires=Tue, 09 Nov 2021 07:45:00 GMT; Domain=foo_test.com; SameSite=Strict") );
        }
    }

    @Nested
    public class UpdateExpiresBasedOnMaxAgeTestCases {
        private static final long T0 = DTMUtils.toEpoch( 2022,1,1, 9,0 );

        @Test
        public void givenMaxAgeAndExpiresSetOnCookie_callUpdateExpiresBasedOnMaxAge_expectNoChange() {
            HttpCookie expected = HttpCookie.builder()
                .expiresAtMillis( DTMUtils.toEpoch(2021,11,9, 7,45) )
                .maxAgeSeconds( 10 )
                .build();

            assertSame(expected, expected.updateExpiresBasedOnMaxAge(T0) );
        }

        @Test
        public void givenNoMaxAgeSet_callUpdateExpiresBasedOnMaxAge_expectNoChange() {
            HttpCookie expected = HttpCookie.builder()
                .expiresAtMillis( DTMUtils.toEpoch(2021,11,9, 7,45) )
                .build();

            assertSame(expected, expected.updateExpiresBasedOnMaxAge(T0) );
        }

        @Test
        public void givenMaxAgeOnly_callUpdateExpiresBasedOnMaxAge_expectExpiresToGetSet() {
            HttpCookie cookie = HttpCookie.builder()
                .maxAgeSeconds( 10 )
                .build();

            HttpCookie expected = HttpCookie.builder()
                .expiresAtMillis( T0 + 10*1000 )
                .build();

            assertEquals(expected, cookie.updateExpiresBasedOnMaxAge(T0) );
        }
    }

    @Nested
    public class IsAliveAtTestCases {
        private static final long T0 = DTMUtils.toEpoch(2020,1,1, 10,0);

        @Test
        public void givenNoExpiresAndNoMaxAge_expectAlwaysAlive() {
            HttpCookie cookie = HttpCookie.builder().build();

            assertTrue(cookie.isAliveAt(0));
            assertTrue(cookie.isAliveAt(T0-1));
            assertTrue(cookie.isAliveAt(T0));
            assertTrue(cookie.isAliveAt(T0+1));
            assertTrue(cookie.isAliveAt(Long.MAX_VALUE));
        }

        @Test
        public void givenExpires_expectOnlyAliveUptoTheExpires() {
            HttpCookie cookie = HttpCookie.builder()
                .expiresAtMillis( T0 )
                .build();

            assertTrue(cookie.isAliveAt(0));
            assertTrue(cookie.isAliveAt(T0-1));
            assertFalse(cookie.isAliveAt(T0));
            assertFalse(cookie.isAliveAt(T0+1));
            assertFalse(cookie.isAliveAt(Long.MAX_VALUE));
        }

        @Test
        public void givenMaxAge_expectAsTheMaxAgeHasNotBeenConvertedIntoAnExpiresYet() {
            HttpCookie cookie = HttpCookie.builder()
                .name( "c1" )
                .maxAgeSeconds( 10 )
                .build();

            IllegalStateException ex = assertThrows(IllegalStateException.class, () -> cookie.isAliveAt(0));
            assertEquals( "Cookie 'c1' cannot check whether it is alive or not because its Max-Age has not been converted to an Expires.  Invoke cookie.updateExpiresBasedOnMaxAge() first.", ex.getMessage() );
        }
    }

    @Nested
    public class MatchesHttpRequestTestCases {
        @Test
        public void givenSecureOnlyCookie_callMatchesWithHttpProtocol_expectFalse() {
            HttpCookie cookie = HttpCookie.builder()
                .isSecure( true )
                .build();

            HttpRequest httpRequest = HttpRequest.GET( "http://home.co.uk/a/b" );

            assertFalse( cookie.accepts(httpRequest) );
        }

        @Test
        public void givenSecureOnlyCookie_callMatchesWithHttpsProtocol_expectTrue() {
            HttpCookie cookie = HttpCookie.builder()
                .isSecure( true )
                .build();

            HttpRequest httpRequest = HttpRequest.GET( "https://home.co.uk/a/b" );

            assertTrue( cookie.accepts(httpRequest) );
        }

        @Test
        public void givenNotSecureOnlyCookie_callMatchesWithHttpProtocol_expectTrue() {
            HttpCookie cookie = HttpCookie.builder()
                .isSecure( false )
                .build();

            HttpRequest httpRequest = HttpRequest.GET( "http://home.co.uk/a/b" );

            assertTrue( cookie.accepts(httpRequest) );
        }

        @Test
        public void givenDomainCookie_callMatchesWithMismatchingDomain_expectFalse() {
            HttpCookie cookie = HttpCookie.builder()
                .domain( FP.option( "home.co.uk" ) )
                .build();

            HttpRequest httpRequest = HttpRequest.GET( "http://home.com/a/b" );

            assertFalse( cookie.accepts(httpRequest) );
        }

        @Test
        public void givenDomainCookie_callMatchesWithMatchingDomain_expectTrue() {
            HttpCookie cookie = HttpCookie.builder()
                .domain( FP.option( "home.co.uk" ) )
                .build();

            HttpRequest httpRequest = HttpRequest.GET( "http://home.co.uk/a/b" );

            assertTrue( cookie.accepts(httpRequest) );
        }

        @Test
        public void givenPathCookie_callMatchesWithMismatchingDomain_expectFalse() {
            HttpCookie cookie = HttpCookie.builder()
                .path( HttpPath.of("/b") )
                .build();

            HttpRequest httpRequest = HttpRequest.GET( "http://home.co.uk/a/b" );

            assertFalse( cookie.accepts(httpRequest) );
        }

        @Test
        public void givenPathCookie_callMatchesWithMatchingPath_expectTrue() {
            HttpCookie cookie = HttpCookie.builder()
                .path( HttpPath.of("/a/b") )
                .build();

            HttpRequest httpRequest = HttpRequest.GET( "http://home.co.uk/a/b" );

            assertTrue( cookie.accepts(httpRequest) );
        }

        @Test
        public void givenPathCookie_callMatchesWithMatchingPrefixPath_expectTrue() {
            HttpCookie cookie = HttpCookie.builder()
                .path( HttpPath.of("/a") )
                .build();

            HttpRequest httpRequest = HttpRequest.GET( "http://home.co.uk/a/b" );

            assertTrue( cookie.accepts(httpRequest) );
            assertTrue( cookie.withPath("/").accepts(httpRequest) );
            assertTrue( cookie.withPath("/a").accepts(httpRequest) );
            assertTrue( cookie.withPath("/a/b").accepts(httpRequest) );
            assertFalse( cookie.withPath("/a/b/c").accepts(httpRequest) );
        }
    }

    @SneakyThrows
    private static final String encode(String str) {
        return URLEncoder.encode( str, "UTF-8" );
    }
}
