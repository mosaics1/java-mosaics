package mosaics.http;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class HttpURLTest {
    @Test
    public void givenProtocolAndHost_callToString() {
        HttpURL url = new HttpURL("http", "a.com");

        assertEquals("http://a.com", url.toString());
    }

    @Test
    public void givenProtocolAndHost_callWithPort() {
        HttpURL url = new HttpURL("http", "a.com").withPort( 8888 );

        assertEquals("http://a.com:8888", url.toString());
    }

    @Test
    public void givenProtocolAndHost_callWithProtocol() {
        HttpURL url = new HttpURL("http", "a.com").withProtocol( "https" );

        assertEquals("https://a.com", url.toString());
    }

    @Test
    public void givenProtocolAndHost_callWithHost() {
        HttpURL url = new HttpURL("http", "a.com").withHost( "b.com" );

        assertEquals("http://b.com", url.toString());
    }

    @Test
    public void givenProtocolAndHost_callWithPath() {
        HttpURL url = new HttpURL("http", "a.com").withPath( HttpPath.of("a","b") );

        assertEquals("http://a.com/a/b", url.toString());
    }

    @Test
    public void givenProtocolAndHost_callWithPathShortCut() {
        HttpURL url = new HttpURL("http", "a.com").withPath( "/a/b" );

        assertEquals("http://a.com/a/b", url.toString());
    }

    @Test
    public void givenProtocolAndHost_callWithFragment() {
        HttpURL url = new HttpURL("http", "a.com")
            .withPath( "/a/b" )
            .withFragment( "abc" );

        assertEquals("http://a.com/a/b#abc", url.toString());
    }

    @Nested
    public class URIStringsToHttpURLTestCases {
        @Test
        public void nullString_expectIllegalArgumentException() {
            IllegalArgumentException ex = assertThrows( IllegalArgumentException.class, () -> HttpURL.of(null) );

            assertEquals( "'url' must not be blank (was null)", ex.getMessage() );
        }

        @Test
        public void emptyString_expectIllegalArgumentException() {
            IllegalArgumentException ex = assertThrows( IllegalArgumentException.class, () -> HttpURL.of("") );

            assertEquals( "'url' must not be blank (was '')", ex.getMessage() );
        }

        @Test
        public void blankString_expectIllegalArgumentException() {
            IllegalArgumentException ex = assertThrows( IllegalArgumentException.class, () -> HttpURL.of("  ") );

            assertEquals( "'url' must not be blank (was '  ')", ex.getMessage() );
        }

        @Test
        public void protocolOnly_expectIllegalArgumentException() {
            IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> HttpURL.of("http"));

            assertEquals( "Host must be specified", ex.getMessage() );
        }

        @Test
        public void protocolWithSeparatorOnly_expectIllegalArgumentException() {
            IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> HttpURL.of("http://"));

            assertEquals( "Expected authority at index 7: http://", ex.getMessage() );
        }

        @Test
        public void protocolWithMalformedSeparator_expectIllegalArgumentException() {
            IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> HttpURL.of("http:/host"));

            assertEquals( "Host must be specified", ex.getMessage() );
        }

        @Test
        public void protocolAndHost() {
            HttpURL actual   = HttpURL.of( "http://host" );
            HttpURL expected = HttpURL.of( "http", "host" );

            assertEquals( expected, actual );
        }

        @Test
        public void protocolAndPort() {
            IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> HttpURL.of("http://:80"));

            assertEquals( "Host must be specified", ex.getMessage() );
        }

        @Test
        public void protocolWithSeparatorAndPort() {
            IllegalArgumentException ex = assertThrows(IllegalArgumentException.class, () -> HttpURL.of("http:80"));

            assertEquals( "Host must be specified", ex.getMessage() );
        }

        @Test
        public void protocolHostAndPort() {
            HttpURL actual   = HttpURL.of( "http://host:80" );
            HttpURL expected = HttpURL.of( "http", "host", 80 );

            assertEquals( expected, actual );
        }

        @Test
        public void protocolHostPath() {
            HttpURL actual   = HttpURL.of( "http://host/a/b/c" );
            HttpURL expected = HttpURL.of( "http", "host", "/a/b/c" );

            assertEquals( expected, actual );
        }

        @Test
        public void protocolHostPortPath() {
            HttpURL actual   = HttpURL.of( "http://host:88/a/b/c" );
            HttpURL expected = HttpURL.of( "http", "host", 88, "/a/b/c" );

            assertEquals( expected, actual );
        }

        @Test
        public void protocolHostPortPathEmptyQueryParams() {
            HttpURL actual   = HttpURL.of( "http://host:88/a/b/c?" );
            HttpURL expected = HttpURL.of( "http", "host", 88, "/a/b/c" );

            assertEquals( expected, actual );
        }

        @Test
        public void protocolHostPortPath1EncodedQueryParams() {
            HttpURL actual   = HttpURL.of( "http://host:88/a/b/c?a=%3C%26%3E" );
            HttpURL expected = HttpURL.of( "http", "host", 88, "/a/b/c" )
                .appendQueryParameter( "a", "<&>" );

            assertEquals( expected, actual );
        }

        @Test
        public void protocolHostPortPath1QueryParams() {
            HttpURL actual   = HttpURL.of( "http://host:88/a/b/c?a=1" );
            HttpURL expected = HttpURL.of( "http", "host", 88, "/a/b/c" )
                .appendQueryParameter( "a", "1" );

            assertEquals( expected, actual );
        }

        @Test
        public void protocolHostPortPath2QueryParams() {
            HttpURL actual   = HttpURL.of( "http://host:88/a/b/c?a=1&b=2" );
            HttpURL expected = HttpURL.of( "http", "host", 88, "/a/b/c" )
                .appendQueryParameter( "a", "1" )
                .appendQueryParameter( "b", "2" );

            assertEquals( expected, actual );
        }

        @Test
        public void protocolHostPortPathOverridingQueryParams() {
            HttpURL actual   = HttpURL.of( "http://host:88/a/b/c?a=1&a=2" );
            HttpURL expected = HttpURL.of( "http", "host", 88, "/a/b/c" )
                .appendQueryParameter( "a", "2" );

            assertEquals( expected, actual );
        }

        @Test
        public void protocolHostPortPathFragment() {
            HttpURL actual   = HttpURL.of( "http://host:88/a/b/c#f" );
            HttpURL expected = HttpURL.of( "http", "host", 88, "/a/b/c" )
                .withFragment( "f" );

            assertEquals( expected, actual );
        }

        @Test
        public void protocolHostPathFragment() {
            HttpURL actual   = HttpURL.of( "http://host/a/b/c#f" );
            HttpURL expected = HttpURL.of( "http", "host", "/a/b/c" )
                .withFragment( "f" );

            assertEquals( expected, actual );
        }

        @Test
        public void protocolHostFragment() {
            HttpURL actual   = HttpURL.of( "http://host#f-1" );
            HttpURL expected = HttpURL.of( "http", "host" )
                .withFragment( "f-1" );

            assertEquals( expected, actual );
        }

        @Test
        public void protocolHostFragmentQuery() {
            HttpURL actual   = HttpURL.of( "http://host#f-1?a=1" );
            HttpURL expected = HttpURL.of( "http", "host" )
                .withFragment( "f-1?a=1" );  // remember, fragments go last

            assertEquals( expected, actual );
        }

        @Test
        public void protocolHostQueryFragment() {
            HttpURL actual   = HttpURL.of( "http://host?a=1#f-1" );
            HttpURL expected = HttpURL.of( "http", "host" )
                .withFragment( "f-1" )
                .appendQueryParameter( "a", "1" );

            assertEquals( expected, actual );
        }
    }
}
