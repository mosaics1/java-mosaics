package mosaics.http;

import mosaics.fp.FP;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class HttpQueryParametersTest {
    @Nested
    public class SizeTestCases {
        @Test
        public void givenEmpty_callSize_expect0() {
            HttpQueryParameters qp = HttpQueryParameters.empty();

            assertEquals(0, qp.size());
        }

        @Test
        public void givenOne_callSize_expect1() {
            HttpQueryParameters qp = HttpQueryParameters.of("a","1");

            assertEquals(1, qp.size());
        }

        @Test
        public void givenTwo_callSize_expect2() {
            HttpQueryParameters qp = HttpQueryParameters.of("a","1", "b","2");

            assertEquals(2, qp.size());
        }
    }
    

    @Nested
    public class GetParameterTestCases {
        @Test
        public void givenEmpty_callGetParameter_expectNone() {
            HttpQueryParameters qp = HttpQueryParameters.empty();

            assertEquals( FP.emptyOption(), qp.getParameter("a") );
        }

        @Test
        public void givenOne_callGetParameterWithMissingKey_expectNone() {
            HttpQueryParameters qp = HttpQueryParameters.of("a","1");

            assertEquals( FP.emptyOption(), qp.getParameter("b") );
        }

        @Test
        public void givenOne_callGetParameterWithExistingKey_expectValue() {
            HttpQueryParameters qp = HttpQueryParameters.of("a","1");

            assertEquals( FP.option("1"), qp.getParameter("a") );
        }
    }


    @Nested
    public class GetParameterMandatoryTestCases {
        @Test
        public void givenEmpty_callGetParameterMandatory_expectException() {
            HttpQueryParameters qp = HttpQueryParameters.empty();

            assertThrows(IllegalStateException.class, () -> qp.getParameterMandatory("a"));
        }

        @Test
        public void givenOne_callGetParameterMandatoryWithExistingKey_expectValue() {
            HttpQueryParameters qp = HttpQueryParameters.of("a","1");

            assertThrows(IllegalStateException.class, () -> qp.getParameterMandatory("b"));
        }

        @Test
        public void givenOne_callGetParameterMandatoryWithMissingKey_expectException() {
            HttpQueryParameters qp = HttpQueryParameters.of("a","1");

            assertEquals( "1", qp.getParameterMandatory("a") );
        }
    }


    @Nested
    public class WithParameterTestCases {
        @Test
        public void givenEmpty_callWithParameter_expectKVToBeAdded() {
            HttpQueryParameters qp = HttpQueryParameters.empty();
            HttpQueryParameters expected = HttpQueryParameters.of( "a", "1" );

            assertEquals( expected, qp.withParameter("a","1") );
        }

        @Test
        public void givenOne_callWithParameterNewKey_expectKVToBeAdded() {
            HttpQueryParameters qp = HttpQueryParameters.of("a","1");
            HttpQueryParameters expected = HttpQueryParameters.of( "a", "1", "b","2" );

            assertEquals( expected, qp.withParameter("b","2") );
        }

        @Test
        public void givenOne_callWithParameterWithExistingKVPair_expectNoChange() {
            HttpQueryParameters qp = HttpQueryParameters.of("a","1");

            assertSame( qp, qp.withParameter("a","1") );
        }

        @Test
        public void givenOne_callWithParameterExistingKey_expectExistingKVToBeUpdated() {
            HttpQueryParameters qp = HttpQueryParameters.of("a","1");
            HttpQueryParameters expected = HttpQueryParameters.of( "a", "2" );

            assertEquals( expected, qp.withParameter("a","2") );
        }
    }

    @Nested
    public class ParseEncodedQPStringTestCases {
        @Test
        public void givenParameterWithNoValue() {
            HttpQueryParameters qp = HttpQueryParameters.of("a=");
            HttpQueryParameters expected = HttpQueryParameters.of( "a", "" );

            assertEquals( expected, qp );
        }

        @Test
        public void givenParameterWithNoEquals_expectValueToDefaultToTrue() {
            HttpQueryParameters qp       = HttpQueryParameters.of("a");
            HttpQueryParameters expected = HttpQueryParameters.of( "a", "true" );

            assertEquals( expected, qp );
        }
    }

    @Nested
    public class RemoveParameterTestCases {
        @Test
        public void givenEmpty_callRemove_expectNoChange() {
            HttpQueryParameters qp = HttpQueryParameters.empty();

            assertEquals( qp, qp.removeParameter("a") );
        }

        @Test
        public void givenOne_removeNonExistingParameter_expectNoChange() {
            HttpQueryParameters qp = HttpQueryParameters.of("a","1");

            assertEquals( qp, qp.removeParameter("b") );
        }

        @Test
        public void givenOne_removeExistingParameter_expectEmpty() {
            HttpQueryParameters qp = HttpQueryParameters.of("a","1");

            assertEquals( HttpQueryParameters.empty(), qp.removeParameter("a") );
        }

        @Test
        public void givenTwo_removeExisting_expectSingleValueLeft() {
            HttpQueryParameters qp       = HttpQueryParameters.of("a","1","b","2");
            HttpQueryParameters expected = HttpQueryParameters.of( "b", "2" );

            assertEquals( expected, qp.removeParameter("a") );
        }
    }


    @Nested
    public class HasParameterTestCases {
        @Test
        public void givenEmpty_hasParameter_expectFalse() {
            HttpQueryParameters qp = HttpQueryParameters.empty();

            assertFalse( qp.hasParameter("a") );
        }

        @Test
        public void givenOne_hasParameterWithExistingKey_expectTrue() {
            HttpQueryParameters qp = HttpQueryParameters.of("a","1");

            assertTrue( qp.hasParameter("a") );
        }

        @Test
        public void givenOne_hasParameterWithNonExistingKey_expectFalse() {
            HttpQueryParameters qp = HttpQueryParameters.of("a","1");

            assertFalse( qp.hasParameter("b") );
        }
    }

    @Test
    public void testToString() {
        assertEquals("", HttpQueryParameters.empty().toString());
        assertEquals("a=1", HttpQueryParameters.of("a","1").toString());
        assertEquals("a=%3C%26%3E", HttpQueryParameters.of("a","<&>").toString());
        assertEquals("a=1&b=2", HttpQueryParameters.of("a","1","b","2").toString());
        assertEquals("a=%3C%26%3E&b=2", HttpQueryParameters.of("a","<&>","b","2").toString());
    }
}
