package mosaics.os;

import io.reactivex.Flowable;
import org.junit.jupiter.api.Test;

import java.io.StringReader;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class JdkReaderToRxFlowableAdapterTest {

    @Test
    public void givenEmptyReader_expectEmptyFlow() {
        Flowable<String> flowable = new JdkReaderToRxFlowableAdapter( new StringReader("") );
        List<String>     actual   = flowable.toList().blockingGet();

        List<String>     expected = Collections.emptyList();

        assertEquals( expected, actual );
    }

    @Test
    public void givenOneLineWithNoNewLineCharacter_expectLineToBeReadBack() {
        Flowable<String> flowable = new JdkReaderToRxFlowableAdapter( new StringReader("abc 123") );
        List<String>     actual   = flowable.toList().blockingGet();

        List<String>     expected = Collections.singletonList("abc 123");

        assertEquals( expected, actual );
    }

    @Test
    public void givenThreeLines_expectThreeLinesOut() {
        Flowable<String> flowable = new JdkReaderToRxFlowableAdapter( new StringReader("line1\nline2\nline3") );
        List<String>     actual   = flowable.toList().blockingGet();

        List<String>     expected = Arrays.asList( "line1", "line2", "line3" );

        assertEquals( expected, actual );
    }

}
