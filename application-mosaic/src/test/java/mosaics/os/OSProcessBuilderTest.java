package mosaics.os;

import mosaics.cli.Env;
import mosaics.cli.requests.Request;
import mosaics.concurrency.Await;
import mosaics.fp.ExceptionFailure;
import mosaics.junit.JMThreadExtension;
import mosaics.lang.Backdoor;
import mosaics.lang.threads.ThreadUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.parallel.Isolated;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.Duration;
import java.util.List;
import java.util.Objects;

import static mosaics.lang.Backdoor.cast;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Isolated
@JMThreadExtension.IgnoreThreads({
    @JMThreadExtension.IgnoreThread("Attach Listener"), // JDK hotspot thread - @see http://openjdk.java.net/groups/hotspot/docs/Serviceability.html
    @JMThreadExtension.IgnoreThread("process reaper")  // JDK daemon threads - @see ProcessHandleImpl.processReaperExecutor
})
@ExtendWith(JMThreadExtension.class)
public class OSProcessBuilderTest {
    private Env     env   = Env.createLiveEnv("junit");
    private Await   await = new Await(env);
    private Request req   = env.createRequest( "junit" );


    @Test
    public void runCommandThatDoesNotExist_expectFailure() {
        OSProcess process = new OSProcessBuilder()
            .withCommand( "ffff" )
            .run(req);

        ExceptionFailure f = cast( await.failure(req,process.toPromise()) );

        assertTrue( f.getMessage().contains("No such file or directory") );
    }

    @Test
    public void runCommand_thatExitsWithZero() {
        System.getProperties().keySet().forEach( k -> System.out.println(k+"->"+System.getProperty(k.toString())) );
        OSProcess process = new OSProcessBuilder()
            .withCommand( System.getenv("JAVA_HOME")+"/bin/java", "-version" )
            .run(req);

        int osExitCode = await.result( req, process.toPromise() );

        assertEquals( 0, osExitCode );
    }

    @Test
    public void runCommand_thatExitsWithOne() {
        OSProcess process = new OSProcessBuilder()
            .withCommand( System.getenv("JAVA_HOME")+"/bin/java" )
            .run(req);


        int osExitCode = await.result( req, process.toPromise() );

        assertEquals( 1, osExitCode );
    }

    @Test
    public void runCommand_that() {
        long       startMillis = System.currentTimeMillis();
        OSProcess process = new OSProcessBuilder()
            .withCommand( SleepMain.class )
            .run(req);

        int        mainMethodDuration = await.result(req,process.toPromise());       // SleepMain uses the return status code as the duration in millis
        long       durationMillis     = System.currentTimeMillis() - startMillis;

        assertTrue( mainMethodDuration >= SleepMain.SLEEP_MILLIS ); // main method must always take as long as its sleep
        assertTrue( durationMillis >= mainMethodDuration );         // the process builder must take at least as long as the main method that gets called
    }

    @Test
    public void captureSystemOutWhileTheCommandIsRunning() {
        OSProcess process = new OSProcessBuilder()
            .withCommand( NoisyMain.class )
            .run(req);


        int osExitCode = await.result( req, process.toPromise() );

        List<String> capturedOutput = process.getStdout().toList().blockingGet();

        String[] expectedText = {"Hello World", "I am noisy Main"};
        assertEquals(0, osExitCode);
        assertArrayEquals( expectedText, capturedOutput.toArray() );
    }

    @Test
    public void captureSystemStdErrWhileTheCommandIsRunning() {
        OSProcess process = new OSProcessBuilder()
            .withCommand( ErrorMain.class )
            .run(req);


        int          osExitCode     = await.result( req, process.toPromise() );
        List<String> capturedOutput = process.getStderr().toList().blockingGet();


        String[] expectedText = {"Hello Error"};
        assertEquals( 0, osExitCode );
        assertArrayEquals( expectedText, capturedOutput.toArray() );
    }

    @Test
    public void abortARunningChildProcessViaPromise() {
        OSProcess process = new OSProcessBuilder()
            .withCommand( NeverCompletesMain.class )
            .run(req);

        assertTrue( process.isRunning() );

        process.toPromise().setFailure(new ExceptionFailure("abort"));

        List<String> output = ThreadUtils.fetchBlockingActionFromAnotherThread( () -> process.getStdout().toList().blockingGet() );

        assertFalse( process.isRunning() );
    }

    @Test
    public void writeTextToTheChildProcessesStdIn_expectTheChildProcessToBeAbleToReadIt() {
        OSProcess process = new OSProcessBuilder()
            .withCommand( EchoMain.class )
            .run(req);

        process.getStdin().println( "hello world" );
        process.getStdin().flush();

        int osExitCode = await.result( req, process.toPromise() );

        // NB EchoMain exits with status zero iff the string 'hello world' is read in
        assertEquals( 0, osExitCode );
    }

    @Test
    public void checkDefaultDirectoryIsSameAsCurrentProcessesWorkingDirectory() {
        OSProcess process = new OSProcessBuilder()
            .withCommand( "pwd" )
            .run(req);


        int osExitCode = await.result( req, process.toPromise() );

        assertEquals( 0, osExitCode );

        List<String> capturedOutput = process.getStdout().toList().blockingGet();
        String[]     expectedText   = {System.getProperty("user.dir")};

        assertArrayEquals( expectedText, capturedOutput.toArray() );
    }

    @Test
    public void checkSpecifingWorkingDirectorChangesTheWorkingDirectoryOfTheChildProcess() {
        OSProcess process = new OSProcessBuilder()
            .withCommand( "pwd" )
            .withWorkingDirectory( "/" )
            .run(req);


        int osExitCode = await.result( req, process.toPromise() );

        assertEquals( 0, osExitCode );

        List<String> capturedOutput = process.getStdout().toList().blockingGet();
        String[]     expectedText   = {"/"};

        assertArrayEquals( expectedText, capturedOutput.toArray() );
    }



    public static class SleepMain {
        public static final long SLEEP_MILLIS = 30;

        public static void main( String[] args ) {
            long startMillis = System.currentTimeMillis();

            Backdoor.sleep(SLEEP_MILLIS);

            long durationMillis = System.currentTimeMillis() - startMillis;

            System.exit( (int) durationMillis );
        }
    }

    public static class NoisyMain {
        public static void main( String[] args ) {
            System.out.println( "Hello World" );
            System.out.println( "I am noisy Main" );
        }
    }

    public static class ErrorMain {
        public static void main( String[] args ) {
            System.err.println( "Hello Error" );
        }
    }


    public static class NeverCompletesMain {
        public static void main( String[] args ) {
            Backdoor.sleep( Duration.ofDays(1).toMillis() );
        }
    }

    public static class EchoMain {
        public static void main( String[] args ) throws IOException {
            System.out.println( "STARTED" );

            BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

            String input = in.readLine();
            System.out.println( "READ: '"+input+"'" );

            if ( Objects.equals(input, "hello world") ) {
                System.out.println(input);
                System.exit( 0 );
            } else {
                System.exit( 1 );
            }
        }
    }

}
