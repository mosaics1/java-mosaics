package mosaics.concurrency;

import mosaics.concurrency.futures.Promise;
import mosaics.concurrency.futures.UnexpectedResultException;
import mosaics.cli.Env;
import mosaics.cli.requests.Request;
import mosaics.fp.Failure;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.parallel.Isolated;

import java.util.concurrent.TimeoutException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;


@Isolated
public class AwaitTest {

    private final Env     env     = Env.createTestEnvWithRealClock();
    private final Request request = env.createRequest( "junit" );


    @Test
    public void spinUntilValueAvailable() {
        Await await = new Await( 5, 1,new SystemScheduler(env,"junit") );

        Promise<String> promise = new Promise<>(request);
        promise.setResult( "v" );

        assertEquals( "v", await.result(request,promise) );
    }

    @Test
    public void waitForResultButGetAFailure_expectError() {
        Await await = new Await( 5, 1,new SystemScheduler(env,"junit") );

        Promise<String> promise = new Promise<>(request);

        Failure failure = new MockFailure("msg");
        promise.setFailure( failure );

        try {
            await.result(request,promise);
            fail("expected IllegalStateException");
        } catch ( IllegalStateException ex ) {
            assertEquals( "msg", ex.getMessage() );
        }
    }

    @Test
    public void spinUntilTimeoutWaitingForAResult() {
        Await await = new Await( 5, 1,new SystemScheduler(env, "junit") );

        Promise<String> promise = new Promise<>(request);

        long startMillis = System.currentTimeMillis();

        try {
            await.result(request,promise);
            fail( "expected timeout" );
        } catch ( Exception ex ) {
            long timedoutAtMillis = System.currentTimeMillis();

            assertTrue( timedoutAtMillis >= startMillis+5 );

            assertEquals( TimeoutException.class, ex.getClass() );
            assertEquals( "Timedout waiting for WaitingPromise()", ex.getMessage() );
        }
    }

    @Test
    public void spinUntilFailureAvailable() {
        Await await = new Await( 5, 1,new SystemScheduler(env, "junit") );

        Promise<String> promise = new Promise<>(request);

        Failure failure = new MockFailure("msg");
        promise.setFailure( failure );

        assertEquals( failure, await.failure(request,promise) );
    }

    @Test
    public void waitForFailureButGetAResult_expectError() {
        Await await = new Await( 5, 1,new SystemScheduler(env, "junit") );

        Promise<String> promise = new Promise<>(request);
        promise.setResult( "hello" );

        try {
            await.failure(request,promise);
            fail("expected UnexpectedResultException");
        } catch ( UnexpectedResultException ex ) {
            assertEquals( "Unexpected result: hello", ex.getMessage() );
        }
    }

    @Test
    public void spinUntilTimeoutWaitingForAFailure() {
        Await await = new Await( 5, 1, new SystemScheduler( env, "junit" ) );

        Promise<String> promise = new Promise<>( request );

        long startMillis = System.currentTimeMillis();

        try {
            await.failure( request, promise );
            fail( "expected timeout" );
        } catch ( Exception ex ) {
            long timedoutAtMillis = System.currentTimeMillis();

            assertTrue( timedoutAtMillis >= startMillis + 5 );

            assertEquals( TimeoutException.class, ex.getClass() );
            assertEquals( "Timedout waiting for WaitingPromise()", ex.getMessage() );
        }
    }

    @Test
    public void testToString() {
        Await await = new Await(env);

        assertEquals( "Await(maxWaitMillis=5000, pollIntervalMillis=1)", await.toString() );
    }

    @Test
    public void equalsContract() {
        EqualsVerifier.forClass( Await.class )
            .suppress( Warning.STRICT_INHERITANCE )
            .verify();
    }
}
