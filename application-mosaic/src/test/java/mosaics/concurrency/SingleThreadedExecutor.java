package mosaics.concurrency;

import mosaics.lang.lifecycle.StartStoppable;

import java.util.Deque;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicReference;


/**
 * Single threaded executor used during unit tests.  Gives access to its underlying thread for
 * assertion purposes.
 */
public class SingleThreadedExecutor extends StartStoppable<SingleThreadedExecutor> implements Executor {

    private AtomicReference<Thread> threadRef = new AtomicReference<>();
    private Deque<Runnable>         workQueue = new ConcurrentLinkedDeque<>();


    public SingleThreadedExecutor( String serviceName ) {
        super( serviceName );
    }


    public Thread getWorkerThread() {
        return threadRef.get();
    }

    public void execute( Runnable command ) {
        workQueue.add( command );
    }


    protected void doStart() throws Exception {
        super.doStart();

        Thread thread = new Thread(() -> {
            while ( SingleThreadedExecutor.this.isRunning() ) {
                Runnable nextJob = workQueue.poll();

                if ( nextJob == null ) {
                    Thread.yield();
                } else {
                    try {
                        nextJob.run();
                    } catch ( Throwable ex ) {
                        Thread currentThread = Thread.currentThread();

                        currentThread.getUncaughtExceptionHandler().uncaughtException( currentThread, ex );
                    }
                }
            }
        });

        this.threadRef.set(thread);

        thread.start();
    }

    protected void doStop() throws Exception {
        super.doStop();

        this.threadRef.set(null);
    }


}
