package mosaics.concurrency;

import mosaics.cli.requests.Request;

import java.util.concurrent.Executor;


public class CompleteRequestExecutor implements Executor {
    private Request request;

    public CompleteRequestExecutor( Request request ) {
        this.request = request;
    }

    public void execute( Runnable command ) {
        request.markCompleted();

        command.run();
    }
}
