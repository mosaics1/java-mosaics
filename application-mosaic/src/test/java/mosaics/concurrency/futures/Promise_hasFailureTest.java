package mosaics.concurrency.futures;

import mosaics.concurrency.CompleteRequestExecutor;
import mosaics.concurrency.MockFailure;
import org.junit.jupiter.api.Test;

import static mosaics.lang.Backdoor.cast;
import static org.junit.jupiter.api.Assertions.assertThrows;


public class Promise_hasFailureTest extends BaseHasFailureFutureTestCases {

    public Promise_hasFailureTest() {
        super( Promise.class );

        Promise<String> promise = new Promise<>( request );
        promise.setFailure( initialFailure );

        this.future = promise;
    }


    @Test
    public void setSuccessOnAFailedPromise_expectUnsupportedOperationException() {
        Promise<String> promise = cast(future);

        assertThrows( UnsupportedOperationException.class, () -> promise.setResult( "abc" ) );
    }

    @Test
    public void setFailureOnAFailedPromise_expectUnsupportedOperationException() {
        Promise<String> promise = cast(future);

        assertThrows( UnsupportedOperationException.class, () -> promise.setFailure( new MockFailure("abc") ) );
    }

    @Test
    public void timeoutAfterMappingAFailedPromise() {
        Promise<String> p = new Promise<>( request );

        p.setFailure( new MockFailure("abc") );

        Future<String> f = p.recover( new CompleteRequestExecutor(request), ( r,failure) -> "a" );

        TimeoutFailure expectation = new TimeoutFailure( request, p, new MockFailure("abc") );
        assertFailure( expectation, f );
    }

}
