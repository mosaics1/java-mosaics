package mosaics.concurrency.futures;

import mosaics.cli.Env;
import mosaics.cli.requests.Request;
import mosaics.concurrency.Await;
import mosaics.concurrency.SystemScheduler;
import mosaics.fp.Failure;
import mosaics.fp.Try;
import mosaics.fp.collections.FPIterable;
import mosaics.junit.JMThreadExtension;
import mosaics.lang.functions.Function1;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.parallel.Isolated;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static mosaics.lang.Backdoor.cast;
import static org.junit.jupiter.api.Assertions.assertEquals;


@Isolated
@JMThreadExtension.IgnoreThread("Attach Listener")
@ExtendWith(JMThreadExtension.class)
public class Promise_StochasticTest {

    private int     numPools          = 4;
    private int     numThreadsPerPool = 5;

    private int     numRoots          = 10;
    private int     breadth           = 3;
    private int     depth             = 5;

    private Env     env               = Env.createTestEnvWithRealClock();
    private Request req               = env.createRequest( "junit" );
    private Random  rnd               = new Random();

    private FPIterable<ExecutorService> pools     = FPIterable.generate(numPools, () -> Executors.newFixedThreadPool(numThreadsPerPool) );
    private List<Executor>              executors = pools.map(p -> (Executor) p).and( UseCallingThreadExecutor.INSTANCE ).toList();



    @AfterEach
    public void tearDown() {
        pools.forEach( ExecutorService::shutdown );
    }


    @Test
    public void strainTest_generateResultFutureGraph_BEFORE_completingThePromises() {
        List<Promise<Integer>> roots = FPIterable.generate( numRoots, () -> new Promise<Integer>( req ) ).toList();
        List<Future<Integer>>  leafs = generateLeafs(cast(roots), nextStepResultGenerators);

        roots.forEach( p -> p.setResult(0) );

        assertLeafNodeResults( leafs );
    }

    @Test
    public void strainTest_generateResultFutureGraph_AFTER_completingThePromises() {
        List<Promise<Integer>> roots = FPIterable.generate( numRoots, () -> new Promise<Integer>( req ) ).toList();

        roots.forEach( p -> p.setResult(0) );

        List<Future<Integer>>  leafs = generateLeafs(cast(roots), nextStepResultGenerators);

        assertLeafNodeResults( leafs );
    }

    @Test
    public void strainTest_generateFailureFutureGraph_BEFORE_completingThePromises() {
        List<Promise<Integer>> roots = FPIterable.generate( numRoots, () -> new Promise<Integer>( req ) ).toList();
        List<Future<Integer>>  leafs = generateLeafs(cast(roots), nextStepFailureGenerators);

        roots.forEach( p -> p.setFailure(new StressFailure(0)) );


        assertLeafNodeFailures( leafs );
    }

    @Test
    public void strainTest_generateFailureFutureGraph_AFTER_completingThePromises() {
        List<Promise<Integer>> roots = FPIterable.generate( numRoots, () -> new Promise<Integer>( req ) ).toList();

        roots.forEach( p -> p.setFailure(new StressFailure(0)) );

        List<Future<Integer>>  leafs = generateLeafs(cast(roots), nextStepFailureGenerators);

        assertLeafNodeFailures( leafs );
    }


    private void assertLeafNodeFailures( List<Future<Integer>> leafs ) {
        Await await = new Await(30_000,1, new SystemScheduler(env, "junit") );
        for ( Future<Integer> leaf : leafs ) {
            Failure result = await.failure( req, leaf );

            assertEquals( depth, ((StressFailure) result).intValue() );
        }
    }

    private void assertLeafNodeResults( List<Future<Integer>> leafs ) {
        Await await = new Await(30_000,1, new SystemScheduler(env, "junit") );
        for ( Future<Integer> leaf : leafs ) {
            Integer result = await.result( req, leaf );

            assertEquals( depth, result.intValue() );
        }
    }


    private List<Future<Integer>> generateLeafs( List<Future<Integer>> roots, List<Function1<Future<Integer>,Future<Integer>>> nextStepGenerators ) {
        List<Future<Integer>> leafs = cast( roots );

        for ( int i = 0; i < depth; i++ ) {
            leafs = generateNextGeneration( leafs, nextStepGenerators );
        }

        return leafs;
    }


    private List<Future<Integer>> generateNextGeneration( List<Future<Integer>> prevGeneration, List<Function1<Future<Integer>,Future<Integer>>> nextStepGenerators ) {
        List<Future<Integer>> newGeneration = new ArrayList<>(prevGeneration.size()*breadth);

        for ( Future<Integer> parent : prevGeneration ) {
            for ( int i=0; i<breadth; i++ ) {
                newGeneration.add( generateNextResultStep(parent,nextStepGenerators) );
            }
        }

        return newGeneration;
    }

    private Future<Integer> generateNextResultStep( Future<Integer> prev, List<Function1<Future<Integer>,Future<Integer>>> nextStepGenerators ) {
        int i = rnd.nextInt( nextStepGenerators.size() );

        return nextStepGenerators.get(i).invoke( prev );
    }


    private List<Function1<Future<Integer>,Future<Integer>>> nextStepResultGenerators = Arrays.asList(
        // Completed Futures
        (prev) -> prev.mapResult((r,v) -> v+1),
        (prev) -> prev.mapResultToTryable((r,v) -> Try.succeeded(v+1)),
        (prev) -> prev.<Integer>mapResultToTryable((r,v) -> Try.failed(new StressFailure(v+1))).recover((r,f) -> ((StressFailure) f).intValue()),
        (prev) -> prev.flatMapResult((r,v) -> Future.successful(req,v+1)),
        (prev) -> prev.<Integer>flatMapResult((r,v) -> Future.failure(req,new StressFailure(v+1))).recover((r,f) -> ((StressFailure) f).intValue()),
        (prev) -> prev.recover((r,f) -> ((StressFailure) f).intValue() + 1).mapResult( (r,v) -> v+1 ),
        (prev) -> prev.flatRecover((r,f) -> Future.successful(req, ((StressFailure) f).intValue()+1)).mapResult( (r,v) -> v+1 ),
        (prev) -> prev.flatRecover((r,f) -> Future.failure(req, ((StressFailure) f).inc())).mapResult( (r,v) -> v+1 ),
        (prev) -> prev.mapFailure((r,f) -> ((StressFailure) f).inc()).mapResult( (r,v) -> v+1 ),
        (prev) -> prev.flatMapFailure((r,f) -> Future.failure(req, ((StressFailure) f).inc())).mapResult( (r,v) -> v+1 ),

        // Defer to another Executor
        (prev) -> prev.mapResult(rndExecutor(), (r,v) -> v+1),
        (prev) -> prev.mapResultToTryable(rndExecutor(), (r,v) -> Try.succeeded(v+1)),
        (prev) -> prev.<Integer>mapResultToTryable(rndExecutor(), (r,v) -> Try.failed(new StressFailure(v+1))).recover( rndExecutor(), (r,f) -> ((StressFailure) f).intValue()),
        (prev) -> prev.flatMapResult((r,v) -> Future.successful(req,v+1)),
        (prev) -> prev.<Integer>flatMapResult(rndExecutor(), (r,v) -> Future.failure(req,new StressFailure(v+1))).recover(rndExecutor(), (r,f) -> ((StressFailure) f).intValue()),
        (prev) -> prev.recover(rndExecutor(), (r,f) -> ((StressFailure) f).intValue() + 1).mapResult( rndExecutor(), (r,v) -> v+1 ),
        (prev) -> prev.flatRecover(rndExecutor(), (r,f) -> Future.successful(req, ((StressFailure) f).intValue()+1)).mapResult( rndExecutor(), (r,v) -> v+1 ),
        (prev) -> prev.flatRecover(rndExecutor(), (r,f) -> Future.failure(req, ((StressFailure) f).inc())).mapResult( rndExecutor(), (r,v) -> v+1 ),
        (prev) -> prev.mapFailure(rndExecutor(), (r,f) -> ((StressFailure) f).inc()).mapResult( rndExecutor(), (r,v) -> v+1 ),
        (prev) -> prev.flatMapFailure(rndExecutor(), (r,f) -> Future.failure(req, ((StressFailure) f).inc())).mapResult( rndExecutor(), (r,v) -> v+1 )
    );

    private List<Function1<Future<Integer>,Future<Integer>>> nextStepFailureGenerators = Arrays.asList(
        // Completed Futures
        (prev) -> prev.mapResult((r,v) -> v+1).mapFailure( (r,f) -> ((StressFailure) f).inc() ),
        (prev) -> prev.mapResultToTryable((r,v) -> Try.succeeded(v+1)).mapFailure( (r,f) -> ((StressFailure) f).inc() ),
        (prev) -> prev.<Integer>mapResultToTryable((r,v) -> Try.failed(new StressFailure(v+1))).mapFailure( (r,f) -> ((StressFailure) f).inc() ),
        (prev) -> prev.flatMapResult((r,v) -> Future.successful(req,v+1)).mapFailure( (r,f) -> ((StressFailure) f).inc() ),
        (prev) -> prev.<Integer>flatMapResult((r,v) -> Future.failure(req,new StressFailure(v+1))).mapFailure( (r,f) -> ((StressFailure) f).inc() ),
        (prev) -> prev.recover((r,f) -> ((StressFailure) f).intValue() + 1).mapResultToTryable( (r,v) -> Try.failed(new StressFailure(v)) ),
        (prev) -> prev.flatRecover((r,f) -> Future.successful(req, ((StressFailure) f).intValue()+1)).mapResultToTryable( (r,v) -> Try.failed(new StressFailure(v)) ),
        (prev) -> prev.flatRecover((r,f) -> Future.failure(req, ((StressFailure) f).inc())),
        (prev) -> prev.mapFailure((r,f) -> ((StressFailure) f).inc()),
        (prev) -> prev.flatMapFailure((r,f) -> Future.failure(req, ((StressFailure) f).inc())),

        // Defer to another Executor
        (prev) -> prev.mapResult(rndExecutor(), (r,v) -> v+1).mapFailure( rndExecutor(), (r,f) -> ((StressFailure) f).inc() ),
        (prev) -> prev.mapResultToTryable(rndExecutor(), (r,v) -> Try.succeeded(v+1)).mapFailure( rndExecutor(), (r,f) -> ((StressFailure) f).inc() ),
        (prev) -> prev.<Integer>mapResultToTryable(rndExecutor(), (r,v) -> Try.failed(new StressFailure(v+1))).mapFailure( rndExecutor(), (r,f) -> ((StressFailure) f).inc() ),
        (prev) -> prev.flatMapResult(rndExecutor(), (r,v) -> Future.successful(req,v+1)).mapFailure( rndExecutor(), (r,f) -> ((StressFailure) f).inc() ),
        (prev) -> prev.<Integer>flatMapResult(rndExecutor(), (r,v) -> Future.failure(req,new StressFailure(v+1))).mapFailure( rndExecutor(), (r,f) -> ((StressFailure) f).inc() ),
        (prev) -> prev.recover(rndExecutor(), (r,f) -> ((StressFailure) f).intValue() + 1).mapResultToTryable( rndExecutor(), (r,v) -> Try.failed(new StressFailure(v)) ),
        (prev) -> prev.flatRecover(rndExecutor(), (r,f) -> Future.successful(req, ((StressFailure) f).intValue()+1)).mapResultToTryable( rndExecutor(), (r,v) -> Try.failed(new StressFailure(v)) ),
        (prev) -> prev.flatRecover(rndExecutor(), (r,f) -> Future.failure(req, ((StressFailure) f).inc())),
        (prev) -> prev.mapFailure(rndExecutor(), (r,f) -> ((StressFailure) f).inc()),
        (prev) -> prev.flatMapFailure(rndExecutor(), (r,f) -> Future.failure(req, ((StressFailure) f).inc()))
 );


    private Executor rndExecutor() {
        return executors.get( rnd.nextInt(executors.size()) );
    }

    private static class StressFailure implements Failure {
        private int i;

        public StressFailure( int i ) {
            this.i = i;
        }

        public int intValue() {
            return i;
        }

        public StressFailure inc() {
            return new StressFailure( i+1 );
        }

        public Throwable toException() {
            throw new UnsupportedOperationException();
        }
    }
}
