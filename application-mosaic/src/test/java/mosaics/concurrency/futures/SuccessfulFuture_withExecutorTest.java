package mosaics.concurrency.futures;

import mosaics.cli.requests.Request;
import mosaics.concurrency.CompleteRequestExecutor;
import mosaics.concurrency.MockFailure;
import mosaics.concurrency.SingleThreadedExecutor;
import mosaics.fp.ExceptionFailure;
import mosaics.fp.Failure;
import mosaics.fp.Try;
import mosaics.junit.JMThreadExtension;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.parallel.Isolated;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;


@Isolated
@JMThreadExtension.IgnoreThread("Attach Listener")
@ExtendWith(JMThreadExtension.class)
public class SuccessfulFuture_withExecutorTest extends BaseFutureTestSupport {

    private SingleThreadedExecutor executor = new SingleThreadedExecutor("junit");
    private Future<String> future   = Future.successful( request, "hello" );


    @BeforeEach
    public void setup() {
        executor.start();
    }

    @AfterEach
    public void tearDown() {
        executor.stop();
    }



    @Test
    public void isComplete_expectTrue() {
        assertTrue( future.withExecutor(executor).isComplete() );
    }

    @Test
    public void hasResult_expectTrue() {
        assertTrue( future.withExecutor(executor).hasResult() );
    }

    @Test
    public void hasFailure_expectFalse() {
        assertFalse( future.withExecutor(executor).hasFailure() );
    }

    @Test
    public void withSameExecutorWillReturnTheSameFuture() {
        Future<String> dispatchingFuture = future.withExecutor(executor);

        assertSame( dispatchingFuture, dispatchingFuture.withExecutor(executor) );
    }

    @Test
    public void withDescription() {
        assertEquals( "foo", future.withDescription( "foo" ).withExecutor(executor).toString() );
        assertEquals( "foo", future.withExecutor(executor).withDescription( "foo" ).toString() );

        Future<String> f = future.withExecutor( executor ).withDescription( "foo" );
        assertSame( f, f.withDescription("foo") );
    }


// public <B> Future<B> mapResult( Executer executer, Function1<T, B> mappingFunction );

    @Test
    public void mapResultWithExecutor_succeed_expectNewFutureWithMappedValue() {
        Future<Integer> resultF = future.mapResult( executor, (r,v) -> {recordCallbackCall(); return v.length();} );

        assertResult( 5, resultF );
        assertCallbackWasCalledFrom(executor.getWorkerThread());
    }

    @Test
    public void givenFinishedRequest_mapResultWithExecutor_expectTimeout() {
        request.markCompleted();

        Failure expectation = new TimeoutFailure( request, future );
        Future<Integer> resultF = future.mapResult( executor, (r,v) -> {recordCallbackCall(); return v.length();} );

        assertFailure( expectation, resultF );
        assertCallbackWasNotCalled();
    }

    @Test
    public void givenRequestCompletesAfterScheduling_mapResultWithExecutor_expectTimeout() {
        Failure expectation = new TimeoutFailure( request, future );
        Future<Integer> resultF = future.mapResult( new CompleteRequestExecutor(request), (r,v) -> {recordCallbackCall(); return v.length();} );

        assertFailure( expectation, resultF );
        assertCallbackWasNotCalled();
    }

    @Test
    public void withDescriptionThenMapResultWithExecutor_expectDescriptionToCarryOver() {
        Future<Integer> resultF = future.withDescription( "abc" )
            .mapResult( executor, (r,v) -> {recordCallbackCall(); return v.length();} );

        assertResult( 5, resultF );
        assertCallbackWasCalledFrom(executor.getWorkerThread());
        assertEquals( "abc", resultF.toString() );
    }

    @Test
    public void mapResultWithExecutor_fail_expectFailure() {
        Exception       ex      = new IllegalArgumentException("ex");
        Future<Integer> resultF = future.mapResult( executor, (r,v) -> {recordCallbackCall(); return throwException(ex);} );

        assertFailure( new ExceptionFailure(ex), resultF );
        assertCallbackWasCalledFrom(executor.getWorkerThread());
    }


// public default <B> Future<B> mapResultToTryable( Executer executer, Function1<T, Tryable<B>> mappingFunction )

    @Test
    public void mapResultToTryableWithExecutor_succeed_expectNewFutureWithMappedValue() {
        Future<Integer> resultF = future.mapResultToTryable( executor, (r,str) -> {recordCallbackCall(); return Try.succeeded(str.length());} );

        assertResult( 5, resultF );
        assertCallbackWasCalledFrom(executor.getWorkerThread());
    }

    @Test
    public void givenFinishedRequest_mapResultToTryableWithExecutor_expectTimeout() {
        request.markCompleted();

        Failure expectation = new TimeoutFailure( request, future );
        Future<Integer> resultF = future.mapResultToTryable( executor, (r,str) -> {recordCallbackCall(); return Try.succeeded(str.length());} );

        assertFailure( expectation, resultF );
        assertCallbackWasNotCalled();
    }

    @Test
    public void withDescriptionThenMapResultToTryableWithExecutor_expectDescriptionToCarryOver() {
        Future<Integer> resultF = future.withDescription( "abc" )
            .mapResultToTryable( executor, (r,str) -> {recordCallbackCall(); return Try.succeeded(str.length());} );

        assertResult( 5, resultF );
        assertCallbackWasCalledFrom(executor.getWorkerThread());
        assertEquals( "abc", resultF.toString() );
    }

    @Test
    public void mapResultToTryableWithExecutor_returnFailure_expectFailure() {
        Failure failure = new MockFailure("junit");
        Future<Integer> resultF = future.mapResultToTryable( executor, (r,str) -> {recordCallbackCall(); return Try.failed(failure);} );

        assertFailure( failure, resultF );
        assertCallbackWasCalledFrom(executor.getWorkerThread());
    }

    @Test
    public void mapResultToTryableWithExecutor_throwException_expectFailure() {
        Exception       ex      = new IllegalArgumentException("ex");
        Future<Integer> resultF = future.mapResultToTryable( executor, (r,str) -> {recordCallbackCall(); return throwException(ex);} );

        assertFailure( new ExceptionFailure(ex), resultF );
        assertCallbackWasCalledFrom(executor.getWorkerThread());
    }


// public <B> Future<B> flatMapResult( Executer executer, Function1<T, Future<B>> mappingFunction );

    @Test
    public void flatMapResultWithExecutor_succeed_expectNewFutureWithMappedValue() {
        Future<Integer> resultF = future.flatMapResult( executor, (r,str) -> {recordCallbackCall(); return Future.successful(request,str.length());} );

        assertResult( 5, resultF );
        assertCallbackWasCalledFrom(executor.getWorkerThread());
    }

    @Test
    public void givenFinishedRequest_flatMapResultWithExecutor_expectTimeout() {
        request.markCompleted();

        Failure expectation = new TimeoutFailure( request, future );
        Future<Integer> resultF = future.flatMapResult( executor, (r,str) -> {recordCallbackCall(); return Future.successful(request,str.length());} );

        assertFailure( expectation, resultF );
        assertCallbackWasNotCalled();
    }

    @Test
    public void withDescriptionThenFlatMapResultWithExecutor_expectDescriptionToCarryOver() {
        Future<Integer> resultF = future.withDescription( "abc" )
            .flatMapResult( executor, (r,str) -> {recordCallbackCall(); return Future.successful(request,str.length());} );

        assertResult( 5, resultF );
        assertCallbackWasCalledFrom(executor.getWorkerThread());
        assertEquals( "abc", resultF.toString() );
    }

    @Test
    public void flatMapResultWithExecutor_returnFailure_expectFailure() {
        Failure         failure = new MockFailure("junit");
        Future<Integer> resultF = future.flatMapResult( executor, (r,str) -> {recordCallbackCall(); return Future.failure(request,failure);} );

        assertFailure( failure, resultF );
        assertCallbackWasCalledFrom(executor.getWorkerThread());
    }

    @Test
    public void flatMapResultWithExecutor_throwException_expectFailure() {
        Exception       ex      = new IllegalArgumentException("ex");
        Future<Integer> resultF = future.flatMapResult( executor, (r,str) -> {recordCallbackCall(); return throwException(ex);} );

        assertFailure( new ExceptionFailure(ex), resultF );
        assertCallbackWasCalledFrom(executor.getWorkerThread());
    }


// public Future<T> recover( Executer executer, Function1<Failure, T> recoveryFunction );

    @Test
    public void recoverWithExecutor_expectRecoverCallbackToNotBeInvoked() {
        Future<String> resultF = future.recover( executor, (r,f) -> {recordCallbackCall(); return "recovered";} );

        assertResult( "hello", resultF );
        assertCallbackWasNotCalled();
    }

    @Test
    public void givenFinishedRequest_recoverWithExecutor_expectRecoverCallbackToNotBeInvoked() {
        request.markCompleted();

        Future<String> resultF = future.recover( executor, (r,f) -> {recordCallbackCall(); return "recovered";} );

        assertResult( "hello", resultF );
        assertCallbackWasNotCalled();
    }

    @Test
    public void withDescriptionThenRecoverWithExecutor_expectDescriptionToCarryOver() {
        Future<String> resultF = future.withDescription( "abc" )
            .recover( executor, (r,f) -> {recordCallbackCall(); return "recovered";} );

        assertResult( "hello", resultF );
        assertCallbackWasNotCalled();
        assertEquals( "abc", resultF.toString() );
    }


// public Future<T> flatRecover( Executer executer, Function1<Failure, Future<T>> recoveryFunction );

    @Test
    public void flatRecoverWithExecutor_expectRecoverCallbackToNotBeInvoked() {
        Future<String> resultF = future.flatRecover( executor, (r,f) -> {recordCallbackCall(); return Future.successful(request,"recovered");} );

        assertResult( "hello", resultF );
        assertCallbackWasNotCalled();
    }

    @Test
    public void givenFinishedRequest_flatRecoverWithExecutor_expectRecoverCallbackToNotBeInvoked() {
        request.markCompleted();

        Future<String> resultF = future.flatRecover( executor, (r,f) -> {recordCallbackCall(); return Future.successful(request,"recovered");} );

        assertResult( "hello", resultF );
        assertCallbackWasNotCalled();
    }

    @Test
    public void withDescriptionThenFlatRecoverWithExecutor_expectDescriptionToCarryOver() {
        Future<String> resultF = future.withDescription( "abc" )
            .flatRecover( executor, (r,f) -> {recordCallbackCall(); return Future.successful(request,"recovered");} );

        assertResult( "hello", resultF );
        assertCallbackWasNotCalled();
        assertEquals( "abc", resultF.toString() );
    }

// public Future<T> mapFailure( Executer executer, Function1<Failure, Failure> mappingFunction );

    @Test
    public void mapFailureWithExecutor_expectRecoverCallbackToNotBeInvoked() {
        Future<String> resultF = future.mapFailure( executor, (r,f) -> {recordCallbackCall(); return f;} );

        assertResult( "hello", resultF );
        assertCallbackWasNotCalled();
    }

    @Test
    public void givenFinishedRequest_mapFailureWithExecutor_expectRecoverCallbackToNotBeInvoked() {
        request.markCompleted();

        Future<String> resultF = future.mapFailure( executor, (r,f) -> {recordCallbackCall(); return f;} );

        assertResult( "hello", resultF );
        assertCallbackWasNotCalled();
    }

    @Test
    public void withDescriptionThenMapFailureWithExecutor_expectDescriptionToCarryOver() {
        Future<String> resultF = future.withDescription( "abc" )
            .mapFailure( executor, (r,f) -> {recordCallbackCall(); return f;} );

        assertResult( "hello", resultF );
        assertCallbackWasNotCalled();
        assertEquals( "abc", resultF.toString() );
    }


// public Future<T> flatMapFailure( Function1<Failure, Failure> mappingFunction );

    @Test
    public void flatMapFailure_expectRecoverCallbackToNotBeInvoked() {
        Future<String> resultF = future.flatMapFailure( executor, (r,f) -> {recordCallbackCall(); return Future.failure(request,f);} );

        assertResult( "hello", resultF );
        assertCallbackWasNotCalled();
    }

    @Test
    public void givenFinishedRequest_flatMapFailure_expectRecoverCallbackToNotBeInvoked() {
        request.markCompleted();

        Future<String> resultF = future.flatMapFailure( executor, (r,f) -> {recordCallbackCall(); return Future.failure(request,f);} );

        assertResult( "hello", resultF );
        assertCallbackWasNotCalled();
    }

    @Test
    public void withDescriptionThenFlatMapFailure_expectDescriptionToCarryOver() {
        Future<String> resultF = future.withDescription( "abc" )
            .flatMapFailure( executor, (r,f) -> {recordCallbackCall(); return Future.failure(request,f);} );

        assertResult( "hello", resultF );
        assertCallbackWasNotCalled();
        assertEquals( "abc", resultF.toString() );
    }


// public Future<T> onSuccess( Executer executer, VoidFunction1<T> callback );

    @Test
    public void onSuccessWithExecutor_expectCallbackWasCalled() {
        Future<String> resultF = future.onSuccess( executor, (r,f) -> recordCallbackCall() );

        assertResult( "hello", resultF );
        assertCallbackWasCalledFrom(executor.getWorkerThread());
    }

    @Test
    public void givenFinishedRequest_onSuccessWithExecutor_expectCallbackWasCalled() {
        request.markCompleted();

        Future<String> resultF = future.onSuccess( executor, (r,f) -> recordCallbackCall() );

        assertResult( "hello", resultF );
        assertCallbackWasCalledFrom(executor.getWorkerThread());
    }

    @Test
    public void withDescriptionThenOnSuccessWithExecutor_expectDescriptionToCarryOver() {
        Future<String> resultF = future.withDescription( "abc" )
            .onSuccess( executor, (r,f) -> recordCallbackCall() );

        assertResult( "hello", resultF );
        assertCallbackWasCalledFrom(executor.getWorkerThread());
        assertEquals( "abc", resultF.toString() );
    }

    @Test
    public void onSuccessThrowsExceptionWithExecutor_expectErrorToBeLogged() {
        Exception       ex      = new IllegalArgumentException("ex");
        Future<String> resultF = future.onSuccess( executor, (r,f) -> {recordCallbackCall(); throwException(ex);} );

        assertResult( "hello", resultF );
        assertCallbackWasCalledFrom(executor.getWorkerThread());

        capturingLogWriter.assertContainsMessage( "A future's side effect threw the following exception: java.lang.IllegalArgumentException: ex" );
    }


// public Future<T> onFailureWithExecutor( Executer executer, VoidFunction1<Failure> callback );

    @Test
    public void onFailure_expectCallbackWasNotCalled() {
        Future<String> resultF = future.onFailure( executor, (r,f) -> recordCallbackCall() );

        assertResult( "hello", resultF );
        assertCallbackWasNotCalled();
    }

    @Test
    public void givenFinishedRequest_onFailure_expectCallbackWasNotCalled() {
        request.markCompleted();

        Future<String> resultF = future.onFailure( executor, (r,f) -> recordCallbackCall() );

        assertResult( "hello", resultF );
        assertCallbackWasNotCalled();
    }

    @Test
    public void withDescriptionThenOnFailure_expectDescriptionToCarryOver() {
        Future<String> resultF = future.withDescription( "abc" )
            .onFailure( executor, (r,f) -> recordCallbackCall() );

        assertResult( "hello", resultF );
        assertCallbackWasNotCalled();
        assertEquals( "abc", resultF.toString() );
    }


    @Test
    public void equalsContract() {
        EqualsVerifier.forClass( SuccessfulDispatchingFuture.class)
            .suppress( Warning.STRICT_INHERITANCE)
            .withPrefabValues( Request.class, request, env.createRequest("equals-verifier") )
            .verify();
    }

    @Test
    public void testToString() {
        assertEquals( "Future(hello)", future.withExecutor(executor).toString() );
    }

}
