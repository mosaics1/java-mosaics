package mosaics.concurrency.futures;

import mosaics.cli.Env;
import mosaics.cli.requests.Request;
import mosaics.concurrency.Await;
import mosaics.concurrency.MockFailure;
import mosaics.fp.MultipleFailures;
import mosaics.fp.Try;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;


@SuppressWarnings("unchecked")
public class FutureTest {

    private Env     env     = Env.createTestEnv();
    private Await await   = new Await(env);
    private Request request = env.createRequest( "junit" );


    @Test
    public void successfulWithDescription() {
        assertEquals( "a", Future.successful(request,123, "a").toString() );
    }

    @Test
    public void completedSuccessfulWithDescription() {
        assertEquals( "a", Future.completed(request, Try.succeeded(123), "a").toString() );
    }

    @Test
    public void completedFailedWithDescription() {
        assertEquals( "a", Future.completed(request, Try.failed(new MockFailure("f")), "a").toString() );
    }


// joinFutures

    @Test
    public void joinFutures_noFutures_expectEmptyList() {
        Future<List<String>> f = Future.joinFutures(request);

        assertEquals( 0, await.result(request,f).size() );
    }

    @Test
    public void joinFutures_threeSuccessfulFutures_expectListContainingResultOfEachFutureInOrder() {
        Future<List<String>> f = Future.joinFutures(
            request,
            Future.successful(request,"1"),
            Future.successful(request,"2"),
            Future.successful(request,"3")
        );

        assertEquals( Arrays.asList("1","2","3"), await.result(request,f) );
    }

    @Test
    public void joinFutures_iterables() {
        Future<List<String>> f = Future.joinFutures(
            request,
            Arrays.asList(
                Future.successful(request,"1"),
                Future.successful(request,"2"),
                Future.successful(request,"3")
            )
        );

        assertEquals( Arrays.asList("1","2","3"), await.result(request,f) );
    }

    @Test
    public void joinFutures_threeFuturesWithOneFailure_expectFailure() {
        Future<List<String>> f = Future.joinFutures(
            request,
            Future.successful(request,"1"),
            Future.failure(request,new MockFailure("2")),
            Future.successful(request,"3")
        );

        assertEquals( new MockFailure("2"), await.failure(request,f) );
    }

    @Test
    public void joinFutures_threeFuturesWithTwoFailures_expectCompositeFailure() {
        Future<List<String>> f = Future.joinFutures(
            request,
            Future.failure(request,new MockFailure("1")),
            Future.failure(request,new MockFailure("2")),
            Future.failure(request,new MockFailure("3"))
        );

        MultipleFailures expectation = new MultipleFailures(
            new MockFailure( "1" ),
            new MockFailure( "2" ),
            new MockFailure( "3" )
        );

        assertEquals( expectation, await.failure(request,f) );
    }

}
