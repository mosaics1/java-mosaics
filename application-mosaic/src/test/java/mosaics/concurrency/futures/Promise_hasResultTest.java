package mosaics.concurrency.futures;


import mosaics.concurrency.MockFailure;
import org.junit.jupiter.api.Test;

import static mosaics.lang.Backdoor.cast;
import static org.junit.jupiter.api.Assertions.assertThrows;


public class Promise_hasResultTest extends BaseHasResultFutureTestCases {

    public Promise_hasResultTest() {
        super( Promise.class );

        Promise<String> promise = new Promise<>(request);
        promise.setResult( "hello" );

        this.future = promise;
    }


//    @Test
//    public void timeoutAfterRecoveringAPromise() {
//        request = new Request();
//
//        Promise<String> p           = new Promise<>( request );
//        Future<String>  f = p.recover( (r,failure) -> "a" );
//        request.withFinishedAtMillis( FP.option(System.currentTimeMillis()) );
//
//        TimeoutFailure  expectation = new TimeoutFailure( request, p );
//        p.setFailure( new MockFailure("") );
//
//        assertFailure( expectation, f );
//    }
    @Test
    public void completePromiseAfterTwoSeparateMapOperationsHaveBeenRegistered() {
        Promise<Integer> z = new Promise<>( request );
        Future<Integer> a = z.mapResult( UseCallingThreadExecutor.INSTANCE, (r,v) -> v+1 );
        Future<Integer>  b = z.mapResult( UseCallingThreadExecutor.INSTANCE, (r,v) -> v+1 );

        z.setResult( 0 );

        assertResult( 1, a );
        assertResult( 1, b );
    }

    @Test
    public void completePromiseAfterChainingInSerialTwoSeparateMapOperationsHaveBeenRegistered() {
        Promise<Integer> z = new Promise<>( request );
        Future<Integer>  a = z.mapResult( UseCallingThreadExecutor.INSTANCE, (r,v) -> v+1 );
        Future<Integer>  c = a.mapResult( UseCallingThreadExecutor.INSTANCE, (r,v) -> v+1 );

        z.setResult( 0 );

        assertResult( 1, a );
        assertResult( 2, c );
    }

    @Test
    public void setSuccessOnASuccessfulPromise_expectUnsupportedOperationException() {
        Promise<String> promise = cast(future);

        assertThrows( UnsupportedOperationException.class, () -> promise.setResult( "abc" ) );
    }

    @Test
    public void setFailureOnASuccessfulPromise_expectUnsupportedOperationException() {
        Promise<String> promise = cast(future);

        assertThrows( UnsupportedOperationException.class, () -> promise.setFailure( new MockFailure("abc") ) );
    }

}
