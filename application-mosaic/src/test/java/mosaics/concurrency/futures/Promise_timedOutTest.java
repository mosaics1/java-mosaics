package mosaics.concurrency.futures;

import mosaics.concurrency.CompleteRequestExecutor;
import mosaics.concurrency.MockFailure;
import mosaics.fp.Failure;
import mosaics.fp.Try;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.concurrent.atomic.AtomicReference;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class Promise_timedOutTest extends BaseFutureTestSupport {
    private Promise<String> promise = new Promise<>(request, "desc");
    private Future<String>  future;

    @BeforeEach
    public void setUp() {
        request.markCompleted();

        future = promise.mapResult( (r,v) -> v );
    }

    @Test
    public void setResult_expectLogMessage() {
        promise.setResult( "abc" );

        capturingLogWriter.assertContainsMessage( "Discarded value: abc" );
    }

    @Test
    public void setFailure_expectLogMessage() {
        Failure f = new MockFailure( "abc" );
        promise.setFailure( f );

//        capturingLogWriter.dumpAll();
        capturingLogWriter.assertContainsMessage( "Discarded failure: MockFailure(msg=abc)" );
    }

    @Test
    public void isComplete() {
        assertTrue( promise.isComplete() );
    }

    @Test
    public void hasResult() {
        assertFalse( promise.hasResult() );
    }

    @Test
    public void hasFailure() {
        assertTrue( promise.hasFailure() );
    }

    @Test
    public void withExecutor_expectTheSameFailedFutureToBeReused() {
        assertSame( future, promise.withExecutor( UseCallingThreadExecutor.INSTANCE) );
    }

    @Test
    public void withDescription_noChangeToDesc_expectTheSameFailedFutureToBeReused() {
        assertSame( future, promise.withDescription("desc") );
    }

    @Test
    public void withDescription_withChangeToDesc_expectTheSameFailedFutureToBeReused() {
        assertSame( future, promise.withDescription("abc") );
    }

    @Test
    public void mapResult_withChangeToDesc_expectTheSameFailedFutureToBeReused() {
        assertSame( future, promise.mapResult( (r,v) -> v ) );
    }

    @Test
    public void mapResultToTryable_withChangeToDesc_expectTheSameFailedFutureToBeReused() {
        assertSame( future, promise.mapResultToTryable( (r,v) -> Try.succeeded("a") ) );
    }

    @Test
    public void flatMapResult_withChangeToDesc_expectTheSameFailedFutureToBeReused() {
        assertSame( future, promise.flatMapResult( Future::successful ) );
    }

    @Test
    public void recover_withChangeToDesc_expectTheSameFailedFutureToBeReused() {
        assertSame( future, promise.recover( (r,f) -> "abc" ) );
    }

    @Test
    public void timeoutRecoveringAPromise() {
        Promise<String> p           = new Promise<>( request );
        TimeoutFailure expectation = new TimeoutFailure( request, p );

        p.recover( (r,f) -> "foo" );

        assertFailure( expectation, p );
    }

    @Test
    public void timeoutAfterMappingAPromise() {
        request = env.createRequest( "junit2" );


        Promise<String> p           = new Promise<>( request );

        Future<String> f = p.mapResult( (r,v) -> v+"a" );
        request.markCompleted();

        TimeoutFailure  expectation = new TimeoutFailure( request, p );
        p.setResult( "foo" );

        assertFailure( expectation, f );
    }

    @Test
    public void timeoutAfterSchedulingAResultMapping() {
        request = env.createRequest( "junit2" );

        Promise<String> p           = new Promise<>( request );
        TimeoutFailure  expectation = new TimeoutFailure( request, p );

        p.setResult( "abc" );
        Future<String> f = p.mapResult( new CompleteRequestExecutor(request), (r,v) -> v+"a" );



        assertFailure( expectation, f );
    }

    @Test
    public void timeoutAndThenTryToSetTheValue_expectNoExceptionBecauseOfTheTimeout() {
        promise.setResult( "abc" );
    }

    @Test
    public void timeoutAfterRecoveringAPromise() {
        request = env.createRequest( "junit2" );

        Promise<String> p           = new Promise<>( request );
        Future<String>  f = p.recover( (r,failure) -> "a" );
        request.markCompleted();

        TimeoutFailure  expectation = new TimeoutFailure( request, p );
        p.setFailure( new MockFailure("") );

        assertFailure( expectation, f );
    }

    @Test
    public void flatRecover_withChangeToDesc_expectTheSameFailedFutureToBeReused() {
        assertSame( future, promise.flatRecover( (r,f) -> Future.successful(r,"abc") ) );
    }

    @Test
    public void mapFailure_withChangeToDesc_expectTheSameFailedFutureToBeReused() {
        assertSame( future, promise.mapFailure( (r,f) -> f ) );
    }

    @Test
    public void flatMapFailure_withChangeToDesc_expectTheSameFailedFutureToBeReused() {
        assertSame( future, promise.flatMapFailure(Future::failure) );
    }

    @Test
    public void onSuccess_withChangeToDesc_expectTheSameFailedFutureToBeReused() {
        AtomicReference<String> ref = new AtomicReference<>();

        assertSame( future, promise.onSuccess( (r,s) -> ref.set(s) ) );
    }


    @Test
    public void onFailure_withChangeToDesc_expectTheSameFailedFutureToBeReused() {
        AtomicReference<Failure> ref = new AtomicReference<>();

        assertSame( future, promise.onFailure( (r,f) -> ref.set(f) ) );
    }

    @Test
    public void testToString() {
        assertEquals( "desc", promise.toString() );
    }

}
