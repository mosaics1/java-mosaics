package mosaics.concurrency.futures;

import mosaics.concurrency.MockFailure;
import mosaics.cli.requests.Request;
import mosaics.fp.ExceptionFailure;
import mosaics.fp.Failure;
import mosaics.fp.Try;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.junit.jupiter.api.Test;

import java.util.concurrent.atomic.AtomicReference;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;


public abstract class BaseHasResultFutureTestCases extends BaseFutureTestSupport {

    private   Class<? extends Future> futureType;

    protected Future<String>          future;


    BaseHasResultFutureTestCases( Class<? extends Future> type ) {
        this.futureType = type;
    }


    @Test
    public void isComplete_expectTrue() {
        assertTrue( future.isComplete() );
    }

    @Test
    public void hasResult_expectTrue() {
        assertTrue( future.hasResult() );
    }

    @Test
    public void hasFailure_expectFalse() {
        assertFalse( future.hasFailure() );
    }


    @Test
    public void withDescription() {
        assertEquals( "foo", future.withDescription( "foo" ).toString() );
    }

// public <B> Future<B> mapResult( Function1<T, B> mappingFunction );

    @Test
    public void mapResult_succeed_expectNewFutureWithMappedValue() {
        Future<Integer> resultF = future.mapResult( (r,v) -> {recordCallbackCall(); return v.length();} );

        assertResult( 5, resultF );
        assertCallbackWasCalledFromCurrentThread();
    }

    @Test
    public void withDescriptionThenMapResult_expectDescriptionToCarryOver() {
        Future<Integer> resultF = future.withDescription( "abc" ).mapResult( (r,v) -> {recordCallbackCall(); return v.length();} );

        assertResult( 5, resultF );
        assertCallbackWasCalledFromCurrentThread();
        assertEquals( "abc", resultF.toString() );
    }

    @Test
    public void mapResult_fail_expectFailure() {
        Exception       ex      = new IllegalArgumentException("ex");
        Future<Integer> resultF = future.mapResult( (r,v) -> {recordCallbackCall(); return throwException(ex);} );

        assertFailure( new ExceptionFailure(ex), resultF );
        assertCallbackWasCalledFromCurrentThread();
    }

    @Test
    public void mapResultAfterRequestHasFinished_expectTimeout() {
        request.markCompleted();

        TimeoutFailure expectation = new TimeoutFailure( request, future );
        Future<Integer> resultF = future.mapResult( (r,v) -> {recordCallbackCall(); return v.length();} );

        assertFailure( expectation, resultF );
        assertCallbackWasNotCalled();
    }

// public default <B> Future<B> mapResultToTryable( Function1<T, Tryable<B>> mappingFunction )

    @Test
    public void mapResultToTryable_succeed_expectNewFutureWithMappedValue() {
        Future<Integer> resultF = future.mapResultToTryable( (r,str) -> {recordCallbackCall(); return Try.succeeded(str.length());} );

        assertResult( 5, resultF );
        assertCallbackWasCalledFromCurrentThread();
    }

    @Test
    public void withDescriptionThenMapResultToTryable_expectDescriptionToCarryOver() {
        Future<Integer> resultF = future.withDescription( "abc" )
            .mapResultToTryable( (r,str) -> {recordCallbackCall(); return Try.succeeded(str.length());} );

        assertResult( 5, resultF );
        assertCallbackWasCalledFromCurrentThread();
        assertEquals( "abc", resultF.toString() );
    }

    @Test
    public void mapResultToTryable_returnFailure_expectFailure() {
        Failure failure = new MockFailure("junit");
        Future<Integer> resultF = future.mapResultToTryable( (r,str) -> {recordCallbackCall(); return Try.failed(failure);} );

        assertFailure( failure, resultF );
        assertCallbackWasCalledFromCurrentThread();
    }

    @Test
    public void mapResultToTryable_throwException_expectFailure() {
        Exception       ex      = new IllegalArgumentException("ex");
        Future<Integer> resultF = future.mapResultToTryable( (r,str) -> {recordCallbackCall(); return throwException(ex);} );

        assertFailure( new ExceptionFailure(ex), resultF );
        assertCallbackWasCalledFromCurrentThread();
    }

    @Test
    public void mapResultToTryable_requestHasFinished_expectTimeout() {
        request.markCompleted();

        TimeoutFailure expectation = new TimeoutFailure( request, future );
        Future<Integer> resultF = future.mapResultToTryable( (r,v) -> {recordCallbackCall(); return Try.succeeded(v.length());} );

        assertFailure( expectation, resultF );
        assertCallbackWasNotCalled();
    }

// public <B> Future<B> flatMapResult( Function1<T, Future<B>> mappingFunction );

    @Test
    public void flatMapResult_succeed_expectNewFutureWithMappedValue() {
        Future<Integer> resultF = future.flatMapResult( (r,str) -> {recordCallbackCall(); return Future.successful(request,str.length());} );

        assertResult( 5, resultF );
        assertCallbackWasCalledFromCurrentThread();
    }

    @Test
    public void withDescriptionThenFlatMapResult_expectDescriptionToCarryOver() {
        Future<Integer> resultF = future.withDescription( "abc" )
            .flatMapResult( (r,str) -> {recordCallbackCall(); return Future.successful(request,str.length());} );

        assertResult( 5, resultF );
        assertCallbackWasCalledFromCurrentThread();
        assertEquals( "abc", resultF.toString() );
    }

    @Test
    public void flatMapResult_returnFailure_expectFailure() {
        Failure         failure = new MockFailure("junit");
        Future<Integer> resultF = future.flatMapResult( (r,str) -> {recordCallbackCall(); return Future.failure(request,failure);} );

        assertFailure( failure, resultF );
        assertCallbackWasCalledFromCurrentThread();
    }

    @Test
    public void flatMapResult_throwException_expectFailure() {
        Exception       ex      = new IllegalArgumentException("ex");
        Future<Integer> resultF = future.flatMapResult( (r,str) -> {recordCallbackCall(); return throwException(ex);} );

        assertFailure( new ExceptionFailure(ex), resultF );
        assertCallbackWasCalledFromCurrentThread();
    }

    @Test
    public void flatMapResult_requestHasFinished_expectTimeout() {
        request.markCompleted();

        TimeoutFailure expectation = new TimeoutFailure( request, future );
        Future<Integer> resultF = future.flatMapResult( (r,v) -> {recordCallbackCall(); return Future.successful(request,v.length());} );

        assertFailure( expectation, resultF );
        assertCallbackWasNotCalled();
    }

// public Future<T> recover( Function1<Failure, T> recoveryFunction );

    @Test
    public void recover_expectRecoverCallbackToNotBeInvoked() {
        Future<String> resultF = future.recover( (r,f) -> {recordCallbackCall(); return "recovered";} );

        assertResult( "hello", resultF );
        assertCallbackWasNotCalled();
    }

    @Test
    public void withDescriptionThenRecover_expectDescriptionToCarryOVer() {
        Future<String> resultF = future.withDescription( "abc" )
            .recover( (r,f) -> {recordCallbackCall(); return "recovered";} );

        assertResult( "hello", resultF );
        assertCallbackWasNotCalled();
        assertEquals( "abc", resultF.toString() );
    }

    @Test
    public void recover_requestHasFinished_expectRecoverNotToBeRun() {
        request.markCompleted();

        Future<String> resultF = future.recover( (r,f) -> {recordCallbackCall(); return "42";} );

        assertResult( "hello", resultF );
        assertCallbackWasNotCalled();
    }

// public Future<T> flatRecover( Function1<Failure, Future<T>> recoveryFunction );

    @Test
    public void flatRecover_expectRecoverCallbackToNotBeInvoked() {
        Future<String> resultF = future.flatRecover( (r,f) -> {recordCallbackCall(); return Future.successful(request,"recovered");} );

        assertResult( "hello", resultF );
        assertCallbackWasNotCalled();
    }

    @Test
    public void withDescriptionThenFlatRecover_expectDescriptionToCarryOver() {
        Future<String> resultF = future.withDescription( "abc" )
            .flatRecover( (r,f) -> {recordCallbackCall(); return Future.successful(request,"recovered");} );

        assertResult( "hello", resultF );
        assertCallbackWasNotCalled();
        assertEquals( "abc", resultF.toString() );
    }

    @Test
    public void flatRecover_requestHasFinished_expectRecoverNotToBeRun() {
        request.markCompleted();

        Future<String> resultF = future.flatRecover( (r,f) -> {recordCallbackCall(); return Future.successful(request,"42");} );

        assertResult( "hello", resultF );
        assertCallbackWasNotCalled();
    }


// public Future<T> mapFailure( Function1<Failure, Failure> mappingFunction );

    @Test
    public void mapFailure_expectRecoverCallbackToNotBeInvoked() {
        Future<String> resultF = future.mapFailure( (r,f) -> {recordCallbackCall(); return f;} );

        assertResult( "hello", resultF );
        assertCallbackWasNotCalled();
    }

    @Test
    public void withDescriptionThenMapFailure_expectDescriptionToCarryOver() {
        Future<String> resultF = future.withDescription( "abc" )
            .mapFailure( (r,f) -> {recordCallbackCall(); return f;} );

        assertResult( "hello", resultF );
        assertCallbackWasNotCalled();
        assertEquals( "abc", resultF.toString() );
    }

    @Test
    public void mapFailure_requestHasFinished_expectRecoverNotToBeRun() {
        request.markCompleted();

        Future<String> resultF = future.mapFailure( (r,f) -> {recordCallbackCall(); return f;} );

        assertResult( "hello", resultF );
        assertCallbackWasNotCalled();
    }

// public Future<T> flatMapFailure( Function1<Failure, Failure> mappingFunction );

    @Test
    public void flatMapFailure_expectRecoverCallbackToNotBeInvoked() {
        Future<String> resultF = future.flatMapFailure( (r,f) -> {recordCallbackCall(); return Future.failure(request,f);} );

        assertResult( "hello", resultF );
        assertCallbackWasNotCalled();
    }

    @Test
    public void withDescriptionThenFlatMapFailure_expectDescriptionToCarryOver() {
        Future<String> resultF = future.withDescription( "abc" )
            .flatMapFailure( (r,f) -> {recordCallbackCall(); return Future.failure(request,f);} );

        assertResult( "hello", resultF );
        assertCallbackWasNotCalled();
        assertEquals( "abc", resultF.toString() );
    }

    @Test
    public void flatMapFailure_requestHasFinished_expectRecoverNotToBeRun() {
        request.markCompleted();

        Future<String> resultF = future.flatMapFailure( (r,f) -> {recordCallbackCall(); return Future.failure(r,f);} );

        assertResult( "hello", resultF );
        assertCallbackWasNotCalled();
    }

// public Future<T> onSuccess( VoidFunction1<T> callback );

    @Test
    public void onSuccess_expectCallbackWasCalled() {
        Future<String> resultF = future.onSuccess( (r,f) -> recordCallbackCall() );

        assertResult( "hello", resultF );
        assertCallbackWasCalledFromCurrentThread();
    }

    @Test
    public void withDescriptionThenOnSuccess_expectDescriptionToCarryOver() {
        Future<String> resultF = future.withDescription( "abc" )
            .onSuccess( (r,f) -> recordCallbackCall() );

        assertResult( "hello", resultF );
        assertCallbackWasCalledFromCurrentThread();
        assertEquals( "abc", resultF.toString() );
    }

    @Test
    public void onSuccessThrowsException_expectErrorToBeLogged() {
        Exception      ex      = new IllegalArgumentException("ex");
        Future<String> resultF = future.onSuccess( (r,f) -> {recordCallbackCall(); throwException(ex);} );

        assertResult( "hello", resultF );
        assertCallbackWasCalledFromCurrentThread();

        capturingLogWriter.assertContainsMessage( "A future's side effect threw the following exception: java.lang.IllegalArgumentException: ex" );
    }

    @Test
    public void onSuccess_requestHasFinished_expectCallbackToBeCalled() {
        request.markCompleted();

        Future<String> resultF = future.onSuccess( (r,f) -> recordCallbackCall() );

        assertResult( "hello", resultF );
        assertCallbackWasCalledFromCurrentThread();
    }

// public Future<T> onFailure( VoidFunction1<Failure> callback );

    @Test
    public void onFailure_expectCallbackWasNotCalled() {
        Future<String> resultF = future.onFailure( (r,f) -> recordCallbackCall() );

        assertResult( "hello", resultF );
        assertCallbackWasNotCalled();
    }

    @Test
    public void withDescriptionThenOnFailure_expectDescriptionToCarryOver() {
        Future<String> resultF = future.withDescription( "abc" )
            .onFailure( (r,f) -> recordCallbackCall() );

        assertResult( "hello", resultF );
        assertCallbackWasNotCalled();
        assertEquals( "abc", resultF.toString() );
    }

    @Test
    public void onFailure_requestHasFinished_expectCallbackToNotBeCalled() {
        request.markCompleted();

        Future<String> resultF = future.onFailure( (r,f) -> recordCallbackCall() );

        assertResult( "hello", resultF );
        assertCallbackWasNotCalled();
    }

// public default Future<T> onCompletion( VoidFunction1<T> onSuccess, VoidFunction1<Failure> onFailure ) {

    @Test
    public void onCompletion_expectOnSuccessToBeInvoked() {
        AtomicReference<String> flag1 = new AtomicReference<>();
        AtomicReference<Failure> flag2 = new AtomicReference<>();

        future.onCompletion( (r,v) -> flag1.set(v), (r,f) -> flag2.set(f) );

        assertEquals( "hello", flag1.get() );
        assertNull( flag2.get() );
    }

    @Test
    public void withDescriptionThenOnCompletion_expectDescriptionToCarryOver() {
        AtomicReference<String>  flag1 = new AtomicReference<>();
        AtomicReference<Failure> flag2 = new AtomicReference<>();

        Future<String> resultF = future.withDescription( "abc" )
            .onCompletion( (r,v) -> flag1.set(v), (r,f) -> flag2.set(f) );

        assertEquals( "hello", flag1.get() );
        assertNull( flag2.get() );
        assertEquals( "abc", resultF.toString() );
    }

// public default Future<T> onCompletion( Executor executor, VoidFunction1<T> onSuccess, VoidFunction1<Failure> onFailure );

    @Test
    public void onCompletionWithExecutor_expectOnSuccessToBeInvoked() {
        AtomicReference<String>  flag1 = new AtomicReference<>();
        AtomicReference<Failure> flag2 = new AtomicReference<>();

        future.onCompletion( UseCallingThreadExecutor.INSTANCE, (r,v) -> flag1.set(v), (r,f) -> flag2.set(f) );

        assertEquals( "hello", flag1.get() );
        assertNull( flag2.get() );
    }


    @Test
    public void equalsContract() {
        if ( futureType != Promise.class ) {
            EqualsVerifier.forClass( futureType )
                .suppress( Warning.STRICT_INHERITANCE )
                .withPrefabValues( Request.class, request, env.createRequest("equals-verifier") )
                .verify();
        }
    }

    @Test
    public void testToString() {
        assertEquals( "Future(hello)", future.toString() );
    }

}
