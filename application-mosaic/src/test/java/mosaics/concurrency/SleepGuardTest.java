package mosaics.concurrency;


import mosaics.cli.Env;
import mosaics.cli.requests.Request;
import mosaics.junit.JMAssertions;
import mosaics.lang.Backdoor;
import mosaics.lang.threads.STWDetector;
import mosaics.lang.time.DTM;
import mosaics.logging.writers.CapturingLogWriter;
import org.junit.jupiter.api.Test;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;



public class SleepGuardTest extends JMAssertions {

    private CapturingLogWriter capturingLogWriter = new CapturingLogWriter();
    private Env                env = Env.createTestEnv(new DTM(2020,3,10, 1,30), capturingLogWriter);
    private Request            request = new Request(env);


    @Test
    public void sleep10() {
        SleepGuard guard = new SleepGuard( "junit" );

        long duration = STWDetector.global().time( () -> guard.sleep(request, 10) );

        assertTrue( duration >= 10 && duration < 200, duration+"ms was out of range" );
    }

    @Test
    public void sleep15() {
        SleepGuard guard = new SleepGuard( "junit" );

        long duration = STWDetector.global().time( () -> guard.sleep(request, 15) );

        assertTrue( duration >= 15 && duration < 50, duration+"ms was out of range" );
    }

    @Test
    public void sleep100_withMax10_expect10() {
        SleepGuard guard = new SleepGuard( "junit", 10 );

        long duration = STWDetector.global().time( () -> guard.sleep(request, 100) );

        assertTrue( duration >= 10 && duration < 50, duration+"ms was out of range" );
    }

    @Test
    public void interruptSleep_expectLogMessageAndWakenUpEarly() throws InterruptedException {
        SleepGuard     guard = new SleepGuard( "junit" );
        CountDownLatch latch = new CountDownLatch( 1 );

        Thread thread = new Thread( () -> {
            guard.sleep( request, 10_000 );

            latch.countDown();
        }, "interruptSleep_expectLogMessageAndWakenUpEarly");

        thread.start();
        Backdoor.sleep( 100 );
        thread.interrupt();

        latch.await( 10, TimeUnit.SECONDS );

        capturingLogWriter.dumpAll();
//        List<Event> expectedMessages = Collections.singletonList(
//            new ServiceInterruptedEvent("junit")
//        );
//
//        assertEquals( expectedMessages, log.getLoggedMessages(ServiceInterruptedEvent.class) );
    }

}
