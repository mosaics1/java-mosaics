package mosaics.concurrency;

import lombok.Value;
import mosaics.fp.FP;
import mosaics.fp.Failure;
import mosaics.fp.collections.FPOption;


@Value
public class MockFailure implements Failure {

    private String msg;

    public Throwable toException() {
        return new IllegalStateException(msg);
    }

    public <T extends Throwable> FPOption<T> toException( Class<T> type ) {
        return FP.emptyOption();
    }

}
