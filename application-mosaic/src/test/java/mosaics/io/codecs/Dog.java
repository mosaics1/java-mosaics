package mosaics.io.codecs;


import lombok.Value;


/**
 *
 */
@Value
public class Dog implements Animal {
    public String name;

    public Dog( String name ) {
        this.name = name;
    }
}
