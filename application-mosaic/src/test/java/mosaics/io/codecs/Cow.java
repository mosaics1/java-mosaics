package mosaics.io.codecs;

import lombok.Value;


@Value
public class Cow  implements Animal {
    public String name;

    public Cow( String name ) {
        this.name = name;
    }
}
