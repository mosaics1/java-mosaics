package mosaics.io.codecs;

import mosaics.fp.FP;
import mosaics.fp.collections.FPOption;
import mosaics.io.resources.MimeType;
import mosaics.utils.MapUtils;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;


/**
 *
 */
public class ObjectCodecsBuilderTest {

    private ObjectCodecs codecs = new ObjectCodecsBuilder()
        .supportsGson()
        .supportsHtml()
        .supportsText()
        .identifyInheritanceTreeByPreRegisteredIdentifiers( Animal.class, "animal", MapUtils.asMap(Cow.class,"cow", Dog.class, "dog") )
        .create();

    private ObjectCodec jsonCodec = codecs.getCodecFor( MimeType.JSON ).get();


    @Test
    public void jsonSupport() {
        String json = jsonCodec.toString( new Dog("Harvey"), Animal.class );

        assertEquals( "{\"name\":\"Harvey\",\"animal\":\"dog\"}", json );

        assertEquals( new Dog("Harvey"), jsonCodec.fromString(json, Animal.class).getResult() );
    }


// getCodecForHttpAcceptHeader

    @Test
    public void givenBlankAcceptHeader_callGetCodecForHttpAcceptHeader_expectNoMatch() {
        FPOption<ObjectCodec> actual   = codecs.getCodecForHttpAcceptHeader( "  " );
        FPOption<ObjectCodec> expected = FP.emptyOption();

        assertSame( expected, actual );
    }

    @Test
    public void givenSingleSupportedMimeTypeInAcceptHeader_callGetCodecForHttpAcceptHeader_expectMatchingObjectCodecBack() {
        ObjectCodec actual   = codecs.getCodecForHttpAcceptHeader( "text/html" ).get();
        ObjectCodec expected = codecs.getCodecFor( MimeType.HTML ).get();

        assertSame( expected, actual );
    }

    @Test
    public void givenSingleUnsupportedMimeTypeInAcceptHeader_callGetCodecForHttpAcceptHeader_expectNoCodec() {
        FPOption<ObjectCodec> actual   = codecs.getCodecForHttpAcceptHeader( "fake/123" );
        FPOption<ObjectCodec> expected = FP.emptyOption();

        assertSame( expected, actual );
    }

    @Test
    public void givenTwoSupportedMimeTypesInAcceptHeader_callGetCodecForHttpAcceptHeader_expectFirstMatch() {
        ObjectCodec actual   = codecs.getCodecForHttpAcceptHeader( "text/html,text/plain" ).get();
        ObjectCodec expected = codecs.getCodecFor( MimeType.HTML ).get();

        assertSame( expected, actual );
    }

    @Test
    public void givenTwoSupportedMimeTypesInAcceptHeaderWhereSecondHasHigherQValue_callGetCodecForHttpAcceptHeader_expectMatchMatch() {
        ObjectCodec actual   = codecs.getCodecForHttpAcceptHeader( "text/html;q=0.5,text/plain" ).get();
        ObjectCodec expected = codecs.getCodecFor( MimeType.TEXT ).get();

        assertSame( expected, actual );
    }

    @Test
    public void givenWildCardMimeType_callGetCodecForHttpAcceptHeader_expectMatch() {
        ObjectCodec actual   = codecs.getCodecForHttpAcceptHeader( "text/*" ).get();
        ObjectCodec expected = codecs.getCodecFor( MimeType.HTML ).get();

        assertSame( expected, actual );
    }

    @Test
    public void givenFullWildCardMimeType_callGetCodecForHttpAcceptHeader_expectFirstCodec() {
        ObjectCodec actual   = codecs.getCodecForHttpAcceptHeader( "*/*" ).get();
        ObjectCodec expected = codecs.getCodecFor( MimeType.JSON ).get();

        assertSame( expected, actual );
    }

}
