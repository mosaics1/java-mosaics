package mosaics.io.codecs.html;

import mosaics.fp.collections.FPList;
import mosaics.fp.collections.FPOption;
import mosaics.io.resources.MimeType;
import mosaics.lang.Secret;
import mosaics.utils.MapUtils;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.assertEquals;


/**
 *
 */
public class HtmlObjectCodec_defaultOutputTests {

    private HtmlObjectCodec codec = new HtmlObjectCodec();


    @Test
    public void mimeTypes() {
        assertEquals( MimeType.HTML, codec.getCodecMimeType() );
        assertEquals( "text/html", MimeType.HTML.getMimeType() );
    }


// BOOLEAN

    @Test
    public void falseBoolean() {
        ObjWithBooleanField bean = new ObjWithBooleanField(false);

        assertHtmlEquals(
            bean,
            "<div class=\"jcpojo jcname_ObjWithBooleanField\">",
            "  <div class=\"jcprop jcprop_ObjWithBooleanField_flag\">",
            "    <div class=\"jcpropkey jckey_ObjWithBooleanField_flag\">flag</div>",
            "    <div class=\"jcpropvalue jcvalue_ObjWithBooleanField_flag\">false</div>",
            "  </div>",
            "</div>"
        );
    }

    @Test
    public void trueBoolean() {
        ObjWithBooleanField bean = new ObjWithBooleanField(true);

        assertHtmlEquals(
            bean,
            "<div class=\"jcpojo jcname_ObjWithBooleanField\">",
            "  <div class=\"jcprop jcprop_ObjWithBooleanField_flag\">",
            "    <div class=\"jcpropkey jckey_ObjWithBooleanField_flag\">flag</div>",
            "    <div class=\"jcpropvalue jcvalue_ObjWithBooleanField_flag\">true</div>",
            "  </div>",
            "</div>"
        );
    }


// BYTE

    @Test
    public void byteValue0() {
        ObjWithByteField bean = new ObjWithByteField( (byte) 0 );

        assertHtmlEquals(
            bean,
            "<div class=\"jcpojo jcname_ObjWithByteField\">",
            "  <div class=\"jcprop jcprop_ObjWithByteField_value\">",
            "    <div class=\"jcpropkey jckey_ObjWithByteField_value\">value</div>",
            "    <div class=\"jcpropvalue jcvalue_ObjWithByteField_value\">0</div>",
            "  </div>",
            "</div>"
        );
    }

    @Test
    public void byteValueMax() {
        ObjWithByteField bean = new ObjWithByteField( Byte.MAX_VALUE );

        assertHtmlEquals(
            bean,
            "<div class=\"jcpojo jcname_ObjWithByteField\">",
            "  <div class=\"jcprop jcprop_ObjWithByteField_value\">",
            "    <div class=\"jcpropkey jckey_ObjWithByteField_value\">value</div>",
            "    <div class=\"jcpropvalue jcvalue_ObjWithByteField_value\">127</div>",
            "  </div>",
            "</div>"
        );
    }

    @Test
    public void byteValueMin() {
        ObjWithByteField bean = new ObjWithByteField( Byte.MIN_VALUE );

        assertHtmlEquals(
            bean,
            "<div class=\"jcpojo jcname_ObjWithByteField\">",
            "  <div class=\"jcprop jcprop_ObjWithByteField_value\">",
            "    <div class=\"jcpropkey jckey_ObjWithByteField_value\">value</div>",
            "    <div class=\"jcpropvalue jcvalue_ObjWithByteField_value\">-128</div>",
            "  </div>",
            "</div>"
        );
    }


// SHORT

    @Test
    public void shortValue0() {
        ObjWithShortField bean = new ObjWithShortField( (short) 0 );

        assertHtmlEquals(
            bean,
            "<div class=\"jcpojo jcname_ObjWithShortField\">",
            "  <div class=\"jcprop jcprop_ObjWithShortField_value\">",
            "    <div class=\"jcpropkey jckey_ObjWithShortField_value\">value</div>",
            "    <div class=\"jcpropvalue jcvalue_ObjWithShortField_value\">0</div>",
            "  </div>",
            "</div>"
        );
    }

    @Test
    public void shortValueMax() {
        ObjWithShortField bean = new ObjWithShortField( Short.MAX_VALUE );

        assertHtmlEquals(
            bean,
            "<div class=\"jcpojo jcname_ObjWithShortField\">",
            "  <div class=\"jcprop jcprop_ObjWithShortField_value\">",
            "    <div class=\"jcpropkey jckey_ObjWithShortField_value\">value</div>",
            "    <div class=\"jcpropvalue jcvalue_ObjWithShortField_value\">32767</div>",
            "  </div>",
            "</div>"
        );
    }

    @Test
    public void shortValueMin() {
        ObjWithShortField bean = new ObjWithShortField( Short.MIN_VALUE );

        assertHtmlEquals(
            bean,
            "<div class=\"jcpojo jcname_ObjWithShortField\">",
            "  <div class=\"jcprop jcprop_ObjWithShortField_value\">",
            "    <div class=\"jcpropkey jckey_ObjWithShortField_value\">value</div>",
            "    <div class=\"jcpropvalue jcvalue_ObjWithShortField_value\">-32768</div>",
            "  </div>",
            "</div>"
        );
    }


// INT

    @Test
    public void intValue0() {
        ObjWithIntField bean = new ObjWithIntField( 0 );

        assertHtmlEquals(
            bean,
            "<div class=\"jcpojo jcname_ObjWithIntField\">",
            "  <div class=\"jcprop jcprop_ObjWithIntField_value\">",
            "    <div class=\"jcpropkey jckey_ObjWithIntField_value\">value</div>",
            "    <div class=\"jcpropvalue jcvalue_ObjWithIntField_value\">0</div>",
            "  </div>",
            "</div>"
        );
    }

    @Test
    public void intValueMax() {
        ObjWithIntField bean = new ObjWithIntField( Integer.MAX_VALUE );

        assertHtmlEquals(
            bean,
            "<div class=\"jcpojo jcname_ObjWithIntField\">",
            "  <div class=\"jcprop jcprop_ObjWithIntField_value\">",
            "    <div class=\"jcpropkey jckey_ObjWithIntField_value\">value</div>",
            "    <div class=\"jcpropvalue jcvalue_ObjWithIntField_value\">2147483647</div>",
            "  </div>",
            "</div>"
        );
    }

    @Test
    public void intValueMin() {
        ObjWithIntField bean = new ObjWithIntField( Integer.MIN_VALUE );

        assertHtmlEquals(
            bean,

            "<div class=\"jcpojo jcname_ObjWithIntField\">",
            "  <div class=\"jcprop jcprop_ObjWithIntField_value\">",
            "    <div class=\"jcpropkey jckey_ObjWithIntField_value\">value</div>",
            "    <div class=\"jcpropvalue jcvalue_ObjWithIntField_value\">-2147483648</div>",
            "  </div>",
            "</div>"
        );
    }


// LONG

    @Test
    public void longValue0() {
        ObjWithLongField bean = new ObjWithLongField( 0 );

        assertHtmlEquals(
            bean,

            "<div class=\"jcpojo jcname_ObjWithLongField\">",
            "  <div class=\"jcprop jcprop_ObjWithLongField_value\">",
            "    <div class=\"jcpropkey jckey_ObjWithLongField_value\">value</div>",
            "    <div class=\"jcpropvalue jcvalue_ObjWithLongField_value\">0</div>",
            "  </div>",
            "</div>"
        );
    }

    @Test
    public void longValueMax() {
        ObjWithLongField bean = new ObjWithLongField( Long.MAX_VALUE );

        assertHtmlEquals(
            bean,
            "<div class=\"jcpojo jcname_ObjWithLongField\">",
            "  <div class=\"jcprop jcprop_ObjWithLongField_value\">",
            "    <div class=\"jcpropkey jckey_ObjWithLongField_value\">value</div>",
            "    <div class=\"jcpropvalue jcvalue_ObjWithLongField_value\">9223372036854775807</div>",
            "  </div>",
            "</div>"
        );
    }

    @Test
    public void longValueMin() {
        ObjWithLongField bean = new ObjWithLongField( Long.MIN_VALUE );

        assertHtmlEquals(
            bean,
            "<div class=\"jcpojo jcname_ObjWithLongField\">",
            "  <div class=\"jcprop jcprop_ObjWithLongField_value\">",
            "    <div class=\"jcpropkey jckey_ObjWithLongField_value\">value</div>",
            "    <div class=\"jcpropvalue jcvalue_ObjWithLongField_value\">-9223372036854775808</div>",
            "  </div>",
            "</div>"
        );
    }


// CHAR

    @Test
    public void charValueA() {
        ObjWithCharField bean = new ObjWithCharField( 'a' );

        assertHtmlEquals(
            bean,
            "<div class=\"jcpojo jcname_ObjWithCharField\">",
            "  <div class=\"jcprop jcprop_ObjWithCharField_value\">",
            "    <div class=\"jcpropkey jckey_ObjWithCharField_value\">value</div>",
            "    <div class=\"jcpropvalue jcvalue_ObjWithCharField_value\">a</div>",
            "  </div>",
            "</div>"
        );
    }

    @Test
    public void charValuePound() {
        ObjWithCharField bean = new ObjWithCharField( '£' );

        assertHtmlEquals(
            bean,
            "<div class=\"jcpojo jcname_ObjWithCharField\">",
            "  <div class=\"jcprop jcprop_ObjWithCharField_value\">",
            "    <div class=\"jcpropkey jckey_ObjWithCharField_value\">value</div>",
            "    <div class=\"jcpropvalue jcvalue_ObjWithCharField_value\">&pound;</div>",
            "  </div>",
            "</div>"
        );
    }

    @Test
    public void charValueMin() {
        ObjWithCharField bean = new ObjWithCharField( '<' );

        assertHtmlEquals(
            bean,
            "<div class=\"jcpojo jcname_ObjWithCharField\">",
            "  <div class=\"jcprop jcprop_ObjWithCharField_value\">",
            "    <div class=\"jcpropkey jckey_ObjWithCharField_value\">value</div>",
            "    <div class=\"jcpropvalue jcvalue_ObjWithCharField_value\">&lt;</div>",
            "  </div>",
            "</div>"
        );
    }


// FLOAT

    @Test
    public void floatValue0() {
        ObjWithFloatField bean = new ObjWithFloatField( 0 );

        assertHtmlEquals(
            bean,
            "<div class=\"jcpojo jcname_ObjWithFloatField\">",
            "  <div class=\"jcprop jcprop_ObjWithFloatField_value\">",
            "    <div class=\"jcpropkey jckey_ObjWithFloatField_value\">value</div>",
            "    <div class=\"jcpropvalue jcvalue_ObjWithFloatField_value\">0.0</div>",
            "  </div>",
            "</div>"
        );
    }

    @Test
    public void floatValue10p1() {
        ObjWithFloatField bean = new ObjWithFloatField( 10.1f );

        assertHtmlEquals(
            bean,
            "<div class=\"jcpojo jcname_ObjWithFloatField\">",
            "  <div class=\"jcprop jcprop_ObjWithFloatField_value\">",
            "    <div class=\"jcpropkey jckey_ObjWithFloatField_value\">value</div>",
            "    <div class=\"jcpropvalue jcvalue_ObjWithFloatField_value\">10.1</div>",
            "  </div>",
            "</div>"
        );
    }

    @Test
    public void floatValueNeg10p1() {
        ObjWithFloatField bean = new ObjWithFloatField( -10.1f );

        assertHtmlEquals(
            bean,
            "<div class=\"jcpojo jcname_ObjWithFloatField\">",
            "  <div class=\"jcprop jcprop_ObjWithFloatField_value\">",
            "    <div class=\"jcpropkey jckey_ObjWithFloatField_value\">value</div>",
            "    <div class=\"jcpropvalue jcvalue_ObjWithFloatField_value\">-10.1</div>",
            "  </div>",
            "</div>"
        );
    }


// DOUBLE

    @Test
    public void doubleValue0() {
        ObjWithDoubleField bean = new ObjWithDoubleField( 0 );

        assertHtmlEquals(
            bean,
            "<div class=\"jcpojo jcname_ObjWithDoubleField\">",
            "  <div class=\"jcprop jcprop_ObjWithDoubleField_value\">",
            "    <div class=\"jcpropkey jckey_ObjWithDoubleField_value\">value</div>",
            "    <div class=\"jcpropvalue jcvalue_ObjWithDoubleField_value\">0.0</div>",
            "  </div>",
            "</div>"
        );
    }

    @Test
    public void doubleValue10p1() {
        ObjWithDoubleField bean = new ObjWithDoubleField( 10.1 );

        assertHtmlEquals(
            bean,
            "<div class=\"jcpojo jcname_ObjWithDoubleField\">",
            "  <div class=\"jcprop jcprop_ObjWithDoubleField_value\">",
            "    <div class=\"jcpropkey jckey_ObjWithDoubleField_value\">value</div>",
            "    <div class=\"jcpropvalue jcvalue_ObjWithDoubleField_value\">10.1</div>",
            "  </div>",
            "</div>"
        );
    }

    @Test
    public void doubleValueNeg10p1() {
        ObjWithDoubleField bean = new ObjWithDoubleField( -10.1 );

        assertHtmlEquals(
            bean,
            "<div class=\"jcpojo jcname_ObjWithDoubleField\">",
            "  <div class=\"jcprop jcprop_ObjWithDoubleField_value\">",
            "    <div class=\"jcpropkey jckey_ObjWithDoubleField_value\">value</div>",
            "    <div class=\"jcpropvalue jcvalue_ObjWithDoubleField_value\">-10.1</div>",
            "  </div>",
            "</div>"
        );
    }


// STRING

    @Test
    public void emptyStringValue() {
        ObjWithStringField bean = new ObjWithStringField( "" );

        assertHtmlEquals(
            bean,
            "<div class=\"jcpojo jcname_ObjWithStringField\">",
            "  <div class=\"jcprop jcprop_ObjWithStringField_value\">",
            "    <div class=\"jcpropkey jckey_ObjWithStringField_value\">value</div>",
            "    <div class=\"jcpropvalue jcvalue_ObjWithStringField_value\"></div>",
            "  </div>",
            "</div>"
        );
    }

    @Test
    public void nullStringValue() {
        ObjWithStringField bean = new ObjWithStringField( null );

        assertHtmlEquals(
            bean,
            "<div class=\"jcpojo jcname_ObjWithStringField\"/>"
        );
    }

    @Test
    public void stringContainingHtmlCharacters_expectItToBeEscaped() {
        ObjWithStringField bean = new ObjWithStringField( "<p>Hello £££</p>" );

        assertHtmlEquals(
            bean,
            "<div class=\"jcpojo jcname_ObjWithStringField\">",
            "  <div class=\"jcprop jcprop_ObjWithStringField_value\">",
            "    <div class=\"jcpropkey jckey_ObjWithStringField_value\">value</div>",
            "    <div class=\"jcpropvalue jcvalue_ObjWithStringField_value\">&lt;p&gt;Hello &pound;&pound;&pound;&lt;/p&gt;</div>",
            "  </div>",
            "</div>"
        );
    }


// OPTION

    @Test
    public void jdkOptionStringValue_hasValue() {
        ObjWithFPOptionStringField bean = new ObjWithFPOptionStringField( "Jen" );

        assertHtmlEquals(
            bean,
            "<div class=\"jcpojo jcname_ObjWithFPOptionStringField\">",
            "  <div class=\"jcprop jcprop_ObjWithFPOptionStringField_value\">",
            "    <div class=\"jcpropkey jckey_ObjWithFPOptionStringField_value\">value</div>",
            "    <div class=\"jcpropvalue jcvalue_ObjWithFPOptionStringField_value\">Jen</div>",
            "  </div>",
            "</div>"
        );
    }

    @Test
    public void optionStringValue_hasNoValue() {
        ObjWithFPOptionStringField bean = new ObjWithFPOptionStringField(null);

        assertHtmlEquals(
            bean,
            "<div class=\"jcpojo jcname_ObjWithFPOptionStringField\"/>"
        );
    }

    @Test
    public void optionalStringValue_hasValue() {
        ObjWithJdkOptionalStringField bean = new ObjWithJdkOptionalStringField( "Jen" );

        assertHtmlEquals(
            bean,
            "<div class=\"jcpojo jcname_ObjWithJdkOptionalStringField\">",
            "  <div class=\"jcprop jcprop_ObjWithJdkOptionalStringField_value\">",
            "    <div class=\"jcpropkey jckey_ObjWithJdkOptionalStringField_value\">value</div>",
            "    <div class=\"jcpropvalue jcvalue_ObjWithJdkOptionalStringField_value\">Jen</div>",
            "  </div>",
            "</div>"
        );
    }

    @Test
    public void optionalStringValue_hasNoValue() {
        ObjWithJdkOptionalStringField bean = new ObjWithJdkOptionalStringField(null);

        assertHtmlEquals(
            bean,
            "<div class=\"jcpojo jcname_ObjWithJdkOptionalStringField\"/>"
        );
    }


// POJO

    @Test
    public void pojoWithOneField() {
        ObjWithStringPojoField bean = new ObjWithStringPojoField( "Hello" );

        assertHtmlEquals(
            bean,
            "<div class=\"jcpojo jcname_ObjWithStringPojoField\">",
            "  <div class=\"jcprop jcprop_ObjWithStringPojoField_value\">",
            "    <div class=\"jcpropkey jckey_ObjWithStringPojoField_value\">value</div>",
            "    <div class=\"jcpropvalue jcvalue_ObjWithStringPojoField_value\">",
            "      <div class=\"jcpojo jcname_StringPojo\">",
            "        <div class=\"jcprop jcprop_StringPojo_name\">",
            "          <div class=\"jcpropkey jckey_StringPojo_name\">name</div>",
            "          <div class=\"jcpropvalue jcvalue_StringPojo_name\">Hello</div>",
            "        </div>",
            "      </div>",
            "    </div>",
            "  </div>",
            "</div>"
        );
    }

    @Test
    public void pojoWithTwoFields() {
        ObjWithStringStringPojoField bean = new ObjWithStringStringPojoField( "Jim", "Super Hero" );

        assertHtmlEquals(
            bean,
            "<div class=\"jcpojo jcname_ObjWithStringStringPojoField\">",
            "  <div class=\"jcprop jcprop_ObjWithStringStringPojoField_value\">",
            "    <div class=\"jcpropkey jckey_ObjWithStringStringPojoField_value\">value</div>",
            "    <div class=\"jcpropvalue jcvalue_ObjWithStringStringPojoField_value\">",
            "      <div class=\"jcpojo jcname_StringStringPojo\">",
            "        <div class=\"jcprop jcprop_StringStringPojo_name\">",
            "          <div class=\"jcpropkey jckey_StringStringPojo_name\">name</div>",
            "          <div class=\"jcpropvalue jcvalue_StringStringPojo_name\">Jim</div>",
            "        </div>",
            "        <div class=\"jcprop jcprop_StringStringPojo_description\">",
            "          <div class=\"jcpropkey jckey_StringStringPojo_description\">description</div>",
            "          <div class=\"jcpropvalue jcvalue_StringStringPojo_description\">Super Hero</div>",
            "        </div>",
            "      </div>",
            "    </div>",
            "  </div>",
            "</div>"
        );
    }


// ARRAYS

    @Test
    public void nullArray() {
        ObjWithStringArrayField bean = new ObjWithStringArrayField(null );

        assertHtmlEquals( bean, "<div class=\"jcpojo jcname_ObjWithStringArrayField\"/>" );
    }

    @Test
    public void emptyArray() {
        ObjWithStringArrayField bean = new ObjWithStringArrayField( new String[] {} );

        assertHtmlEquals(
            bean,
            "<div class=\"jcpojo jcname_ObjWithStringArrayField\"/>"
        );
    }

    @Test
    public void arrayWithOneValue() {
        ObjWithStringArrayField bean = new ObjWithStringArrayField( new String[] {"Hello"} );

        assertHtmlEquals(
            bean,
            "<div class=\"jcpojo jcname_ObjWithStringArrayField\">",
            "  <div class=\"jcprop jcprop_ObjWithStringArrayField_value\">",
            "    <div class=\"jcpropkey jckey_ObjWithStringArrayField_value\">value</div>",
            "    <div class=\"jcpropvalue jcvalue_ObjWithStringArrayField_value\">",
            "      <ol>",
            "        <li>Hello</li>",
            "      </ol>",
            "    </div>",
            "  </div>",
            "</div>"
        );
    }

    @Test
    public void arrayWithTwoValues() {
        ObjWithStringArrayField bean = new ObjWithStringArrayField( new String[] {"Hello", "Jenni"} );

        assertHtmlEquals(
            bean,
            "<div class=\"jcpojo jcname_ObjWithStringArrayField\">",
            "  <div class=\"jcprop jcprop_ObjWithStringArrayField_value\">",
            "    <div class=\"jcpropkey jckey_ObjWithStringArrayField_value\">value</div>",
            "    <div class=\"jcpropvalue jcvalue_ObjWithStringArrayField_value\">",
            "      <ol>",
            "        <li>Hello</li>",
            "        <li>Jenni</li>",
            "      </ol>",
            "    </div>",
            "  </div>",
            "</div>"
        );
    }

    @Test
    public void arrayContainingAnArray() {
        ObjWithStringArrayOfArraysField bean = new ObjWithStringArrayOfArraysField( new String[] {"Hello", "Jenni"} );

        assertHtmlEquals(
            bean,
            "<div class=\"jcpojo jcname_ObjWithStringArrayOfArraysField\">",
            "  <div class=\"jcprop jcprop_ObjWithStringArrayOfArraysField_value\">",
            "    <div class=\"jcpropkey jckey_ObjWithStringArrayOfArraysField_value\">value</div>",
            "    <div class=\"jcpropvalue jcvalue_ObjWithStringArrayOfArraysField_value\">",
            "      <ol>",
            "        <li>",
            "          <ol>",
            "            <li>Hello</li>",
            "            <li>Jenni</li>",
            "          </ol>",
            "        </li>",
            "      </ol>",
            "    </div>",
            "  </div>",
            "</div>"
        );
    }


// MAPS

    @Test
    public void nullMap() {
        ObjWithMapField bean = new ObjWithMapField(null);

        assertHtmlEquals( bean, "<div class=\"jcpojo jcname_ObjWithMapField\"/>" );
    }

    @Test
    public void emptyMap() {
        ObjWithMapField bean = new ObjWithMapField(new HashMap());

        assertHtmlEquals(
            bean,
            "<div class=\"jcpojo jcname_ObjWithMapField\"/>"
        );
    }

    @Test
    public void singleEntryMap() {
        ObjWithMapField bean = new ObjWithMapField( MapUtils.asMap("A", "1"));

        assertHtmlEquals(
            bean,
            "<div class=\"jcpojo jcname_ObjWithMapField\">",
            "  <div class=\"jcprop jcprop_ObjWithMapField_value\">",
            "    <div class=\"jcpropkey jckey_ObjWithMapField_value\">value</div>",
            "    <div class=\"jcpropvalue jcvalue_ObjWithMapField_value\">",
            "      <dl>",
            "        <dt>A</dt>",
            "        <dd>1</dd>",
            "      </dl>",
            "    </div>",
            "  </div>",
            "</div>"
        );
    }

    @Test
    public void singleEntryMapContainingANullValue() {
        ObjWithMapField bean = new ObjWithMapField( MapUtils.asMap("A", null));

        assertHtmlEquals(
            bean,
            "<div class=\"jcpojo jcname_ObjWithMapField\"/>"
        );
    }

    @Test
    public void twoEntryMap() {
        ObjWithMapField bean = new ObjWithMapField( MapUtils.asMap("A", "1", "B", "2"));

        assertHtmlEquals(
            bean,
            "<div class=\"jcpojo jcname_ObjWithMapField\">",
            "  <div class=\"jcprop jcprop_ObjWithMapField_value\">",
            "    <div class=\"jcpropkey jckey_ObjWithMapField_value\">value</div>",
            "    <div class=\"jcpropvalue jcvalue_ObjWithMapField_value\">",
            "      <dl>",
            "        <dt>A</dt>",
            "        <dd>1</dd>",
            "        <dt>B</dt>",
            "        <dd>2</dd>",
            "      </dl>",
            "    </div>",
            "  </div>",
            "</div>"
        );
    }


// LIST

    @Test
    public void nullList() {
        ObjWithStringListField bean = new ObjWithStringListField(null );

        assertHtmlEquals( bean, "<div class=\"jcpojo jcname_ObjWithStringListField\"/>" );
    }

    @Test
    public void emptyList() {
        ObjWithStringListField bean = new ObjWithStringListField( new ArrayList() );

        assertHtmlEquals(
            bean,
            "<div class=\"jcpojo jcname_ObjWithStringListField\"/>"
        );
    }

    @Test
    public void listWithOneValue() {
        ObjWithStringListField bean = new ObjWithStringListField( singletonList( "Hello" ) );

        assertHtmlEquals(
            bean,
            "<div class=\"jcpojo jcname_ObjWithStringListField\">",
            "  <div class=\"jcprop jcprop_ObjWithStringListField_value\">",
            "    <div class=\"jcpropkey jckey_ObjWithStringListField_value\">value</div>",
            "    <div class=\"jcpropvalue jcvalue_ObjWithStringListField_value\">",
            "      <ol>",
            "        <li>Hello</li>",
            "      </ol>",
            "    </div>",
            "  </div>",
            "</div>"
        );
    }

    @Test
    public void listWithTwoValues() {
        ObjWithStringListField bean = new ObjWithStringListField( Arrays.asList("Hello", "Jenni") );

        assertHtmlEquals(
            bean,
            "<div class=\"jcpojo jcname_ObjWithStringListField\">",
            "  <div class=\"jcprop jcprop_ObjWithStringListField_value\">",
            "    <div class=\"jcpropkey jckey_ObjWithStringListField_value\">value</div>",
            "    <div class=\"jcpropvalue jcvalue_ObjWithStringListField_value\">",
            "      <ol>",
            "        <li>Hello</li>",
            "        <li>Jenni</li>",
            "      </ol>",
            "    </div>",
            "  </div>",
            "</div>"
        );
    }

    @Test
    public void ListContainingAList() {
        ObjWithStringListOfListsField bean = new ObjWithStringListOfListsField( Arrays.asList(Arrays.asList("Hello", "Jenni")) );

        assertHtmlEquals(
            bean,
            "<div class=\"jcpojo jcname_ObjWithStringListOfListsField\">",
            "  <div class=\"jcprop jcprop_ObjWithStringListOfListsField_value\">",
            "    <div class=\"jcpropkey jckey_ObjWithStringListOfListsField_value\">value</div>",
            "    <div class=\"jcpropvalue jcvalue_ObjWithStringListOfListsField_value\">",
            "      <ol>",
            "        <li>",
            "          <ol>",
            "            <li>Hello</li>",
            "            <li>Jenni</li>",
            "          </ol>",
            "        </li>",
            "      </ol>",
            "    </div>",
            "  </div>",
            "</div>"
        );
    }

    @Test
    public void secret() {
        ObjWithSecretField bean = new ObjWithSecretField( new Secret("foo") );

        assertHtmlEquals(
            bean,

            "<div class=\"jcpojo jcname_ObjWithSecretField\">",
            "  <div class=\"jcprop jcprop_ObjWithSecretField_value\">",
            "    <div class=\"jcpropkey jckey_ObjWithSecretField_value\">value</div>",
            "    <div class=\"jcpropvalue jcvalue_ObjWithSecretField_value\">********</div>",
            "  </div>",
            "</div>"
        );
    }



    private void assertHtmlEquals( Object v, String...expectedHtml ) {
        FPList<String> t = FPList.wrapArray(expectedHtml).map( line -> "    "+line).toFPList();

        FPList<String> prefix = FPList.wrapArray( "<html>", "  <body>" );
        FPList<String> postfix = FPList.wrapArray( "  </body>", "</html>" );


        String expected = prefix.and(t).and(postfix).mkString("\n");
        String actual   = codec.toString( v, v.getClass() );

        assertEquals( expected, actual );
    }


    @SuppressWarnings("unused")
    public static class ObjWithBooleanField {
        private boolean flag;

        public ObjWithBooleanField( boolean v ) {
            this.flag = v;
        }

        public boolean hasFlag() {
            return flag;
        }
    }

    @SuppressWarnings("unused")
    public static class ObjWithByteField {
        private byte value;

        public ObjWithByteField( byte v ) {
            this.value = v;
        }

        public byte getValue() {
            return value;
        }
    }

    @SuppressWarnings("unused")
    public static class ObjWithShortField {
        private short value;

        public ObjWithShortField( short v ) {
            this.value = v;
        }

        public short getValue() {
            return value;
        }
    }

    @SuppressWarnings("unused")
    public static class ObjWithIntField {
        private int value;

        public ObjWithIntField( int v ) {
            this.value = v;
        }

        public int getValue() {
            return value;
        }
    }

    @SuppressWarnings("unused")
    public static class ObjWithLongField {
        private long value;

        public ObjWithLongField( long v ) {
            this.value = v;
        }

        public long getValue() {
            return value;
        }
    }

    @SuppressWarnings("unused")
    public static class ObjWithCharField {
        private char value;

        public ObjWithCharField( char v ) {
            this.value = v;
        }

        public char getValue() {
            return value;
        }
    }

    @SuppressWarnings("unused")
    public static class ObjWithFloatField {
        private float value;

        public ObjWithFloatField( float v ) {
            this.value = v;
        }

        public float getValue() {
            return value;
        }
    }

    @SuppressWarnings("unused")
    public static class ObjWithDoubleField {
        private double value;

        public ObjWithDoubleField( double v ) {
            this.value = v;
        }

        public double getValue() {
            return value;
        }
    }

    @SuppressWarnings("unused")
    public static class ObjWithStringField {
        private String value;

        public ObjWithStringField( String v ) {
            this.value = v;
        }

        public String getValue() {
            return value;
        }
    }

    @SuppressWarnings("unused")
    public static class ObjWithFPOptionStringField {
        private FPOption<String> value;

        public ObjWithFPOptionStringField( String v ) {
            this.value = FPOption.of(v);
        }

        public FPOption<String> getValue() {
            return value;
        }
    }

    @SuppressWarnings("unused")
    public static class ObjWithJdkOptionalStringField {
        private Optional<String> value;

        public ObjWithJdkOptionalStringField( String v ) {
            this.value = Optional.ofNullable(v);
        }

        public Optional<String> getValue() {
            return value;
        }
    }

    @SuppressWarnings("unused")
    public static class StringPojo {
        private String name;

        public StringPojo( String v ) {
            this.name = v;
        }

        public String getName() {
            return name;
        }
    }

    @SuppressWarnings("unused")
    public static class ObjWithStringPojoField {
        private StringPojo value;

        public ObjWithStringPojoField( String v ) {
            this.value = new StringPojo(v);
        }

        public StringPojo getValue() {
            return value;
        }
    }

    @SuppressWarnings("unused")
    public static class StringStringPojo {
        private String name;
        private String description;

        public StringStringPojo( String name, String desc ) {
            this.name        = name;
            this.description = desc;
        }

        public String getName() {
            return name;
        }

        public String getDescription() {
            return description;
        }
    }

    @SuppressWarnings("unused")
    public static class ObjWithStringStringPojoField {
        private StringStringPojo value;

        public ObjWithStringStringPojoField( String name, String desc ) {
            this.value = new StringStringPojo(name,desc);
        }

        public StringStringPojo getValue() {
            return value;
        }
    }

    @SuppressWarnings("unused")
    public static class ObjWithStringArrayField {
        private String[] value;

        public ObjWithStringArrayField( String[] values ) {
            this.value = values;
        }

        public String[] getValue() {
            return value;
        }
    }

    @SuppressWarnings("unused")
    public static class ObjWithStringArrayOfArraysField {
        private String[][] value;

        public ObjWithStringArrayOfArraysField( String[]...values ) {
            this.value = values;
        }

        public String[][] getValue() {
            return value;
        }
    }

    @SuppressWarnings("unused")
    public static class ObjWithMapField {
        private Map value;

        public ObjWithMapField( Map values ) {
            this.value = values;
        }

        public Map getValue() {
            return value;
        }
    }

    @SuppressWarnings("unused")
    public static class ObjWithStringListField {
        private List value;

        public ObjWithStringListField( List values ) {
            this.value = values;
        }

        public List getValue() {
            return value;
        }
    }

    @SuppressWarnings("unused")
    public static class ObjWithStringListOfListsField {
        private List<List> value;

        public ObjWithStringListOfListsField( List<List> values ) {
            this.value = values;
        }

        public List<List> getValue() {
            return value;
        }
    }

    @SuppressWarnings("unused")
    public static class ObjWithSecretField {
        private Secret value;

        public ObjWithSecretField( Secret value ) {
            this.value = value;
        }

        public Secret getValue() {
            return value;
        }
    }

}
