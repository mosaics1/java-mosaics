package mosaics.io;

import mosaics.cli.Env;
import mosaics.cli.requests.Request;
import mosaics.concurrency.Scheduler;
import mosaics.io.RetryOp;
import mosaics.lang.functions.Function0WithThrows;
import mosaics.lang.functions.LongFunction0;
import mosaics.logging.writers.CapturingLogWriter;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.util.function.IntToLongFunction;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verifyNoMoreInteractions;


public class RetryOpTest {

    private CapturingLogWriter capturingLogWriter = new CapturingLogWriter();
    private Env                env                = Env.createTestEnvWithRealClock(capturingLogWriter);
    private Request            request            = env.createRequest( "junit" );
    private Scheduler          schedulerMock      = mock( Scheduler.class );
    private IntToLongFunction  retryDelayMock     = mock( IntToLongFunction.class );


    private RetryOp retryOp   = new RetryOp( schedulerMock )
        .withRetryDelayGenerator( retryDelayMock )
        .withIsRetryableClassifier( ex -> true )
        .withMaxAttemptCount( 3 )
        .withMaxDelay( Duration.ofMillis(1000) );


    @AfterEach
    public void tearDown() {
        verifyNoMoreInteractions(schedulerMock);
    }

// SYNCHRONOUS RETRIES

    @Test
    public void givenJobSucceeds_expectNoRetriesAreLogged() {
        assertEquals( "ok", retryOp.invoke( request, () -> "ok" ) );

        assertTrue( capturingLogWriter.getCapturedMessages().isEmpty() );
    }

//    @Test
//    public void givenSecondAttemptSucceeds_expectLogMessages() {
//        FPIterable<Event> expectedLogMessages = FP.wrapAll(
//            new RetryFailedWillRetryAgainEvent("EventuallySucceedsTask(1)", 1, 3, Duration.ofMillis(10), new ExceptionFailure(new IllegalStateException()))
//        );
//
//        when(retryDelayMock.applyAsLong(1)).thenReturn( 10L );
//
//        assertEquals( "ok", retryOp.invoke( request, new EventuallySucceedsTask(1)) );
//
//        assertEquals( expectedLogMessages, captureLogHandler.getLoggedMessages() );
//
//        verify( schedulerMock, times(1)).sleep( request, 10L );
//    }
//
//    @Test
//    public void givenThirdAttemptSucceeds_expectLogMessages() {
//        FPIterable<Event> expectedLogMessages = FP.wrapAll(
//            new RetryFailedWillRetryAgainEvent("EventuallySucceedsTask(2)", 1, 3, Duration.ofMillis(10), new ExceptionFailure(new IllegalStateException())),
//            new RetryFailedWillRetryAgainEvent("EventuallySucceedsTask(2)", 2, 3, Duration.ofMillis(20), new ExceptionFailure(new IllegalStateException()))
//        );
//
//        when(retryDelayMock.applyAsLong(1)).thenReturn( 10L );
//        when(retryDelayMock.applyAsLong(2)).thenReturn( 20L );
//
//        assertEquals( "ok", retryOp.invoke( request, new EventuallySucceedsTask(2)) );
//
//        assertEquals( expectedLogMessages, captureLogHandler.getLoggedMessages() );
//
//        verify( schedulerMock, times(1)).sleep( request, 10L );
//        verify( schedulerMock, times(1)).sleep( request, 20L );
//    }
//
//    @Test
//    public void givenAllAttemptsFail_expectExceptionAndLogMessages() {
//        when(retryDelayMock.applyAsLong(1)).thenReturn( 10L );
//        when(retryDelayMock.applyAsLong(2)).thenReturn( 20L );
//
//        try {
//            retryOp.invoke( request, new EventuallySucceedsTask( 3 ) );
//            fail( "Expected exception" );
//        } catch ( IllegalStateException ex ) {
//
//        }
//
//        Throwable ex = ((RetryFailedForTheLastTimeActionAbortedEvent) captureLogHandler.getLoggedMessages().get(2)).getFailure();
//        FPIterable<Event> expectedLogMessages = FP.wrapAll(
//            new RetryFailedWillRetryAgainEvent("EventuallySucceedsTask(3)", 1, 3, Duration.ofMillis(10), new ExceptionFailure(new IllegalStateException())),
//            new RetryFailedWillRetryAgainEvent("EventuallySucceedsTask(3)", 2, 3, Duration.ofMillis(20), new ExceptionFailure(new IllegalStateException())),
//            new RetryFailedForTheLastTimeActionAbortedEvent("EventuallySucceedsTask(3)",3, ex)
//        );
//
//        assertEquals( expectedLogMessages.toList().get(0), captureLogHandler.getLoggedMessages().get(0) );
//        assertEquals( expectedLogMessages.toList().get(1), captureLogHandler.getLoggedMessages().get(1) );
//        assertEquals( expectedLogMessages.toList().get(2), captureLogHandler.getLoggedMessages().get(2) );
//
//        verify( schedulerMock, times(1)).sleep( request, 10L );
//        verify( schedulerMock, times(1)).sleep( request, 20L );
//    }
//
//    @Test
//    public void attemptErrorsButTheErrorIsNotRetryable_expectExceptionOnFirstTry() {
//        FPIterable<Event> expectedLogMessages = FP.wrapAll(
//            new OpFailedWithNonRetryableExceptionEvent("EventuallySucceedsTask(3)",new ExceptionFailure(new IllegalStateException()))
//        );
//
//        retryOp = retryOp.withIsRetryableClassifier( ex -> false );
//
//        try {
//            retryOp.invoke( request, new EventuallySucceedsTask( 3 ) );
//            fail( "Expected exception" );
//        } catch ( IllegalStateException ex ) {
//
//        }
//
//        assertEquals( expectedLogMessages, captureLogHandler.getLoggedMessages() );
//    }
//
//    @Test
//    public void givenDelayGTMax_expectDelayToBeDroppedToTheMaxDelay() {
//        FPIterable<Event> expectedLogMessages = FP.wrapAll(
//            new RetryFailedWillRetryAgainEvent("EventuallySucceedsTask(1)", 1, 3, Duration.ofMillis(1000), new ExceptionFailure(new IllegalStateException()))
//        );
//
//        when(retryDelayMock.applyAsLong(1)).thenReturn( 1001L );
//
//        assertEquals( "ok", retryOp.invoke( request, new EventuallySucceedsTask(1)) );
//
//        assertEquals( expectedLogMessages, captureLogHandler.getLoggedMessages() );
//
//        verify( schedulerMock, times(1)).sleep( request, 1000L );
//    }
//
//    @Test
//    public void givenDelayLTZero_expectDelayToBeRaisedToZero() {
//        FPIterable<Event> expectedLogMessages = FP.wrapAll(
//            new RetryFailedWillRetryAgainEvent("EventuallySucceedsTask(1)", 1, 3, Duration.ofMillis(0), new ExceptionFailure(new IllegalStateException()))
//        );
//
//        when(retryDelayMock.applyAsLong(1)).thenReturn( -1L );
//
//        assertEquals( "ok", retryOp.invoke( request, new EventuallySucceedsTask(1)) );
//
//        assertEquals( expectedLogMessages, captureLogHandler.getLoggedMessages() );
//
//        verify( schedulerMock, times(1)).sleep( request, 0L );
//    }
//
//    @Test
//    public void givenDelayEQZero_expectDelayToBeZero() {
//        FPIterable<Event> expectedLogMessages = FP.wrapAll(
//            new RetryFailedWillRetryAgainEvent("EventuallySucceedsTask(1)", 1, 3, Duration.ofMillis(0), new ExceptionFailure(new IllegalStateException()))
//        );
//
//        when(retryDelayMock.applyAsLong(1)).thenReturn( 0L );
//
//        assertEquals( "ok", retryOp.invoke( request, new EventuallySucceedsTask(1)) );
//        assertEquals( expectedLogMessages, captureLogHandler.getLoggedMessages() );
//
//        verify( schedulerMock, times(1)).sleep( request, 0L );
//    }
//
//
//// default plugins
//
//    @Test
//    public void defaultRetryGenerator() {
//        // // seq: 25+rnd(50), 50+rnd(100), 100+rnd(200), ...
//        expectBetween( 25, 75, () -> RetryOp.defaultRetryGenerator(1) );
//        expectBetween( 50, 150, () -> RetryOp.defaultRetryGenerator(2) );
//        expectBetween( 100, 300, () -> RetryOp.defaultRetryGenerator(3) );
//    }
//
//    @Test
//    public void defaultIsRetryable() {
//        assertFalse( RetryOp.defaultIsRetryable(new OutOfMemoryError()) );
//        assertFalse( RetryOp.defaultIsRetryable(new IllegalStateException()) );
//        assertFalse( RetryOp.defaultIsRetryable(new IllegalArgumentException()) );
//
//        assertTrue( RetryOp.defaultIsRetryable(new IOException()) );
//        assertTrue( RetryOp.defaultIsRetryable(new SQLException()) );
//    }


    private void expectBetween( long minInc, long maxInc, LongFunction0 task ) {
        for ( int i=0; i<100; i++ ) {
            long v = task.invoke();

            assertTrue( v >= minInc, v+" >= " + minInc );
            assertTrue( v <= maxInc, v + " <= " + maxInc );
        }
    }



    private static class EventuallySucceedsTask implements Function0WithThrows<String> {
        private final String taskName;

        private int failCounter;

        public EventuallySucceedsTask( int numFails ) {
            taskName    = "EventuallySucceedsTask("+numFails+")";
            failCounter = numFails;
        }

        @Override
        public String invoke() {
            if ( failCounter > 0 ) {
                failCounter--;

                throw new IllegalStateException();
            }

            return "ok";
        }

        public String toString() {
            return taskName;
        }
    }
}
