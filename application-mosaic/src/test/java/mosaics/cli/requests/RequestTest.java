package mosaics.cli.requests;

import mosaics.cli.Env;
import mosaics.fp.FP;
import mosaics.fp.collections.maps.FPMap;
import mosaics.lang.Random;
import mosaics.lang.clock.Clock;
import mosaics.lang.time.DTM;
import mosaics.logging.LogFilter;
import mosaics.logging.LogMessage;
import mosaics.logging.LogMessageFactory;
import mosaics.logging.LogMessageProxyFactory;
import mosaics.logging.LogSeverity;
import mosaics.logging.TargetAudience;
import mosaics.logging.loglevels.UserError;
import mosaics.logging.writers.LogWriter;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;


public class RequestTest {
    private LogWriter logWriterMock = Mockito.mock(LogWriter.class);
    private Clock     clockMock     = Mockito.mock(Clock.class);
    private Random    randomMock    = Mockito.mock(Random.class);
    private Env       env           = Env.createEnv("junit-env", clockMock, logWriterMock, LogFilter.DEBUG, randomMock);
    private DTM       T0            = new DTM(2020,3,3, 10,30);

    @Nested
    public class LogTestCases {
        @Nested
        public class DirectLoggingTestCases {
            @Test
            public void createLogMessageAndLogDirectlyViaRequestLog() {
                LogMessage origMessage = new LogMessage(
                    TargetAudience.DEV,
                    LogSeverity.INFO,
                    "hello $name",
                    "name",
                    "Jim"
                );

                LogMessage expectedMessage = new LogMessage(
                    T0,
                    TargetAudience.DEV,
                    LogSeverity.INFO,
                    FP.option( "uuid1" ),
                    FP.emptyOption(),
                    "hello $name",
                    FP.emptyOption(),
                    FP.option( "DirectLoggingTestCases#createLogMessageAndLogDirectlyViaRequestLog" ),
                    FPMap.wrapMap( "name", "Jim" ),
                    false

                );

                when( clockMock.currentDTM() ).thenReturn( T0 );
                when( randomMock.nextUUID() ).thenReturn( "uuid1" );

                Request request = new Request( env );
                request.log( origMessage );

                Mockito.verify( logWriterMock ).write( expectedMessage );
                Mockito.verify( clockMock, Mockito.atLeast( 1 ) ).currentDTM();
                Mockito.verify( randomMock ).nextUUID();

                Mockito.verifyNoMoreInteractions( logWriterMock, clockMock, randomMock );
            }

            @Test
            public void givenTracerRequest_createLogMessageAndLogDirectlyViaRequestLog() {
                LogMessage origMessage = new LogMessage(
                    TargetAudience.DEV,
                    LogSeverity.INFO,
                    "hello $name",
                    "name",
                    "Jim"
                );

                LogMessage expectedMessage = new LogMessage(
                    T0,
                    TargetAudience.DEV,
                    LogSeverity.INFO,
                    FP.option( "uuid1" ),
                    FP.emptyOption(),
                    "hello $name",
                    FP.emptyOption(),
                    FP.option("DirectLoggingTestCases#givenTracerRequest_createLogMessageAndLogDirectlyViaRequestLog"),
                    FPMap.wrapMap( "name", "Jim" ),
                    true
                );

                when( clockMock.currentDTM() ).thenReturn( T0 );
                when( randomMock.nextUUID() ).thenReturn( "uuid1" );

                Request request = new Request( env ).withIsTracerRound( true );
                request.log( origMessage );

                Mockito.verify( logWriterMock ).write( expectedMessage );
                Mockito.verify( clockMock, Mockito.atLeast( 1 ) ).currentDTM();
                Mockito.verify( randomMock ).nextUUID();

                Mockito.verifyNoMoreInteractions( logWriterMock, clockMock, randomMock );
            }
        }

        @Nested
        public class LoggerTestCases {
//            @Test
//            public void createALoggerAndExpectItToCallTheLogWriterForUs() {
//                when( clockMock.currentDTM() ).thenReturn( T0 );
//                when( randomMock.nextUUID() ).thenReturn( "uuid1" );
//
//                Request request = new Request( env );
//                request.createLogger( TestLogger.class ).event1( "Jim" );
//                request.log( createLogger(TestLogger.class).event1( "Jim" ) );
//
//                LogMessage expectedMessage = new LogMessage(
//                    T0,
//                    TargetAudience.USER,
//                    LogSeverity.ERROR,
//                    FP.option( "uuid1" ),
//                    FP.option("mosaics.cli.requests.RequestTest$TestLogger#event1(java.lang.String name)"),
//                    "Hello $name",
//                    FPMap.wrapMap( "name", "Jim" ),
//                    false
//                );
//THINKING:  change the interface proxy to create LogMessages, but not to log them
//
//                Mockito.verify( logWriterMock ).write( expectedMessage );
//                Mockito.verify( clockMock, Mockito.atLeast( 1 ) ).currentDTM();
//                Mockito.verify( randomMock ).nextUUID();
//
//                Mockito.verifyNoMoreInteractions( logWriterMock, clockMock, randomMock );
//            }
        }
    }

    @Nested
    public class TimeTestCases {
        private final Env     envMock = Mockito.mock( Env.class );
        private final Request request = new Request(envMock, "id", "junit");

        @Test
        public void timeHowLongItTakesToExecuteARunnable() {
            Mockito.when(envMock.currentTimeMillis()).thenReturn(1000L,1010L);

            assertEquals(10, request.time(() -> {}) );
        }

        @Test
        public void timeHowLongItTakesToExecuteAFunction0() {
            Mockito.when(envMock.currentTimeMillis()).thenReturn(1000L,1019L);

            assertEquals(new TimedCallResult<>(19L,"hello"), request.time(() -> "hello") );
        }
    }


    public static interface TestLogMessageFactory extends LogMessageFactory {
        public static final TestLogMessageFactory INSTANCE = LogMessageProxyFactory.INSTANCE.createLogMessageFactory( TestLogMessageFactory.class );

        @UserError("Hello $name")
        public LogMessage event1( String name );
    }
}
