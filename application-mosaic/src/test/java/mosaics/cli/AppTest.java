package mosaics.cli;

import lombok.Builder;
import lombok.Value;
import mosaics.cli.requests.Request;
import mosaics.concurrency.Await;
import mosaics.fp.collections.FPOption;
import mosaics.os.OSProcess;
import mosaics.os.OSProcessBuilder;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class AppTest {

    private Env     env   = Env.createTestEnvWithRealClock();
    private Await   await = new Await(env);
    private Request req   = env.createRequest( "junit" );


    @Test
    public void givenAppWithNoArgConstructor_expectItToRun() {
        OSProcess process = new OSProcessBuilder()
            .withCommand( App1.class )
            .run(req);


        int osExitCode = await.result( req, process.toPromise() );
        assertEquals( 0, osExitCode );
    }

    @Test
    public void givenAppWithOptionalDestFlag_expectItToRun() {
        OSProcess process = new OSProcessBuilder()
            .withCommand( App2.class, "--dest=a.html" )
            .run(req);


        int osExitCode = await.result( req, process.toPromise() );

        List<String> capturedOutput = process.getStdout().toList().blockingGet();
        assertEquals( Arrays.asList("HELLO: Some(a.html)"), capturedOutput );

        assertEquals( 0, osExitCode );
    }


    public static class App1 extends App<Void> {
        public void run() {
            System.out.println("HELLO!");
        }
    }

    public static class App2 extends App<App2.MyConfig> {
        @Value
        @Builder
        public static class MyConfig {
            private FPOption<File> dest;
        }

        public void run( MyConfig config ) {
            System.out.println("HELLO: " + config.dest);
        }
    }

    @Value
    @Builder
    public static class AppConfig {
        private String  val1;
        private boolean val2;
    }

    // RunWithConfig
    // RunWithVoid
    // RunWithNoConfigDTODeclared_expectException

    public static class AppWithConfig extends App<AppConfig> {
        public void run() {
            System.out.println("HELLO");
        }
    }
}
