//package com.softwaremosaic.mosaics.cli.internals;
//
//import com.softwaremosaic.app.DebugEnv;
//import com.softwaremosaic.app.Env;
//import com.softwaremosaic.junit.JUnitMosaic;
//import com.softwaremosaic.junit.JUnitMosaicRunner;
//import com.softwaremosaic.junit.annotations.Test;
//import com.softwaremosaic.lang.Backdoor;
//import com.softwaremosaic.lang.time.DTM;
//import com.softwaremosaic.mosaics.cli.App;
//import org.junit.After;
//import org.junit.runner.RunWith;
//
//import java.util.concurrent.CountDownLatch;
//import java.util.concurrent.TimeUnit;
//
//import static org.junit.Assert.*;
//
//
///**
// * Tests for restricting the app to one running instance per data directory.
// */
//@RunWith( JUnitMosaicRunner.class )
//public class CommandLineRunner_lockFileTests {
//
//    private DebugEnv       env1              = new DebugEnv("CLApp_fileLockInMemoryTests1");
//    private DebugEnv       env2              = DebugEnv.mirrorEnv("CLApp_fileLockInMemoryTests2", env1);
//    private CountDownLatch continuationLatch = new CountDownLatch(1);
//
//
//    @After
//    public void tearDown() {
//        env1.stop();
//        env2.stop();
//    }
//
//    @Test(threadCheck=true)
//    public void givenSingleInstanceApp_startTwoApps_expectSecondOneToNotStart() {
//        env1.getClock().fixCurrentDTM( DTM.toEpoch(2020,1,1, 10,0,0) );
//
//        SingleInstanceApp app1 = new SingleInstanceApp(continuationLatch);
//        SingleInstanceApp app2 = new SingleInstanceApp(continuationLatch);
//
//        new Thread( () -> CommandLineRunner.testInvoke(env1, app1)).start();
//        app1.spinUntilAppIsRunning(env1);
//
//        new Thread( () -> CommandLineRunner.testInvoke(env2, app2) ).start();
//
//        JUnitMosaic.spinUntilTrue(3000, () -> env1.doesUserInfoContain("Application is already running, only one instance is allowed at a time."));
//
//        continuationLatch.countDown();
//
//
//        app1.spinUntilAppShutsDown(env1);
//        app2.spinUntilAppShutsDown(env2);
//    }
//
////    @Test(threadCheck=true)
////    public void givenSingleInstanceApp_startTwoAppsUsingDifferentDataDirectories_expectBothToStartFine() {
////        system1.clock.fixCurrentDTM( new DTM(2020,1,1, 10,0,0) );
////        system2.clock.fixCurrentDTM( new DTM(2020,1,1, 10,0,0) );
////
////        SingleInstanceApp app1 = new SingleInstanceApp(system1, continuationLatch);
////        SingleInstanceApp app2 = new SingleInstanceApp(system1, continuationLatch);
////
////        new Thread( () -> app1.runApp("data1") ).start();
////
////        app1.spinUntilAppIsRunning();
////
////        new Thread( () -> app2.runApp("data2") );
////
////        app2.spinUntilAppIsRunning();
////
////
////        continuationLatch.countDown();
////
////
////        app1.spinUntilAppShutsDown();
////        app2.spinUntilAppShutsDown();
////
////
////        assertFalse( system1.doesFatalAuditContain("Application is already running, only one instance is allowed at a time.") );
////        assertFalse( system2.doesFatalAuditContain("Application is already running, only one instance is allowed at a time.") );
////    }
//
//
//    public static class SingleInstanceApp extends App {
//        private final CountDownLatch continuationLatch;
//
//        protected SingleInstanceApp( CountDownLatch continuationLatch ) {
//            this.continuationLatch = continuationLatch;
//        }
//
//        protected void setup( Env env ) {
//            super.setup(env);
//
//            setUseLockFileFlag( env, ".LOCK" );
//        }
//
//        public void run( Env env ) {
//            env.stdout("app is running");
//
//            try {
//                assertTrue(continuationLatch.await(1, TimeUnit.SECONDS));
//            } catch ( InterruptedException e ) {
//                throw Backdoor.throwException(e);
//            }
//
//            env.stdout("app is shutting down");
//        }
//
//        public void spinUntilAppIsRunning( DebugEnv env ) {
//            try {
//                JUnitMosaic.spinUntilTrue(() -> env.doesUserInfoContain("app is running"));
//            } finally {
//                env.dumpLog();
//            }
//        }
//
//        public void spinUntilAppShutsDown( DebugEnv env ) {
//            JUnitMosaic.spinUntilTrue( () -> env.doesUserInfoContain("app is shutting down") );
//        }
//    }
//
//}
