package mosaics.cli.runner;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.Value;
import lombok.With;
import mosaics.cli.CommandLineArg;
import mosaics.cli.Env;
import mosaics.cli.meta.AppHelpLogMessage;
import mosaics.cli.meta.AppMeta;
import mosaics.cli.meta.AppMetaReflectionFactory;
import mosaics.cli.requests.Request;
import mosaics.fp.FP;
import mosaics.fp.collections.FPOption;
import mosaics.junit.JMAssertions;
import mosaics.junit.JMFileExtension;
import mosaics.lang.Secret;
import mosaics.lang.lifecycle.StartStoppable;
import mosaics.lang.reflection.DI;
import mosaics.lang.time.DTM;
import mosaics.logging.LogFilter;
import mosaics.logging.LogMessage;
import mosaics.logging.writers.CapturingLogWriter;
import mosaics.strings.StringCodecs;
import mosaics.strings.StringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static mosaics.cli.runner.AppResult.NO_RUN_METHOD_FOUND;


@SuppressWarnings("unused")
public class AppRunnerTest extends JMAssertions {
    private CapturingLogWriter capturingLogWriter = new CapturingLogWriter();
    private Env                env                = Env.createTestEnv( new DTM(2020,5,26,10,30), capturingLogWriter );


    @Nested
    public class DisplayHelpTestCases {
        @Test
        public void givenNoArgAppWithNoDefinedHelp_invokeHelp_expectTheAppToNotRunAndTheDefaultHelpToBeDisplayed() {
            AppRunner<NoArgApp> runner = new AppRunner<>( NoArgApp.class, env );
            NoArgApp            app    = new NoArgApp();

            AppResult result = runner.run( app, "--help" );

            assertTrue( result.isSuccess() );
            assertFalse( app.didRun );

            LogMessage actual = capturingLogWriter.getLogMessagesByType( "mosaics.cli.meta.AppHelpLogMessage" ).get(0)
                .withRequestId( FP.emptyOption() );
            LogMessage expected = getHelpMessage(NoArgApp.class)
                .withRequestDescription( FP.option("NoArgApp") );

            assertEquals( expected, actual );
        }

        private LogMessage getHelpMessage( Class<?> appClass ) {
            AppMeta appMeta = new AppMetaReflectionFactory().createAppMetaFrom( appClass, FP.emptyOption() );

            return AppHelpLogMessage.createHelpMessage( env.currentDTM(), appMeta );
        }
    }


    @Nested
    public class InvalidRunMethodTestCases {
        @Test
        public void givenAppWithNoRunMethods_expectError() {
            try {
                new AppRunner<>( NoRunMethodApp.class, env );

                Assertions.fail( "expected IllegalArgumentException" );
            } catch ( IllegalArgumentException e ) {
                assertEquals( "NoRunMethodApp is not valid.  There is no run method declared.  A valid run method is called run, returns int or void and can accept any arg that there is a valid StringCodec declared for its type.", e.getMessage() );
            }
        }

        @Test
        public void givenRunMethodThatReturnsNonVoidOrInt_expectNoRunMethodFound() {
            try {
                new AppRunner<>( HasRunMethodThatReturnsString.class, env );
                Assertions.fail( "expected exception" );
            } catch ( IllegalArgumentException ex ) {
                assertEquals(
                    "HasRunMethodThatReturnsString is not valid.  There is no run " +
                        "method declared.  A valid run method is called run, returns int or " +
                        "void and can accept any arg that there is a valid StringCodec declared " +
                        "for its type.",
                    ex.getMessage()
                );
            }
        }

        @Test
        public void givenRunMethodWithArgWhichHasNoCodec_expectExceptionFromConstructor() {
            try {
                new AppRunner<>( HasArgOfUnknownType.class, env );
                Assertions.fail("expected exception");
            } catch ( IllegalArgumentException ex ) {
                assertEquals(
                    "HasArgOfUnknownType is not valid.  There is no StringCodec for " +
                        "type 'UnknownType', see param 'unknownArg' on the run method.",
                    ex.getMessage()
                );
            }
        }

        @Test
        public void givenRunMethodWithAnOptionalArgBeforeANoneOptionalArg_expectException() {
            try {
                new AppRunner<>( HasRunMethodWithAnOptionalArgBeforeANoneOptionalArg.class, env );
                Assertions.fail("expected exception");
            } catch ( IllegalArgumentException ex ) {
                assertEquals(
                    "'HasRunMethodWithAnOptionalArgBeforeANoneOptionalArg' " +
                    "is not valid.  The run method has mandatory arg 'otherArg' after an " +
                        "optional arg.  All optional args must be declared last, order " +
                        "matters here.",
                    ex.getMessage()
                );
            }
        }

        @Test
        public void givenRunMethodWithAnFPOptionalArgBeforeANoneFPOptionalArg_expectException() {
            try {
                new AppRunner<>( HasRunMethodWithAnFPOptionalArgBeforeANoneOtionalArg.class, env );
                Assertions.fail("expected exception");
            } catch ( IllegalArgumentException ex ) {
                assertEquals(
                    "'HasRunMethodWithAnFPOptionalArgBeforeANoneOtionalArg' is not " +
                        "valid.  The run method has mandatory arg 'otherArg' after an " +
                        "optional arg.  All optional args must be declared last, order " +
                        "matters here.",
                    ex.getMessage()
                );
            }
        }

        @Test
        public void givenRunMethodWithAnOptionalWhoseParameterHasNoCodec_expectException() {
            try {
                new AppRunner<>( HasRunMethodWithAnOptionalArgThatHasNoCodec.class, env );
                Assertions.fail("expected exception");
            } catch ( IllegalArgumentException ex ) {
                assertEquals(
                    "HasRunMethodWithAnOptionalArgThatHasNoCodec is not valid.  " +
                        "There is no StringCodec for type " +
                        "'HasRunMethodWithAnOptionalArgThatHasNoCodec', see param 'oArg' on " +
                        "the run method.",
                    ex.getMessage()
                );
            }
        }

        @Test
        public void givenRunMethodWithAnFPOptionalWhoseParameterHasNoCodec_expectException() {
            try {
                new AppRunner<>( HasRunMethodWithAnFPOptionalArgThatHasNoCodec.class, env );
                Assertions.fail("expected exception");
            } catch ( IllegalArgumentException ex ) {
                assertEquals(
                    "HasRunMethodWithAnFPOptionalArgThatHasNoCodec is not valid.  " +
                        "There is no StringCodec for type " +
                        "'HasRunMethodWithAnOptionalArgThatHasNoCodec', see param 'oArg' on " +
                        "the run method.",
                    ex.getMessage()
                );
            }
        }
    }


    @Nested
    public class BasicRunNoArgsNoFlagsTestCases {
        @Test
        public void givenNoArgAppWithNoAnnotations_invoke_expectTheAppToRun() {
            AppRunner<NoArgApp> runner = new AppRunner<>( NoArgApp.class, env );
            NoArgApp app = new NoArgApp();

            AppResult result = runner.run( app );

            assertTrue( result.isSuccess() );
            assertTrue( app.didRun );
        }

        @Test
        public void givenStartStoppableApp_expendStartAndStopToBeCalled() {
            AppRunner<HasStartStoppableSupport> runner = new AppRunner<>( HasStartStoppableSupport.class, env );
            HasStartStoppableSupport app = new HasStartStoppableSupport();

            AppResult result = runner.run( app );

            assertTrue( result.isSuccess() );
            assertEquals( Arrays.asList("doStart", "run", "doStop"), app.events );
        }

        @Test
        public void givenNoArgAppWithNoAnnotations_invokeWithOneArg_expectTheAppToError() {
            AppRunner<NoArgApp> runner = new AppRunner<>( NoArgApp.class, env );
            NoArgApp app = new NoArgApp();

            AppResult result = runner.run( app, "foo" );

            assertFalse( result.isSuccess() );
            assertEquals( NO_RUN_METHOD_FOUND, result.getExitCode() );
            assertFalse( app.didRun );

            capturingLogWriter.assertContainsMessage( "No run() method found supporting args: foo" );
        }

        @Test
        public void givenNoArgAppWithNoAnnotations_invokeUsingNonLoggingRunMethod_expectTheAppToRun() {
            AppRunner<NoArgApp> runner = new AppRunner<>( NoArgApp.class, env );
            NoArgApp app = new NoArgApp();

            AppResult result = runner.run( app );

            assertTrue( result.isSuccess() );
            assertTrue( app.didRun );
        }

//        @Test
//        public void givenNoArgAppWithNoAnnotations_invokeUsingDIRunMethod_expectTheAppToRun() {
//            AppRunner<NoArgApp> runner = new AppRunner<>( NoArgApp.class, env );
//            NoArgApp app = new NoArgApp();
//            Request req = new SystemRequest();
//
//            AppResult result = runner.run( req, app, () -> new DI() );
//
//            assertTrue( result.isSuccess() );
//            assertTrue( app.didRun );
//        }
    }

    @Nested
    public class ReservedFlagNameTestCases {
        @Test
        public void givenFieldWithReservedFlagNameHelp_expectError() {
            try {
                new AppRunner<>( ReservedFlagHelpApp.class, env, FP.option(ReservedFlagHelpApp.MyConfig.class) );
                Assertions.fail( "expected IllegalArgumentException" );
            } catch ( IllegalArgumentException ex ) {
                assertEquals( "ReservedFlagHelpApp is not valid.  Flag 'help' has already been reserved for another use.", ex.getMessage() );
            }
        }

        @Test
        public void givenFieldWithReservedShortFlagNameOfQM_expectError() {
            try {
                new AppRunner<>( ReservedFlagHelpQM.class, env, FP.option(ReservedFlagHelpQM.MyConfig.class) );
                Assertions.fail( "expected IllegalArgumentException" );
            } catch ( IllegalArgumentException ex ) {
                assertEquals( "ReservedFlagHelpQM is not valid.  Flag '?' has already been reserved for another use.", ex.getMessage() );
            }
        }

        @Test
        public void givenFieldWithReservedShortFlagNameOfV_expectError() {
            try {
                new AppRunner<>( ReservedFlagHelpV.class, env, FP.option(ReservedFlagHelpV.MyConfig.class) );
                Assertions.fail( "expected IllegalArgumentException" );
            } catch ( IllegalArgumentException ex ) {
                assertEquals( "ReservedFlagHelpV is not valid.  Flag 'v' has already been reserved for another use.", ex.getMessage() );
            }
        }

        @Test
        public void givenFieldWithReservedShortFlagNameOfd_expectError() {
            try {
                new AppRunner<>( ReservedFlagHelpD.class, env, FP.option(ReservedFlagHelpD.MyConfig.class) );
                Assertions.fail( "expected IllegalArgumentException" );
            } catch ( IllegalArgumentException ex ) {
                assertEquals( "ReservedFlagHelpD is not valid.  Flag 'd' has already been reserved for another use.", ex.getMessage() );
            }
        }

        @Test
        public void givenFieldWithReservedShortFlagNameOfDebug_expectError() {
            try {
                new AppRunner<>( ReservedFlagHelpDebug.class, env, FP.option(ReservedFlagHelpDebug.MyConfig.class) );
                Assertions.fail( "expected IllegalArgumentException" );
            } catch ( IllegalArgumentException ex ) {
                assertEquals( "ReservedFlagHelpDebug is not valid.  Flag 'debug' has already been reserved for another use.", ex.getMessage() );
            }
        }

        @Test
        public void givenFieldWithReservedShortFlagNameOfQ_expectError() {
            try {
                new AppRunner<>( ReservedFlagHelpQ.class, env, FP.option(ReservedFlagHelpQ.MyConfig.class) );
                Assertions.fail( "expected IllegalArgumentException" );
            } catch ( IllegalArgumentException ex ) {
                assertEquals( "ReservedFlagHelpQ is not valid.  Flag 'q' has already been reserved for another use.", ex.getMessage() );
            }
        }

        @Test
        public void givenFieldWithReservedShortFlagNameOfQuiet_expectError() {
            try {
                new AppRunner<>( ReservedFlagHelpQuiet.class, env, FP.option(ReservedFlagHelpQuiet.MyConfig.class) );
                Assertions.fail( "expected IllegalArgumentException" );
            } catch ( IllegalArgumentException ex ) {
                assertEquals( "ReservedFlagHelpQuiet is not valid.  Flag 'quiet' has already been reserved for another use.", ex.getMessage() );
            }
        }

        @Test
        public void givenFieldWithReservedShortFlagNameOfS_expectError() {
            try {
                new AppRunner<>( ReservedFlagHelps.class, env, FP.option(ReservedFlagHelps.MyConfig.class) );
                Assertions.fail( "expected IllegalArgumentException" );
            } catch ( IllegalArgumentException ex ) {
                assertEquals( "ReservedFlagHelps is not valid.  Flag 's' has already been reserved for another use.", ex.getMessage() );
            }
        }

        @Test
        public void givenFieldWithReservedShortFlagNameOfSilent_expectError() {
            try {
                new AppRunner<>( ReservedFlagHelpSilent.class, env, FP.option(ReservedFlagHelpSilent.MyConfig.class) );
                Assertions.fail( "expected IllegalArgumentException" );
            } catch ( IllegalArgumentException ex ) {
                assertEquals( "ReservedFlagHelpSilent is not valid.  Flag 'silent' has already been reserved for another use.", ex.getMessage() );
            }
        }
    }

    @Nested
    public class FieldInjectionTestCases {
        @Test
        public void givenAppWithInjectableFields_invoke_expectTheFieldsToBeInjectedFromDI() {
            AppRunner<HasInjectableFieldsApp> runner = new AppRunner<>( HasInjectableFieldsApp.class, env );
            HasInjectableFieldsApp            app    = new HasInjectableFieldsApp();

            String ipAddress = "1.2.3.4";
            DI di = new DI();
            di.putByName( "ipAddress", ipAddress );

            AppResult result = runner.run( app, di );

            assertTrue( result.isSuccess() );
            assertTrue( app.didRun );

            assertNotNull( app.codecs );
            assertEquals( ipAddress, app.ipAddress );
        }

//        @Test
//        public void givenRunnableApp_invoke_expectStartAndStopToBeLogged() {
//            AppRunner<HasInjectableFieldsApp> runner = new AppRunner<>( HasInjectableFieldsApp.class, env );
//            HasInjectableFieldsApp            app    = new HasInjectableFieldsApp();
//
//            StringCodecs codecs    = new StringCodecs();
//            String       ipAddress = "1.2.3.4";
//
//            runner.run( app, di -> {
//                di.putByType( StringCodecs.class, codecs );
//                di.putByName( "ipAddress", ipAddress );
//
//                return di;
//            }, "--verbose" );
//
//            List<Event> expected = Collections.singletonList(
//                new ConfigLogMessageEvent( MapUtils.asMap("ipAddress", "1.2.3.4") )
//            );
//
//            assertEquals( expected, logHandler.getLoggedMessages(ConfigLogMessageEvent.class) );
//        }
    }

    @Nested
    public class LogLevelTestCases {
        @Test
        public void givenRunnableApp_invoke_expectLogDebugAndVerboseChannelsToBeOffByDefault() {
            givenFlagAssertLogFilter( "", LogFilter.NORMAL );
        }

        @Test
        public void givenRunnableApp_invokeWithDebugFlag_expectLoggerToBeNotified() {
            givenFlagAssertLogFilter( "-d", LogFilter.DEBUG );
        }

        @Test
        public void givenRunnableApp_invokeWithVerboseFlag_expectLoggerToBeNotified() {
            givenFlagAssertLogFilter( "-v", LogFilter.VERBOSE );
        }

        @Test
        public void givenRunnableApp_invokeWithQuietFlag_expectLoggerToBeNotified() {
            givenFlagAssertLogFilter( "-q", LogFilter.ERRORS_ONLY );
        }

        @Test
        public void givenRunnableApp_invokeWithSilentFlag_expectLoggerToBeNotified() {
            givenFlagAssertLogFilter( "-s", LogFilter.SILENT );
        }

        @Test
        public void givenRunnableApp_invokeWithLongDebugFlag_expectLoggerToBeNotified() {
            givenFlagAssertLogFilter( "--debug", LogFilter.DEBUG );
        }

        @Test
        public void givenRunnableApp_invokeWithLongVerboseFlag_expectLoggerToBeNotified() {
            givenFlagAssertLogFilter( "--verbose", LogFilter.VERBOSE );
        }

        @Test
        public void givenRunnableApp_invokeWithLongQuietFlag_expectLoggerToBeNotified() {
            givenFlagAssertLogFilter( "--quiet", LogFilter.ERRORS_ONLY );
        }

        @Test
        public void givenRunnableApp_invokeWithLongSilentFlag_expectLoggerToBeNotified() {
            givenFlagAssertLogFilter( "--silent", LogFilter.SILENT );
        }

        private void givenFlagAssertLogFilter( String flag, LogFilter expectedFilter ) {
            AppRunner<LogFilterApp> runner = new AppRunner<>( LogFilterApp.class, env );
            LogFilterApp            app    = new LogFilterApp();

            if ( StringUtils.isBlank(flag) ) {
                runner.run( app );
            } else {
                runner.run( app, flag );
            }


            assertEquals( expectedFilter, app.logFilterUsed );
        }
    }


    @Nested
    public class ReturnTypeTestCases {
        @Test
        public void givenVoidReturnType_expectZeroOnSuccess() {
            AppRunner<HasVoidReturnMethod> runner = new AppRunner<>( HasVoidReturnMethod.class, env );
            HasVoidReturnMethod            app    = new HasVoidReturnMethod();

            AppResult result = runner.run( app );

            assertEquals( AppResult.SUCCESS, result.getExitCode() );
        }

        @Test
        public void givenIntReturnType_expectValueReturnedToBeTheExitCode() {
            AppRunner<HasIntReturnMethod> runner = new AppRunner<>( HasIntReturnMethod.class, env );
            HasIntReturnMethod            app    = new HasIntReturnMethod();

            AppResult result = runner.run( app );

            assertEquals( 42, result.getExitCode() );
        }
    }

    @Nested
    public class RunMethodArgTypeTestCases {
        @Test
        public void givenMultipleParameterRunMethod_invokeWithArgs_expectRunMethodToBeInvokedWithParams() {
            AppRunner<HasMultiArgRunMethod> runner = new AppRunner<>( HasMultiArgRunMethod.class, env );
            HasMultiArgRunMethod            app    = new HasMultiArgRunMethod();

            AppResult result = runner.run( app, "Hello Jim", "true", "42", "1.1", "a");

            assertEquals( AppResult.SUCCESS, result.getExitCode() );
            assertEquals( "Hello Jim", app.arg1 );
            assertEquals( true, app.arg2 );
            assertEquals( 42, app.arg3 );
            assertEquals( 1.1, app.arg4, 0.00001 );
            assertEquals( 'a', app.arg5 );
        }

        @Test
        public void givenLongNameStringFlag_expectValueToBeAvailableInAppField() {
            AppRunner<HasFlags> runner = new AppRunner<>( HasFlags.class, env, FP.option(HasFlags.MyConfig.class) );
            HasFlags            app    = new HasFlags();
            AppResult           result = runner.run( app, "--flag1=donkey" );

            assertEquals( AppResult.SUCCESS, result.getExitCode() );
            assertEquals( "donkey", app.config.flag1 );
        }

        @Test
        public void givenLongNameBooleanFlag_setItToTrue_expectValueToBeAvailableInAppField() {
            AppRunner<HasBooleanFlag> runner = new AppRunner<>( HasBooleanFlag.class, env, FP.option(HasBooleanFlag.MyConfig.class) );
            HasBooleanFlag            app    = new HasBooleanFlag();
            AppResult                 result = runner.run( app, "--flag2=true" );

            assertEquals( AppResult.SUCCESS, result.getExitCode() );
            assertTrue( app.config.flag2 );
        }

        @Test
        public void givenShortNameBooleanFlag_setItToTrue_expectValueToBeAvailableInAppField() {
            AppRunner<HasBooleanFlag> runner = new AppRunner<>( HasBooleanFlag.class, env, FP.option(HasBooleanFlag.MyConfig.class) );
            HasBooleanFlag            app    = new HasBooleanFlag();
            AppResult result = runner.run( app, "-c=true" );

            assertEquals( AppResult.SUCCESS, result.getExitCode() );
            assertTrue( app.config.flag2 );
        }

        @Test
        public void givenBooleanShortFlagWithNoValue_expectItsPresenceAloneWillSetItToTrue() {
            AppRunner<HasBooleanFlag> runner = new AppRunner<>( HasBooleanFlag.class, env, FP.option(HasBooleanFlag.MyConfig.class) );
            HasBooleanFlag            app    = new HasBooleanFlag();
            AppResult result = runner.run( app, "-c" );

            assertEquals( AppResult.SUCCESS, result.getExitCode() );
            assertTrue( app.config.flag2 );
        }

        @Test
        public void givenBooleanLongFlagWithNoValue_expectItsPresenceAloneWillSetItToTrue() {
            AppRunner<HasBooleanFlag> runner = new AppRunner<>( HasBooleanFlag.class, env, FP.option(HasBooleanFlag.MyConfig.class) );
            HasBooleanFlag            app    = new HasBooleanFlag();
            AppResult result = runner.run( app, "--flag2" );

            assertEquals( AppResult.SUCCESS, result.getExitCode() );
            assertTrue( app.config.flag2 );
        }

        @Test
        public void givenLongValueFlag() {
            AppRunner<LongFlagApp> runner = new AppRunner<>( LongFlagApp.class, env, FP.option(LongFlagApp.MyConfig.class) );
            LongFlagApp            app    = new LongFlagApp();
            AppResult              result = runner.run( app, "--flag1=123" );

            assertEquals( AppResult.SUCCESS, result.getExitCode() );
            assertEquals( 123, app.config.flag1 );
        }

        @Test
        public void givenDebugFlagBeforeAProgramArg_expectFlagToBeSetOnSystem() {
            AppRunner<OneArgApp> runner = new AppRunner<>( OneArgApp.class, env );
            OneArgApp            app    = new OneArgApp();

            runner.run( app, "--debug", "foo" );


            assertEquals( LogFilter.DEBUG, env.getLogFilter() );

            assertEquals( "foo", app.arg1 );
        }
    }


    @Nested
    public class EnumTestCases {
        @Test
        public void invokeAppWithValidEnumArg_expectItToRun() {
            AppRunner<HasEnumRunMethodApp> runner = new AppRunner<>( HasEnumRunMethodApp.class, env );
            HasEnumRunMethodApp app = new HasEnumRunMethodApp();
            AppResult result = runner.run( app, "abc", "OP1" );

            assertEquals( AppResult.SUCCESS, result.getExitCode() );
            assertEquals( AppEnum.OP1, app.arg2 );
        }

        @Test
        public void invokeAppWithAnotherValidEnumArg_expectItToRun() {
            AppRunner<HasEnumRunMethodApp> runner = new AppRunner<>( HasEnumRunMethodApp.class, env );
            HasEnumRunMethodApp            app    = new HasEnumRunMethodApp();
            AppResult                      result = runner.run( app, "abc", "OP2" );

            assertEquals( AppResult.SUCCESS, result.getExitCode() );
            assertEquals( AppEnum.OP2, app.arg2 );
        }

        @Test
        public void invokeAppWithInvalidEnumArg_expectItToFail() {
            AppRunner<HasEnumRunMethodApp> runner = new AppRunner<>( HasEnumRunMethodApp.class, env );
            HasEnumRunMethodApp            app    = new HasEnumRunMethodApp();
            AppResult                      result = runner.run( app, "abc", "OP5" );

            assertEquals( AppResult.USER_ERROR, result.getExitCode() );
            assertNull( app.arg2 );

            capturingLogWriter.assertContainsMessage(
                "Malformed value 'OP5' for arg 'arg2'"
            );
        }

        @Test
        public void invokeAppWithValidEnumFlag_expectItToRun() {
            AppRunner<HasEnumFlagApp> runner = new AppRunner<>( HasEnumFlagApp.class, env, FP.option(HasEnumFlagApp.MyConfig.class) );
            HasEnumFlagApp app = new HasEnumFlagApp();
            AppResult result = runner.run( app, "--flag=OP2" );

            assertEquals( AppResult.SUCCESS, result.getExitCode() );
            assertEquals( AppEnum.OP2, app.config.flag );
        }

        @Test
        public void invokeAppWithInvalidEnumFlag_expectItToFail() {
            AppRunner<HasEnumFlagApp> runner = new AppRunner<>( HasEnumFlagApp.class, env, FP.option(HasEnumFlagApp.MyConfig.class) );
            HasEnumFlagApp app = new HasEnumFlagApp();
            AppResult result = runner.run( app, "--flag=OP5" );

            assertEquals( AppResult.USER_ERROR, result.getExitCode() );
            assertNull( app.config );

            capturingLogWriter.assertContainsMessage(
                "Unable to convert 'OP5' of type 'java.lang.String' to property 'flag' of type 'mosaics.cli.runner.AppRunnerTest$AppEnum'"
            );
        }
    }


    @Nested
    public class StringArraySupportTestCases {
        @Test
        public void givenRunMethodWithArgAfterArrayOfStrings_expectError() {
            try {
                new AppRunner<>( ArgAfterArrayOfStrings.class, env );

                fail( "expected IllegalArgumentException" );
            } catch ( IllegalArgumentException e ) {
                assertEquals( "'ArgAfterArrayOfStrings' is not valid.  The var arg run method arg 'args' must be the last parameter of the method.", e.getMessage() );
            }
        }

        @Test
        public void givenRunMethodWithOptionBeforeArrayOfStrings_expectError() {
            try {
                new AppRunner<>( OptionalArgBeforeArrayOfStrings.class, env );

                fail( "expected IllegalArgumentException" );
            } catch ( IllegalArgumentException e ) {
                assertEquals( "'OptionalArgBeforeArrayOfStrings' is not valid.  The optional run method arg 'invalidArg' is now allowed before any var args.", e.getMessage() );
            }
        }

        @Test
        public void givenRunMethodWithFPOptionalBeforeArrayOfStrings_expectError() {
            try {
                new AppRunner<>( FPOptionArgBeforeArrayOfStrings.class, env );

                fail( "expected IllegalArgumentException" );
            } catch ( IllegalArgumentException e ) {
                assertEquals( "'FPOptionArgBeforeArrayOfStrings' is not valid.  The optional run method arg 'invalidArg' is now allowed before any var args.", e.getMessage() );
            }
        }

        @Test
        public void supplyNoArgs_expectMethodToRunWithEmptyArrayPastToIt() {
            AppRunner<RunMethodIsAnArrayOfStrings> runner = new AppRunner<>( RunMethodIsAnArrayOfStrings.class, env );
            RunMethodIsAnArrayOfStrings            app    = new RunMethodIsAnArrayOfStrings();
            AppResult                              result = runner.run( app );

            assertTrue( result.isSuccess() );
            assertArrayEquals( new String[] {}, app.args );
        }

        @Test
        public void supplyOneArgs_expectMethodToRunWithEmptyArrayPastToIt() {
            AppRunner<RunMethodIsAnArrayOfStrings> runner = new AppRunner<>( RunMethodIsAnArrayOfStrings.class, env );
            RunMethodIsAnArrayOfStrings            app    = new RunMethodIsAnArrayOfStrings();
            AppResult                              result = runner.run( app, "hello" );

            assertTrue( result.isSuccess() );
            assertArrayEquals( new String[] {"hello"}, app.args );
        }

        @Test
        public void supplyTwoArg_expectMethodToRunWithArrayPastToIt() {
            AppRunner<RunMethodIsAnArrayOfStrings> runner = new AppRunner<>( RunMethodIsAnArrayOfStrings.class, env );
            RunMethodIsAnArrayOfStrings            app    = new RunMethodIsAnArrayOfStrings();
            AppResult                              result = runner.run( app, "hello", "world" );

            assertTrue( result.isSuccess() );
            assertArrayEquals( new String[] {"hello", "world"}, app.args );
        }



        public class ArgAfterArrayOfStrings {
            private String[] args;

            public void run( String[] args, String invalidArg ) {
                this.args = args;
            }
        }


        public class OptionalArgBeforeArrayOfStrings {
            private String[] args;

            public void run( Optional<String> invalidArg, String[] args) {
                this.args = args;
            }
        }


        public class FPOptionArgBeforeArrayOfStrings {
            private String[] args;

            public void run( FPOption<String> invalidArg, String[] args) {
                this.args = args;
            }
        }


        public class RunMethodIsAnArrayOfStrings {
            private String[] args;

            public void run( String[] args ) {
                this.args = args;
            }
        }
    }


    @Nested
    public class IntListSupportTestCases {
        @Test
        public void givenRunMethodWithArgAfterListOfInts_expectError() {
            try {
                new AppRunner<>( ArgAfterListOfInts.class, env );

                fail( "expected IllegalArgumentException" );
            } catch ( IllegalArgumentException e ) {
                assertEquals( "'ArgAfterListOfInts' is not valid.  The var arg run method arg 'args' must be the last parameter of the method.", e.getMessage() );
            }
        }

        @Test
        public void givenRunMethodWithOptionBeforeListOfInts_expectError() {
            try {
                new AppRunner<>( OptionalArgBeforeListOfInts.class, env );

                fail( "expected IllegalArgumentException" );
            } catch ( IllegalArgumentException e ) {
                assertEquals( "'OptionalArgBeforeListOfInts' is not valid.  The optional run method arg 'invalidArg' is now allowed before any var args.", e.getMessage() );
            }
        }

        @Test
        public void givenRunMethodWithFPOptionalBeforeListOfInts_expectError() {
            try {
                new AppRunner<>( FPOptionArgBeforeListOfInts.class, env );

                fail( "expected IllegalArgumentException" );
            } catch ( IllegalArgumentException e ) {
                assertEquals( "'FPOptionArgBeforeListOfInts' is not valid.  The optional run method arg 'invalidArg' is now allowed before any var args.", e.getMessage() );
            }
        }

        @Test
        public void supplyNoArgs_expectMethodToRunWithEmptyArrayPastToIt() {
            AppRunner<RunMethodIsAnListOfInts> runner = new AppRunner<>( RunMethodIsAnListOfInts.class, env );
            RunMethodIsAnListOfInts            app    = new RunMethodIsAnListOfInts();
            AppResult                          result = runner.run( app );

            assertTrue( result.isSuccess() );
            assertEquals( Collections.emptyList(), app.args );
        }

        @Test
        public void supplyOneArgs_expectMethodToRunWithEmptyArrayPastToIt() {
            AppRunner<RunMethodIsAnListOfInts> runner = new AppRunner<>( RunMethodIsAnListOfInts.class, env );
            RunMethodIsAnListOfInts            app    = new RunMethodIsAnListOfInts();
            AppResult                          result = runner.run( app, "42" );

            assertTrue( result.isSuccess() );
            assertEquals( Arrays.asList(42), app.args );
        }

        @Test
        public void supplyTwoArg_expectMethodToRunWithArrayPastToIt() {
            AppRunner<RunMethodIsAnListOfInts> runner = new AppRunner<>( RunMethodIsAnListOfInts.class, env );
            RunMethodIsAnListOfInts            app    = new RunMethodIsAnListOfInts();
            AppResult                          result = runner.run( app, "1", "2" );

            assertTrue( result.isSuccess() );
            assertEquals( Arrays.asList(1, 2), app.args );
        }



        public class ArgAfterListOfInts {
            private List<Integer> args;

            public void run( List<Integer> args, String invalidArg ) {
                this.args = args;
            }
        }


        public class OptionalArgBeforeListOfInts {
            private List<Integer> args;

            public void run( Optional<String> invalidArg, List<Integer> args) {
                this.args = args;
            }
        }


        public class FPOptionArgBeforeListOfInts {
            private List<Integer> args;

            public void run( FPOption<String> invalidArg, List<Integer> args) {
                this.args = args;
            }
        }


        public class RunMethodIsAnListOfInts {
            private List<Integer> args;

            public void run( List<Integer> args ) {
                this.args = args;
            }
        }
    }


//// COLLECTION FLAGS
//
//    // givenArrayOfStringFlag_supplyNoFlag_expectRunMethodToRunAndFlagToBeAnEmptyArray
//    // givenArrayOfStringFlag_supplySingleValueFlag_expectRunMethodToRunAndFlagToBeAnSetToValue
//    // givenArrayOfStringFlag_supplyTwoValueCommaSeparatedFlag_expectRunMethodToRunAndFlagToBeAnSetToValues
//    // givenArrayOfStringFlag_supplyThreeValueCommaSeparatedFlag_expectRunMethodToRunAndFlagToBeAnSetToValues
//
//
//// COMBINE SHORT FLAGS TOGETHER (eg -alh)
//
    @Nested
    public class CombineShortFlagsTogetherTestCases {
        @Test
        public void givenThreeShortFlagsConcatenatedTogether_expectAllThreeToBeSet () {
            AppRunner<HasThreeBooleanFlags> runner = new AppRunner<>( HasThreeBooleanFlags.class, env, FP.option(HasThreeBooleanFlags.MyConfig.class) );
            HasThreeBooleanFlags            app    = new HasThreeBooleanFlags();
            AppResult                       result = runner.run( app, "-abc" );

            assertEquals( AppResult.SUCCESS, result.getExitCode() );
            assertTrue( app.config.flag1 );
            assertTrue( app.config.flag2 );
            assertTrue( app.config.flag3 );
        }

        @Test
        public void givenTwoShortFlagsConcatenatedTogether_expectAllThreeToBeSet () {
            AppRunner<HasThreeBooleanFlags> runner = new AppRunner<>( HasThreeBooleanFlags.class, env, FP.option(HasThreeBooleanFlags.MyConfig.class) );
            HasThreeBooleanFlags            app    = new HasThreeBooleanFlags();
            AppResult                       result = runner.run( app, "-ca" );

            assertEquals( AppResult.SUCCESS, result.getExitCode() );
            assertTrue( app.config.flag1 );
            assertFalse( app.config.flag2 );
            assertTrue( app.config.flag3 );
        }
    }

//
//// ERROR HANDLING
//
    @Nested
    public class ErrorHandlingTestCases {
        @Test
        public void givenMissingValueForNonBooleanFlagThatCanBeConverted_expectBooleanTrueToBeConvertedToString() {
            AppRunner<HasFlags> runner = new AppRunner<>( HasFlags.class, env, FP.option(HasFlags.MyConfig.class) );
            HasFlags            app    = new HasFlags();
            AppResult           result = runner.run( app, "--flag1" );

            assertEquals( AppResult.SUCCESS, result.getExitCode() );
            assertEquals( "true", app.config.flag1 );
            assertFalse( app.config.flag2 );
            assertFalse( app.config.flag3 );
        }

    @Test
    public void givenMissingValueForNonBooleanFlagThatCannotBeConverted_expectError() {
        AppRunner<HasFlags> runner = new AppRunner<>( HasFlags.class, env, FP.option(HasFlags.MyConfig.class) );
        HasFlags            app    = new HasFlags();
        AppResult           result = runner.run( app, "--flag4" );

        assertEquals( AppResult.USER_ERROR, result.getExitCode() );
        assertNull( app.config );
capturingLogWriter.dumpAll();
        capturingLogWriter.assertContainsMessage(
            "Unable to convert 'true' of type 'java.lang.String' to property 'flag4' of type 'java.lang.Float'"
        );
    }

        @Test
        public void givenAMalformedIntValueForAFlag_expectError() {
            AppRunner<HasMultipleFlags> runner = new AppRunner<>( HasMultipleFlags.class, env, FP.option(HasMultipleFlags.MyConfig.class) );
            HasMultipleFlags            app    = new HasMultipleFlags();
            AppResult result = runner.run( app, "--intFlag=abc" );

            assertEquals( AppResult.USER_ERROR, result.getExitCode() );

            capturingLogWriter.assertContainsMessage(
                "Unable to convert 'abc' of type 'java.lang.String' to property 'intFlag' of type 'int'"
            );
        }

        @Test
        public void givenAMalformedIntValueForArg_expectError() {
            AppRunner<HasIntArgOnRunMethod> runner = new AppRunner<>( HasIntArgOnRunMethod.class, env );
            HasIntArgOnRunMethod            app    = new HasIntArgOnRunMethod();
            AppResult                       result = runner.run( app, "abc" );

            assertEquals( AppResult.USER_ERROR, result.getExitCode() );

            capturingLogWriter.assertContainsMessage(
                "Malformed value 'abc' for arg 'intArg'"
            );
        }

        @Test
        public void givenTwoRunMethods_runTheFirstMethod() {
            AppRunner<HasTwoRunMethods> runner = new AppRunner<>( HasTwoRunMethods.class, env );
            HasTwoRunMethods            app    = new HasTwoRunMethods();
            AppResult result = runner.run( app );

            assertEquals( AppResult.SUCCESS, result.getExitCode() );
            assertEquals( "no-arg", app.method );
        }

        @Test
        public void givenTwoRunMethods_runTheSecondMethod() {
            AppRunner<HasTwoRunMethods> runner = new AppRunner<>( HasTwoRunMethods.class, env );
            HasTwoRunMethods            app    = new HasTwoRunMethods();
            AppResult                   result = runner.run( app, "10" );

            assertEquals( AppResult.SUCCESS, result.getExitCode() );
            assertEquals( "oneArg(10)", app.method );
        }
    }


    @Nested
    public class CompositeObjectArgTestCases {
        @Test
        public void givenAppThatHasCompositeArg_supplyArgsFromCLI_expectArgsToBeSet() {
            AppRunner<HasCompositeArgRunMethod> runner = new AppRunner<>( HasCompositeArgRunMethod.class, env, FP.option(HasCompositeArgRunMethod.MyConfig.class) );
            HasCompositeArgRunMethod app = new HasCompositeArgRunMethod();
            AppResult result = runner.run(
                app,
                "--args.arg1=hello", "--args.arg2=true", "--args.arg3=10", "--args.arg4=-3.14",
                "--args.arg5=c"
            );


            assertEquals( AppResult.SUCCESS, result.getExitCode() );
            assertEquals( "hello", app.config.args.arg1 );
            assertEquals( true, app.config.args.arg2 );
            assertEquals( 10, app.config.args.arg3 );
            assertEquals( -3.14, app.config.args.arg4, 0.00001 );
            assertEquals( 'c', app.config.args.arg5 );
        }

        @Test
        public void givenAppThatHasCompositeArgWithShortName_invokeWithShortName() {
            AppRunner<HasCompositeArgWithShortName> runner = new AppRunner<>( HasCompositeArgWithShortName.class, env, FP.option(HasCompositeArgWithShortName.MyConfig.class) );
            HasCompositeArgWithShortName app = new HasCompositeArgWithShortName();
            AppResult result = runner.run( app, "-a=hello" );


            assertEquals( AppResult.SUCCESS, result.getExitCode() );
            assertEquals( "hello", app.config.args.arg1 );
        }

        @Test
        public void givenAppThatHasCompositeArgWithRemappedLongName_invokeWithNewLongName_expectValues() {
            AppRunner<HasCompositeArgWithRemappedLongName> runner = new AppRunner<>( HasCompositeArgWithRemappedLongName.class, env, FP.option(HasCompositeArgWithRemappedLongName.MyConfig.class) );
            HasCompositeArgWithRemappedLongName app = new HasCompositeArgWithRemappedLongName();
            AppResult result = runner.run( app, "--name=Jim" );


            assertEquals( AppResult.SUCCESS, result.getExitCode() );
            assertEquals( "Jim", app.config.args.arg1 );
        }

//        @Test // does not pay attention to annotation yet
//        public void givenAppThatHasCompositeArgWithRemappedLongName_invokeWithOldLongName_expectValueToNotBeSet() {
//            AppRunner<HasCompositeArgWithRemappedLongName> runner = new AppRunner<>( HasCompositeArgWithRemappedLongName.class, env );
//            HasCompositeArgWithRemappedLongName app = new HasCompositeArgWithRemappedLongName();
//            AppResult result = runner.run( app, "--args.arg1=hello" );
//
//
//            assertEquals( AppResult.MISSING_CONFIG_ERROR, result.getExitCode() );
//            assertNull( app.args );
//
//            capturingLogWriter.assertContainsMessage( "Missing config: name" );
//        }
    }


    @Nested
    @ExtendWith( JMFileExtension.class )
    public class ConfigFileTestCases {
        @Test
        public void givenMissingClassPathConfigFile_expectError() {
            AppRunner<HasCompositeArgRunMethod> runner = new AppRunner<>( HasCompositeArgRunMethod.class, env, FP.option(HasCompositeArgRunMethod.MyConfig.class) );
            HasCompositeArgRunMethod            app    = new HasCompositeArgRunMethod();
            AppResult                           result = runner.run( app, "--config=classpath:/mosaics/cli/internals/missing.properties" );

            assertEquals( AppResult.USER_ERROR, result.getExitCode() );

            capturingLogWriter.assertContainsMessage(
                "Unable to load missing configuration from 'classpath:/mosaics/cli/internals/missing.properties'"
            );
        }

        @Test
        public void givenClassPathConfigFile_runWithFile_expectConfigToBeVisible() {
            AppRunner<HasCompositeArgRunMethod> runner = new AppRunner<>( HasCompositeArgRunMethod.class, env, FP.option(HasCompositeArgRunMethod.MyConfig.class) );
            HasCompositeArgRunMethod app = new HasCompositeArgRunMethod();
            AppResult result = runner.run( app, "--config=classpath:/mosaics/cli/internals/HasCompositeArgRunMethod.properties" );


            assertEquals( AppResult.SUCCESS, result.getExitCode() );
            assertEquals( "hello world", app.config.args.arg1 );
            assertEquals( true, app.config.args.arg2 );
            assertEquals( 101, app.config.args.arg3 );
            assertEquals( 3.14, app.config.args.arg4, 0.00001 );
            assertEquals( 'a', app.config.args.arg5 );
        }

        @Test
        public void givenFileURLConfigFile_runWithFile_expectConfigToBeVisible(
            @JMFileExtension.FileContents(
                name = "config.properties",
                lines = {
                    "args.arg1=hello world",
                    "args.arg2=true",
                    "args.arg3=101",
                    "args.arg4=3.14",
                    "args.arg5=a"
                }
            )
            File configFile
        ) {
            AppRunner<HasCompositeArgRunMethod> runner = new AppRunner<>( HasCompositeArgRunMethod.class, env, FP.option(HasCompositeArgRunMethod.MyConfig.class) );

            HasCompositeArgRunMethod app = new HasCompositeArgRunMethod();
            AppResult result = runner.run( app, "--config=file://" + configFile.getAbsolutePath() );

            assertEquals( AppResult.SUCCESS, result.getExitCode() );
            assertEquals( "hello world", app.config.args.arg1 );
            assertEquals( true, app.config.args.arg2 );
            assertEquals( 101, app.config.args.arg3 );
            assertEquals( 3.14, app.config.args.arg4, 0.00001 );
            assertEquals( 'a', app.config.args.arg5 );
        }

        @Test
        public void givenFileURLConfigFileWithNoProtocol_runWithFile_expectConfigToDefaultToFileProtocol(
            @JMFileExtension.FileContents(
                name = "config.properties",
                lines = {
                    "args.arg1=hello world",
                    "args.arg2=true",
                    "args.arg3=101",
                    "args.arg4=3.14",
                    "args.arg5=a"
                }
            )
            File configFile
        ) {
            AppRunner<HasCompositeArgRunMethod> runner = new AppRunner<>( HasCompositeArgRunMethod.class, env, FP.option(HasCompositeArgRunMethod.MyConfig.class) );

            HasCompositeArgRunMethod app = new HasCompositeArgRunMethod();
            AppResult result = runner.run( app, "--config=" + configFile.getAbsolutePath() );

            assertEquals( AppResult.SUCCESS, result.getExitCode() );
            assertEquals( "hello world", app.config.args.arg1 );
            assertEquals( true, app.config.args.arg2 );
            assertEquals( 101, app.config.args.arg3 );
            assertEquals( 3.14, app.config.args.arg4, 0.00001 );
            assertEquals( 'a', app.config.args.arg5 );
        }

        @Test
        public void givenClassPathConfigFile_runWithOverridesOnTheCLI_expectTheCLIArgsToTakePriorityOverWhatIsInTheConfigFile(
            @JMFileExtension.FileContents(
                name = "config.properties",
                lines = {
                    "args.arg1=hello world",
                    "args.arg2=true",
                    "args.arg3=101",
                    "args.arg4=3.14",
                    "args.arg5=a"
                }
            )
            File configFile
        ) {
            AppRunner<HasCompositeArgRunMethod> runner = new AppRunner<>( HasCompositeArgRunMethod.class, env, FP.option(HasCompositeArgRunMethod.MyConfig.class) );

            HasCompositeArgRunMethod app = new HasCompositeArgRunMethod();
            AppResult result = runner.run( app,
                "--config=file://" + configFile.getAbsolutePath(),
                "--args.arg3=42"
            );


            assertEquals( AppResult.SUCCESS, result.getExitCode() );
            assertEquals( "hello world", app.config.args.arg1 );
            assertEquals( true, app.config.args.arg2 );
            assertEquals( 42, app.config.args.arg3 );
            assertEquals( 3.14, app.config.args.arg4, 0.00001 );
            assertEquals( 'a', app.config.args.arg5 );
        }

        @Test
        public void givenClassPathConfigFile_runWithOverrides_expectUsedConfigValuesToBeLogged(
            @JMFileExtension.FileContents(
                name = "config.properties",
                lines = {
                    "args.arg1=hello world",
                    "args.arg2=true",
                    "args.arg3=101",
                    "args.arg4=3.14",
                    "args.arg5=a"
                }
            )
                File configFile
        ) {
            AppRunner<HasCompositeArgRunMethod> runner = new AppRunner<>( HasCompositeArgRunMethod.class, env, FP.option(HasCompositeArgRunMethod.MyConfig.class) );

            HasCompositeArgRunMethod app = new HasCompositeArgRunMethod();
            runner.run(app, "-v", "--config=file://" + configFile.getAbsolutePath(), "--args.arg3=42");

            List<String> expected = List.of(
                "Supplied config: {args.arg1=hello world, args.arg2=true, args.arg3=42, args.arg4=3.14, args.arg5=a}"
            );
capturingLogWriter.dumpAll();
            assertEquals(
                expected,
                capturingLogWriter.getFormattedLogMessagesByType("mosaics.cli.runner.AppLogMessageFactory#configLogMessage(java.util.Map<java.lang.String, java.lang.String> config)")
            );
        }
    }

    @Test
    public void givenHasRequestOnRunMethod_invoke_expectRequestToBePastIntoTheRunMethod() {
        AppRunner<HasAppEnvOnRunMethod> runner = new AppRunner<>( HasAppEnvOnRunMethod.class, env );
        HasAppEnvOnRunMethod app = new HasAppEnvOnRunMethod();
        AppResult result = runner.run( app );


        assertEquals( AppResult.SUCCESS, result.getExitCode() );
        assertNotNull( app.initialRequest );
    }

    public static class NoRunMethodApp {}

    public static class NoArgApp {
        private boolean didRun = false;

        public void run() {
            this.didRun = true;
        }
    }

    public static class LogFilterApp {
        private boolean didRun = false;
        private LogFilter logFilterUsed;

        public void run(Request env) {
            this.didRun        = true;
            this.logFilterUsed = env.getEnv().getLogFilter();
        }
    }

    @ToString(exclude = {"codecs"})
    @EqualsAndHashCode(of={"ipAddress"})
    public static class HasInjectableFieldsApp {
        public StringCodecs codecs;
        public String       ipAddress;

        private boolean didRun = false;

        public HasInjectableFieldsApp() {}

        public HasInjectableFieldsApp( String ipAddress, boolean didRun ) {
            this.ipAddress = ipAddress;
            this.didRun    = didRun;
        }

        public void run() {
            this.didRun = true;
        }
    }

    public static class OneArgApp {
        private String arg1;

        public void run( String arg1 ) {
            this.arg1 = arg1;
        }
    }

    public static class LongFlagApp {
        @Value
        @Builder
        public static class MyConfig {
            @Builder.Default
            public long flag1 = 10;
        }

        private MyConfig config;

        public void run( MyConfig config ) {
            this.config = config;
        }
    }

    public static enum AppEnum {
        OP1, OP2, OP3
    }


    public static class HasEnumRunMethodApp {
        private AppEnum arg2;

        public void run( String arg1, AppEnum arg2) {
            this.arg2 = arg2;
        }
    }


    public static class HasEnumFlagApp {
        public static class MyConfig {
            public AppEnum flag;
        }

        private MyConfig config;

        public void run( MyConfig config ) {
            this.config = config;
        }
    }
////    /**
////     * This is a test application.
////     */
////    public static class HasOptionArgOnRunMethod {
////        /**
////         *
////         * @param arg1 this is arg 1
////         * @param arg2 this is arg 2
////         */
////        public void run( String arg1, Option<Boolean> arg2) {
////
////        }
////    }
////
////    /**
////     * This is a test application.
////     */
////    public static class HasClassDocumentationApp {
////        public HasClassDocumentationApp( Env system ) {}
////
////        public void run() {
////        }
////    }
////
////    /**
////     * This is a test application.
////     */
////    public static class HasOptionalArgOnRunMethod {
////        /**
////         *
////         * @param arg1 this is arg 1
////         * @param arg2 this is arg 2
////         */
////        public void run( String arg1, Optional<Boolean> arg2) {
////        }
////    }
////
////    public enum AppEnum {
////        OP1, OP2, OP3
////    }
////
////
////    public static class HasOptionalArgOutOfOrder {
////        public void run( Optional<Boolean> flag, String name ) {
////        }
////    }
////
////    public static class HasOptionArgOutOfOrder {
////        public void run( Option<Boolean> flag, String name ) {
////        }
////    }
////
    public static class HasVoidReturnMethod {
        public void run() { }
    }

    public static class HasStartStoppableSupport extends StartStoppable<HasStartStoppableSupport> {
        public HasStartStoppableSupport() {
            super( "s" );
        }

        private List<String> events = new ArrayList<>();

        protected void doStart() {
            events.add("doStart");
        }

        protected void doStop() throws Exception {
            events.add("doStop");
        }

        public void run() {
            events.add("run");
        }
    }

    public static class HasIntReturnMethod {
        public int run() {
            return 42;
        }
    }

    public static class HasFlags {
        @Value
        @Builder
        public static class MyConfig {
            private String flag1;
            private boolean flag2;

            @CommandLineArg(shortName = 'c')
            private boolean flag3;

            private Float flag4 = 1.0f;
        }

        private MyConfig config;

        public void run( MyConfig config ) {
            this.config = config;
        }
    }

    public static class HasBooleanFlag {
        @Value
        @Builder
        public static class MyConfig {
            @CommandLineArg(shortName = 'c')
            public boolean flag2;
        }

        private MyConfig config;

        public void run( MyConfig config ) {
            this.config = config;
        }
    }

    public static class HasThreeBooleanFlags {
        public static class MyConfig {
            @CommandLineArg(shortName = 'a')
            public boolean flag1;
            @CommandLineArg(shortName = 'b')
            public boolean flag2;
            @CommandLineArg(shortName = 'c')
            public boolean flag3;
        }

        private MyConfig config;

        public void run( MyConfig config ) {
            this.config = config;
        }
    }
////
////    public static class HasThreeBooleanFlags {
////        @CommandLineArg(shortName='a')
////        public boolean flag1;
////
////        @CommandLineArg(shortName='b')
////        public boolean flag2;
////
////        @CommandLineArg(shortName='c')
////        public boolean flag3;
////
////        public HasThreeBooleanFlags( Env system ) {}
////
////        public void run() {}
////    }
////
    public static class HasMultiArgRunMethod {
        private String  arg1;
        private boolean arg2;
        private int     arg3;
        private float   arg4;
        private char    arg5;

        public void run( String arg1, boolean arg2, int arg3, float arg4, char arg5 ) {
            this.arg1 = arg1;
            this.arg2 = arg2;
            this.arg3 = arg3;
            this.arg4 = arg4;
            this.arg5 = arg5;
        }
    }

    public static class HasCompositeArgRunMethod {
        @ToString
        public static class MyComposite {
            /**
             * A trusty String arg.
             */
            public String  arg1;

            /**
             * A trusty boolean arg.
             */
            public boolean arg2;

            /**
             * A trusty integer arg.
             */
            public int     arg3;

            /**
             * A trusty float arg.
             */
            public float   arg4;

            /**
             * A trusty character arg.
             */
            public char    arg5;
        }

        @ToString
        public static class MyConfig {
            public MyComposite args;
        }

        public MyConfig config;
        public void run( MyConfig config ) {
            this.config = config;
        }
    }

    public static class HasCompositeArgWithShortName {
        public static class MyComposite {
            /**
             * A trusty String arg.
             */
            @CommandLineArg(shortName='a')
            public String arg1;
        }

        @With
        @Value
        @AllArgsConstructor
        public static class MyConfig {
            private MyComposite args;

            public MyConfig() {
                this(null);
            }
        }

        public MyConfig config;

        public void run( MyConfig config ) {
            this.config = config;
        }
    }

    public static class HasCompositeArgWithRemappedLongName {
        public static class MyComposite {
            /**
             * A trusty String arg.
             */
            @CommandLineArg(shortName='a', longName="name")
            public String  arg1;
        }

        public static class MyConfig {
            public MyComposite args;
        }

        private MyConfig config;

        public void run( MyConfig config ) {
            this.config = config;
        }
    }

//    public static class InvalidFieldType {
//        public InvalidFieldType(Long a, String b, Double c) {}
//    }
//
    public static class ReservedFlagHelpApp {
        @Value
        @Builder
        public static class MyConfig {
            public String help;
        }

        private MyConfig config;

        public void run( MyConfig config ) {}
    }

    public static class ReservedFlagHelpQM {
        @Value
        @Builder
        public static class MyConfig {
            @CommandLineArg(shortName = '?')
            public String foo;
        }

        private MyConfig config;

        public void run( MyConfig config ) {
            this.config = config;
        }
    }

    public static class ReservedFlagHelpV {
        @Value
        @Builder
        public static class MyConfig {
            @CommandLineArg(shortName = 'v')
            public String foo;
        }

        public void run() {}
    }

    public static class ReservedFlagHelpD {
        @Value
        @Builder
        public static class MyConfig {
            public String d;
        }

        public void run() {}
    }

    public static class ReservedFlagHelpDebug {
        @Value
        @Builder
        public static class MyConfig {
            public String debug;
        }

        public void run() {}
    }

    public static class ReservedFlagHelpQ {
        @Value
        @Builder
        public static class MyConfig {
            public String q;
        }

        public void run() {}
    }

    public static class ReservedFlagHelpQuiet {
        @Value
        @Builder
        public static class MyConfig {
            public String quiet;
        }

        public void run() {}
    }

    public static class ReservedFlagHelps {
        @Value
        @Builder
        public static class MyConfig {
            public String s;
        }

        public void run() {}
    }

    public static class ReservedFlagHelpSilent {
        @Value
        @Builder
        public static class MyConfig {
            public String silent;
        }

        public void run() {}
    }


////    public static class HasCompositeArgWithRemappedBaseName {
////        public static class MyOwnComposite {
////            public String arg1;
////        }
////
////        @CommandLineArg(longName="config")
////        public MyOwnComposite args;
////
////        public HasCompositeArgWithRemappedBaseName( Env system ) {}
////
////        public void run() {}
////    }
////
////    public static class HasCompositeArgWithArgNamePostfixedWithConfig {
////        public static class MyOwnComposite {
////            public String arg1;
////        }
////
////        public MyOwnComposite argsConfig;
////
////        public HasCompositeArgWithArgNamePostfixedWithConfig( Env system ) {}
////
////        public void run() {}
////    }
////
//
//
    public static class HasAppEnvOnRunMethod {
        private Request initialRequest;

        public void run( Request initialRequest ) {
            this.initialRequest = initialRequest;
        }
    }

    public static class HasIntArgOnRunMethod {
        private int intArg;

        public void run( int intArg ) {
            this.intArg = intArg;
        }
    }

    public static class HasMultipleFlags {
        public static class MyConfig {
            public String stringFlag;
            public boolean booleanFlag;
            public int intFlag;
            public float floatFlag;
            public char charFlag;
        }

        private MyConfig config;

        public void run( MyConfig config ) {
            this.config = config;
        }
    }


    public static class UnknownType {}

    public static class HasArgOfUnknownType {
        public void run( UnknownType unknownArg ) {}
    }
//
//
    public static class HasRunMethodWithAnOptionalArgBeforeANoneOptionalArg {
        public void run( Optional<String> oArg, String otherArg ) {}
    }

    public static class HasRunMethodWithAnFPOptionalArgBeforeANoneOtionalArg {
        public void run( FPOption<String> oArg, String otherArg ) {}
    }

    public static class HasRunMethodWithAnOptionalArgThatHasNoCodec {
        public void run( Optional<HasRunMethodWithAnOptionalArgThatHasNoCodec> oArg ) {}
    }

    public static class HasRunMethodWithAnFPOptionalArgThatHasNoCodec {
        public void run( FPOption<HasRunMethodWithAnOptionalArgThatHasNoCodec> oArg ) {}
    }

    public static class HasRunMethodThatReturnsString {
        public String run() {
            return null;
        }
    }

    public static class HasTwoRunMethods {
        private String method;

        public void run() {
            method = "no-arg";
        }

        public void run(int a) {
            method = "oneArg("+a+")";
        }
    }


    public static class HasPassword {
        public Secret password;

        public void run() {}
    }

////    public static class HasDefaultValuesApp {
////        public String  flag1 = "abc";
////
////        public boolean flag2 = true;
////        public boolean flag3 = false;
////
////
////        public HasDefaultValuesApp( Env system ) {}
////
////        public void run() {
////        }
////    }
////
////}



}



//// MULTIPLE RUN METHODS
//
