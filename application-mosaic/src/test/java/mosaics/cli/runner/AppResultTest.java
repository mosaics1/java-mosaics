package mosaics.cli.runner;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class AppResultTest {

    @Test
    public void testNoValidRunMethodsFactoryMethod() {
        assertEquals( AppResult.NO_RUN_METHOD_FOUND, AppResult.noValidRunMethods().getExitCode() );
    }

}
