package mosaics.cli;

import mosaics.cli.runner.AppResult;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class CommandLineExceptionTest {

    @Test
    public void givenMessageOnly_expectExceptionStatusCodeAsDefault() {
        CommandLineException ex = new CommandLineException( "msg" );

        assertEquals( "msg", ex.getMessage() );
        assertEquals( AppResult.EXCEPTION_THROWN, ex.getExitStatus() );
    }

    @Test
    public void givenStatusCodeOf42_expectStatusCodeOf42() {
        CommandLineException ex = new CommandLineException( "msg", 42 );

        assertEquals( "msg", ex.getMessage() );
        assertEquals( 42, ex.getExitStatus() );
    }

}
