package mosaics.cli.meta;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.Value;
import mosaics.fp.collections.FPOption;
import mosaics.lang.reflection.JavaClass;
import mosaics.lang.reflection.JavaDoc;
import mosaics.lang.reflection.JavaProperty;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class AppFlagMetaFactoryTest {
    private final AppFlagMetaFactory appFlagMetaFactory = new AppFlagMetaFactory();

    @Nested
    public class PublicFieldTestCases {
        @Nested
        public class RootDTOLevelTestCases {
            @Test
            public void stringField() {
                AppFlagMeta actual = createFlagFor( ClassWithPublicFields.class, "stringField" );

                AppFlagMeta expected = new AppFlagMeta(
                    FPOption.none(),
                    "stringField",
                    "stringField",
                    JavaClass.of( String.class ),
                    FPOption.of( new JavaDoc( "stringField" ) ),
                    FPOption.none()
                );

                assertEquals( expected, actual );
            }

            @Test
            public void booleanField() {
                AppFlagMeta actual = createFlagFor( ClassWithPublicFields.class, "booleanField" );

                AppFlagMeta expected = new AppFlagMeta(
                    FPOption.none(),
                    "booleanField",
                    "booleanField",
                    JavaClass.of( boolean.class ),
                    FPOption.of( new JavaDoc( "booleanField" ) ),
                    FPOption.of(false)
                );

                assertEquals( expected, actual );
            }
        }

        @Nested
        public class OneLevelDeepDTOLevelTestCases {
            @Test
            public void stringField() {
                AppFlagMeta actual = createFlagFor( ClassWithPublicFields.class, "dtoField", "stringField" );

                AppFlagMeta expected = new AppFlagMeta(
                    FPOption.none(),
                    "dtoField.stringField",
                    "dtoField.stringField",
                    JavaClass.of( String.class ),
                    FPOption.of( new JavaDoc( "PublicFieldDto.stringField" ) ),
                    FPOption.none()
                );

                assertEquals( expected, actual );
            }
        }

        @Nested
        public class TwoLevelDeepDTOLevelTestCases {
            @Test
            public void booleanObjectField() {
                AppFlagMeta actual = createFlagFor( ClassWithPublicFields.class, "dtoField", "dtoField2", "booleanObjectField" );

                AppFlagMeta expected = new AppFlagMeta(
                    FPOption.none(),
                    "dtoField.dtoField2.booleanObjectField",
                    "dtoField.dtoField2.booleanObjectField",
                    JavaClass.of( Boolean.class ),
                    FPOption.of( new JavaDoc( "PublicFieldDto2.booleanObjectField" ) ),
                    FPOption.none()
                );

                assertEquals( expected, actual );
            }
        }
    }

    /**
     * Support DTOs with traditional getter/setters
     */
    @Nested
    public class GetterSetterTestCases{
        @Test
        public void stringField() {
            AppFlagMeta actual = createFlagFor( ClassWithGetterSetters.class, "dtoField", "stringField" );

            AppFlagMeta expected = new AppFlagMeta(
                FPOption.none(),
                "dtoField.stringField",
                "dtoField.stringField",
                JavaClass.of( String.class ),
                FPOption.of( new JavaDoc( "stringField" ) ),
                FPOption.none()
            );

            assertEquals( expected, actual );
        }
    }

    @Nested
    public class LombokValueTestCases {
        @Test
        public void stringField() {
            AppFlagMeta actual = createFlagFor( LombokValueClass.class, "dtoField", "stringField" );

            AppFlagMeta expected = new AppFlagMeta(
                FPOption.none(),
                "dtoField.stringField",
                "dtoField.stringField",
                JavaClass.of( String.class ),
                FPOption.of( new JavaDoc( "stringField" ) ),
                FPOption.none()
            );

            assertEquals( expected, actual );
        }
    }

    @Nested
    public class LombokBuilderTestCases {
        @Test
        public void stringField() {
            AppFlagMeta actual = createFlagFor( LombokBuilderClass.class, "dtoField", "stringField" );

            AppFlagMeta expected = new AppFlagMeta(
                FPOption.none(),
                "dtoField.stringField",
                "dtoField.stringField",
                JavaClass.of( String.class ),
                FPOption.of( new JavaDoc( "stringField" ) ),
                FPOption.none()
            );

            assertEquals( expected, actual );
        }
    }


    public static class ClassWithPublicFields {
        /**
         * stringField
         */
        public String         stringField;

        /**
         * booleanField
         */
        public boolean        booleanField;

        public PublicFieldDto dtoField;
    }

    /**
     * PublicFieldDto
     */
    public static class PublicFieldDto {
        /**
         * PublicFieldDto.stringField
         */
        public String stringField;

        /**
         * PublicFieldDto2
         */
        public PublicFieldDto2 dtoField2;
    }

    /**
     * PublicFieldDto2
     */
    public static class PublicFieldDto2 {
        /**
         * PublicFieldDto2.booleanObjectField
         */
        public Boolean booleanObjectField;
    }

    @Getter
    @Setter
    public static class ClassWithGetterSetters {
        /**
         * stringField
         */
        public String         stringField;

        /**
         * booleanField
         */
        public boolean        booleanField;

        /**
         * In elementum ligula quis dui faucibus sagittis.
         */
        public ClassWithGetterSetters dtoField;
    }

    @Value
    @AllArgsConstructor
    public static class LombokValueClass {
        /**
         * stringField
         */
        public String         stringField;

        /**
         * booleanField
         */
        public boolean        booleanField;

        /**
         * In elementum ligula quis dui faucibus sagittis.
         */
        public LombokValueClass dtoField;

        public LombokValueClass() {
            this(null, false, null);
        }
    }

    @Value
    @Builder
    @AllArgsConstructor
    public static class LombokBuilderClass {
        /**
         * stringField
         */
        public String         stringField;

        /**
         * booleanField
         */
        public boolean        booleanField;

        /**
         * In elementum ligula quis dui faucibus sagittis.
         */
        public LombokValueClass dtoField;

        public LombokBuilderClass() {
            this(null, false, null);
        }
    }

    private AppFlagMeta createFlagFor( Class<?> dtoType, String firstPropertyName, String...propertyNames ) {
        JavaClass         javaClass    = JavaClass.of(dtoType);
        JavaProperty<?,?> javaProperty = javaClass.getPropertyMandatory( firstPropertyName );

        for ( String childPropertyName : propertyNames ) {
            javaProperty = javaProperty.getProperty( childPropertyName ).get();
        }

        return appFlagMetaFactory.createFlagFor( javaProperty ).get();
    }
}
