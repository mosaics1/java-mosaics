package mosaics.cli.meta;

import lombok.Builder;
import lombok.Value;
import mosaics.cli.App;
import mosaics.cli.CommandLineArg;
import mosaics.fp.FP;
import mosaics.fp.collections.FPIterable;
import mosaics.lang.reflection.JavaClass;
import mosaics.lang.reflection.JavaDoc;
import mosaics.lang.reflection.JavaDocTag;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class AppMetaReflectionFactoryTest {
    private AppMetaReflectionFactory appMetaReflectionFactory = new AppMetaReflectionFactory();

    // AppDocTestCases

    @Nested
    public class FlagTestCases {
        @Test
        public void withNoConfigClass_expectNoFlags() {
            AppMeta appMeta = appMetaReflectionFactory.createAppMetaFrom( AppWithLombokBuilderConfig.class, FP.emptyOption() );

            assertEquals( FP.emptyIterable(), appMeta.getFlags() );
        }

        @Test
        public void withLombokBuilderConfigClass_expectFlags() {
            AppMeta appMeta = appMetaReflectionFactory.createAppMetaFrom( AppWithLombokBuilderConfig.class, FP.option(LombokBuilderConfig.class) );
            FPIterable<AppFlagMeta> expected = FP.wrapAll(
                new AppFlagMeta(
                    FP.option('a'),
                    "foobar",
                    "foo",
                    JavaClass.STRING,
                    FP.option(
                        new JavaDoc(
                            List.of("Pellentesque habitant morbi tristique."),
                            List.of(new JavaDocTag("see", "AppWithLombokBuilderConfig") )
                        )
                    ),
                    FP.emptyOption()
                ),
                new AppFlagMeta(
                    FP.emptyOption(),
                    "bar",
                    "bar",
                    JavaClass.INTEGER_PRIMITIVE,
                    FP.option( new JavaDoc("Phasellus porttitor, leo id ornare eleifend.") ),
                    FP.option(1)
                )
            );

            assertEquals( expected, appMeta.getFlags() );
        }

        @Test
        public void withNestedLombokBuilderConfigClass_expectFlags() {
            AppMeta appMeta = appMetaReflectionFactory.createAppMetaFrom( AppWithLombokBuilderConfig.class, FP.option(NestedLombokBuilderConfig.class) );
            FPIterable<AppFlagMeta> expected = FP.wrapAll(
                new AppFlagMeta(
                    FP.option('a'),
                    "foobar",
                    "lbc.foo",
                    JavaClass.STRING,
                    FP.option(
                        new JavaDoc(
                            List.of("Pellentesque habitant morbi tristique."),
                            List.of(new JavaDocTag("see", "AppWithLombokBuilderConfig") )
                        )
                    ),
                    FP.emptyOption()
                ),
                new AppFlagMeta(
                    FP.emptyOption(),
                    "lbc.bar",
                    "lbc.bar",
                    JavaClass.INTEGER_PRIMITIVE,
                    FP.option( new JavaDoc("Phasellus porttitor, leo id ornare eleifend.") ),
                    FP.option(1)
                )
            );

            assertEquals( expected, appMeta.getFlags() );
        }
    }


    // RunMethodTestCases


    /**
     * Mauris vitae lorem varius, dignissim elit vel, euismod est.
     */
    @Value
    @Builder
    public static class LombokBuilderConfig {
        /**
         * Pellentesque habitant morbi tristique.
         *
         * @see AppWithLombokBuilderConfig
         */
        @CommandLineArg(shortName = 'a', longName = "foobar")
        private String foo;

        /**
         *Phasellus porttitor, leo id ornare eleifend.
         */
        private int bar = 1;
    }

    @Value
    @Builder
    public static class NestedLombokBuilderConfig {
        private LombokBuilderConfig lbc = LombokBuilderConfig.builder().build();
    }

    /**
     * Lorem ipsum dolor sit amet, consectetur adipiscing elit.
     */
    public static class AppWithLombokBuilderConfig extends App<LombokBuilderConfig> {
        /**
         * Aliquam porttitor purus sed lacus sodales
         *
         * @param arg1 Nulla a augue nisl.
         * @param arg2 Aenean egestas dolor ut pharetra feugiat.
         */
        public void run( String arg1, String arg2 ) {

        }
    }

}
