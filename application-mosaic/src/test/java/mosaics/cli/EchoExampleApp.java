package mosaics.cli;

import mosaics.lang.functions.Function1WithThrows;

import java.util.List;


/**
 * Write arguments to standard output.
 */
public class EchoExampleApp extends App {
    public enum TextMode {
        UNCHANGED( v -> v ), LOWERCASE( String::toLowerCase ), UPPERCASE( String::toUpperCase );

        private Function1WithThrows<String,String> converter;

        TextMode( Function1WithThrows<String, String> converter ) {
            this.converter = converter;
        }

        public Function1WithThrows<String,String> getConverter() {
            return converter;
        }
    }

    /**
     * Specify whether the text should be modified as it is printed to standard out.
     */
    @CommandLineArg(shortName = 't')
    public TextMode textMode = TextMode.UNCHANGED;

    public void run( List<String> text ) {
        text.forEach( this::printLine );
    }

    private void printLine( String line ) {
        System.out.println( textMode.getConverter().invokeSilently(line) );
    }
}
