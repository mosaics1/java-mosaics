plugins {
    `java-library`
    `maven-publish`  // adds new task  'publishToMavenLocal'
}


val javaVersion        : JavaVersion = JavaVersion.VERSION_17
val projectGroupId     : String      = "com.mosaics"
val debugFlag          : Boolean     = false
val gitLabProjectId    : Int         = 31624381
val gitLabGroupLabel   : String      = "mosaics1"
val gitLabProjectLabel : String      = "java-mosaics"
val gitLabPublishRepo  : Int         = gitLabProjectId


val scmConnection          : String = "git@gitlab.com:"+gitLabGroupLabel+"/"+gitLabProjectLabel+".git"
val scmDeveloperConnection : String = "https://gitlab.com/"+gitLabGroupLabel+"/"+gitLabProjectLabel+".git"
val scmUrl                 : String = "https://gitlab.com/"+gitLabGroupLabel+"/"+gitLabProjectLabel+"/-/tree/master"

val developerDetails = listOf(
    mapOf(
        "id" to "chrisk",
        "name" to "Chris Kirk",
        "email" to "kirkch@gmail.com"
    )
)



repositories {
    gradlePluginPortal()
    mavenCentral()

    maven { // adds the mosaics group repo
        url = uri("https://gitlab.com/api/v4/groups/8642646/-/packages/maven")
    }

    mavenLocal()
}


java {
    sourceCompatibility = JavaVersion.VERSION_17
    targetCompatibility = JavaVersion.VERSION_17

    withJavadocJar()
    withSourcesJar()
}

dependencies {
    val lombokVersion  = "1.18.22"
    val therapiVersion = "0.12.0"

    implementation("com.github.therapi:therapi-runtime-javadoc:$therapiVersion")
    testAnnotationProcessor("com.github.therapi:therapi-runtime-javadoc-scribe:$therapiVersion")

    compileOnly("org.projectlombok:lombok:$lombokVersion")
    annotationProcessor("org.projectlombok:lombok:$lombokVersion")
    testCompileOnly("org.projectlombok:lombok:$lombokVersion")
    testAnnotationProcessor("org.projectlombok:lombok:$lombokVersion")

    testImplementation("com.mosaics:junit-mosaic:485572458")
    testImplementation("org.slf4j:slf4j-nop:2.0.0-alpha6")
//    testImplementation("org.slf4j:slf4j-jdk14:1.7.32")
}


tasks.javadoc {
    if (JavaVersion.current().isJava9Compatible) {
        (options as StandardJavadocDocletOptions).addBooleanOption("html5", true)
    }
}

tasks.compileJava {
    options.isIncremental = true
    options.compilerArgs.add("-Xlint:deprecation")
}

val test by tasks.getting(Test::class) {
    useJUnitPlatform() {
        excludeTags("internal") // tests marked with 'internal' are not to be auto run - they have their own test harness that triggers them
    }
}

// (enable jvm preview features) see https://docs.gradle.org/current/userguide/building_java_projects.html#sec:feature_preview
tasks.withType<JavaCompile> {
    options.encoding = "UTF-8"
    options.compilerArgs.add("-parameters")
    options.compilerArgs.add("-Xlint:deprecation")
    options.compilerArgs.add("-Xlint:unchecked")
}

tasks.named<Test>("test") {
    systemProperty( "file.encoding", "UTF-8" )


    failFast = false
    reports {
        junitXml.apply {
            isOutputPerTestCase = true // defaults to false
            mergeReruns.set(true) // defaults to false
        }
    }

    reports.html.required.set(false)

    testLogging.showExceptions = true
    testLogging.exceptionFormat = org.gradle.api.tasks.testing.logging.TestExceptionFormat.FULL
    testLogging.showCauses = true
    testLogging.showStackTraces = true
    testLogging.showStandardStreams = debugFlag

    // https://stackoverflow.com/questions/56628983/how-to-give-system-property-to-my-test-via-kotiln-gradle-and-d/62629332#62629332
    @Suppress("UNCHECKED_CAST")
    systemProperties(System.getProperties().toMap() as Map<String,Any>)
}

// Share the test report data to be aggregated for the whole project
// See https://docs.gradle.org/current/userguide/java_testing.html#test_reporting
configurations.create("binaryTestResultsElements") {
    isCanBeResolved = false
    isCanBeConsumed = true
    attributes {
        attribute(Category.CATEGORY_ATTRIBUTE, objects.named(Category.DOCUMENTATION))
        attribute(DocsType.DOCS_TYPE_ATTRIBUTE, objects.named("test-report-data"))
    }
    outgoing.artifact(tasks.test.map { task -> task.getBinaryResultsDirectory().get() })
}

tasks.withType<JavaExec> {

}

tasks.withType<Javadoc> {
    options.encoding = "UTF-8"

    val javadocOptions = options as CoreJavadocOptions

    javadocOptions.addStringOption("source", javaVersion.toString())
    javadocOptions.addBooleanOption("Xdoclint:none", true)  // todo remove this and fix the errors, test with gradlew javadoc
}

tasks.withType<Jar> {
    manifest {
        attributes["Main-Class"] = "com.example.MainKt"
        //Created-By: 11.0.3 (AdoptOpenJDK)
        //Built-By: baeldung
        //Build-Jdk: 11.0.3
        /*
        Name: the package
Implementation-Build-Date: the build date for the implementation
Implementation-Title: the title of the implementation
Implementation-Vendor: the vendor for the implementation
Implementation-Version: the implementation version
         */
    }
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            groupId    = projectGroupId
            artifactId = project.name
            version    = System.getenv("CI_PIPELINE_ID") ?: "SNAPSHOT"

            from(components["java"])

            repositories {
                maven {
                    name = "GitLab"
                    url = uri("https://gitlab.com/api/v4/projects/"+gitLabPublishRepo+"/packages/maven")

                    credentials(HttpHeaderCredentials::class) {
                        name = "Job-Token"
                        value = System.getenv("CI_JOB_TOKEN")
                    }

                    authentication {
                        create<HttpHeaderAuthentication>("header")
                    }
                }
            }

            pom {
                name.set(project.name)
                description.set(project.description)
                url.set("https://gitlab.com/"+gitLabGroupLabel+"/"+gitLabProjectLabel)
                developers {
                    developerDetails.forEach({d ->
                        developer {
                            id.set(d.get("id"))
                            name.set(d.get("name"))
                            email.set(d.get("email"))
                        }
                    })
                }
                scm {
                    connection.set(scmConnection)
                    developerConnection.set(scmDeveloperConnection)
                    url.set(scmUrl)
                }
            }
        }
    }
}
